package cronjobservice.services.implementation;

import com.fasterxml.jackson.databind.ObjectMapper;

import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.FormNumber;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by chiomarose on 07/09/2017.
 */
@Service
public class FormARetailServiceImpl implements FormARetailService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private FormARepo formARepo;
    private MessageSource messageSource;
    private WorkFlowRepo workFlowRepo;
    private ConfigurationService configurationService;
    private RetailUserService retailUserService;
    private AccountService accountService;
    @Autowired
    private RestTemplate template;
    @Value("${tradeservice.service.uri}")
    private String URI;
    @Autowired
    private WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    private BranchRepo branchRepo;
    @Autowired
    private CodeRepo codeRepo;
    @Autowired
    private FormaDocumentRepo formaDocumentRepo;
    @Autowired
    private FormaAllocationRepo formaAllocationRepo;
    private Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    CodeService codeService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EntityManager entityManager;

//    @Autowired
//    CommentService commentService;

    @Autowired
    MailService mailService;

    @Value("${forma.upload.files}")
    private String UPLOADED_FOLDER;

    public FormARetailServiceImpl(FormARepo formARepo, MessageSource messageSource, WorkFlowRepo workFlowRepo, ConfigurationService configurationService, RetailUserService retailUserService, CorporateService corporateService, AccountService accountService) {
        this.formARepo = formARepo;
        this.messageSource = messageSource;
        this.workFlowRepo = workFlowRepo;
        this.configurationService = configurationService;
//        this.userGroupService = userGroupService;
        this.retailUserService = retailUserService;
        this.accountService = accountService;
//        this.retailBeneficiaryService = retailBeneficiaryService;
    }

    @Override
    public FormADTO editFormA(Long id) throws CronJobException {
        FormA formA = formARepo.findOne(id);
        return convertEntityTODTO(formA);
    }

    @Override
    public String deleteFormA(Long id) throws CronJobException {
        try {
            FormA formA = formARepo.findOne(id);
            logger.info("getting the form a status {}", formA.getStatus());
            if (formA.getStatus().equalsIgnoreCase("S") || formA.getStatus().equalsIgnoreCase("P") || formA.getStatus().equalsIgnoreCase("D")) {
                formA.setDelFlag("Y");
                formARepo.save(formA);

                return messageSource.getMessage("forma.delete.success", null, locale);

            } else {
                throw new CronJobException(messageSource.getMessage("forma.delete.failure", null, locale));

            }
        } catch (CronJobException e) {
            throw new CronJobException(messageSource.getMessage("forma.delete.failure", null, locale));
        }


    }

    @Override
    public String addAttachments(MultipartFile file, String formaNumber, String documentType) {
        String uploadedFileName = "";


        if (file.isEmpty()) {
            System.out.println("file is empty");
        }

        try {
            byte[] bytes = file.getBytes();
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User users = principal.getUser();

            if (users.getUserType().equals(UserType.RETAIL)) {
                Path  path = Paths.get(UPLOADED_FOLDER + formaNumber + documentType + file.getOriginalFilename());
                logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());


                File f = new File(path.toString());
                if(f.exists()){
                    System.out.println("file already exist");
                }else{
                    System.out.println("file does not exist");
                }

                Files.write(path, bytes);
                logger.info("file uploaded successfully to {} directory", path);
                uploadedFileName =file.getOriginalFilename();
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("failed to upload file");
        }

        logger.info("uploadFileName {}", uploadedFileName);
        if (StringUtils.isEmpty(uploadedFileName)) {
            logger.info("No file added, Please select a file to upload");

        } else {
            logger.info("You successfully uploaded {}", uploadedFileName);

        }
        return uploadedFileName;
    }

    @Override
    public Boolean checkAvailableBalance(String amount, String accountId) throws CronJobException {
        Boolean result;
        Map<String, BigDecimal> availableBalance = integrationService.getBalance(accountId);
        BigDecimal availableAmount = availableBalance.get("availableBalance");
        if (!(amount.isEmpty())) {
            new BigDecimal(amount);
        }
        logger.info("Amount {}",amount);
        logger.info("Available balance {}",availableAmount);
        if (!(amount).isEmpty()) {
            int compareResult = new BigDecimal(amount).compareTo(availableAmount);

            if (compareResult == 1) {
                throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
            }

        }
        result = true;

        return result;
    }

    @Override
    public List<AccountDTO> getCustomerNairaAccounts() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> nairaAccountList = new ArrayList<>();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        if ((users.getUserType()).equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) users;

            nairaAccountList = accountService.getCustomerNairaAccounts(retailUser.getCustomerId(), "NGN");

        }


        return nairaAccountList;

    }

    @Override
    public List<AccountDTO> getCustomerAccounts() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> accountList = new ArrayList<>();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        if ((users.getUserType()).equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) users;

            accountList = accountService.getAccounts(retailUser.getCustomerId());

        }


        return accountList;
    }


    public List<AccountDTO> getCustomerDomAccounts() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> domaccountList = new ArrayList<>();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }

        if (users.getUserType().equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) users;

            domaccountList = accountService.getCustomerDomAccounts(retailUser.getCustomerId(), "NGN");

        }
        return domaccountList;
    }

    @Override
    public FormADTO getInitiatorDetails() throws CronJobException {

        FormADTO formADTO = new FormADTO();
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        if ((users.getUserType()).equals(UserType.RETAIL)) {
            RetailUser user = (RetailUser) users;
            formADTO.setFirstName(user.getFirstName());
            formADTO.setLastName(user.getLastName());
            formADTO.setAddress(user.getAddress());
            formADTO.setPhone(user.getPhoneNumber());
            formADTO.setTown(user.getTown());
            formADTO.setState(user.getState());
            formADTO.setCountry(user.getCountry());
            formADTO.setEmail(user.getEmail());
        } else {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        return formADTO;

    }

    @Override
    public FormADTO getFormA(Long id) throws CronJobException {
        FormA formA = formARepo.findOne(id);
        FormADTO formADTO =convertEntityTODTO(formA);
//        if(formA.getDocuments() != null) {
//            Iterator<FormaDocument> formaDocumentIterator = formA.getDocuments().iterator();
//            Iterator<FormaDocumentDTO> formaDocumentDTOIterator = formADTO.getDocuments().iterator();
//
//            while (formaDocumentIterator.hasNext() && formaDocumentDTOIterator.hasNext()) {
//                FormaDocument formaDocument = formaDocumentIterator.next();
//                FormaDocumentDTO formaDocumentDTO = formaDocumentDTOIterator.next();
//                if (formaDocument.getDocumentType() == null) {
//                    formaDocumentDTOIterator.remove();
//                    formaDocumentIterator.remove();
//                } else {
//                    formaDocumentDTO.setTypeOfDocument(formaDocument.getDocumentType().getCode());
//                }
//            }
//        }
        return formADTO;
    }


    public List<CommentDTO> getComments(Long id) throws CronJobException {
        FormA formA = formARepo.findOne(id);
        List<CommentDTO> commentDTOList = new ArrayList<>();
        if (formA != null) {
            commentDTOList = convertEntitiestoDTOs(formA.getComment());
        }
        return commentDTOList;
    }

    @Override
    public Page<FormADTO> getFormAs(Pageable pageDetails) {
        Page<FormA> page = formARepo.findStatus(" ", pageDetails);
        List<FormADTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormADTO> pageImpl = new PageImpl<FormADTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<FormADTO> getAllFormAs(Pageable pageDetails) {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        RetailUser retailUser = (RetailUser) users;

        Page<FormA> page = formARepo.findByClosedCifidAndStatus(retailUser.getCustomerId(), pageDetails);
        List<FormADTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        List<FormADTO> sortedList = dtOs.stream().sorted(Comparator.comparing(FormADTO::getDateCreated).reversed()).collect(Collectors.toList());
        Page<FormADTO> page1=new PageImpl<FormADTO>(sortedList,pageDetails,t);
        return page1;
    }

    @Override
    public Page<FormADTO> getOpenFormAs(Pageable pageable) {

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        RetailUser retailUser = (RetailUser) users;
        Page<FormA> pages = formARepo.findByCifidAndStatus(retailUser.getCustomerId(), "P", "S", "D", pageable);
        List<FormADTO> formADTOS = convertEntitiesToDTOs(pages.getContent());
        long t = pages.getTotalElements();
        List<FormADTO> sortedList = formADTOS.stream().sorted(Comparator.comparing(FormADTO::getDateCreated).reversed()).collect(Collectors.toList());
        Page<FormADTO> page1=new PageImpl<FormADTO>(sortedList,pageable,t);
        return page1;
    }

    @Override
    public Page<FormADTO> getClosedFormAs(Pageable pageable) {
        Page<FormA> page = formARepo.findByStatusNotNull(pageable);
        List<FormADTO> formADTOS = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        List<FormADTO> sortedList = formADTOS.stream().sorted(Comparator.comparing(FormADTO::getDateCreated).reversed()).collect(Collectors.toList());
        Page<FormADTO> page1=new PageImpl<FormADTO>(sortedList,pageable,t);
        return page1;
    }


    private FormADTO convertEntityTODTOView(FormA formA) {
        FormADTO formADTO = modelMapper.map(formA, FormADTO.class);
        if (formADTO.getStatus() != null && !(formADTO.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formADTO.getWorkFlow().getCodeType(), formADTO.getStatus());
            formADTO.setStatusDesc(workFlowStates.getDescription());
            formADTO.setWorkFlow(formA.getWorkFlow());
            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
        }

        if (formA.getDateCreated() != null) {
            formADTO.setDateCreated(formA.getDateCreated());
        }
        if (formA.getAmount() != null) {
            formADTO.setAmount(formA.getAmount().toString());
        }
        if (formA.getAllocatedAmount() != null) {
            formADTO.setAllocatedAmount(formA.getAllocatedAmount().toString());
        }
        if (formA.getUtilization() != null) {
            formADTO.setUtilization(formA.getUtilization().toString());
        }
        if(formA.getDocSubBranch() != null){
            formADTO.setBranch(formA.getDocSubBranch().getId().toString());
        }
        return formADTO;
    }

    private CommentDTO convertEntityToDTO(Comments comments) {
        return modelMapper.map(comments, CommentDTO.class);
    }


    private FormADTO convertEntityTODTO(FormA formA) {
        FormADTO formADTO = modelMapper.map(formA, FormADTO.class);

        if (formADTO.getStatus() != null && !(formADTO.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formADTO.getWorkFlow().getCodeType(), formADTO.getStatus());
            formADTO.setStatusDesc(workFlowStates.getDescription());
            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
            formADTO.setWorkFlow(formA.getWorkFlow());
        }
        if(formA.getPurposeOfPayment() != null && !"".equals(formA.getPurposeOfPayment())){
            Code codec = codeService.getByTypeAndCode("PURPOSE_OF_PAYMENT", formA.getPurposeOfPayment());
            formADTO.setPurposeOfPaymentDesc(codec.getDescription());
        }
        if (formADTO.getStatus().equals("P")) {
            formADTO.setStatusDesc("Saved");
            formADTO.setWorkFlow(formA.getWorkFlow());
            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
        }
        if (formA.getDateCreated() != null) {
            formADTO.setDateCreated(formA.getDateCreated());
        }
        if (formA.getAmount() != null && formA.getCurrencyCode() != null) {
            formADTO.setCurrAmount(formA.getCurrencyCode() + " " + formA.getAmount().toString());
        }
        if (formA.getAmount() != null) {
            formADTO.setAmount(formA.getAmount().toString());
        }

        if(formA.getDocSubBranch() != null){
            formADTO.setBranch(formA.getDocSubBranch().getId().toString());
        }

        return formADTO;
    }

    private List<FormADTO> convertEntitiesToDTOs(Iterable<FormA> formAs) {
        List<FormADTO> formADTOs = new ArrayList<FormADTO>();
        for (FormA formA : formAs) {
            formADTOs.add(convertEntityTODTO(formA));
        }
        return formADTOs;
    }


    @Override
    public long getTotalNumberOpenFormA() {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User logginUser = principal.getUser();
        long totalNumberOfOpenRequest = 0;
        if (logginUser.getUserType().equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) logginUser;
            totalNumberOfOpenRequest = formARepo.countByCifidAndStatus(retailUser.getCustomerId(), "P", "S", "D");
        }

        return totalNumberOfOpenRequest;
    }

    //getting the total number of forma for retail user
    @Override
    public long getTotalNumberFormA() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User logginUser = principal.getUser();
        long totalNumberOfRequest = 0;
        if (logginUser.getUserType().equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) logginUser;
            totalNumberOfRequest = formARepo.countByCifid(retailUser.getCustomerId());
        }

        return totalNumberOfRequest;
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS) {
        List<CommentDTO> commentDTOList = new ArrayList<>();

        for (Comments comments : commentDTOS) {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return commentDTOList;
    }


    @Override
    public Iterable<FormADTO> getAllFormAs() {
        Iterable<FormA> formAs = formARepo.findAll();
        return convertEntitiesToDTOs(formAs);
    }

    public FormA convertDTOTOEntity(FormADTO formADTO) {
        logger.info("saving the form a{}", formADTO.getWorkFlowId());
        WorkFlow workFlow = workFlowRepo.findOne(formADTO.getWorkFlowId());
        logger.info("saving the form a{}", workFlow);
        formADTO.setWorkFlow(workFlow);
        logger.info("getting date created {}",formADTO.getDateCreated());
        logger.info("getting date expired {}",formADTO.getExpiryDate());
        String bidRateCode = configurationService.getSettingByName("FORMA_RATE_CODE").getValue();


        FormA formA = modelMapper.map(formADTO, FormA.class);
        formA.setBidRateCode(bidRateCode);
        logger.info("saving the form a{}",formA);
        return formA;
    }


//    @Override
//    public String saveFormA(FormADTO formADTO) throws CronJobException {
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User doneBy = principal.getUser();
//
//        RetailUser retailUser = (RetailUser) doneBy;
//
//        if (doneBy == null) {
//            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
//        }
//
//        try {
//            if (formADTO.isSaveBeneficiary()){
//                saveBeneficiary(retailUser, formADTO);
//            }
//
//            String generatedFormNumber = FormNumber.getFormNumber();
//            if(formADTO.getDocuments() != null) {
//                for (FormaDocumentDTO d : formADTO.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, generatedFormNumber, d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//            }
//
//            FormA formA = convertDTOTOEntity(formADTO);
//            formA.setUserType(doneBy.getUserType().toString());
//            formA.setInitiatedBy(doneBy.getUserName());
//            formA.setDateCreated(new Date());
//            formA.setCifid(retailUser.getCustomerId());
//            formA.setFormNumber(generatedFormNumber);
//            formA.setTempFormNumber(generatedFormNumber);
//            formA.setUtilization(new BigDecimal(0));
//            formA.setAllocatedAmount(new BigDecimal(0));
//            formA.setExpiryDate(getFormExpiryDate());
//            formA.setCountry("NG");
//            formA.setSubmitFlag("U");
//
//            /**
//             * BillsDocument table
//             */
//            if(!"".equalsIgnoreCase(formADTO.getBranch()) && null != formADTO.getBranch()) {
//                System.out.println(formADTO.getBranch());
//                Branch branch = branchRepo.findOne(Long.parseLong(formADTO.getBranch()));
//                formA.setDocSubBranch(branch);
//            }
//
//            if(formADTO.getDocuments() != null) {
//                Iterator<FormaDocument> DocumentIterator = formA.getDocuments().iterator();
//                Iterator<FormaDocumentDTO> DocumentDTOIterator = formADTO.getDocuments().iterator();
//
//                while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                    FormaDocument document = DocumentIterator.next();
//                    FormaDocumentDTO documentDTO = DocumentDTOIterator.next();
//                    if (documentDTO.getTypeOfDocument() == null) {
//                        DocumentIterator.remove();
//                        DocumentDTOIterator.remove();
//                    } else {
//                        document.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                        document.setFormA(formA);
//                    }
//                }
//            }
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_A_WF");
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            formA.setWorkFlow(workFlow);
//            formARepo.save(formA);
//            if(formADTO.getStatus().equalsIgnoreCase("S")){
//                sendITSmail(retailUser,formA);
//            }
//            return messageSource.getMessage("forma.add.success", null, locale);
//        } catch (CronJobException e) {
//            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale), e);
//        }
//    }



//    private void saveBeneficiary(RetailUser retailUser, FormADTO formADTO){
//        BeneficiaryDTO beneficiaryDTO = new BeneficiaryDTO();
//        beneficiaryDTO.setBeneficiaryName(formADTO.getBeneficiaryName());
//        beneficiaryDTO.setBeneficiaryAddress(formADTO.getBeneficiaryAddress());
//        beneficiaryDTO.setBeneficiaryAccount(formADTO.getBeneficiaryAccount());
//        beneficiaryDTO.setBeneficiaryCountry(formADTO.getBeneficiaryCountry());
//        beneficiaryDTO.setBeneficiaryState(formADTO.getBeneficiaryState());
//        beneficiaryDTO.setBeneficiaryPostCode(formADTO.getBeneficiaryPostCode());
//        beneficiaryDTO.setBeneficiaryTown(formADTO.getBeneficiaryTown());
//        beneficiaryDTO.setBankId(formADTO.getBeneBankId());
//
//        logger.info("BENEFICIARY DETAILS {}", beneficiaryDTO);
//        retailBeneficiaryService.addBeneficiary(retailUser, beneficiaryDTO);
//    }

    @Override
    public String deleteForm(Long id) throws CronJobException {
        FormA formA = formARepo.findOne(id);

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();

        if (doneBy == null) {
            logger.info("user is null");
            throw new CronJobException(messageSource.getMessage("forma.delete.failure", null, locale));
        }
        try {
            if(!formA.getStatus().equalsIgnoreCase("P")){
                throw new CronJobException(messageSource.getMessage("forma.delete.saved.failure", null, locale));
            }
            formARepo.delete(formA);
            return messageSource.getMessage("forma.delete.success", null, locale);
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("forma.delete.failure", null, locale), e);
        }

    }


//    @Override
//    public String updateFormA(FormADTO formADTO) throws CronJobException {
//        FormA formA = formARepo.findOne(formADTO.getId());
//
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User doneBy = principal.getUser();
//
//        RetailUser retailUser = (RetailUser) doneBy;
//        if (doneBy == null) {
//            logger.info("user is null");
//            throw new CronJobException(messageSource.getMessage("forma.update.failure", null, locale));
//        }
//        try {
//            if (formADTO.isSaveBeneficiary()){
//                saveBeneficiary(retailUser, formADTO);
//            }
//
//            if(formADTO.getDocuments() != null) {
//                for (FormaDocumentDTO d : formADTO.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        System.out.println("forma service saving the document");
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, formA.getTempFormNumber(), d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//            }
//
//
//            entityManager.detach(formA);
//            formA = convertDTOTOEntity(formADTO);
//            formA.setDateCreated(new Date());
//            formA.setExpiryDate(getFormExpiryDate());
//            formA.setUserType(doneBy.getUserType().toString());
//            formA.setInitiatedBy(doneBy.getUserName());
//            formA.setCifid(retailUser.getCustomerId());
//            formA.setCountry("NG");
//            formA.setSubmitFlag("U");
//
//            /**
//             * BillsDocument table
//             */
//            if(!"".equalsIgnoreCase(formADTO.getBranch()) && null != formADTO.getBranch()) {
//                System.out.println(formADTO.getBranch());
//                Branch branch = branchRepo.findOne(Long.parseLong(formADTO.getBranch()));
//                formA.setDocSubBranch(branch);
//            }
//            if(formADTO.getDocuments() != null) {
//                Iterator<FormaDocument> DocumentIterator = formA.getDocuments().iterator();
//                Iterator<FormaDocumentDTO> DocumentDTOIterator = formADTO.getDocuments().iterator();
//                Collection<FormaDocument> fields = new ArrayList<FormaDocument>();
//                while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                    FormaDocument document = DocumentIterator.next();
//                    FormaDocumentDTO documentDTO = DocumentDTOIterator.next();
//                    FormaDocument formaDocument = null;
//                    if (documentDTO.getTypeOfDocument() == null) {
//                        DocumentIterator.remove();
//                        DocumentDTOIterator.remove();
//                    } else if (documentDTO.getId() == null) {
//                        System.out.println("newly added document");
//                        formaDocument = new FormaDocument();
//                        System.out.println("before model mapper");
//                        modelMapper.map(documentDTO, formaDocument);
//                        System.out.println("after model mapper");
//                        formaDocument.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                        formaDocument.setFormA(formA);
//                        System.out.println("newly added document ending");
//                    } else {
//                        System.out.println("existing document");
//                        formaDocument = formaDocumentRepo.findOne(documentDTO.getId());
//                        System.out.println("after finding existing document");
//                        formaDocument.setDocumentValue(documentDTO.getDocumentValue());
//                        formaDocument.setDocumentRemark(documentDTO.getDocumentRemark());
//                        formaDocument.setDocumentNumber(documentDTO.getDocumentNumber());
//                        formaDocument.setDocumentDate(documentDTO.getDocumentDate());
//                        if (documentDTO.getFileUpload() != null) {
//                            formaDocument.setFileUpload(documentDTO.getFileUpload());
//                        }
//                        formaDocument.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                        formaDocument.setFormA(formA);
//                        System.out.println("ending of  existing document");
//
//                    }
//                    fields.add(formaDocument);
//                }
//                formA.setDocuments(fields);
//            }
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_A_WF");
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            formARepo.save(formA);
//            return messageSource.getMessage("form.update.success", null, locale);
//        } catch (CronJobException e) {
//
//            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
//        }
//
//    }

    @Override
    public Boolean updateFormAFromFinacle(FormA tmsForm, FormA finForm) throws CronJobException {
        System.out.println("got into the service");

        try {
            tmsForm.setFormNumber(finForm.getFormNumber());
            tmsForm.setTranId(finForm.getTranId());
            formARepo.save(tmsForm);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }


    }

    @Override
    @Transactional
    public Boolean updateFormAAllocation(FormA tmsForm, FormA finForm,FormADTO formADTO) throws CronJobException {
        try {
            if(null != finForm){
                List<FormaAllocation> formaAllocations = formaAllocationRepo.findByFormaNumberAndDealId(finForm.getFormNumber(), finForm.getDealId());
                if(formaAllocations.size()>1){
                    int counter =0;
                    for (FormaAllocation formaAllocation:formaAllocations) {
                        if(counter==0){
                            continue;
                        }else {

                            formaAllocationRepo.delete(formaAllocation);
                        }
                        counter++;
                    }
                }else if(formaAllocations.size()==0){
                    FormaAllocation formaAllocation = new FormaAllocation();
                    formaAllocation.setAllocationAccount(finForm.getAllocationAccount());
                    formaAllocation.setAllocationAmount(finForm.getAllocatedAmount());
                    formaAllocation.setAllocationCurrency(finForm.getAllocationCurrencyCode());
                    formaAllocation.setExchangeRate(finForm.getExchangeRate());
                    formaAllocation.setFormaNumber(finForm.getFormNumber());
                    formaAllocation.setDateOfAllocation(finForm.getDateOfAllocation());
                    formaAllocation.setDealId(finForm.getDealId());
                    formaAllocation.setTranId(finForm.getTranId());
                    formaAllocation.setCreatedOn(new Date());
                    formaAllocationRepo.save(formaAllocation);

                    BigDecimal formAmount = tmsForm.getAmount();
                    BigDecimal utilized = tmsForm.getUtilization();
                    int i = formAmount.compareTo(utilized);
                    if(i == 0){
                        tmsForm.setStatus("R");
                    }else if((utilized.compareTo(BigDecimal.ZERO) == 0)){
                        tmsForm.setStatus("L");
                    }else if(i > 0 && !(utilized.compareTo(BigDecimal.ZERO) == 0)){
                        tmsForm.setStatus("PR");
                    }
                    formARepo.save(tmsForm);
                    String uri = URI + "/api/forma/allocationresponse";
                    template.postForObject(uri, formADTO, String.class);
                    logger.info("Successly allocated form {}", formADTO.getFormNumber());
                    return true;
                }

            }
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }catch (Exception e){
            logger.info("the can't create allocation because {}",e);
        }
        return false;
    }

    @Override
    public Boolean updateFormAUtilization(FormA formA, Remittance remittance) throws CronJobException {
        try {
            BigDecimal remitUtilization = new BigDecimal(remittance.getTransferAmount());
            BigDecimal formaUtilization = formA.getUtilization();
            BigDecimal updatedUtilization = remitUtilization.add(formaUtilization);
            formA.setAllocatedAmount(remitUtilization);
            formA.setUtilization(updatedUtilization);
            formARepo.save(formA);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

    private Date getFormExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        SettingDTO setting = configurationService.getSettingByName("FORM_A_EXPIRY");
        if (setting != null && setting.isEnabled()) {
            int days = NumberUtils.toInt(setting.getValue());
            calendar.add(Calendar.DAY_OF_YEAR, days);
            return calendar.getTime();
        }
        return null;
    }

    @Override
    public AccountDTO getAccountByAccountNumber(String accountNumber) {

        if(accountNumber==null)
        {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }

        AccountDTO  accountDTO=accountService.getAccountByAccountNumber(accountNumber);

        return accountDTO;
    }

    private void sendITSmail(RetailUser user, FormA formADTO) {

        try {
            SettingDTO settingDTO = configurationService.getSettingByName("GROUP_EMAIL");
            String itsMail = settingDTO.getValue();
            String fullName = user.getLastName()+" "+user.getFirstName();
            String formType = "form A";
            Context context = new Context();
            context.setVariable("name", fullName);
            context.setVariable("username", user.getUserName());
            context.setVariable("formNumber", formADTO.getFormNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", new Date());
            context.setVariable("customerId", user.getCustomerId());
            context.setVariable("code", "FORM_A");
            String subject = String.format(messageSource.getMessage("form.submission.subject", null, locale),formType);
            Email email = new Email.Builder()
                    .setRecipient(itsMail)
                    .setSubject(subject)
                    .setTemplate("mail/formSubmission")
                    .build();

            mailService.sendMail(email, context);
        } catch (Exception e) {
            logger.error("Error occurred sending activation credentials", e);
        }
    }

}
