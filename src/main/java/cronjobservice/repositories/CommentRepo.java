package cronjobservice.repositories;

import cronjobservice.models.Comments;
import org.springframework.stereotype.Repository;

/**
 * Created by chiomarose on 13/09/2017.
 */
@Repository
public interface CommentRepo extends CommonRepo<Comments,Long>
{


}
