package cronjobservice.exception;

/**
 * Created by Fortune on 5/9/2017.
 */
public class DuplicateObjectException extends CronJobException {

    public DuplicateObjectException(){
        super("The target object already exists");
            }

    public DuplicateObjectException(String message){
        super(message);
    }
}
