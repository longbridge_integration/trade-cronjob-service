package cronjobservice.jobs.formqjobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.services.FormQService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;


@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class FormQAllocationJob implements Job {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextFormA = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextFormA.getBean (CronJobService.class);
        FormQService formQService = contextFormA.getBean (FormQService.class);

//        System.out.println("five minute job runing");
        try {
            logger.info("Form Q allocation Job starting");
            cronJobService.saveRunningJob(JobCategory.FormQ_ALLOCATION.toString(),cronJobService.getCurrentExpression(JobCategory.FormQ_ALLOCATION.toString()));

            formQService.getFormQAllocation();
            cronJobService.updateRunningJob(JobCategory.FormQ_ALLOCATION.toString());
            logger.info("Form Q allocation Job ends");
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
