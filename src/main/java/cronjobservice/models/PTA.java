package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Inheritance
@DiscriminatorColumn(name="type")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class PTA extends Process{


    private String refNumber;
    @ManyToOne
    private Branch docSubBranch;
    private String appName;
    private String appAddress;
    private String appPhone;

    private String appEmail;
    private String appCountry;
    private String typeOfBusiness;

    private String passportNo;
    private String bvn;
    private String passIssueDate;

    private BigDecimal forexInFigure; //Amount BigDecimal
    private String nairaInFigure;
    private String modeOfPayments;
    private boolean guidelineApproval;
    private boolean paymentApproval;

    private String travelDate;
    private String flightHour;
    private String currency;
    private String countryOfVisit;
    private String rate;
    private String town;
    private String state;
    private String counter;
    private Date counterDate;
    private String dateOfBirth;
    private String oneTimeTrip;
    private String returnTrip;
    private boolean immigrants;
    private boolean student;
    private String accountNumber;
    private String endDate;
    private String initiatedBy;
    private String userType;
    private String approvedBy;
    private String solId;
    private String RequestType;
    private String paymentInitiatedBy;
    private String chargeFlag;
    private String chargePcnt;



    public String getChargePcnt() {
        return chargePcnt;
    }

    public void setChargePcnt(String chargePcnt) {
        this.chargePcnt = chargePcnt;
    }



    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public Branch getDocSubBranch() {
        return docSubBranch;
    }

    public void setDocSubBranch(Branch docSubBranch) {
        this.docSubBranch = docSubBranch;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppAddress() {
        return appAddress;
    }

    public void setAppAddress(String appAddress) {
        this.appAddress = appAddress;
    }

    public String getAppPhone() {
        return appPhone;
    }

    public void setAppPhone(String appPhone) {
        this.appPhone = appPhone;
    }

    public String getAppEmail() {
        return appEmail;
    }

    public void setAppEmail(String appEmail) {
        this.appEmail = appEmail;
    }

    public String getAppCountry() {
        return appCountry;
    }

    public void setAppCountry(String appCountry) {
        this.appCountry = appCountry;
    }

    public String getTypeOfBusiness() {
        return typeOfBusiness;
    }

    public void setTypeOfBusiness(String typeOfBusiness) {
        this.typeOfBusiness = typeOfBusiness;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public String getPassIssueDate() {
        return passIssueDate;
    }

    public void setPassIssueDate(String passIssueDate) {
        this.passIssueDate = passIssueDate;
    }

    public BigDecimal getForexInFigure() {
        return forexInFigure;
    }

    public void setForexInFigure(BigDecimal forexInFigure) {
        this.forexInFigure = forexInFigure;
    }

    public String getNairaInFigure() {
        return nairaInFigure;
    }

    public void setNairaInFigure(String nairaInFigure) {
        this.nairaInFigure = nairaInFigure;
    }

    public String getModeOfPayments() {
        return modeOfPayments;
    }

    public void setModeOfPayments(String modeOfPayments) {
        this.modeOfPayments = modeOfPayments;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public boolean isPaymentApproval() {
        return paymentApproval;
    }

    public void setPaymentApproval(boolean paymentApproval) {
        this.paymentApproval = paymentApproval;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public String getFlightHour() {
        return flightHour;
    }

    public void setFlightHour(String flightHour) {
        this.flightHour = flightHour;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountryOfVisit() {
        return countryOfVisit;
    }

    public void setCountryOfVisit(String countryOfVisit) {
        this.countryOfVisit = countryOfVisit;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public Date getCounterDate() {
        return counterDate;
    }

    public void setCounterDate(Date counterDate) {
        this.counterDate = counterDate;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getOneTimeTrip() {
        return oneTimeTrip;
    }

    public void setOneTimeTrip(String oneTimeTrip) {
        this.oneTimeTrip = oneTimeTrip;
    }

    public String getReturnTrip() {
        return returnTrip;
    }

    public void setReturnTrip(String returnTrip) {
        this.returnTrip = returnTrip;
    }

    public boolean isImmigrants() {
        return immigrants;
    }

    public void setImmigrants(boolean immigrants) {
        this.immigrants = immigrants;
    }

    public boolean isStudent() {
        return student;
    }

    public void setStudent(boolean student) {
        this.student = student;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getSolId() {
        return solId;
    }

    public void setSolId(String solId) {
        this.solId = solId;
    }

    public String getRequestType() {
        return RequestType;
    }

    public void setRequestType(String requestType) {
        RequestType = requestType;
    }

    public String getPaymentInitiatedBy() {
        return paymentInitiatedBy;
    }

    public void setPaymentInitiatedBy(String paymentInitiatedBy) {
        this.paymentInitiatedBy = paymentInitiatedBy;
    }

    public String getChargeFlag() {
        return chargeFlag;
    }

    public void setChargeFlag(String chargeFlag) {
        this.chargeFlag = chargeFlag;
    }
}
