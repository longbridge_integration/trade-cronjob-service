package cronjobservice.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import cronjobservice.models.UserType;

import java.util.Date;

/**
 * Created by chiomarose on 13/09/2017.
 */
public class CommentDTO {


    @JsonProperty("DT_RowId")
    private Long id;

    private int version;

    private String comment;

    private Date madeOn;

    private String username;

    private UserType userType;

    private String status;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public Date getMadeOn()
    {
        return madeOn;
    }

    public void setMadeOn(Date madeOn)
    {
        this.madeOn = madeOn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
