package cronjobservice.jobs;


import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

/**
 * Created by Longbridge on 9/12/2017.
 */
@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class FormAJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextFormA = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextFormA.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
//            cronJobService.saveRunningJob("in-built",cronJobService.getCurrentExpression());
//            cronJobService.submitFormAToFinacle();
//            cronJobService.getFormAResponse();
//            cronJobService.getFormAAllocated();
//            cronJobService.getFormAAddedOnFinacle();
//            cronJobService.updateRunningJob();
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
