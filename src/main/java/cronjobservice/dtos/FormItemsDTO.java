package cronjobservice.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by user on 9/22/2017.
 */
public class FormItemsDTO {

    @JsonProperty("DT_RowId")
    private Long id;

    private String formNum;

    private String hsCode;

    private String goodsDescription;


    private String stateGoods;

    private String sectoralPurpose;

    private String   unitMeasure;

    private long  fobvalue;

    private long  frieghtCharge;

    private String unitPrice;

    private  String quantity;

    private String modeOfPackaging;

    private String netWeight;

    private String grossWeight;

    private String count;


    public FormItemsDTO() {
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getGoodsDescription() {
        return goodsDescription;
    }

    public void setGoodsDescription(String goodsDescription) {
        this.goodsDescription = goodsDescription;
    }

    public String getStateGoods() {
        return stateGoods;
    }

    public void setStateGoods(String stateGoods) {
        this.stateGoods = stateGoods;
    }

    public String getSectoralPurpose() {
        return sectoralPurpose;
    }

    public void setSectoralPurpose(String sectoralPurpose) {
        this.sectoralPurpose = sectoralPurpose;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public long getFobvalue() {
        return fobvalue;
    }

    public void setFobvalue(long fobvalue) {
        this.fobvalue = fobvalue;
    }

    public long getFrieghtCharge() {
        return frieghtCharge;
    }

    public void setFrieghtCharge(long frieghtCharge) {
        this.frieghtCharge = frieghtCharge;
    }

    public String getFormNum() {
        return formNum;
    }

    public void setFormNum(String formNum) {
        this.formNum = formNum;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getModeOfPackaging() {
        return modeOfPackaging;
    }

    public void setModeOfPackaging(String modeOfPackaging) {
        this.modeOfPackaging = modeOfPackaging;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean isLike(PaarItemsDTO paarItemsDTO){

        if(this.getFobvalue() == paarItemsDTO.getFobvalue()){
            return true;
        }
        if(this.frieghtCharge == paarItemsDTO.getFrieghtCharge()){
            return  true;
        }
        if(this.quantity.equals(paarItemsDTO.getQuantity())){
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return "FormItemsDTO{" +
                "id=" + id +
                ", formNum='" + formNum + '\'' +
                ", hsCode='" + hsCode + '\'' +
                ", goodsDescription='" + goodsDescription + '\'' +
                ", stateGoods='" + stateGoods + '\'' +
                ", sectoralPurpose='" + sectoralPurpose + '\'' +
                ", unitMeasure='" + unitMeasure + '\'' +
                ", fobvalue=" + fobvalue +
                ", frieghtCharge=" + frieghtCharge +
                ", unitPrice='" + unitPrice + '\'' +
                ", quantity='" + quantity + '\'' +
                ", modeOfPackaging='" + modeOfPackaging + '\'' +
                ", netWeight='" + netWeight + '\'' +
                ", grossWeight='" + grossWeight + '\'' +
                ", count='" + count + '\'' +
                '}';
    }
}
