package cronjobservice.services;

import cronjobservice.dtos.AccountDTO;
import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.FormADTO;
import cronjobservice.dtos.FormHolder;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.FormA;
import cronjobservice.models.Remittance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by chiomarose on 21/09/2017.
 */
public interface FormACorporateService
{
//    String saveFormA(FormADTO formADTO) throws CronJobException;

    Boolean saveFormAFromFinacle(FormA formADTO) throws CronJobException;

//    String updateFormA(FormADTO formADTO) throws CronJobException;

    Boolean updateFormAFromFinacle(FormA tmsForm, FormA finForm) throws CronJobException;

    Boolean cancelFormAFromFinacle(FormA tmsForm, FormA finForm) throws CronJobException;

    Boolean updateFormAAllocation(FormA tmsForm, FormA finForm) throws CronJobException;

    Boolean updateFormAUtilization(FormA formA, Remittance remittance) throws CronJobException;

    FormADTO getFormA(Long id) throws CronJobException;
//    FormADTO getFormAForView(Long id) throws CronJobException;

    Page<FormADTO> getFormAs(Pageable pageDetails) throws CronJobException;

    Page<FormADTO> getAllFormAs(Pageable pageDetails);

    Page<FormADTO> getOpenFormAs(Pageable pageable) throws CronJobException;

    Page<FormADTO> getClosedFormAs(Pageable pageable);

//    Page<FormADTO> getOpenFormAsForBankUser(Pageable pageable) throws CronJobException;

    FormADTO convertEntityTODTO(FormA formA);

    FormADTO convertEntityTODTOFinacle(FormA formA);

//    FormHolder createFormHolder(FormADTO formADTO, String name) throws CronJobException;

    FormADTO editFormA(Long id) throws CronJobException;

    Iterable<FormADTO> getAllFormAs() throws CronJobException;
    String deleteForm(Long id) throws CronJobException;
    FormADTO getInitiatorDetails() throws CronJobException;

//    Page<FormADTO> getFormAsForBankUser(Pageable pageable) throws CronJobException;

    Boolean sendFormAITSmail(FormA formA);

    List<AccountDTO> getCustomerNairaAccounts() throws CronJobException;

    List<AccountDTO> getCustomerDomAccounts() throws CronJobException;

    AccountDTO getAccountByAccountNumber(String accountNumber);

    long getTotalNumberOpenFormA() throws CronJobException;

    long getTotalNumberFormA() throws CronJobException;

    List<CommentDTO> getComments(Long id) throws CronJobException;

    Boolean checkAvailableBalance(String amount, String accountId) throws CronJobException;

    String addAttachments(MultipartFile file, String formaNumber, String documentType);
}
