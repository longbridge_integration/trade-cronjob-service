package cronjobservice.jobs.lcjobs;


import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class UpdateExportLcPendingVerificationJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextLc = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextLc.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.UPDATE_EXPORT_LC_PENDING_VERIFICATION.toString(),cronJobService.getCurrentExpression(JobCategory.UPDATE_EXPORT_LC_PENDING_VERIFICATION.toString()));

            cronJobService.UpdateExportLcPendingResponse();
            cronJobService.updateRunningJob(JobCategory.UPDATE_EXPORT_LC_PENDING_VERIFICATION.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
