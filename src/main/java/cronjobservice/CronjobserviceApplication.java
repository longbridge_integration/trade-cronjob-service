package cronjobservice;

import cronjobservice.jobs.CronJobScheduler;
import cronjobservice.models.Code;
import cronjobservice.repositories.CodeRepo;
import cronjobservice.repositories.CustomJpaRepositoryFactoryBean;
import cronjobservice.services.IntegrationService;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableJpaRepositories(repositoryFactoryBeanClass = CustomJpaRepositoryFactoryBean.class)
@EnableBatchProcessing
@EnableAsync
public class CronjobserviceApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	IntegrationService integrationService;
	@Autowired
	CodeRepo codeRepo;
	public static void main(String[] args) {
		SpringApplication.run(CronjobserviceApplication.class, args);
	}


	@Override
	public void run(String... Strings) throws Exception
	{
		CronJobScheduler.startJobs();



	}
}


