package cronjobservice.repositories;

import cronjobservice.models.FormQ;
import cronjobservice.models.FormQAllocation;
import cronjobservice.models.FormaAllocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormQAllocationRepo extends JpaRepository<FormQAllocation, Long> {
    List<FormQAllocation> findByFormNumber(String formaNumber);
}
