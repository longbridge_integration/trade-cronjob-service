package cronjobservice.repositories;

import cronjobservice.models.BTA;
import cronjobservice.models.FormA;
import cronjobservice.models.PTA;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BtaRepo  extends  CommonRepo<BTA,Long>{

    List<BTA> findByStatusAndSubmitFlag(String S, String SF);


    BTA findByRefNumber(String refNumber);

}
