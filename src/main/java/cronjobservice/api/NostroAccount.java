package cronjobservice.api;

public class NostroAccount {

    private String nostroAccount;
    private String branchCode;
    private String bankCode;

    public String getNostroAccount() {
        return nostroAccount;
    }

    public void setNostroAccount(String nostroAccount) {
        this.nostroAccount = nostroAccount;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
}
