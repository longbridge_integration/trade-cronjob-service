package cronjobservice.repositories;

import cronjobservice.models.AccountRestriction;
import org.springframework.stereotype.Repository;

/**
 * Created by Fortune on 4/27/2017.
 */
@Repository
public interface AccountRestrictionRepo extends CommonRepo<AccountRestriction,Long> {

    AccountRestriction findByRestrictionValue(String accountNumber);
    AccountRestriction findByRestrictionTypeAndRestrictionValue(String restrictionType, String restrictionValue);
    AccountRestriction findFirstByRestrictionValueAndRestrictedForIgnoreCase(String accountNumber, String s);


}
