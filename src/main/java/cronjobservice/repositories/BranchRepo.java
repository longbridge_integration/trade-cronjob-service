package cronjobservice.repositories;

import cronjobservice.models.Branch;
import org.springframework.stereotype.Repository;

/**
 * Created by chiomarose on 07/09/2017.
 */
@Repository
public interface BranchRepo extends CommonRepo<Branch,Long>
{
    Branch findBySortCode(String sortCode);
    Branch findByName(String name);




}
