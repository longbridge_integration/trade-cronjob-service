package cronjobservice.dtos;


import cronjobservice.models.Account;
import cronjobservice.models.Code;
import cronjobservice.models.UserType;
import cronjobservice.models.WorkFlow;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class LCDTO {

    private Long id;
    private int version;
    private String lcNumber;
    private String tempLcNumber;
    private String bankCode;
    private String backTobackLcNo;
    private String bankIdent;

    //lc number entry by the customer
    private String lcNumberEntry;

    //validator to check for lc number error
    private Boolean lcNumberError;

    //To differentiate the lc with forex and the one without forex
    private String newnoforex;

    //form m allocation number
    private  String formMAllocationNumber;
    private BigDecimal formMAmount;
    private String formMutilizedAmount;
    private Boolean formnnumbercheck;
    private boolean usuanceCheck;
    private boolean tolerancenegcheck;
    private boolean exchangeratecheck;

    //pass the form M number to Finacle as formMNumber
    private long formMid;
    private String formMNumber;
    private String lcType;
    private String lcTypeName;
    private Boolean error;
    private String reInstateStatus;

    private Code typeOfLc;
    private String portOfLoading;
    private String placeOfFinalDest;
    private String portOfDischarge;
    private String shipmentPeriod;//freetext - cust
    private String shipmentTerms;//Code - cust
    private String shipmentMode;//Code - cust
    private String originOfGoods;//from FormM country dropDown

    //applicant Details
    private String applicantName;
    private String applicantAddress;
    private String applicantCity;
    private String applicantState;
    private String applicantCountry;
    //    private String corrNostroAcct;
    private Long formmAllocationId;

    private String accountToChargeId;
    private String accountToChargeNumber;
    private Account accountToCharge;
    private String accountForTransNumber;//account should be of same currency as that of form + Naira; display with currency
    private String accountForTransId;
    private Account accountForTrans;
    private BigDecimal lcAmount;
    private String lcCurrency;
    private BigDecimal partCleanline;
    private BigDecimal partConfirmed;
    private BigDecimal partUnconfirmed;
    private String exchangeRate;

    private String advBankName;
    private String advBankBranch;
    private String advBankAddress;
    private String advSwiftCode;
    private String advBankIdent;
    private String advBankCode;

    private String advThrBankName;
    private String advThrBankBranch;
    private String advThrBankAddress;
    private String advThrSwiftCode;
    private String advThrBankCode;



    //TEXT INFO
    private String bankInstructions;
    private String docInstruction;
    private String conditions;
    private String sendRecInfo;
    private String periodOfPresentation;//textInfo- bank
    private String chargeDescription;//textInfo- bank

    private Boolean transferable;
    private Boolean revolving;
    private Boolean revokable;
    private Boolean backToBack;
    private Boolean forex;
    private Boolean lcCurrencycheck;
    private Boolean partShipment;
    private Boolean redLetter;
    private Boolean standBy;
    private Boolean tranShipment;
    private Boolean deferred;
    private Boolean reimbursementMessage;
    private Boolean reimbursementAppRules;//dropdown NOTURR, URR Latest Version
    private String reimbursementAppRule;
    private Boolean utilbyBill;
    private Boolean utilWithoutBill;
    private boolean formDetRequired;
    private boolean select;
    private boolean lcAmountcheck;
    private boolean partCheck;
    private String expiryDate;
    private Date dateOfExpiry;
    private String countryOfExpiry;
    private String cityOfExpiry;
    private Date dateOfAllocation;
    //    private String dateOfAllocations;
    private String lcDateOfExpiry;
    private boolean toleranceposcheck;
    private boolean applicablesubrulecheck;
    private String offShoreRate;
    private String marginRate;
    private String valueDates;


    private String reimbursingBankName;
    private String reimbursingBankAcct;//bank
    private String reimbursingBankBranch;
    private String reimbursingBankAddress;
    private String reimbursingSwiftCode;
    private String reimbursingBankIdent;
    private String chargesBorneBy;//dropdown A-Applicant, B-Beneficiary

    private String availWithBankName;
    private String availWithBankBranch;
    private String availWithBankAddress;
    private String availWithSwiftCode;
    private String availWithBankIdent;
    private String availWithBankCode;

    private String insurCompName;
    private String insurCompAddr;
    private String insurPolicyNo;
    private String insurPolicyDates;
    private String insurPayableAt;
    private String insurDateOfExpirys;
    private String insurExpiryDate;
    private String insurPercentage;
    private BigDecimal insuredAmount;
    private BigDecimal premiumAmount;
    private String insuredBy;//dropdown A-Applicant, B-Beneficiary, O-Others

    private String lcPaymentTenorId;
    private Code lcPaymentTenor;
    private String usancePeriod;
    private String lcPaymentTenorCode;
    private String foreignBankRefNum;
    //private String beneficiaryBankDetails;
    private String beneficiaryName;
    private String beneficiaryAddress;
    private String beneficiaryCountry;
    private String beneficiaryTown;
    private String beneficiaryState;
    private String beneficiaryAccount;

    private String beneficiaryBankName;
    private String beneficiaryBankBranch;
    private String beneficiaryBankAddress;
    private String beneficiarySwiftCode;
    private String beneficiaryBankIdent;

    //private String correspondingBanks;
    private String lcValidCountry;
    private String beneficiaryCharges;
    private String negotiationPeriod;
    private String file;


    //New
    private String tranId;
    //private String solId;
    private String paysysId;
    private String applicableRules;//codes- bank
    private String applicableSubRule;//if "others" was chosen- bank
    private String tenorDetails;//textInfo - bank


    private String concession; //flg(Y/N)- bank
    private String corrNostroAccount;

    private String docCreditType;//dropdown - bank

    private BigDecimal allocatedAmount;
    private BigDecimal utilizedAmount;


    private String draweeBankName;
    private String draweeBankAddr;
    private String draweeBankSwift;
    private String draweeBankCtry;
    private String draweeBankState;
    private String draweeBankCity;
    private String draweeBankPoCode;
    private String draweeBankIdent;//code dropdown remittance
    private String draweeBankSwiftCode;
    private String draweeBankBr;
    private String draweeBank;
    private Boolean irrevokable;
    private List<LcDocumentDTO> documents;
    private String availableBy;//change to dropdown


    //1-100 integer
    private String tolerancePos;
    private String availableWith;
    private String toleranceNeg;

    private String confirmInstruct;//dropdown
    private String confirmBy;//dropdown
    private BigDecimal rate;
    private String bidRateCode;
    // private BigDecimal lcAvailableAmount;
    private BigDecimal outstanding;
    private String lastShipment;
    private String lastShipmentDates;
    private int numberOfAmendments;
    private int numberOfReinstatement;
    //    private String lcAccount;
    private String amendAction;
    private String confirmationType;
    private String cifid;
    private String date;
    private Date dateCreated;
    private long workFlowId;
    private WorkFlow workFlow;
    private String status;
    private String statusDesc;
    private String submitAction;
    private String initiatedBy;
    private String comments;

    private String userType;
    private boolean guidelineApproval;
    private String instruction;
    private String instructionBy;//dropdown A-Applicant, B-Beneficiary, C-Correspondent Bank, N-Collecting Bank, O-Others, S-Self
    private String instructionDate;
    private String instructionDates;
    private String negPeriodMonth;
    private String negeriodDay;
    //issueDate should be passed to finacle gotten from the comment on Approval.

    private String issuingBank;
    private String issuingBankBrnch;
    private String issuingBankSwift;
    private String issuingBankCity;
    private String issuingBankState;
    private String issuingBankCode;
    private String issuingBankAddr;
    private String issuingBankCountry;


    private String dealId;
    private BigDecimal utilizedFormMAmount;
    private String branch;

    private String applicantCityName;
    private String applicantCountryName;
    private String applicantStateName;
    private String beneficiaryStateName;
    private String beneficiaryTownName;
    private String beneficiaryCountryName;
    private String draweeBankIdentName;
    private String availableBankIdentName;
    private String reembursementBankIdentName;
    private String availableByName;
    private String lcCurrencyName;
    private String customerSegment;
    private String lcAmountAndCurrency;
    private BigDecimal formMAvailableAmount;
    private  String branchName;

    public boolean isExchangeratecheck() {
        return exchangeratecheck;
    }

    public void setExchangeratecheck(boolean exchangeratecheck) {
        this.exchangeratecheck = exchangeratecheck;
    }

    public String getOffShoreRate() {
        return offShoreRate;
    }

    public void setOffShoreRate(String offShoreRate) {
        this.offShoreRate = offShoreRate;
    }

    public String getMarginRate() {
        return marginRate;
    }

    public void setMarginRate(String marginRate) {
        this.marginRate = marginRate;
    }

    public String getValueDates() {
        return valueDates;
    }

    public void setValueDates(String valueDates) {
        this.valueDates = valueDates;
    }

    public boolean isApplicablesubrulecheck() {
        return applicablesubrulecheck;
    }

    public void setApplicablesubrulecheck(boolean applicablesubrulecheck) {
        this.applicablesubrulecheck = applicablesubrulecheck;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getLcAmountAndCurrency() {
        return lcAmountAndCurrency;
    }

    public void setLcAmountAndCurrency(String lcAmountAndCurrency) {
        this.lcAmountAndCurrency = lcAmountAndCurrency;
    }

    public String getReInstateStatus() {
        return reInstateStatus;
    }

    public void setReInstateStatus(String reInstateStatus) {
        this.reInstateStatus = reInstateStatus;
    }

    public String getCustomerSegment() {
        return customerSegment;
    }

    public void setCustomerSegment(String customerSegment) {
        this.customerSegment = customerSegment;
    }

    public boolean isTolerancenegcheck() {
        return tolerancenegcheck;
    }

    public void setTolerancenegcheck(boolean tolerancenegcheck) {
        this.tolerancenegcheck = tolerancenegcheck;
    }

    public boolean isUsuanceCheck() {
        return usuanceCheck;
    }

    public void setUsuanceCheck(boolean usuanceCheck) {
        this.usuanceCheck = usuanceCheck;
    }

    public String getLcPaymentTenorCode() {
        return lcPaymentTenorCode;
    }

    public void setLcPaymentTenorCode(String lcPaymentTenorCode) {
        this.lcPaymentTenorCode = lcPaymentTenorCode;
    }

    public boolean isToleranceposcheck() {
        return toleranceposcheck;
    }

    public void setToleranceposcheck(boolean toleranceposcheck) {
        this.toleranceposcheck = toleranceposcheck;
    }

    public String getLcCurrencyName() {
        return lcCurrencyName;
    }

    public void setLcCurrencyName(String lcCurrencyName) {
        this.lcCurrencyName = lcCurrencyName;
    }

    public String getAvailableByName() {
        return availableByName;
    }

    public void setAvailableByName(String availableByName) {
        this.availableByName = availableByName;
    }

    public String getReembursementBankIdentName() {
        return reembursementBankIdentName;
    }

    public void setReembursementBankIdentName(String reembursementBankIdentName) {
        this.reembursementBankIdentName = reembursementBankIdentName;
    }

    public String getAvailableBankIdentName() {
        return availableBankIdentName;
    }

    public void setAvailableBankIdentName(String availableBankIdentName) {
        this.availableBankIdentName = availableBankIdentName;
    }

    public String getDraweeBankIdentName() {
        return draweeBankIdentName;
    }

    public void setDraweeBankIdentName(String draweeBankIdentName) {
        this.draweeBankIdentName = draweeBankIdentName;
    }

    public String getBeneficiaryCountryName() {
        return beneficiaryCountryName;
    }

    public void setBeneficiaryCountryName(String beneficiaryCountryName) {
        this.beneficiaryCountryName = beneficiaryCountryName;
    }

    public String getBeneficiaryTownName() {
        return beneficiaryTownName;
    }

    public void setBeneficiaryTownName(String beneficiaryTownName) {
        this.beneficiaryTownName = beneficiaryTownName;
    }

    public String getBeneficiaryStateName() {
        return beneficiaryStateName;
    }

    public void setBeneficiaryStateName(String beneficiaryStateName) {
        this.beneficiaryStateName = beneficiaryStateName;
    }

    public String getApplicantStateName() {
        return applicantStateName;
    }

    public void setApplicantStateName(String applicantStateName) {
        this.applicantStateName = applicantStateName;
    }

    public String getApplicantCountryName() {
        return applicantCountryName;
    }

    public void setApplicantCountryName(String applicantCountryName) {
        this.applicantCountryName = applicantCountryName;
    }

    public Long getFormmAllocationId() {
        return formmAllocationId;
    }

    public String getApplicantCityName() {
        return applicantCityName;
    }

    public void setApplicantCityName(String applicantCityName) {
        this.applicantCityName = applicantCityName;
    }

    public void setFormmAllocationId(Long formmAllocationId) {
        this.formmAllocationId = formmAllocationId;
    }

    public String getLcDateOfExpiry() {
        return lcDateOfExpiry;
    }

    public void setLcDateOfExpiry(String lcDateOfExpiry) {
        this.lcDateOfExpiry = lcDateOfExpiry;
    }

    public Boolean getLcNumberError() {
        return lcNumberError;
    }

    public void setLcNumberError(Boolean lcNumberError) {
        this.lcNumberError = lcNumberError;
    }

    public String getLcNumberEntry() {
        return lcNumberEntry;
    }

    public void setLcNumberEntry(String lcNumberEntry) {
        this.lcNumberEntry = lcNumberEntry;
    }

//    public String getDateOfAllocations() {
//        return dateOfAllocations;
//    }
//
//    public void setDateOfAllocations(String dateOfAllocations) {
//        this.dateOfAllocations = dateOfAllocations;
//    }

    public String getReimbursementAppRule() {
        return reimbursementAppRule;
    }

    public void setReimbursementAppRule(String reimbursementAppRule) {
        this.reimbursementAppRule = reimbursementAppRule;
    }

    public String getInstructionDates() {
        return instructionDates;
    }

    public void setInstructionDates(String instructionDates) {
        this.instructionDates = instructionDates;
    }

    public Boolean getIrrevokable() {
        return irrevokable;
    }

    public void setIrrevokable(Boolean irrevokable) {
        this.irrevokable = irrevokable;
    }

    public String getNewnoforex() {
        return newnoforex;
    }

    public void setNewnoforex(String newnoforex) {
        this.newnoforex = newnoforex;
    }

    private String approver;

//    public String getCorrNostroAcct() {
//        return corrNostroAcct;
//    }
//
//    public void setCorrNostroAcct(String corrNostroAcct) {
//        this.corrNostroAcct = corrNostroAcct;
//    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public BigDecimal getUtilizedFormMAmount() {
        return utilizedFormMAmount;
    }

    public void setUtilizedFormMAmount(BigDecimal utilizedFormMAmount) {
        this.utilizedFormMAmount = utilizedFormMAmount;
    }

    public Boolean getFormnnumbercheck() {
        return formnnumbercheck;
    }

    public void setFormnnumbercheck(Boolean formnnumbercheck) {
        this.formnnumbercheck = formnnumbercheck;
    }

    public String getFormMAllocationNumber() {
        return formMAllocationNumber;
    }

    public void setFormMAllocationNumber(String formMAllocationNumber) {
        this.formMAllocationNumber = formMAllocationNumber;
    }

    public Date getDateOfAllocation() {
        return dateOfAllocation;
    }

    public void setDateOfAllocation(Date dateOfAllocation) {
        this.dateOfAllocation = dateOfAllocation;
    }

    public Boolean getLcCurrencycheck() {
        return lcCurrencycheck;
    }

    public void setLcCurrencycheck(Boolean lcCurrencycheck) {
        this.lcCurrencycheck = lcCurrencycheck;
    }

    public Boolean getForex() {
        return forex;
    }

    public void setForex(Boolean forex) {
        this.forex = forex;
    }

    public boolean isPartCheck() {
        return partCheck;
    }

    public void setPartCheck(boolean partCheck) {
        this.partCheck = partCheck;
    }

    public boolean isLcAmountcheck() {
        return lcAmountcheck;
    }

    public void setLcAmountcheck(boolean lcAmountcheck) {
        this.lcAmountcheck = lcAmountcheck;
    }

    public BigDecimal getFormMAmount() {
        return formMAmount;
    }

    public void setFormMAmount(BigDecimal formMAmount) {
        this.formMAmount = formMAmount;
    }

    public String getFormMutilizedAmount() {
        return formMutilizedAmount;
    }

    public void setFormMutilizedAmount(String formMutilizedAmount) {
        this.formMutilizedAmount = formMutilizedAmount;
    }

    public String getBankIdent() {
        return bankIdent;
    }

    public void setBankIdent(String bankIdent) {
        this.bankIdent = bankIdent;
    }

    public String getBackTobackLcNo() {
        return backTobackLcNo;
    }

//
//    public String getLcAccount() {
//        return lcAccount;
//    }
//
//    public void setLcAccount(String lcAccount) {
//        this.lcAccount = lcAccount;
//    }

    public void setBackTobackLcNo(String backTobackLcNo) {
        this.backTobackLcNo = backTobackLcNo;
    }

    public long getFormMid() {
        return formMid;
    }

    public void setFormMid(long formMid) {
        this.formMid = formMid;
    }

    public Boolean getStandBy() {
        return standBy;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public void setStandBy(Boolean standBy) {
        this.standBy = standBy;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }

    public String getAvailableWith() {
        return availableWith;
    }

    public void setAvailableWith(String availableWith) {
        this.availableWith = availableWith;
    }

    public String getIssuingBank() {
        return issuingBank;
    }

    public void setIssuingBank(String issuingBank) {
        this.issuingBank = issuingBank;
    }

    public String getIssuingBankBrnch() {
        return issuingBankBrnch;
    }

    public void setIssuingBankBrnch(String issuingBankBrnch) {
        this.issuingBankBrnch = issuingBankBrnch;
    }

    public String getIssuingBankSwift() {
        return issuingBankSwift;
    }

    public void setIssuingBankSwift(String issuingBankSwift) {
        this.issuingBankSwift = issuingBankSwift;
    }

    public String getIssuingBankCity() {
        return issuingBankCity;
    }

    public void setIssuingBankCity(String issuingBankCity) {
        this.issuingBankCity = issuingBankCity;
    }

    public String getIssuingBankState() {
        return issuingBankState;
    }

    public void setIssuingBankState(String issuingBankState) {
        this.issuingBankState = issuingBankState;
    }

    public String getIssuingBankCode() {
        return issuingBankCode;
    }

    public void setIssuingBankCode(String issuingBankCode) {
        this.issuingBankCode = issuingBankCode;
    }

    public String getIssuingBankAddr() {
        return issuingBankAddr;
    }

    public void setIssuingBankAddr(String issuingBankAddr) {
        this.issuingBankAddr = issuingBankAddr;
    }

    public String getIssuingBankCountry() {
        return issuingBankCountry;
    }

    public void setIssuingBankCountry(String issuingBankCountry) {
        this.issuingBankCountry = issuingBankCountry;
    }

    public String getDraweeBankPoCode() {
        return draweeBankPoCode;
    }

    public void setDraweeBankPoCode(String draweeBankPoCode) {
        this.draweeBankPoCode = draweeBankPoCode;
    }

    public String getDraweeBank() {
        return draweeBank;
    }

    public void setDraweeBank(String draweeBank) {
        this.draweeBank = draweeBank;
    }

    public String getDraweeBankSwiftCode() {
        return draweeBankSwiftCode;
    }

    public void setDraweeBankSwiftCode(String draweeBankSwiftCode) {
        this.draweeBankSwiftCode = draweeBankSwiftCode;
    }

    public String getAvailWithBankCode() {
        return availWithBankCode;
    }

    public String getDraweeBankBr() {
        return draweeBankBr;
    }

    public void setDraweeBankBr(String draweeBankBr) {
        this.draweeBankBr = draweeBankBr;
    }

    public void setAvailWithBankCode(String availWithBankCode) {
        this.availWithBankCode = availWithBankCode;
    }

    public String getTempLcNumber() {
        return tempLcNumber;
    }

    public void setTempLcNumber(String tempLcNumber) {
        this.tempLcNumber = tempLcNumber;
    }

    public Boolean getReimbursementMessage() {
        return reimbursementMessage;
    }

    public void setReimbursementMessage(Boolean reimbursementMessage) {
        this.reimbursementMessage = reimbursementMessage;
    }

    public Boolean getReimbursementAppRules() {
        return reimbursementAppRules;
    }

    public void setReimbursementAppRules(Boolean reimbursementAppRules) {
        this.reimbursementAppRules = reimbursementAppRules;
    }

    public String getReimbursingBankAcct() {
        return reimbursingBankAcct;
    }

    public void setReimbursingBankAcct(String reimbursingBankAcct) {
        this.reimbursingBankAcct = reimbursingBankAcct;
    }

    public String getReimbursingBankIdent() {
        return reimbursingBankIdent;
    }

    public void setReimbursingBankIdent(String reimbursingBankIdent) {
        this.reimbursingBankIdent = reimbursingBankIdent;
    }

    public String getChargesBorneBy() {
        return chargesBorneBy;
    }

    public void setChargesBorneBy(String chargesBorneBy) {
        this.chargesBorneBy = chargesBorneBy;
    }

    public String getAvailWithBankIdent() {
        return availWithBankIdent;
    }

    public void setAvailWithBankIdent(String availWithBankIdent) {
        this.availWithBankIdent = availWithBankIdent;
    }

    public String getForeignBankRefNum() {
        return foreignBankRefNum;
    }

    public void setForeignBankRefNum(String foreignBankRefNum) {
        this.foreignBankRefNum = foreignBankRefNum;
    }

    public String getApplicableSubRule() {
        return applicableSubRule;
    }

    public void setApplicableSubRule(String applicableSubRule) {
        this.applicableSubRule = applicableSubRule;
    }

    public String getTenorDetails() {
        return tenorDetails;
    }

    public void setTenorDetails(String tenorDetails) {
        this.tenorDetails = tenorDetails;
    }

    public String getConfirmBy() {
        return confirmBy;
    }

    public void setConfirmBy(String confirmBy) {
        this.confirmBy = confirmBy;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getInstructionBy() {
        return instructionBy;
    }

    public void setInstructionBy(String instructionBy) {
        this.instructionBy = instructionBy;
    }

    public String getInstructionDate() {
        return instructionDate;
    }

    public void setInstructionDate(String instructionDate) {
        this.instructionDate = instructionDate;
    }

    public String getNegPeriodMonth() {
        return negPeriodMonth;
    }

    public void setNegPeriodMonth(String negPeriodMonth) {
        this.negPeriodMonth = negPeriodMonth;
    }

    public String getNegeriodDay() {
        return negeriodDay;
    }

    public void setNegeriodDay(String negeriodDay) {
        this.negeriodDay = negeriodDay;
    }

    public String getAdvThrBankCode() {
        return advThrBankCode;
    }

    public void setAdvThrBankCode(String advThrBankCode) {
        this.advThrBankCode = advThrBankCode;
    }

    public String getAdvBankCode() {
        return advBankCode;
    }

    public void setAdvBankCode(String advBankCode) {
        this.advBankCode = advBankCode;
    }

    public Long getId() {
        return id;
    }

    public String getAdvBankIdent() {
        return advBankIdent;
    }

    public void setAdvBankIdent(String advBankIdent) {
        this.advBankIdent = advBankIdent;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getLcNumber() {
        return lcNumber;
    }

    public void setLcNumber(String lcNumber) {
        this.lcNumber = lcNumber;
    }

    public String getLcType() {
        return lcType;
    }

    public void setLcType(String lcType) {
        this.lcType = lcType;
    }

    public Code getTypeOfLc() {
        return typeOfLc;
    }

    public void setTypeOfLc(Code typeOfLc) {
        this.typeOfLc = typeOfLc;
    }

    public Account getAccountToCharge() {
        return accountToCharge;
    }

    public void setAccountToCharge(Account accountToCharge) {
        this.accountToCharge = accountToCharge;
    }

    public BigDecimal getLcAmount() {
        return lcAmount;
    }

    public void setLcAmount(BigDecimal lcAmount) {
        this.lcAmount = lcAmount;
    }

    public String getLcCurrency() {
        return lcCurrency;
    }

    public void setLcCurrency(String lcCurrency) {
        this.lcCurrency = lcCurrency;
    }

    public String getAdvBankName() {
        return advBankName;
    }

    public void setAdvBankName(String advBankName) {
        this.advBankName = advBankName;
    }

    public String getAdvBankBranch() {
        return advBankBranch;
    }

    public void setAdvBankBranch(String advBankBranch) {
        this.advBankBranch = advBankBranch;
    }

    public String getAdvBankAddress() {
        return advBankAddress;
    }

    public void setAdvBankAddress(String advBankAddress) {
        this.advBankAddress = advBankAddress;
    }

    public String getAdvSwiftCode() {
        return advSwiftCode;
    }

    public void setAdvSwiftCode(String advSwiftCode) {
        this.advSwiftCode = advSwiftCode;
    }

    public String getAdvThrBankName() {
        return advThrBankName;
    }

    public void setAdvThrBankName(String advThrBankName) {
        this.advThrBankName = advThrBankName;
    }

    public String getAdvThrBankBranch() {
        return advThrBankBranch;
    }

    public void setAdvThrBankBranch(String advThrBankBranch) {
        this.advThrBankBranch = advThrBankBranch;
    }

    public String getAdvThrBankAddress() {
        return advThrBankAddress;
    }

    public void setAdvThrBankAddress(String advThrBankAddress) {
        this.advThrBankAddress = advThrBankAddress;
    }

    public String getAdvThrSwiftCode() {
        return advThrSwiftCode;
    }

    public void setAdvThrSwiftCode(String advThrSwiftCode) {
        this.advThrSwiftCode = advThrSwiftCode;
    }

    public String getDocInstruction() {
        return docInstruction;
    }

    public void setDocInstruction(String docInstruction) {
        this.docInstruction = docInstruction;
    }

    public String getSendRecInfo() {
        return sendRecInfo;
    }

    public void setSendRecInfo(String sendRecInfo) {
        this.sendRecInfo = sendRecInfo;
    }

    public Boolean getTransferable() {
        return transferable;
    }

    public void setTransferable(Boolean transferable) {
        this.transferable = transferable;
    }

    public Boolean getRevolving() {
        return revolving;
    }

    public void setRevolving(Boolean revolving) {
        this.revolving = revolving;
    }

    public Boolean getRevokable() {
        return revokable;
    }

    public void setRevokable(Boolean revokable) {
        this.revokable = revokable;
    }

    public Boolean getBackToBack() {
        return backToBack;
    }

    public void setBackToBack(Boolean backToBack) {
        this.backToBack = backToBack;
    }

    public Boolean getPartShipment() {
        return partShipment;
    }

    public void setPartShipment(Boolean partShipment) {
        this.partShipment = partShipment;
    }

    public Boolean getRedLetter() {
        return redLetter;
    }

    public void setRedLetter(Boolean redLetter) {
        this.redLetter = redLetter;
    }

    public Boolean getTranShipment() {
        return tranShipment;
    }

    public void setTranShipment(Boolean tranShipment) {
        this.tranShipment = tranShipment;
    }

    public Boolean getDeferred() {
        return deferred;
    }

    public void setDeferred(Boolean deferred) {
        this.deferred = deferred;
    }

    public String getAvailWithBankName() {
        return availWithBankName;
    }

    public void setAvailWithBankName(String availWithBankName) {
        this.availWithBankName = availWithBankName;
    }

    public String getAvailWithBankBranch() {
        return availWithBankBranch;
    }

    public void setAvailWithBankBranch(String availWithBankBranch) {
        this.availWithBankBranch = availWithBankBranch;
    }

    public String getAvailWithBankAddress() {
        return availWithBankAddress;
    }

    public void setAvailWithBankAddress(String availWithBankAddress) {
        this.availWithBankAddress = availWithBankAddress;
    }

    public String getAvailWithSwiftCode() {
        return availWithSwiftCode;
    }

    public void setAvailWithSwiftCode(String availWithSwiftCode) {
        this.availWithSwiftCode = availWithSwiftCode;
    }

    public String getInsurCompName() {
        return insurCompName;
    }

    public void setInsurCompName(String insurCompName) {
        this.insurCompName = insurCompName;
    }

    public String getInsurCompAddr() {
        return insurCompAddr;
    }

    public void setInsurCompAddr(String insurCompAddr) {
        this.insurCompAddr = insurCompAddr;
    }

    public String getInsurPolicyNo() {
        return insurPolicyNo;
    }

    public void setInsurPolicyNo(String insurPolicyNo) {
        this.insurPolicyNo = insurPolicyNo;
    }

    public String getInsurPayableAt() {
        return insurPayableAt;
    }

    public void setInsurPayableAt(String insurPayableAt) {
        this.insurPayableAt = insurPayableAt;
    }

    public String getInsurPolicyDates() {
        return insurPolicyDates;
    }

    public void setInsurPolicyDates(String insurPolicyDates) {
        this.insurPolicyDates = insurPolicyDates;
    }

    public String getInsurDateOfExpirys() {
        return insurDateOfExpirys;
    }

    public void setInsurDateOfExpirys(String insurDateOfExpirys) {
        this.insurDateOfExpirys = insurDateOfExpirys;
    }

    public String getLastShipmentDates() {
        return lastShipmentDates;
    }

    public void setLastShipmentDates(String lastShipmentDates) {
        this.lastShipmentDates = lastShipmentDates;
    }





    public String getInsurPercentage() {
        return insurPercentage;
    }

    public void setInsurPercentage(String insurPercentage) {
        this.insurPercentage = insurPercentage;
    }

    public String getCorrNostroAccount() {
        return corrNostroAccount;
    }

    public void setCorrNostroAccount(String corrNostroAccount) {
        this.corrNostroAccount = corrNostroAccount;
    }

    public String getDocCreditType() {
        return docCreditType;
    }

    public void setDocCreditType(String docCreditType) {
        this.docCreditType = docCreditType;
    }

    public boolean isFormDetRequired() {
        return formDetRequired;
    }

    public void setFormDetRequired(boolean formDetRequired) {
        this.formDetRequired = formDetRequired;
    }

    public String getDraweeBankSwift() {
        return draweeBankSwift;
    }

    public void setDraweeBankSwift(String draweeBankSwift) {
        this.draweeBankSwift = draweeBankSwift;
    }



    public String getDraweeBankAddr() {
        return draweeBankAddr;
    }

    public void setDraweeBankAddr(String draweeBankAddr) {
        this.draweeBankAddr = draweeBankAddr;
    }

    public String getDraweeBankCtry() {
        return draweeBankCtry;
    }

    public void setDraweeBankCtry(String draweeBankCtry) {
        this.draweeBankCtry = draweeBankCtry;
    }

    public String getDraweeBankState() {
        return draweeBankState;
    }

    public void setDraweeBankState(String draweeBankState) {
        this.draweeBankState = draweeBankState;
    }

    public String getDraweeBankCity() {
        return draweeBankCity;
    }

    public void setDraweeBankCity(String draweeBankCity) {
        this.draweeBankCity = draweeBankCity;
    }

    public String getDraweeBankIdent() {
        return draweeBankIdent;
    }

    public void setDraweeBankIdent(String draweeBankIdent) {
        this.draweeBankIdent = draweeBankIdent;
    }

    public String getConfirmInstruct() {
        return confirmInstruct;
    }

    public void setConfirmInstruct(String confirmInstruct) {
        this.confirmInstruct = confirmInstruct;
    }

    public Boolean getUtilbyBill() {
        return utilbyBill;
    }

    public void setUtilbyBill(Boolean utilbyBill) {
        this.utilbyBill = utilbyBill;
    }

    public Boolean getUtilWithoutBill() {
        return utilWithoutBill;
    }

    public void setUtilWithoutBill(Boolean utilWithoutBill) {
        this.utilWithoutBill = utilWithoutBill;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }



    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getDateOfExpiry() {
        return dateOfExpiry;
    }

    public void setDateOfExpiry(Date dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }

    public String getCountryOfExpiry() {
        return countryOfExpiry;
    }

    public void setCountryOfExpiry(String countryOfExpiry) {
        this.countryOfExpiry = countryOfExpiry;
    }

    public String getCityOfExpiry() {
        return cityOfExpiry;
    }

    public void setCityOfExpiry(String cityOfExpiry) {
        this.cityOfExpiry = cityOfExpiry;
    }

    public String getLcPaymentTenorId() {
        return lcPaymentTenorId;
    }

    public void setLcPaymentTenorId(String lcPaymentTenorId) {
        this.lcPaymentTenorId = lcPaymentTenorId;
    }

    public Code getLcPaymentTenor() {
        return lcPaymentTenor;
    }

    public void setLcPaymentTenor(Code lcPaymentTenor) {
        this.lcPaymentTenor = lcPaymentTenor;
    }

    public String getUsancePeriod() {
        return usancePeriod;
    }

    public void setUsancePeriod(String usancePeriod) {
        this.usancePeriod = usancePeriod;
    }

    public String getLcValidCountry() {
        return lcValidCountry;
    }

    public void setLcValidCountry(String lcValidCountry) {
        this.lcValidCountry = lcValidCountry;
    }

    public String getBeneficiaryCharges() {
        return beneficiaryCharges;
    }

    public void setBeneficiaryCharges(String beneficiaryCharges) {
        this.beneficiaryCharges = beneficiaryCharges;
    }

    public String getNegotiationPeriod() {
        return negotiationPeriod;
    }

    public void setNegotiationPeriod(String negotiationPeriod) {
        this.negotiationPeriod = negotiationPeriod;
    }

    public String getPortOfLoading() {
        return portOfLoading;
    }

    public void setPortOfLoading(String portOfLoading) {
        this.portOfLoading = portOfLoading;
    }


    public String getShipmentTerms() {
        return shipmentTerms;
    }

    public void setShipmentTerms(String shipmentTerms) {
        this.shipmentTerms = shipmentTerms;
    }

    public String getOriginOfGoods() {
        return originOfGoods;
    }

    public void setOriginOfGoods(String originOfGoods) {
        this.originOfGoods = originOfGoods;
    }

    public List<LcDocumentDTO> getDocuments() {
        return documents;
    }

    public void setDocuments(List<LcDocumentDTO> documents) {
        this.documents = documents;
    }

    public String getAvailableBy() {
        return availableBy;
    }

    public void setAvailableBy(String availableBy) {
        this.availableBy = availableBy;
    }

    public String getTolerancePos() {
        return tolerancePos;
    }

    public void setTolerancePos(String tolerancePos) {
        this.tolerancePos = tolerancePos;
    }

    public String getToleranceNeg() {
        return toleranceNeg;
    }

    public void setToleranceNeg(String toleranceNeg) {
        this.toleranceNeg = toleranceNeg;
    }

    public String getLcTypeName() {
        return lcTypeName;
    }

    public void setLcTypeName(String lcTypeName) {
        this.lcTypeName = lcTypeName;
    }

    public String getAccountToChargeNumber() {
        return accountToChargeNumber;
    }

    public void setAccountToChargeNumber(String accountToChargeNumber) {
        this.accountToChargeNumber = accountToChargeNumber;
    }

    public String getAccountForTransNumber() {
        return accountForTransNumber;
    }

    public void setAccountForTransNumber(String accountForTransNumber) {
        this.accountForTransNumber = accountForTransNumber;
    }

//    public BigDecimal getRate() {
//        return rate;
//    }
//
//    public void setRate(BigDecimal rate) {
//        this.rate = rate;
//    }


    public BigDecimal getFormMAvailableAmount() {
        return formMAvailableAmount;
    }

    public void setFormMAvailableAmount(BigDecimal formMAvailableAmount) {
        this.formMAvailableAmount = formMAvailableAmount;
    }

    public BigDecimal getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(BigDecimal outstanding) {
        this.outstanding = outstanding;
    }

    public String getLastShipment() {
        return lastShipment;
    }

    public void setLastShipment(String lastShipment) {
        this.lastShipment = lastShipment;
    }




    public int getNumberOfAmendments() {
        return numberOfAmendments;
    }

    public void setNumberOfAmendments(int numberOfAmendments) {
        this.numberOfAmendments = numberOfAmendments;
    }

    public int getNumberOfReinstatement() {
        return numberOfReinstatement;
    }

    public void setNumberOfReinstatement(int numberOfReinstatement) {
        this.numberOfReinstatement = numberOfReinstatement;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getWorkFlowId() {
        return workFlowId;
    }

    public void setWorkFlowId(long workFlowId) {
        this.workFlowId = workFlowId;
    }

    public WorkFlow getWorkFlow() {
        return workFlow;
    }

    public void setWorkFlow(WorkFlow workFlow) {
        this.workFlow = workFlow;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getSubmitAction() {
        return submitAction;
    }

    public void setSubmitAction(String submitAction) {
        this.submitAction = submitAction;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

//    public String getFormMid() {
//        return formMid;
//    }
//
//    public void setFormMid(String formMid) {
//        this.formMid = formMid;
//    }

    public String getFormMNumber() {
        return formMNumber;
    }

    public void setFormMNumber(String formMNumber) {
        this.formMNumber = formMNumber;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public String getAccountToChargeId() {
        return accountToChargeId;
    }

    public void setAccountToChargeId(String accountToChargeId) {
        this.accountToChargeId = accountToChargeId;
    }

    public String getAccountForTransId() {
        return accountForTransId;
    }

    public void setAccountForTransId(String accountForTransId) {
        this.accountForTransId = accountForTransId;
    }

    public Account getAccountForTrans() {
        return accountForTrans;
    }

    public void setAccountForTrans(Account accountForTrans) {
        this.accountForTrans = accountForTrans;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryAddress() {
        return beneficiaryAddress;
    }

    public void setBeneficiaryAddress(String beneficiaryAddress) {
        this.beneficiaryAddress = beneficiaryAddress;
    }

    public BigDecimal getPartCleanline() {
        return partCleanline;
    }

    public void setPartCleanline(BigDecimal partCleanline) {
        this.partCleanline = partCleanline;
    }

    public BigDecimal getPartConfirmed() {
        return partConfirmed;
    }

    public void setPartConfirmed(BigDecimal partConfirmed) {
        this.partConfirmed = partConfirmed;
    }

    public BigDecimal getPartUnconfirmed() {
        return partUnconfirmed;
    }

    public void setPartUnconfirmed(BigDecimal partUnconfirmed) {
        this.partUnconfirmed = partUnconfirmed;
    }

    public String getReimbursingBankName() {
        return reimbursingBankName;
    }

    public void setReimbursingBankName(String reimbursingBankName) {
        this.reimbursingBankName = reimbursingBankName;
    }

    public String getReimbursingBankBranch() {
        return reimbursingBankBranch;
    }

    public void setReimbursingBankBranch(String reimbursingBankBranch) {
        this.reimbursingBankBranch = reimbursingBankBranch;
    }

    public String getReimbursingBankAddress() {
        return reimbursingBankAddress;
    }

    public void setReimbursingBankAddress(String reimbursingBankAddress) {
        this.reimbursingBankAddress = reimbursingBankAddress;
    }

    public String getReimbursingSwiftCode() {
        return reimbursingSwiftCode;
    }

    public void setReimbursingSwiftCode(String reimbursingSwiftCode) {
        this.reimbursingSwiftCode = reimbursingSwiftCode;
    }

    public BigDecimal getInsuredAmount() {
        return insuredAmount;
    }

    public void setInsuredAmount(BigDecimal insuredAmount) {
        this.insuredAmount = insuredAmount;
    }

    public BigDecimal getPremiumAmount() {
        return premiumAmount;
    }

    public void setPremiumAmount(BigDecimal premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    public String getInsuredBy() {
        return insuredBy;
    }

    public void setInsuredBy(String insuredBy) {
        this.insuredBy = insuredBy;
    }

    public String getAmendAction() {
        return amendAction;
    }

    public void setAmendAction(String amendAction) {
        this.amendAction = amendAction;
    }

    public String getConfirmationType() {
        return confirmationType;
    }

    public void setConfirmationType(String confirmationType) {
        this.confirmationType = confirmationType;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getPaysysId() {
        return paysysId;
    }

    public void setPaysysId(String paysysId) {
        this.paysysId = paysysId;
    }

    public String getApplicableRules() {
        return applicableRules;
    }

    public void setApplicableRules(String applicableRules) {
        this.applicableRules = applicableRules;
    }

    public String getPeriodOfPresentation() {
        return periodOfPresentation;
    }

    public void setPeriodOfPresentation(String periodOfPresentation) {
        this.periodOfPresentation = periodOfPresentation;
    }

    public String getChargeDescription() {
        return chargeDescription;
    }

    public void setChargeDescription(String chargeDescription) {
        this.chargeDescription = chargeDescription;
    }

    public String getConcession() {
        return concession;
    }

    public void setConcession(String concession) {
        this.concession = concession;
    }

    public String getApplicantAddress() {
        return applicantAddress;
    }

    public void setApplicantAddress(String applicantAddress) {
        this.applicantAddress = applicantAddress;
    }

    public String getApplicantCity() {
        return applicantCity;
    }

    public void setApplicantCity(String applicantCity) {
        this.applicantCity = applicantCity;
    }

    public String getApplicantState() {
        return applicantState;
    }

    public void setApplicantState(String applicantState) {
        this.applicantState = applicantState;
    }

    public String getApplicantCountry() {
        return applicantCountry;
    }

    public void setApplicantCountry(String applicantCountry) {
        this.applicantCountry = applicantCountry;
    }

    public BigDecimal getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(BigDecimal allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public BigDecimal getUtilizedAmount() {
        return utilizedAmount;
    }

    public void setUtilizedAmount(BigDecimal utilizedAmount) {
        this.utilizedAmount = utilizedAmount;
    }

    public String getDraweeBankName() {
        return draweeBankName;
    }

    public void setDraweeBankName(String draweeBankName) {
        this.draweeBankName = draweeBankName;
    }

    public String getInsurExpiryDate() {
        return insurExpiryDate;
    }

    public void setInsurExpiryDate(String insurExpiryDate) {
        this.insurExpiryDate = insurExpiryDate;
    }

    public String getBankInstructions() {
        return bankInstructions;
    }

    public void setBankInstructions(String bankInstructions) {
        this.bankInstructions = bankInstructions;
    }

    public String getPlaceOfFinalDest() {
        return placeOfFinalDest;
    }

    public void setPlaceOfFinalDest(String placeOfFinalDest) {
        this.placeOfFinalDest = placeOfFinalDest;
    }

    public String getShipmentPeriod() {
        return shipmentPeriod;
    }

    public void setShipmentPeriod(String shipmentPeriod) {
        this.shipmentPeriod = shipmentPeriod;
    }

    public String getShipmentMode() {
        return shipmentMode;
    }

    public void setShipmentMode(String shipmentMode) {
        this.shipmentMode = shipmentMode;
    }

    public String getBeneficiaryBankName() {
        return beneficiaryBankName;
    }

    public void setBeneficiaryBankName(String beneficiaryBankName) {
        this.beneficiaryBankName = beneficiaryBankName;
    }

    public String getBeneficiaryBankBranch() {
        return beneficiaryBankBranch;
    }

    public void setBeneficiaryBankBranch(String beneficiaryBankBranch) {
        this.beneficiaryBankBranch = beneficiaryBankBranch;
    }

    public String getBeneficiaryBankAddress() {
        return beneficiaryBankAddress;
    }

    public void setBeneficiaryBankAddress(String beneficiaryBankAddress) {
        this.beneficiaryBankAddress = beneficiaryBankAddress;
    }

    public String getBeneficiarySwiftCode() {
        return beneficiarySwiftCode;
    }

    public void setBeneficiarySwiftCode(String beneficiarySwiftCode) {
        this.beneficiarySwiftCode = beneficiarySwiftCode;
    }

    public String getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    public void setBeneficiaryCountry(String beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
    }

    public String getBeneficiaryTown() {
        return beneficiaryTown;
    }

    public void setBeneficiaryTown(String beneficiaryTown) {
        this.beneficiaryTown = beneficiaryTown;
    }

    public String getBeneficiaryState() {
        return beneficiaryState;
    }

    public void setBeneficiaryState(String beneficiaryState) {
        this.beneficiaryState = beneficiaryState;
    }

    public String getBeneficiaryAccount() {
        return beneficiaryAccount;
    }

    public void setBeneficiaryAccount(String beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
    }

    public String getBidRateCode() {
        return bidRateCode;
    }

    public void setBidRateCode(String bidRateCode) {
        this.bidRateCode = bidRateCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }



    public String getBeneficiaryBankIdent() {
        return beneficiaryBankIdent;
    }

    public void setBeneficiaryBankIdent(String beneficiaryBankIdent) {
        this.beneficiaryBankIdent = beneficiaryBankIdent;
    }
}
