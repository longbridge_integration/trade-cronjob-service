package cronjobservice.dtos;//package longbridge.dtos;

import cronjobservice.models.WorkFlow;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by SYLVESTER on 9/7/2017.
 */

public class FormMDTO {


    private Long id;
    private String submitAction;
    private String amendAction;
    private boolean forex;
//    private String prefex;
    private String GenDesc;
    private String netWeight;
    private String grossWeight;
    private int quantityOfItems;
    private String airTicketNum;
    private String route;
    private String airline;
    private String benePhone;
    private String beneEmail;
    private String beneOrderBy;
    private String appName;
    private String appEmail;
    private String appAddress1;
    private String appAddress2;
    private String city;
    private String appState;
    private String appCountry;

    private String appPhone;
    private String appTIN;
    private String appRCNo;
    private String appFax;
    private String appNEPC;
    private String totalFB;
    private String totalFreight;
    private String ancillaryCharge;
    private String proformaDate;
    private String TOD;
    private String insurValue;
    private String totalCF;
    private String currencyCode;
    private String exchangeRate;
    private String sourceFunds;
    private String paymentMode;
    private String transferMode;
    private String proformaNo;
    private String paymentDate;
    private String ModeOfTransport;
    private String portOfDischarge;
    private String portOfLoading;
    private String countryOfOrigin;
    private String countryOfSupply;
    private String customOffice;
    private String beneName;
    private String beneAddress1;
    private String beneAddress2;
    private String beneStateCode;
    private String beneCountry;
    private String beneFax;
    private String appEndorsementRep;
    private String appEndorsementDeaker;
    private Date date1;
    private Date date2;
    private String attachments;
    private String status;
    private String beneCity;
    private String beneState;
    private Long workFlowId;
    private int version;
    private String cifid;
    private Date expiry;
    private String allocatedAmount;
    private String initiatedBy;
    private Date dateCreated;
    private String formNumber;
    private String utilization;
    private String statusDesc;
    private String convert;

    private String comments;

    private WorkFlow workFlow;

    private BigDecimal formAmount;

    //Insurance details
    private String insuranceCompanyName;
    private String insuranceCompanyAddress;
    private String insurancePolicyNo;
    private String insurancePolicyDate;
    private String insurancePayableAt;
    private String insuranceDateOfExpiry;
    private String insurancePercentage;
    private BigDecimal insuredAmount;
    private BigDecimal premiumAmount;
    private String insuredBy;
    private String insuranceRate;
    private String submitFlag;
    private String keepPrefix;
    private BigDecimal avaliableAmount;


    public String getKeepPrefix() {
        return keepPrefix;
    }

    public void setKeepPrefix(String keepPrefix) {
        this.keepPrefix = keepPrefix;
    }

    private List<FormMAttachmentDTO> formMAttachmentDTOS;


    List<ItemsDTO> items;
//    @ManyToOne
//    @JsonIgnore
//    private CorporateUser corporateUser;

    private String designatedBank;
    private String inspectionAgent;

    private String tempFormNumber;//trade app no
    private String branch;

    private String allocationAccount;
    private String allocationCurrency;
    private BigDecimal allocationAmount;
    //    private String exchangeRate;
    private Date dateOfAllocation;
    private String dealId;


    //    /***/linking form m
    private String chargeAccount;
    private String transactionAccount;
    private String purposeOfForm;
    private String prefex;
    private String linkingStatus;

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public String getTransactionAccount() {
        return transactionAccount;
    }

    public void setTransactionAccount(String transactionAccount) {
        this.transactionAccount = transactionAccount;
    }

    public String getPurposeOfForm() {
        return purposeOfForm;
    }

    public void setPurposeOfForm(String purposeOfForm) {
        this.purposeOfForm = purposeOfForm;
    }

    public String getLinkingStatus() {
        return linkingStatus;
    }

    public void setLinkingStatus(String linkingStatus) {
        this.linkingStatus = linkingStatus;
    }

    public BigDecimal getAvaliableAmount() {
        return avaliableAmount;
    }

    public void setAvaliableAmount(BigDecimal avaliableAmount) {
        this.avaliableAmount = avaliableAmount;
    }

    public String getTempFormNumber() {
        return tempFormNumber;
    }

    public void setTempFormNumber(String tempFormNumber) {
        this.tempFormNumber = tempFormNumber;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public List<FormMAttachmentDTO> getFormMAttachmentDTOS() {
        return formMAttachmentDTOS;
    }

    public void setFormMAttachmentDTOS(List<FormMAttachmentDTO> formMAttachmentDTOS) {
        this.formMAttachmentDTOS = formMAttachmentDTOS;
    }

    public String getBeneCountry() {
        return beneCountry;
    }

    public void setBeneCountry(String beneCountry) {
        this.beneCountry = beneCountry;
    }

    public String getAmendAction() {
        return amendAction;
    }

    public void setAmendAction(String amendAction) {
        this.amendAction = amendAction;
    }

    public String getAppCountry() {
        return appCountry;
    }

    public void setAppCountry(String appCountry) {
        this.appCountry = appCountry;
    }

    public String getInsuranceRate() {
        return insuranceRate;
    }

    public void setInsuranceRate(String insuranceRate) {
        this.insuranceRate = insuranceRate;
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    public String getInsuranceCompanyAddress() {
        return insuranceCompanyAddress;
    }

    public void setInsuranceCompanyAddress(String insuranceCompanyAddress) {
        this.insuranceCompanyAddress = insuranceCompanyAddress;
    }

    public String getInsurancePolicyNo() {
        return insurancePolicyNo;
    }

    public void setInsurancePolicyNo(String insurancePolicyNo) {
        this.insurancePolicyNo = insurancePolicyNo;
    }


    public String getInsurancePayableAt() {
        return insurancePayableAt;
    }

    public void setInsurancePayableAt(String insurancePayableAt) {
        this.insurancePayableAt = insurancePayableAt;
    }

    public String getInsurancePolicyDate() {
        return insurancePolicyDate;
    }

    public void setInsurancePolicyDate(String insurancePolicyDate) {
        this.insurancePolicyDate = insurancePolicyDate;
    }

    public String getInsuranceDateOfExpiry() {
        return insuranceDateOfExpiry;
    }

    public void setInsuranceDateOfExpiry(String insuranceDateOfExpiry) {
        this.insuranceDateOfExpiry = insuranceDateOfExpiry;
    }

    public String getInsurancePercentage() {
        return insurancePercentage;
    }

    public void setInsurancePercentage(String insurancePercentage) {
        this.insurancePercentage = insurancePercentage;
    }

    public BigDecimal getInsuredAmount() {
        return insuredAmount;
    }

    public void setInsuredAmount(BigDecimal insuredAmount) {
        this.insuredAmount = insuredAmount;
    }

    public BigDecimal getPremiumAmount() {
        return premiumAmount;
    }

    public void setPremiumAmount(BigDecimal premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    public String getInsuredBy() {
        return insuredBy;
    }

    public void setInsuredBy(String insuredBy) {
        this.insuredBy = insuredBy;
    }

    public String getAppState() {
        return appState;
    }

    public void setAppState(String appState) {
        this.appState = appState;
    }

    public BigDecimal getFormAmount() {
        return formAmount;
    }

    public void setFormAmount(BigDecimal formAmount) {
        this.formAmount = formAmount;
    }

    public String getBeneEmail() {
        return beneEmail;
    }

    public void setBeneEmail(String beneEmail) {
        this.beneEmail = beneEmail;
    }

    public List<ItemsDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemsDTO> items) {
        this.items = items;
    }

    public String getConvert() {
        return convert;
    }

    public void setConvert(String convert) {
        this.convert = convert;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatusDesc() {
        return statusDesc;
    }


    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getUtilization() {
        return utilization;
    }

    public void setUtilization(String utilization) {
        this.utilization = utilization;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public String getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(String allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public Long getWorkFlowId() {
        return workFlowId;
    }

    public void setWorkFlowId(Long workFlowId) {
        this.workFlowId = workFlowId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isForex() {
        return forex;
    }

    public void setForex(boolean forex) {
        this.forex = forex;
    }

    public String getGenDesc() {
        return GenDesc;
    }

    public void setGenDesc(String genDesc) {
        GenDesc = genDesc;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }

    public int getQuantityOfItems() {
        return quantityOfItems;
    }

    public void setQuantityOfItems(int quantityOfItems) {
        this.quantityOfItems = quantityOfItems;
    }

    public String getAirTicketNum() {
        return airTicketNum;
    }

    public void setAirTicketNum(String airTicketNum) {
        this.airTicketNum = airTicketNum;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getBenePhone() {
        return benePhone;
    }

    public void setBenePhone(String benePhone) {
        this.benePhone = benePhone;
    }

    public String getBeneOrderBy() {
        return beneOrderBy;
    }

    public void setBeneOrderBy(String beneOrderBy) {
        this.beneOrderBy = beneOrderBy;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppEmail() {
        return appEmail;
    }

    public void setAppEmail(String appEmail) {
        this.appEmail = appEmail;
    }

    public String getAppAddress1() {
        return appAddress1;
    }

    public void setAppAddress1(String appAddress1) {
        this.appAddress1 = appAddress1;
    }

    public String getAppAddress2() {
        return appAddress2;
    }

    public void setAppAddress2(String appAddress2) {
        this.appAddress2 = appAddress2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }



    public String getAppPhone() {
        return appPhone;
    }

    public void setAppPhone(String appPhone) {
        this.appPhone = appPhone;
    }

    public String getAppTIN() {
        return appTIN;
    }

    public void setAppTIN(String appTIN) {
        this.appTIN = appTIN;
    }

    public String getAppRCNo() {
        return appRCNo;
    }

    public void setAppRCNo(String appRCNo) {
        this.appRCNo = appRCNo;
    }

    public String getAppFax() {
        return appFax;
    }

    public void setAppFax(String appFax) {
        this.appFax = appFax;
    }

    public String getAppNEPC() {
        return appNEPC;
    }

    public void setAppNEPC(String appNEPC) {
        this.appNEPC = appNEPC;
    }

    public String getTotalFB() {
        return totalFB;
    }

    public void setTotalFB(String totalFB) {
        this.totalFB = totalFB;
    }

    public String getTotalFreight() {
        return totalFreight;
    }

    public void setTotalFreight(String totalFreight) {
        this.totalFreight = totalFreight;
    }

    public String getAncillaryCharge() {
        return ancillaryCharge;
    }

    public void setAncillaryCharge(String ancillaryCharge) {
        this.ancillaryCharge = ancillaryCharge;
    }

    public String getProformaDate() {
        return proformaDate;
    }

    public void setProformaDate(String proformaDate) {
        this.proformaDate = proformaDate;
    }

    public String getTOD() {
        return TOD;
    }

    public void setTOD(String TOD) {
        this.TOD = TOD;
    }

    public String getInsurValue() {
        return insurValue;
    }

    public void setInsurValue(String insurValue) {
        this.insurValue = insurValue;
    }

    public String getTotalCF() {
        return totalCF;
    }

    public void setTotalCF(String totalCF) {
        this.totalCF = totalCF;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getSourceFunds() {
        return sourceFunds;
    }

    public void setSourceFunds(String sourceFunds) {
        this.sourceFunds = sourceFunds;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getTransferMode() {
        return transferMode;
    }

    public void setTransferMode(String transferMode) {
        this.transferMode = transferMode;
    }

    public String getProformaNo() {
        return proformaNo;
    }

    public void setProformaNo(String proformaNo) {
        this.proformaNo = proformaNo;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getModeOfTransport() {
        return ModeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        ModeOfTransport = modeOfTransport;
    }

    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }

    public String getPortOfLoading() {
        return portOfLoading;
    }

    public void setPortOfLoading(String portOfLoading) {
        this.portOfLoading = portOfLoading;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public String getCountryOfSupply() {
        return countryOfSupply;
    }

    public void setCountryOfSupply(String countryOfSupply) {
        this.countryOfSupply = countryOfSupply;
    }

    public String getCustomOffice() {
        return customOffice;
    }

    public void setCustomOffice(String customOffice) {
        this.customOffice = customOffice;
    }

    public String getBeneName() {
        return beneName;
    }

    public void setBeneName(String beneName) {
        this.beneName = beneName;
    }

    public String getBeneAddress1() {
        return beneAddress1;
    }

    public void setBeneAddress1(String beneAddress1) {
        this.beneAddress1 = beneAddress1;
    }

    public String getBeneAddress2() {
        return beneAddress2;
    }

    public void setBeneAddress2(String beneAddress2) {
        this.beneAddress2 = beneAddress2;
    }

    public String getBeneStateCode() {
        return beneStateCode;
    }

    public void setBeneStateCode(String beneStateCode) {
        this.beneStateCode = beneStateCode;
    }

    public String getBeneFax() {
        return beneFax;
    }

    public void setBeneFax(String beneFax) {
        this.beneFax = beneFax;
    }

    public String getAppEndorsementRep() {
        return appEndorsementRep;
    }

    public void setAppEndorsementRep(String appEndorsementRep) {
        this.appEndorsementRep = appEndorsementRep;
    }

    public String getAppEndorsementDeaker() {
        return appEndorsementDeaker;
    }

    public void setAppEndorsementDeaker(String appEndorsementDeaker) {
        this.appEndorsementDeaker = appEndorsementDeaker;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getDesignatedBank() {
        return designatedBank;
    }

    public void setDesignatedBank(String designatedBank) {
        this.designatedBank = designatedBank;
    }

    public String getInspectionAgent() {
        return inspectionAgent;
    }

    public void setInspectionAgent(String inspectionAgent) {
        this.inspectionAgent = inspectionAgent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrefex() {
        return prefex;
    }

    public void setPrefex(String prefex) {
        this.prefex = prefex;
    }

    public String getBeneCity() {
        return beneCity;
    }

    public void setBeneCity(String beneCity) {
        this.beneCity = beneCity;
    }

    public String getBeneState() {
        return beneState;
    }

    public void setBeneState(String beneState) {
        this.beneState = beneState;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public WorkFlow getWorkFlow() {
        return workFlow;
    }

    public void setWorkFlow(WorkFlow workFlow) {
        this.workFlow = workFlow;
    }

    public String getSubmitAction() {
        return submitAction;
    }

    public void setSubmitAction(String submitAction) {
        this.submitAction = submitAction;
    }

    public String getSubmitFlag() {
        return submitFlag;
    }

    public void setSubmitFlag(String submitFlag) {
        this.submitFlag = submitFlag;
    }

    public String getAllocationAccount() {
        return allocationAccount;
    }

    public void setAllocationAccount(String allocationAccount) {
        this.allocationAccount = allocationAccount;
    }

    public String getAllocationCurrency() {
        return allocationCurrency;
    }

    public void setAllocationCurrency(String allocationCurrency) {
        this.allocationCurrency = allocationCurrency;
    }

    public BigDecimal getAllocationAmount() {
        return allocationAmount;
    }

    public void setAllocationAmount(BigDecimal allocationAmount) {
        this.allocationAmount = allocationAmount;
    }

    public Date getDateOfAllocation() {
        return dateOfAllocation;
    }

    public void setDateOfAllocation(Date dateOfAllocation) {
        this.dateOfAllocation = dateOfAllocation;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    @Override
    public String toString() {
        return "FormMDTO{" +
                "id=" + id +
                ", submitAction='" + submitAction + '\'' +
                ", amendAction='" + amendAction + '\'' +
                ", forex=" + forex +
                ", prefex='" + prefex + '\'' +
                ", GenDesc='" + GenDesc + '\'' +
                ", netWeight='" + netWeight + '\'' +
                ", grossWeight='" + grossWeight + '\'' +
                ", quantityOfItems=" + quantityOfItems +
                ", airTicketNum='" + airTicketNum + '\'' +
                ", route='" + route + '\'' +
                ", airline='" + airline + '\'' +
                ", benePhone='" + benePhone + '\'' +
                ", beneEmail='" + beneEmail + '\'' +
                ", beneOrderBy='" + beneOrderBy + '\'' +
                ", appName='" + appName + '\'' +
                ", appEmail='" + appEmail + '\'' +
                ", appAddress1='" + appAddress1 + '\'' +
                ", appAddress2='" + appAddress2 + '\'' +
                ", city='" + city + '\'' +
                ", appState='" + appState + '\'' +
                ", appCountry='" + appCountry + '\'' +
                ", appPhone='" + appPhone + '\'' +
                ", appTIN='" + appTIN + '\'' +
                ", appRCNo='" + appRCNo + '\'' +
                ", appFax='" + appFax + '\'' +
                ", appNEPC='" + appNEPC + '\'' +
                ", totalFB='" + totalFB + '\'' +
                ", totalFreight='" + totalFreight + '\'' +
                ", ancillaryCharge='" + ancillaryCharge + '\'' +
                ", proformaDate='" + proformaDate + '\'' +
                ", TOD='" + TOD + '\'' +
                ", insurValue='" + insurValue + '\'' +
                ", totalCF='" + totalCF + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", exchangeRate='" + exchangeRate + '\'' +
                ", sourceFunds='" + sourceFunds + '\'' +
                ", paymentMode='" + paymentMode + '\'' +
                ", transferMode='" + transferMode + '\'' +
                ", proformaNo='" + proformaNo + '\'' +
                ", paymentDate='" + paymentDate + '\'' +
                ", ModeOfTransport='" + ModeOfTransport + '\'' +
                ", portOfDischarge='" + portOfDischarge + '\'' +
                ", portOfLoading='" + portOfLoading + '\'' +
                ", countryOfOrigin='" + countryOfOrigin + '\'' +
                ", countryOfSupply='" + countryOfSupply + '\'' +
                ", customOffice='" + customOffice + '\'' +
                ", beneName='" + beneName + '\'' +
                ", beneAddress1='" + beneAddress1 + '\'' +
                ", beneAddress2='" + beneAddress2 + '\'' +
                ", beneStateCode='" + beneStateCode + '\'' +
                ", beneCountry='" + beneCountry + '\'' +
                ", beneFax='" + beneFax + '\'' +
                ", appEndorsementRep='" + appEndorsementRep + '\'' +
                ", appEndorsementDeaker='" + appEndorsementDeaker + '\'' +
                ", date1=" + date1 +
                ", date2=" + date2 +
                ", attachments='" + attachments + '\'' +
                ", status='" + status + '\'' +
                ", beneCity='" + beneCity + '\'' +
                ", beneState='" + beneState + '\'' +
                ", workFlowId=" + workFlowId +
                ", version=" + version +
                ", cifid='" + cifid + '\'' +
                ", expiry='" + expiry + '\'' +
                ", allocatedAmount='" + allocatedAmount + '\'' +
                ", initiatedBy='" + initiatedBy + '\'' +
                ", dateCreated=" + dateCreated +
                ", formNumber='" + formNumber + '\'' +
                ", utilization='" + utilization + '\'' +
                ", statusDesc='" + statusDesc + '\'' +
                ", convert='" + convert + '\'' +
                ", comments='" + comments + '\'' +
                ", workFlow=" + workFlow +
                ", formAmount=" + formAmount +
                ", insuranceCompanyName='" + insuranceCompanyName + '\'' +
                ", insuranceCompanyAddress='" + insuranceCompanyAddress + '\'' +
                ", insurancePolicyNo='" + insurancePolicyNo + '\'' +
                ", insurancePolicyDate='" + insurancePolicyDate + '\'' +
                ", insurancePayableAt='" + insurancePayableAt + '\'' +
                ", insuranceDateOfExpiry='" + insuranceDateOfExpiry + '\'' +
                ", insurancePercentage='" + insurancePercentage + '\'' +
                ", insuredAmount=" + insuredAmount +
                ", premiumAmount=" + premiumAmount +
                ", insuredBy='" + insuredBy + '\'' +
                ", insuranceRate='" + insuranceRate + '\'' +
                ", items=" + items +
                ", designatedBank='" + designatedBank + '\'' +
                ", inspectionAgent='" + inspectionAgent + '\'' +
                '}';
    }
}

