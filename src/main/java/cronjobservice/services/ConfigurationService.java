package cronjobservice.services;


import cronjobservice.dtos.SettingDTO;
import cronjobservice.exception.CronJobException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *The {@code ConfigurationService} interface provides the methods for setting and managing system configurations
 *@author Fortunatus Ekenachi
 */

public interface ConfigurationService {

    String addSetting(SettingDTO setting) throws CronJobException;

    SettingDTO getSetting(Long id);

    SettingDTO getSettingByName(String name);

    Iterable<SettingDTO> getSettings();

    Page<SettingDTO> getSettings(Pageable pageDetails);

    String updateSetting(SettingDTO setting) throws CronJobException;

    String deleteSetting(Long id) throws CronJobException;

}
