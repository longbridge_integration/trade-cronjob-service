package cronjobservice.services.implementation;

import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.FormNxpDTO;
import cronjobservice.dtos.SettingDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.FormNxpRepo;
import cronjobservice.repositories.ServiceAuditRepo;
import cronjobservice.repositories.WorkFlowRepo;
import cronjobservice.repositories.WorkFlowStatesRepo;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.ConfigurationService;
import cronjobservice.services.FormNxpBankService;
import cronjobservice.services.MailService;
import cronjobservice.utils.DateFormatter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 9/27/2017.
 */
@Service
public class FormNxpBankServiceImpl implements FormNxpBankService {

    private Logger logger= LoggerFactory.getLogger(this.getClass());
    private Locale locale= LocaleContextHolder.getLocale();
//    private MessageSource messageSource;
//    private ConfigurationService configurationService;
//    private WorkFlowRepo workFlowRepo;

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    private WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    FormNxpRepo formNxpRepo;

    @Autowired
    ServiceAuditRepo serviceAuditRepo;

    @Autowired
    MailService mailService;
    @Autowired
    ConfigurationService configurationService;
    @Autowired
    WorkFlowRepo workFlowRepo;
    @Autowired
    MessageSource messageSource;

    @Override
    public FormNxpDTO getInitiatorDetails() throws CronJobException
    {

        FormNxpDTO formNxpDTO = new FormNxpDTO();
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();

        if(users==null)
        {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        else if ((users.getUserType()).equals(UserType.BANK))
        {

            BankUser user = (BankUser) users;
//            formNxpDTO.setExporterFirstName(user.getFirstName());
//            formNxpDTO.setExporterLastName(user.getLastName());
//            formNxpDTO.setExporterAddress(user.getAddress());

        }

        else {
            throw new CronJobException(messageSource.getMessage("form.process.failure",null,locale));
        }

        return formNxpDTO;

    }

    @Override
    public String saveFormNxp(FormNxpDTO formNxpDTO) throws CronJobException
    { CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
            .getPrincipal();
        User doneBy = principal.getUser();

        BankUser bankUser=(BankUser) doneBy;

        if(doneBy==null)
        {
            throw new CronJobException(messageSource.getMessage("form.add.failure",null,locale));
        }

        try {
            FormNxp formNxp = convertDTOTOEntity(formNxpDTO);
            formNxp.setStatus(formNxpDTO.getStatus());
            formNxp.setUserType(doneBy.getUserType().toString());
            formNxp.setInitiatedBy(doneBy.getUserName());
            formNxp.setDateCreated(new Date());
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_NXP_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            logger.info("work flow id {}", workFlow);
            formNxp.setWorkFlow(workFlow);
            formNxpRepo.save(formNxp);

            logger.info("Form NXP created successfully");

            return messageSource.getMessage("formnxp.add.success", null, locale);
        } catch (CronJobException te) {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, null), te);

        }
    }

    @Override
    public Boolean sendFormNxpITSmail(FormNxp formNxp) {

        try {
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "Form Nxp";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(formNxp.getTempFormNumber(),RequestType.FORM_NXP);
            Context context = new Context();
            context.setVariable("name", formNxp.getExporterName());
            context.setVariable("formNumber", formNxp.getTempFormNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", DateFormatter.format(new Date()));
            context.setVariable("customerId", formNxp.getCifid());
            context.setVariable("username", formNxp.getInitiatedBy());
            context.setVariable("error", serviceAudit.getResponseMessage());
            context.setVariable("code", "FORM_NXP");
            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
            Email email = new Email.Builder()
                    .setRecipients(allMails)
                    .setSubject(subject)
                    .setTemplate("mail/finacleSubmission")
                    .build();

            mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("form.add.success", null, locale));
        }
    }

    private FormNxp convertDTOTOEntity(FormNxpDTO formNxpDTO) throws CronJobException
    {
        logger.info("workflow id {}", formNxpDTO.getWorkFlowId());
//        logger.info("workflow  {} ", formNxpDTO.getWorkFlow());
        WorkFlow workFlow= workFlowRepo.findOne(formNxpDTO.getWorkFlowId());
//        formNxpDTO.setWorkFlow(workFlow);
        FormNxp formNxp=modelMapper.map(formNxpDTO,FormNxp.class);
        return formNxp;
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS)
    {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        for(Comments comments:commentDTOS)
        {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return  commentDTOList;
    }

    private CommentDTO convertEntityToDTO(Comments comments)
    {
        WorkFlowStates workFlowStates= workFlowStatesRepo.findByTypeAndCode("FORM_Q",comments.getStatus());
        if(workFlowStates!=null)
        {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments,CommentDTO.class);
    }


    @Override
    public List<CommentDTO> getComments(FormNxp formNxp) throws CronJobException {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        {
            commentDTOList=convertEntitiestoDTOs(formNxp.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus()!="A");


        }
        return  commentDTOList;
    }

    @Override
    public FormNxp convertDTOToEntity(FormNxpDTO formNxpDTO)
    {
        FormNxp formNXP = modelMapper.map(formNxpDTO, FormNxp.class);

        formNXP.setWorkFlow(workFlowRepo.findOne(formNxpDTO.getWorkFlowId()));


        return formNXP;
    }

}
