package cronjobservice.jobs.formajobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class FormACancelledOnFinacleJob implements Job {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextFormA = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextFormA.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
            logger.info("Form A cancelled Job starting");
            cronJobService.saveRunningJob(JobCategory.FORMA_ALLOCATION.toString(),cronJobService.getCurrentExpression(JobCategory.FORMA_ALLOCATION.toString()));

            cronJobService.getFormACanceledOnFinacle();
            cronJobService.updateRunningJob(JobCategory.FORMA_ALLOCATION.toString());
            logger.info("Form A cancelled Job ends");
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
