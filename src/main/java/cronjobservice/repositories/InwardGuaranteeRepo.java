package cronjobservice.repositories;

import cronjobservice.models.InwardGuarantee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by SYLVESTER on 10/18/2017.
 */
@Repository
@Transactional
public interface InwardGuaranteeRepo extends CommonRepo<InwardGuarantee,Long> {

    Long countByCifid(String cifid);

    InwardGuarantee findByGuaranteeNumber(String guaranteeNumber);

    @Query("select s from InwardGuarantee s where s.status!=:openstatus")
    Page<InwardGuarantee> findStatus(@Param("openstatus") String status, Pageable pageable);

    @Query("select s from InwardGuarantee s where s.status=:openstatus")
    Page<InwardGuarantee> findOpenStatus(@Param("openstatus") String status, Pageable pageable);

    Page<InwardGuarantee> findByStatusIsNull(Pageable pageable);

    Page<InwardGuarantee> findByStatusNotNull(Pageable pageable);

    Page<InwardGuarantee> findByCifidAndStatusNotNull(String cif, Pageable pageable);

//  @Query("select s from InwardGuarantee s where s.status is not null and s.status=:submitted")
//  Page<InwardGuarantee> findByStatusAndCifid(@Param("cifid") String cifid,@Param("submitted") String submitted,Pageable pageable);


    @Query("select distinct s from InwardGuarantee s where s.cifid=:cifid and s.status=:submitted")
    Page<InwardGuarantee> findByCifidAndStatusS(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

//  @Query("select s from InwardGuarantee s where s.status is null or s.status=:submitted or s.status=:declined")
//  Page<InwardGuarantee> findByStatus(@Param("submitted") String submitted,@Param("declined") String decline,Pageable pageable);


    @Query("select distinct s from InwardGuarantee s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<InwardGuarantee> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);



    @Query("select distinct s from InwardGuarantee s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<InwardGuarantee> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    Page<InwardGuarantee> findByCifid(String cifid, Pageable pageable);

    @Query("select distinct s from InwardGuarantee s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<InwardGuarantee> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    @Query("select distinct s from InwardGuarantee s where s.cifid=:cifid  and  s.status is not null  and   s.status!=:submitted and s.status!=:saved")
    Page<InwardGuarantee> findByClosedCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, @Param("saved") String saved, Pageable pageable);
}
