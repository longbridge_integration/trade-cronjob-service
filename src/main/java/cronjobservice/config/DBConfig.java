package cronjobservice.config;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DBConfig {

    org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${db.driverClassName}")
    private String dbDriverClassName;

    @Value("${db.url}")
    private String dbUrl;

    @Value("${db.username}")
    private String dbUsername;

    @Value("${db.password}")
    private String dbPassword;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        logger.info("dataSource url: {}" ,dbDriverClassName);
        dataSource.setDriverClassName(dbDriverClassName);
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);

        logger.info("dataSource url: " + dataSource.getUrl());
        logger.info("dataSource password: " + dataSource.getPassword());
        return dataSource;
    }

}
