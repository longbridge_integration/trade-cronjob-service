package cronjobservice.api;

import cronjobservice.dtos.BTADTO;
import cronjobservice.dtos.PTADTO;

import java.util.Date;

public class PtaFI {

    PTADTO ptadto;
    String comment ;
    String  authFirstName;
    String authLastName;
    String sortCode;
    Date madeOn;

    public PTADTO getPtadto() {
        return ptadto;
    }

    public void setPtadto(PTADTO ptadto) {
        this.ptadto = ptadto;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAuthFirstName() {
        return authFirstName;
    }

    public void setAuthFirstName(String authFirstName) {
        this.authFirstName = authFirstName;
    }

    public String getAuthLastName() {
        return authLastName;
    }

    public void setAuthLastName(String authLastName) {
        this.authLastName = authLastName;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public Date getMadeOn() {
        return madeOn;
    }

    public void setMadeOn(Date madeOn) {
        this.madeOn = madeOn;
    }
}
