package cronjobservice.dtos;

import com.fasterxml.jackson.annotation.JsonSetter;

public class ContactDTO {

	private long dt_RowId;
	private String firstName;
	private String lastName;
	private String branchName;
	private String email;
	private String userType;
	private boolean external;
	
	public ContactDTO() {
	}
	public ContactDTO(String firstName, String lastName, String branchName, String email, String userType, boolean external) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.branchName = branchName;
		this.email = email;
		this.userType = userType;
		this.external = external;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getEmail() {
		return email;
	}
	public boolean isExternal() {
		return external;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public long getDt_RowId() {
		return dt_RowId;
	}
	
	@JsonSetter("DT_RowId")
	public void setDt_RowId(long dt_RowId) {
		this.dt_RowId = dt_RowId;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setExternal(boolean external) {
		this.external = external;
	}
	
	
}
