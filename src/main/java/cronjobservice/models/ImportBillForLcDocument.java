package cronjobservice.models;


import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("bill under lc")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class ImportBillForLcDocument extends ImportBillDocument {
}
