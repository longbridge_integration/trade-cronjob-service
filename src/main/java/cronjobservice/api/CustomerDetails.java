package cronjobservice.api;

/**
 * Created by ayoade_farooq@yahoo.com on 4/24/2017.
 */
public class CustomerDetails {

    private String cifId;
    private String customerName;
    private String dateOfBirth;
    private String taxId;
    private String email;
    private String phoneNumber;
    private String bvn;
    private boolean isCorp;
    private String lastName;
    private String firstName;
    private String middleName;
    private String preferredName;
    private String address;
    private String town;
    private String state;
    private String country;
    private String rcNo;


    public CustomerDetails() {
    }

    public CustomerDetails(String cifId, String country, String customerName, String dateOfBirth, String email, String phoneNumber, String bvn, String lastName, String firstName, String middleName,String town,String address,String state, String preferredName, boolean isCorp, String rcNo) {
        this.cifId = cifId;
        this.customerName = customerName;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.bvn = bvn;
        this.isCorp = isCorp;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.address = address;
        this.state = state;
        this.town = town;
        this.preferredName = preferredName;
        this.rcNo=rcNo;
        this.country=country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isCorp() {
        return isCorp;
    }

    public void setCorp(boolean corp) {
        isCorp = corp;
    }

    public String getCifId() {
        return cifId;
    }

    public void setCifId(String cifId) {
        this.cifId = cifId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPreferredName() {
        return preferredName;
    }

    public void setPreferredName(String preferredName) {
        this.preferredName = preferredName;
    }

    public String getRcNo() {
        return rcNo;
    }

    public void setRcNo(String rcNo) {
        this.rcNo = rcNo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "CustomerDetails{" +
                "cifId='" + cifId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", bvn='" + bvn + '\'' +
                ", isCorp=" + isCorp +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", preferredName='" + preferredName + '\'' +
                ", rcNo='" + rcNo + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
