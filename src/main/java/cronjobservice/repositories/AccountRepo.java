package cronjobservice.repositories;

import cronjobservice.models.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Wunmi on 27/03/2017.
 */
@Repository
@Transactional
public interface AccountRepo extends CommonRepo<Account,Long> {

    Account findById(Long id);

    Account findFirstById(Long id);

    Account findFirstByAccountNumber(String accountNumber);

    Account findFirstByAccountId(String accountId);

    Account findFirstAccountByCustomerId(String customerId);


    boolean existsByCustomerId(String customerId);

    Page<Account> findAccountByCustomerId(String customerId, Pageable pageable);

    List<Account> findByCustomerId(String customerId);
    List<Account> findByCustomerIdAndSchemeTypeIn(String customerId, List<String> schmTypes);
    List<Account> findByCustomerIdAndSchemeTypeInIgnoreCase(String customerId, List<String> schmTypes);
    List<Account> findByCustomerIdAndCurrencyCodeIgnoreCase(String customerId, String currency);
    List<Account>  findByCustomerIdAndHiddenFlagIgnoreCase(String s1, String s2);

    List<Account> findByCustomerIdAndCurrencyCode(String customerId, String currency);

    @Modifying
    @Query("update Account a set a.primaryFlag = 'N' where a.primaryFlag = 'Y' and a.customerId = :customer")
    void unsetPrimaryAccount(@Param("customer") String customerId);

    @Query("select a  from  Account a where a.customerId=:customerId and a.currencyCode!=:currencyCode")
    List<Account> findCustomerDomAccounts(@Param("customerId") String customerId, @Param("currencyCode") String currencyCode);

    @Query("select a  from  Account a where a.customerId=:customerId and a.currencyCode!=:currencyCode and a.schemeType=:schemeType")
    List<Account> findCustomerDomAccountsAndSchemeCode(@Param("customerId") String customerId, @Param("currencyCode") String currencyCode, @Param("schemeType") String schemeType);

}
