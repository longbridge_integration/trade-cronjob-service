package cronjobservice.controller;

import cronjobservice.models.Email;
import cronjobservice.services.MailService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * Created by mac on 29/06/2018.
 */
@ControllerAdvice
public class ExceptionHandlerController {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    public static final String DEFAULT_ERROR_VIEW = "error";
    @Autowired
    private ErrorAttributes errorAttributes;
    @Value("${dev.members.mail}")
    private String devMembersMails;
    @Autowired
    private MailService mailService;

    @ExceptionHandler(value = {Exception.class, RuntimeException.class})
    public void defaultErrorHandler(HttpServletRequest request, Exception e) {
        e.printStackTrace();
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        System.out.println("the error is been caught ");
        Map<String, Object> errorDetails = errorAttributes.getErrorAttributes(requestAttributes, true);

        String errorPath = (String) errorDetails.get("path");
        String statusCode = errorDetails.get("status").toString();
        Object exception = errorDetails.get("exception");
        System.out.println("the error is due to "+errorDetails.toString());
        sendNotification(errorDetails);
        ModelAndView mav = new ModelAndView(DEFAULT_ERROR_VIEW);

//        mav.addObject("datetime", new Date());
//        mav.addObject("exception", e);
//        mav.addObject("url", request.getRequestURL());
    }
    private void sendNotification(Map errorDetails) {
        String time = errorDetails.get("timestamp").toString();
        String statusCode = errorDetails.get("status").toString();
        String error = errorDetails.get("error").toString();
        String exception = (String) errorDetails.get("exception");
        String message = errorDetails.get("message").toString();
        String trace = (String) errorDetails.get("trace");
        String path = (String) errorDetails.get("path");

        if (!"405".equals(statusCode)) {
            StringBuilder messageBuilder = new StringBuilder();
            messageBuilder.append("Time: " + time + "\n")
                    .append("Path: " + path + "\n")
                    .append("Status Code: " + statusCode + "\n")
                    .append("Error: " + error + "\n")
                    .append("Exception: " + exception + "\n")
                    .append("Message: " + message + "\n")
                    .append("Trace: " + trace + "\n");

            if(devMembersMails!=null) {
                String[] mailAddresses = StringUtils.split(devMembersMails, ",");
                logger.info("Email Addresses {}", mailAddresses);
                Email email = new Email.Builder().setRecipients(mailAddresses)
                        .setSubject("FCMB Trade Job")
                        .setBody(messageBuilder.toString())
                        .build();
                new Thread(() -> mailService.send(email)).start();
            }
        }


    }

}
