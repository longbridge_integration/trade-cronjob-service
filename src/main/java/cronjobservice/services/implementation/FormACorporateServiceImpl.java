package cronjobservice.services.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.FormNumber;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by chiomarose on 21/09/2017.
 */
@Service
public class FormACorporateServiceImpl implements FormACorporateService {


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private FormARepo formARepo;
    private MessageSource messageSource;
    private WorkFlowRepo workFlowRepo;
    private ConfigurationService configurationService;
//    private UserGroupService userGroupService;
    private RetailUserService retailUserService;
    private CorporateService corporateService;
    @Autowired
    private WorkFlowStatesRepo workFlowStatesRepo;
//    @Autowired
//    private  CorporateUserService corporateUserService;
    @Autowired
    private FormaDocumentRepo formaDocumentRepo;
    @Autowired
    private FormaAllocationRepo formaAllocationRepo;
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    private BranchRepo branchRepo;
    @Autowired
    private CodeService codeService;

    @Autowired
    private CodeRepo codeRepo;
// @Autowired
//    private CorporateBeneficiaryService corporateBeneficiaryService;

    @Autowired
    private CorporateRepo corporateRepo;

    private Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    AccountService accountService;

    @Autowired
    EntityManager entityManager;
    @Autowired
    ServiceAuditRepo serviceAuditRepo;

    @Autowired
    MailService mailService;

    @Value("${forma.upload.files}")
    private String UPLOADED_FOLDER;

    public FormACorporateServiceImpl(FormARepo formARepo, MessageSource messageSource, WorkFlowRepo workFlowRepo, ConfigurationService configurationService,RetailUserService retailUserService, CorporateService corporateService) {
        this.formARepo = formARepo;
        this.messageSource = messageSource;
        this.workFlowRepo = workFlowRepo;
        this.configurationService = configurationService;
//        this.userGroupService = userGroupService;
        this.retailUserService = retailUserService;
        this.corporateService = corporateService;
    }

    @Override
    public FormADTO editFormA(Long id) throws CronJobException {
        FormA formA = formARepo.findOne(id);
        return convertEntityTODTO(formA);


    }

    @Override
    public FormADTO getInitiatorDetails() throws CronJobException {

        FormADTO formADTO = new FormADTO();
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        } else if ((users.getUserType()).equals(UserType.CORPORATE)) {

            CorporateUser user = (CorporateUser) users;
            Corporate corporate = user.getCorporate();
            formADTO.setCorporateName(corporate.getName());
            formADTO.setPhone(corporate.getPhone());
            formADTO.setTown(corporate.getTown());
            formADTO.setState(corporate.getState());
            formADTO.setUserType("CORPORATE");
            formADTO.setAddress(corporate.getAddress());
            formADTO.setEmail(corporate.getEmail());
            formADTO.setCountry(corporate.getCountry());

        } else {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        return formADTO;


    }

    @Override
    public Boolean sendFormAITSmail(FormA formA) {

        try {
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "Form A";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(formA.getTempFormNumber(),RequestType.FORM_A);
            Context context = new Context();
            context.setVariable("name", formA.getFirstName() + " " + formA.getLastName() + " " + formA.getCorporateName());
            context.setVariable("formNumber", formA.getTempFormNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", DateFormatter.format(new Date()));
            context.setVariable("customerId", formA.getCifid());
            context.setVariable("username", formA.getInitiatedBy());
            context.setVariable("error", serviceAudit.getResponseMessage());
            context.setVariable("code", "FORM_A");
            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
            Email email = new Email.Builder()
                    .setRecipients(allMails)
                    .setSubject(subject)
                    .setTemplate("mail/finacleSubmission")
                    .build();

            mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("form.add.success", null, locale));
        }
    }


    @Override
    public List<AccountDTO> getCustomerNairaAccounts() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> nairaAccountList = new ArrayList<>();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        if ((users.getUserType()).equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) users;
            nairaAccountList = accountService.getCustomerNairaAccounts(corporateUser.getCorporate().getCustomerId(), "NGN");

        }
        return nairaAccountList;

    }


    public List<AccountDTO> getCustomerDomAccounts() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> domaccountList = new ArrayList<>();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }

        if (users.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) users;
            domaccountList = accountService.getCustomerDomAccounts(corporateUser.getCorporate().getCustomerId(), "NGN");
        }
        return domaccountList;
    }

    @Override
    public AccountDTO getAccountByAccountNumber(String accountNumber) {

        if(accountNumber==null)
        {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }

         AccountDTO  accountDTO=accountService.getAccountByAccountNumber(accountNumber);

        return accountDTO;
    }

    @Override
    public FormADTO getFormA(Long id) throws CronJobException {
        FormA formA = formARepo.findOne(id);
        FormADTO formADTO =convertEntityTODTOView(formA);
//        if(formA.getDocuments() != null) {
//            Iterator<FormaDocument> formaDocumentIterator = formA.getDocuments().iterator();
//            Iterator<FormaDocumentDTO> formaDocumentDTOIterator = formADTO.getDocuments().iterator();
//
//            while (formaDocumentIterator.hasNext() && formaDocumentDTOIterator.hasNext()) {
//                FormaDocument formaDocument = formaDocumentIterator.next();
//                FormaDocumentDTO formaDocumentDTO = formaDocumentDTOIterator.next();
//                if (formaDocument.getDocumentType() == null) {
//                    formaDocumentDTOIterator.remove();
//                    formaDocumentIterator.remove();
//                } else {
//                    formaDocumentDTO.setTypeOfDocument(formaDocument.getDocumentType().getCode());
//                }
//            }
//        }
        return formADTO;

    }


    @Override
    public Page<FormADTO> getFormAs(Pageable pageDetails) {
        Page<FormA> page = formARepo.findStatus(" ", pageDetails);
        List<FormADTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormADTO> pageImpl = new PageImpl<FormADTO>(dtOs, pageDetails, t);
        return pageImpl;
    }


    @Override
    public Page<FormADTO> getAllFormAs(Pageable pageDetails) {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        CorporateUser corporateUser = (CorporateUser) users;
        Page<FormA> page = formARepo.findByClosedCifidAndStatus(corporateUser.getCorporate().getCustomerId(), pageDetails);
        List<FormADTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        List<FormADTO> sortedList = dtOs.stream().sorted(Comparator.comparing(FormADTO::getDateCreated).reversed()).collect(Collectors.toList());
        Page<FormADTO> page1=new PageImpl<FormADTO>(sortedList,pageDetails,t);
        return page1;
    }

    @Override
    public Page<FormADTO> getOpenFormAs(Pageable pageable) {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        CorporateUser corporateUser = (CorporateUser) users;
        Page<FormA> pages = formARepo.findByCifidAndStatus(corporateUser.getCorporate().getCustomerId(), "P", "S", "D", pageable);
        List<FormADTO> formADTOS = convertEntitiesToDTOs(pages.getContent());
        long t = pages.getTotalElements();
        List<FormADTO> sortedList = formADTOS.stream().sorted(Comparator.comparing(FormADTO::getDateCreated).reversed()).collect(Collectors.toList());
        Page<FormADTO> page1=new PageImpl<FormADTO>(sortedList,pageable,t);
        return page1;
    }

    @Override
    public Page<FormADTO> getClosedFormAs(Pageable pageable) {
        Page<FormA> page = formARepo.findByStatusNotNull(pageable);
        List<FormADTO> formADTOS = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        List<FormADTO> sortedList = formADTOS.stream().sorted(Comparator.comparing(FormADTO::getDateCreated).reversed()).collect(Collectors.toList());
        Page<FormADTO> page1=new PageImpl<FormADTO>(sortedList,pageable,t);
        return page1;
    }


//    @Override
//    public Page<FormADTO> getFormAsForBankUser(Pageable pageable) {
//
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User loggedInUser = principal.getUser();
//        BankUser bankUser = (BankUser) loggedInUser;
//
//        //get all groups
//        List<UserGroupDTO> allUserGroups = userGroupService.getGroups();
//        List<CodeDTO> customerSegmentsInUserGroups = new ArrayList<>();
//        for (UserGroupDTO userGroupDTO : allUserGroups) {
//
//            List<BankUserDTO> bankUserList = userGroupService.getBankUsers(userGroupDTO.getId());
//            if (!bankUserList.isEmpty()) {
//
//                for (BankUserDTO bu : bankUserList) {
//                    if (bu.getId().equals(bankUser.getId())) {
//
//                        customerSegmentsInUserGroups.addAll(userGroupService.getCustomerSegments(userGroupDTO.getId()));
//                    }
//                }
//            }
//        }
//
//        logger.info("CUSTOMER LIST {}", customerSegmentsInUserGroups);
//
//        //get  customers in Customer Segments of user Groups
//        List<RetailUserDTO> retailCustomers = new ArrayList<>();
//        List<CorporateDTO> corporateCustomers = new ArrayList<>();
//        for (CodeDTO custSegment : customerSegmentsInUserGroups) {
//            retailCustomers.addAll(retailUserService.getByCustomerSegment(custSegment));
//            corporateCustomers.addAll(corporateService.getByCustomerSegment(custSegment));
//        }
//
//
//        //get submitted forms
//        List<FormADTO> formADTOS = new ArrayList<>();
//        for (RetailUserDTO retailUser : retailCustomers) {
//            Page<FormA> retailForms = formARepo.findByCifidAndStatus(retailUser.getCustomerId(), "P", pageable);
//            formADTOS.addAll(convertEntitiesToDTOs(retailForms.getContent()));
//        }
//        for (CorporateDTO corporate : corporateCustomers) {
//            Page<FormA> corporateForms = formARepo.findByCifidAndStatus(corporate.getCustomerId(), "P", pageable);
//            formADTOS.addAll(convertEntitiesToDTOs(corporateForms.getContent()));
//        }
//
//        long t = formADTOS.size();
//        Page<FormADTO> page1 = new PageImpl<FormADTO>(formADTOS, pageable, t);
//        return page1;
//    }


//    @Override
//    public Page<FormADTO> getOpenFormAsForBankUser(Pageable pageable) {
//
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User loggedInUser = principal.getUser();
//        BankUser bankUser = (BankUser) loggedInUser;
//
//        //get all groups
//        List<UserGroupDTO> allUserGroups = userGroupService.getGroups();
//
//        //get Groups user belongs to and customer Segments
//        List<CodeDTO> customerSegmentsInUserGroups = new ArrayList<>();
//        for (UserGroupDTO userGroupDTO : allUserGroups) {
//
//            List<BankUserDTO> bankUserList = userGroupService.getBankUsers(userGroupDTO.getId());
//            if (!bankUserList.isEmpty()) {
//                logger.info("BANK USER LIST {}", bankUserList);
//                logger.info("BANK USER {}", bankUser);
//                for (BankUserDTO bu : bankUserList) {
//                    if (bu.getId().equals(bankUser.getId())) {
//                        //userBelongsTo.add(userGroupDTO);
//                        customerSegmentsInUserGroups.addAll(userGroupService.getCustomerSegments(userGroupDTO.getId()));
//                    }
//                }
//            }
//        }
//
//        logger.info("CUSTOMER LIST {}", customerSegmentsInUserGroups);
//
//        //get  customers in Customer Segments of user Groups
//        List<RetailUserDTO> retailCustomers = new ArrayList<>();
//        List<CorporateDTO> corporateCustomers = new ArrayList<>();
//        for (CodeDTO custSegment : customerSegmentsInUserGroups) {
//            retailCustomers.addAll(retailUserService.getByCustomerSegment(custSegment));
//            corporateCustomers.addAll(corporateService.getByCustomerSegment(custSegment));
//        }
//
//
//        //get submitted forms
//        List<FormADTO> formADTOS = new ArrayList<>();
//        for (RetailUserDTO retailUser : retailCustomers) {
//            Page<FormA> retailForms = formARepo.findByOpenCifidAndStatus(retailUser.getCustomerId(), "S", pageable);
//            logger.info("RETAIL FORMS {}", retailForms.getContent());
//            formADTOS.addAll(convertEntitiesToDTOs(retailForms.getContent()));
//        }
//        for (CorporateDTO corporate : corporateCustomers) {
//            Page<FormA> corporateForms = formARepo.findByOpenCifidAndStatus(corporate.getCustomerId(), "S", pageable);
//            formADTOS.addAll(convertEntitiesToDTOs(corporateForms.getContent()));
//        }
//
//        long t = formADTOS.size();
//        Page<FormADTO> page1 = new PageImpl<FormADTO>(formADTOS, pageable, t);
//        return page1;
//    }

    public FormADTO convertEntityTODTO(FormA formA) {
        FormADTO formADTO = modelMapper.map(formA, FormADTO.class);
        if (formA.getStatus() != null && !(formA.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formADTO.getWorkFlow().getCodeType(), formADTO.getStatus());
            formADTO.setStatusDesc(workFlowStates.getDescription());
            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
            formADTO.setWorkFlow(formA.getWorkFlow());

        }

        if ("P".equals(formA.getStatus())) {
            formADTO.setStatusDesc("Saved");
            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
            formADTO.setWorkFlow(formA.getWorkFlow());
        }
        if (formA.getDateCreated() != null) {
            formADTO.setDateCreated(formA.getDateCreated());
        }
        if (formA.getAmount() != null && formA.getCurrencyCode() != null) {
            formADTO.setCurrAmount(formA.getCurrencyCode() + " " + formA.getAmount().toString());
        }
        if (formA.getAmount() != null) {
            formADTO.setAmount(formA.getAmount().toString());
        }
        if(formA.getDocSubBranch() != null){
            formADTO.setBranch(formA.getDocSubBranch().getId().toString());
        }
        if (formA.getAllocatedAmount() != null) {
            formADTO.setAllocatedAmount(formA.getAllocatedAmount().toString());
        }

        if (formA.getUtilization() != null) {
            logger.info("the utilization {}",formA.getUtilization());
            formADTO.setUtilization(formA.getUtilization().toString());
        }

        return formADTO;
    }

    public FormADTO convertEntityTODTOFinacle(FormA formA) {
        FormADTO formADTO = modelMapper.map(formA, FormADTO.class);

        if (formA.getDateCreated() != null) {
            formADTO.setDateCreated(formA.getDateCreated());
        }

        if (formA.getAllocatedAmount() != null) {
            formADTO.setAllocatedAmount(formA.getAllocatedAmount().toString());
        }
        return formADTO;
    }

    private FormADTO convertEntityTODTOView(FormA formA) {
        FormADTO formADTO = modelMapper.map(formA, FormADTO.class);
        if (formADTO.getStatus() != null && !(formADTO.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formADTO.getWorkFlow().getCodeType(), formADTO.getStatus());
            formADTO.setStatusDesc(workFlowStates.getDescription());
            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
            formADTO.setWorkFlow(formA.getWorkFlow());
        }
        if (formADTO.getStatus().equals("P")) {
            formADTO.setStatusDesc("Saved");
            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
            formADTO.setWorkFlow(formA.getWorkFlow());
        }
        if (formA.getDateCreated() != null) {
            formADTO.setDateCreated(formA.getDateCreated());
        }

        if (formA.getAmount() != null) {
            formADTO.setAmount(formA.getAmount().toString());
        }
        if (formA.getAllocatedAmount() != null) {
            formADTO.setAllocatedAmount(formA.getAllocatedAmount().toString());
        }
        if (formA.getUtilization() != null) {
            formADTO.setUtilization(formA.getUtilization().toString());
        }
        if(formA.getDocSubBranch() != null){
            formADTO.setBranch(formA.getDocSubBranch().getId().toString());
        }
        return formADTO;
    }

    private List<FormADTO> convertEntitiesToDTOs(Iterable<FormA> formAs) {
        List<FormADTO> formADTOs = new ArrayList<FormADTO>();
        for (FormA formA : formAs) {
            formADTOs.add(convertEntityTODTO(formA));
        }
        return formADTOs;
    }

//    @Override
//    public FormHolder createFormHolder(FormADTO formADTO, String name) throws CronJobException {
//
//        try {
//            FormHolder holder = new FormHolder();
//            List<Comments> allComments;
//            FormA formA = convertDTOTOEntity(formADTO);
//            Comments comments = commentService.addComment(formADTO.getComments(), formA.getWorkFlow().getDescription());
//            allComments = commentService.getAllComments();
//            allComments.add(comments);
//            formA.setComment(allComments);
//            holder.setObjectData(objectMapper.writeValueAsString(formA));
//            holder.setEntityId(formA.getId());
//            holder.setEntityName("FormA");
//            holder.setAction(name);
//            return holder;
//
//        } catch (JsonProcessingException e) {
//            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
//
//        }
//
//
//    }


    @Override
    public String deleteForm(Long id) throws CronJobException {
        FormA formA = formARepo.findOne(id);

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();

        if (doneBy == null) {
            logger.info("user is null");
            throw new CronJobException(messageSource.getMessage("forma.delete.failure", null, locale));
        }
        try {
            if(!formA.getStatus().equalsIgnoreCase("P")){
                throw new CronJobException(messageSource.getMessage("forma.delete.saved.failure", null, locale));
            }
            formARepo.delete(formA);
            return messageSource.getMessage("forma.delete.success", null, locale);
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("forma.delete.failure", null, locale), e);
        }

    }



    @Override
    public Iterable<FormADTO> getAllFormAs() {
        Iterable<FormA> formAs = formARepo.findAll();
        return convertEntitiesToDTOs(formAs);
    }

    private FormA convertDTOTOEntity(FormADTO formADTO) throws CronJobException {


        WorkFlow workFlow = workFlowRepo.findOne(formADTO.getWorkFlowId());
        logger.info("getting the work flow{}", workFlow);
        String bidRateCode = configurationService.getSettingByName("FORMA_RATE_CODE").getValue();

        formADTO.setWorkFlow(workFlow);
        FormA formA = modelMapper.map(formADTO, FormA.class);
        formA.setBidRateCode(bidRateCode);
        return formA;
    }



//    @Override
//    public String saveFormA(FormADTO formADTO) throws CronJobException {
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User doneBy = principal.getUser();
//
//        CorporateUser corporateUser = (CorporateUser) doneBy;
//
//        if (doneBy == null) {
//            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
//        }
//
//        try {
//            if (formADTO.isSaveBeneficiary()){
//                saveBeneficiary(corporateUser.getCorporate(), formADTO);
//            }
//            String generatedFormNumber = FormNumber.getFormNumber();
//
//            if(formADTO.getDocuments() != null) {
//                for (FormaDocumentDTO d : formADTO.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, generatedFormNumber, d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//            }
//            FormA formA = convertDTOTOEntity(formADTO);
//            formA.setUserType(doneBy.getUserType().toString());
//            formA.setInitiatedBy(doneBy.getUserName());
//            formA.setDateCreated(new Date());
//            formA.setCreatedOn(new Date());
//            formA.setFormNumber(generatedFormNumber);
//            formA.setTempFormNumber(generatedFormNumber);
//            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
//            formA.setBankCode(bankCodeSetting.getValue());
//            SettingDTO getBidRateCode = configurationService.getSettingByName("FORMA_RATE_CODE");
//            formA.setBidRateCode(getBidRateCode.getValue());
//            formA.setUtilization(new BigDecimal(0));
//
//            formA.setExpiryDate(getFormExpiryDate());
//            formA.setAllocatedAmount(new BigDecimal(0));
//            formA.setUtilization(new BigDecimal(0));
//            formA.setCifid(corporateUser.getCorporate().getCustomerId());
//            formA.setCountry("NG");
//
//            Date date = getFormExpiryDate();
//            if (date != null) {
//                formA.setExpiryDate(date);
//            }
//
//            /**
//             * BillsDocument table
//             */
//            if(formADTO.getDocuments() != null) {
//
//                if(!"".equalsIgnoreCase(formADTO.getBranch()) && null != formADTO.getBranch()) {
//                    System.out.println(formADTO.getBranch());
//                    Branch branch = branchRepo.findOne(Long.parseLong(formADTO.getBranch()));
//                    formA.setDocSubBranch(branch);
//                }
//                Iterator<FormaDocument> DocumentIterator = formA.getDocuments().iterator();
//                Iterator<FormaDocumentDTO> DocumentDTOIterator = formADTO.getDocuments().iterator();
//
//                while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                    FormaDocument document = DocumentIterator.next();
//                    FormaDocumentDTO documentDTO = DocumentDTOIterator.next();
//                    if (documentDTO.getTypeOfDocument() == null) {
//                        DocumentIterator.remove();
//                        DocumentDTOIterator.remove();
//                    } else {
//                        document.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                        document.setFormA(formA);
//                    }
//                }
//            }
//
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_A_WF");
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            formA.setWorkFlow(workFlow);
//            formARepo.save(formA);
//
//            if(formADTO.getStatus().equalsIgnoreCase("S")){
//                sendITSmail(corporateUser,formA);
//            }
//
//            return messageSource.getMessage("forma.add.success", null, locale);
//        } catch (CronJobException e) {
//            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale), e);
//        }
//    }

    @Override
    public Boolean saveFormAFromFinacle(FormA formA) throws CronJobException {

        try {
            String generatedFormNumber = FormNumber.getFormNumber();

            formA.setUserType("BANK");
            formA.setInitiatedBy("FINACLE");
            formA.setDateCreated(new Date());
            formA.setCreatedOn(new Date());
            formA.setSubmitFlag("S");
            formA.setCorporateName(formA.getFirstName());
            formA.setTempFormNumber(generatedFormNumber);
            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
            formA.setBankCode(bankCodeSetting.getValue());
            SettingDTO getBidRateCode = configurationService.getSettingByName("FORMA_RATE_CODE");
            formA.setBidRateCode(getBidRateCode.getValue());


            if (null==formA.getUtilization()){
                formA.setUtilization(new BigDecimal("0"));
            }

            BigDecimal formAmount = formA.getAmount();
            BigDecimal utilized = formA.getUtilization();
            logger.info("form a utilized amt for form a added on fiancle {} ",utilized);

            int i = formAmount.compareTo(utilized);
            if(i == 0){
                formA.setStatus("R");
            }else if(utilized.compareTo(BigDecimal.ZERO) == 0){
                formA.setStatus("A");
            }else if(i > 0 && !(utilized.compareTo(BigDecimal.ZERO) == 0) ){
                formA.setStatus("PR");
            }

            formA.setExpiryDate(getFormExpiryDate());

            formA.setCifid(formA.getCifid());

            Date date = getFormExpiryDate();
            if (date != null) {
                formA.setExpiryDate(date);
            }



            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_A_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            formA.setWorkFlow(workFlow);
            formARepo.save(formA);


            return true;
        } catch (CronJobException e) {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale), e);
        }
    }


    @Override
    public long getTotalNumberOpenFormA() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User logginUser = principal.getUser();
        long totalNumberOfOpenRequest = 0;
        if (logginUser.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) logginUser;
            totalNumberOfOpenRequest = formARepo.countByCifidAndStatus(corporateUser.getCorporate().getCustomerId(), "P", "S", "D");
        }

        return totalNumberOfOpenRequest;
    }

    @Override
    public long getTotalNumberFormA() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User logginUser = principal.getUser();
        long totalNumberOfRequest = 0;
        if (logginUser.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) logginUser;
            totalNumberOfRequest = formARepo.countByCifid(corporateUser.getCorporate().getCustomerId());
        }

        return totalNumberOfRequest;
    }

//    @Override
//    public String updateFormA(FormADTO formADTO) throws CronJobException {
//        System.out.println("got into the service");
//        FormA formA = formARepo.findOne(formADTO.getId());
//
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User doneBy = principal.getUser();
//
//        CorporateUser corporateUser = (CorporateUser) doneBy;
//        if (doneBy == null) {
//            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
//        }
//
//
//        try {
//            if (formADTO.isSaveBeneficiary()){
//                saveBeneficiary(corporateUser.getCorporate(), formADTO);
//            }
//            System.out.println("forma service inside the try");
//            if (formADTO.getDocuments() != null) {
//                for (FormaDocumentDTO d : formADTO.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        System.out.println("forma service saving the document");
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, formA.getTempFormNumber(), d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//            }
//
//            entityManager.detach(formA);
//            formA = convertDTOTOEntity(formADTO);
//            formA.setUserType(doneBy.getUserType().toString());
//            formA.setInitiatedBy(doneBy.getUserName());
//            formA.setDateCreated(new Date());
//            formA.setExpiryDate(getFormExpiryDate());
//            formA.setCifid(corporateUser.getCorporate().getCustomerId());
//            formA.setUtilization(new BigDecimal(0));
//            formA.setAllocatedAmount(new BigDecimal(0));
//            formA.setCountry("NG");
//            System.out.println("forma service after setting all forma parameters");
//            /**
//             * BillsDocument table
//             */
//            if (formADTO.getDocuments() != null) {
//                if (!"".equalsIgnoreCase(formADTO.getBranch()) && null != formADTO){
//                    System.out.println(formADTO.getBranch());
//                Branch branch = branchRepo.findOne(Long.parseLong(formADTO.getBranch()));
//                formA.setDocSubBranch(branch);
//            }
//            Iterator<FormaDocument> DocumentIterator = formA.getDocuments().iterator();
//            Iterator<FormaDocumentDTO> DocumentDTOIterator = formADTO.getDocuments().iterator();
//            Collection<FormaDocument> fields = new ArrayList<FormaDocument>();
//            while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                FormaDocument document = DocumentIterator.next();
//                FormaDocumentDTO documentDTO = DocumentDTOIterator.next();
//                FormaDocument formaDocument = null;
//                if (documentDTO.getTypeOfDocument() == null) {
//                    DocumentIterator.remove();
//                    DocumentDTOIterator.remove();
//                } else if (documentDTO.getId() == null) {
//                    System.out.println("newly added document");
//                    formaDocument = new FormaDocument();
//                    System.out.println("before model mapper");
//                    modelMapper.map(documentDTO, formaDocument);
//                    System.out.println("after model mapper");
//                    formaDocument.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                    formaDocument.setFormA(formA);
//                    System.out.println("newly added document ending");
//                } else {
//                    System.out.println("existing document");
//                    formaDocument = formaDocumentRepo.findOne(documentDTO.getId());
//                    System.out.println("after finding existing document");
//                    formaDocument.setDocumentValue(documentDTO.getDocumentValue());
//                    formaDocument.setDocumentRemark(documentDTO.getDocumentRemark());
//                    formaDocument.setDocumentNumber(documentDTO.getDocumentNumber());
//                    formaDocument.setDocumentDate(documentDTO.getDocumentDate());
//                    if (documentDTO.getFileUpload() != null) {
//                        formaDocument.setFileUpload(documentDTO.getFileUpload());
//                    }
//                    formaDocument.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                    formaDocument.setFormA(formA);
//                    System.out.println("ending of  existing document");
//
//                }
//                fields.add(formaDocument);
//            }
//            formA.setDocuments(fields);
//        }
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_A_WF");
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            formA.setWorkFlow(workFlow);
//            formARepo.save(formA);
//            return messageSource.getMessage("forma.update.success", null, locale);
//        } catch (CronJobException e) {
//
//            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
//        }
//
//
//    }

    @Override
    public Boolean updateFormAFromFinacle(FormA tmsForm, FormA finForm) throws CronJobException {
        System.out.println("got into the service");

        try {
            tmsForm.setFormNumber(finForm.getFormNumber());
            tmsForm.setTranId(finForm.getTranId());
            formARepo.save(tmsForm);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }


    }

    @Override
    public Boolean cancelFormAFromFinacle(FormA tmsForm, FormA finForm) throws CronJobException {


        try {
            tmsForm.setStatus("X");
//            tmsForm.setFormNumber(finForm.getFormNumber());
//            tmsForm.setTranId(finForm.getTranId());
            formARepo.save(tmsForm);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }


    }

    @Override
    @Transactional
    public Boolean updateFormAAllocation(FormA tmsForm, FormA finForm) throws CronJobException {
        try {
            FormaAllocation formaAllocation = new FormaAllocation();
            formaAllocation.setAllocationAccount(finForm.getAllocationAccount());
            formaAllocation.setAllocationAmount(finForm.getAllocatedAmount());
            formaAllocation.setAllocationCurrency(finForm.getAllocationCurrencyCode());
            formaAllocation.setExchangeRate(finForm.getExchangeRate());
            formaAllocation.setFormaNumber(finForm.getFormNumber());
            formaAllocation.setDateOfAllocation(finForm.getDateOfAllocation());
            formaAllocation.setDealId(finForm.getDealId());
            formaAllocation.setTranId(finForm.getTranId());
            formaAllocation.setCreatedOn(new Date());
            formaAllocationRepo.save(formaAllocation);
            tmsForm.setAllocatedAmount(finForm.getAllocatedAmount());
            tmsForm.setStatus("L");
            formARepo.save(tmsForm);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }catch (Exception e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

    @Override
    public Boolean updateFormAUtilization(FormA formA, Remittance remittance) throws CronJobException {
        try {
            BigDecimal remitUtilization = new BigDecimal(remittance.getTransferAmount());
            BigDecimal formaUtilization = formA.getUtilization();
            BigDecimal updatedUtilization = remitUtilization.add(formaUtilization);
            formA.setUtilization(updatedUtilization);
            formA.setAllocatedAmount(remitUtilization);



            if (formA.getUtilization().compareTo(formA.getAmount()) == -1){
                formA.setStatus("PR");
            }else {
                formA.setStatus("R");
            }
            formARepo.save(formA);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }


    private Date getFormExpiryDate() {
        try {
            Calendar calendar = Calendar.getInstance();
            SettingDTO setting = configurationService.getSettingByName("FORM_A_EXPIRY");
            if (setting != null && setting.isEnabled()) {
                int days = NumberUtils.toInt(setting.getValue());
                calendar.add(Calendar.DAY_OF_YEAR, days);

            }
            return calendar.getTime();
        } catch (CronJobException e) {
            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);

        }

    }

    private CommentDTO convertEntityToDTO(Comments comments) {
        return modelMapper.map(comments, CommentDTO.class);
    }

    private List<CommentDTO> convertEntitiestoDTOs(FormA formA) {
        List<CommentDTO> commentDTOList = new ArrayList<>();

        List<Comments> commentDTOS = formA.getComment();

        for (Comments comments : commentDTOS) {

            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formA.getWorkFlow().getCodeType(), formA.getStatus());
            comments.setStatus(workFlowStates.getDescription());

            commentDTOList.add(convertEntityToDTO(comments));

        }

        return commentDTOList;
    }

    public List<CommentDTO> getComments(Long id) throws CronJobException {
        FormA formA = formARepo.findOne(id);
        List<CommentDTO> commentDTOList = new ArrayList<>();
        if (formA != null) {


            commentDTOList = convertEntitiestoDTOs(formA);
        }
        return commentDTOList;
    }

    @Override
    public Boolean checkAvailableBalance(String amount, String accountId) throws CronJobException {
        Boolean result;
        Map<String, BigDecimal> availableBalance = integrationService.getBalance(accountId);
        BigDecimal availableAmount = availableBalance.get("AvailableBalance");
        if (!(amount.isEmpty())) {
            new BigDecimal(amount);
        }
        if (!(amount).isEmpty()) {
            int compareResult = new BigDecimal(amount).compareTo(availableAmount);

            if (compareResult == 1) {
                throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));

            }

        }
        result = true;

        return result;
    }

//


    @Override
    public String addAttachments(MultipartFile file, String formaNumber, String documentType) {
        String uploadedFileName = "";


        if (file.isEmpty()) {
            System.out.println("file is empty");
        }

        try {
            byte[] bytes = file.getBytes();
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User users = principal.getUser();

            if (users.getUserType().equals(UserType.CORPORATE)) {
                CorporateUser corporateUser = (CorporateUser) users;
                String username = corporateUser.getUserName();
                logger.info("corporate user {} with username {}", corporateUser, username);

                Path  path = Paths.get(UPLOADED_FOLDER + formaNumber + documentType + file.getOriginalFilename());
                logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());


                File f = new File(path.toString());
                if(f.exists()){
                    System.out.println("file already exist");
                }else{
                    System.out.println("file does not exist");
                }

                Files.write(path, bytes);
                logger.info("file uploaded successfully to {} directory", path);
                uploadedFileName =file.getOriginalFilename();
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("failed to upload file");
        }

        logger.info("uploadFileName {}", uploadedFileName);
        if (StringUtils.isEmpty(uploadedFileName)) {
            logger.info("No file added, Please select a file to upload");

        } else {
            logger.info("You successfully uploaded {}", uploadedFileName);

        }
        return uploadedFileName;
    }

    private void sendITSmail(CorporateUser user, FormA formADTO) {

        try {
            SettingDTO settingDTO = configurationService.getSettingByName("GROUP_EMAIL");
            String itsMail = settingDTO.getValue();
            String fullName = user.getCorporate().getName();
            String formType = "form A";
            Context context = new Context();
            context.setVariable("name", fullName);
            context.setVariable("username", user.getUserName());
            context.setVariable("formNumber", formADTO.getFormNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", new Date());
            context.setVariable("customerId", user.getCorporate().getCustomerId());
            context.setVariable("code", "FORM_A");
            String subject = String.format(messageSource.getMessage("form.submission.subject", null, locale),formType);
            Email email = new Email.Builder()
                    .setRecipient(itsMail)
                    .setSubject(subject)
                    .setTemplate("mail/formSubmission")
                    .build();

            mailService.sendMail(email, context);
        } catch (Exception e) {
            logger.error("Error occurred sending activation credentials", e);
        }
    }


}