package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by JUDE on 12/6/2017.
 */
@Entity
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = "documentNumber",""))
public class BillsDocument extends AbstractEntity{

    private String documentNumber;
    private String documentDate;

    //    private String documentType;
    @ManyToOne
    private Code documentType;
    private String documentValue;
    private String documentRemark;
    private String fileUpload;
    private String numberOfOriginalDoc;
    private String numberOfDuplicateDoc;


    @ManyToOne
    private BillsUnderLc billsUnderLc;


    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public Code getDocumentType() {
        return documentType;
    }

    public void setDocumentType(Code documentType) {
        this.documentType = documentType;
    }

    public String getDocumentValue() {
        return documentValue;
    }

    public void setDocumentValue(String documentValue) {
        this.documentValue = documentValue;
    }

    public String getDocumentRemark() {
        return documentRemark;
    }

    public void setDocumentRemark(String documentRemark) {
        this.documentRemark = documentRemark;
    }

    public String getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(String fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getNumberOfOriginalDoc() {
        return numberOfOriginalDoc;
    }

    public void setNumberOfOriginalDoc(String numberOfOriginalDoc) {
        this.numberOfOriginalDoc = numberOfOriginalDoc;
    }

    public String getNumberOfDuplicateDoc() {
        return numberOfDuplicateDoc;
    }

    public void setNumberOfDuplicateDoc(String numberOfDuplicateDoc) {
        this.numberOfDuplicateDoc = numberOfDuplicateDoc;
    }

    public BillsUnderLc getBillsUnderLc() {
        return billsUnderLc;
    }

    public void setBillsUnderLc(BillsUnderLc billsUnderLc) {
        this.billsUnderLc = billsUnderLc;
    }
}
