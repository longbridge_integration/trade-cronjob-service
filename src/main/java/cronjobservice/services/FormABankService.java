package cronjobservice.services;


import cronjobservice.api.FormAFI;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.FormA;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by chiomarose on 21/09/2017.
 */
public interface FormABankService {


    String saveFormA(FormADTO formADTO) throws CronJobException;

    String updateFormA(FormADTO formADTO) throws CronJobException;

    FormADTO getFormA(Long id) throws CronJobException;

    Page<FormADTO> getAllFormAs(Pageable pageDetails);

    Page<FormADTO> getOpenFormAs(Pageable pageable) throws CronJobException;

    Page<FormADTO> getClosedFormAs(Pageable pageable);

//    Page<FormADTO> getOpenFormAsForBankUser(Pageable pageable) throws CronJobException;

    FormHolder createFormHolder(FormADTO formADTO, String name) throws CronJobException;

    FormADTO editFormA(Long id) throws CronJobException;

    Iterable<FormADTO> getAllFormAs() throws CronJobException;

    FormADTO getInitiatorDetails() throws CronJobException;

//    Page<FormADTO> getFormAsForBankUser(Pageable pageable) throws CronJobException;

    String getFormNumber() throws CronJobException;

    List<AccountDTO>  getCustomerNairaAccounts(String cifid) throws CronJobException;

    List<AccountDTO> getCustomerDomAccounts(String cifid) throws CronJobException;

     List<CommentDTO> getComments(Long id) throws CronJobException;

    FormAFI getCommentsAndBankersDetails(FormA formA);

    List<CommentDTO> getComments(FormA formA) throws CronJobException;
    String addAttachments(MultipartFile file, String formaNumber, String documentType);
    AccountDTO getAccountByAccountNumber(String accountNumber) throws CronJobException;
    BankCustomerDTO getBankCustomerByCustomerId(String customerId) throws CronJobException;
    String checkApplicationNumber(String applicationNumber) throws CronJobException;
    String checkUsertype(String cifid) throws CronJobException;



}
