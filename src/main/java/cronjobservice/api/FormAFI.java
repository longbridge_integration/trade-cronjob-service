package cronjobservice.api;


import cronjobservice.dtos.FormADTO;

import java.util.Date;

public class FormAFI {

    FormADTO formADTO;
    String comment ;
    String  authFirstName;
    String authLastName;
    String sortCode;
    Date madeOn;

    public FormADTO getFormADTO() {
        return formADTO;
    }

    public void setFormADTO(FormADTO formADTO) {
        this.formADTO = formADTO;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAuthFirstName() {
        return authFirstName;
    }

    public void setAuthFirstName(String authFirstName) {
        this.authFirstName = authFirstName;
    }

    public String getAuthLastName() {
        return authLastName;
    }

    public void setAuthLastName(String authLastName) {
        this.authLastName = authLastName;
    }

    public Date getMadeOn() {
        return madeOn;
    }

    public void setMadeOn(Date madeOn) {
        this.madeOn = madeOn;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }
}
