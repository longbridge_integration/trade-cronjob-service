package cronjobservice.models;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;

/**
 * Created by chiomarose on 19/10/2017.
 */
@Entity
@Audited
public class LCFields extends AbstractEntity {

    private String fieldName;

    private String enabled;

    private String fieldDescription;

    public String getFieldDescription() {
        return fieldDescription;
    }

    public void setFieldDescription(String fieldDescription) {
        this.fieldDescription = fieldDescription;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public String toString() {
        return "LCFields{" +
                "fieldName='" + fieldName + '\'' +
                ", enabled='" + enabled + '\'' +
                '}';
    }
}

