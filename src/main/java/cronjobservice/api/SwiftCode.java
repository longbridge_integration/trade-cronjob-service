package cronjobservice.api;

import lombok.Data;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Kwere on 10/18/2017.
 */

@Data
public class SwiftCode {

    private String swiftCode;
    private String bankName;

    public SwiftCode(){}

    public SwiftCode(String swiftCode, String bankName) {
        this.swiftCode = swiftCode;
        this.bankName = bankName;
    }

    public SwiftCode(ResultSet set) throws SQLException {
        this.swiftCode = set.getString("UNQ_BANK_IDENTIFIER");
        this.bankName = set.getString("bank_name");
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
