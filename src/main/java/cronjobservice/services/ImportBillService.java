package cronjobservice.services;

import cronjobservice.dtos.ImportBillForCollectionDTO;
import cronjobservice.dtos.ImportBillUnderLcDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.BillsUnderLc;
import cronjobservice.models.ImportBillForCollection;
import cronjobservice.models.ImportBillUnderLc;

import javax.transaction.Transactional;

public interface ImportBillService {
    @Transactional
    String getImportBillUnderLcAddedOnFinacle();

    @Transactional
    String getImportBillForCollectionAddedOnFinacle();

    ImportBillUnderLcDTO convertEntityToDTO(ImportBillUnderLc importBillUnderLc);

    ImportBillUnderLcDTO convertEntityToBillsUnderLcDTO(ImportBillUnderLc billsUnderLc);

    ImportBillForCollectionDTO convertEntityToBillsForCollectioncDTO(ImportBillForCollection billsForCollection);

    @Transactional
    Boolean saveImportBillsUnderLcFromFinacle(ImportBillUnderLc billsUnderLc) throws CronJobException;

    @Transactional
    Boolean saveImportBillsForCollectionFromFinacle(ImportBillForCollection billsForCollection) throws CronJobException;
}
