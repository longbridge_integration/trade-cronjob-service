package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import cronjobservice.utils.PrettySerializer;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.IOException;

/**
 * Created by chiomarose on 15/08/2017.
 */
@Entity
@Audited(withModifiedFlag=true)
@Where(clause ="del_Flag='N'" )
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"userName","deletedOn"}))
public class BankUser extends  User implements Person,PrettySerializer {

//    @JsonIgnore
//    @ManyToMany(mappedBy = "users")
//    private List<UserGroup> groups;
//
    @ManyToOne
    private Branch branch;

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
    //    @JsonIgnore
//    private List<UserGroup> getGroups()
//    {
//        return groups;
//    }

    public BankUser()
    {

        this.userType = (UserType.BANK);
    }

//    @JsonIgnore
//    private void setGroups(List<UserGroup> groups)
//    {
//
//        this.groups=groups;
//    }

    @Override
    @JsonIgnore
    public boolean isExternal() {
        return false;
    }


    @Override
    public String toString() {
        return "BankUser{" +
                "branch=" + branch +
                ", userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", password='" + password + '\'' +
                ", status='" + status + '\'' +
                ", createdOnDate=" + createdOnDate +
                ", expiryDate=" + expiryDate +
                ", lockedUntilDate=" + lockedUntilDate +
                ", lastLoginDate=" + lastLoginDate +
                ", noOfLoginAttempts=" + noOfLoginAttempts +
                ", noOfTokenAttempts=" + noOfTokenAttempts +
                ", userType=" + userType +
                ", alertPreference=" + alertPreference +
                ", role=" + role +
                ", entrustId='" + entrustId + '\'' +
                ", entrustGroup='" + entrustGroup + '\'' +
                ", id=" + id +
                ", version=" + version +
                ", delFlag='" + delFlag + '\'' +
                ", deletedOn=" + deletedOn +
                '}';
    }

    @Override
    @JsonIgnore
    public JsonSerializer<User> getSerializer() {
        return new JsonSerializer<User>() {
            @Override
            public void serialize(User value, JsonGenerator gen, SerializerProvider serializers)
                    throws IOException {

                gen.writeStartObject();
                gen.writeStringField("Username", value.userName);
                gen.writeStringField("First Name", value.firstName);
                gen.writeStringField("Last Name", value.lastName);
                gen.writeStringField("Email", value.email);
                gen.writeStringField("Phone", value.phoneNumber);
                String status =null;
                if ("A".equals(value.status))
                    status = "Active";
                else if ("I".equals(value.status))
                    status = "Inactive";
                else if ("L".equals(value.status))
                    status = "Locked";
                gen.writeStringField("Status", status);
                gen.writeStringField("Role", value.role.getName());
                gen.writeEndObject();
            }
        };
    }




}
