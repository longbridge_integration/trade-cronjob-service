package cronjobservice.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import cronjobservice.utils.PrettySerializer;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fortune on 7/20/2017.
 */
public class CorporateRequestDTO implements PrettySerializer {

    @JsonProperty("DT_RowId")
    private Long id;
    private int version;
    private String corporateType;
    @NotEmpty(message = "corporateName")
    private String corporateName;
    @NotEmpty(message = "corporateId")
    private String corporateId;
    private String bvn;
    private String taxId;
    private String status;
    private String rcNumber;
    private String address;
    @NotEmpty(message = "customerId")
    private String customerId;
    private String customerName;
    private String email;
    private String phoneNumber;
    private String createdOn;
    private Long customerSegmentId;
    private List<AuthorizerLevelDTO> authorizers = new ArrayList<>();
    private List<CorporateUserDTO> corporateUsers = new ArrayList<>();
    private List<AccountDTO> accounts = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCorporateType() {
        return corporateType;
    }

    public void setCorporateType(String corporateType) {
        this.corporateType = corporateType;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRcNumber() {
        return rcNumber;
    }

    public void setRcNumber(String rcNumber) {
        this.rcNumber = rcNumber;
    }

    public List<AccountDTO> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountDTO> accounts) {
        this.accounts = accounts;
    }

    public List<CorporateUserDTO> getCorporateUsers() {
        return corporateUsers;
    }

    public void setCorporateUsers(List<CorporateUserDTO> corporateUsers) {
        this.corporateUsers = corporateUsers;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }

    public Long getCustomerSegmentId() {
        return customerSegmentId;
    }

    public void setCustomerSegmentId(Long customerSegmentId) {
        this.customerSegmentId = customerSegmentId;
    }

    public List<AuthorizerLevelDTO> getAuthorizers() {
        return authorizers;
    }

    public void setAuthorizers(List<AuthorizerLevelDTO> authorizers) {
        this.authorizers = authorizers;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "CorporateRequestDTO{" +
                "id=" + id +
                ", version=" + version +
                ", corporateType='" + corporateType + '\'' +
                ", corporateName='" + corporateName + '\'' +
                ", corporateId='" + corporateId + '\'' +
                ", bvn='" + bvn + '\'' +
                ", status='" + status + '\'' +
                ", rcNumber='" + rcNumber + '\'' +
                ", customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", createdOn='" + createdOn + '\'' +
                ", corporateUsers=" + corporateUsers +
                ", accounts=" + accounts +
                ", address=" + address +
                '}';
    }


    @Override
    @JsonIgnore
    public JsonSerializer<CorporateRequestDTO> getSerializer() {
        return new JsonSerializer<CorporateRequestDTO>() {
            @Override
            public void serialize(CorporateRequestDTO value, JsonGenerator gen, SerializerProvider serializers)
                    throws IOException {
                gen.writeStartObject();
                gen.writeStringField("CIFID", value.customerId);
                gen.writeStringField("Corporate Type", value.corporateType);
                gen.writeStringField("Corporate Name", value.corporateName);
                gen.writeStringField("Unique Corporate ID", value.corporateId);

                gen.writeObjectFieldStart("Accounts");
                Integer count = 0;
                for (AccountDTO accountDTO : value.accounts) {
                    gen.writeObjectFieldStart((++count).toString());
                    gen.writeStringField("Account Number", accountDTO.getAccountNumber());
                    gen.writeEndObject();
                }
                gen.writeEndObject();


                gen.writeObjectFieldStart("Users");
                count = 0;
                for (CorporateUserDTO user : value.corporateUsers) {

                    gen.writeObjectFieldStart((++count).toString());
                    //gen.writeStartObject();
                    gen.writeStringField("Username", user.getUserName());
                    gen.writeStringField("First Name", user.getFirstName());
                    gen.writeStringField("Last Name", user.getLastName());
                    gen.writeStringField("Email", user.getEmail());
                    gen.writeStringField("Phone Number", user.getPhoneNumber());
                    gen.writeStringField("Role", user.getRole());
                    gen.writeEndObject();
                }
                gen.writeEndObject();
            }

        };
    }

    private String getStatusDescription(String status) {
        String description = null;
        if ("A".equals(status))
            description = "Active";
        else if ("I".equals(status))
            description = "Inactive";
        else if ("L".equals(status))
            description = "Locked";
        return description;
    }
}
