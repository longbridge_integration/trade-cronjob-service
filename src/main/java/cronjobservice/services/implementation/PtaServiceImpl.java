package cronjobservice.services.implementation;

import cronjobservice.api.*;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.context.Context;

import java.util.*;

@Service
public class PtaServiceImpl  implements PtaService{

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    PtaRepo ptaRepo;
    @Autowired
    PtaService ptaService;
    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    BankUserService bankUserService;
    @Autowired
    WorkFlowRepo workFlowRepo;
    @Autowired
    ServiceAuditRepo serviceAuditRepo;
    @Value("${tradeservice.service.uri}")
    private String URI;
    @Autowired
    RestTemplate template;
    @Autowired
    ConfigurationService configurationService;
    @Autowired
    MessageSource messageSource;
    @Autowired
    MailService mailService;
    @Autowired
    IntegrationService integrationService;
    @Autowired
    CodeRepo codeRepo;

    @Transactional
    @Override
    public List<String> submitPtaToFinacle() throws CronJobException {
        try{
            HashMap<String, String> map = new HashMap<>();
            List<String> responses = new ArrayList<>();
            HashMap<String, Date> map2 = new HashMap<>();
            List<PTA> ptas = ptaRepo.findByStatusAndSubmitFlag("A", "U");

            int ptasize = ptas.size();
            logger.info("This is the Number of PTAs Fetched from TMS {}", ptasize);

            for (PTA pta:ptas) {
                logger.info("Pta Cifid {}",pta.getCifid());
                logger.info("Pta date created {} ",pta.getCreatedOn());
                logger.info("PTA Teller mail ID {} ",pta.getPaymentInitiatedBy());


                List<CommentDTO> commentDTO = ptaService.getComments(pta);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    commentDtd.getMadeOn();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                    map2.put("madeOn", commentDtd.getMadeOn());
                });

                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
                PTADTO ptadto = ptaService.convertEntityTODTO(pta);

                String desc = "PTA_BTA PARAMETERS";
                String eventId="CHARGE EVENT ID";
                String bdcTransist = "BDC TRANSIT";
                String receviable = "BDC RECIEVABLE";
                String payable = "BDC PAYABLE";
                String chargeeventtype = "CHARGE EVENT TYPE";

                Code chargeeventId = codeRepo.findByTypeAndDescription(desc,eventId );
                Code BDCTransist = codeRepo.findByTypeAndDescription(desc,bdcTransist );
                Code BDCreceviable = codeRepo.findByTypeAndDescription(desc,receviable );
                Code BDCpayable = codeRepo.findByTypeAndDescription(desc,payable );
                Code chargetype = codeRepo.findByTypeAndDescription(desc,chargeeventtype );


                System.out.println("pta chargeeventId {} "+chargeeventId.getCode());
                System.out.println("pta BDCTransist {} "+BDCTransist.getCode());
                System.out.println("pta BDCreceviable {} "+BDCreceviable.getCode());
                System.out.println("pta BDCpayable {} "+BDCpayable.getCode());
                System.out.println("pta chargetype {} "+chargetype.getCode());

                ptadto.setEventId(chargeeventId.getCode());
                ptadto.setTransitAcct(BDCTransist.getCode());
                ptadto.setReceivableAcct(BDCreceviable.getCode());
                ptadto.setPayableAcct(BDCpayable.getCode());
                ptadto.setEventType(chargetype.getCode());


                PtaFI ptaFI = new PtaFI();
                ptaFI.setPtadto(ptadto);
                ptaFI.setComment(map.get("comment"));
                ptaFI.setAuthFirstName(bankUser.getFirstName());
                ptaFI.setAuthLastName(bankUser.getLastName());
                ptaFI.setMadeOn(map2.get("madeOn"));
                ptaFI.setSortCode(bankUser.getBranch().getSortCode());
                logger.info("The Main PTA Bank Useer{}", bankUser.getFirstName());


                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(ptadto.getRefNumber(), RequestType.PTA);
                logger.info("PTA Audit size {}", serviceAudits);
                int auditSize = serviceAudits.size();


                if(auditSize >=10) {
                    pta.setSubmitFlag("F");
                    ptaRepo.save(pta);
                    ptaService.sendPTAITSmail(pta);

                    logger.info("UPDATED PTA SUBMIT FLAG WITH F DUE TO FAILURE");
                }

                if (auditSize < 10) {

                    String uri = URI + "/api/pta/submit";
                    try {
                        String ptaSubmitResponse = template.postForObject(uri, ptaFI, String.class);
                        logger.info("PTA Submit to Finacle Success Message {} ", ptaSubmitResponse);
                        responses.add(ptaSubmitResponse);

                        ServiceAudit serviceAudit = new ServiceAudit();
                        serviceAudit.setRequestType(RequestType.PTA);
                        serviceAudit.setRequestDate(new Date());
                        serviceAudit.setTempNumber(ptadto.getRefNumber());




                        if (ptaSubmitResponse == null){
                            serviceAudit.setResponseFlag("N");
                            serviceAudit.setResponseMessage(ptaSubmitResponse);
                            serviceAuditRepo.save(serviceAudit);

                        } else if  (ptaSubmitResponse.equalsIgnoreCase("Y")) {
                            pta.setSubmitFlag("S");
                            serviceAudit.setResponseFlag("Y");
                            serviceAudit.setResponseMessage("Successful");
                            serviceAuditRepo.save(serviceAudit);
                            ptaRepo.save(pta);

                        } else if (ptaSubmitResponse.equalsIgnoreCase("S")) {
                            pta.setStatus("PV");
                            pta.setSubmitFlag("S");
                            ptaRepo.save(pta);
                        }
                        else {

                            serviceAudit.setResponseFlag("N");
                            serviceAudit.setResponseMessage(ptaSubmitResponse);
                            serviceAuditRepo.save(serviceAudit);
                            logger.error("Error response for PTA {} ", ptaSubmitResponse);
                        }
                    }catch(Exception e){
                        logger.info("PTA Submit To Finacle Error {} ",e);
                    }
                }

            }
            return responses;
        }catch(Exception e){
            logger.info("Error submitting PTA to Finacle {} ",e);
            e.printStackTrace();
            throw  new CronJobException();
        }
    }

    public void getPtaSubmitResponse (){

        List<PtaDetails> ptaDetails = integrationService.getPtaSubmitResponse();

        try{

            for (PtaDetails ptaDetail : ptaDetails) {
                logger.info("The PTA Tran id abt getting its Response {} and ref num {}", ptaDetail.getTranId(),ptaDetail.getRefNumber());
                if (null==ptaDetail.getTranId()) {
                    logger.info("The PTA Finacle Tran ID Does Not Exist");
                    continue;
                }
                PTA pta = modelMapper.map(ptaDetail, PTA.class);
                PTADTO ptadto = ptaService.convertEntityTODTOFinacle(pta);
//                CorporateUser corporateUser = corporateUserRepo.findFirstByCustomerId(cifId);
                PTA temp = ptaRepo.findByRefNumber(ptadto.getRefNumber());


                if(null != temp) {

                    Boolean response = ptaService.updatePtaFromFinacle(temp, pta);

                    if (response) {
                        String uri = URI + "/api/pta/updatePtaFlag";
                        try {
                            template.postForObject(uri, ptadto, String.class);
                            logger.info("PTA Update Flg was successful");
                        } catch (Exception ex) {
                            logger.error("Exception occurred getting response for PTA {}", ex);
                            ex.printStackTrace();
                        }
                    }
                }else {
                    logger.info("pta tran id does not exist");
                }
//                else {
//                    logger.info("PTA Tran ID not Saved");
//                }

            }
        }catch (Exception e){
            logger.info("Error getting PTA Response from FInacle {} ",e);
            e.printStackTrace();
            throw new CronJobException();
        }
    }

    @Override
    public Boolean sendPTAITSmail(PTA pta) {
        try {
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "PTA";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(pta.getRefNumber(),RequestType.PTA);
            Context context = new Context();
            context.setVariable("name", pta.getAppName());
            context.setVariable("formNumber", pta.getRefNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", DateFormatter.format(new Date()));
            context.setVariable("customerId", pta.getCifid());
            context.setVariable("username", pta.getInitiatedBy());
            context.setVariable("error", serviceAudit.getResponseMessage());
            context.setVariable("code", "PTA");
            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
            Email email = new Email.Builder()
                    .setRecipients(allMails)
                    .setSubject(subject)
                    .setTemplate("mail/finacleSubmission")
                    .build();

            mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("guarantee.add.success", null, locale));
        }
    }


    @Transactional
    @Override
    public Boolean updatePtaFromFinacle(PTA tmspta, PTA finpta) throws CronJobException {
        try {
            logger.info("PTA Tran Id from Finacle {} ",finpta.getTranId());
//            logger.info("PTA TMS Tran id {} ",tmspta.getTranId());
//            tmspta.setRefNumber(finpta.getRefNumber());
            tmspta.setTranId(finpta.getTranId());
            ptaRepo.save(tmspta);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

    @Override
    public List<CommentDTO> getComments(PTA pta) throws CronJobException {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        {
            commentDTOList=convertEntitiestoDTOs(pta.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus()!="A");


        }
        return  commentDTOList;
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS)
    {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        for(Comments comments:commentDTOS)
        {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return  commentDTOList;
    }

    private CommentDTO convertEntityToDTO(Comments comments)
    {
        WorkFlowStates workFlowStates= workFlowStatesRepo.findByTypeAndCode("PTA",comments.getStatus());
        if(workFlowStates!=null)
        {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments,CommentDTO.class);
    }

    @Override
    public PTADTO convertEntityTODTO(PTA pta) {
        PTADTO ptadto = modelMapper.map(pta, PTADTO.class);

////        formNxpDTO.setProformaInvoiceDate(DateFormatter.format(formNxp.getProformaDate()));
        if (pta.getStatus() != null && !(pta.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(ptadto.getWorkFlow().getCodeType(), ptadto.getStatus());
            ptadto.setStatusDesc(workFlowStates.getDescription());
            ptadto.setWorkFlowId(pta.getWorkFlow().getId());
        }
        if (pta.getStatus().equals("P")) {
            ptadto.setStatusDesc("Saved");
        }
//        if (pta.getDateCreated() != null) {
//            ptadto.setConvert(bta.getDateCreated().toString());
//        }

//        if (bta.getFormNum() == null) {
//            btadto.setFormNum("NOT REGISTERED");
//        }
        return ptadto;
    }



    @Override
    public PTADTO convertEntityTODTOFinacle(PTA pta)
    {
        PTADTO ptadto=modelMapper.map(pta , PTADTO.class);

//        formNxpDTO.setProformaInvoiceDate(DateFormatter.format(formNxp.getProformaDate()));
        if(pta.getStatus() != null && !(pta.getStatus().equals("P")) ){
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(ptadto.getWorkFlow().getCodeType(), ptadto.getStatus());
            ptadto.setStatusDesc(workFlowStates.getDescription());
            ptadto.setWorkFlowId(pta.getWorkFlow().getId());
        }

//        if(bta.getDateCreated()!=null){
//            btadto.setConvert(bta.getDateCreated().toString());
//        }

        return ptadto;
    }

    private PTA convertDTOTOEntity(PTADTO ptadto) throws CronJobException
    {
//        try {
//            formNxpDTO.setProformaDate(DateFormatter.parse(formNxpDTO.getProformaInvoiceDate()));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        //   formNxpDTO.setWorkFlowId(formNxpDTO .getWorkFlow().getId());
        WorkFlow workFlow = workFlowRepo.findOne(ptadto.getWorkFlowId());
        ptadto.setWorkFlow(workFlow);
        PTA pta = modelMapper.map(ptadto, PTA.class);

        return pta;
    }
}
