package cronjobservice.repositories;

import cronjobservice.models.CronJobJar;
import org.springframework.stereotype.Repository;

/**
 * Created by Longbridge on 7/5/2017.
 */
@Repository
public interface CronJobJarRepo extends CommonRepo<CronJobJar,Long> {
}
