package cronjobservice.jobs.remittancejobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class RemittanceSubmitJob implements Job{
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextRemittance = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextRemittance.getBean (CronJobService.class);
        logger.info("Submit remitance job about starting");

        try {
            logger.info("Submit remitance job starts");
            logger.info("Save remitance job starts");
            cronJobService.saveRunningJob(JobCategory.REMITTANCE_SUBMIT.toString(),cronJobService.getCurrentExpression(JobCategory.REMITTANCE_SUBMIT.toString()));
            logger.info("Save remitance in db ends");
            logger.info("Passing remitance details to finacle starts");
            cronJobService.submitRemittanceToFinacle();
            logger.info("Passing remitance details to finacle end");
            logger.info("Updating remitance details starts");
            cronJobService.updateRunningJob(JobCategory.REMITTANCE_SUBMIT.toString());
            logger.info("Updating remitance details ends");
        } catch (CronJobException e) {
            logger.info("remitance response job  stops because {}",e);
        }catch (Exception e){
            logger.info("remitance response job  stops because {}",e);
        }
    }
}
