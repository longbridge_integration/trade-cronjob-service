package cronjobservice.exception;

/**
 * Created by Fortune on 5/8/2017.
 */
public class CronJobException extends RuntimeException {

    public CronJobException() {
        super("Failed to perform the requested action");
    }

    public CronJobException(Throwable cause) {
        super("Failed to perform the requested action", cause);
    }

    public CronJobException(String message) {
        super(message);
    }

    public CronJobException(String message, Throwable cause) {
        super(message, cause);
    }
}
