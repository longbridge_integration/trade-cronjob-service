package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by Sylvester on 12/6/2017.
 */
@Entity
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class FormMAttachment extends AbstractEntity{

    private String documentNumber;
    private String documentDate;
    private String documentType;
    private String documentValue;
    private String documentRemark;


    @ManyToOne
    private FormM formM;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentValue() {
        return documentValue;
    }

    public void setDocumentValue(String documentValue) {
        this.documentValue = documentValue;
    }

    public String getDocumentRemark() {
        return documentRemark;
    }

    public void setDocumentRemark(String documentRemark) {
        this.documentRemark = documentRemark;
    }

    public FormM getFormM() {
        return formM;
    }

    public void setFormM(FormM formM) {
        this.formM = formM;
    }
}
