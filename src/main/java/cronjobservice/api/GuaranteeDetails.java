package cronjobservice.api;

import cronjobservice.dtos.OutGuaranteeDocDTO;

import javax.persistence.Lob;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class GuaranteeDetails {

    private String guaranteeNumber;
    private String initiatedBy;
    //Beneficiary Details
    private String beneName;
    private String beneAddress1;
    private String beneAddress2;
    private String beneState;
    private String beneCountry;
    private String beneCity;
    private String postalCode;
    private String refNo;
    //Amount Details
    private BigDecimal GuaranteeAmount;
    private Date issueOn;
    private String bankRefNo;
    //Applicant Details
    private String cifid;
    private String appName;
    private String appAddress1;
    private String appAddress2;
    private String appState;
    private String appCountry;
    private String appCity;
    private String apppostalCode;
    private String apprefNo;
    private String appBank;
    private String appBranch;
    //Bank Details
    private String adviseBank;
    private String adviseBankCode;
    private String adviseBranch;
    private String adviseBankAddress;
    //Guarantee Details
    private String purposeOfGuarantee;
    private String guaranteeClass;
    private String effectiveOn;
    private String expiryOn;
    //expiry period is montha nd date
    private String expiryPeriod;
    private String claimExpiryOn;
    private String claimPeriod;
    private String otherDetailsRefNo;
    private String otherDetailsRefDate;
    private String chargesBorneBy;
    @Lob
    private String detailsOfGuarantee;
    private String senderToReciever;
    private String remarks;
    //Rules
    private String applicableRules;
    //Counter guarantee Details
    private String linkToCounter;
    private String guaranteeNo;
    private String guaranteeExpiryD;
    private String bankCode;
    private String rateCode;
    private String guranteeStatus;
    private String branchCode;
    private String rate;
    private String tranId;

    private String expiryMonth;
    private String expiryDay;
    private String guaranteeType;
    private String sortCode;
    private String counterRate;
    private String bgApplicableSubRules; //
    private String chargeAccount;
    private String attachments;

    //new oness
    private String freeCode1;
    private String freeCode2;
    private String freeCode3;
    private String tempFormNumber;

    private String beneBankName;
    private String beneBankAddr;
    private String beneBankSwiftCode;
    private String beneBankCtry;
    private String beneBankCity;
    private String beneBankState;
    private String beneBankPoCode;
    private String beneBankIdent;//code dropdown remitance

    //Issue bank Details
    private String issueBankName;
    private String issueBankAddr;
    private String issueBankSwiftCode;
    private String issueBankCtry;
    private String issueBankState;
    private String issueBankCity;
    private String issueBankPoCode;
    private String issueBankIdent;//

    //Corresponding bank Details
    private String corrBankCode;
    private String corrBranchId;
    private String corrBankName;
    private String corrBankAddr;
    private String corrBankSwiftCode;
    private String corrBankCtry;
    private String corrBankState;
    private String corrBankCity;
    private String corrBankPoCode;
    private String corrBankIdent;//code dropdown remittance

    private String autoRenewFlg;
    private String autoRenewNextCycle;
    private String autoRenewfreqType;
    private String autoRenewfreqWeekNum;
    private String autoRenewfreqWeekDay;
    private String autoRenewfreqStartDd;
    private String autoRenewfreqhldyStat;
    private String autoRenewfreqMonths;
    private String autoRenewfreqDays;
    private String autoRenewDate;

    private String markInvocationAmt;
    private String cumMarkInvocationAmt; //cummulation
    private String cumInvocationAmt;
    private String markInvokeStatus;
    private String operativeAcct;
    private String loanAcct;
    private String invocationCrAcct;
    private String invocationRateCode;
    private String invocationRate;
    private String remitMode;
    private String bgFructAmt;

    private String eventRate;
    private String eventRateCode;
    private String eventAmtCrncy;
    private String eventAmt;
    private String chrgWaiveFlg;
    private String subType;

    //added recently
    private String regType; //stating the types of guarantee
    private String operAcct; //operative account entered by bank
    private String currency; //currency code
    private String advBankSwiftCode;
    private String adviceBankId;
    private List<OutGuaranteeDocDTO> documents;
    private boolean guidelineApproval;

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public GuaranteeDetails() {
    }


    public String getGuaranteeNumber() {
        return guaranteeNumber;
    }

    public void setGuaranteeNumber(String guaranteeNumber) {
        this.guaranteeNumber = guaranteeNumber;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public String getBeneName() {
        return beneName;
    }

    public void setBeneName(String beneName) {
        this.beneName = beneName;
    }

    public String getBeneAddress1() {
        return beneAddress1;
    }

    public void setBeneAddress1(String beneAddress1) {
        this.beneAddress1 = beneAddress1;
    }

    public String getBeneAddress2() {
        return beneAddress2;
    }

    public void setBeneAddress2(String beneAddress2) {
        this.beneAddress2 = beneAddress2;
    }

    public String getBeneState() {
        return beneState;
    }

    public void setBeneState(String beneState) {
        this.beneState = beneState;
    }

    public String getBeneCountry() {
        return beneCountry;
    }

    public void setBeneCountry(String beneCountry) {
        this.beneCountry = beneCountry;
    }

    public String getBeneCity() {
        return beneCity;
    }

    public void setBeneCity(String beneCity) {
        this.beneCity = beneCity;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public BigDecimal getGuaranteeAmount() {
        return GuaranteeAmount;
    }

    public void setGuaranteeAmount(BigDecimal guaranteeAmount) {
        GuaranteeAmount = guaranteeAmount;
    }

    public Date getIssueOn() {
        return issueOn;
    }

    public void setIssueOn(Date issueOn) {
        this.issueOn = issueOn;
    }

    public String getBankRefNo() {
        return bankRefNo;
    }

    public void setBankRefNo(String bankRefNo) {
        this.bankRefNo = bankRefNo;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppAddress1() {
        return appAddress1;
    }

    public void setAppAddress1(String appAddress1) {
        this.appAddress1 = appAddress1;
    }

    public String getAppAddress2() {
        return appAddress2;
    }

    public void setAppAddress2(String appAddress2) {
        this.appAddress2 = appAddress2;
    }

    public String getAppState() {
        return appState;
    }

    public void setAppState(String appState) {
        this.appState = appState;
    }

    public String getAppCountry() {
        return appCountry;
    }

    public void setAppCountry(String appCountry) {
        this.appCountry = appCountry;
    }

    public String getAppCity() {
        return appCity;
    }

    public void setAppCity(String appCity) {
        this.appCity = appCity;
    }

    public String getApppostalCode() {
        return apppostalCode;
    }

    public void setApppostalCode(String apppostalCode) {
        this.apppostalCode = apppostalCode;
    }

    public String getApprefNo() {
        return apprefNo;
    }

    public void setApprefNo(String apprefNo) {
        this.apprefNo = apprefNo;
    }

    public String getAppBank() {
        return appBank;
    }

    public void setAppBank(String appBank) {
        this.appBank = appBank;
    }

    public String getAppBranch() {
        return appBranch;
    }

    public void setAppBranch(String appBranch) {
        this.appBranch = appBranch;
    }

    public String getAdviseBank() {
        return adviseBank;
    }

    public void setAdviseBank(String adviseBank) {
        this.adviseBank = adviseBank;
    }

    public String getAdviseBankCode() {
        return adviseBankCode;
    }

    public void setAdviseBankCode(String adviseBankCode) {
        this.adviseBankCode = adviseBankCode;
    }

    public String getAdviseBranch() {
        return adviseBranch;
    }

    public void setAdviseBranch(String adviseBranch) {
        this.adviseBranch = adviseBranch;
    }

    public String getAdviseBankAddress() {
        return adviseBankAddress;
    }

    public void setAdviseBankAddress(String adviseBankAddress) {
        this.adviseBankAddress = adviseBankAddress;
    }

    public String getPurposeOfGuarantee() {
        return purposeOfGuarantee;
    }

    public void setPurposeOfGuarantee(String purposeOfGuarantee) {
        this.purposeOfGuarantee = purposeOfGuarantee;
    }

    public String getGuaranteeClass() {
        return guaranteeClass;
    }

    public void setGuaranteeClass(String guaranteeClass) {
        this.guaranteeClass = guaranteeClass;
    }

    public String getEffectiveOn() {
        return effectiveOn;
    }

    public void setEffectiveOn(String effectiveOn) {
        this.effectiveOn = effectiveOn;
    }

    public String getExpiryOn() {
        return expiryOn;
    }

    public void setExpiryOn(String expiryOn) {
        this.expiryOn = expiryOn;
    }

    public String getExpiryPeriod() {
        return expiryPeriod;
    }

    public void setExpiryPeriod(String expiryPeriod) {
        this.expiryPeriod = expiryPeriod;
    }

    public String getClaimExpiryOn() {
        return claimExpiryOn;
    }

    public void setClaimExpiryOn(String claimExpiryOn) {
        this.claimExpiryOn = claimExpiryOn;
    }

    public String getClaimPeriod() {
        return claimPeriod;
    }

    public void setClaimPeriod(String claimPeriod) {
        this.claimPeriod = claimPeriod;
    }

    public String getOtherDetailsRefNo() {
        return otherDetailsRefNo;
    }

    public void setOtherDetailsRefNo(String otherDetailsRefNo) {
        this.otherDetailsRefNo = otherDetailsRefNo;
    }

    public String getOtherDetailsRefDate() {
        return otherDetailsRefDate;
    }

    public void setOtherDetailsRefDate(String otherDetailsRefDate) {
        this.otherDetailsRefDate = otherDetailsRefDate;
    }

    public String getChargesBorneBy() {
        return chargesBorneBy;
    }

    public void setChargesBorneBy(String chargesBorneBy) {
        this.chargesBorneBy = chargesBorneBy;
    }

    public String getDetailsOfGuarantee() {
        return detailsOfGuarantee;
    }

    public void setDetailsOfGuarantee(String detailsOfGuarantee) {
        this.detailsOfGuarantee = detailsOfGuarantee;
    }

    public String getSenderToReciever() {
        return senderToReciever;
    }

    public void setSenderToReciever(String senderToReciever) {
        this.senderToReciever = senderToReciever;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getApplicableRules() {
        return applicableRules;
    }

    public void setApplicableRules(String applicableRules) {
        this.applicableRules = applicableRules;
    }

    public String getLinkToCounter() {
        return linkToCounter;
    }

    public void setLinkToCounter(String linkToCounter) {
        this.linkToCounter = linkToCounter;
    }

    public String getGuaranteeNo() {
        return guaranteeNo;
    }

    public void setGuaranteeNo(String guaranteeNo) {
        this.guaranteeNo = guaranteeNo;
    }

    public String getGuaranteeExpiryD() {
        return guaranteeExpiryD;
    }

    public void setGuaranteeExpiryD(String guaranteeExpiryD) {
        this.guaranteeExpiryD = guaranteeExpiryD;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getRateCode() {
        return rateCode;
    }

    public void setRateCode(String rateCode) {
        this.rateCode = rateCode;
    }

    public String getGuranteeStatus() {
        return guranteeStatus;
    }

    public void setGuranteeStatus(String guranteeStatus) {
        this.guranteeStatus = guranteeStatus;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryDay() {
        return expiryDay;
    }

    public void setExpiryDay(String expiryDay) {
        this.expiryDay = expiryDay;
    }

    public String getGuaranteeType() {
        return guaranteeType;
    }

    public void setGuaranteeType(String guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public String getCounterRate() {
        return counterRate;
    }

    public void setCounterRate(String counterRate) {
        this.counterRate = counterRate;
    }

    public String getBgApplicableSubRules() {
        return bgApplicableSubRules;
    }

    public void setBgApplicableSubRules(String bgApplicableSubRules) {
        this.bgApplicableSubRules = bgApplicableSubRules;
    }

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getFreeCode1() {
        return freeCode1;
    }

    public void setFreeCode1(String freeCode1) {
        this.freeCode1 = freeCode1;
    }

    public String getFreeCode2() {
        return freeCode2;
    }

    public void setFreeCode2(String freeCode2) {
        this.freeCode2 = freeCode2;
    }

    public String getFreeCode3() {
        return freeCode3;
    }

    public void setFreeCode3(String freeCode3) {
        this.freeCode3 = freeCode3;
    }

    public String getTempFormNumber() {
        return tempFormNumber;
    }

    public void setTempFormNumber(String tempFormNumber) {
        this.tempFormNumber = tempFormNumber;
    }

    public String getBeneBankName() {
        return beneBankName;
    }

    public void setBeneBankName(String beneBankName) {
        this.beneBankName = beneBankName;
    }

    public String getBeneBankAddr() {
        return beneBankAddr;
    }

    public void setBeneBankAddr(String beneBankAddr) {
        this.beneBankAddr = beneBankAddr;
    }

    public String getBeneBankSwiftCode() {
        return beneBankSwiftCode;
    }

    public void setBeneBankSwiftCode(String beneBankSwiftCode) {
        this.beneBankSwiftCode = beneBankSwiftCode;
    }

    public String getBeneBankCtry() {
        return beneBankCtry;
    }

    public void setBeneBankCtry(String beneBankCtry) {
        this.beneBankCtry = beneBankCtry;
    }

    public String getBeneBankCity() {
        return beneBankCity;
    }

    public void setBeneBankCity(String beneBankCity) {
        this.beneBankCity = beneBankCity;
    }

    public String getBeneBankState() {
        return beneBankState;
    }

    public void setBeneBankState(String beneBankState) {
        this.beneBankState = beneBankState;
    }

    public String getBeneBankPoCode() {
        return beneBankPoCode;
    }

    public void setBeneBankPoCode(String beneBankPoCode) {
        this.beneBankPoCode = beneBankPoCode;
    }

    public String getBeneBankIdent() {
        return beneBankIdent;
    }

    public void setBeneBankIdent(String beneBankIdent) {
        this.beneBankIdent = beneBankIdent;
    }

    public String getIssueBankName() {
        return issueBankName;
    }

    public void setIssueBankName(String issueBankName) {
        this.issueBankName = issueBankName;
    }

    public String getIssueBankAddr() {
        return issueBankAddr;
    }

    public void setIssueBankAddr(String issueBankAddr) {
        this.issueBankAddr = issueBankAddr;
    }

    public String getIssueBankSwiftCode() {
        return issueBankSwiftCode;
    }

    public void setIssueBankSwiftCode(String issueBankSwiftCode) {
        this.issueBankSwiftCode = issueBankSwiftCode;
    }

    public String getIssueBankCtry() {
        return issueBankCtry;
    }

    public void setIssueBankCtry(String issueBankCtry) {
        this.issueBankCtry = issueBankCtry;
    }

    public String getIssueBankState() {
        return issueBankState;
    }

    public void setIssueBankState(String issueBankState) {
        this.issueBankState = issueBankState;
    }

    public String getIssueBankCity() {
        return issueBankCity;
    }

    public void setIssueBankCity(String issueBankCity) {
        this.issueBankCity = issueBankCity;
    }

    public String getIssueBankPoCode() {
        return issueBankPoCode;
    }

    public void setIssueBankPoCode(String issueBankPoCode) {
        this.issueBankPoCode = issueBankPoCode;
    }

    public String getIssueBankIdent() {
        return issueBankIdent;
    }

    public void setIssueBankIdent(String issueBankIdent) {
        this.issueBankIdent = issueBankIdent;
    }

    public String getCorrBankCode() {
        return corrBankCode;
    }

    public void setCorrBankCode(String corrBankCode) {
        this.corrBankCode = corrBankCode;
    }

    public String getCorrBranchId() {
        return corrBranchId;
    }

    public void setCorrBranchId(String corrBranchId) {
        this.corrBranchId = corrBranchId;
    }

    public String getCorrBankName() {
        return corrBankName;
    }

    public void setCorrBankName(String corrBankName) {
        this.corrBankName = corrBankName;
    }

    public String getCorrBankAddr() {
        return corrBankAddr;
    }

    public void setCorrBankAddr(String corrBankAddr) {
        this.corrBankAddr = corrBankAddr;
    }

    public String getCorrBankSwiftCode() {
        return corrBankSwiftCode;
    }

    public void setCorrBankSwiftCode(String corrBankSwiftCode) {
        this.corrBankSwiftCode = corrBankSwiftCode;
    }

    public String getCorrBankCtry() {
        return corrBankCtry;
    }

    public void setCorrBankCtry(String corrBankCtry) {
        this.corrBankCtry = corrBankCtry;
    }

    public String getCorrBankState() {
        return corrBankState;
    }

    public void setCorrBankState(String corrBankState) {
        this.corrBankState = corrBankState;
    }

    public String getCorrBankCity() {
        return corrBankCity;
    }

    public void setCorrBankCity(String corrBankCity) {
        this.corrBankCity = corrBankCity;
    }

    public String getCorrBankPoCode() {
        return corrBankPoCode;
    }

    public void setCorrBankPoCode(String corrBankPoCode) {
        this.corrBankPoCode = corrBankPoCode;
    }

    public String getCorrBankIdent() {
        return corrBankIdent;
    }

    public void setCorrBankIdent(String corrBankIdent) {
        this.corrBankIdent = corrBankIdent;
    }

    public String getAutoRenewFlg() {
        return autoRenewFlg;
    }

    public void setAutoRenewFlg(String autoRenewFlg) {
        this.autoRenewFlg = autoRenewFlg;
    }

    public String getAutoRenewNextCycle() {
        return autoRenewNextCycle;
    }

    public void setAutoRenewNextCycle(String autoRenewNextCycle) {
        this.autoRenewNextCycle = autoRenewNextCycle;
    }

    public String getAutoRenewfreqType() {
        return autoRenewfreqType;
    }

    public void setAutoRenewfreqType(String autoRenewfreqType) {
        this.autoRenewfreqType = autoRenewfreqType;
    }

    public String getAutoRenewfreqWeekNum() {
        return autoRenewfreqWeekNum;
    }

    public void setAutoRenewfreqWeekNum(String autoRenewfreqWeekNum) {
        this.autoRenewfreqWeekNum = autoRenewfreqWeekNum;
    }

    public String getAutoRenewfreqWeekDay() {
        return autoRenewfreqWeekDay;
    }

    public void setAutoRenewfreqWeekDay(String autoRenewfreqWeekDay) {
        this.autoRenewfreqWeekDay = autoRenewfreqWeekDay;
    }

    public String getAutoRenewfreqStartDd() {
        return autoRenewfreqStartDd;
    }

    public void setAutoRenewfreqStartDd(String autoRenewfreqStartDd) {
        this.autoRenewfreqStartDd = autoRenewfreqStartDd;
    }

    public String getAutoRenewfreqhldyStat() {
        return autoRenewfreqhldyStat;
    }

    public void setAutoRenewfreqhldyStat(String autoRenewfreqhldyStat) {
        this.autoRenewfreqhldyStat = autoRenewfreqhldyStat;
    }

    public String getAutoRenewfreqMonths() {
        return autoRenewfreqMonths;
    }

    public void setAutoRenewfreqMonths(String autoRenewfreqMonths) {
        this.autoRenewfreqMonths = autoRenewfreqMonths;
    }

    public String getAutoRenewfreqDays() {
        return autoRenewfreqDays;
    }

    public void setAutoRenewfreqDays(String autoRenewfreqDays) {
        this.autoRenewfreqDays = autoRenewfreqDays;
    }

    public String getAutoRenewDate() {
        return autoRenewDate;
    }

    public void setAutoRenewDate(String autoRenewDate) {
        this.autoRenewDate = autoRenewDate;
    }

    public String getMarkInvocationAmt() {
        return markInvocationAmt;
    }

    public void setMarkInvocationAmt(String markInvocationAmt) {
        this.markInvocationAmt = markInvocationAmt;
    }

    public String getCumMarkInvocationAmt() {
        return cumMarkInvocationAmt;
    }

    public void setCumMarkInvocationAmt(String cumMarkInvocationAmt) {
        this.cumMarkInvocationAmt = cumMarkInvocationAmt;
    }

    public String getCumInvocationAmt() {
        return cumInvocationAmt;
    }

    public void setCumInvocationAmt(String cumInvocationAmt) {
        this.cumInvocationAmt = cumInvocationAmt;
    }

    public String getMarkInvokeStatus() {
        return markInvokeStatus;
    }

    public void setMarkInvokeStatus(String markInvokeStatus) {
        this.markInvokeStatus = markInvokeStatus;
    }

    public String getOperativeAcct() {
        return operativeAcct;
    }

    public void setOperativeAcct(String operativeAcct) {
        this.operativeAcct = operativeAcct;
    }

    public String getLoanAcct() {
        return loanAcct;
    }

    public void setLoanAcct(String loanAcct) {
        this.loanAcct = loanAcct;
    }

    public String getInvocationCrAcct() {
        return invocationCrAcct;
    }

    public void setInvocationCrAcct(String invocationCrAcct) {
        this.invocationCrAcct = invocationCrAcct;
    }

    public String getInvocationRateCode() {
        return invocationRateCode;
    }

    public void setInvocationRateCode(String invocationRateCode) {
        this.invocationRateCode = invocationRateCode;
    }

    public String getInvocationRate() {
        return invocationRate;
    }

    public void setInvocationRate(String invocationRate) {
        this.invocationRate = invocationRate;
    }

    public String getRemitMode() {
        return remitMode;
    }

    public void setRemitMode(String remitMode) {
        this.remitMode = remitMode;
    }

    public String getBgFructAmt() {
        return bgFructAmt;
    }

    public void setBgFructAmt(String bgFructAmt) {
        this.bgFructAmt = bgFructAmt;
    }

    public String getEventRate() {
        return eventRate;
    }

    public void setEventRate(String eventRate) {
        this.eventRate = eventRate;
    }

    public String getEventRateCode() {
        return eventRateCode;
    }

    public void setEventRateCode(String eventRateCode) {
        this.eventRateCode = eventRateCode;
    }

    public String getEventAmtCrncy() {
        return eventAmtCrncy;
    }

    public void setEventAmtCrncy(String eventAmtCrncy) {
        this.eventAmtCrncy = eventAmtCrncy;
    }

    public String getEventAmt() {
        return eventAmt;
    }

    public void setEventAmt(String eventAmt) {
        this.eventAmt = eventAmt;
    }

    public String getChrgWaiveFlg() {
        return chrgWaiveFlg;
    }

    public void setChrgWaiveFlg(String chrgWaiveFlg) {
        this.chrgWaiveFlg = chrgWaiveFlg;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public String getOperAcct() {
        return operAcct;
    }

    public void setOperAcct(String operAcct) {
        this.operAcct = operAcct;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAdvBankSwiftCode() {
        return advBankSwiftCode;
    }

    public void setAdvBankSwiftCode(String advBankSwiftCode) {
        this.advBankSwiftCode = advBankSwiftCode;
    }

    public String getAdviceBankId() {
        return adviceBankId;
    }

    public void setAdviceBankId(String adviceBankId) {
        this.adviceBankId = adviceBankId;
    }

    public List<OutGuaranteeDocDTO> getDocuments() {
        return documents;
    }

    public void setDocuments(List<OutGuaranteeDocDTO> documents) {
        this.documents = documents;
    }
}
