package cronjobservice.services;


import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.FormNxpDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.FormNxp;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by user on 9/27/2017.
 */
@Service
public interface FormNxpBankService {

    FormNxpDTO getInitiatorDetails() throws CronJobException;
    //String saveFormA(FormNxpDTO formNxpDTO) throws CronJobException;

    String saveFormNxp(FormNxpDTO formNxpDTO) throws CronJobException;

    Boolean sendFormNxpITSmail(FormNxp formNxp);

    List<CommentDTO> getComments(FormNxp formNxp) throws CronJobException;

    FormNxp convertDTOToEntity(FormNxpDTO formNxpDTO);
}
