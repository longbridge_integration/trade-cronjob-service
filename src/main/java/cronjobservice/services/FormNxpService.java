package cronjobservice.services;


import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.FormNxp;
import cronjobservice.models.FormNxpAmend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * Created by user on 9/20/2017.
 */
@Service
public interface FormNxpService {
 FormNxpDTO editFormM(Long id) throws CronJobException;

 FormNxpDTO getInitiatorDetails() throws CronJobException;

// Page<FormNxpDTO> getFormNxpsForBankUser(Pageable pageable);
//
// Page<FormNxpDTO> getOpenFormNxpsForBankUser(Pageable pageable);
//
// String addForm(FormNxpDTO formNxpDTO) throws CronJobException;
 Boolean addFormFromFinacle(FormNxpDTO formNxpDTO) throws CronJobException;
 FormNxpDTO getFormNxp(Long id) throws CronJobException;
 String updateFormNxp(FormNxpDTO formNxpDTO) throws CronJobException;

 Boolean updateFormNxpFromFinacle(FormNxp tmsformnxp, FormNxp finformnxp) throws CronJobException;
 Page<FormNxpDTO> getBankFormNxps(Pageable pageable);

 FormHolder createFormHolder(FormNxpDTO formNxpDTO, String name) throws CronJobException;

 FormHolder executeAction(FormNxpDTO formNxpDTO, String name) throws CronJobException;

 Page<FormNxpDTO> getFormNxps(Pageable pageDetails);
 String deleteForm(Long id) throws CronJobException;
 List<FormItemsDTO> getFormItems(Long formId);
 Page<FormNxp> getFormNxpDetails(Pageable pageable) throws CronJobException;
 // Page<ExportLc> getExportLc(Pageable pageable) throws CronJobException;
 FormNxpDTO convertEntityTODTO(FormNxp formNxp);
 FormNxpDTO convertEntityTODTOFinacle(FormNxp formNxp);
 Page<FormNxp> getApprovedFormNxpDetails(Pageable pageable) throws CronJobException;
 FormNxp getFormNum(Long id) throws CronJobException;
 FormNxp getDetForLc(String formNum) throws CronJobException;

// public List<Map<String,Object>> getFirstOfPage(long id) throws CronJobException;
// public List<Map<String,Object>> getListOfPage(Long id) throws CronJobException;

 String getTotalAmt(String cifId) throws CronJobException;
 Page<FormNxpDTO> getOpenFormNxps(Pageable pageable) throws CronJobException;
 Page<FormNxpDTO> geBankOpenFormNxps(Pageable pageable) throws CronJobException;
// FormNxp getDetailForExportLc(String formNum) throws CronJobException;

 long getTotalExpiredProformaFormNxp() throws CronJobException;

 Page<FormNxpDTO> getListExpiredFormNxp(Pageable pageDetails) ;


// String addAttachments(MultipartFile[] files);


 Page<FormNxpDTO> getAllFormNxps(Pageable pageDetails);

 String addAttachments(MultipartFile file, String formaNumber, String documentType);

// String amendFormNxp(FormNxpDTO formNxpDTO) throws CronJobException;
//
// FormNxpAmend convertFormNxpDTOToAmendEntity(FormNxpDTO formNxpDTO);
//
// boolean checkIfAmendable(Long id);
//
// Page<FormNxpDTO> getFormMAmendments(String formNumber, Pageable pageDetails);

// List<FormNxpDTO> convertAmendEntitiesToFormNxpDTOs(Iterable<FormNxpAmend> formNxpAmends);

// FormNxpDTO convertAmendEntityToFormNxpDTO(FormNxpAmend formNxpAmend);

 FormNxpDTO getFormNxpByFormNumber(String formNumber) throws CronJobException;

 List<CommentDTO> getComments(Long id) throws CronJobException;

 List<CommentDTO> getComments(FormNxp formNxp) throws CronJobException;

 List<AccountDTO> getCustomerNairaAccounts() throws CronJobException;

 List<AccountDTO> getCustomerDomAccounts() throws CronJobException;

 List<AccountDTO> getCustomerDomAccountsWithScheme()throws CronJobException;

 AccountDTO getAccountByAccountNumber(String accountNumber);

 FormNxpDTO getFormNxpForView(Long id) throws CronJobException;

 List<AccountDTO>  getCustomerAccounts() throws CronJobException;

 BankCustomerDTO getBankCustomerByCustomerId(String customerId) throws CronJobException;
}
