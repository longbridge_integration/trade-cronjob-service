package cronjobservice.repositories;

import cronjobservice.models.FormMAllocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by JUDE on 1/26/2018.
 */
public interface FormMAllocationRepo extends JpaRepository<FormMAllocation,Long> {


         List<FormMAllocation> findByFormmNumber(String formmNumber);


    FormMAllocation findByFormmNumberAndDealId(String formmNumber, String dealId);

    @Query("select  sum (s.allocationAmount)  from FormMAllocation s  where  s.formmNumber=:formmNumber")
    BigDecimal sumAllocationAmountInFormmAllocation(@Param("formmNumber") String formmNumber);

    List<FormMAllocation> findByformmNumber(String formMNumber);

    @Query("select  sum (s.allocationAmount)  from FormMAllocation s  where  s.dealId=:dealId")
    BigDecimal sumAllocationAmountInFormaAllocation(@Param("dealId") String dealId);

    FormMAllocation findByDealId(String s);

}
