package cronjobservice.models;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Showboy on 28/07/2017.
 */
public enum TokenType {

    HARD("HARD"),
    SOFT("SOFT");

    private String description;

    TokenType(String description){
        this.description = description;
    }

    public static List<TokenType> getTokenTypes(){
        return Arrays.asList(TokenType.values());
    }

}