package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

/**
 * Created by Showboy on 07/09/2017.
 */
@Entity
@Audited(withModifiedFlag=true)
@Where(clause ="del_Flag='N'" )
public class WorkFlowStates extends AbstractEntity {

    private String code;
    private String description;
    private String action;
    private String type;
    private boolean enableNotification;
    private String notificationMessage;
    private String createdOn;
    @Enumerated(value = EnumType.STRING)
    private UserType userType;
    @OneToOne
    private Permission permission;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEnableNotification() {
        return enableNotification;
    }

    public void setEnableNotification(boolean enableNotification) {
        this.enableNotification = enableNotification;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    @Override
    public String toString() {
        return "WorkFlowStates{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", action='" + action + '\'' +
                ", type='" + type + '\'' +
                ", enableNotification=" + enableNotification +
                ", notificationMessage='" + notificationMessage + '\'' +
                ", createdOn='" + createdOn + '\'' +
                ", permission=" + permission +
                '}';
    }
}
