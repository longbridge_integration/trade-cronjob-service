package cronjobservice.models;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

@Entity
@Where(clause = "del_Flag='N'")
@Audited(withModifiedFlag = true)
public class FormQ extends Process{

    private String customerName;
    private String bvn;
    private String address;
    private String town;
    private String state;
    private String phone;
    private String email;
    private String postCode;
    private String country;
    private String bidRateCode;
    private boolean guidelineApproval;

    private String amountInWords;
    private String formNumber;//ref No
    private String tempFormNumber;//trade app no
    private String applicationNumber;//fin app no
    private BigDecimal utilization;
    private BigDecimal allocatedAmount;
    private String transactionDebitAmt;
    private String allocationAccount;
    private String allocationCurrencyCode;//fetch currency code but bank editable
    private BigDecimal amount;
    private BigDecimal approvalAmount;//amount approved - bank
    private String approvalCurrency;//currency of approved - bank
    private String approverName;//Name of approver - bank

    private String transferAccounts;

    private String operationCode;//bank
    private String beneficiaryAddress;
    private String beneficiaryCountry;
    private String beneficiaryPhone;
    private String beneficiaryRegistration;
    private String beneficiaryTown;
    private String beneficiaryState;
    private String beneficiaryName;
    private String beneficiaryAccount;
    private String beneficiaryPostCode;
    private String interBankName;
    private String interBankAddr;
    private String interBankSwiftCode;
    private String interBankCtry;
    private String interBankState;
    private String interBankCity;
    private String interBankPoCode;
    private String interBankIdent;//code dropdown remittance

    private String remitanceId;
    private String remittanceType;//Codes
    private String messageType;//bank

    private String realizationAcct;

    private String corrBankCode;
    private String corrBranchId;
    private String corrBankName;
    private String corrBankAddr;
    private String corrBankSwiftCode;
    private String corrBankCtry;
    private String corrBankState;
    private String corrBankCity;
    private String corrBankPoCode;
    private String corrBankIdent;//code dropdown remittance
    private String corrNostroAcct;
    private String beneBankName;
    private String beneBankAddr;
    private String beneBankSwiftCode;
    private String beneBankCtry;
    private String beneBankCity;
    private String beneBankState;
    private String beneBankPoCode;
    private String beneBankIdent;//code dropdown remitance

    private String initiatedBy;
    private String userType;
    private String purposeOfPayment;

    private String currencyCode;
    private String exchangeRate;
    private String paymentMode;

    private String paymentCode;
    private String description;
    private String numOfEmployees;

    private Date expiryDate;

    private String offShoreChargeDoneBy;
    private BigDecimal offShoreCharges;
    @Lob
    private String senderToReceiverInfos;
    @Lob
    private String remittanceInfos;

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Collection<FormQDocument> documents;
    @ManyToOne
    private Branch docSubBranch;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBidRateCode() {
        return bidRateCode;
    }

    public void setBidRateCode(String bidRateCode) {
        this.bidRateCode = bidRateCode;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public String getAmountInWords() {
        return amountInWords;
    }

    public void setAmountInWords(String amountInWords) {
        this.amountInWords = amountInWords;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public String getTempFormNumber() {
        return tempFormNumber;
    }

    public void setTempFormNumber(String tempFormNumber) {
        this.tempFormNumber = tempFormNumber;
    }

    public String getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public BigDecimal getUtilization() {
        return utilization;
    }

    public void setUtilization(BigDecimal utilization) {
        this.utilization = utilization;
    }

    public BigDecimal getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(BigDecimal allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public String getTransactionDebitAmt() {
        return transactionDebitAmt;
    }

    public void setTransactionDebitAmt(String transactionDebitAmt) {
        this.transactionDebitAmt = transactionDebitAmt;
    }

    public String getAllocationAccount() {
        return allocationAccount;
    }

    public void setAllocationAccount(String allocationAccount) {
        this.allocationAccount = allocationAccount;
    }

    public String getAllocationCurrencyCode() {
        return allocationCurrencyCode;
    }

    public void setAllocationCurrencyCode(String allocationCurrencyCode) {
        this.allocationCurrencyCode = allocationCurrencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getApprovalAmount() {
        return approvalAmount;
    }

    public void setApprovalAmount(BigDecimal approvalAmount) {
        this.approvalAmount = approvalAmount;
    }

    public String getApprovalCurrency() {
        return approvalCurrency;
    }

    public void setApprovalCurrency(String approvalCurrency) {
        this.approvalCurrency = approvalCurrency;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public String getTransferAccounts() {
        return transferAccounts;
    }

    public void setTransferAccounts(String transferAccounts) {
        this.transferAccounts = transferAccounts;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getBeneficiaryAddress() {
        return beneficiaryAddress;
    }

    public void setBeneficiaryAddress(String beneficiaryAddress) {
        this.beneficiaryAddress = beneficiaryAddress;
    }

    public String getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    public void setBeneficiaryCountry(String beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
    }

    public String getBeneficiaryPhone() {
        return beneficiaryPhone;
    }

    public void setBeneficiaryPhone(String beneficiaryPhone) {
        this.beneficiaryPhone = beneficiaryPhone;
    }

    public String getBeneficiaryRegistration() {
        return beneficiaryRegistration;
    }

    public void setBeneficiaryRegistration(String beneficiaryRegistration) {
        this.beneficiaryRegistration = beneficiaryRegistration;
    }

    public String getBeneficiaryTown() {
        return beneficiaryTown;
    }

    public void setBeneficiaryTown(String beneficiaryTown) {
        this.beneficiaryTown = beneficiaryTown;
    }

    public String getBeneficiaryState() {
        return beneficiaryState;
    }

    public void setBeneficiaryState(String beneficiaryState) {
        this.beneficiaryState = beneficiaryState;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryAccount() {
        return beneficiaryAccount;
    }

    public void setBeneficiaryAccount(String beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
    }

    public String getBeneficiaryPostCode() {
        return beneficiaryPostCode;
    }

    public void setBeneficiaryPostCode(String beneficiaryPostCode) {
        this.beneficiaryPostCode = beneficiaryPostCode;
    }

    public String getInterBankName() {
        return interBankName;
    }

    public void setInterBankName(String interBankName) {
        this.interBankName = interBankName;
    }

    public String getInterBankAddr() {
        return interBankAddr;
    }

    public void setInterBankAddr(String interBankAddr) {
        this.interBankAddr = interBankAddr;
    }

    public String getInterBankSwiftCode() {
        return interBankSwiftCode;
    }

    public void setInterBankSwiftCode(String interBankSwiftCode) {
        this.interBankSwiftCode = interBankSwiftCode;
    }

    public String getInterBankCtry() {
        return interBankCtry;
    }

    public void setInterBankCtry(String interBankCtry) {
        this.interBankCtry = interBankCtry;
    }

    public String getInterBankState() {
        return interBankState;
    }

    public void setInterBankState(String interBankState) {
        this.interBankState = interBankState;
    }

    public String getInterBankCity() {
        return interBankCity;
    }

    public void setInterBankCity(String interBankCity) {
        this.interBankCity = interBankCity;
    }

    public String getInterBankPoCode() {
        return interBankPoCode;
    }

    public void setInterBankPoCode(String interBankPoCode) {
        this.interBankPoCode = interBankPoCode;
    }

    public String getInterBankIdent() {
        return interBankIdent;
    }

    public void setInterBankIdent(String interBankIdent) {
        this.interBankIdent = interBankIdent;
    }

    public String getRemitanceId() {
        return remitanceId;
    }

    public void setRemitanceId(String remitanceId) {
        this.remitanceId = remitanceId;
    }

    public String getRemittanceType() {
        return remittanceType;
    }

    public void setRemittanceType(String remittanceType) {
        this.remittanceType = remittanceType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getRealizationAcct() {
        return realizationAcct;
    }

    public void setRealizationAcct(String realizationAcct) {
        this.realizationAcct = realizationAcct;
    }

    public String getCorrBankCode() {
        return corrBankCode;
    }

    public void setCorrBankCode(String corrBankCode) {
        this.corrBankCode = corrBankCode;
    }

    public String getCorrBranchId() {
        return corrBranchId;
    }

    public void setCorrBranchId(String corrBranchId) {
        this.corrBranchId = corrBranchId;
    }

    public String getCorrBankName() {
        return corrBankName;
    }

    public void setCorrBankName(String corrBankName) {
        this.corrBankName = corrBankName;
    }

    public String getCorrBankAddr() {
        return corrBankAddr;
    }

    public void setCorrBankAddr(String corrBankAddr) {
        this.corrBankAddr = corrBankAddr;
    }

    public String getCorrBankSwiftCode() {
        return corrBankSwiftCode;
    }

    public void setCorrBankSwiftCode(String corrBankSwiftCode) {
        this.corrBankSwiftCode = corrBankSwiftCode;
    }

    public String getCorrBankCtry() {
        return corrBankCtry;
    }

    public void setCorrBankCtry(String corrBankCtry) {
        this.corrBankCtry = corrBankCtry;
    }

    public String getCorrBankState() {
        return corrBankState;
    }

    public void setCorrBankState(String corrBankState) {
        this.corrBankState = corrBankState;
    }

    public String getCorrBankCity() {
        return corrBankCity;
    }

    public void setCorrBankCity(String corrBankCity) {
        this.corrBankCity = corrBankCity;
    }

    public String getCorrBankPoCode() {
        return corrBankPoCode;
    }

    public void setCorrBankPoCode(String corrBankPoCode) {
        this.corrBankPoCode = corrBankPoCode;
    }

    public String getCorrBankIdent() {
        return corrBankIdent;
    }

    public void setCorrBankIdent(String corrBankIdent) {
        this.corrBankIdent = corrBankIdent;
    }

    public String getCorrNostroAcct() {
        return corrNostroAcct;
    }

    public void setCorrNostroAcct(String corrNostroAcct) {
        this.corrNostroAcct = corrNostroAcct;
    }

    public String getBeneBankName() {
        return beneBankName;
    }

    public void setBeneBankName(String beneBankName) {
        this.beneBankName = beneBankName;
    }

    public String getBeneBankAddr() {
        return beneBankAddr;
    }

    public void setBeneBankAddr(String beneBankAddr) {
        this.beneBankAddr = beneBankAddr;
    }

    public String getBeneBankSwiftCode() {
        return beneBankSwiftCode;
    }

    public void setBeneBankSwiftCode(String beneBankSwiftCode) {
        this.beneBankSwiftCode = beneBankSwiftCode;
    }

    public String getBeneBankCtry() {
        return beneBankCtry;
    }

    public void setBeneBankCtry(String beneBankCtry) {
        this.beneBankCtry = beneBankCtry;
    }

    public String getBeneBankCity() {
        return beneBankCity;
    }

    public void setBeneBankCity(String beneBankCity) {
        this.beneBankCity = beneBankCity;
    }

    public String getBeneBankState() {
        return beneBankState;
    }

    public void setBeneBankState(String beneBankState) {
        this.beneBankState = beneBankState;
    }

    public String getBeneBankPoCode() {
        return beneBankPoCode;
    }

    public void setBeneBankPoCode(String beneBankPoCode) {
        this.beneBankPoCode = beneBankPoCode;
    }

    public String getBeneBankIdent() {
        return beneBankIdent;
    }

    public void setBeneBankIdent(String beneBankIdent) {
        this.beneBankIdent = beneBankIdent;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getPurposeOfPayment() {
        return purposeOfPayment;
    }

    public void setPurposeOfPayment(String purposeOfPayment) {
        this.purposeOfPayment = purposeOfPayment;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumOfEmployees() {
        return numOfEmployees;
    }

    public void setNumOfEmployees(String numOfEmployees) {
        this.numOfEmployees = numOfEmployees;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getOffShoreChargeDoneBy() {
        return offShoreChargeDoneBy;
    }

    public void setOffShoreChargeDoneBy(String offShoreChargeDoneBy) {
        this.offShoreChargeDoneBy = offShoreChargeDoneBy;
    }

    public BigDecimal getOffShoreCharges() {
        return offShoreCharges;
    }

    public void setOffShoreCharges(BigDecimal offShoreCharges) {
        this.offShoreCharges = offShoreCharges;
    }

    public String getSenderToReceiverInfos() {
        return senderToReceiverInfos;
    }

    public void setSenderToReceiverInfos(String senderToReceiverInfos) {
        this.senderToReceiverInfos = senderToReceiverInfos;
    }

    public String getRemittanceInfos() {
        return remittanceInfos;
    }

    public void setRemittanceInfos(String remittanceInfos) {
        this.remittanceInfos = remittanceInfos;
    }

    public Collection<FormQDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(Collection<FormQDocument> documents) {
        this.documents = documents;
    }

    public Branch getDocSubBranch() {
        return docSubBranch;
    }

    public void setDocSubBranch(Branch docSubBranch) {
        this.docSubBranch = docSubBranch;
    }
}
