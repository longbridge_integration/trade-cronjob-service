package cronjobservice.repositories;

import cronjobservice.models.LcConfirmation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LcConfirmationRepo extends CommonRepo<LcConfirmation,Long>{

    Long countByCifid(String cifid);

    LcConfirmation getByLcNumber(String lcNumber);

    List<LcConfirmation> findByLcNumber(String lcNumber);

    Page<LcConfirmation> findByLcNumberAndCifid(String lcNumber, String cifid, Pageable pageable);

    @Query("select s from LcConfirmation s where s.status!=:openstatus")
    Page<LcConfirmation> findStatus(@Param("openstatus") String status, Pageable pageable);

    @Query("select s from LcConfirmation s where s.status=:openstatus")
    Page<LcConfirmation> findOpenStatus(@Param("openstatus") String status, Pageable pageable);

    Page<LcConfirmation> findByStatusIsNull(Pageable pageable);

    Page<LcConfirmation> findByStatusNotNull(Pageable pageable);

    Page<LcConfirmation> findByCifidAndStatusNotNull(String cif, Pageable pageable);

    @Query("select distinct s from LcConfirmation s where s.cifid=:cifid and s.status=:submitted")
    Page<LcConfirmation> findByCifidAndStatusS(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    @Query("select distinct s from LcConfirmation s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<LcConfirmation> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);

    @Query("select distinct s from LcConfirmation s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<LcConfirmation> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    Page<LcConfirmation> findByCifid(String cifid, Pageable pageable);

    @Query("select distinct s from LcConfirmation s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<LcConfirmation> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    @Query("select distinct s from LcConfirmation s where s.cifid=:cifid  and  s.status is not null  and   s.status!=:submitted and s.status!=:saved")
    Page<LcConfirmation> findByClosedCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, @Param("saved") String saved, Pageable pageable);

}
