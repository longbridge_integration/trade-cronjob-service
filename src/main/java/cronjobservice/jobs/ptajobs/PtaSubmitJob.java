package cronjobservice.jobs.ptajobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.services.PtaService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.batch.BatchProperties;
import org.springframework.context.ApplicationContext;
import org.quartz.Job;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class PtaSubmitJob implements Job{
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextLc = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextLc.getBean (CronJobService.class);
        PtaService ptaService = contextLc.getBean (PtaService.class);
//        System.out.println("five minute job runing");
        try {
            logger.info("PTA Job Starting");
//            cronJobService.saveRunningJob(JobCategory.SUBMIT_PTA.toString(),cronJobService.getCurrentExpression(JobCategory.SUBMIT_PTA.toString()));

            ptaService.submitPtaToFinacle();
//            cronJobService.updateRunningJob(JobCategory.SUBMIT_PTA.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
