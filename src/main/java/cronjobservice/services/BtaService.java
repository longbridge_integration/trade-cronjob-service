package cronjobservice.services;

import cronjobservice.dtos.BTADTO;
import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.FormNxpDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.BTA;
import cronjobservice.models.FormNxp;
import cronjobservice.models.Remittance;

import java.util.List;

public interface BtaService {

    List<String> submitBtaToFinacle() throws CronJobException;
    void getBtaSubmitResponse();
    Boolean updateBtaFromFinacle(BTA tmsbta, BTA finbta) throws CronJobException;

    Boolean sendBTAITSmail(BTA bta);

    BTADTO convertEntityTODTO(BTA bta);
    BTADTO convertEntityTODTOFinacle(BTA bta);
    List<CommentDTO> getComments(BTA bta);


}
