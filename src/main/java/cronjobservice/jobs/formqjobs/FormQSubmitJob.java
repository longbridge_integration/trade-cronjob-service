package cronjobservice.jobs.formqjobs;


import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.BtaService;
import cronjobservice.services.CronJobService;
import cronjobservice.services.FormQService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class FormQSubmitJob implements Job{

    @Autowired
    CronJobService cronJobService;
    @Autowired
    FormQService formQService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextLc = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextLc.getBean (CronJobService.class);
        FormQService formQService = contextLc.getBean (FormQService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.FORMQ_SUBMIT.toString(),cronJobService.getCurrentExpression(JobCategory.FORMQ_SUBMIT.toString()));

            formQService.submitFormQToFinacle();
            cronJobService.updateRunningJob(JobCategory.FORMQ_SUBMIT.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
