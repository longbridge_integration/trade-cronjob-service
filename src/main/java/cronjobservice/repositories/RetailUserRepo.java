package cronjobservice.repositories;

import cronjobservice.models.Code;
import cronjobservice.models.RetailUser;
import cronjobservice.models.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by Fortune on 4/5/2017.
 */
@Repository
@Transactional
public interface RetailUserRepo extends CommonRepo<RetailUser, Long> {

	Iterable<RetailUser> findByRole(Role r);
    Page<RetailUser> findByRole(Role r, Pageable pageDetail);
    Integer countByRole(Role r);
    RetailUser findFirstByUserName(String username);
    RetailUser findFirstByUserNameIgnoreCase(String username);
    RetailUser findFirstByEntrustIdIgnoreCase(String entrustId);
    RetailUser findFirstByEmailIgnoreCase(String email);
    RetailUser findFirstByCustomerId(String customerId);
    boolean existsByCustomerId(String customerId);
    List<RetailUser> findByCustomerSegment(Code custSegment);
    @Modifying
    @Query("update RetailUser  u set u.lastLoginDate = current_timestamp() , u.lockedUntilDate = NULL, u.noOfLoginAttempts = 0, u.status='A' where u.userName = :name")
    void updateUserAfterLogin(@Param("name") String userName);

}
