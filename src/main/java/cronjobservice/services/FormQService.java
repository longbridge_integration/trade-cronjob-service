package cronjobservice.services;


import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.FormHolder;
import cronjobservice.dtos.FormQDTO;
import cronjobservice.dtos.PTADTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.FormA;
import cronjobservice.models.FormQ;
import cronjobservice.models.PTA;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
//
public interface FormQService {

    List<String> submitFormQToFinacle() throws CronJobException;

    void getFormQSubmitResponse();

    void getFormQAllocation();

    @Transactional
    Boolean updateFormQAllocation(FormQ tmsForm, FormQ finForm, FormQDTO formQDTO) throws CronJobException;

    @javax.transaction.Transactional
    String getFormQAddedOnFinacle();

    Boolean saveFormQFromFinacle(FormQ formQ) throws CronJobException;

    Boolean updateFormQFromFinacle(FormQ tmsFormQ, FormQ finFormQ) throws CronJobException;

    FormQDTO convertEntityTODTO(FormQ formQ);

    FormQDTO convertEntityTODTOFinacle(FormQ formQ);

    List<CommentDTO> getComments(FormQ formQ);
}
