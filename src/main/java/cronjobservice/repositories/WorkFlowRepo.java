package cronjobservice.repositories;

import cronjobservice.models.WorkFlow;
import org.springframework.stereotype.Repository;

/**
 * Created by Showboy on 28/08/2017.
 */
@Repository
public interface WorkFlowRepo extends CommonRepo<WorkFlow, Long> {
    WorkFlow findByName(String name);
    WorkFlow findByCodeType(String type);
}
