package cronjobservice.dtos;

import cronjobservice.models.Code;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by longbridge on 1/11/18.
 */
public class OutGuaranteeDocDTO {

    private Long id;

    private String documentNumber;
    private String documentDate;
    private CodeDTO documentType;
    private String documentValue;
    private String documentRemark;
    private String fileUpload;
    private String numberOfOriginalDoc;
    private String numberOfDuplicateDoc;
    private String codedoctype;
    private String documentCurrency;
    private String docMandFlg;

    private int version;
    private MultipartFile file;

    public String getCodedoctype() {
        return codedoctype;
    }

    public void setCodedoctype(String codedoctype) {
        this.codedoctype = codedoctype;
    }

    public CodeDTO getDocumentType() {
        return documentType;
    }

    public void setDocumentType(CodeDTO documentType) {
        this.documentType = documentType;
    }

    public String getNumberOfOriginalDoc() {
        return numberOfOriginalDoc;
    }

    public void setNumberOfOriginalDoc(String numberOfOriginalDoc) {
        this.numberOfOriginalDoc = numberOfOriginalDoc;
    }

    public String getNumberOfDuplicateDoc() {
        return numberOfDuplicateDoc;
    }

    public void setNumberOfDuplicateDoc(String numberOfDuplicateDoc) {
        this.numberOfDuplicateDoc = numberOfDuplicateDoc;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(String fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getDocumentValue() {
        return documentValue;
    }

    public void setDocumentValue(String documentValue) {
        this.documentValue = documentValue;
    }

    public String getDocumentRemark() {
        return documentRemark;
    }

    public void setDocumentRemark(String documentRemark) {
        this.documentRemark = documentRemark;
    }

    public String getDocumentCurrency() {
        return documentCurrency;
    }

    public void setDocumentCurrency(String documentCurrency) {
        this.documentCurrency = documentCurrency;
    }

    public String getDocMandFlg() {
        return docMandFlg;
    }

    public void setDocMandFlg(String docMandFlg) {
        this.docMandFlg = docMandFlg;
    }
}
