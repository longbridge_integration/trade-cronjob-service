package cronjobservice.services.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cronjobservice.api.*;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.FormNumber;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class FormQServiceImpl implements FormQService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ModelMapper modelMapper;

    private Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    private MessageSource messageSource;

    @Autowired
    ObjectMapper objectMapper;

//    @Autowired
//    WorkFlowStatesService workFlowStatesService;

    @Autowired
    WorkFlowRepo workFlowRepo;

    @Autowired
    FormQRepo formQRepo;

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private ConfigurationService configurationService;

//    @Autowired
//    CommentService commentService;
//
//    @Autowired
//    UserGroupService userGroupService;
    @Autowired
    private RetailUserService retailUserService;
    @Autowired
    private CorporateService corporateService;

    @Autowired
    EntityManager entityManager;

    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    FormQService formQService;
    @Autowired
    BankUserService bankUserService;
    @Autowired
    ServiceAuditRepo serviceAuditRepo;
    @Value("${tradeservice.service.uri}")
    private String URI;
    @Autowired
    private RestTemplate template;
    @Autowired
    IntegrationService integrationService;
    @Autowired
    FormQAllocationRepo formQAllocationRepo;


    @Override
    public List<String> submitFormQToFinacle() throws CronJobException {
        try{
            HashMap<String, String> map = new HashMap<>();
            List<String> responses = new ArrayList<>();
            HashMap<String, Date> map2 = new HashMap<>();
            List<FormQ> formQS = formQRepo.findByStatusAndSubmitFlag("A", "U");

            int formQSize = formQS.size();
            logger.info("This is the Number of Form Qs Fetched from TMS {}", formQSize);

            for (FormQ formQ:formQS) {
                logger.info("Form Q number {}",formQ.getFormNumber());

                List<CommentDTO> commentDTO = formQService.getComments(formQ);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    commentDtd.getMadeOn();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                    map2.put("madeOn", commentDtd.getMadeOn());
                });

                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
                FormQDTO formQDTO = formQService.convertEntityTODTO(formQ);

                FormQFI formQFI = new FormQFI();
                formQFI.setFormQDTO(formQDTO);
                formQFI.setComment(map.get("comment"));
                formQFI.setAuthFirstName(bankUser.getFirstName());
                formQFI.setAuthLastName(bankUser.getLastName());
                formQFI.setMadeOn(map2.get("madeOn"));
                formQFI.setSortCode(bankUser.getBranch().getSortCode());
                logger.info("The Main Form Q Bank Useer{}", bankUser.getFirstName());


                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(formQDTO.getTempFormNumber(), RequestType.FORM_Q);
                logger.info("Form Q Audit size {}", serviceAudits);
                int auditSize = serviceAudits.size();


                if(auditSize >=10) {
                    formQ.setSubmitFlag("F");
                    formQRepo.save(formQ);
//                    ptaService.sendPTAITSmail(pta);

                    logger.info("UPDATED Form Q SUBMIT FLAG WITH F DUE TO FAILURE");
                }

                if (auditSize < 10) {

                    String uri = URI + "/api/formq/submit";
                    try {
                        String formQSubmitResponse = template.postForObject(uri, formQFI, String.class);
                        logger.info("Form Q Submit to Finacle Message {} ", formQSubmitResponse);
                        responses.add(formQSubmitResponse);

                        ServiceAudit serviceAudit = new ServiceAudit();
                        serviceAudit.setRequestType(RequestType.FORM_Q);
                        serviceAudit.setRequestDate(new Date());
                        serviceAudit.setTempNumber(formQDTO.getTempFormNumber());


                        if (formQSubmitResponse.equalsIgnoreCase("Y")) {
                            formQ.setSubmitFlag("S");
                            serviceAudit.setResponseFlag("Y");
                            serviceAudit.setResponseMessage("Successful");
                            serviceAuditRepo.save(serviceAudit);
                            formQRepo.save(formQ);


                        } else if (formQSubmitResponse.equalsIgnoreCase("S")) {
                            formQ.setStatus("PV");
                            formQ.setSubmitFlag("S");
                            formQRepo.save(formQ);

                        } else {

                            serviceAudit.setResponseFlag("N");
                            serviceAudit.setResponseMessage(formQSubmitResponse);
                            serviceAuditRepo.save(serviceAudit);
                            logger.error("Error response for Form Q {} ", formQSubmitResponse);
                        }
                    }catch(Exception e){
                        logger.info("Form Q Submit To Finacle Error {} ",e);
                    }
                }

            }
            return responses;
        }catch(Exception e){
            logger.info("Error submitting Form Q to Finacle {} ",e);
            e.printStackTrace();
            throw  new CronJobException();
        }
    }

    public void getFormQSubmitResponse(){

        List<FormQDetails> formQDetails = integrationService.getFormQSubmitResponse();

        try{

            for (FormQDetails formQDetail : formQDetails) {
                logger.info("The Form Q Form number abt getting its Response {} ",formQDetail.getFormNumber());
                if (null==formQDetail.getFormNumber()) {
                    logger.info("Form Q Form Number from Finacle does not exist");
                    continue;
                }
                FormQ formQ = modelMapper.map(formQDetail, FormQ.class);
                FormQDTO formQDTO = formQService.convertEntityTODTOFinacle(formQ);
                FormQ temp = formQRepo.findByTempFormNumber(formQDTO.getTempFormNumber());


                if(null != temp) {

                    Boolean response = formQService.updateFormQFromFinacle(temp, formQ);

                    if (response) {
                        String uri = URI + "/api/formq/updateformqflag";
                        try {
                            template.postForObject(uri, formQDTO, String.class);
                            logger.info("Form Q Update Flg was successful");
                        } catch (Exception ex) {
                            logger.error("Exception occurred getting response for Form Q {}", ex);
                            ex.printStackTrace();
                        }
                    }
                }else {
                    logger.info("Form Q Form Number does not exist");
                }
//                else {
//                    logger.info("PTA Tran ID not Saved");
//                }

            }
        }catch (Exception e){
            logger.info("Error getting Form Q Response from FInacle {} ",e);
            e.printStackTrace();
            throw new CronJobException();
        }
    }


    @Override
    public void getFormQAllocation() {
        List<FormQDetails> formQDetails = integrationService.getFormQAllocation();
        try {
            for (FormQDetails formQDetail : formQDetails) {


                logger.info("The Form Q Number  for allocation {} ", formQDetail.getFormNumber());
                FormQ formQ = modelMapper.map(formQDetail, FormQ.class);
                logger.info("The Form Q Allocation amount {} ", formQ.getAllocatedAmount());

                if (formQ.getFormNumber() != null && !"".equals(formQ.getFormNumber())) {
                    logger.info("The Form Q FORM number {} ", formQ.getFormNumber());
                    FormQ temp = formQRepo.findByFormNumber(formQ.getFormNumber());
                    if (null==temp) {
                        logger.info("The Temp Form Q {} Does Not Exist",formQ.getFormNumber());
                        continue;
                    }
                    if(null==formQ.getTempFormNumber() || "".equals(formQ.getTempFormNumber())) {
                        formQ.setTempFormNumber(temp.getTempFormNumber());
                    }
                    FormQDTO formQDTO = formQService.convertEntityTODTOFinacle(formQ);
                    Boolean response = formQService.updateFormQAllocation(temp, formQ,formQDTO);

                    if (response) {
                        String uri = URI + "/api/formq/updateallocationflag";
                        template.postForObject(uri, formQDTO, String.class);
                        logger.info("updating form q allocation job was successful");
                    } else {
                        System.out.println(" Form Q Allocation not saved on TMS");
                    }
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public Boolean updateFormQAllocation(FormQ tmsForm, FormQ finForm, FormQDTO formQDTO) throws CronJobException {
        try {
            if(null != finForm){
                List<FormQAllocation> formQAllocations = formQAllocationRepo.findByFormNumber(finForm.getFormNumber());
                if(formQAllocations.size()>1){
                    int counter =0;
                    for (FormQAllocation formQAllocation:formQAllocations) {
                        if(counter==0){
                            continue;
                        }else {

                            formQAllocationRepo.delete(formQAllocation);
                        }
                        counter++;
                    }
                }else if(formQAllocations.size()==0){
                    FormQAllocation formQAllocation = new FormQAllocation();
                    formQAllocation.setAllocationAccount(finForm.getAllocationAccount());
                    formQAllocation.setAllocationAmount(finForm.getAllocatedAmount());
                    formQAllocation.setAllocationCurrency(finForm.getAllocationCurrencyCode());
                    formQAllocation.setExchangeRate(finForm.getExchangeRate());
                    formQAllocation.setFormNumber(finForm.getFormNumber());
//                    formQAllocation.setDateOfAllocation(finForm.getDateOfAllocation());
//                    formQAllocation.setDealId(finForm.getDealId());
//                    formQAllocation.setT(finForm.getTranId());
                    formQAllocation.setDateOfAllocation(new Date());
                    formQAllocationRepo.save(formQAllocation);

                    BigDecimal formAmount = tmsForm.getAmount();
                    BigDecimal utilized = tmsForm.getUtilization();
                    int i = formAmount.compareTo(utilized);
                    if(i == 0){
                        tmsForm.setStatus("R");
                    }else if((utilized.compareTo(BigDecimal.ZERO) == 0)){
                        tmsForm.setStatus("L");
                    }else if(i > 0 && !(utilized.compareTo(BigDecimal.ZERO) == 0)){
                        tmsForm.setStatus("PR");
                    }
                    formQRepo.save(tmsForm);
                    String uri = URI + "/api/formq/allocationresponse";
                    template.postForObject(uri, formQDTO, String.class);
                    logger.info("Successly allocated form q {}", formQDTO.getFormNumber());
                    return true;
                }

            }
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }catch (Exception e){
            logger.info("the form q can't create allocation because {}",e);
        }
        return false;
    }

    @javax.transaction.Transactional
    @Override
    public String getFormQAddedOnFinacle() {
        List<FormQDetails> formQDetails = integrationService.getFormQAddedOnFinacle();
        try {
            for (FormQDetails formADetail : formQDetails) {
                try{
//
                    FormQ formQ = modelMapper.map(formADetail, FormQ.class);
//
                    Boolean response = formQService.saveFormQFromFinacle(formQ);
                    FormQ formQ1 = formQRepo.findByFormNumber(formQ.getFormNumber());
                    FormQDTO formQDTO = formQService.convertEntityTODTOFinacle(formQ1);
                    if (response) {
                        String uri = URI + "/api/formq/updateformqaddedonfinacle";
                        template.postForObject(uri, formQDTO, String.class);
//                        logger.info("Success after response Form Q added  on finacle with utilize amt {}",formA.getUtilization());
                    } else {
                        System.out.println("Form Q not Saved");
                    }

                }catch (Exception e){
                    logger.info("can't update because {}",e);
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    public Boolean saveFormQFromFinacle(FormQ formQ) throws CronJobException {

        try {
            String generatedFormNumber = FormNumber.getFormNumber();

            formQ.setUserType("BANK");
            formQ.setInitiatedBy("FINACLE");
            formQ.setCreatedOn(new Date());
            formQ.setSubmitFlag("S");
//            formQ.setCorporateName(formA.getFirstName());
            formQ.setTempFormNumber(generatedFormNumber);
            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
            formQ.setBankCode(bankCodeSetting.getValue());
            SettingDTO getBidRateCode = configurationService.getSettingByName("FORMQ_RATE_CODE");
            formQ.setBidRateCode(getBidRateCode.getValue());


            if (null==formQ.getUtilization()){
                formQ.setUtilization(new BigDecimal("0"));
            }

            BigDecimal formAmount = formQ.getAmount();
            BigDecimal utilization = formQ.getUtilization();
            logger.info("form q utilized amt for form q added on fiancle {} ",utilization);

            int i = formAmount.compareTo(utilization);
            if(i == 0){
                formQ.setStatus("R");
            }else if(utilization.compareTo(BigDecimal.ZERO) == 0){
                formQ.setStatus("A");
            }else if(i > 0 && !(utilization.compareTo(BigDecimal.ZERO) == 0) ){
                formQ.setStatus("PR");
            }

//            formQ.setExpiryDate(getFormExpiryDate());
//
//
//            Date date = getFormExpiryDate();
//            if (date != null) {
//                formA.setExpiryDate(date);
//            }



            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_Q_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            formQ.setWorkFlow(workFlow);
            formQRepo.save(formQ);


            return true;
        } catch (CronJobException e) {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale), e);
        }
    }



    @Transactional
    @Override
    public Boolean updateFormQFromFinacle(FormQ tmsFormQ, FormQ finFormQ) throws CronJobException {
        try {
            tmsFormQ.setFormNumber(finFormQ.getFormNumber());
            tmsFormQ.setApplicationNumber(finFormQ.getFormNumber());
            formQRepo.save(tmsFormQ);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }




    @Override
    public FormQDTO convertEntityTODTO(FormQ formQ) {
        FormQDTO formQDTO = modelMapper.map(formQ, FormQDTO.class);

        if (formQ.getStatus() != null && !(formQ.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formQDTO.getWorkFlow().getCodeType(), formQDTO.getStatus());
            formQDTO.setStatusDesc(workFlowStates.getDescription());
            formQDTO.setWorkFlowId(formQ.getWorkFlow().getId());
        }
        if (formQ.getStatus().equals("P")) {
            formQDTO.setStatusDesc("Saved");
        }

        return formQDTO;
    }

    @Override
    public FormQDTO convertEntityTODTOFinacle(FormQ formQ)
    {
        FormQDTO formQDTO=modelMapper.map(formQ , FormQDTO.class);


        if(formQ.getStatus() != null && !(formQ.getStatus().equals("P")) ){
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formQDTO.getWorkFlow().getCodeType(), formQDTO.getStatus());
            formQDTO.setStatusDesc(workFlowStates.getDescription());
            formQDTO.setWorkFlowId(formQ.getWorkFlow().getId());
        }

//        if(bta.getDateCreated()!=null){
//            btadto.setConvert(bta.getDateCreated().toString());
//        }

        return formQDTO;
    }



    @Override
    public List<CommentDTO> getComments(FormQ formQ) throws CronJobException {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        {
            commentDTOList=convertEntitiestoDTOs(formQ.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus()!="A");


        }
        return  commentDTOList;
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS)
    {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        for(Comments comments:commentDTOS)
        {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return  commentDTOList;
    }

    private CommentDTO convertEntityToDTO(Comments comments)
    {
        WorkFlowStates workFlowStates= workFlowStatesRepo.findByTypeAndCode("FORM_Q",comments.getStatus());
        if(workFlowStates!=null)
        {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments,CommentDTO.class);
    }

}
