package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "lc_confirmation")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class LcConfirmation extends AbstractEntity{

    private String lcNumber;


    @ManyToMany(cascade={CascadeType.ALL})
    private List<Comments> comment = new ArrayList<>();

    private String status;

    private String cifid;

    private Date createdOn;

    @ManyToOne
    private Account chargeAccount;

    private String confirmationType;
    private BigDecimal lcAmount;
    private String lcCurrency;

    private UserType userType;
    private String InitiatedBy;
    private Date dateCreated;
    private String applicantName;
    private Boolean guidelineApproval;


    public String getLcNumber() {
        return lcNumber;
    }

    public void setLcNumber(String lcNumber) {
        this.lcNumber = lcNumber;
    }

    public List<Comments> getComment() {
        return comment;
    }

    public void setComment(List<Comments> comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Account getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(Account chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public String getConfirmationType() {
        return confirmationType;
    }

    public void setConfirmationType(String confirmationType) {
        this.confirmationType = confirmationType;
    }

    public BigDecimal getLcAmount() {
        return lcAmount;
    }

    public void setLcAmount(BigDecimal lcAmount) {
        this.lcAmount = lcAmount;
    }

    public String getLcCurrency() {
        return lcCurrency;
    }

    public void setLcCurrency(String lcCurrency) {
        this.lcCurrency = lcCurrency;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getInitiatedBy() {
        return InitiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        InitiatedBy = initiatedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public Boolean getGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(Boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }
}
