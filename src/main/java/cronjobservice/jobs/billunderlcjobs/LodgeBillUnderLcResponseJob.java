package cronjobservice.jobs.billunderlcjobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.services.ExportBillsService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;


@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class LodgeBillUnderLcResponseJob implements Job {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CronJobService cronJobService;
    @Autowired
    ExportBillsService exportBillsService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextLc = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextLc.getBean (CronJobService.class);
        ExportBillsService exportBillsService = contextLc.getBean (ExportBillsService.class);
//        System.out.println("five minute job runing");
        try {
            logger.info("LODGE EXPORT BILL Under Lc Response job is starting");
            cronJobService.saveRunningJob(JobCategory.LODGE_EXPORT_BILL_UNDER_LC_RESPONSE.toString(),cronJobService.getCurrentExpression(JobCategory.LODGE_EXPORT_BILL_UNDER_LC_RESPONSE.toString()));

            exportBillsService.getLodgeResponseExportBillsUnderLc();
            cronJobService.updateRunningJob(JobCategory.LODGE_EXPORT_BILL_UNDER_LC_RESPONSE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
