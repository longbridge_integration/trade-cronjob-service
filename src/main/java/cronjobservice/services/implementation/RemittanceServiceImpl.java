package cronjobservice.services.implementation;

import cronjobservice.api.CustomerDetails;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.FormNumber;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Created by chiomarose on 05/10/2017.
 */
@Service
public class RemittanceServiceImpl implements RemittanceService
{


    private Logger logger= LoggerFactory.getLogger(this.getClass());

    @Autowired
    MessageSource messageSource;

    @Autowired
    ModelMapper modelMapper;
  @Autowired
  BranchRepo branchRepo;
  @Autowired
  CodeRepo codeRepo;

    @Autowired
    RemittanceRepo remittanceRepo;
// @Autowired
// RemittanceDocumentsRepo remittanceDocumentsRepo;

    @Autowired
    WorkFlowRepo workFlowRepo;

    @Autowired
    ConfigurationService configurationService;

    @Autowired
    private WorkFlowStatesRepo workFlowStatesRepo;

    @Autowired
    EntityManager entityManager;

    @Autowired
    private IntegrationService integrationService;
// @Autowired
//    private RetailBeneficiaryService retailBeneficiaryService;

    @Autowired
    AccountService accountService;
    @Autowired
    CodeService codeService;
    @Autowired
    MailService mailService;

    @Autowired
    RetailUserRepo retailUserRepo;

    @Autowired
    ServiceAuditRepo serviceAuditRepo;

    @Autowired
    CorporateUserRepo corporateUserRepo;
    @Autowired
    FormARepo formARepo;

    @Value("${remittance.upload.files}")
    private String UPLOADED_FOLDER;

    private Locale locale= LocaleContextHolder.getLocale();



    @javax.transaction.Transactional
    @Override
    public Boolean saveRemittanceFromFinacle(Remittance finremittance, FormA tmsformA) throws CronJobException {
        try {
            Remittance remittance= new Remittance();
//            tmsRemit.setFormNumber(finRemit.getFormNumber());
//            tmsRemit.setRemitanceId(finRemit.getRemitanceId());
//            tmsRemit.setTranId(finRemit.getTranId());
//            tmsRemit.setStatus("R");

            finremittance.setStatus("R");
            finremittance.setInitiatedBy("FINACLE");
            finremittance.setDateCreated(new Date());
            finremittance.setSubmitFlag("S");

            SettingDTO wfSetting = configurationService.getSettingByName("DEF_REMITTANCE_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            finremittance.setWorkFlow(workFlow);

            String number = FormNumber.getRemittanceFormNumber();
            finremittance.setTempFormNumber(number);

            remittanceRepo.save(finremittance);
            logger.info(" Remittance added on finacle transfer amt {} ", finremittance.getTransferAmount());
            logger.info(" Remittance added on finacle forma  utilized amt {} ", tmsformA.getUtilization());
//            logger.info(" Remittance added on finacle transfer amt {} ", remittanceDetail.getF());


            //Updating Form A utilization and status

            BigDecimal remitUtilization = new BigDecimal(finremittance.getTransferAmount());
            BigDecimal formaUtilization = tmsformA.getUtilization();
            BigDecimal updatedUtilization = remitUtilization.add(formaUtilization);
            logger.info(" Remittance added on finacle updated forma  utilized amt {} ", updatedUtilization);
            tmsformA.setUtilization(updatedUtilization);
            tmsformA.setAllocatedAmount(remitUtilization);



            if (tmsformA.getUtilization().compareTo(tmsformA.getAmount()) == -1){
                tmsformA.setStatus("PR");
            }else {
                tmsformA.setStatus("R");
            }
            formARepo.save(tmsformA);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }


    @javax.transaction.Transactional
    @Override
    public Boolean saveFundTransferFromFinacle(Remittance finFundTransfer) throws CronJobException {
        try {
            Remittance remittance= new Remittance();

            finFundTransfer.setStatus("R");
            finFundTransfer.setInitiatedBy("FINACLE");
            finFundTransfer.setDateCreated(new Date());
            finFundTransfer.setSubmitFlag("S");

            SettingDTO wfSetting = configurationService.getSettingByName("DEF_REMITTANCE_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            finFundTransfer.setWorkFlow(workFlow);

            String number = FormNumber.getRemittanceFormNumber();
            finFundTransfer.setTempFormNumber(number);

            remittanceRepo.save(finFundTransfer);
            logger.info(" Remittance added on finacle transfer amt {} ", finFundTransfer.getTransferAmount());
//            logger.info(" Remittance added on finacle transfer amt {} ", remittanceDetail.getF());



            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }


//    @Override
//    public Boolean saveRemittanceFromFinacle(RemittanceDTO remittanceDTO) throws CronJobException {
//        System.out.println("im in the service");
//
//        SettingDTO remitanceApproval = configurationService.getSettingByName("APPROVAL_OF_ REMITTANCE");
//        if(remitanceApproval!=null && remitanceApproval.isEnabled()&& remittanceDTO.getStatus().equals("S"))
//        {
//            remittanceDTO.setStatus("A");
//        }
//        try {
//
//            String number = FormNumber.getRemittanceFormNumber();
//            if(remittanceDTO.getDocuments() != null) {
//                for (RemittanceDocumentDTO d : remittanceDTO.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, number, d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//            }
//
//
//            Remittance remittance=convertDTOToEntity(remittanceDTO);
//            remittance.setUserType("BANK");
//            remittance.setInitiatedBy("FINACLE");
//            remittance.setCifid(remittanceDTO.getCifid());
//            remittance.setDateCreated(new Date());
//            remittance.setExpiry(getFormExpiryDate());
//            remittance.setTempFormNumber(number);
//            remittance.setRemittedCurrency(remittanceDTO.getCurrencyCode());
//            remittance.setRemittedAmount(new BigDecimal(remittanceDTO.getTransferAmount()));
//
//            /**
//             * BillsDocument table
//             */
//            if(remittanceDTO.getDocuments() != null) {
//                if(!"".equalsIgnoreCase(remittanceDTO.getBranch()) && null != remittanceDTO.getBranch()) {
//                    System.out.println(remittanceDTO.getBranch());
//                    Branch branch = branchRepo.findOne(Long.parseLong(remittanceDTO.getBranch()));
//                    remittance.setDocSubBranch(branch);
//                }
//
//                Iterator<RemittanceDocument> DocumentIterator = remittance.getDocuments().iterator();
//                System.out.println("document size "+remittance.getDocuments().size());
//                System.out.println("documentDTO size "+remittanceDTO.getDocuments().size());
//                Iterator<RemittanceDocumentDTO> DocumentDTOIterator = remittanceDTO.getDocuments().iterator();
//
//                while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                    RemittanceDocument document = DocumentIterator.next();
//                    RemittanceDocumentDTO documentDTO = DocumentDTOIterator.next();
//                    if (documentDTO.getTypeOfDocument() == null) {
//                        System.out.println("type of doc is null");
//                        DocumentIterator.remove();
//                        DocumentDTOIterator.remove();
//                    } else {
//                        System.out.println("in the else");
//                        document.setDocumentType(codeRepo.findByTypeAndCode("REMITTANCE_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                        System.out.println("lllllll");
//                        document.setRemittance(remittance);
//                        System.out.println("owkay");
//                    }
//                }
//            }
//            System.out.println("before getting settings");
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_REMITTANCE_WF");
//            System.out.println("after getting settings");
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            System.out.println("after getting workflow");
//            remittance.setWorkFlow(workFlow);
//            remittanceRepo.save(remittance);
//            return true;
//        }
//
//        catch (CronJobException e)
//        {
//            throw new CronJobException(messageSource.getMessage("remittance.add.success", null, locale));
//        }
//    }


    @Override
    @Async
    public Boolean sendRemittanceITSmail(Remittance remittance) {

        try {
           // boolean result=false;
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "Remittance";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(remittance.getTempFormNumber(),RequestType.REMITTANCE);
            logger.info("remittance serviceAudit {}",serviceAudit);
            Context context = new Context();
                context.setVariable("name", remittance.getFirstName() + " " + remittance.getLastName());
                context.setVariable("formNumber", remittance.getTempFormNumber());
                context.setVariable("formType", formType);
                context.setVariable("date", DateFormatter.format(new Date()));
                context.setVariable("customerId", remittance.getCifid());
                context.setVariable("username", remittance.getInitiatedBy());
                context.setVariable("error", serviceAudit.getResponseMessage());
                context.setVariable("code", "REMITTANCE");
                String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
                Email email = new Email.Builder()
                        .setRecipients(allMails)
                        .setSubject(subject)
                        .setTemplate("mail/finacleSubmission")
                        .build();

                mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("form.add.success", null, locale));
        }
    }


    private String addAttachments(MultipartFile file, String formNumber, String documentType) {
        String uploadedFileName = "";


        if (file.isEmpty()) {
            System.out.println("file is empty");
        }

        try {
            byte[] bytes = file.getBytes();
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User users = principal.getUser();

            if (users.getUserType().equals(UserType.RETAIL)) {
                Path  path = Paths.get(UPLOADED_FOLDER + formNumber + documentType + file.getOriginalFilename());
                logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());


                File f = new File(path.toString());
                if(f.exists()){
                    System.out.println("file already exist");
                }else{
                    System.out.println("file does not exist");
                }

                Files.write(path, bytes);
                logger.info("file uploaded successfully to {} directory", path);
                uploadedFileName =file.getOriginalFilename();
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("failed to upload file");
        }

        logger.info("uploadFileName {}", uploadedFileName);
        if (StringUtils.isEmpty(uploadedFileName)) {
            logger.info("No file added, Please select a file to upload");

        } else {
            logger.info("You successfully uploaded {}", uploadedFileName);

        }
        return uploadedFileName;
    }

//    public List<Map<String,Object>> getDisplayOpenPageForCorporateUser(Long id,String entityName)
//    {
//        CustomUserPrincipal principal=(CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        User doneby=principal.getUser();
//        if(doneby==null)
//        {
//            logger.info("User does not exit");
//        }
//        id = (id - 1) * 4 + 1;
//        CorporateUser corporateUser=(CorporateUser) doneby;
//        logger.info("retail user get customer id{}",corporateUser.getCorporate().getCustomerId());
//        List<Map<String, Object>> objects = null;
//        objects = page.getOpenPage(entityName,id, 4,corporateUser.getCorporate().getCustomerId(),"P","S","D");
//        return objects;
//
//    }



    @Override
    public RemittanceDTO getRemittance(Long id) throws CronJobException
    {
        Remittance remittance=remittanceRepo.findOne(id);
        RemittanceDTO remittanceDTO = convertEntityTODTOView(remittance);
        if(remittance.getDocuments() != null) {
            Iterator<RemittanceDocument> formDocumentIterator = remittance.getDocuments().iterator();
            Iterator<RemittanceDocumentDTO> formDocumentDTOIterator = remittanceDTO.getDocuments().iterator();

            while (formDocumentIterator.hasNext() && formDocumentDTOIterator.hasNext()) {
                RemittanceDocument remittanceDocument = formDocumentIterator.next();
                RemittanceDocumentDTO remittanceDocumentDTO = formDocumentDTOIterator.next();
                if (remittanceDocument.getDocumentType() == null) {
                    formDocumentDTOIterator.remove();
                    formDocumentIterator.remove();
                } else {
                    remittanceDocumentDTO.setTypeOfDocument(remittanceDocument.getDocumentType().getCode());
                }
            }
        }
        return  remittanceDTO;
    }

    @Override
    public RemittanceDTO getRemittanceForView(Long id) throws CronJobException
    {
        Remittance remittance=remittanceRepo.findOne(id);
        RemittanceDTO remittanceDTO = convertEntityTODTOView(remittance);

        if (!remittanceDTO.getState().equalsIgnoreCase("") &&  null != remittanceDTO.getState()) {
            String state = remittance.getState();
            remittanceDTO.setState(codeService.getByTypeAndCode("STATE",state).getDescription());
        }
        if (!remittanceDTO.getTown().equalsIgnoreCase("") && null != remittanceDTO.getTown()) {
            String town = remittance.getTown();
            remittanceDTO.setTown(codeService.getByTypeAndCode("CITY",town).getDescription());
        }
        if (remittanceDTO.getBeneficiaryCountry().equalsIgnoreCase("") && null != remittanceDTO.getBeneficiaryCountry()) {
            String country = remittance.getBeneficiaryCountry();
            remittanceDTO.setBeneficiaryCountry(codeService.getByTypeAndCode("COUNTRY",country).getDescription());
        }

        if(remittance.getDocuments() != null) {
            Iterator<RemittanceDocument> formDocumentIterator = remittance.getDocuments().iterator();
        Iterator<RemittanceDocumentDTO> formDocumentDTOIterator = remittanceDTO.getDocuments().iterator();

        if(remittance.getDocSubBranch() != null ){
            remittanceDTO.setBranch(remittance.getDocSubBranch().getName());
        }
            if(remittance.getModeOfIdentification().equalsIgnoreCase("") ){
                remittanceDTO.setModeOfIdentification(codeRepo.findByTypeAndCode("MODE_OF_IDENTIFICATION",remittance.getModeOfIdentification()).getDescription());
            }

            while (formDocumentIterator.hasNext() && formDocumentDTOIterator.hasNext()) {
                RemittanceDocument remittanceDocument = formDocumentIterator.next();
                RemittanceDocumentDTO remittanceDocumentDTO = formDocumentDTOIterator.next();
                if (remittanceDocument.getDocumentType() == null) {
                    formDocumentDTOIterator.remove();
                    formDocumentIterator.remove();
                } else {
                    remittanceDocumentDTO.setTypeOfDocument(remittanceDocument.getDocumentType().getDescription());
                    remittanceDocumentDTO.setCodedoctype(remittanceDocument.getDocumentType().getCode());
                }
            }
        }
        return remittanceDTO;
    }


    @Override
    public Page<RemittanceDTO> getOpenRemittance(Pageable pageable)
    {

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        RetailUser retailUser=(RetailUser)users;
        Page<Remittance> pages=remittanceRepo.findByCifidAndStatus(retailUser.getCustomerId(),"P","S","D",pageable);
        List<RemittanceDTO> remittanceDTOS=convertEntitiesToDTOs(pages.getContent());
        long t=pages.getTotalElements();
        List<RemittanceDTO> sortedList = remittanceDTOS.stream().sorted(Comparator.comparing(RemittanceDTO::getDateCreated).reversed()).collect(Collectors.toList());
        Page<RemittanceDTO> page1=new PageImpl<RemittanceDTO>(sortedList,pageable,t);
        return page1;
    }

    @Override
    public Boolean checkAvailableBalance(String amount,String accountId) throws CronJobException
    {
        Boolean result;
        Map<String,BigDecimal> availableBalance=integrationService.getBalance(accountId);
        BigDecimal availableAmount= availableBalance.get("availablebalance");
        if(!(amount.isEmpty()))
        {
            new BigDecimal(amount);
        }
        logger.info("getting the current amount{}",availableAmount);
        if(!(amount).isEmpty())
        {
            int compareResult=new BigDecimal(amount).compareTo(availableAmount);


            if(compareResult==1)
            {
                throw new CronJobException(messageSource.getMessage("form.add.failure",null,locale));

            }

        }
        result=true;

        return result;
    }

    @Override
    public long getTotalNumberOpenRemittance() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User logginUser = principal.getUser();
        long totalNumberOfOpenRequest=0;
        if(logginUser.getUserType().equals(UserType.RETAIL))
        {
            RetailUser retailUser=(RetailUser)logginUser;
            totalNumberOfOpenRequest=remittanceRepo.countByCifidAndStatus(retailUser.getCustomerId(),"P","S","D");
        }else if (logginUser.getUserType().equals(UserType.CORPORATE)){
            CorporateUser corporatesUser=(CorporateUser) logginUser;
            totalNumberOfOpenRequest=remittanceRepo.countByCifidAndStatus(corporatesUser.getCorporate().getCustomerId()
                    ,"P","S","D");
        }

        return totalNumberOfOpenRequest;
    }

    @Override
    public long getTotalNumberRemittance() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User logginUser = principal.getUser();
        long totalNumberOfRequest=0;
        if(logginUser.getUserType().equals(UserType.RETAIL))
        {
            RetailUser retailUser=(RetailUser)logginUser;
            totalNumberOfRequest=remittanceRepo.countByCifid(retailUser.getCustomerId());
        }else if (logginUser.getUserType().equals(UserType.CORPORATE)){
            CorporateUser corporateUser=(CorporateUser) logginUser;
            totalNumberOfRequest=remittanceRepo.countByCifid(corporateUser.getCorporate().getCustomerId());
        }

        return totalNumberOfRequest;
    }

    private RemittanceDTO convertEntityTODTOView(Remittance remittance)
    {
        RemittanceDTO remittanceDTO=modelMapper.map(remittance , RemittanceDTO.class);
        if(remittanceDTO.getStatus() != null &&!(remittanceDTO.getStatus().equals("P")))
        {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(remittanceDTO.getWorkFlow().getCodeType(), remittanceDTO.getStatus());
            remittanceDTO.setStatusDesc(workFlowStates.getDescription());
            remittanceDTO.setWorkFlowId(remittance.getWorkFlow().getId());
            remittanceDTO.setWorkFlow(remittance.getWorkFlow());
        }
        if(remittanceDTO.getStatus().equals("P"))
        {
            remittanceDTO.setStatusDesc("Saved");
            remittanceDTO.setWorkFlowId(remittance.getWorkFlow().getId());
            remittanceDTO.setWorkFlow(remittance.getWorkFlow());
        }
        if (remittance.getDateCreated()!=null)
        {
            remittanceDTO.setCreatedOn(remittance.getDateCreated());
        }
        if(remittance.getRemittedAmount()!=null)
        {
            remittanceDTO.setAmountForDisplay(remittance.getRemittedAmount().toString());
        }
        if(remittance.getDocSubBranch() != null){
            remittanceDTO.setBranch(remittance.getDocSubBranch().getId().toString());
        }
        return remittanceDTO;
    }

    @Override
    public String deleteForm(Long id) throws CronJobException {
        Remittance remittance = remittanceRepo.findOne(id);

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();

        if (doneBy == null) {
            logger.info("user is null");
            throw new CronJobException(messageSource.getMessage("remittance.delete.failure", null, locale));
        }
        try {
            if(!remittance.getStatus().equalsIgnoreCase("P")){
                throw new CronJobException(messageSource.getMessage("remittance.delete.saved.failure", null, locale));
            }
            remittanceRepo.delete(remittance);
            return messageSource.getMessage("remittance.delete.success", null, locale);
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("remittance.delete.failure", null, locale), e);
        }

    }



//    @Override
//    public String updateRemittance(RemittanceDTO remittanceDTO) throws CronJobException
//    {
//       Remittance remittance= remittanceRepo.findOne(remittanceDTO.getId());
//
//       CustomUserPrincipal principal=(CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//
//       User doneBy=principal.getUser();
//
//       RetailUser user=(RetailUser)doneBy;
//
//       if(doneBy==null)
//       {
//           throw new CronJobException(messageSource.getMessage("remittance.update.failure",null,locale));
//       }
//
//       try{
//
//           if (remittanceDTO.isSaveBeneficiary()){
//               saveBeneficiary(user, remittanceDTO);
//           }
//
//
//           if(remittanceDTO.getDocuments() != null) {
//               for (RemittanceDocumentDTO d : remittanceDTO.getDocuments()) {
//                   if (!(d.getFile().isEmpty())) {
//                       System.out.println("remittance service saving the document");
//                       MultipartFile file = d.getFile();
//                       String filename = addAttachments(file, remittance.getTempFormNumber(), d.getTypeOfDocument());
//                       d.setFileUpload(filename);
//                   }
//               }
//           }
//
//
//           entityManager.detach(remittance);
//           remittance=convertDTOToEntity(remittanceDTO);
//           remittance.setInitiatedBy(user.getUserName());
//           remittance.setCifid(user.getCustomerId());
//           remittance.setExpiry(getFormExpiryDate());
//           remittance.setDateCreated(new Date());
//           remittance.setCountry("NG");
//           remittance.setSubmitFlag("U");
//           remittance.setUserType(user.getUserType().toString());
//
//
//           /**
//            * BillsDocument table
//            */
//           if(!"".equalsIgnoreCase(remittanceDTO.getBranch()) && null != remittanceDTO.getBranch()) {
//               System.out.println(remittanceDTO.getBranch());
//               Branch branch = branchRepo.findOne(Long.parseLong(remittanceDTO.getBranch()));
//               remittance.setDocSubBranch(branch);
//           }
//           if(remittanceDTO.getDocuments() != null) {
//               Iterator<RemittanceDocument> DocumentIterator = remittance.getDocuments().iterator();
//               Iterator<RemittanceDocumentDTO> DocumentDTOIterator = remittanceDTO.getDocuments().iterator();
//               Collection<RemittanceDocument> fields = new ArrayList<RemittanceDocument>();
//               while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                   RemittanceDocument document = DocumentIterator.next();
//                   RemittanceDocumentDTO documentDTO = DocumentDTOIterator.next();
//                   RemittanceDocument remittanceDocument = null;
//                   if (documentDTO.getTypeOfDocument() == null) {
//                       DocumentIterator.remove();
//                       DocumentDTOIterator.remove();
//                   } else if (documentDTO.getId() == null) {
//                       remittanceDocument = new RemittanceDocument();
//                       modelMapper.map(documentDTO, remittanceDocument);
//                       remittanceDocument.setDocumentType(codeRepo.findByTypeAndCode("REMITTANCE_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                       remittanceDocument.setRemittance(remittance);
//                   } else {
//                       remittanceDocument = remittanceDocumentsRepo.findOne(documentDTO.getId());
//                       remittanceDocument.setDocumentValue(documentDTO.getDocumentValue());
//                       remittanceDocument.setDocumentRemark(documentDTO.getDocumentRemark());
//                       remittanceDocument.setDocumentNumber(documentDTO.getDocumentNumber());
//                       remittanceDocument.setDocumentDate(documentDTO.getDocumentDate());
//                       if (documentDTO.getFileUpload() != null) {
//                           remittanceDocument.setFileUpload(documentDTO.getFileUpload());
//                       }
//                       remittanceDocument.setDocumentType(codeRepo.findByTypeAndCode("REMITTANCE_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                       remittanceDocument.setRemittance(remittance);
//
//                   }
//                   fields.add(remittanceDocument);
//               }
//               remittance.setDocuments(fields);
//           }
//
//
//           SettingDTO wfSetting = configurationService.getSettingByName("DEF_REMITTANCE_WF");
//           WorkFlow workFlow=workFlowRepo.findByCodeType(wfSetting.getValue());
//           remittance.setWorkFlow(workFlow);
//           remittanceRepo.save(remittance);
//
//           return messageSource.getMessage("remittance.update.success",null,locale);
//       }
//
//       catch (CronJobException e)
//       {
//           throw new CronJobException(messageSource.getMessage("remittance.update.success",null,locale),e);
//       }
//
//
//    }


    @Transactional
    @Override
    public Boolean updateRemittanceFromFinacle(Remittance tmsRemit, Remittance finRemit) throws CronJobException {
        try {
            tmsRemit.setFormNumber(finRemit.getFormNumber());
            tmsRemit.setRemitanceId(finRemit.getRemitanceId());
            tmsRemit.setTranId(finRemit.getTranId());
            tmsRemit.setStatus("R");
            remittanceRepo.save(tmsRemit);
            return true;
        } catch (CronJobException e) {
            logger.info("can't save remitance because {}",e);
            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }catch (Exception e){
            e.printStackTrace();
            logger.info("can't save remitance because {}",e);
            return false;
        }
    }



    @Transactional
    @Override
    public Boolean updateRemittance(Remittance tmsRemit, Remittance finRemit) throws CronJobException {
        try {
            tmsRemit.setFormNumber(finRemit.getFormNumber());
            tmsRemit.setRemitanceId(finRemit.getRemitanceId());
            tmsRemit.setTranId(finRemit.getTranId());
            tmsRemit.setStatus("R");
            remittanceRepo.save(tmsRemit);
            return true;
        } catch (CronJobException e) {
            logger.info("can't save remitance because {}",e);
            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }catch (Exception e){
            e.printStackTrace();
            logger.info("can't save remitance because {}",e);
            return false;
        }
    }


    @Override
    public RemittanceDTO getInitiatorDetails() throws CronJobException
    {

        RemittanceDTO remittanceDTO = new RemittanceDTO();
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();

        if(users==null)
        {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        if ((users.getUserType()).equals(UserType.RETAIL))
        {
            RetailUser user = (RetailUser) users;
            remittanceDTO.setFirstName(user.getFirstName());
            remittanceDTO.setLastName(user.getLastName());
            remittanceDTO.setAddress(user.getAddress());
            remittanceDTO.setPhone(user.getPhoneNumber());
            remittanceDTO.setTown(user.getTown());
            remittanceDTO.setState(user.getState());
            remittanceDTO.setCountry(user.getCountry());
            remittanceDTO.setEmail(user.getEmail());
            remittanceDTO.setUserType("RETAIL");
        }

        else {
            throw new CronJobException(messageSource.getMessage("form.process.failure",null,locale));
        }

        return remittanceDTO;

    }

    public Remittance convertDTOToEntity(RemittanceDTO remittanceDTO )
    {

        return modelMapper.map(remittanceDTO,Remittance.class);
    }


    @Override
    public List<AccountDTO>  getCustomerNairaAccounts() throws CronJobException
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> nairaAccountList=new ArrayList<>();

        if(users==null)
        {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        if((users.getUserType()).equals(UserType.RETAIL))
        {
            RetailUser retailUser=(RetailUser)users;

            nairaAccountList=accountService.getCustomerNairaAccounts(retailUser.getCustomerId(),"NGN");

        }
        return nairaAccountList;

    }


    public List<AccountDTO> getCustomerDomAccounts() throws CronJobException
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> domaccountList=new ArrayList<>();

        if(users==null)
        {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }

        if(users.getUserType().equals(UserType.RETAIL))
        {
            RetailUser retailUser=(RetailUser)users;

            domaccountList=accountService.getCustomerDomAccounts(retailUser.getCustomerId(),"NGN");

        }
        return  domaccountList;
    }

    @Override
    public Page<RemittanceDTO> getAllRemittance(Pageable pageable) throws CronJobException
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        RetailUser retailUser=(RetailUser)users;

      Page<Remittance> remittances= remittanceRepo.findByCifid(retailUser.getCustomerId(),pageable);
        System.out.println("cifid "+retailUser.getCustomerId());
        System.out.println(remittances.getSize());
      List<RemittanceDTO>  remittanceDTOS= convertEntitiesToDTOs(remittances.getContent());

      long t=remittances.getTotalElements();

        List<RemittanceDTO> sortedList = remittanceDTOS.stream().sorted(Comparator.comparing(RemittanceDTO::getDateCreated).reversed()).collect(Collectors.toList());
        Page<RemittanceDTO> page1=new PageImpl<RemittanceDTO>(sortedList,pageable,t);
        return page1;
    }


    private List<RemittanceDTO> convertEntitiesToDTOs(Iterable<Remittance> remittances)
    {

        List<RemittanceDTO> remittanceDTOS=new ArrayList<>();

        for(Remittance remittance:remittances)
        {
            remittanceDTOS.add((convertEntityToDTO(remittance)));

        }
        return remittanceDTOS;
    }





    public RemittanceDTO convertEntityToDTO(Remittance remittance)
    {
        RemittanceDTO remittanceDTO= modelMapper.map(remittance,RemittanceDTO.class);

        if(remittanceDTO.getStatus() != null &&!(remittanceDTO.getStatus().equals("P")))
        {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(remittanceDTO.getWorkFlow().getCodeType(), remittanceDTO.getStatus());
            remittanceDTO.setStatusDesc(workFlowStates.getDescription());
            remittanceDTO.setWorkFlowId(remittance.getWorkFlow().getId());
            remittanceDTO.setWorkFlow(remittance.getWorkFlow());
        }

        if(remittanceDTO.getStatus().equals("P"))
        {
            remittanceDTO.setStatusDesc("Saved");
            remittanceDTO.setWorkFlowId(remittance.getWorkFlow().getId());
            remittanceDTO.setWorkFlow(remittance.getWorkFlow());
        }
        if (remittanceDTO.getDateCreated()!=null)
        {
            remittanceDTO.setCreatedOn(remittance.getDateCreated());
        }


        if(remittance.getRemittedAmount()!=null && remittance.getRemittedCurrency()!=null)
        {
            remittanceDTO.setAmountForDisplay(remittance.getRemittedCurrency() + " " + remittance.getRemittedAmount().toString());
        }


        return remittanceDTO;

    }

    @Override
    public RemittanceDTO convertEntityToDTOFinacle(Remittance remittance) {
        RemittanceDTO remittanceDTO= modelMapper.map(remittance,RemittanceDTO.class);

        if(remittanceDTO.getStatus() != null &&!(remittanceDTO.getStatus().equals("P")))
        {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(remittanceDTO.getWorkFlow().getCodeType(), remittanceDTO.getStatus());
            remittanceDTO.setStatusDesc(workFlowStates.getDescription());
            remittanceDTO.setWorkFlowId(remittance.getWorkFlow().getId());
            remittanceDTO.setWorkFlow(remittance.getWorkFlow());
        }

        if (remittanceDTO.getDateCreated()!=null)
        {
            remittanceDTO.setCreatedOn(remittance.getDateCreated());
        }

        return remittanceDTO;
    }

    private Date getFormExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        SettingDTO setting = configurationService.getSettingByName("REMITTANCE_EXPIRY");
        if (setting != null && setting.isEnabled()) {
            int days = NumberUtils.toInt(setting.getValue());
            calendar.add(Calendar.DAY_OF_YEAR, days);
            return calendar.getTime();
        }
        return null;
    }


    private String addAttachments(MultipartFile[] files) {
        StringJoiner sj = new StringJoiner(" , ");
        String uploadedFileName = "";

        for (MultipartFile file : files) {

            if (file.isEmpty()) {
                continue; //next pls
            }

            try {

                byte[] bytes = file.getBytes();
                CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                        .getPrincipal();
                User users = principal.getUser();

if(users.getUserType().equals(UserType.CORPORATE)) {
   CorporateUser corporateUser= (CorporateUser) users;
    String username = corporateUser.getUserName();
    logger.info("corporate user {} with username {}", corporateUser, username);
    Path path = Paths.get(UPLOADED_FOLDER + username + file.getOriginalFilename());
    logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());
    Files.write(path, bytes);
    sj.add(file.getOriginalFilename());
    logger.info("file uploaded successfully to {} directory", path);

}

else if (users.getUserType().equals(UserType.RETAIL)) {
                    RetailUser retailUser= (RetailUser) users;
                    String username = retailUser.getUserName();
                    logger.info("retail user {} with username {}", retailUser, username);
                    Path path = Paths.get(UPLOADED_FOLDER + username + file.getOriginalFilename());
                    logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());
                    Files.write(path, bytes);
                    sj.add(file.getOriginalFilename());
                    logger.info("file uploaded successfully to {} directory", path);

                }
        } catch (IOException e) {
                e.printStackTrace();
                logger.warn("failed to upload file");
            }

        }

        uploadedFileName = sj.toString();
        logger.info("uploadFileName {}", uploadedFileName);
        if (StringUtils.isEmpty(uploadedFileName)) {
            logger.info("No file added, Please select a file to upload");

        } else {
            logger.info("You successfully uploaded {}", uploadedFileName);

        }
        return uploadedFileName;
    }


    @Override
    public List<CommentDTO> getComments(Remittance remittance) throws CronJobException {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        {
            commentDTOList=convertEntitiestoDTOs(remittance.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus()!="A");


        }
        return  commentDTOList;
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS)
    {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        for(Comments comments:commentDTOS)
        {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return  commentDTOList;
    }

    private CommentDTO convertEntityToDTO(Comments comments)
    {
        WorkFlowStates workFlowStates= workFlowStatesRepo.findByTypeAndCode("REMITTANCE",comments.getStatus());
        if(workFlowStates!=null)
        {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments,CommentDTO.class);
    }

    private void sendITSmail(RetailUser user, Remittance remittance) {

        try {
            SettingDTO settingDTO = configurationService.getSettingByName("GROUP_EMAIL");
            String itsMail = settingDTO.getValue();
            String fullName = user.getLastName()+" "+user.getFirstName();
            String formType = "Remittance";
            Context context = new Context();
            context.setVariable("name", fullName);
            context.setVariable("username", user.getUserName());
            context.setVariable("formNumber", remittance.getFormNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", new Date());
            context.setVariable("customerId", user.getCustomerId());
            context.setVariable("code", "REMITTANCE");
            String subject = String.format(messageSource.getMessage("form.submission.subject", null, locale),formType);
            Email email = new Email.Builder()
                    .setRecipient(itsMail)
                    .setSubject(subject)
                    .setTemplate("mail/formSubmission")
                    .build();

            mailService.sendMail(email, context);
        } catch (Exception e) {
            logger.error("Error occurred sending activation credentials", e);
        }
    }

}
