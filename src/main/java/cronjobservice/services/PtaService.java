package cronjobservice.services;

import cronjobservice.dtos.BTADTO;
import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.PTADTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.BTA;
import cronjobservice.models.ImportLc;
import cronjobservice.models.PTA;

import java.util.List;

public interface PtaService {

    List<String> submitPtaToFinacle() throws CronJobException;
    void getPtaSubmitResponse();

    Boolean updatePtaFromFinacle(PTA tmspta, PTA finpta) throws CronJobException;

    List<CommentDTO> getComments(PTA pta);
    PTADTO convertEntityTODTO(PTA pta);
    Boolean sendPTAITSmail(PTA pta);
    PTADTO convertEntityTODTOFinacle(PTA pta);
}
