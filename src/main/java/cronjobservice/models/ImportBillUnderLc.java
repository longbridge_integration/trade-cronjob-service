package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Collection;

@Entity
@DiscriminatorValue("Under Lc")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class ImportBillUnderLc extends ImportBill{

    @ManyToOne
    ImportLc importLc;

    @OneToMany(cascade = CascadeType.ALL)
    private Collection<ImportBillForLcDocument> billForLcDocuments;

    public Collection<ImportBillForLcDocument> getDocuments() {
        return billForLcDocuments;
    }

    public void setDocuments(Collection<ImportBillForLcDocument> billForLcDocuments) {
        this.billForLcDocuments = billForLcDocuments;
    }

    public ImportLc getImportLc() {
        return importLc;
    }

    public void setImportLc(ImportLc importLc) {
        this.importLc = importLc;
    }
}