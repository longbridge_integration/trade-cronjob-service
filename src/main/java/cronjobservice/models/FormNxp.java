package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by chiomarose on 11/09/2017.
 */
@Entity
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class FormNxp extends Process
{

    //Recently added
    private String sortCode;

    private Date expiryDate;

    private String chargeAccount;
    private String transferAccount;
    private String domAcct;
    private String domAmt;
    private String domCcy;
    private String authName;
    private String authDate;
    private String remarks;
    private String formPurposeCode;
    private String prefix;
    private String exporterCity;
    private String exporterState;
    private String exporterPhone;
    private String pincode;
    private String exporterCountry;
    private String concFlag;
    private String concPcnt;
    private String congPinCode;


    private String dueDatePay;
    private String collBranchCode;
    private String collBankCode;
    private String collBankName;
    private String otherPinCode;
//*****


    private String exporterName;

    private String exporterAddress;

    private String exporterRcNo;

    private String exporterNepcReg;

    private String otherFirstName;

    private String otherLastName;



    private String otherAddress;

    private String otherCity;

    private String otherState;

    private String congFirstname;

    private String congLastName;

    private String congAddress;

    private String congCity;

    private String congState;

    private String congCountry;


    private String goodsDescription;

    private long totalQuantity;

    private String noOfItems;

    private String otherCountry;

    private String modeOfPackaging;

    private String netWeight;

    private BigDecimal unitPrice;

    private long grossMass;

    private long quantity;

    private BigDecimal freightChanges;

    private String shippedOn;

    private String expectedPortShipment;

    private String code;

    private String totalNetWeight;

    private String totalGrossWeight;

    private String totalValueOfGoods;

    private String currencyCode;

    private String totalFOB;

    private String totalFC;

    private String totalFreightFOB;

    private String portOfDestination;

    private String initiatedBy;

    private Date dateCreated;

    private String formNum;

    private String methodOfPayment;

    private String UserType;

    private String utilization;

    private String cifid;

    private String proformaDate;

    private String proformaNo;

    private String proformaInvoiceStatus;

    private String attachments;

    private String tempFormNumber;

    private boolean guidelineApproval;

    private String tranId;

//    private String fcyAmount;




    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getDueDatePay() {
        return dueDatePay;
    }

    public void setDueDatePay(String dueDatePay) {
        this.dueDatePay = dueDatePay;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public String getTempFormNumber() {
        return tempFormNumber;
    }

    public void setTempFormNumber(String tempFormNumber) {
        this.tempFormNumber = tempFormNumber;
    }

    @OneToMany(cascade = CascadeType.ALL)
    private Collection<FormNxpDocument> documents;

    @OneToOne
    private Branch docSubBranch;

    public Collection<FormNxpDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(Collection<FormNxpDocument> documents) {
        this.documents = documents;
    }

    public Branch getDocSubBranch() {
        return docSubBranch;
    }

    public void setDocSubBranch(Branch docSubBranch) {
        this.docSubBranch = docSubBranch;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }


    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public String getTransferAccount() {
        return transferAccount;
    }

    public void setTransferAccount(String transferAccount) {
        this.transferAccount = transferAccount;
    }

    public String getDomAcct() {
        return domAcct;
    }

    public void setDomAcct(String domAcct) {
        this.domAcct = domAcct;
    }

    public String getDomAmt() {
        return domAmt;
    }

    public void setDomAmt(String domAmt) {
        this.domAmt = domAmt;
    }

    public String getDomCcy() {
        return domCcy;
    }

    public void setDomCcy(String domCcy) {
        this.domCcy = domCcy;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getFormPurposeCode() {
        return formPurposeCode;
    }

    public void setFormPurposeCode(String formPurposeCode) {
        this.formPurposeCode = formPurposeCode;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getExporterCity() {
        return exporterCity;
    }

    public void setExporterCity(String exporterCity) {
        this.exporterCity = exporterCity;
    }

    public String getExporterState() {
        return exporterState;
    }

    public void setExporterState(String exporterState) {
        this.exporterState = exporterState;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getExporterCountry() {
        return exporterCountry;
    }

    public void setExporterCountry(String exporterCountry) {
        this.exporterCountry = exporterCountry;
    }

    public String getConcFlag() {
        return concFlag;
    }

    public void setConcFlag(String concFlag) {
        this.concFlag = concFlag;
    }

    public String getConcPcnt() {
        return concPcnt;
    }

    public void setConcPcnt(String concPcnt) {
        this.concPcnt = concPcnt;
    }

    public String getCongPinCode() {
        return congPinCode;
    }

    public void setCongPinCode(String congPinCode) {
        this.congPinCode = congPinCode;
    }



    public String getCollBranchCode() {
        return collBranchCode;
    }

    public void setCollBranchCode(String collBranchCode) {
        this.collBranchCode = collBranchCode;
    }

    public String getCollBankCode() {
        return collBankCode;
    }

    public void setCollBankCode(String collBankCode) {
        this.collBankCode = collBankCode;
    }

    public String getCollBankName() {
        return collBankName;
    }

    public void setCollBankName(String collBankName) {
        this.collBankName = collBankName;
    }

    public String getOtherPinCode() {
        return otherPinCode;
    }

    public void setOtherPinCode(String otherPinCode) {
        this.otherPinCode = otherPinCode;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getProformaInvoiceStatus() {
        return proformaInvoiceStatus;
    }

    public void setProformaInvoiceStatus(String proformaInvoiceStatus) {
        this.proformaInvoiceStatus = proformaInvoiceStatus;
    }



    @JsonIgnore
    @OneToMany(mappedBy = "formnxp", targetEntity = FormNxpItem.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<FormNxpItem> items;

    public List<FormNxpItem> getItems() {
        return items;
    }

    public void setItems(List<FormNxpItem> items) {
        this.items = items;
    }

    public String getModeOfPackaging() {
        return modeOfPackaging;
    }

    public void setModeOfPackaging(String modeOfPackaging) {
        this.modeOfPackaging = modeOfPackaging;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterNepcReg() {
        return exporterNepcReg;
    }

    public void setExporterNepcReg(String exporterNepcReg) {
        this.exporterNepcReg = exporterNepcReg;
    }

    public String getOtherFirstName() {
        return otherFirstName;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public long getGrossMass() {
        return grossMass;
    }

    public void setGrossMass(long grossMass) {
        this.grossMass = grossMass;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getFreightChanges() {
        return freightChanges;
    }

    public void setFreightChanges(BigDecimal freightChanges) {
        this.freightChanges = freightChanges;
    }

    public void setOtherFirstName(String otherFirstName) {
        this.otherFirstName = otherFirstName;
    }

    public String getOtherLastName() {
        return otherLastName;
    }

    public void setOtherLastName(String otherLastName) {
        this.otherLastName = otherLastName;
    }

    public String getOtherAddress() {
        return otherAddress;
    }

    public void setOtherAddress(String otherAddress) {
        this.otherAddress = otherAddress;
    }

    public String getOtherCity() {
        return otherCity;
    }

    public void setOtherCity(String otherCity) {
        this.otherCity = otherCity;
    }

    public String getOtherState() {
        return otherState;
    }

    public void setOtherState(String otherState) {
        this.otherState = otherState;
    }

    public String getOtherCountry() {
        return otherCountry;
    }

    public void setOtherCountry(String otherCountry) {
        this.otherCountry = otherCountry;
    }

    public String getProformaDate() {
        return proformaDate;
    }

    public void setProformaDate(String proformaDate) {
        this.proformaDate = proformaDate;
    }

    public String getProformaNo() {
        return proformaNo;
    }

    public void setProformaNo(String proformaNo) {
        this.proformaNo = proformaNo;
    }

    public String getGoodsDescription() {
        return goodsDescription;
    }

    public void setGoodsDescription(String goodsDescription) {
        this.goodsDescription = goodsDescription;
    }

    public long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getExpectedPortShipment() {
        return expectedPortShipment;
    }

    public void setExpectedPortShipment(String expectedPortShipment) {
        this.expectedPortShipment = expectedPortShipment;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCongFirstname() {
        return congFirstname;
    }

    public void setCongFirstname(String congFirstname) {
        this.congFirstname = congFirstname;
    }

    public String getCongAddress() {
        return congAddress;
    }

    public void setCongAddress(String congAddress) {
        this.congAddress = congAddress;
    }

    public String getCongCity() {
        return congCity;
    }

    public void setCongCity(String congCity) {
        this.congCity = congCity;
    }

    public String getCongState() {
        return congState;
    }

    public void setCongState(String congState) {
        this.congState = congState;
    }

    public String getCongCountry() {
        return congCountry;
    }

    public void setCongCountry(String congCountry) {
        this.congCountry = congCountry;
    }

    public String getCongLastName() {
        return congLastName;
    }

    public void setCongLastName(String congLastName) {
        this.congLastName = congLastName;
    }

    public String getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(String noOfItems) {
        this.noOfItems = noOfItems;
    }

    public String getTotalNetWeight() {
        return totalNetWeight;
    }

    public void setTotalNetWeight(String totalNetWeight) {
        this.totalNetWeight = totalNetWeight;
    }

    public String getTotalGrossWeight() {
        return totalGrossWeight;
    }

    public void setTotalGrossWeight(String totalGrossWeight) {
        this.totalGrossWeight = totalGrossWeight;
    }

    public String getTotalValueOfGoods() {
        return totalValueOfGoods;
    }

    public void setTotalValueOfGoods(String totalValueOfGoods) {
        this.totalValueOfGoods = totalValueOfGoods;
    }

    public String getTotalFOB() {
        return totalFOB;
    }

    public void setTotalFOB(String totalFOB) {
        this.totalFOB = totalFOB;
    }

    public String getTotalFC() {
        return totalFC;
    }

    public void setTotalFC(String totalFC) {
        this.totalFC = totalFC;
    }

    public String getTotalFreightFOB() {
        return totalFreightFOB;
    }

    public void setTotalFreightFOB(String totalFreightFOB) {
        this.totalFreightFOB = totalFreightFOB;
    }

    public String getPortOfDestination() {
        return portOfDestination;
    }

    public void setPortOfDestination(String portOfDestination) {
        this.portOfDestination = portOfDestination;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getFormNum() {
        return formNum;
    }

    public void setFormNum(String formNum) {
        this.formNum = formNum;
    }

    public String getShippedOn() {
        return shippedOn;
    }

    public void setShippedOn(String shippedOn) {
        this.shippedOn = shippedOn;
    }

    public String getMethodOfPayment() {
        return methodOfPayment;
    }

    public void setMethodOfPayment(String methodOfPayment) {
        this.methodOfPayment = methodOfPayment;
    }

    public String getExporterRcNo() {
        return exporterRcNo;
    }

    public void setExporterRcNo(String exporterRcNo) {
        this.exporterRcNo = exporterRcNo;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public String getUtilization() {
        return utilization;
    }

    public void setUtilization(String utilization) {
        this.utilization = utilization;
    }

    @Override
    public String getCifid() {
        return cifid;
    }

    @Override
    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

//    public String getFcyAmount() {
//        return fcyAmount;
//    }
//
//    public void setFcyAmount(String fcyAmount) {
//        this.fcyAmount = fcyAmount;
//    }
}
