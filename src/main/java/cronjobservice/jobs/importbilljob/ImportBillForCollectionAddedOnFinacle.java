package cronjobservice.jobs.importbilljob;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.services.ExportBillsService;
import cronjobservice.services.ImportBillService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;


@Component
@DisallowConcurrentExecution
public class ImportBillForCollectionAddedOnFinacle implements Job {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CronJobService cronJobService;
    @Autowired
    ImportBillService importBillService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextLc = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextLc.getBean (CronJobService.class);
        ImportBillService importBillService = contextLc.getBean (ImportBillService.class);
//        System.out.println("five minute job runing");
        try {
            logger.info(" Import BILL for collection job is starting");
            cronJobService.saveRunningJob(JobCategory.IMPORT_BILL_FOR_COLLECTION_ADDED_ON_FINACLE.toString(),cronJobService.getCurrentExpression(JobCategory.IMPORT_BILL_FOR_COLLECTION_ADDED_ON_FINACLE.toString()));
            importBillService.getImportBillForCollectionAddedOnFinacle();
            cronJobService.updateRunningJob(JobCategory.IMPORT_BILL_FOR_COLLECTION_ADDED_ON_FINACLE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
