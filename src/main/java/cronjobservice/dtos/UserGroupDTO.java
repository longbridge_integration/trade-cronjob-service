package cronjobservice.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

/**
 * Created by Fortune on 5/3/2017.
 */
public class UserGroupDTO {

    @JsonProperty("DT_RowId")
    private Long id;
    private int version;
    private String name;
    private String description;
    private Date dateCreated;

    private List<AdminUserDTO> users;

    private List<BankUserDTO> bankUsers;

    private List<ContactDTO> contacts;

    private List<CodeDTO> customerSegment;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public List<AdminUserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<AdminUserDTO> users) {
        this.users = users;
    }

    public List<BankUserDTO> getBankUsers() {
        return bankUsers;
    }

    public void setBankUsers(List<BankUserDTO> bankUsers) {
        this.bankUsers = bankUsers;
    }

    public List<ContactDTO> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactDTO> contacts) {
        this.contacts = contacts;
    }

    public List<CodeDTO> getCustomerSegment() {
        return customerSegment;
    }

    public void setCustomerSegment(List<CodeDTO> customerSegment) {
        this.customerSegment = customerSegment;
    }
}
