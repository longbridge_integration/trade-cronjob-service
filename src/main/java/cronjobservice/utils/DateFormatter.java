package cronjobservice.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Fortune on 5/17/2017.
 */

public class DateFormatter {

    static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
    static SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");

    public static String format(Date date){

        return dateFormatter.format(date);
    }

    public static Date parse(String date) throws java.text.ParseException
    {
       Date date1=dateFormat.parse(date);

       return date1;
    }

}




