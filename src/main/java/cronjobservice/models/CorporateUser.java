package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import cronjobservice.utils.PrettySerializer;
import org.hibernate.annotations.Where;
import org.hibernate.envers.AuditOverride;
import org.hibernate.envers.Audited;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.io.IOException;


@Entity
@Audited(withModifiedFlag=true)
@AuditOverride(forClass=User.class)
@Where(clause ="del_Flag='N'" )
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"userName"}))
public class CorporateUser extends User implements PrettySerializer {

	protected String isFirstTimeLogon = "Y";

	@Enumerated(EnumType.ORDINAL)
	protected CorpUserType corpUserType;

	@Enumerated(EnumType.ORDINAL)
	protected TokenType tokenType;

	@Nullable
	private boolean admin;

	@ManyToOne
	private Corporate corporate;

	private String assignedCif;

	private String tempPassword;

	public String getIsFirstTimeLogon() {
        return isFirstTimeLogon;
    }

    public void setIsFirstTimeLogon(String isFirstTimeLogon) {
        this.isFirstTimeLogon = isFirstTimeLogon;
    }

    public CorporateUser(){
		this.userType = (UserType.CORPORATE);
	}

	public Corporate getCorporate() {
		return corporate;
	}

	public void setCorporate(Corporate corporate) {
		this.corporate = corporate;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public CorpUserType getCorpUserType() {
		return corpUserType;
	}

	public void setCorpUserType(CorpUserType corpUserType) {
		this.corpUserType = corpUserType;
	}

	public TokenType getTokenType() {
		return tokenType;
	}

	public void setTokenType(TokenType tokenType) {
		this.tokenType = tokenType;
	}

	@Override
	public int hashCode(){
		return super.hashCode();
	}

	@Override
	public boolean equals(Object o){
		return super.equals(o);
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

    public String getAssignedCif() {
        return assignedCif;
    }

    public void setAssignedCif(String assignedCif) {
        this.assignedCif = assignedCif;
    }

    @Override
	@JsonIgnore
	public JsonSerializer<CorporateUser> getSerializer() {
		return new JsonSerializer<CorporateUser>() {
			@Override
			public void serialize(CorporateUser value, JsonGenerator gen, SerializerProvider serializers)
					throws IOException {

				gen.writeStartObject();
				gen.writeStringField("Corporate Name", value.corporate.getName());
				gen.writeStringField("Username", value.userName);
				gen.writeStringField("First Name", value.firstName);
				gen.writeStringField("Last Name", value.lastName);
				gen.writeStringField("Email", value.email);
				gen.writeStringField("Phone", value.phoneNumber);
				gen.writeStringField("Assigned Cif", value.assignedCif);
				gen.writeStringField("Token Type", value.tokenType.name());
				String status =null;
				if ("A".equals(value.status))
					status = "Active";
				else if ("I".equals(value.status))
					status = "Inactive";
				else if ("L".equals(value.status))
					status = "Locked";
				gen.writeStringField("Status", status);
				gen.writeStringField("Role", value.role.getName());
				if("MULTI".equals(corporate.getCorporateType())) {
					if(value.corpUserType!=null) {
						gen.writeStringField("User Type", value.corpUserType.name());
					}
				}
//				if("MULTI".equals(corporate.getCorporateType())) {
//					gen.writeBooleanField("Is Admin", value.admin);
//				}
				gen.writeEndObject();
			}
		};
	}

}
