package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import cronjobservice.utils.PrettySerializer;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.IOException;
import java.util.*;

/**
 * Created by Fortune on 3/29/2017.
 */
@Entity
@Audited(withModifiedFlag=true)
@Where(clause ="del_Flag='N'" )
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"corporateId"}))

public class Corporate extends AbstractEntity implements PrettySerializer {

    private String rcNumber;
    private String customerId;
    private String corporateType;
    private String corporateId;
    private String name;
    private String email;
    private String address;
    private String town;
    private String state;
    private String country;
    private String phone;
    private String status ;
    private Date createdOnDate;
    private String bvn;

    @OneToMany(mappedBy = "corporate")
    @JsonIgnore
    private List<BulkTransfer> transfers;

    @OneToMany
    @JsonIgnore
    Set<CorporateRole> corporateRoles;

    @OneToMany(mappedBy = "corporate")
    @JsonIgnore
    private List<CorporateUser> users;

    @OneToMany
    @JsonIgnore
    private Collection<CorpLimit> corpLimits;

    @OneToMany
    @JsonIgnore
    List<CorpTransRule> corpTransRules;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "corporate_account", joinColumns =
    @JoinColumn(name = "corporate_id", referencedColumnName = "id"), inverseJoinColumns =
    @JoinColumn(name = "account_id", referencedColumnName = "id") )
    private List<Account> accounts;

    @OneToOne
    private Code customerSegment;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Code getCustomerSegment() {
        return customerSegment;
    }

    public void setCustomerSegment(Code customerSegment) {
        this.customerSegment = customerSegment;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<BulkTransfer> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<BulkTransfer> transfers) {
        this.transfers = transfers;
    }

    public Collection<CorpLimit> getCorpLimits() {
        return corpLimits;
    }

    public void setCorpLimits(Collection<CorpLimit> corpLimits) {
        this.corpLimits = corpLimits;
    }

    public Set<CorporateRole> getCorporateRoles() {
        return corporateRoles;
    }

    public void setCorporateRoles(Set<CorporateRole> corporateRoles) {
        this.corporateRoles = corporateRoles;
    }

    public List<CorpTransRule> getCorpTransRules() {
        return corpTransRules;
    }

    public void setCorpTransRules(List<CorpTransRule> corpTransRules) {
        this.corpTransRules = corpTransRules;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getRcNumber() {
        return rcNumber;
    }

    public void setRcNumber(String rcNumber) {
        this.rcNumber = rcNumber;
    }

    public String getCorporateType() {
        return corporateType;
    }

    public void setCorporateType(String corporateType) {
        this.corporateType = corporateType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

	public List<CorporateUser> getUsers() {
		return users;
	}

	public void setUsers(List<CorporateUser> users) {
		this.users = users;
	}

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public Date getCreatedOnDate() {
        return createdOnDate;
    }

    public void setCreatedOnDate(Date createdOnDate) {
        this.createdOnDate = createdOnDate;
    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
	public String toString() {
		return "Corporate [rcNumber=" + rcNumber + ", customerId=" + customerId + ", corporateType=" + corporateType
				+ ", name=" + name + ", email=" + email + ", address=" + address + ", status=" + status
				+ ", createdOnDate=" + createdOnDate + ", bvn=" + bvn + "]";
	}

	@Override @JsonIgnore
   	public List<String> getDefaultSearchFields() {
   		return Arrays.asList("name", "rcNumber","customerId");
   	}

    @Override @JsonIgnore
    public JsonSerializer<Corporate> getSerializer() {
        return new JsonSerializer<Corporate>() {
            @Override
            public void serialize(Corporate value, JsonGenerator gen, SerializerProvider serializers)
                    throws IOException {
                gen.writeStartObject();
                gen.writeStringField("Name",value.name);
                gen.writeStringField("Customer Id",value.customerId);
                gen.writeStringField("Type",value.corporateType);
                gen.writeStringField("RC Number",value.rcNumber);
                gen.writeStringField("Status",getStatusDescription(value.status));

                gen.writeEndObject();
            }
        };
    }

    private  String getStatusDescription(String status){
        String description =null;
        if ("A".equals(status))
            description = "Active";
        else if ("I".equals(status))
            description = "Inactive";
        else if ("L".equals(status))
            description = "Locked";
        return description;
    }
}
