package cronjobservice.dtos;

import cronjobservice.models.WorkFlow;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 9/20/2017.
 */
public class FormNxpDTO {

    // @JsonProperty("DT_RowId")
    private Long id;

    private int version;

    private String submitAction;

    private String exporterName;

    private String exporterAddress;

    private String exporterRcNo;

    private String exporterNepcReg;

    private String otherFirstName;

    private String otherLastName;

    private String otherAddress;

    private String otherCity;

    private String otherState;

    private String congFirstname;

    private String congLastName;

    private String congAddress;

    private String congCity;

    private String congState;

    private String congCountry;

    private String goodsDescription;

    private long totalQuantity;

    private String noOfItems;

    private String otherCountry;

    private String modeOfPackaging;

    private String netWeight;

    private BigDecimal unitPrice;

    private long grossMass;

    private long quantity;

    private BigDecimal freightChanges;

    private String shippedOn;

    private String expectedPortShipment;

    private String code;

    private String status;

    private  String cifid;

    private String totalNetWeight;

    private String totalGrossWeight;

    private String totalValueOfGoods;

    private String currencyCode;

    private String totalFOB;

    private String totalFC;

    private String totalFreightFOB;

    private String portOfDestination;

    private String initiatedBy;

    private long workFlowId;

    private WorkFlow workFlow;
    private String statusDesc;

//    private String formDate;

    private String formNum;

    private Date dateCreated;

    private String methodOfPayment;

    private String hsCode;

    private String comments;

    private String utilization;

    private String convert;

    private String chargeAcct;

    private String transferAcct;

    private String proformaDate;

    private String proformaNo;

    private String proformaInvoiceStatus;

    private String attachments;

    private Date expiryDate;

    private String authDate;

    private Date createdOn;

    private String sortCode;
    private String bankCode;

    private String chargeAccount;
    private String transferAccount;
    private String domAcct;
    private String domAmt;
    private String domCcy;
    private String authName;
    private String remarks;
    private String formPurposeCode;
    private String prefix;
    private String exporterCity;
    private String exporterState;
    private String exporterPhone;
    private String pincode;
    private String exporterCountry;
    private String concFlag;
    private String concPcnt;
    private String congPinCode;
    private String dueDatePay;
    private String collBranchCode;
    private String collBankCode;
    private String collBankName;
    private String otherPinCode;
    private String submitFlag;

    private String tempFormNumber;//trade app no
    private List<FormNxpAttachmentDTO> documents;
    private String branch;
    private String userType;
    private boolean guidelineApproval;
    private String file;
    private String tranId;
    private String fcyAmount;


    public String getFcyAmount() {
        return fcyAmount;
    }

    public void setFcyAmount(String fcyAmount) {
        this.fcyAmount = fcyAmount;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;

    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getTempFormNumber() {
        return tempFormNumber;
    }

    public void setTempFormNumber(String tempFormNumber) {
        this.tempFormNumber = tempFormNumber;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public List<FormNxpAttachmentDTO> getDocuments() {
        return documents;
    }

    public void setDocuments(List<FormNxpAttachmentDTO> documents) {
        this.documents = documents;
    }


    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getProformaInvoiceStatus() {
        return proformaInvoiceStatus;
    }

    public void setProformaInvoiceStatus(String proformaInvoiceStatus) {
        this.proformaInvoiceStatus = proformaInvoiceStatus;
    }

    public String getProformaDate() {
        return proformaDate;
    }

    public void setProformaDate(String proformaDate) {
        this.proformaDate = proformaDate;
    }

    List<FormItemsDTO> items;

    public List<FormItemsDTO> getItems() {
        return items;
    }

    public void setItems(List<FormItemsDTO> items) {
        this.items = items;
    }

    public String getProformaNo() {
        return proformaNo;
    }

    public void setProformaNo(String proformaNo) {
        this.proformaNo = proformaNo;
    }

    public String getModeOfPackaging() {
        return modeOfPackaging;
    }

    public void setModeOfPackaging(String modeOfPackaging) {
        this.modeOfPackaging = modeOfPackaging;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }


    public String getExporterNepcReg() {
        return exporterNepcReg;
    }

    public void setExporterNepcReg(String exporterNepcReg) {
        this.exporterNepcReg = exporterNepcReg;
    }

    public String getOtherFirstName() {
        return otherFirstName;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public long getGrossMass() {
        return grossMass;
    }

    public void setGrossMass(long grossMass) {
        this.grossMass = grossMass;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getFreightChanges() {
        return freightChanges;
    }

    public void setFreightChanges(BigDecimal freightChanges) {
        this.freightChanges = freightChanges;
    }

    public void setOtherFirstName(String otherFirstName) {
        this.otherFirstName = otherFirstName;
    }

    public String getOtherLastName() {
        return otherLastName;
    }

    public void setOtherLastName(String otherLastName) {
        this.otherLastName = otherLastName;
    }

    public String getOtherAddress() {
        return otherAddress;
    }

    public void setOtherAddress(String otherAddress) {
        this.otherAddress = otherAddress;
    }

    public String getOtherCity() {
        return otherCity;
    }

    public void setOtherCity(String otherCity) {
        this.otherCity = otherCity;
    }

    public String getOtherState() {
        return otherState;
    }

    public void setOtherState(String otherState) {
        this.otherState = otherState;
    }

    public String getOtherCountry() {
        return otherCountry;
    }

    public void setOtherCountry(String otherCountry) {
        this.otherCountry = otherCountry;
    }


    public String getGoodsDescription() {
        return goodsDescription;
    }

    public void setGoodsDescription(String goodsDescription) {
        this.goodsDescription = goodsDescription;
    }

    public long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getExpectedPortShipment() {
        return expectedPortShipment;
    }

    public void setExpectedPortShipment(String expectedPortShipment) {
        this.expectedPortShipment = expectedPortShipment;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCongFirstname() {
        return congFirstname;
    }

    public void setCongFirstname(String congFirstname) {
        this.congFirstname = congFirstname;
    }

    public String getCongAddress() {
        return congAddress;
    }

    public void setCongAddress(String congAddress) {
        this.congAddress = congAddress;
    }

    public String getCongCity() {
        return congCity;
    }

    public void setCongCity(String congCity) {
        this.congCity = congCity;
    }

    public String getCongState() {
        return congState;
    }

    public void setCongState(String congState) {
        this.congState = congState;
    }

    public String getCongCountry() {
        return congCountry;
    }

    public void setCongCountry(String congCountry) {
        this.congCountry = congCountry;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public String getCongLastName() {
        return congLastName;
    }

    public void setCongLastName(String congLastName) {
        this.congLastName = congLastName;
    }

    public String getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(String noOfItems) {
        this.noOfItems = noOfItems;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getTotalNetWeight() {
        return totalNetWeight;
    }

    public void setTotalNetWeight(String totalNetWeight) {
        this.totalNetWeight = totalNetWeight;
    }

    public String getTotalGrossWeight() {
        return totalGrossWeight;
    }

    public void setTotalGrossWeight(String totalGrossWeight) {
        this.totalGrossWeight = totalGrossWeight;
    }

    public String getTotalValueOfGoods() {
        return totalValueOfGoods;
    }

    public void setTotalValueOfGoods(String totalValueOfGoods) {
        this.totalValueOfGoods = totalValueOfGoods;
    }

    public String getTotalFOB() {
        return totalFOB;
    }

    public void setTotalFOB(String totalFOB) {
        this.totalFOB = totalFOB;
    }

    public String getTotalFC() {
        return totalFC;
    }

    public void setTotalFC(String totalFC) {
        this.totalFC = totalFC;
    }

    public String getTotalFreightFOB() {
        return totalFreightFOB;
    }

    public void setTotalFreightFOB(String totalFreightFOB) {
        this.totalFreightFOB = totalFreightFOB;
    }

    public String getPortOfDestination() {
        return portOfDestination;
    }

    public void setPortOfDestination(String portOfDestination) {
        this.portOfDestination = portOfDestination;
    }

    public String getSubmitAction() {
        return submitAction;
    }

    public void setSubmitAction(String submitAction) {
        this.submitAction = submitAction;
    }

    public String getExporterRcNo() {
        return exporterRcNo;
    }

    public void setExporterRcNo(String exporterRcNo) {
        this.exporterRcNo = exporterRcNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public long getWorkFlowId() {
        return workFlowId;
    }

    public void setWorkFlowId(long workFlowId) {
        this.workFlowId = workFlowId;
    }

    public WorkFlow getWorkFlow() {
        return workFlow;
    }

    public void setWorkFlow(WorkFlow workFlow) {
        this.workFlow = workFlow;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }


//    public String getFormDate() {
//        return formDate;
//    }
//
//    public void setFormDate(String formDate) {
//        this.formDate = formDate;
//    }

    public String getFormNum() {
        return formNum;
    }

    public void setFormNum(String formNum) {
        this.formNum = formNum;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getShippedOn() {
        return shippedOn;
    }

    public void setShippedOn(String shippedOn) {
        this.shippedOn = shippedOn;
    }

    public String getMethodOfPayment() {
        return methodOfPayment;
    }

    public void setMethodOfPayment(String methodOfPayment) {
        this.methodOfPayment = methodOfPayment;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUtilization() {
        return utilization;
    }

    public void setUtilization(String utilization) {
        this.utilization = utilization;
    }

    public String getConvert() {
        return convert;
    }

    public void setConvert(String convert) {
        this.convert = convert;
    }

    public String getChargeAcct() {
        return chargeAcct;
    }

    public void setChargeAcct(String chargeAcct) {
        this.chargeAcct = chargeAcct;
    }

    public String getTransferAcct() {
        return transferAcct;
    }

    public void setTransferAcct(String transferAcct) {
        this.transferAcct = transferAcct;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public String getTransferAccount() {
        return transferAccount;
    }

    public void setTransferAccount(String transferAccount) {
        this.transferAccount = transferAccount;
    }

    public String getDomAcct() {
        return domAcct;
    }

    public void setDomAcct(String domAcct) {
        this.domAcct = domAcct;
    }

    public String getDomAmt() {
        return domAmt;
    }

    public void setDomAmt(String domAmt) {
        this.domAmt = domAmt;
    }

    public String getDomCcy() {
        return domCcy;
    }

    public void setDomCcy(String domCcy) {
        this.domCcy = domCcy;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getFormPurposeCode() {
        return formPurposeCode;
    }

    public void setFormPurposeCode(String formPurposeCode) {
        this.formPurposeCode = formPurposeCode;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getExporterCity() {
        return exporterCity;
    }

    public void setExporterCity(String exporterCity) {
        this.exporterCity = exporterCity;
    }

    public String getExporterState() {
        return exporterState;
    }

    public void setExporterState(String exporterState) {
        this.exporterState = exporterState;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getExporterCountry() {
        return exporterCountry;
    }

    public void setExporterCountry(String exporterCountry) {
        this.exporterCountry = exporterCountry;
    }

    public String getConcFlag() {
        return concFlag;
    }

    public void setConcFlag(String concFlag) {
        this.concFlag = concFlag;
    }

    public String getConcPcnt() {
        return concPcnt;
    }

    public void setConcPcnt(String concPcnt) {
        this.concPcnt = concPcnt;
    }

    public String getCongPinCode() {
        return congPinCode;
    }

    public void setCongPinCode(String congPinCode) {
        this.congPinCode = congPinCode;
    }

    public String getDueDatePay() {
        return dueDatePay;
    }

    public void setDueDatePay(String dueDatePay) {
        this.dueDatePay = dueDatePay;
    }

    public String getCollBranchCode() {
        return collBranchCode;
    }

    public void setCollBranchCode(String collBranchCode) {
        this.collBranchCode = collBranchCode;
    }

    public String getCollBankCode() {
        return collBankCode;
    }

    public void setCollBankCode(String collBankCode) {
        this.collBankCode = collBankCode;
    }

    public String getCollBankName() {
        return collBankName;
    }

    public void setCollBankName(String collBankName) {
        this.collBankName = collBankName;
    }

    public String getOtherPinCode() {
        return otherPinCode;
    }

    public void setOtherPinCode(String otherPinCode) {
        this.otherPinCode = otherPinCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getSubmitFlag() {
        return submitFlag;
    }

    public void setSubmitFlag(String submitFlag) {
        this.submitFlag = submitFlag;
    }
}

