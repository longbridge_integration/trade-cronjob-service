package cronjobservice.dtos;

public class CodeTypeDTO {

	private String type;

	public CodeTypeDTO(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
