package cronjobservice.services.implementation;//package longbridge.services.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cronjobservice.api.FormMDetails;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.FormNumber;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * // * Created by SYLVESTER on 9/7/2017.
 */

@Service
public class FormMServiceImpl implements FormMService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    MessageSource messageSource;
    private FormMRepo formMRepo;
//    @Autowired
//    private FormMAmendRepo formMAmendRepo;
//    @Autowired
//    private FormMCancelRepo formMCancelRepo;
    @Autowired
    private PaarRepo paarRepo;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private WorkFlowRepo workFlowRepo;
    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    LcRepo lcRepo;
    @Autowired
    CorporateRepo corporateRepo;
//    @Autowired
//    CommentService commentService;
    @Autowired
    RetailUserRepo retailUserRepo;
    @Autowired
    AccountService accountService;
//    @Autowired
//    LcService lcService;
//    @Autowired
//    ImportBillService importBillService;

    @Autowired
    CodeService codeService;

    @Autowired
    ItemsRepo itemsRepo;

//    @Autowired
//    private UserGroupService userGroupService;

    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private IntegrationService integrationService;
    private Locale locale = LocaleContextHolder.getLocale();
    @Autowired
    CorporateService corporateService;

    @Autowired
    RetailUserService retailUserService;

    @Autowired
    FormMAllocationRepo formMAllocationRepo;

//    @Autowired
//    FinancialInstitutionService financialInstitutionService;

    @Autowired
    EntityManager entityManager;

    @Value("${upload.files}")
    private String UPLOADED_FOLDER;

    @Value("${bankupload.files}")
    private String BANK_UPLOADED_FOLDER;

    private String REAL_PATH;

    @Autowired
    public FormMServiceImpl(FormMRepo formMRepo) {
        this.formMRepo = formMRepo;
    }


    @Override
    public String updateFormM(FormMDTO formMDTO) throws CronJobException {
        FormM formM = formMRepo.findOne(formMDTO.getId());
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        CorporateUser corporateUser = (CorporateUser) doneBy;
        if (doneBy == null) {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
        }
        try {

            entityManager.detach(formM);

            formM = convertDTOTOEntity(formMDTO);
            formM.setVersion(formMDTO.getVersion());
            formM.setInitiatedBy(doneBy.getUserName());
            formM.setDateCreated(new Date());
            formM.setCifid(corporateUser.getCorporate().getCustomerId());
            formM.setAllocatedAmount("0");
            formM.setUtilization("0");
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            formM.setWorkFlow(workFlow);
            formMRepo.save(formM);
            return messageSource.getMessage("form.update.success", null, locale);
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }

    }

    @Override
    public FormMDTO editFormM(Long id) throws CronJobException {
        FormM formM = formMRepo.findOne(id);
        return convertEntityTODTO(formM);


    }

    @Override
    public FormMDTO getInitiatorDetails() throws CronJobException {

        FormMDTO doneBy = new FormMDTO();
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        } else if (users.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) users;
            doneBy.setAppName(corporateUser.getCorporate().getName());
            doneBy.setAppPhone(corporateUser.getPhoneNumber());
            doneBy.setAppEmail(corporateUser.getEmail());
            doneBy.setAppRCNo(corporateUser.getCorporate().getRcNumber());
            doneBy.setAppState(corporateUser.getCorporate().getState());
            doneBy.setCity(corporateUser.getCorporate().getTown());
            doneBy.setAppAddress1(corporateUser.getCorporate().getAddress());


        } else if (users.getUserType().equals(UserType.BANK)) {
            BankUser user = (BankUser) users;
            doneBy.setAppName(user.getFirstName());
        } else {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        return doneBy;

    }

    @Override
    public Boolean updateFormMAllocation(FormM tmsForm, FormM finForm) throws CronJobException {
        try {
            FormMAllocation formMAllocation = new FormMAllocation();
            formMAllocation.setAllocationAccount(finForm.getAllocationAccount());
            formMAllocation.setAllocationAmount(finForm.getAllocationAmount());
            formMAllocation.setAllocationCurrency(finForm.getAllocationCurrency());
            formMAllocation.setExchangeRate(finForm.getExchangeRate());
            formMAllocation.setFormmNumber(finForm.getFormNumber());
            formMAllocation.setDateOfAllocation(finForm.getDateOfAllocation());
            formMAllocation.setDealId(finForm.getDealId());
            formMAllocation.setTranId(finForm.getTranId());
            formMAllocationRepo.save(formMAllocation);
//            tmsForm.setStatus("L");
//            formMRepo.save(tmsForm);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

    @Override
    public Page<FormMDTO> getAllFormMs(Pageable pageDetails)
    {
        Page<FormM> page = formMRepo.findByStatus("P", pageDetails);
        List<FormMDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormMDTO> pageImpl = new PageImpl<FormMDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public String addForm(FormMDTO formMDTO) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        String cif = null;
        if ((doneBy.getUserType()).equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) doneBy;
            cif = retailUser.getCustomerId();
        } else if (doneBy.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) doneBy;
            cif = corporateUser.getCorporate().getCustomerId();
        } else if (doneBy.getUserType().equals(UserType.BANK)) {
            cif = formMDTO.getCifid();
        }
        if (doneBy == null) {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
        }
        try {

            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_WF");
            logger.info("setting the workflow {}", wfSetting);
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            logger.info("workflow {}", workFlow);
            formMDTO.setWorkFlowId(workFlow.getId());
            logger.info("workflow ID {}", workFlow.getId());

            FormM formM = convertDTOTOEntity(formMDTO);

            /**
             * Items table
             */
            List<Items> itemsList = new ArrayList<>();
            List<ItemsDTO> itemsDTOS = formMDTO.getItems();
            Items fitem;
            for (ItemsDTO formItemsDTO : itemsDTOS) {
                fitem = modelMapper.map(formItemsDTO, Items.class);
                fitem.setFormm(formM);
                itemsList.add(fitem);
            }
            formM.setItems(itemsList);
            logger.info("Form M {}", formM.getItems());


            formM.setInitiatedBy(doneBy.getUserName());

            logger.info("FOB VALUE 3 {}", formM.getItems());
            // Extra details to be updated by finacle
            //  formM.setExpiry(formMDTO.getExpiry());
            formM.setFormNumber(FormNumber.getFormMNumber());
            formM.setAllocatedAmount("0");
            formM.setTotalFB(formMDTO.getTotalFB());
            formM.setUtilization("0");
            formM.setInitiatedBy(doneBy.getUserType().toString());
            formM.setExpiry(getFormExpiryDate());
            formM.setDateCreated(new Date());
            formM.setCifid(cif);

//            logger.info("Attachment table");
//            Iterator<FormMAttachment> formMAttachmentIterator = formM.getFormMAttachments().iterator();
//
//            while (formMAttachmentIterator.hasNext()) {
//                FormMAttachment formMAttachment = formMAttachmentIterator.next();
//                if (formMAttachment.getDocumentType() == null) {
//                    formMAttachmentIterator.remove();
//                } else {
//                    formMAttachment.setFormM(formM);
//                }
//            }

            formMRepo.save(formM);

            logger.info("Form M created successfully");
            return messageSource.getMessage("form.add.success", null, null);
        } catch (CronJobException te) {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, null), te);

        }
    }

    @Transactional
    @Override
    public Boolean addFormMFromFinacle(FormMDTO formMDTO) throws CronJobException {

        try {
            BigDecimal utilization = new BigDecimal(formMDTO.getUtilization());
            BigDecimal avaliableAmount = formMDTO.getFormAmount().subtract(utilization);
//            if(exists == null) {
                SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_WF");
                logger.info("setting the workflow value {}", wfSetting.getValue());
                WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
                formMDTO.setWorkFlowId(workFlow.getId());
                logger.info("workflow ID {}", workFlow.getId());

                FormM formM = convertDTOTOEntity(formMDTO);

                /**
                 * Items table
                 */
                List<Items> itemsList = new ArrayList<>();
                if (formMDTO.getItems() != null){
                    List<ItemsDTO> itemsDTOS = formMDTO.getItems();
                    logger.info("This is the Form M Items Added on Finacle {} ", itemsList.size());
                    Items fitem;
                    for (ItemsDTO formItemsDTO : itemsDTOS) {
                        fitem = modelMapper.map(formItemsDTO, Items.class);
                        fitem.setFormm(formM);
                        itemsList.add(fitem);
                        itemsRepo.save(fitem);
                    }
                    formM.setItems(itemsList);
                    logger.info("Form M items {}", formM.getItems().size());
                }


                // Extra details to be updated by finacle
                //  formM.setExpiry(formMDTO.getExpiry());
                if (null==formMDTO.getTempFormNumber() || formMDTO.getTempFormNumber().equals(""))
                {
                    formM.setTempFormNumber(FormNumber.getFormMNumber());
                }
                formM.setInitiatedBy("FINACLE");
//                formM.setExpiry(getFormExpiryDate());
                formM.setAvaliableAmount(avaliableAmount);
                if(null != formM.getStatus()){
                    String status = convertFomMStatus(formM.getStatus());
                    formM.setStatus(status);
                }

//checking for update before saving to TMS
                String newStatus = integrationService.getFormMStatusUpdate(formM.getFormNumber());
//                logger.info("Form M new Status {} ",newStatus);
            if(!newStatus.isEmpty()) {
                String status = convertFomMStatus(newStatus);
                formM.setStatus(status);
            }

                formMRepo.save(formM);
            logger.info("Form M created successfully");
            return true;
        } catch (CronJobException te) {
            te.printStackTrace();
            return false;
        }
    }
    @Transactional
    @Override
    public Boolean updateFormMFromFinacle(FormMDetails formMDetails, FormM formM) throws CronJobException {

        try {
            BigDecimal utilization = new BigDecimal(formMDetails.getUtilization());
            BigDecimal avaliableAmount = formMDetails.getFormAmount().subtract(utilization);
//            if(exists == null) {
                SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_WF");
                logger.info("setting the workflow value {}", wfSetting.getValue());
                WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
                formM.setWorkFlow(workFlow);
                formM.setAvaliableAmount(avaliableAmount);
                if(null != formM.getStatus()){
                    String status = convertFomMStatus(formM.getStatus());
                    formM.setStatus(status);
                }
                if(null != formMDetails.getExpiry()){
                    formM.setExpiry(formMDetails.getExpiry());
                }
                if(null!=formMDetails.getCifid()){
                    formM.setCifid(formMDetails.getCifid());
                }

            //checking for update before saving to TMS
                String newStatus = integrationService.getFormMStatusUpdate(formM.getFormNumber());
            //                logger.info("Form M new Status {} ",newStatus);
            if(!newStatus.isEmpty()) {
                String status = convertFomMStatus(newStatus);
                formM.setStatus(status);
            }

                formMRepo.save(formM);
            logger.info("Form M updated successfully successfully");
            return true;
        } catch (CronJobException te) {
            te.printStackTrace();
            return false;
        }
    }

    private String convertFomMStatus(String status){
    status = status.trim();
//    logger.info("the status of the formm {}",status);
    if("Reject".equalsIgnoreCase(status)){
        return  "D";
    }else if ("Update".equalsIgnoreCase(status)){
        return  "R";
    }else if ("Submit".equalsIgnoreCase(status)){
        return  "S";
    }else if ("Validate".equalsIgnoreCase(status)){
        return  "V";
    }else if ("Register".equalsIgnoreCase(status)||"REGISTERED".equalsIgnoreCase(status)){
        return  "R";
    }else if ("RAR Lock".equalsIgnoreCase(status)){
        return  "RL";
    }else if ("Post Entry".equalsIgnoreCase(status)){
        return  "PE";
    }else if ("Extend Validity".equalsIgnoreCase(status)){
        return  "EV";
    } else if ("O".equalsIgnoreCase(status)|| "Open".equalsIgnoreCase(status)){
        return  "R";
    }else return status;
}
    @Transactional
    @Override
    public Boolean updateFormMAddedOnFinacle(FormMDTO formMDTO) throws CronJobException {

        try {
            BigDecimal utilization = new BigDecimal(formMDTO.getUtilization());
            BigDecimal avaliableAmount = formMDTO.getFormAmount().subtract(utilization);
            FormM exists = formMRepo.findByFormNumber(formMDTO.getFormNumber());
            if(exists == null) {
                SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_WF");
                logger.info("setting the workflow {}", wfSetting);
                WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
                formMDTO.setWorkFlowId(workFlow.getId());
                logger.info("workflow ID {}", workFlow.getId());

                FormM formM = convertDTOTOEntity(formMDTO);

                /**
                 * Items table
                 */
                List<Items> itemsList = new ArrayList<>();
                if (formMDTO.getItems() != null){
                    List<ItemsDTO> itemsDTOS = formMDTO.getItems();
                    logger.info("This is the Form M Items {} ", itemsList);
                    Items fitem;
                    for (ItemsDTO formItemsDTO : itemsDTOS) {
                        fitem = modelMapper.map(formItemsDTO, Items.class);
                        fitem.setFormm(formM);
                        itemsList.add(fitem);
                        itemsRepo.save(fitem);
                    }
                    formM.setItems(itemsList);
                    logger.info("Form M {}", formM.getItems());
                }

                formM.setInitiatedBy(formMDTO.getAppName());

                logger.info("FOB VALUE 3 {}", formM.getItems());
                // Extra details to be updated by finacle
                //  formM.setExpiry(formMDTO.getExpiry());
                if (formMDTO.getTempFormNumber() == null || formMDTO.getTempFormNumber().equals(""))
                {
                    formM.setTempFormNumber(FormNumber.getFormMNumber());
                }
                formM.setInitiatedBy(UserType.CORPORATE.toString());
                formM.setExpiry(getFormExpiryDate());
                formM.setAvaliableAmount(avaliableAmount);


                String newStatus = integrationService.getFormMStatusUpdate(formM.getFormNumber());
                if("Reject".equals(newStatus)){
                    formM.setStatus("D");
                }else if ("Update".equalsIgnoreCase(newStatus)){
                    formM.setStatus("R");
                }else if ("Submit".equalsIgnoreCase(newStatus)){
                    formM.setStatus("S");
                }else if ("Validate".equalsIgnoreCase(newStatus)){
                    formM.setStatus("V");
                }else if ("Register".equalsIgnoreCase(newStatus)){
                    formM.setStatus("R");
                }else if ("RAR Lock".equalsIgnoreCase(newStatus)){
                    formM.setStatus("RL");
                }else if ("Post Entry".equalsIgnoreCase(newStatus)){
                    formM.setStatus("PE");
                }else if ("Extend Validity".equalsIgnoreCase(newStatus)){
                    formM.setStatus("EV");
                }
                formMRepo.save(formM);

            }else{

                logger.info("FOrm M already exist, trying to update d record ");
                String newStatusDesc = integrationService.getFormMStatusUpdate(exists.getFormNumber());
                String newStatusCode = "";
                logger.info(" NEW STATUS {}", newStatusDesc);
                if("Reject".equals(newStatusDesc)){
                    newStatusCode = "D";
                }else if ("Update".equalsIgnoreCase(newStatusDesc)){
                    newStatusCode = "R";
                }else if ("Submit".equalsIgnoreCase(newStatusDesc)){
                    newStatusCode = "S";
                }else if ("Validate".equalsIgnoreCase(newStatusDesc)){
                    newStatusCode = "V";
                }else if ("Register".equalsIgnoreCase(newStatusDesc)){
                    newStatusCode = "R";
                }else if ("RAR Lock".equalsIgnoreCase(newStatusDesc)){
                    newStatusCode = "RL";
                }else if ("Post Entry".equalsIgnoreCase(newStatusDesc)){
                    newStatusCode = "PE";
                }else if ("Extend Validity".equalsIgnoreCase(newStatusDesc)){
                    newStatusCode = "EV";
                }else if ("O".equalsIgnoreCase(newStatusDesc)){
                    newStatusCode = "R";
                }
                logger.info(" NEW STATUS {}", newStatusCode);

                if (exists.getStatus().equalsIgnoreCase(newStatusCode)){
                    logger.info("" + "Form M already exist");
                }
                else {
                    logger.info("" + "Performing Update");

                    SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_WF");
                    logger.info("setting the workflow {}", wfSetting);
                    WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
                    formMDTO.setWorkFlowId(workFlow.getId());
                    logger.info("workflow ID {}", workFlow.getId());

                    FormM newForm = convertDTOTOEntity(formMDTO);
                    newForm.setId(exists.getId());
                    newForm.setTempFormNumber(exists.getTempFormNumber());
                    newForm.setStatus(newStatusCode);
                    newForm.setAvaliableAmount(avaliableAmount);
                    newForm.setPaarList(exists.getPaarList());
                    newForm.setVersion(exists.getVersion());
                    formMRepo.save(newForm);

                    logger.info("FORM M SAVED SUCCESSFULLY");

                    try {
                        Object updateForm = updateObject(exists, newForm);
                        FormM updatedProduct = (FormM) updateForm;
                        updatedProduct.setStatus(newStatusCode);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            logger.info("Form M created successfully");
            return true;
        } catch (CronJobException te) {
            te.printStackTrace();
            return false;
        }
    }

    private static Map<Class, PropertyDescriptor[]> descriptorsMap = new HashMap<Class, PropertyDescriptor[]>();

    private static Object updateObject(Object originalObject, Object updateObject)
            throws InvocationTargetException, IllegalAccessException
    {
        if (updateObject == null || originalObject == null)
        {
            throw new NullPointerException("A null paramter was passed into updateObject");
        }

        Object original = new Object();



        //Only go through the process if the objects are not the same reference
        if (originalObject != updateObject)
        {
            Class orignalClass = originalObject.getClass();
            Class updateClass = updateObject.getClass();
            //you may want to work this check if you need to handle polymorphic relations
            if (!orignalClass.equals(updateClass))
            {
                throw new IllegalArgumentException("Received parameters are not the same type of class, but must be");
            }


            PropertyDescriptor[] descriptors = descriptorsMap.get(orignalClass);
            if (descriptors == null)
            {
                descriptors = PropertyUtils.getPropertyDescriptors(orignalClass);
                descriptorsMap.put(orignalClass, descriptors);
            }


            for (PropertyDescriptor descriptor : descriptors)
            {
                if (PropertyUtils.isReadable(originalObject, descriptor.getName())
                        && PropertyUtils.isWriteable(originalObject, descriptor.getName()))
                {
                    Method readMethod = descriptor.getReadMethod();
                    Object value = readMethod.invoke(updateObject);
                    if (value != null)
                    {
                        Method writeMethod = descriptor.getWriteMethod();
                        original = writeMethod.invoke(originalObject, value);

                    }
                }
            }
        }

        System.out.println("OBJECT " + original);
         return original;
    }

//    @Override
//    public String amendFormM(FormMDTO formMDTO) throws CronJobException {
//        try {
//
//            FormMAmend formMAmend = formMAmendRepo.findByFormNumber(formMDTO.getFormNumber());
//            if (formMAmend != null && ("S".equals(formMAmend.getStatus()) || "P".equals(formMAmend.getStatus()))) {
//                logger.error("Pending Amendment Request already exists");
//                throw new CronJobException(messageSource.getMessage("lc.pending.amendment", null, locale));
//            }
//
//            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                    .getPrincipal();
//            User doneBy = principal.getUser();
//
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_AMEND_WF");
//            logger.info("setting the workflow {}", wfSetting);
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            logger.info("workflow {}", workFlow);
//            formMDTO.setWorkFlowId(workFlow.getId());
//            logger.info("workflow ID {}", workFlow.getId());
//
//            FormMAmend formM = convertFormMDTOToAmendEntity(formMDTO);
//            formM.setUserType(doneBy.getUserType());
//            formM.setInitiatedBy(doneBy.getUserName());
//            formM.setDateCreated(new Date());
//            formMAmendRepo.save(formM);
//            logger.info("Amendment Submited for LC with Id {}", formM.getFormNumber());
//            return messageSource.getMessage("state.update.success", null, locale);
//        } catch (CronJobException e) {
//            throw e;
//        } catch (Exception e) {
//            throw new CronJobException(messageSource.getMessage("formm.update.failure", null, locale), e);
//        }
//    }

//    @Override
//    public String cancelForm(String formNumber) throws CronJobException {
//        try {
//
//            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                    .getPrincipal();
//            User doneBy = principal.getUser();
//            FormM formM = formMRepo.findByFormNumber(formNumber);
//
//            FormMCancellation formMCancel = formMCancelRepo.findByFormNumberAndCifid(formNumber, formM.getCifid());
//            if (formMCancel != null){
//                throw new CronJobException(messageSource.getMessage("formm.cancel.duplicate", null, locale));
//            }
//
//            FormMCancellation formMCancellation = new FormMCancellation();
//            formMCancellation.setCifid(formM.getCifid());
//            formMCancellation.setDateCreated(new Date());
//            formMCancellation.setUserType(doneBy.getUserType());
//            formMCancellation.setInitiatedBy(doneBy.getUserName());
//            formMCancellation.setStatus("S");
//            formMCancellation.setFormNumber(formNumber);
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_CANCEL_WF");
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            formMCancellation.setWorkFlow(workFlow);
//
//            formMCancelRepo.save(formMCancellation);
//            return messageSource.getMessage("state.update.success", null, locale);
//
//        } catch (CronJobException e) {
//            throw e;
//        } catch (Exception e) {
//            throw new CronJobException(messageSource.getMessage("formm.update.failure", null, locale), e);
//        }
//    }

    @Override
    public FormMDTO getFormM(Long id) throws CronJobException {
        FormM formM = formMRepo.findOne(id);
        FormMDTO formMDTO=convertEntityTODTO(formM);
//        if (!"".equalsIgnoreCase(formMDTO.getCountryOfOrigin())) {
//            String origin = formM.getCountryOfOrigin();
//            if(!"".equalsIgnoreCase(origin)) {
//                if(codeService.getByTypeAndCode("COUNTRY", origin).getDescription()!=null);
//                formMDTO.setCountryOfOrigin(codeService.getByTypeAndCode("COUNTRY", origin).getDescription());
//            }
//            }
//        if (!"".equalsIgnoreCase(formMDTO.getCountryOfSupply())) {
//            String supply = formM.getCountryOfOrigin();
//           if(!"".equalsIgnoreCase(supply)) {
//               if(codeService.getByTypeAndCode("COUNTRY", supply).getDescription()!=null) {
//                   formMDTO.setCountryOfSupply(codeService.getByTypeAndCode("COUNTRY", supply).getDescription());
//               }
//           }
//        }

//        if (!"".equalsIgnoreCase(formMDTO.getBeneCountry())) {
//            String beneCountry = formM.getBeneCountry();
//            if(!"".equalsIgnoreCase(beneCountry)) {
//                if(codeService.getByTypeAndCode("COUNTRY", beneCountry).getDescription()!=null && !"".equalsIgnoreCase(codeService.getByTypeAndCode("COUNTRY", beneCountry).getDescription())) {
//                    formMDTO.setBeneCountry(codeService.getByTypeAndCode("COUNTRY", beneCountry).getDescription());
//                }
//            }
//        }
//        if(!"".equalsIgnoreCase(formMDTO.getPrefex())) {
//            if (formMDTO.getPrefex().equalsIgnoreCase("CB")) {
//
//                formMDTO.setPrefex("INSPECTABLE GOODS");
//            } else if (formMDTO.getPrefex().equalsIgnoreCase("BA")) {
//                formMDTO.setPrefex("NON INSPECTABLE GOODS");
//            }
//        }
//        boolean value=formMDTO.isForex();
//        if(value==true){
//        formMDTO.setKeepPrefix("YES");
//        }
//        else if(value==false){
//            formMDTO.setKeepPrefix("NO");
//        }
//
//        if (!"".equalsIgnoreCase(formMDTO.getPortOfDischarge())) {
//            String portofdischarge = formM.getPortOfDischarge();
//            if(!"".equalsIgnoreCase(portofdischarge)) {
//                if(codeService.getByTypeAndCode("PORT_CODES", portofdischarge).getDescription()!=null) {
//                    formMDTO.setPortOfDischarge(codeService.getByTypeAndCode("PORT_CODES", portofdischarge).getDescription());
//                }
//            }
//        }
//        if (!"".equalsIgnoreCase(formMDTO.getDesignatedBank())) {
//            String designBnk = formM.getDesignatedBank();
//            if(!"".equalsIgnoreCase(designBnk)) {
//                formMDTO.setDesignatedBank(financialInstitutionService.getFinancialInstitutionByCode(designBnk).getInstitutionName());
//            }
//            }

        return formMDTO;
    }

    @Override
    public FormMDTO getFormMByFormNumber(String formNumber) throws CronJobException {
        FormM formM = formMRepo.findByFormNumber(formNumber);
        return convertEntityTODTO(formM);

    }

    @Override
    public Page<FormMDTO> getFormMs(Pageable pageDetails) {

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        CorporateUser corporateUser=(CorporateUser) users;

        Page<FormM> page = formMRepo.findByClosedCifidAndStatus(corporateUser.getCorporate().getCustomerId(),pageDetails);
        List<FormMDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormMDTO> pageImpl = new PageImpl<FormMDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }


    @Override
    public Page<FormMDTO> getClosedFormMs(Pageable pageable) {
        Page<FormM> page = formMRepo.findByStatusNotNull(pageable);
        List<FormMDTO> formMDTOS = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormMDTO> page1 = new PageImpl<FormMDTO>(formMDTOS, pageable, t);
        return page1;
    }


//    @Override
//    public Page<FormMDTO> getFormMsForBankUser(Pageable pageable) throws CronJobException {
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User loggedInUser = principal.getUser();
//        BankUser bankUser = (BankUser) loggedInUser;
//
//        //get all groups
//        List<UserGroupDTO> allUserGroups = userGroupService.getGroups();
//        logger.info("GROUPS {}", allUserGroups.toString());
//        List<CodeDTO> customerSegmentsInUserGroups = new ArrayList<>();
//        for (UserGroupDTO userGroupDTO : allUserGroups){
//
//            List<BankUserDTO> bankUserList = userGroupService.getBankUsers(userGroupDTO.getId());
//            if (!bankUserList.isEmpty())
//            {
//
//                for (BankUserDTO bu : bankUserList){
//                    if (bu.getId().equals(bankUser.getId()))
//                    {
//                        logger.info("CUSTOMER {}", userGroupService.getCustomerSegments(userGroupDTO.getId()).toString());
//                        customerSegmentsInUserGroups.addAll(userGroupService.getCustomerSegments(userGroupDTO.getId()));
//                    }
//                }
//            }
//        }
//
//        logger.info("CUSTOMER LIST {}", customerSegmentsInUserGroups);
//
//        //get  customers in Customer Segments of user Groups
//        List<CorporateDTO> corporateCustomers = new ArrayList<>();
//
//        for (CodeDTO custSegment : customerSegmentsInUserGroups){
//            corporateCustomers.addAll(corporateService.getByCustomerSegment(custSegment));
//        }
//
//        //get submitted forms
//        List<FormMDTO> formMDTOS = new ArrayList<>();
//
//        for (CorporateDTO corporate : corporateCustomers){
//            Page<FormM> corporateForms = formMRepo.findByCifidAndStatus(corporate.getCustomerId(),"P", pageable);
//            formMDTOS.addAll(convertEntitiesToDTOs(corporateForms.getContent()));
//        }
//
//        long t=formMDTOS.size();
//        Page<FormMDTO> page1=new PageImpl<FormMDTO>(formMDTOS,pageable,t);
//        return page1;
//    }
//*************
//    @Override
//    public Page<FormMDTO> getOpenFormMsForBankUser(Pageable pageable) {
//
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User loggedInUser = principal.getUser();
//        BankUser bankUser = (BankUser) loggedInUser;
//
//        //get all groups
//        List<UserGroupDTO> allUserGroups = userGroupService.getGroups();
//
//        //get Groups user belongs to and customer Segments
//        List<CodeDTO> customerSegmentsInUserGroups = new ArrayList<>();
//        for (UserGroupDTO userGroupDTO : allUserGroups){
//
//            List<BankUserDTO> bankUserList = userGroupService.getBankUsers(userGroupDTO.getId());
//            if (!bankUserList.isEmpty()){
//                logger.info("BANK USER LIST {}", bankUserList);
//                logger.info("BANK USER {}", bankUser);
//                for (BankUserDTO bu : bankUserList){
//                    if (bu.getId().equals(bankUser.getId())) {
//                        //userBelongsTo.add(userGroupDTO);
//                        customerSegmentsInUserGroups.addAll(userGroupService.getCustomerSegments(userGroupDTO.getId()));
//                    }
//                }
//            }
//        }
//
//        logger.info("CUSTOMER LIST {}", customerSegmentsInUserGroups);
//
//        //get  customers in Customer Segments of user Groups
//
//        List<CorporateDTO> corporateCustomers = new ArrayList<>();
//        for (CodeDTO custSegment : customerSegmentsInUserGroups){
//
//            corporateCustomers.addAll(corporateService.getByCustomerSegment(custSegment));
//        }
//        //get submitted forms
//        List<FormMDTO> formMDTOS = new ArrayList<>();
//
//        for (CorporateDTO corporate : corporateCustomers)
//        {
//            Page<FormM> corporateForms = formMRepo.findByOpenCifidAndStatus(corporate.getCustomerId(),"S", pageable);
//            formMDTOS.addAll(convertEntitiesToDTOs(corporateForms.getContent()));
//        }
//
//        long t=formMDTOS.size();
//        Page<FormMDTO> page1=new PageImpl<FormMDTO>(formMDTOS,pageable,t);
//        return page1;
//    }

    public List<CommentDTO> getComments(Long id) throws CronJobException
    {
        FormM formM=formMRepo.findOne(id);
        List<CommentDTO> allComment=new ArrayList<>();
//
//        if(formM!=null) {
//
//            List<CommentDTO> lcComments = lcService.getLcComments(formM);
//            logger.info("getting the lc comment{}", lcComments);
//            List<CommentDTO> billsComments = importBillService.getBillComments(formM);
//            logger.info("getting the bill comment{}", billsComments);
//            List<CommentDTO> commentDTOList = new ArrayList<>();
//            commentDTOList = convertEntitiestoDTOs(formM.getComment(),formM);
//            allComment.addAll(commentDTOList);
//            allComment.addAll(billsComments);
//            allComment.addAll(lcComments);
//        }
        return  allComment;
    }


    @Override
    public long getTotalExpiredFormM() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User logginUser = principal.getUser();
        long totalNumberExpiredFormM=0;
        if(logginUser.getUserType().equals(UserType.CORPORATE))
        {
            CorporateUser corporateUser=(CorporateUser) logginUser;
            totalNumberExpiredFormM=formMRepo.countByCifidAndExpiryStatus(corporateUser.getCorporate().getCustomerId(),"Y");
        }

        return totalNumberExpiredFormM;
    }

    @Override
    public Page<FormMDTO> getListExpiredFormM(Pageable pageDetails) {

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        CorporateUser corporateUser=(CorporateUser) doneBy;
        String cifid=((CorporateUser) doneBy).getCorporate().getCustomerId();
        Page<FormM> page=formMRepo.findByCifidAndExpiryStatus(cifid,"Y",pageDetails);
        List<FormMDTO> formMDTOS=convertEntitiesToDTOs(page.getContent());
        long t=page.getTotalElements();
        Page<FormMDTO> page1=new PageImpl<FormMDTO>(formMDTOS,pageDetails,t);
        return page1;
    }

    @Override
    public Page<FormMDTO> getListPendingFormM(Pageable pageable)
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        CorporateUser corporateUser=(CorporateUser) doneBy;
        String cifid=((CorporateUser) doneBy).getCorporate().getCustomerId();
        Page<FormM> page=formMRepo.findByCifidAndExpiryStatus(cifid,"P",pageable);
        List<FormMDTO> formMDTOS=convertEntitiesToDTOs(page.getContent());
        long t=page.getTotalElements();
        Page<FormMDTO> page1=new PageImpl<FormMDTO>(formMDTOS,pageable,t);
        return page1;
    }

    @Override
    public long getTotalPendingFormM() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User logginUser = principal.getUser();
        long totalNumberExpiredFormM=0;
        if(logginUser.getUserType().equals(UserType.CORPORATE))
        {
            CorporateUser corporateUser=(CorporateUser) logginUser;
            totalNumberExpiredFormM=formMRepo.countByCifidAndExpiryStatus(corporateUser.getCorporate().getCustomerId(),"P");
        }

        return totalNumberExpiredFormM;
    }

    @Override
    public long getTotalPendingAndExpiredFormM(long pendingFormM,long expiredFormM) throws CronJobException {
        return pendingFormM+expiredFormM;
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS, FormM formM)
    {
        List<CommentDTO> commentDTOList=new ArrayList<>();


        for(Comments comments:commentDTOS)
        {
            WorkFlowStates workFlowStates=workFlowStatesRepo.findByTypeAndCode(formM.getWorkFlow().getCodeType(),comments.getStatus());
            if(workFlowStates!=null)
            {
              comments.setStatus(workFlowStates.getDescription());
            }
            commentDTOList.add(convertEntityToDTO(comments));
        }

        return  commentDTOList;
    }


    private CommentDTO convertEntityToDTO(Comments comments)
    {
        logger.info("getting form m status {}",comments.getStatus());
        WorkFlowStates workFlowStates=workFlowStatesRepo.findByTypeAndCode("FORM_M",comments.getStatus());
        if(workFlowStates!=null)
        {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments,CommentDTO.class);
    }

    public FormMDTO convertEntityTODTO(FormM formM) {
        FormMDTO formMDTO = modelMapper.map(formM, FormMDTO.class);
//        formM.set
        formMDTO.setWorkFlowId(formM.getWorkFlow().getId());

        if (formM.getDateCreated() != null) {
            formMDTO.setDateCreated(formM.getDateCreated());
        }

        return formMDTO;
    }
 public FormMDTO UpdateFormM(FormM formM) {
        FormMDTO formMDTO = modelMapper.map(formM, FormMDTO.class);
//        formM.set
        formMDTO.setWorkFlowId(formM.getWorkFlow().getId());

        if (formMDTO.getStatus() != null && !(formMDTO.getStatus().equals("P"))) {
            logger.info("form M status {} the workflow {} the type {}", formMDTO.getStatus(), formMDTO.getWorkFlow(), formMDTO.getWorkFlow().getCodeType());
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formMDTO.getWorkFlow().getCodeType(), formMDTO.getStatus());
            logger.info("workFlowStates {}", workFlowStates.toString());
            formMDTO.setStatusDesc(workFlowStates.getDescription());
        }
        if (formMDTO.getStatus().equals("P")) {
            formMDTO.setStatusDesc("Saved");
        }
        if (formM.getDateCreated() != null) {
            formMDTO.setDateCreated(formM.getDateCreated());
        }
        return formMDTO;
    }

    private List<FormMDTO> convertEntitiesToDTOs(Iterable<FormM> formMs) {
        List<FormMDTO> formMDTOs = new ArrayList<FormMDTO>();
        for (FormM formM : formMs) {
            formMDTOs.add(convertEntityTODTO(formM));
        }
        return formMDTOs;
    }

    private FormM convertDTOTOEntity(FormMDTO formMDTO) {

        WorkFlow workFlow = workFlowRepo.findOne(formMDTO.getWorkFlowId());
        formMDTO.setWorkFlow(workFlow);
        FormM formM = modelMapper.map(formMDTO, FormM.class);

        return formM;
    }

    @Override
    public FormHolder createFormHolder(FormMDTO formMDTO, String name) throws CronJobException {

        try {
            FormHolder holder = new FormHolder();
            List<Comments> allComments;
            logger.info("getting the FormHolder");
            FormM formM = convertDTOTOEntity(formMDTO);
            formM.setDateCreated(new Date());
            holder.setObjectData(objectMapper.writeValueAsString(formM));
            holder.setEntityId(formM.getId());
            holder.setEntityName("FormM");
            holder.setAction(name);
            logger.info("getting the form holder {}", holder.getObjectData().toString());
            return holder;

//            FormHolder holder = new FormHolder();
//            FormM formM  = addComments(formMDTO);
//            formM.setDateCreated(new Date());
//            holder.setObjectData(objectMapper.writeValueAsString(formM));
//            holder.setEntityId(formM.getId());
//            holder.setEntityName("FormM");
//            holder.setAction(name);
//            logger.info("getting the formM");
//            return holder;

        } catch (JsonProcessingException e) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }


    }

    @Override
    public Page<FormMDTO> getOpenFormMs(Pageable pageable) {

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        CorporateUser corporateUser = (CorporateUser) users;
        Page<FormM> pages = formMRepo.findByCifidAndStatusP(corporateUser.getCorporate().getCustomerId(), "P", "S", "D", pageable);
        List<FormMDTO> formMDTOS = convertEntitiesToDTOs(pages.getContent());
        long t = pages.getTotalElements();
        Page<FormMDTO> page1 = new PageImpl<FormMDTO>(formMDTOS, pageable, t);
        return page1;
    }

    @Override
    public Page<FormMDTO> getAwaitsFormMs(Pageable pageable) {

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
//        CorporateUser corporateUser = (CorporateUser) users;
        Page<FormM> pages = formMRepo.findByCifidAndStatusSA("P", "S", pageable);
        List<FormMDTO> formMDTOS = convertEntitiesToDTOs(pages.getContent());
        long t = pages.getTotalElements();
        Page<FormMDTO> page1 = new PageImpl<FormMDTO>(formMDTOS, pageable, t);
        return page1;
    }
//    @Override
//    public Page<FormMDTO> getOpenFormMsForBankUser(Pageable pageable) {
//
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User loggedInUser = principal.getUser();
//        BankUser bankUser = (BankUser) loggedInUser;
//
//        //get all groups
//        List<UserGroupDTO> allUserGroups = userGroupService.getGroups();
//
//        //get Groups user belongs to and customer Segments
//        List<CodeDTO> customerSegmentsInUserGroups = new ArrayList<>();
//        for (UserGroupDTO userGroupDTO : allUserGroups) {
//
//            List<BankUserDTO> bankUserList = userGroupService.getBankUsers(userGroupDTO.getId());
//            if (!bankUserList.isEmpty()) {
//                logger.info("BANK USER LIST {}", bankUserList);
//                logger.info("BANK USER {}", bankUser);
//                for (BankUserDTO bu : bankUserList) {
//                    if (bu.getId().equals(bankUser.getId())) {
//                        //userBelongsTo.add(userGroupDTO);
//                        customerSegmentsInUserGroups.addAll(userGroupService.getCustomerSegments(userGroupDTO.getId()));
//                    }
//                }
//            }
//        }
//
//        logger.info("CUSTOMER LIST {}", customerSegmentsInUserGroups);
//
//        //get  customers in Customer Segments of user Groups
//
//        List<CorporateDTO> corporateCustomers = new ArrayList<>();
//        for (CodeDTO custSegment : customerSegmentsInUserGroups) {
//
//            corporateCustomers.addAll(corporateService.getByCustomerSegment(custSegment));
//        }
//
//
//        //get submitted forms
//        List<FormMDTO> formMDTOS = new ArrayList<>();
//
//        for (CorporateDTO corporate : corporateCustomers) {
//            Page<FormM> corporateForms = formMRepo.findByCifidAndStatusNotNull(corporate.getCustomerId(), pageable);
//            logger.info("CORPORATE FORMS {}", corporateForms.getContent());
//            formMDTOS.addAll(convertEntitiesToDTOs(corporateForms.getContent()));
//        }
//
//        long t = formMDTOS.size();
//        Page<FormMDTO> page1 = new PageImpl<FormMDTO>(formMDTOS, pageable, t);
//        return page1;
//    }
//

    @Override
    public FormHolder executeAction(FormMDTO formMDTO, String name) throws CronJobException {

        try {
            FormHolder holder = new FormHolder();
            FormM formM = convertDTOTOEntity(formMDTO);

            holder.setObjectData(objectMapper.writeValueAsString(formM));
            holder.setEntityId(formM.getId());
            holder.setEntityName("FormM");
            holder.setAction(name);
            return holder;
        } catch (JsonProcessingException e) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }


    }

    @Override
    public Page<FormMDTO> getBankFormMs(Pageable pageable) {

        Page<FormM> page = formMRepo.findAll(pageable);
        List<FormMDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormMDTO> pageImpl = new PageImpl<FormMDTO>(dtOs, pageable, t);
        return pageImpl;
    }

    @Override
    public Page<FormMDTO> getBankOpenFormMs(Pageable pageable) {
        Page<FormM> page = formMRepo.findByStatusS("S", pageable);
        List<FormMDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormMDTO> pageImpl = new PageImpl<FormMDTO>(dtOs, pageable, t);
        return pageImpl;
    }


    @Override
    public List<FormItemsDTO> getFormItems(Long formId) {
        FormM formM = formMRepo.findOne(formId);
        List<Items> fitem = formM.getItems();
        List<FormItemsDTO> formItems = new ArrayList<FormItemsDTO>();
        for (Items items : fitem) {
            FormItemsDTO formItemsDTO = modelMapper.map(items, FormItemsDTO.class);
            formItems.add(formItemsDTO);
        }
        return formItems;
    }

    @Override
    public List<FormMDTO> getCustomerClosedForms() {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        CorporateUser corporateUser = (CorporateUser) users;
        List<FormMDTO> formMDTOS = convertEntitiesToDTOs(formMRepo.findByCifidAndStatus(corporateUser.getCorporate().getCustomerId(), "C"));
        logger.info("closed {}", formMDTOS);
        return formMDTOS;
    }

    @Override
    public List<FormMDTO> getCustomerClosedFormsByCif(String cifid) {
        List<FormMDTO> formMDTOS = convertEntitiesToDTOs(formMRepo.findByCifidAndStatus(cifid, "C"));
        logger.info("closed {}", formMDTOS);
        return formMDTOS;
    }

    @Override
    public Page<FormMDTO> getCustomerAllClosedForms(Pageable pageable) {
        Page<FormM> page = formMRepo.findByStatusClosed("C",pageable);
        List<FormMDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormMDTO> pageImpl = new PageImpl<FormMDTO>(dtOs, pageable, t);
        return pageImpl;
    }

    private Date getFormExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        SettingDTO setting = configurationService.getSettingByName("FORM_M_EXPIRY");

        if (setting != null && setting.isEnabled()) {

            int days = NumberUtils.toInt(setting.getValue());
            System.out.println("the number of days "+days);
            calendar.add(Calendar.DAY_OF_YEAR, days);
            return calendar.getTime();
        }
        return null;
    }

    @Override
    public String addAttachments(MultipartFile files[]) {
        StringJoiner sj = new StringJoiner(" , ");
        String uploadedFileName = "";

        for (MultipartFile file : files) {

            if (file.isEmpty()) {
                continue; //next pls
            }

            try {

                byte[] bytes = file.getBytes();
                CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                        .getPrincipal();
                User users = principal.getUser();
//                CorporateUser corporateUser=(CorporateUser) users;
//                String username=corporateUser.getUserName();

                if (users.getUserType().equals(UserType.CORPORATE)) {
                    CorporateUser corporateUser = (CorporateUser) users;
                    String username = corporateUser.getUserName();
                    logger.info("corporate user {} with username {}", corporateUser, username);
                    Path path = Paths.get(UPLOADED_FOLDER + username + file.getOriginalFilename());
                    logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());
                    Files.write(path, bytes);
                    sj.add(file.getOriginalFilename());
                    logger.info("file uploaded successfully to {} directory", path);

                } else if (users.getUserType().equals(UserType.BANK)) {
                    BankUser bankUser = (BankUser) users;
                    String username = bankUser.getUserName();
                    logger.info("corporate user {} with username {}", bankUser, username);
                    Path path = Paths.get(BANK_UPLOADED_FOLDER + username + file.getOriginalFilename());
                    logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());
                    Files.write(path, bytes);
                    sj.add(file.getOriginalFilename());
                    logger.info("file uploaded successfully to {} directory", path);

                }

              /*  // Create the file on server
                REAL_PATH=request.getSession().getServletContext().getRealPath("templates/corporate/upload");
                logger.info("Real Path {}",REAL_PATH);
                File dir = new File(REAL_PATH + File.separator );
                if (!dir.exists())
                    dir.mkdirs();
                File serverFile = new File(dir.getAbsolutePath()
                        + File.separator + file.getOriginalFilename());
                logger.info("DIR {}, SERVER FILE {} ",dir,serverFile);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();

                logger.info("Server File Location=" + serverFile.getAbsolutePath());

                //  String message = message + "You successfully uploaded file=" + file.getOriginalFilename();

*/
            } catch (IOException e) {
                e.printStackTrace();
                logger.warn("failed to upload file");
            }

        }

        uploadedFileName = sj.toString();
        logger.info("uploadFileName {}", uploadedFileName);
        if (StringUtils.isEmpty(uploadedFileName)) {
            logger.info("No file added, Please select a file to upload");

        } else {
            logger.info("You successfully uploaded {}", uploadedFileName);

        }
        return uploadedFileName;
    }


    @Override
    public FormMAmend convertFormMDTOToAmendEntity(FormMDTO formMDTO) {
        WorkFlow workFlow = workFlowRepo.findOne(formMDTO.getWorkFlowId());
        formMDTO.setWorkFlow(workFlow);
        FormMAmend formm = modelMapper.map(formMDTO, FormMAmend.class);
//        try {
//            if (lcdto.getExpiryDate() != null && !"".equals(lcdto.getExpiryDate())) {
//                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//                lc.setDateOfExpiry(df.parse(lcdto.getExpiryDate()));
//            }
//            if (lcdto.getInsuranceExpiryDate() != null && !"".equals(lcdto.getInsuranceExpiryDate())) {
//                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//                lc.setInsuranceDateOfExpiry(df.parse(lcdto.getInsuranceExpiryDate()));
//            }
//            if (lcdto.getDate() != null && !"".equals(lcdto.getDate())) {
//                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//                lc.setDateCreated(df.parse(lcdto.getDate()));
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        return formm;
    }

//    @Override
//    public boolean checkIfAmendable(Long id) {
//        FormM formM = formMRepo.findOne(id);
//        if (!"S".equals(formM.getStatus()) && !"P".equals(formM.getStatus())) {
//            List<FormMAmend> formMAmendList = formMAmendRepo.getByFormNumber(formM.getFormNumber());
//
//            for (FormMAmend formMAmend : formMAmendList) {
//                if ("S".equals(formMAmend.getStatus())) {
//                    return false;
//                }
//            }
//            return true;
//        }
//        return false;
//    }

//    @Override
//    public boolean canCancel(Long id) {
//        FormM formM = formMRepo.findOne(id);
//        if (!"S".equals(formM.getStatus()) && !"P".equals(formM.getStatus())) {
//
//            FormMCancellation formMCancel = formMCancelRepo.findByFormNumberAndCifid(formM.getFormNumber(), formM.getCifid());
//            if (formMCancel != null){
//                return false;
//            }else{
//                List<Paar> paars = paarRepo.findByFormM(formM);
//
//                for (Paar paar : paars) {
//                    if ("I".equals(paar.getStatus())) {
//                        return false;
//                    }
//                }
//            }
//            return true;
//        }
//        return false;
//    }

//    @Override
//    public Page<FormMDTO> getFormMAmendments(String formNumber, Pageable pageDetails) {
//        FormM formM = formMRepo.findByFormNumber(formNumber);
//        logger.info("formNumber is {}", formNumber);
//        Page<FormMAmend> page = formMAmendRepo.findByFormNumberAndCifid(formNumber, formM.getCifid(), pageDetails);
//        List<FormMDTO> dtOs = convertAmendEntitiesToFormMDTOs(page);
//        long t = page.getTotalElements();
//        Page<FormMDTO> pageImpl = new PageImpl<FormMDTO>(dtOs, pageDetails, t);
//        return pageImpl;
//    }

    @Override
    public List<FormMDTO> convertAmendEntitiesToFormMDTOs(Iterable<FormMAmend> formMAmends) {
        List<FormMDTO> formMDTOList = new ArrayList<>();
        for (FormMAmend formMAmend : formMAmends) {
            FormMDTO formMDTO = convertAmendEntityToFormMDTO(formMAmend);
            formMDTOList.add(formMDTO);
        }
        return formMDTOList;
    }

    @Override
    public FormMDTO convertAmendEntityToFormMDTO(FormMAmend formMAmend) {
        FormMDTO formMDTO = modelMapper.map(formMAmend, FormMDTO.class);
        if ("P".equals(formMAmend.getStatus())) {
            formMDTO.setStatusDesc("Saved");
        } else if ("S".equals(formMAmend.getStatus())) {
            formMDTO.setStatusDesc("Submitted");
        } else if ("A".equals(formMAmend.getStatus())) {
            formMDTO.setStatusDesc("Approved");
        } else if ("D".equals(formMAmend.getStatus())) {
            formMDTO.setStatusDesc("Declined");
        }

        return formMDTO;
    }


    private FormM addComments(FormMDTO formMDTO) throws CronJobException
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        logger.info("WORK_FLOW_ID {}", formMDTO.getWorkFlow().getId());
        WorkFlow workFlow= workFlowRepo.findOne(formMDTO.getWorkFlow().getId());
        formMDTO.setWorkFlow(workFlow);
        FormM singleFormM=  formMRepo.findOne(formMDTO.getId());

        formMDTO.setExpiry(singleFormM.getExpiry());
        FormM formM=modelMapper.map(formMDTO,FormM.class);
        Comments comments=new Comments();
        comments.setMadeOn(new Date());
        comments.setStatus(formMDTO.getSubmitAction());
        comments.setComment(formMDTO.getComments());
        comments.setUsername(doneBy.getUserName());
        List<Comments> oldComment=singleFormM.getComment();
        oldComment.add(comments);
        formM.setComment(oldComment);
        return formM;
    }

    @Override
    public BankCustomerDTO getBankCustomerByCustomerId(String customerId) throws CronJobException {
        BankCustomerDTO bankCustomerDTO = null;
        if (customerId != null){
            List<AccountDTO> customerAccounts = new ArrayList<>();
            List<String> accountNumbers = new ArrayList<>();

            if (retailUserRepo.existsByCustomerId(customerId)){
                System.out.println("RETAIL CUSTOMER");
                RetailUser retailUser = retailUserRepo.findFirstByCustomerId(customerId);
                System.out.println("RETAIL CUSTOMER");
                bankCustomerDTO=modelMapper.map(retailUser , BankCustomerDTO.class);
                System.out.println("RETAIL CUSTOMER");
                customerAccounts = accountService.getAccounts(customerId);
                for(AccountDTO accountDTO: customerAccounts){
                    accountNumbers.add(accountDTO.getAccountNumber());
                }
                bankCustomerDTO.setAccountNumbers(accountNumbers);
                bankCustomerDTO.setType("RETAIL");
                bankCustomerDTO.setLabel("Customer's name (lastname first)");
                bankCustomerDTO.setCustomerName(retailUser.getLastName() +" "+retailUser.getFirstName());
                bankCustomerDTO.setPhone(bankCustomerDTO.getPhoneNumber());
                System.out.println("RETAIL CUSTOMER");
            }else if(corporateRepo.existsByCustomerId(customerId)){
                System.out.println("CORPORATE CUSTOMER");
                Corporate corporate = corporateRepo.findFirstByCustomerId(customerId);
                System.out.println("CORPORATE CUSTOMER");
                bankCustomerDTO=modelMapper.map(corporate , BankCustomerDTO.class);
                System.out.println("CORPORATE CUSTOMER");
                customerAccounts = accountService.getAccounts(customerId);
                for(AccountDTO accountDTO: customerAccounts){
                    System.out.println("corporate customer account "+accountDTO.getAccountNumber());
                    accountNumbers.add(accountDTO.getAccountNumber());
                }
                bankCustomerDTO.setAccountNumbers(accountNumbers);
                bankCustomerDTO.setType("CORPORATE");
                bankCustomerDTO.setLabel("Corporate name");
                bankCustomerDTO.setCustomerName(corporate.getName());
                System.out.println("CORPORATE CUSTOMER");
            }else{
//                bankCustomerDTO.setType("NOT_FOUND");
                return null;
            }
        }
        return bankCustomerDTO;
    }


}



