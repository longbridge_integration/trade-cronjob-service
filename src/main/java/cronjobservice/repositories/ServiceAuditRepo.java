package cronjobservice.repositories;

import cronjobservice.models.RequestType;
import cronjobservice.models.ServiceAudit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by kwere on 01/03/2018.
 */
@Repository
@Transactional
public interface ServiceAuditRepo extends CommonRepo<ServiceAudit,Long>{

//    ServiceAudit findByTempNumber(String tempNumber);

    ServiceAudit findFirstByTempNumber(String tempNumber);
    ServiceAudit findFirstByTempNumberAndRequestType(String tempNumber,RequestType requestType);

    List<ServiceAudit> findByTempNumber(String tempNumber);

    List<ServiceAudit> findByTempNumberAndRequestType(String tempNumber, RequestType requestType);

    Page<ServiceAudit> findByResponseFlagIsTrue(Pageable pageable);

}
