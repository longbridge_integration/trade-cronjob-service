package cronjobservice.jobs.guaranteejobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

/**
 * Created by Longbridge on 9/12/2017.
 */
@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class InwardGuaranteeAddedOnFinJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextGuarantee = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextGuarantee.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.INWARDGUARANTEE_ADDED_ON_FINACLE.toString(),cronJobService.getCurrentExpression(JobCategory.INWARDGUARANTEE_ADDED_ON_FINACLE.toString()));

            cronJobService.getInwardGuaranteeAddedOnFinacle();
//            cronJobService.getInwardGuaranteeAddedOnFinacleByCifid();

            cronJobService.updateRunningJob(JobCategory.INWARDGUARANTEE_ADDED_ON_FINACLE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
