package cronjobservice.services;//package longbridge.services;


import cronjobservice.api.FormMDetails;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.FormM;
import cronjobservice.models.FormMAmend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by SYLVESTER on 9/7/2017.
 */
public interface FormMService {



    String updateFormM(FormMDTO formMDTO) throws CronJobException;

    FormMDTO editFormM(Long id) throws CronJobException;

    FormMDTO getInitiatorDetails() throws CronJobException;

    Page<FormMDTO> getAllFormMs(Pageable pageDetails);

    String addForm(FormMDTO formMDTO) throws CronJobException;

    Boolean addFormMFromFinacle(FormMDTO formMDTO) throws CronJobException;

    Boolean updateFormMAllocation(FormM tmsForm, FormM finForm) throws CronJobException;

//    String amendFormM(FormMDTO formMDTO) throws CronJobException;
//
//    String cancelForm(String formNumber) throws CronJobException;

    @Transactional
    Boolean updateFormMFromFinacle(FormMDetails formMDetails, FormM formM) throws CronJobException;

    @Transactional
    Boolean updateFormMAddedOnFinacle(FormMDTO formMDTO) throws CronJobException;

    FormMDTO getFormM(Long id) throws CronJobException;

    FormMDTO getFormMByFormNumber(String formNumber) throws CronJobException;



    Page<FormMDTO> getClosedFormMs(Pageable pageable);

//    Page<FormMDTO> getFormMsForBankUser(Pageable pageable) throws CronJobException;

    FormHolder createFormHolder(FormMDTO formMDTO, String name) throws CronJobException;

    Page<FormMDTO> getOpenFormMs(Pageable pageable);

//    Page<FormMDTO> getOpenFormMsForBankUser(Pageable pageable);

    Page<FormMDTO> getAwaitsFormMs(Pageable pageable);

    FormHolder executeAction(FormMDTO formMDTO, String name) throws CronJobException;
    Page<FormMDTO> getFormMs(Pageable pageDetails);

    Page<FormMDTO> getListExpiredFormM(Pageable pageDetails);

    Page<FormMDTO> getListPendingFormM(Pageable pageable);

    Page<FormMDTO> getBankFormMs(Pageable pageable);

    Page<FormMDTO> getBankOpenFormMs(Pageable pageable);

    List<FormItemsDTO> getFormItems(Long formId);

    List<FormMDTO> getCustomerClosedForms();

    List<FormMDTO> getCustomerClosedFormsByCif(String cifid);

    Page<FormMDTO> getCustomerAllClosedForms(Pageable pageable);

    String addAttachments(MultipartFile file1[]);

    List<CommentDTO> getComments(Long id) throws CronJobException;

    long getTotalExpiredFormM() throws CronJobException;

    long getTotalPendingFormM() throws CronJobException;

    long  getTotalPendingAndExpiredFormM(long pendingFormM, long ExpiredFormM) throws CronJobException;

    FormMAmend convertFormMDTOToAmendEntity(FormMDTO formMDTO);

    FormMDTO convertEntityTODTO(FormM formM);
    FormMDTO UpdateFormM(FormM formM);

//    boolean checkIfAmendable(Long id);
//
//    boolean canCancel(Long id);
//
//    Page<FormMDTO> getFormMAmendments(String formNumber, Pageable pageDetails);

    List<FormMDTO> convertAmendEntitiesToFormMDTOs(Iterable<FormMAmend> formMAmends);

    FormMDTO convertAmendEntityToFormMDTO(FormMAmend formMAmend);

    BankCustomerDTO getBankCustomerByCustomerId(String customerId) throws CronJobException;
}
