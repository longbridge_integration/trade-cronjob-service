package cronjobservice.repositories;

import cronjobservice.models.FormNxp;
import cronjobservice.models.ImportBill;
import cronjobservice.models.ImportLc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * Created by user on 10/22/2017.
 */
@Repository
public interface ImportBillRepo extends CommonRepo<ImportBill,Long> {




//    ImportBill findOne(Long id);
//    ImportBill findByTemporalBllNumb(String temporalBllNumb);
////    Page<ImportBill> findByExportLcId(Long corpId, Pageable pageDetail);
//    @Query("select  sum (s.billAmount)  from ImportBill s  where  s.cifid=:cifid and s.importLc=:importLc")
//    BigDecimal sumAmountInBill(@Param("cifid") String cifid, @Param("importLc") ImportLc importLc);
//
//    @Query("select s from ImportBill s where s.cifid=:cifid and s.status=:submitted")
//    Page<ImportBill> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);
//
//    @Query("select s from ImportBill s where s.cifid=:cifid")
//    Page<ImportBill> findByCifid(@Param("cifid") String cifid, Pageable pageable);
//
//
//    @Query("select s from ImportBill s where  (s.status=:saved or s.status=:decline) and s.importBillType='Collection'")
//    Page<ImportBill> findBankOpenImportBillForCollection(@Param("saved") String saved, @Param("decline") String decline, Pageable pageable);

}
