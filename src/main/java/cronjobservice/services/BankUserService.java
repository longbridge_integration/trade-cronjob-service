package cronjobservice.services;

import cronjobservice.dtos.BankUserDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.AdminUser;
import cronjobservice.models.BankUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * Created by chiomarose on 15/08/2017.
 */
public interface BankUserService {

     BankUserDTO getBankUser(Long id);

     boolean userExist(String userName);

     BankUser getUserByName(String userName);

     Iterable<BankUserDTO> getUsers();

    @PreAuthorize("hasAuthority('UNLOCK_BANK_USER')")
    String unlockUser(Long id) throws CronJobException;

    Page<BankUserDTO> getUsers(Pageable pageableDetails);

    Page<BankUserDTO> getUsersByBranchSortCode(String sortCode, Pageable pageDetails);

    Page<BankUserDTO> findUsers(BankUserDTO example, Pageable pageDetails);

    Page<BankUserDTO> findUsers(String pattern, Pageable pageDetails);

    String addBranchUsers(BankUserDTO user) throws CronJobException;

    String updateUser(BankUserDTO user) throws CronJobException;

    String changeActivationStatus(Long userId) throws CronJobException;

    void sendActivationCredentials(BankUser user, String password);

    String deleteUser(Long userId) throws CronJobException;

    String resetPassword(String userName) throws CronJobException;

    String resetPassword(Long id) throws CronJobException;


//    String changePassword(AdminUser user, ChangePassword changePassword) throws CronJobException;

















}
