package cronjobservice.jobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class BillsJob implements Job{
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextBills = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextBills.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
//            cronJobService.saveRunningJob("in-built",cronJobService.getCurrentExpression());
//            cronJobService.updateRunningJob();
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
