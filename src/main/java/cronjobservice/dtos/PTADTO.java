package cronjobservice.dtos;

import cronjobservice.models.Branch;
import cronjobservice.models.WorkFlow;

import java.math.BigDecimal;
import java.util.Date;

public class PTADTO {

    private Long id;

    private long workFlowId;
    private WorkFlow workFlow;
    private int version;

    private String submitAction;

    private String refNumber;
    private Branch docSubBranch;
    private String appName;
    private String appAddress;
    private String appPhone;

    private String appEmail;
    private String appCountry;
    private String typeOfBusiness;

    private String passportNo;
    private String bvn;
    private String passIssueDate;

    private BigDecimal forexInFigure; //Amount BigDecimal
    private String nairaInFigure;
    private String modeOfPayments;
    private boolean guidelineApproval;
    private boolean paymentApproval;

    private String travelDate;
    private String flightHour;
    private String currency;
    private String countryOfVisit;
    private String rate;
    private String town;
    private String state;
    private String counter;
    private Date counterDate;
    private String dateOfBirth;
    private String oneTimeTrip;
    private String returnTrip;
    private boolean immigrants;
    private boolean student;
    private String accountNumber;
    private String endDate;
    private String initiatedBy;
    private String userType;
    private String approvedBy;
    private String solId;
    private String RequestType;
    private String paymentInitiatedBy;
    private String tranId;
    private  String status;
    private String statusDesc;
    private String cifid;
    private Date createdOn;
    private String chargeFlag;
    private String chargePcnt;
    private String concessionFlag;
    private Double concessionPercentage;
    private String payableAcct;
    private String receivableAcct;
    private String transitAcct;
    private String eventId;
    private  String eventType;


    public String getPayableAcct() {
        return payableAcct;
    }

    public void setPayableAcct(String payableAcct) {
        this.payableAcct = payableAcct;
    }

    public String getReceivableAcct() {
        return receivableAcct;
    }

    public void setReceivableAcct(String receivableAcct) {
        this.receivableAcct = receivableAcct;
    }

    public String getTransitAcct() {
        return transitAcct;
    }

    public void setTransitAcct(String transitAcct) {
        this.transitAcct = transitAcct;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getConcessionFlag() {
        return concessionFlag;
    }

    public void setConcessionFlag(String concessionFlag) {
        this.concessionFlag = concessionFlag;
    }

    public Double getConcessionPercentage() {
        return concessionPercentage;
    }

    public void setConcessionPercentage(Double concessionPercentage) {
        this.concessionPercentage = concessionPercentage;
    }

    public String getChargePcnt() {
        return chargePcnt;
    }

    public void setChargePcnt(String chargePcnt) {
        this.chargePcnt = chargePcnt;
    }


    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public Branch getDocSubBranch() {
        return docSubBranch;
    }

    public void setDocSubBranch(Branch docSubBranch) {
        this.docSubBranch = docSubBranch;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppAddress() {
        return appAddress;
    }

    public void setAppAddress(String appAddress) {
        this.appAddress = appAddress;
    }

    public String getAppPhone() {
        return appPhone;
    }

    public void setAppPhone(String appPhone) {
        this.appPhone = appPhone;
    }

    public String getAppEmail() {
        return appEmail;
    }

    public void setAppEmail(String appEmail) {
        this.appEmail = appEmail;
    }

    public String getAppCountry() {
        return appCountry;
    }

    public void setAppCountry(String appCountry) {
        this.appCountry = appCountry;
    }

    public String getTypeOfBusiness() {
        return typeOfBusiness;
    }

    public void setTypeOfBusiness(String typeOfBusiness) {
        this.typeOfBusiness = typeOfBusiness;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public String getPassIssueDate() {
        return passIssueDate;
    }

    public void setPassIssueDate(String passIssueDate) {
        this.passIssueDate = passIssueDate;
    }

    public BigDecimal getForexInFigure() {
        return forexInFigure;
    }

    public void setForexInFigure(BigDecimal forexInFigure) {
        this.forexInFigure = forexInFigure;
    }

    public String getNairaInFigure() {
        return nairaInFigure;
    }

    public void setNairaInFigure(String nairaInFigure) {
        this.nairaInFigure = nairaInFigure;
    }

    public String getModeOfPayments() {
        return modeOfPayments;
    }

    public void setModeOfPayments(String modeOfPayments) {
        this.modeOfPayments = modeOfPayments;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public boolean isPaymentApproval() {
        return paymentApproval;
    }

    public void setPaymentApproval(boolean paymentApproval) {
        this.paymentApproval = paymentApproval;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public String getFlightHour() {
        return flightHour;
    }

    public void setFlightHour(String flightHour) {
        this.flightHour = flightHour;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountryOfVisit() {
        return countryOfVisit;
    }

    public void setCountryOfVisit(String countryOfVisit) {
        this.countryOfVisit = countryOfVisit;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public Date getCounterDate() {
        return counterDate;
    }

    public void setCounterDate(Date counterDate) {
        this.counterDate = counterDate;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getOneTimeTrip() {
        return oneTimeTrip;
    }

    public void setOneTimeTrip(String oneTimeTrip) {
        this.oneTimeTrip = oneTimeTrip;
    }

    public String getReturnTrip() {
        return returnTrip;
    }

    public void setReturnTrip(String returnTrip) {
        this.returnTrip = returnTrip;
    }

    public boolean isImmigrants() {
        return immigrants;
    }

    public void setImmigrants(boolean immigrants) {
        this.immigrants = immigrants;
    }

    public boolean isStudent() {
        return student;
    }

    public void setStudent(boolean student) {
        this.student = student;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getSolId() {
        return solId;
    }

    public void setSolId(String solId) {
        this.solId = solId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getSubmitAction() {
        return submitAction;
    }

    public void setSubmitAction(String submitAction) {
        this.submitAction = submitAction;
    }

    public long getWorkFlowId() {
        return workFlowId;
    }

    public void setWorkFlowId(long workFlowId) {
        this.workFlowId = workFlowId;
    }

    public WorkFlow getWorkFlow() {
        return workFlow;
    }

    public void setWorkFlow(WorkFlow workFlow) {
        this.workFlow = workFlow;
    }

    public String getRequestType() {
        return RequestType;
    }

    public void setRequestType(String requestType) {
        RequestType = requestType;
    }

    public String getPaymentInitiatedBy() {
        return paymentInitiatedBy;
    }

    public void setPaymentInitiatedBy(String paymentInitiatedBy) {
        this.paymentInitiatedBy = paymentInitiatedBy;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getChargeFlag() {
        return chargeFlag;
    }

    public void setChargeFlag(String chargeFlag) {
        this.chargeFlag = chargeFlag;
    }
}
