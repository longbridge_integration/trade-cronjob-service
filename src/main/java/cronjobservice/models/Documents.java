package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;

@Entity
@Where(clause ="del_Flag='N'" )
@Audited(withModifiedFlag=true)
public class Documents extends AbstractEntity{

}
