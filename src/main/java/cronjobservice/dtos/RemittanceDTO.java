package cronjobservice.dtos;

import cronjobservice.models.WorkFlow;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by chiomarose on 05/10/2017.
 */
public class RemittanceDTO {

    private Long id;
    private int version;
    private long workFlowId;

    private String submitAction;
    private String customerName;
    private String corporateName;
    private String firstName;
    private String lastName;
    private String address;
    private String town;
    private String state;
    private String country;
    private String phone;
    private String email;
    private String postCode;
    private String route;
    private String airLine;
    private String airTicketNo;
    private String registration;

    private String beneficiaryName;
    private String beneficiaryAccount;
    private String beneficiaryAddress;
    private String beneficiaryTown;
    private String beneficiaryState;
    private String beneficiaryPhone;
    private String beneficiaryCountry;
    private String beneficiaryPostCode;
    private String beneficiaryRegistration;
    private String beneBankName;
    private String beneBankAddr;
    private String beneBankSwiftCode;
    private String beneBankCtry;
    private String beneBankCity;
    private String beneBankState;
    private String beneBankPoCode;
    private String beneBankIdent;

    private String interBankName;
    private String interBankAddr;
    private String interBankSwiftCode;
    private String interBankCtry;
    private String interBankCity;
    private String interBankState;
    private String interBankPoCode;
    private String interBankIdent;

    private String corrBankCode;
    private String corrBranchId;
    private String corrBankName;
    private String corrBankAddr;
    private String corrBankSwiftCode;
    private String corrBankCtry;
    private String corrBankState;
    private String corrBankCity;
    private String corrBankPoCode;
    private String corrBankIdent;
    private String corrNostroAcct;

    private String chargeAccount;
    private String realizationAcct;
    private String operativeAccount;//bank
    private String operativeAccountCurrency;

    private String formNumber;
    private String tempFormNumber;
    private BigDecimal offShoreCharges;
    private String offShoreChargeDoneBy;
    private String paymentMode;
    private String currencyCode;
    private String paymentCode;
    private String modeOfIdentification;
    private String identityNo;
    private String status;
    private String statusDesc;
    private String userType;

    private boolean guidelineApproval;
    private String initiatedBy;
    private WorkFlow workFlow;
    private String senderToReceiverInfo;
    private String senderToReceiverInfos;
    private String narrative;
    private String remittanceInfo;
    private String remittanceInfos;
    private String description;
    private String utilization;
    private String formaAmount;


    private String allocatedAmount;
    private String amountInWords;
    private String transferAmount;
    private String transactionDebitAmt;
    private String amountForDisplay;

    private String comments;
    private Date expiry;
    private String expiryDate;
    private Date dateCreated;
    private Date createdOn;
    private String attachments;
    private String concessionFlag;
    private Double concessionPercentage;

    private String transactionId;
    private String remitanceId;
    private Date remittedDate;
    private String remittanceType;//bank - Codes
    private String messageType;//bank - Codes
    private String operationCode;//bank - Codes
    private String remittedCurrency;//fetch currency but bank editable- third world
    private BigDecimal remittedAmount;
    private String bidRateCode;
    private String remittedExRate;
    private String cifid;
    private List<RemittanceDocumentDTO> documents;
    private String branch;
    private String submitFlag;
    private String brDesc;
    private String formaNumber;
    private String allocationDealId;
    private String exchangeRate;
    private String tranId;

    private String allocationCurrencyCode;
    private String file;
    private boolean saveBeneficiary;
    private Long beneBankId;
    private Long beneficiaryId;

    private String isThirdWorldTransaction;
    private BigDecimal tranAmtInAllocationCurr;
    private BigDecimal formaUtilizedAmt;

    public BigDecimal getFormaUtilizedAmt() {
        return formaUtilizedAmt;
    }

    public void setFormaUtilizedAmt(BigDecimal formaUtilizedAmt) {
        this.formaUtilizedAmt = formaUtilizedAmt;
    }

    public String getIsThirdWorldTransaction() {
        return isThirdWorldTransaction;
    }

    public void setIsThirdWorldTransaction(String isThirdWorldTransaction) {
        this.isThirdWorldTransaction = isThirdWorldTransaction;
    }

    public BigDecimal getTranAmtInAllocationCurr() {
        return tranAmtInAllocationCurr;
    }

    public void setTranAmtInAllocationCurr(BigDecimal tranAmtInAllocationCurr) {
        this.tranAmtInAllocationCurr = tranAmtInAllocationCurr;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getCorrBankCode() {
        return corrBankCode;
    }

    public void setCorrBankCode(String corrBankCode) {
        this.corrBankCode = corrBankCode;
    }

    public String getCorrBranchId() {
        return corrBranchId;
    }

    public void setCorrBranchId(String corrBranchId) {
        this.corrBranchId = corrBranchId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getBeneBankId() {
        return beneBankId;
    }

    public void setBeneBankId(Long beneBankId) {
        this.beneBankId = beneBankId;
    }

    public Long getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(Long beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public boolean isSaveBeneficiary() {
        return saveBeneficiary;
    }

    public void setSaveBeneficiary(boolean saveBeneficiary) {
        this.saveBeneficiary = saveBeneficiary;
    }

    public String getFormaAmount() {
        return formaAmount;
    }

    public void setFormaAmount(String formaAmount) {
        this.formaAmount = formaAmount;
    }

    public String getAllocationDealId() {
        return allocationDealId;
    }

    public void setAllocationDealId(String allocationDealId) {
        this.allocationDealId = allocationDealId;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
    private String bankCode;

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getFormaNumber() {
        return formaNumber;
    }

    public void setFormaNumber(String formaNumber) {
        this.formaNumber = formaNumber;
    }

    public String getBrDesc() {
        return brDesc;
    }

    public void setBrDesc(String brDesc) {
        this.brDesc = brDesc;
    }

    public List<RemittanceDocumentDTO> getDocuments() {
        return documents;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public void setDocuments(List<RemittanceDocumentDTO> documents) {
        this.documents = documents;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getOffShoreChargeDoneBy() {
        return offShoreChargeDoneBy;
    }

    public void setOffShoreChargeDoneBy(String offShoreChargeDoneBy) {
        this.offShoreChargeDoneBy = offShoreChargeDoneBy;
    }

    public Long getId() {
        return id;
    }

    public WorkFlow getWorkFlow() {
        return workFlow;
    }

    public String getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(String transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public void setWorkFlow(WorkFlow workFlow) {
        this.workFlow = workFlow;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubmitAction() {
        return submitAction;
    }

    public void setSubmitAction(String submitAction) {
        this.submitAction = submitAction;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getAirLine() {
        return airLine;
    }

    public void setAirLine(String airLine) {
        this.airLine = airLine;
    }

    public String getAirTicketNo() {
        return airTicketNo;
    }

    public void setAirTicketNo(String airTicketNo) {
        this.airTicketNo = airTicketNo;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryAddress() {
        return beneficiaryAddress;
    }

    public void setBeneficiaryAddress(String beneficiaryAddress) {
        this.beneficiaryAddress = beneficiaryAddress;
    }

    public String getBeneficiaryTown() {
        return beneficiaryTown;
    }

    public void setBeneficiaryTown(String beneficiaryTown) {
        this.beneficiaryTown = beneficiaryTown;
    }

    public String getBeneficiaryState() {
        return beneficiaryState;
    }

    public void setBeneficiaryState(String beneficiaryState) {
        this.beneficiaryState = beneficiaryState;
    }

    public String getBeneficiaryPhone() {
        return beneficiaryPhone;
    }

    public void setBeneficiaryPhone(String beneficiaryPhone) {
        this.beneficiaryPhone = beneficiaryPhone;
    }

    public String getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    public void setBeneficiaryCountry(String beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
    }

    public String getBeneficiaryRegistration() {
        return beneficiaryRegistration;
    }

    public void setBeneficiaryRegistration(String beneficiaryRegistration) {
        this.beneficiaryRegistration = beneficiaryRegistration;
    }

    public String getAmountInWords() {
        return amountInWords;
    }

    public void setAmountInWords(String amountInWords) {
        this.amountInWords = amountInWords;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getModeOfIdentification() {
        return modeOfIdentification;
    }

    public void setModeOfIdentification(String modeOfIdentification) {
        this.modeOfIdentification = modeOfIdentification;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public long getWorkFlowId() {
        return workFlowId;
    }

    public void setWorkFlowId(long workFlowId) {
        this.workFlowId = workFlowId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUtilization() {
        return utilization;
    }

    public void setUtilization(String utilization) {
        this.utilization = utilization;
    }

    public String getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(String allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public String getInterBankName() {
        return interBankName;
    }

    public void setInterBankName(String interBankName) {
        this.interBankName = interBankName;
    }

    public String getInterBankAddr() {
        return interBankAddr;
    }

    public void setInterBankAddr(String interBankAddr) {
        this.interBankAddr = interBankAddr;
    }

    public String getInterBankSwiftCode() {
        return interBankSwiftCode;
    }

    public void setInterBankSwiftCode(String interBankSwiftCode) {
        this.interBankSwiftCode = interBankSwiftCode;
    }

    public String getInterBankCtry() {
        return interBankCtry;
    }

    public void setInterBankCtry(String interBankCtry) {
        this.interBankCtry = interBankCtry;
    }

    public String getInterBankCity() {
        return interBankCity;
    }

    public void setInterBankCity(String interBankCity) {
        this.interBankCity = interBankCity;
    }

    public String getInterBankState() {
        return interBankState;
    }

    public void setInterBankState(String interBankState) {
        this.interBankState = interBankState;
    }

    public String getInterBankPoCode() {
        return interBankPoCode;
    }

    public void setInterBankPoCode(String interBankPoCode) {
        this.interBankPoCode = interBankPoCode;
    }

    public String getBeneBankName() {
        return beneBankName;
    }

    public void setBeneBankName(String beneBankName) {
        this.beneBankName = beneBankName;
    }

    public String getBeneBankAddr() {
        return beneBankAddr;
    }

    public void setBeneBankAddr(String beneBankAddr) {
        this.beneBankAddr = beneBankAddr;
    }

    public String getBeneBankSwiftCode() {
        return beneBankSwiftCode;
    }

    public void setBeneBankSwiftCode(String beneBankSwiftCode) {
        this.beneBankSwiftCode = beneBankSwiftCode;
    }

    public String getBeneBankCtry() {
        return beneBankCtry;
    }

    public void setBeneBankCtry(String beneBankCtry) {
        this.beneBankCtry = beneBankCtry;
    }

    public String getBeneBankCity() {
        return beneBankCity;
    }

    public void setBeneBankCity(String beneBankCity) {
        this.beneBankCity = beneBankCity;
    }

    public String getBeneBankState() {
        return beneBankState;
    }

    public void setBeneBankState(String beneBankState) {
        this.beneBankState = beneBankState;
    }

    public String getBeneBankPoCode() {
        return beneBankPoCode;
    }

    public void setBeneBankPoCode(String beneBankPoCode) {
        this.beneBankPoCode = beneBankPoCode;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getBeneficiaryAccount() {
        return beneficiaryAccount;
    }

    public void setBeneficiaryAccount(String beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
    }

    public String getBeneficiaryPostCode() {
        return beneficiaryPostCode;
    }

    public void setBeneficiaryPostCode(String beneficiaryPostCode) {
        this.beneficiaryPostCode = beneficiaryPostCode;
    }

    public String getBeneBankIdent() {
        return beneBankIdent;
    }

    public void setBeneBankIdent(String beneBankIdent) {
        this.beneBankIdent = beneBankIdent;
    }

    public String getInterBankIdent() {
        return interBankIdent;
    }

    public void setInterBankIdent(String interBankIdent) {
        this.interBankIdent = interBankIdent;
    }

    public String getCorrBankName() {
        return corrBankName;
    }

    public void setCorrBankName(String corrBankName) {
        this.corrBankName = corrBankName;
    }

    public String getCorrBankAddr() {
        return corrBankAddr;
    }

    public void setCorrBankAddr(String corrBankAddr) {
        this.corrBankAddr = corrBankAddr;
    }

    public String getCorrBankSwiftCode() {
        return corrBankSwiftCode;
    }

    public void setCorrBankSwiftCode(String corrBankSwiftCode) {
        this.corrBankSwiftCode = corrBankSwiftCode;
    }

    public String getCorrBankCtry() {
        return corrBankCtry;
    }

    public void setCorrBankCtry(String corrBankCtry) {
        this.corrBankCtry = corrBankCtry;
    }

    public String getCorrBankState() {
        return corrBankState;
    }

    public void setCorrBankState(String corrBankState) {
        this.corrBankState = corrBankState;
    }

    public String getCorrBankCity() {
        return corrBankCity;
    }

    public void setCorrBankCity(String corrBankCity) {
        this.corrBankCity = corrBankCity;
    }

    public String getCorrBankPoCode() {
        return corrBankPoCode;
    }

    public void setCorrBankPoCode(String corrBankPoCode) {
        this.corrBankPoCode = corrBankPoCode;
    }

    public String getCorrBankIdent() {
        return corrBankIdent;
    }

    public void setCorrBankIdent(String corrBankIdent) {
        this.corrBankIdent = corrBankIdent;
    }

    public String getCorrNostroAcct() {
        return corrNostroAcct;
    }

    public void setCorrNostroAcct(String corrNostroAcct) {
        this.corrNostroAcct = corrNostroAcct;
    }

    public BigDecimal getOffShoreCharges() {
        return offShoreCharges;
    }

    public void setOffShoreCharges(BigDecimal offShoreCharges) {
        this.offShoreCharges = offShoreCharges;
    }

    public String getOperativeAccount() {
        return operativeAccount;
    }

    public void setOperativeAccount(String operativeAccount) {
        this.operativeAccount = operativeAccount;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getTempFormNumber() {
        return tempFormNumber;
    }

    public void setTempFormNumber(String tempFormNumber) {
        this.tempFormNumber = tempFormNumber;
    }

    public String getTransactionDebitAmt() {
        return transactionDebitAmt;
    }

    public void setTransactionDebitAmt(String transactionDebitAmt) {
        this.transactionDebitAmt = transactionDebitAmt;
    }

    public String getBidRateCode() {
        return bidRateCode;
    }

    public void setBidRateCode(String bidRateCode) {
        this.bidRateCode = bidRateCode;
    }

    public String getAllocationCurrencyCode() {
        return allocationCurrencyCode;
    }

    public void setAllocationCurrencyCode(String allocationCurrencyCode) {
        this.allocationCurrencyCode = allocationCurrencyCode;
    }

    public String getConcessionFlag() {
        return concessionFlag;
    }

    public void setConcessionFlag(String concessionFlag) {
        this.concessionFlag = concessionFlag;
    }

    public Double getConcessionPercentage() {
        return concessionPercentage;
    }

    public void setConcessionPercentage(Double concessionPercentage) {
        this.concessionPercentage = concessionPercentage;
    }

    public String getRemittanceInfo() {
        return remittanceInfo;
    }

    public void setRemittanceInfo(String remittanceInfo) {
        this.remittanceInfo = remittanceInfo;
    }

    public String getRemitanceId() {
        return remitanceId;
    }

    public void setRemitanceId(String remitanceId) {
        this.remitanceId = remitanceId;
    }

    public String getRemittanceType() {
        return remittanceType;
    }

    public void setRemittanceType(String remittanceType) {
        this.remittanceType = remittanceType;
    }

    public String getRemittedCurrency() {
        return remittedCurrency;
    }

    public void setRemittedCurrency(String remittedCurrency) {
        this.remittedCurrency = remittedCurrency;
    }

    public String getRemittedExRate() {
        return remittedExRate;
    }

    public void setRemittedExRate(String remittedExRate) {
        this.remittedExRate = remittedExRate;
    }

    public Date getRemittedDate() {
        return remittedDate;
    }

    public void setRemittedDate(Date remittedDate) {
        this.remittedDate = remittedDate;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getRealizationAcct() {
        return realizationAcct;
    }

    public void setRealizationAcct(String realizationAcct) {
        this.realizationAcct = realizationAcct;
    }

    public String getOperativeAccountCurrency() {
        return operativeAccountCurrency;
    }

    public void setOperativeAccountCurrency(String operativeAccountCurrency) {
        this.operativeAccountCurrency = operativeAccountCurrency;
    }

    public String getSenderToReceiverInfo() {
        return senderToReceiverInfo;
    }

    public void setSenderToReceiverInfo(String senderToReceiverInfo) {
        this.senderToReceiverInfo = senderToReceiverInfo;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setRemittedAmount(BigDecimal remittedAmount) {
        this.remittedAmount = remittedAmount;
    }

    public Date getExpiry() {
        return expiry;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public BigDecimal getRemittedAmount() {
        return remittedAmount;
    }

    public String getAmountForDisplay() {
        return amountForDisplay;
    }

    public void setAmountForDisplay(String amountForDisplay) {
        this.amountForDisplay = amountForDisplay;
    }

    public String getSubmitFlag() {
        return submitFlag;
    }

    public void setSubmitFlag(String submitFlag) {
        this.submitFlag = submitFlag;
    }

    public String getSenderToReceiverInfos() {
        return senderToReceiverInfos;
    }

    public void setSenderToReceiverInfos(String senderToReceiverInfos) {
        this.senderToReceiverInfos = senderToReceiverInfos;
    }

    public String getRemittanceInfos() {
        return remittanceInfos;
    }

    public void setRemittanceInfos(String remittanceInfos) {
        this.remittanceInfos = remittanceInfos;
    }
}
