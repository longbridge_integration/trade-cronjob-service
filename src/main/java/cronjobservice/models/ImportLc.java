package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by user on 10/7/2017.
 */
@Entity
@DiscriminatorValue("Import")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class ImportLc extends LC{

    //pass the form M number to Finacle as formMnumber
    @ManyToOne
    private FormM formM;

    @OneToMany
    private List<ImportBill> importBillList;

    public List<ImportBill> getImportBillList() {
        return importBillList;
    }

    public void setImportBillList(List<ImportBill> importBillList) {
        this.importBillList = importBillList;
    }

    public FormM getFormM() {
        return formM;
    }

    public void setFormM(FormM formM) {
        this.formM = formM;
    }

}
