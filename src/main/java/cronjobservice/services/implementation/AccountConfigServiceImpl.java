package cronjobservice.services.implementation;

import cronjobservice.dtos.AccountClassRestrictionDTO;
import cronjobservice.dtos.AccountRestrictionDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.exception.DuplicateObjectException;
import cronjobservice.exception.VerificationInterruptedException;
import cronjobservice.models.Account;
import cronjobservice.models.AccountClassRestriction;
import cronjobservice.models.AccountRestriction;
import cronjobservice.repositories.AccountClassRestrictionRepo;
import cronjobservice.repositories.AccountRepo;
import cronjobservice.repositories.AccountRestrictionRepo;
import cronjobservice.services.AccountConfigService;
import cronjobservice.services.CodeService;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.Verifiable;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Fortune on 5/1/2017.
 */


@Service
public class AccountConfigServiceImpl implements AccountConfigService {


    private AccountRestrictionRepo accountRestrictionRepo;
    private AccountClassRestrictionRepo accountClassRestrictionRepo;
    private CodeService codeService;
    private ModelMapper modelMapper;

    @Autowired
    private AccountRepo accountRepo;
    @Autowired
    private MessageSource messageSource;

    private Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    public AccountConfigServiceImpl(AccountRestrictionRepo accountRestrictionRepo, AccountClassRestrictionRepo accountClassRestrictionRepo, CodeService codeService, ModelMapper modelMapper) {
        this.accountRestrictionRepo = accountRestrictionRepo;
        this.accountClassRestrictionRepo = accountClassRestrictionRepo;
        this.codeService = codeService;
        this.modelMapper = modelMapper;
    }


    @Override
    public boolean isAccountHidden(String accountNumber) {

        Account account = accountRepo.findFirstByAccountNumber(accountNumber);
        if(account!=null) {
            if ("Y".equals(account.getHiddenFlag())) {
                return true;
            }
        }
        return false;
    }

    @Override
    @Verifiable(operation="ADD_ACCT_RESTRICT",description="Adding Account Restriction")
    public String addAccountRestriction(AccountRestrictionDTO accountRestrictionDTO) throws CronJobException {

        validateNoAccountDuplication(accountRestrictionDTO);
        try {
            AccountRestriction accountRestriction = convertAccountRestrictionDTOToEntity(accountRestrictionDTO);
            accountRestriction.setDateCreated(new Date());
            accountRestrictionRepo.save(accountRestriction);
            return messageSource.getMessage("account.restrict.add.success", null, locale);
        }  catch (VerificationInterruptedException e) {
            return e.getMessage();
        }

        catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("account.restrict.add.failure", null, locale), e);
        }
    }


    @Override
    @Verifiable(operation="UPDATE_ACCT_RESTRICT",description="Updating Account Restriction")
    public String updateAccountRestriction(AccountRestrictionDTO accountRestrictionDTO) throws CronJobException {

        validateNoAccountDuplication(accountRestrictionDTO);
        try {
            AccountRestriction accountRestriction = accountRestrictionRepo.findOne(accountRestrictionDTO.getId());
            accountRestriction.setVersion(accountRestrictionDTO.getVersion());
            accountRestriction.setRestrictionValue(accountRestrictionDTO.getRestrictionValue());
            accountRestriction.setRestrictionType(accountRestrictionDTO.getRestrictionType());
            accountRestrictionRepo.save(accountRestriction);
            return messageSource.getMessage("account.restrict.update.success", null, locale);
        }
        catch (VerificationInterruptedException e) {
            return e.getMessage();
        }
        catch (CronJobException e){
            throw e;
        }
        catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("account.restrict.update.failure", null, locale), e);
        }
    }

    @Override
    public AccountRestrictionDTO getAccountRestriction(Long id) {
        AccountRestriction accountRestriction = accountRestrictionRepo.findOne(id);
        return convertAccountRestrictionEntityToDTO(accountRestriction);
    }

    @Override
    public AccountClassRestrictionDTO getAccountClassRestriction(Long id) {
        AccountClassRestriction accountClassRestriction = accountClassRestrictionRepo.findOne(id);
        return convertAccountClassRestrictionEntityToDTO(accountClassRestriction);
    }

    @Override
    @Transactional
    @Verifiable(operation="DELETE_ACCT_RESTRICT",description="Deleting Account Restriction")
    public String deleteAccountRestriction(Long id) throws CronJobException {
        try {
            AccountRestriction accountRestriction = accountRestrictionRepo.findOne(id);
            accountRestrictionRepo.delete(accountRestriction);
            return messageSource.getMessage("account.restrict.delete.success", null, LocaleContextHolder.getLocale());

        }
        catch (VerificationInterruptedException e) {
            return e.getMessage();
        }
        catch (CronJobException e){
            throw e;
        }
        catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("account.restrict.delete.failure", null, locale), e);

        }
    }

    @Override
    public String addAccountClassRestriction(AccountClassRestrictionDTO accountClassRestrictionDTO) throws CronJobException {

        validateNoAccountClassDuplication(accountClassRestrictionDTO);
        try {
            AccountClassRestriction accountClassRestriction = convertAccountClassRestrictionDTOToEntity(accountClassRestrictionDTO);
            accountClassRestriction.setDateCreated(new Date());
            accountClassRestrictionRepo.save(accountClassRestriction);
            return messageSource.getMessage("class.restrict.add.success", null, locale);
        }
        catch (VerificationInterruptedException e) {
            return e.getMessage();
        }

        catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("class.restrict.add.failure", null, locale), e);
        }
    }

    @Override
    public String updateAccountClassRestriction(AccountClassRestrictionDTO accountClassRestrictionDTO) throws CronJobException {

        validateNoAccountClassDuplication(accountClassRestrictionDTO);
        try {
            AccountClassRestriction accountClassRestriction = accountClassRestrictionRepo.findOne(accountClassRestrictionDTO.getId());
            accountClassRestriction.setVersion(accountClassRestrictionDTO.getVersion());
            accountClassRestriction.setAccountClass(accountClassRestrictionDTO.getAccountClass());
            accountClassRestriction.setRestrictionType(accountClassRestrictionDTO.getRestrictionType());
            accountClassRestrictionRepo.save(accountClassRestriction);
            return messageSource.getMessage("class.restrict.update.success", null, locale);

        }
        catch (VerificationInterruptedException e) {
            return e.getMessage();
        }
        catch (CronJobException e){
            throw e;
        }
        catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("class.restrict.update.failure", null, locale), e);

        }
    }

    @Override
    public String deleteAccountClassRestriction(Long id) throws CronJobException {
        try {
            AccountClassRestriction accountClassRestriction = accountClassRestrictionRepo.findOne(id);
            accountClassRestriction.setDeletedOn(new Date());
            accountClassRestrictionRepo.save(accountClassRestriction);
            accountClassRestrictionRepo.delete(id);
            return messageSource.getMessage("class.restrict.delete.success", null, locale);

        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("class.restrict.update.failure", null, locale), e);

        }

    }

    @Override
    public boolean isAccountRestrictedForDebit(String accountNumber) {
        boolean isRestricted = false;
        AccountRestriction accountRestriction = accountRestrictionRepo.findByRestrictionValue(accountNumber);
        if (accountRestriction != null) {
            if (accountRestriction.getRestrictionType().equals("D")) {
                isRestricted = true;
            }
        }
        return isRestricted;
    }

    @Override
    public boolean isAccountRestrictedForCredit(String accountNumber) {
        boolean isRestricted = false;
        AccountRestriction accountRestriction = accountRestrictionRepo.findByRestrictionValue(accountNumber);
        if (accountRestriction != null) {
            if (accountRestriction.getRestrictionType().equals("C")) {
                isRestricted = true;
            }
        }
        return isRestricted;
    }


    @Override
    public boolean isAccountRestrictedForView(String accountNumber) {
        boolean isRestricted = false;
        AccountRestriction accountRestriction = accountRestrictionRepo.findFirstByRestrictionValueAndRestrictedForIgnoreCase(accountNumber,"V");
        if (accountRestriction != null) {

                isRestricted = true;

        }
        return isRestricted;
    }

    @Override
    public boolean isAccountSchemeTypeRestrictedForDebit(String schemeType) {
        boolean isRestricted = false;
        AccountRestriction accountRestriction = accountRestrictionRepo.findFirstByRestrictionValueAndRestrictedForIgnoreCase(schemeType,"D");
        if (accountRestriction != null) {

                isRestricted = true;

        }
        return isRestricted;
    }

    @Override
    public boolean isAccountSchemeTypeRestrictedForCredit(String schemeType) {
        boolean isRestricted = false;
        AccountRestriction accountRestriction = accountRestrictionRepo.findFirstByRestrictionValueAndRestrictedForIgnoreCase(schemeType,"D");
        if (accountRestriction != null) {

                isRestricted = true;

        }
        return isRestricted;
    }



    @Override
    public boolean isAccountSchemeTypeRestrictedForView(String schemeType) {
        boolean isRestricted = false;
        AccountRestriction accountRestriction = accountRestrictionRepo.findFirstByRestrictionValueAndRestrictedForIgnoreCase(schemeType,"V");
        if (accountRestriction != null) {
                isRestricted = true;
        }
        return isRestricted;
    }


    @Override
    public boolean isAccountSchemeCodeRestrictedForDebit(String schemeCode) {
        boolean isRestricted = false;
        AccountRestriction accountRestriction = accountRestrictionRepo.findFirstByRestrictionValueAndRestrictedForIgnoreCase(schemeCode,"D");
        if (accountRestriction != null) {

            isRestricted = true;

        }
        return isRestricted;
    }


    @Override
    public boolean isAccountSchemeCodeRestrictedForCredit(String schemeCode) {
        boolean isRestricted = false;
        AccountRestriction accountRestriction = accountRestrictionRepo.findFirstByRestrictionValueAndRestrictedForIgnoreCase(schemeCode,"D");
        if (accountRestriction != null) {

            isRestricted = true;

        }
        return isRestricted;
    }



    @Override
    public boolean isAccountSchemeCodeRestrictedForView(String schemeCode) {
        boolean isRestricted = false;
        AccountRestriction accountRestriction = accountRestrictionRepo.findFirstByRestrictionValueAndRestrictedForIgnoreCase(schemeCode,"V");
        if (accountRestriction != null) {
            isRestricted = true;
        }
        return isRestricted;
    }

    @Override
    public Iterable<AccountRestrictionDTO> getAccountRestrictions() {
        Iterable<AccountRestriction> accountRestrictions = accountRestrictionRepo.findAll();
        return convertAccountRestrictionEntitiesToDTOs(accountRestrictions);
    }

    @Override
    public Iterable<AccountClassRestrictionDTO> getAccountClassRestrictions() {
        Iterable<AccountClassRestriction> accountClassRestrictions = accountClassRestrictionRepo.findAll();
        return convertAccountClassRestrictionEntitiesToDTOs(accountClassRestrictions);
    }

    @Override
    public Page<AccountRestrictionDTO> getAccountRestrictions(Pageable pageable) {
        Page<AccountRestriction> accountRestrictionPageable = accountRestrictionRepo.findAll(pageable);
        List<AccountRestrictionDTO> dtos = convertAccountRestrictionEntitiesToDTOs(accountRestrictionPageable.getContent());
        long t = accountRestrictionPageable.getTotalElements();
        return new PageImpl<>(dtos, pageable, t);


    }

    @Override
    public Page<AccountClassRestrictionDTO> getAccountClassRestrictions(Pageable pageable) {
        Page<AccountClassRestriction> accountClassRestrictionPageable = accountClassRestrictionRepo.findAll(pageable);
        List<AccountClassRestrictionDTO> dtos = convertAccountClassRestrictionEntitiesToDTOs(accountClassRestrictionPageable.getContent());
        long t = accountClassRestrictionPageable.getTotalElements();
        return new PageImpl<AccountClassRestrictionDTO>(dtos, pageable, t);
    }

    private void validateNoAccountDuplication(AccountRestrictionDTO accountRestrictionDTO) throws DuplicateObjectException {
        AccountRestriction accountRestriction = accountRestrictionRepo.findByRestrictionTypeAndRestrictionValue(accountRestrictionDTO.getRestrictionType(),accountRestrictionDTO.getRestrictionValue());
        if (accountRestrictionDTO.getId() == null && accountRestriction != null) {
            throw new DuplicateObjectException(String.format(messageSource.getMessage("account.restrict.exists", null, locale), accountRestrictionDTO.getRestrictionValue())); //Duplication on creation
        }
        if (accountRestriction != null && (!accountRestriction.getId().equals(accountRestrictionDTO.getId()))) {
            throw new DuplicateObjectException(String.format(messageSource.getMessage("account.restrict.exists", null, locale), accountRestrictionDTO.getRestrictionValue())); //Duplication on update

        }
    }

    private void validateNoAccountClassDuplication(AccountClassRestrictionDTO accountClassRestrictionDTO) throws DuplicateObjectException {
        AccountClassRestriction accountClassRestriction = accountClassRestrictionRepo.findByAccountClass(accountClassRestrictionDTO.getAccountClass());
        if (accountClassRestrictionDTO.getId() == null && accountClassRestriction != null) {
            throw new DuplicateObjectException(String.format(messageSource.getMessage("class.restrict.exists", null, locale), accountClassRestrictionDTO.getAccountClass())); //Duplication on creation
        }
        if (accountClassRestriction != null && (!accountClassRestriction.getId().equals(accountClassRestrictionDTO.getId()))) {
            throw new DuplicateObjectException(String.format(messageSource.getMessage("class.restrict.exists", null, locale), accountClassRestrictionDTO.getAccountClass())); //Duplication on update

        }
    }


    private AccountRestrictionDTO convertAccountRestrictionEntityToDTO(AccountRestriction accountRestriction) {
        AccountRestrictionDTO accountRestrictionDTO = modelMapper.map(accountRestriction, AccountRestrictionDTO.class);
        accountRestrictionDTO.setDateCreated(DateFormatter.format(accountRestriction.getDateCreated()));

        return accountRestrictionDTO;
    }


    private AccountRestriction convertAccountRestrictionDTOToEntity(AccountRestrictionDTO accountRestrictionDTO) {
        return this.modelMapper.map(accountRestrictionDTO, AccountRestriction.class);
    }

    private List<AccountRestrictionDTO> convertAccountRestrictionEntitiesToDTOs(Iterable<AccountRestriction> accountRestrictions) {
        List<AccountRestrictionDTO> accountRestrictionDTOList = new ArrayList<>();
        for (AccountRestriction accountRestriction : accountRestrictions) {
            AccountRestrictionDTO accountRestrictionDTO = convertAccountRestrictionEntityToDTO(accountRestriction);
            accountRestrictionDTO.setRestrictionType(codeService.getByTypeAndCode("RESTRICTION_TYPE", accountRestriction.getRestrictionType()).getDescription());
            accountRestrictionDTOList.add(accountRestrictionDTO);
        }
        return accountRestrictionDTOList;
    }



    private AccountClassRestrictionDTO convertAccountClassRestrictionEntityToDTO(AccountClassRestriction accountClassRestriction) {
        AccountClassRestrictionDTO accountClassRestrictionDTO = modelMapper.map(accountClassRestriction, AccountClassRestrictionDTO.class);
        accountClassRestrictionDTO.setDateCreated(DateFormatter.format(accountClassRestriction.getDateCreated()));
        return accountClassRestrictionDTO;

    }


    private AccountClassRestriction convertAccountClassRestrictionDTOToEntity(AccountClassRestrictionDTO accountClassRestrictionDTO) {
        return this.modelMapper.map(accountClassRestrictionDTO, AccountClassRestriction.class);
    }

    private List<AccountClassRestrictionDTO> convertAccountClassRestrictionEntitiesToDTOs(Iterable<AccountClassRestriction> accountClassRestrictions) {
        List<AccountClassRestrictionDTO> accountClassRestrictionDTOList = new ArrayList<>();
        for (AccountClassRestriction accountClassRestriction : accountClassRestrictions) {
            AccountClassRestrictionDTO accountClassRestrictionDTO = convertAccountClassRestrictionEntityToDTO(accountClassRestriction);
            accountClassRestrictionDTO.setRestrictionType(codeService.getByTypeAndCode("RESTRICTION_TYPE", accountClassRestriction.getRestrictionType()).getDescription());
            accountClassRestrictionDTOList.add(accountClassRestrictionDTO);
        }
        return accountClassRestrictionDTOList;
    }


}
