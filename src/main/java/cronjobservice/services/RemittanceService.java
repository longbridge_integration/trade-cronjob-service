package cronjobservice.services;


import cronjobservice.dtos.AccountDTO;
import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.RemittanceDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.FormA;
import cronjobservice.models.Remittance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by chiomarose on 05/10/2017.
 */
public interface RemittanceService
{

//    String saveRemittance(RemittanceDTO remittanceDTO) throws CronJobException;

//    Boolean saveRemittanceFromFinacle(RemittanceDTO remittanceDTO) throws CronJobException;

    String deleteForm(Long id) throws CronJobException;
//    String updateRemittance(RemittanceDTO remittanceDTO) throws CronJobException;

    @Transactional
    Boolean updateRemittanceFromFinacle(Remittance tmsRemit, Remittance finRemit) throws CronJobException;

    Boolean updateRemittance(Remittance tmsRemit, Remittance finRemit) throws CronJobException;

    Page<RemittanceDTO> getAllRemittance(Pageable pageable) throws CronJobException;

    RemittanceDTO getInitiatorDetails() throws CronJobException;

    @javax.transaction.Transactional
    Boolean saveRemittanceFromFinacle(Remittance finremittance, FormA tmsformA) throws CronJobException;

    @javax.transaction.Transactional
    Boolean saveFundTransferFromFinacle(Remittance finFundTransfer) throws CronJobException;

    Boolean sendRemittanceITSmail(Remittance remittance);

    RemittanceDTO getRemittance(Long id) throws CronJobException;
    RemittanceDTO getRemittanceForView(Long id) throws CronJobException;

    RemittanceDTO convertEntityToDTO(Remittance remittance);

    RemittanceDTO convertEntityToDTOFinacle(Remittance remittance);

    List<CommentDTO> getComments(Remittance remittance);

    Page<RemittanceDTO> getOpenRemittance(Pageable pageable) throws CronJobException;

    List<AccountDTO> getCustomerNairaAccounts() throws CronJobException;

    List<AccountDTO> getCustomerDomAccounts() throws CronJobException;

    Boolean checkAvailableBalance(String amount, String accountId) throws CronJobException;

    long getTotalNumberOpenRemittance() throws CronJobException;

    long getTotalNumberRemittance() throws CronJobException;


}
