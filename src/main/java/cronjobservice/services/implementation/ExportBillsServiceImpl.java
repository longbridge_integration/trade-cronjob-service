package cronjobservice.services.implementation;

import cronjobservice.api.ExportBillsDetails;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.context.Context;

import java.util.*;

@Service
public class ExportBillsServiceImpl implements ExportBillsService {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    private Locale locale = LocaleContextHolder.getLocale();


    @Autowired
    ExportBillsRepo exportBillsRepo;
    @Autowired
    ServiceAuditRepo serviceAuditRepo;
    @Autowired
    ExportBillsService exportBillsService;
    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    ModelMapper  modelMapper;
    @Autowired
    BankUserService bankUserService;
    @Value("${tradeservice.service.uri}")
    private String URI;
    @Autowired
    RestTemplate template;
    @Autowired
    ConfigurationService configurationService;
    @Autowired
    MessageSource messageSource;
    @Autowired
    MailService mailService;
    @Autowired
    IntegrationService integrationService;
    @Autowired
    ExportBillForCollectionRepo exportBillForCollectionRepo;
    @Autowired
    BillsUnderLcRepo billsUnderLcRepo;


    @Transactional
    @Override
    public List<String> LodgeExportBillsUnderCollection() throws CronJobException {
        try{
            HashMap<String, String> map = new HashMap<>();
            HashMap<String, Date> map2 = new HashMap<>();

            List<String> responses = new ArrayList<>();
            List<ExportBillForCollection> exportBillForCollections = exportBillForCollectionRepo.findByStatusAndSubmitFlag("A", "U");

            int exportBillForCollectionSize = exportBillForCollections.size();
            logger.info("This is the Number of Export Bills For collection Fetched from TMS {}", exportBillForCollectionSize);

            for (ExportBillForCollection exportBillForCollection:exportBillForCollections) {

                List<CommentDTO> commentDTO = exportBillsService.getComments(exportBillForCollection);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    commentDtd.getMadeOn();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                    map2.put("madeOn", commentDtd.getMadeOn());
                });
                logger.info("Export Bill Temp No  {} ",exportBillForCollection.getTemporalBllNumb());
                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
                ExportBillForCollectionDTO exportBillForCollectionDTO = exportBillsService.convertEntityToExportBillForCollectionDTO(exportBillForCollection);

                exportBillForCollectionDTO.setComments(map.get("comment"));
//                exportBillForCollectionDTO.setSo(bankUser.getBranch().getSortCode());
//                logger.info("The Main Export Bill Bank Useer{}", bankUser.getFirstName());

                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(exportBillForCollection.getTemporalBllNumb(), RequestType.LODGE_EXPORT_BILLS_FOR_COLLECTION);
                logger.info("Export Bill Audit size {}", serviceAudits);
                int auditSize = serviceAudits.size();

                if(auditSize >=10) {
                    exportBillForCollection.setSubmitFlag("F");
                    exportBillForCollectionRepo.save(exportBillForCollection);
                    exportBillsService.sendLodgeBillUnderCollectionITSmail(exportBillForCollection);

                    logger.info("UPDATED Export Bill For Colllection SUBMIT FLAG WITH F DUE TO FAILURE");
                }

                if (auditSize < 10) {

                    String uri = URI + "/api/bills/export/forcollection/lodge";
                    try{
                        String lodgeBillSubmitResponse = template.postForObject(uri, exportBillForCollectionDTO, String.class);
                        logger.info("Lodge Export Bill FOr collection to Finacle  Message {} ", lodgeBillSubmitResponse);
                        responses.add(lodgeBillSubmitResponse);

                        ServiceAudit serviceAudit = new ServiceAudit();
                        serviceAudit.setRequestType(RequestType.LODGE_EXPORT_BILLS_FOR_COLLECTION);
                        serviceAudit.setRequestDate(new Date());
                        serviceAudit.setTempNumber(exportBillForCollectionDTO.getTemporalBllNumb());

                        if (lodgeBillSubmitResponse.equalsIgnoreCase("Y")) {
                            exportBillForCollection.setSubmitFlag("S");
                            serviceAudit.setResponseFlag("Y");
                            serviceAudit.setResponseMessage("Successful");
                            serviceAuditRepo.save(serviceAudit);
                            exportBillForCollectionRepo.save(exportBillForCollection);

                        } else if (lodgeBillSubmitResponse.equalsIgnoreCase("S")) {
                            exportBillForCollection.setStatus("PV");
                            exportBillForCollection.setSubmitFlag("S");
                            exportBillForCollectionRepo.save(exportBillForCollection);
                        } else {
                            serviceAudit.setResponseFlag("N");
                            serviceAudit.setResponseMessage(lodgeBillSubmitResponse);
                            serviceAuditRepo.save(serviceAudit);
                            logger.error("Error response for Lodge Export Bill For Collection {} ", lodgeBillSubmitResponse);
                        }

                    }catch (Exception e){
                        logger.info("Lodge Export Bill For Collection Response error {} ",e);
                    }

                }

            }
            return responses;
        }catch(Exception e){
            logger.info("Error submitting Lodge Export Bill For collection to Finacle {} ",e);
            throw  new CronJobException();
        }
    }

    @Override
    public void getLodgeResponseExportBillsUnderCollection(){

        List<ExportBillsDetails> exportBillsDetails = integrationService.getLodgeBillForCollectionResponse();

        try{

            for (ExportBillsDetails exportBillsDetail : exportBillsDetails) {

                if (null==exportBillsDetail.getBillNumber()) {
                    logger.info("The Export Bill number from Finacle Does Not Exist ");  //convertEntityToImportLcDTO
                    continue;
                }
                ExportBillForCollection finexportBillForCollection = modelMapper.map(exportBillsDetail, ExportBillForCollection.class);
//                ExportBillForCollectionDTO exportBillForCollectionDTO = exportBillsService.convertEntityToExportBillForCollectionDTO(finexportBillForCollection);
                logger.info(" Export Bill Parametre {} ", finexportBillForCollection.getTemporalBllNumb());
//                CorporateUser corporateUser = corporateUserRepo.findFirstByCustomerId(cifId);
                ExportBillForCollection tmsexportbillforcollection = exportBillForCollectionRepo.findByTemporalBllNumb(finexportBillForCollection.getTemporalBllNumb());

                if(null != tmsexportbillforcollection) {
                    Boolean response = exportBillsService.updateExportBillsForCollectionFromFinacle(tmsexportbillforcollection, finexportBillForCollection);

                    if (response) {
                        String uri = URI + "/api/bills/export/forcollection/updateFlag";
                        try {
                            template.postForObject(uri, finexportBillForCollection, String.class);
                            logger.info("Success after response Export Bills for collection");
                        } catch (Exception ex) {
                            logger.error("Exception occurred getting response for Export Bills (Lodge) for collection {}", ex);
                            ex.printStackTrace();
                        }
                    }
                }else {
                    logger.info("lodge Export Bill For collection   not Saved");
                }

            }
        }catch (Exception e){
            logger.info("Error getting Export Bill (Lodge) Response from FInacle {} ",e);
            throw new CronJobException();
        }
    }


    @Transactional
    @Override
    public List<String> LodgeExportBillsUnderLc() throws CronJobException {
        try{
            HashMap<String, String> map = new HashMap<>();
            HashMap<String, Date> map2 = new HashMap<>();

            List<String> responses = new ArrayList<>();
            List<BillsUnderLc> billsUnderLcs = billsUnderLcRepo.findByStatusAndSubmitFlag("A", "U");

            int billsunderLcsize = billsUnderLcs.size();
            logger.info("This is the Number of Export Bills Under Lc Fetched from TMS {}", billsunderLcsize);

            for (BillsUnderLc billsUnderLc:billsUnderLcs) {

                List<CommentDTO> commentDTO = exportBillsService.getComments(billsUnderLc);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    commentDtd.getMadeOn();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                    map2.put("madeOn", commentDtd.getMadeOn());
                });
                logger.info("Export Bill under Lc Temp No  {} ",billsUnderLc.getTemporalBllNumb());
                logger.info("Export Bill under Lc exporter name {} ",billsUnderLc.getExportLc().getFormNxp().getExporterName());
                logger.info("Export Bill under Lc export lc No  {} ",billsUnderLc.getExportLc().getLcNumber());
                logger.info("Export Bill under Lc nxp  No  {} ",billsUnderLc.getExportLc().getFormNxp().getFormNum());
                logger.info("Lodge Export Bill under lc document {} ", billsUnderLc.getDocuments().size());
                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
                BillsDTO billsDTO = exportBillsService.convertEntityToBillsUnderLcDTO(billsUnderLc);

                billsDTO.setComments(map.get("comment"));
                logger.info("The Main Export Bill under Lc Bank Useer{}", bankUser.getFirstName());

                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(billsUnderLc.getTemporalBllNumb(), RequestType.LODGE_EXPORT_BILLS_UNDER_LC);
                logger.info("Export Bill under Lc Audit size {}", serviceAudits.size());
                int auditSize = serviceAudits.size();

                if(auditSize >=10) {
                    billsUnderLc.setSubmitFlag("F");
                    billsUnderLcRepo.save(billsUnderLc);
                    exportBillsService.sendLodgeBillUnderLcITSmail(billsUnderLc);

                    logger.info("UPDATED Export Bill under Lc SUBMIT FLAG WITH F DUE TO FAILURE");
                }

                if (auditSize < 10) {

                    String uri = URI + "/api/bills/export/underlc/lodge";
                    try{
                        logger.info("Lodge Export Bill under lc document {} ", billsDTO.getDocuments().size());
                        String lodgeBillSubmitResponse = template.postForObject(uri, billsDTO, String.class);
                        logger.info("Lodge Export Bill under lc to Finacle  Message {} ", lodgeBillSubmitResponse);
                        responses.add(lodgeBillSubmitResponse);

                        ServiceAudit serviceAudit = new ServiceAudit();
                        serviceAudit.setRequestType(RequestType.LODGE_EXPORT_BILLS_UNDER_LC);
                        serviceAudit.setRequestDate(new Date());
                        serviceAudit.setTempNumber(billsDTO.getTemporalBllNumb());

                        if (lodgeBillSubmitResponse.equalsIgnoreCase("Y")) {
                            billsUnderLc.setSubmitFlag("S");
                            serviceAudit.setResponseFlag("Y");
                            serviceAudit.setResponseMessage("Successful");
                            serviceAuditRepo.save(serviceAudit);
                            billsUnderLcRepo.save(billsUnderLc);

                        } else if (lodgeBillSubmitResponse.equalsIgnoreCase("S")) {
                            billsUnderLc.setStatus("PV");
                            billsUnderLc.setSubmitFlag("S");
                            billsUnderLcRepo.save(billsUnderLc);
                        } else {
                            serviceAudit.setResponseFlag("N");
                            serviceAudit.setResponseMessage(lodgeBillSubmitResponse);
                            serviceAuditRepo.save(serviceAudit);
                            logger.error("Error response for Lodge Export Bill under lc {} ", lodgeBillSubmitResponse);
                        }

                    }catch (Exception e){
                        logger.info("Lodge Export Bill under lc Response error {} ",e);
                    }

                }

            }
            return responses;
        }catch(Exception e){
            logger.info("Error submitting Lodge Export Bill For collection to Finacle {} ",e);
            throw  new CronJobException();
        }
    }




    @Override
    public void getLodgeResponseExportBillsUnderLc(){

        List<ExportBillsDetails> exportBillsDetails = integrationService.getLodgeBillUnderLcResponse();

        try{

            for (ExportBillsDetails exportBillsDetail : exportBillsDetails) {


                if (null==exportBillsDetail.getBillNumber()) {
                    logger.info("The Export Bill number under lc from Finacle Does Not Exist");  //convertEntityToImportLcDTO
                    continue;
                }
                BillsUnderLc finbillsUnderLc = modelMapper.map(exportBillsDetail, BillsUnderLc.class);
//                BillsDTO billsDTO = exportBillsService.convertEntityToBillsUnderLcDTO(finbillsUnderLc);
                logger.info(" Export Bill Parametre {} ", finbillsUnderLc.getTemporalBllNumb());

                BillsUnderLc tmsbillsunderlc = billsUnderLcRepo.findByTemporalBllNumb(finbillsUnderLc.getTemporalBllNumb());

                if(null != tmsbillsunderlc) {
                    Boolean response = exportBillsService.updateExportBillsUnderLcFromFinacle(tmsbillsunderlc, finbillsUnderLc);

                    if (response) {
                        String uri = URI + "/api/bills/export/underlc/updateFlag";
                        try {
                            template.postForObject(uri, finbillsUnderLc, String.class);
                            logger.info("Success after response Export Bills under lc");
                        } catch (Exception ex) {
                            logger.error("Exception occurred getting response for Export Bills (Lodge) under lc {}", ex);
                            ex.printStackTrace();
                        }
                    }
                }else {
                    logger.info("lodge Export Bill under lc not Saved");
                }

            }
        }catch (Exception e){
            logger.info("Error getting Export Bill (Lodge) Response from FInacle {} ",e);
            throw new CronJobException();
        }
    }




    @Override
    public Boolean sendLodgeBillUnderCollectionITSmail(ExportBillForCollection exportBillForCollection) {
        try {
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "Lodge Export Bill For Collection";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(exportBillForCollection.getTemporalBllNumb(),RequestType.LODGE_EXPORT_BILLS_FOR_COLLECTION);
            Context context = new Context();
            context.setVariable("name", exportBillForCollection.getImporterName());
            context.setVariable("formNumber", exportBillForCollection.getTemporalBllNumb());
            context.setVariable("formType", formType);
            context.setVariable("date", DateFormatter.format(new Date()));
            context.setVariable("customerId", exportBillForCollection.getCifid());
            context.setVariable("username", exportBillForCollection.getInitiatedBy());
            context.setVariable("error", serviceAudit.getResponseMessage());
            context.setVariable("code", "LODGE_EXPORT_BILLS_FOR_COLLECTION");
            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
            Email email = new Email.Builder()
                    .setRecipients(allMails)
                    .setSubject(subject)
                    .setTemplate("mail/finacleSubmission")
                    .build();

            mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("guarantee.add.success", null, locale));
        }
    }


    @Override
    public Boolean sendLodgeBillUnderLcITSmail(BillsUnderLc billsUnderLc) {
        try {
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "Lodge Export Bill Under Lc";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(billsUnderLc.getTemporalBllNumb(),RequestType.LODGE_EXPORT_BILLS_UNDER_LC);
            Context context = new Context();
            context.setVariable("name", billsUnderLc.getImporterName());
            context.setVariable("formNumber", billsUnderLc.getTemporalBllNumb());
            context.setVariable("formType", formType);
            context.setVariable("date", DateFormatter.format(new Date()));
            context.setVariable("customerId", billsUnderLc.getCifid());
            context.setVariable("username", billsUnderLc.getInitiatedBy());
            context.setVariable("error", serviceAudit.getResponseMessage());
            context.setVariable("code", "LODGE_EXPORT_BILLS_UNDER_LC");
            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
            Email email = new Email.Builder()
                    .setRecipients(allMails)
                    .setSubject(subject)
                    .setTemplate("mail/finacleSubmission")
                    .build();

            mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("guarantee.add.success", null, locale));
        }
    }


    @Override
    public ExportBillsDTO convertEntityTODTO(ExportBills exportBills) {
        ExportBillsDTO exportBillsDTO = modelMapper.map(exportBills, ExportBillsDTO.class);
        if (exportBills.getStatus() != null && !(exportBills.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(exportBillsDTO.getWorkFlow().getCodeType(), exportBillsDTO.getStatus());
            exportBillsDTO.setStatusDesc(workFlowStates.getDescription());
            exportBillsDTO.setWorkFlowId(exportBills.getWorkFlow().getId());
        }
        return exportBillsDTO;
    }

//    @Override
//    public BillsDTO convertEntityTODLCTO(Bills bills) {
//        return null;
//    }

    @Override
    public BillsDTO convertEntityTODTO(BillsUnderLc bills) {
        BillsDTO billsDTO = modelMapper.map(bills, BillsDTO.class);
        if (bills.getStatus() != null && !(bills.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(billsDTO.getWorkFlow().getCodeType(), billsDTO.getStatus());
            billsDTO.setStatusDesc(workFlowStates.getDescription());
            billsDTO.setWorkFlowId(bills.getWorkFlow().getId());
        }
        return billsDTO;
    }

    @Override
    public ExportBillForCollectionDTO convertEntityToExportBillForCollectionDTO(ExportBillForCollection exportBillForCollection) {
        ExportBillForCollectionDTO exportBillForCollectionDTO = modelMapper.map(exportBillForCollection, ExportBillForCollectionDTO.class);

        exportBillForCollectionDTO.setFormNxpNumber(exportBillForCollection.getFormNxp().getFormNum());
        exportBillForCollectionDTO.setExporterName(exportBillForCollection.getFormNxp().getExporterName());
        exportBillForCollectionDTO.setExporterAddress(exportBillForCollection.getFormNxp().getExporterAddress());
        exportBillForCollectionDTO.setExporterCity(exportBillForCollection.getFormNxp().getExporterCity());
        exportBillForCollectionDTO.setExporterState(exportBillForCollection.getFormNxp().getExporterState());
        exportBillForCollectionDTO.setExporterCountry(exportBillForCollection.getFormNxp().getExporterCountry());

        if (exportBillForCollection.getWorkFlow() != null) {
            exportBillForCollectionDTO.setWorkFlowId(exportBillForCollection.getWorkFlow().getId());
//            WorkFlowStatesDTO workFlowState = workFlowStatesService.getByTypeAndCode(importLc.getWorkFlow().getCodeType(), importLc.getStatus());
//            importLcDTO.setStatusDesc(workFlowState.getDescription());
        }

        return exportBillForCollectionDTO;
    }

//    @Override
//    public BillsDTO convertEntityToBillsUnderLcDTO(Bill billsUnderLc) {
//        return null;
//    }

    @Override
    public BillsDTO convertEntityToBillsUnderLcDTO(BillsUnderLc billsUnderLc) {
        BillsDTO billsUnderLcDTO = modelMapper.map(billsUnderLc, BillsDTO.class);
       billsUnderLcDTO.setLcNumber(billsUnderLc.getExportLc().getLcNumber());
       billsUnderLcDTO.setLcCurrency(billsUnderLc.getExportLc().getLcCurrency());
       billsUnderLcDTO.setExporterName(billsUnderLc.getExportLc().getFormNxp().getExporterName());
        billsUnderLcDTO.setExporterAddress(billsUnderLc.getExportLc().getFormNxp().getExporterAddress());
        billsUnderLcDTO.setExporterCountry(billsUnderLc.getExportLc().getFormNxp().getExporterCountry());
        billsUnderLcDTO.setExporterState(billsUnderLc.getExportLc().getFormNxp().getExporterState());
        billsUnderLcDTO.setFormNxpNumber(billsUnderLc.getExportLc().getFormNxp().getFormNum());

        if (billsUnderLc.getWorkFlow() != null) {
            billsUnderLcDTO.setWorkFlowId(billsUnderLc.getWorkFlow().getId());

        }

        return billsUnderLcDTO;
    }





    @Override
    public List<CommentDTO> getComments(ExportBills exportBills) throws CronJobException {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        {
            commentDTOList=convertEntitiestoDTOs(exportBills.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus()!="A");


        }
        return  commentDTOList;
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS)
    {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        for(Comments comments:commentDTOS)
        {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return  commentDTOList;
    }

    private CommentDTO convertEntityToDTO(Comments comments)
    {
        WorkFlowStates workFlowStates= workFlowStatesRepo.findByTypeAndCode("ExportBills",comments.getStatus());
        if(workFlowStates!=null)
        {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments,CommentDTO.class);
    }

    @Override
    public ExportBillsDTO convertEntityTODTOFinacle(ExportBills exportBills)
    {
        ExportBillsDTO exportBillsDTO=modelMapper.map(exportBills , ExportBillsDTO.class);

        if(exportBills.getStatus() != null && !(exportBills.getStatus().equals("P")) ){
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(exportBillsDTO.getWorkFlow().getCodeType(), exportBillsDTO.getStatus());
            exportBillsDTO.setStatusDesc(workFlowStates.getDescription());
            exportBillsDTO.setWorkFlowId(exportBills.getWorkFlow().getId());
        }


        return exportBillsDTO;
    }

    @Transactional
    @Override
    public Boolean updateExportBillsForCollectionFromFinacle(ExportBillForCollection tmsExportBillForCollection, ExportBillForCollection finExportBillForCollection) throws CronJobException {
        try {

            tmsExportBillForCollection.setBillNumber(finExportBillForCollection.getBillNumber());
            tmsExportBillForCollection.setStatus("A");
            exportBillForCollectionRepo.save(tmsExportBillForCollection);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

    @Transactional
    @Override
    public Boolean updateExportBillsUnderLcFromFinacle(BillsUnderLc tmsBillsUnderLc, BillsUnderLc finBillsUnderLc) throws CronJobException {
        try {

            tmsBillsUnderLc.setBillNumber(finBillsUnderLc.getBillNumber());
            tmsBillsUnderLc.setStatus("A");
            billsUnderLcRepo.save(tmsBillsUnderLc);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }


}
