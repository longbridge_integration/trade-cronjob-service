package cronjobservice.repositories;


import cronjobservice.models.FormM;
import cronjobservice.models.Paar;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaarRepo extends CommonRepo<Paar,Long>{

    int countByFormM(FormM formM);

    List<Paar> findByFormM(FormM formM);

    Paar findByFormNumber(String formNumber);
    Paar findFirstByFormM(FormM formM);
//    P find(FormM formM);
    Long countByCifid(String cifid);

    @Query("select s from Paar s where s.status!=:openstatus")
    Page<Paar> findStatus(@Param("openstatus") String status, Pageable pageable);

    @Query("select s from Paar s where s.status=:openstatus")
    Page<Paar> findOpenStatus(@Param("openstatus") String status, Pageable pageable);

    Page<Paar> findByStatusIsNull(Pageable pageable);

    Page<Paar> findByStatusNotNull(Pageable pageable);

    Page<Paar> findByCifidAndStatusNotNull(String cif, Pageable pageable);

    @Query("select s from Paar s where s.cifid=:cifid  and  s.status=:submitted ")
    Page<Paar> findClosedLC(@Param("cifid") String cifid, @Param("submitted") String status, Pageable pageable);

//  @Query("select s from Paar s where s.status is not null and s.status=:submitted")
//  Page<Paar> findByStatusAndCifid(@Param("cifid") String cifid,@Param("submitted") String submitted,Pageable pageable);


    @Query("select distinct s from Paar s where s.cifid=:cifid and s.status=:submitted")
    Page<Paar> findByCifidAndStatusS(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

//  @Query("select s from Paar s where s.status is null or s.status=:submitted or s.status=:declined")
//  Page<Paar> findByStatus(@Param("submitted") String submitted,@Param("declined") String decline,Pageable pageable);


    @Query("select distinct s from Paar s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<Paar> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);



    @Query("select distinct s from Paar s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<Paar> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    Page<Paar> findByCifid(String cifid, Pageable pageable);

    @Query("select distinct s from Paar s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<Paar> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    @Query("select distinct s from Paar s where s.cifid=:cifid  and  s.status is not null  and   s.status!=:submitted and s.status!=:saved")
    Page<Paar> findByClosedCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, @Param("saved") String saved, Pageable pageable);



    //Getting Paar Details for Import Bill
    Paar findByPaarNumber(String paarNumber);

    @Query("select s from Paar s where  s.status=:issued")
    Page<Paar> findFormByStatus(@Param("issued") String submitted, Pageable pageable);



    @Query("select s from Paar s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<Paar> findByCifidAndStatusP(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);


    @Query("select s from Paar s  where (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<Paar> findByAndStatusP(@Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);

}
