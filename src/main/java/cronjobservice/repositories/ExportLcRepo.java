package cronjobservice.repositories;

import cronjobservice.models.ExportLc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 10/11/2017.
 */
@Repository
public interface ExportLcRepo extends CommonRepo<ExportLc,Long> {

    Long countByCifid(String cifid);

    ExportLc findByLcNumber(String lcNumber);

    ExportLc findByTempLcNumber(String tempLcNumber);

    List<ExportLc> findByStatusAndSubmitFlag(String S, String SF);

    @Query("select s from ExportLc s where s.cifid=:cifId and (s.status=:submitted or s.status=:declined )")
    Page<ExportLc> findExportByCifid(@Param("cifId") String cifId, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);

    @Query("select s from ExportLc s where s.status=:decline or s.status=:saved ")
    Page<ExportLc> findExportLcByStatus(@Param("decline") String decline, @Param("saved") String saved, Pageable pageable);

    @Query("select distinct s from ExportLc s  where  s.status=:submitted or s.status=:saved")
    Page<ExportLc> findByStatus(@Param("submitted") String submitted, @Param("saved") String saved, Pageable pageable);


    @Query("select s from ExportLc s where s.cifid=:cifId and s.status=:approved ")
    Page<ExportLc> findApprovedLC(@Param("cifId") String cifId, @Param("approved") String approved, Pageable pageable);




}
