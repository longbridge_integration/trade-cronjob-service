package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class BTACorpUsers  extends  AbstractEntity{


    private String firstName;
    private String lastName;
    private String corpBvn;
    private String travelDate;
    private String flightHour;
    private String issueDate;
    private String dob;
    private String countryOfVisit;

    @JsonIgnore
    @ManyToOne
    private BTA bta;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCorpBvn() {
        return corpBvn;
    }

    public void setCorpBvn(String corpBvn) {
        this.corpBvn = corpBvn;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public String getFlightHour() {
        return flightHour;
    }

    public void setFlightHour(String flightHour) {
        this.flightHour = flightHour;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCountryOfVisit() {
        return countryOfVisit;
    }

    public void setCountryOfVisit(String countryOfVisit) {
        this.countryOfVisit = countryOfVisit;
    }

    public BTA getBta() {
        return bta;
    }

    public void setBta(BTA bta) {
        this.bta = bta;
    }
}
