package cronjobservice.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cronjobservice.models.FormaDocument;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * Created by SYLVESTER on 9/17/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FormADetails {


    private String firstName;
    private String lastName;
    private String corporateName;
    private String address;
    private String town;
    private String state;
    private String phone;
    private String email;
    private String postCode;
    private String country;
    private String bidRateCode;
    private boolean guidelineApproval;
    private String route;
    private String airLine;
    private String airTicketNo;
    private String registration;

    private String amountInWords;

    private String formNumber;//ref No
    private String tempFormNumber;//trade app no
    private String applicationNumber;//fin app no


    private BigDecimal utilization;
    private BigDecimal allocatedAmount;
    private String transactionDebitAmt;
    private String allocationAccount;
    private String allocationCurrencyCode;//fetch currency code but bank editable
    private BigDecimal amount;
    private BigDecimal approvalAmount;//amount approved - bank
    private String approvalCurrency;//currency of approved - bank
    private String approverName;//Name of approver - bank

    private String chargeAccounts;
    private String transferAccounts;

    private String operativeAccount;//bank
    private String operationCode;//bank


    private String beneficiaryAddress;
    private String beneficiaryCountry;
    private String beneficiaryPhone;
    private String beneficiaryRegistration;
    private String beneficiaryTown;
    private String beneficiaryState;
    private String beneficiaryName;
    private String beneficiaryAccount;
    private String beneficiaryPostCode;

    private String interBankName;
    private String interBankAddr;
    private String interBankSwiftCode;
    private String interBankCtry;
    private String interBankState;
    private String interBankCity;
    private String interBankPoCode;
    private String interBankIdent;//code dropdown remittance

    private String remitanceId;
    private String remittanceType;//Codes
    private String messageType;//bank
    private String remittedCurrency;//fetch currency but bank editable- third world
    private String remittedAmount;//bank
    private String remittedExRate;//bank
    private String remittedRateCode;//bank
    private Date remittedDate;//bank

    private String chargeAccount;
    private String realizationAcct;

    private String corrBankName;
    private String corrBankAddr;
    private String corrBankSwiftCode;
    private String corrBankCtry;
    private String corrBankState;
    private String corrBankCity;
    private String corrBankPoCode;
    private String corrBankIdent;//code dropdown remittance
    private String corrNostroAcct;

    private String beneBankName;
    private String beneBankAddr;
    private String beneBankSwiftCode;
    private String beneBankCtry;
    private String beneBankCity;
    private String beneBankState;
    private String beneBankPoCode;
    private String beneBankIdent;//code dropdown remitance

    private String forexValue;
    private String initiatedBy;
    private String userType;
    private String purposeOfPayment;
    private String secPuposeCode;

    private BigDecimal nairaEquivalent;
    private String currencyCode;
    private String exchangeRate;
    private String paymentMode;
    private String modeOfIdentification;
    private String identityNo;
    private String paymentCode;

    private String description;

    private Date expiryDate;
    private Date  dateCreated;
    private String offShoreChargeDoneBy;
    private BigDecimal offShoreCharges;
    private String senderToReceiverInfo;
    private String senderToReceiverInfos;
    private String remittanceInfo;//bank-customer
    private String remittanceInfos;//bank-customer

    private String attachments;
    private Collection<FormaDocument> documents;
    private String status;
    private String cifid;
    private Date createdOn;
    private BigDecimal dollarEquivalent;
    private String formStatus;
    private String concessionFlag;
    private Double concessionPercentage;
    private String bankCode;
    private String submitFlag;
    private String docSubBranch;
    private Date dateOfAllocation;
    private String dealId;
    private String tranId;


    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBidRateCode() {
        return bidRateCode;
    }

    public void setBidRateCode(String bidRateCode) {
        this.bidRateCode = bidRateCode;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getAirLine() {
        return airLine;
    }

    public void setAirLine(String airLine) {
        this.airLine = airLine;
    }

    public String getAirTicketNo() {
        return airTicketNo;
    }

    public void setAirTicketNo(String airTicketNo) {
        this.airTicketNo = airTicketNo;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getAmountInWords() {
        return amountInWords;
    }

    public void setAmountInWords(String amountInWords) {
        this.amountInWords = amountInWords;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public String getTempFormNumber() {
        return tempFormNumber;
    }

    public void setTempFormNumber(String tempFormNumber) {
        this.tempFormNumber = tempFormNumber;
    }

    public String getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public BigDecimal getUtilization() {
        return utilization;
    }

    public void setUtilization(BigDecimal utilization) {
        this.utilization = utilization;
    }

    public BigDecimal getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(BigDecimal allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public String getTransactionDebitAmt() {
        return transactionDebitAmt;
    }

    public void setTransactionDebitAmt(String transactionDebitAmt) {
        this.transactionDebitAmt = transactionDebitAmt;
    }

    public String getAllocationAccount() {
        return allocationAccount;
    }

    public void setAllocationAccount(String allocationAccount) {
        this.allocationAccount = allocationAccount;
    }

    public String getAllocationCurrencyCode() {
        return allocationCurrencyCode;
    }

    public void setAllocationCurrencyCode(String allocationCurrencyCode) {
        this.allocationCurrencyCode = allocationCurrencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getApprovalAmount() {
        return approvalAmount;
    }

    public void setApprovalAmount(BigDecimal approvalAmount) {
        this.approvalAmount = approvalAmount;
    }

    public String getApprovalCurrency() {
        return approvalCurrency;
    }

    public void setApprovalCurrency(String approvalCurrency) {
        this.approvalCurrency = approvalCurrency;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public String getChargeAccounts() {
        return chargeAccounts;
    }

    public void setChargeAccounts(String chargeAccounts) {
        this.chargeAccounts = chargeAccounts;
    }

    public String getTransferAccounts() {
        return transferAccounts;
    }

    public void setTransferAccounts(String transferAccounts) {
        this.transferAccounts = transferAccounts;
    }

    public String getOperativeAccount() {
        return operativeAccount;
    }

    public void setOperativeAccount(String operativeAccount) {
        this.operativeAccount = operativeAccount;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getBeneficiaryAddress() {
        return beneficiaryAddress;
    }

    public void setBeneficiaryAddress(String beneficiaryAddress) {
        this.beneficiaryAddress = beneficiaryAddress;
    }

    public String getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    public void setBeneficiaryCountry(String beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
    }

    public String getBeneficiaryPhone() {
        return beneficiaryPhone;
    }

    public void setBeneficiaryPhone(String beneficiaryPhone) {
        this.beneficiaryPhone = beneficiaryPhone;
    }

    public String getBeneficiaryRegistration() {
        return beneficiaryRegistration;
    }

    public void setBeneficiaryRegistration(String beneficiaryRegistration) {
        this.beneficiaryRegistration = beneficiaryRegistration;
    }

    public String getBeneficiaryTown() {
        return beneficiaryTown;
    }

    public void setBeneficiaryTown(String beneficiaryTown) {
        this.beneficiaryTown = beneficiaryTown;
    }

    public String getBeneficiaryState() {
        return beneficiaryState;
    }

    public void setBeneficiaryState(String beneficiaryState) {
        this.beneficiaryState = beneficiaryState;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryAccount() {
        return beneficiaryAccount;
    }

    public void setBeneficiaryAccount(String beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
    }

    public String getBeneficiaryPostCode() {
        return beneficiaryPostCode;
    }

    public void setBeneficiaryPostCode(String beneficiaryPostCode) {
        this.beneficiaryPostCode = beneficiaryPostCode;
    }

    public String getInterBankName() {
        return interBankName;
    }

    public void setInterBankName(String interBankName) {
        this.interBankName = interBankName;
    }

    public String getInterBankAddr() {
        return interBankAddr;
    }

    public void setInterBankAddr(String interBankAddr) {
        this.interBankAddr = interBankAddr;
    }

    public String getInterBankSwiftCode() {
        return interBankSwiftCode;
    }

    public void setInterBankSwiftCode(String interBankSwiftCode) {
        this.interBankSwiftCode = interBankSwiftCode;
    }

    public String getInterBankCtry() {
        return interBankCtry;
    }

    public void setInterBankCtry(String interBankCtry) {
        this.interBankCtry = interBankCtry;
    }

    public String getInterBankState() {
        return interBankState;
    }

    public void setInterBankState(String interBankState) {
        this.interBankState = interBankState;
    }

    public String getInterBankCity() {
        return interBankCity;
    }

    public void setInterBankCity(String interBankCity) {
        this.interBankCity = interBankCity;
    }

    public String getInterBankPoCode() {
        return interBankPoCode;
    }

    public void setInterBankPoCode(String interBankPoCode) {
        this.interBankPoCode = interBankPoCode;
    }

    public String getInterBankIdent() {
        return interBankIdent;
    }

    public void setInterBankIdent(String interBankIdent) {
        this.interBankIdent = interBankIdent;
    }

    public String getRemitanceId() {
        return remitanceId;
    }

    public void setRemitanceId(String remitanceId) {
        this.remitanceId = remitanceId;
    }

    public String getRemittanceType() {
        return remittanceType;
    }

    public void setRemittanceType(String remittanceType) {
        this.remittanceType = remittanceType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getRemittedCurrency() {
        return remittedCurrency;
    }

    public void setRemittedCurrency(String remittedCurrency) {
        this.remittedCurrency = remittedCurrency;
    }

    public String getRemittedAmount() {
        return remittedAmount;
    }

    public void setRemittedAmount(String remittedAmount) {
        this.remittedAmount = remittedAmount;
    }

    public String getRemittedExRate() {
        return remittedExRate;
    }

    public void setRemittedExRate(String remittedExRate) {
        this.remittedExRate = remittedExRate;
    }

    public String getRemittedRateCode() {
        return remittedRateCode;
    }

    public void setRemittedRateCode(String remittedRateCode) {
        this.remittedRateCode = remittedRateCode;
    }

    public Date getRemittedDate() {
        return remittedDate;
    }

    public void setRemittedDate(Date remittedDate) {
        this.remittedDate = remittedDate;
    }

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public String getRealizationAcct() {
        return realizationAcct;
    }

    public void setRealizationAcct(String realizationAcct) {
        this.realizationAcct = realizationAcct;
    }

    public String getCorrBankName() {
        return corrBankName;
    }

    public void setCorrBankName(String corrBankName) {
        this.corrBankName = corrBankName;
    }

    public String getCorrBankAddr() {
        return corrBankAddr;
    }

    public void setCorrBankAddr(String corrBankAddr) {
        this.corrBankAddr = corrBankAddr;
    }

    public String getCorrBankSwiftCode() {
        return corrBankSwiftCode;
    }

    public void setCorrBankSwiftCode(String corrBankSwiftCode) {
        this.corrBankSwiftCode = corrBankSwiftCode;
    }

    public String getCorrBankCtry() {
        return corrBankCtry;
    }

    public void setCorrBankCtry(String corrBankCtry) {
        this.corrBankCtry = corrBankCtry;
    }

    public String getCorrBankState() {
        return corrBankState;
    }

    public void setCorrBankState(String corrBankState) {
        this.corrBankState = corrBankState;
    }

    public String getCorrBankCity() {
        return corrBankCity;
    }

    public void setCorrBankCity(String corrBankCity) {
        this.corrBankCity = corrBankCity;
    }

    public String getCorrBankPoCode() {
        return corrBankPoCode;
    }

    public void setCorrBankPoCode(String corrBankPoCode) {
        this.corrBankPoCode = corrBankPoCode;
    }

    public String getCorrBankIdent() {
        return corrBankIdent;
    }

    public void setCorrBankIdent(String corrBankIdent) {
        this.corrBankIdent = corrBankIdent;
    }

    public String getCorrNostroAcct() {
        return corrNostroAcct;
    }

    public void setCorrNostroAcct(String corrNostroAcct) {
        this.corrNostroAcct = corrNostroAcct;
    }

    public String getBeneBankName() {
        return beneBankName;
    }

    public void setBeneBankName(String beneBankName) {
        this.beneBankName = beneBankName;
    }

    public String getBeneBankAddr() {
        return beneBankAddr;
    }

    public void setBeneBankAddr(String beneBankAddr) {
        this.beneBankAddr = beneBankAddr;
    }

    public String getBeneBankSwiftCode() {
        return beneBankSwiftCode;
    }

    public void setBeneBankSwiftCode(String beneBankSwiftCode) {
        this.beneBankSwiftCode = beneBankSwiftCode;
    }

    public String getBeneBankCtry() {
        return beneBankCtry;
    }

    public void setBeneBankCtry(String beneBankCtry) {
        this.beneBankCtry = beneBankCtry;
    }

    public String getBeneBankCity() {
        return beneBankCity;
    }

    public void setBeneBankCity(String beneBankCity) {
        this.beneBankCity = beneBankCity;
    }

    public String getBeneBankState() {
        return beneBankState;
    }

    public void setBeneBankState(String beneBankState) {
        this.beneBankState = beneBankState;
    }

    public String getBeneBankPoCode() {
        return beneBankPoCode;
    }

    public void setBeneBankPoCode(String beneBankPoCode) {
        this.beneBankPoCode = beneBankPoCode;
    }

    public String getBeneBankIdent() {
        return beneBankIdent;
    }

    public void setBeneBankIdent(String beneBankIdent) {
        this.beneBankIdent = beneBankIdent;
    }

    public String getForexValue() {
        return forexValue;
    }

    public void setForexValue(String forexValue) {
        this.forexValue = forexValue;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getPurposeOfPayment() {
        return purposeOfPayment;
    }

    public void setPurposeOfPayment(String purposeOfPayment) {
        this.purposeOfPayment = purposeOfPayment;
    }

    public String getSecPuposeCode() {
        return secPuposeCode;
    }

    public void setSecPuposeCode(String secPuposeCode) {
        this.secPuposeCode = secPuposeCode;
    }

    public BigDecimal getNairaEquivalent() {
        return nairaEquivalent;
    }

    public void setNairaEquivalent(BigDecimal nairaEquivalent) {
        this.nairaEquivalent = nairaEquivalent;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getModeOfIdentification() {
        return modeOfIdentification;
    }

    public void setModeOfIdentification(String modeOfIdentification) {
        this.modeOfIdentification = modeOfIdentification;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getOffShoreChargeDoneBy() {
        return offShoreChargeDoneBy;
    }

    public void setOffShoreChargeDoneBy(String offShoreChargeDoneBy) {
        this.offShoreChargeDoneBy = offShoreChargeDoneBy;
    }

    public BigDecimal getOffShoreCharges() {
        return offShoreCharges;
    }

    public void setOffShoreCharges(BigDecimal offShoreCharges) {
        this.offShoreCharges = offShoreCharges;
    }

    public String getSenderToReceiverInfo() {
        return senderToReceiverInfo;
    }

    public void setSenderToReceiverInfo(String senderToReceiverInfo) {
        this.senderToReceiverInfo = senderToReceiverInfo;
    }

    public String getRemittanceInfo() {
        return remittanceInfo;
    }

    public void setRemittanceInfo(String remittanceInfo) {
        this.remittanceInfo = remittanceInfo;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public Collection<FormaDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(Collection<FormaDocument> documents) {
        this.documents = documents;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public BigDecimal getDollarEquivalent() {
        return dollarEquivalent;
    }

    public void setDollarEquivalent(BigDecimal dollarEquivalent) {
        this.dollarEquivalent = dollarEquivalent;
    }

    public String getFormStatus() {
        return formStatus;
    }

    public void setFormStatus(String formStatus) {
        this.formStatus = formStatus;
    }

    public String getConcessionFlag() {
        return concessionFlag;
    }

    public void setConcessionFlag(String concessionFlag) {
        this.concessionFlag = concessionFlag;
    }

    public Double getConcessionPercentage() {
        return concessionPercentage;
    }

    public void setConcessionPercentage(Double concessionPercentage) {
        this.concessionPercentage = concessionPercentage;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getSubmitFlag() {
        return submitFlag;
    }

    public void setSubmitFlag(String submitFlag) {
        this.submitFlag = submitFlag;
    }

    public String getDocSubBranch() {
        return docSubBranch;
    }

    public void setDocSubBranch(String docSubBranch) {
        this.docSubBranch = docSubBranch;
    }

    public Date getDateOfAllocation() {
        return dateOfAllocation;
    }

    public void setDateOfAllocation(Date dateOfAllocation) {
        this.dateOfAllocation = dateOfAllocation;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getSenderToReceiverInfos() {
        return senderToReceiverInfos;
    }

    public void setSenderToReceiverInfos(String senderToReceiverInfos) {
        this.senderToReceiverInfos = senderToReceiverInfos;
    }

    public String getRemittanceInfos() {
        return remittanceInfos;
    }

    public void setRemittanceInfos(String remittanceInfos) {
        this.remittanceInfos = remittanceInfos;
    }
}
