package cronjobservice.services.implementation;

import cronjobservice.api.AccountInfo;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.AccountRepo;
import cronjobservice.repositories.CorporateRepo;
import cronjobservice.repositories.CorporateUserRepo;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Fortune on 4/5/2017.
 */
@Service
public class CorporateServiceImpl implements CorporateService {

    private CorporateRepo corporateRepo;
//    private CorpLimitRepo corpLimitRepo;
//    private CorpTransferRuleRepo corpTransferRuleRepo;
    @Autowired
    private CorporateUserRepo corporateUserRepo;
    @Autowired
    private AccountService accountService;
    @Autowired
    private AccountRepo accountRepo;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CodeService codeService;
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    private ConfigurationService configService;
//    @Autowired
//    private SecurityService securityService;
//
//    @Autowired
//    private PasswordPolicyService passwordPolicyService;
//
//    //@Autowired
//   // private RoleService roleService;
//    @Autowired
//    private RoleRepo roleRepo;
//
//    @Autowired
//    @Qualifier("bCryptPasswordEncoder")
//    private CustomerTradePassWordEncoder passwordEncoder;
//
    @Autowired
    private MailService mailService;
//
//    @Autowired
//    private CorporateRoleRepo corporateRoleRepo;


    @Autowired
    private EntityManager entityManager;

    private Locale locale = LocaleContextHolder.getLocale();
    private Logger logger = LoggerFactory.getLogger(this.getClass());


//    private void validateCorporateCreation(CorporateRequestDTO corporateRequestDTO) throws CronJobException {
//
//        if (corporateIdExists(corporateRequestDTO.getCorporateId())) {
//            throw new DuplicateObjectException(messageSource.getMessage("corp.id.exists", null, locale));
//        }
//
////        if (corporateRequestDTO.getAccounts().isEmpty()) {
////            logger.error("Corporate creation request found with no accounts");
////            throw new CronJobException(messageSource.getMessage("corporate.add.failure", null, locale));
////        }
//
//        if ("SOLE".equals(corporateRequestDTO.getCorporateType())) {
//
//            if (corporateRequestDTO.getCorporateUsers().size() != 1) {
//                logger.error("Sole Corporate creation request found with {} users", corporateRequestDTO.getCorporateUsers().size());
//                throw new CronJobException(messageSource.getMessage("corporate.add.failure", null, locale));
//
//            }
//
//        }
//
//        else if ("MULTI".equals(corporateRequestDTO.getCorporateType())) {
////            if (corporateRequestDTO.getAuthorizers().isEmpty()) {
////                logger.error("Corporate creation request found with no authorizer levels");
////                throw new CronJobException(messageSource.getMessage("corporate.add.failure", null, locale));
////
////            }
//
//            if (corporateRequestDTO.getCorporateUsers().isEmpty()) {
//                logger.error("Sole Corporate creation request found with no users");
//                throw new CronJobException(messageSource.getMessage("corporate.add.failure", null, locale));
//
//            }
//        }
//        else
//            {
//            logger.error("Corporate creation request found with invalid corporate type");
//            throw new CronJobException(messageSource.getMessage("corporate.add.failure", null, locale));
//           }
//    }
//
//
//    private  void validateCorporateUser(CorporateUserDTO user) {
//        CorporateUser corporateUser = corporateUserRepo.findFirstByUserNameIgnoreCase(user.getUserName());
//        if (corporateUser != null) {
//            throw new DuplicateObjectException(messageSource.getMessage("user.exists.id", null, locale));
//        }
//    }
//
//    private CorpUserType getUserType(String userType) {
//
//        if ("ADMIN".equals(userType)) {
//            return CorpUserType.ADMIN;
//        } else if ("INITIATOR".equals(userType)) {
//            return CorpUserType.INITIATOR;
//        } else if ("AUTHORIZER".equals(userType)) {
//            return CorpUserType.AUTHORIZER;
//        }
//        return null;
//    }
//
//
//    private Role getSoleCoporateRole() {
//        Role role = null;
//        SettingDTO setting = configService.getSettingByName("SOLE_CORPORATE_ROLE");
//        logger.info("getting the setting value {}",setting);
//        if (setting != null && setting.isEnabled()) {
//            String roleName = setting.getValue();
//            logger.info("getting the role name {}",roleName);
//            role = roleRepo.findByUserTypeAndName(UserType.CORPORATE, roleName);
//            logger.info("getting the role value {}",role);
//        }
//        return role;
//    }

    @Override
    @Transactional
    public void addAccounts(CorporateRequestDTO requestDTO){
        Corporate corporate = corporateRepo.findOne(requestDTO.getId());
        List<Account> newAccounts = accountService.addAccounts(requestDTO.getAccounts());
        List<Account> existingAccounts = corporate.getAccounts();
        existingAccounts.addAll(newAccounts);
        corporate.setAccounts(existingAccounts);
        corporateRepo.save(corporate);
    }

    public void addAccounts(Corporate corporate) {
        String customerId = corporate.getCustomerId();
        Corporate corp = corporateRepo.findFirstByCustomerId(customerId);
        if (corp != null) {
            Collection<AccountInfo> accounts = integrationService.fetchAccountsByCifId(customerId);
            for (AccountInfo acct : accounts) {
                accountService.AddFIAccount(customerId, acct);
            }
        }
    }


    @Override
    public String deleteCorporate(Long id) throws CronJobException {
        try {
            corporateRepo.delete(id);
            return messageSource.getMessage("corporate.delete.success", null, locale);
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("corporate.delete.failure", null, locale));

        }
    }

    @Async
    public void sendUserCredentials(CorporateUser user, String password) throws CronJobException {
        String fullName = user.getFirstName() + " " + user.getLastName();
        Corporate corporate = user.getCorporate();
        Email email = new Email.Builder()
                .setRecipient(user.getEmail())
                .setSubject(messageSource.getMessage("corporate.customer.create.subject", null, locale))
                .setBody(String.format(messageSource.getMessage("corporate.customer.create.message", null, locale), fullName, user.getUserName(), password, corporate.getCorporateId()))
                .build();
        new Thread(() -> {
            mailService.send(email);
        }).start();
    }

    @Override
//    @Verifiable(operation = "UPDATE_CORPORATE", description = "Updating Corporate Entity")
    public String updateCorporate(CorporateDTO corporateDTO) throws CronJobException {
        try {
            Corporate corporate = corporateRepo.findOne(corporateDTO.getId());
            entityManager.detach(corporate);
            corporate.setVersion(corporateDTO.getVersion());
            corporate.setEmail(corporateDTO.getEmail());
            corporate.setName(corporateDTO.getName());
            corporate.setAddress(corporateDTO.getAddress());
            corporateRepo.save(corporate);
            return messageSource.getMessage("corporate.update.success", null, locale);
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("corporate.update.failure", null, locale), e);

        }
    }

    @Override
    public CorporateDTO getCorporate(Long id) {
        Corporate corporate = corporateRepo.findOne(id);
        return convertEntityToDTO(corporate);
    }

    @Override
    public Corporate getCorp(Long id) {
        Corporate corporate = corporateRepo.findOne(id);
        return corporate;
    }

    @Override
    public Corporate getCorporateByCustomerId(String customerId) {
        System.out.println("entered the corp service ");
        Corporate corporate = corporateRepo.findByCustomerId(customerId);
        System.out.println("after the corp repo");
        return corporate;
    }

    @Override
    public List<CorporateDTO> getCorporates() {
        Iterable<Corporate> corporateDTOS = corporateRepo.findAll();
        return convertEntitiesToDTOs(corporateDTOS);
    }

    @Override
    public List<CorporateDTO> getListOfCorporates() {
        List<Corporate> corporateDTOS = corporateRepo.findAll();
        return convertEntitiesToDTOs(corporateDTOS);
    }


    @Override
    public String addAccount(Corporate corporate, AccountDTO accountDTO) throws CronJobException {

        try {
            boolean ok = accountService.AddAccount(corporate.getCustomerId(), accountDTO);
            if (ok) {
                return messageSource.getMessage("account.add.success", null, locale);
            } else {
                throw new CronJobException(messageSource.getMessage("corporate.account.add.failure", null, locale));
            }


        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("corporate.account.add.failure", null, locale), e);
        }
    }

    @Override

//
    public boolean corporateExists(String customerId) {
        Corporate corporate = corporateRepo.findByCustomerId(customerId);
        return corporate != null;
    }

//    @Override
//    public void setLimit(Corporate corporate, CorpLimit limit) throws CronJobException {
//        limit.setCorporate(corporate);
//        corpLimitRepo.save(limit);
//    }
//
//    @Override
//    public void updateLimit(Corporate corporate, CorpLimit limit) throws CronJobException {
//        limit.setCorporate(corporate);
//        corpLimitRepo.save(limit);
//    }
//
//    @Override
//    public List<CorpLimit> getLimit(Corporate corporate) {
//        return corpLimitRepo.findByCorporate(corporate);
//    }
//
//    @Override
//    public void deleteLimit(Long corporateId, CorpLimit limit) {
//        limit.setDelFlag("Y");
//        corpLimitRepo.save(limit);
//    }

    @Override
    public Page<CorporateDTO> getCorporates(Pageable pageDetails) {
        Page<Corporate> page = corporateRepo.findAll(pageDetails);
        List<CorporateDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();

        // return  new PageImpl<ServiceReqConfigDTO>(dtOs,pageDetails,page.getTotalElements());
        Page<CorporateDTO> pageImpl = new PageImpl<CorporateDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<AccountDTO> getAccounts(Long corpId, Pageable pageDetails) {
        Corporate corporate = corporateRepo.findOne(corpId);
        Page<AccountDTO> page = accountService.getAccounts(corporate.getCustomerId(), pageDetails);
        List<AccountDTO> dtOs = page.getContent();
        long t = page.getTotalElements();
        Page<AccountDTO> pageImpl = new PageImpl<AccountDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public List<Account> getAccounts(Long corpId) {
        Corporate corporate = corporateRepo.findOne(corpId);
        return corporate.getAccounts();
    }


//    @Override
//    @Verifiable(operation = "ADD_CORPORATE_RULE", description = "Add Corporate Transfer Rule")
//    public String addCorporateRule(CorpTransferRuleDTO transferRuleDTO) throws CronJobException {
//
//
//        if (new BigDecimal(transferRuleDTO.getLowerLimitAmount()).compareTo(new BigDecimal("0")) < 0) {
//            throw new TransferRuleException(messageSource.getMessage("rule.amount.zero", null, locale));
//        }
//        if (new BigDecimal(transferRuleDTO.getUpperLimitAmount()).compareTo(new BigDecimal(transferRuleDTO.getLowerLimitAmount())) < 0) {
//            throw new TransferRuleException(messageSource.getMessage("rule.range.violation", null, locale));
//        }
//
//        try {
//            CorpTransRule corpTransRule = convertTransferRuleDTOToEntity(transferRuleDTO);
//            corpTransferRuleRepo.save(corpTransRule);
//
//            logger.info("Added transfer rule for corporate with Id {}", transferRuleDTO.getCorporateId());
//            return messageSource.getMessage("rule.add.success", null, locale);
//        } catch (VerificationInterruptedException e) {
//            return e.getMessage();
//        } catch (Exception e) {
//            throw new CronJobException(messageSource.getMessage("rule.add.failure", null, locale), e);
//        }
//    }
//
//    @Override
//    public CorpTransferRuleDTO getCorporateRule(Long id) {
//        CorpTransRule transferRule = corpTransferRuleRepo.findOne(id);
//        return convertTransferRuleEntityToDTO(transferRule);
//    }



//    @Override
//    public boolean corporateIdExists(String corporateId) {
//        return corporateRepo.existsByCorporateIdIgnoreCase(corporateId);
//    }

//    @Override
//    @Transactional
//    public List<CorpTransferRuleDTO> getCorporateRules(Long corpId) {
//        Corporate corporate = corporateRepo.findOne(corpId);
//        List<CorpTransRule> transferRules = corporate.getCorpTransRules();
//        Collections.sort(transferRules, new TransferRuleComparator());
//        return convertTransferRuleEntitiesToDTOs(transferRules);
//
//
//    }



//    @Override
//    @Verifiable(operation = "DELETE_CORPORATE_RULE", description = "Delete Corporate Transfer Rule")
//    public String deleteCorporateRule(Long id) throws CronJobException {
//        try {
//            CorpTransRule transferRule = corpTransferRuleRepo.findOne(id);
//            corpTransferRuleRepo.delete(transferRule);
//            logger.info("Updated transfer rule  with Id {}", id);
//            return messageSource.getMessage("rule.delete.success", null, locale);
//        } catch (VerificationInterruptedException e) {
//            return e.getMessage();
//        } catch (CronJobException e) {
//            throw e;
//        } catch (Exception e) {
//            throw new CronJobException(messageSource.getMessage("rule.delete.failure", null, locale), e);
//        }
//    }





//    @Override
//    @Transactional
//    public List<CorporateRoleDTO> getRoles(Long corpId) {
//        Corporate corporate = corporateRepo.findOne(corpId);
//        List<CorporateRole> corporateRoles = corporateRoleRepo.findByCorporate(corporate);
////        sortRolesByRank(corporateRoles);
//        List<CorporateRoleDTO> roles = convertCorporateRoleEntitiesToDTOs(corporateRoles);
//        return roles;
//    }



    private CorporateRole convertCorporateRoleDTOToEntity(CorporateRoleDTO roleDTO) {
        CorporateRole corporateRole = new CorporateRole();
        corporateRole.setId(roleDTO.getId());
        corporateRole.setVersion(roleDTO.getVersion());
        corporateRole.setName(roleDTO.getName());
        corporateRole.setRank(roleDTO.getRank());
        corporateRole.setCorporate(corporateRepo.findOne(NumberUtils.toLong(roleDTO.getCorporateId())));
        Set<CorporateUserDTO> userDTOs = roleDTO.getUsers();
        Set<CorporateUser> users = new HashSet<CorporateUser>();
        for (CorporateUserDTO user : userDTOs) {
            CorporateUser corporateUser = new CorporateUser();
            corporateUser.setId(user.getId());
            users.add(corporateUser);
        }
        corporateRole.setUsers(users);
        return corporateRole;
    }

    private CorporateRoleDTO convertCorporateRoleEntityToDTO(CorporateRole role) {
        CorporateRoleDTO roleDTO = new CorporateRoleDTO();
        roleDTO.setId(role.getId());
        roleDTO.setVersion(role.getVersion());
        roleDTO.setName(role.getName());
        roleDTO.setRank(role.getRank());
        roleDTO.setCorporateId(role.getCorporate().getId().toString());
        Set<CorporateUserDTO> userDTOs = new HashSet<CorporateUserDTO>();
        for (CorporateUser user : role.getUsers()) {
            CorporateUserDTO userDTO = new CorporateUserDTO();
            userDTO.setId(user.getId());
            userDTO.setUserName(user.getUserName());
            userDTO.setFirstName(user.getFirstName());
            userDTO.setLastName(user.getLastName());
            userDTOs.add(userDTO);

        }
        roleDTO.setUsers(userDTOs);

        return roleDTO;
    }

    private Set<CorporateRoleDTO> convertCorporateRoleEntitiesToDTOs(Set<CorporateRole> roles) {
        Set<CorporateRoleDTO> roleDTOs = new HashSet<>();
        for (CorporateRole role : roles) {
            CorporateRoleDTO roleDTO = convertCorporateRoleEntityToDTO(role);
            roleDTOs.add(roleDTO);
        }
        return roleDTOs;
    }

    private List<CorporateRoleDTO> convertCorporateRoleEntitiesToDTOs(List<CorporateRole> roles) {
        List<CorporateRoleDTO> roleDTOs = new ArrayList<>();
        for (CorporateRole role : roles) {
            CorporateRoleDTO roleDTO = convertCorporateRoleEntityToDTO(role);
            roleDTOs.add(roleDTO);
        }
        return roleDTOs;
    }


//    private CorpTransRule convertTransferRuleDTOToEntity(CorpTransferRuleDTO transferRuleDTO) {
//        CorpTransRule corpTransRule = new CorpTransRule();
//        corpTransRule.setLowerLimitAmount(new BigDecimal(transferRuleDTO.getLowerLimitAmount()));
//        corpTransRule.setUpperLimitAmount(new BigDecimal(transferRuleDTO.getUpperLimitAmount()));
//        corpTransRule.setUnlimited(transferRuleDTO.isUnlimited());
//        corpTransRule.setCurrency(transferRuleDTO.getCurrency());
//        corpTransRule.setAnyCanAuthorize(transferRuleDTO.isAnyCanAuthorize());
//        corpTransRule.setRank(transferRuleDTO.isRank());
//        corpTransRule.setCorporate(corporateRepo.findOne(Long.parseLong(transferRuleDTO.getCorporateId())));
//
//        List<CorporateRole> roleList = new ArrayList<CorporateRole>();
//        for (CorporateRoleDTO roleDTO : transferRuleDTO.getRoles()) {
//            roleList.add(corporateRoleRepo.findOne((roleDTO.getId())));
//        }
//        corpTransRule.setRoles(roleList);
//        return corpTransRule;
//    }

//    private List<CorpTransferRuleDTO> convertTransferRuleEntitiesToDTOs(List<CorpTransRule> transferRules) {
//        List<CorpTransferRuleDTO> transferRuleDTOs = new ArrayList<CorpTransferRuleDTO>();
//        for (CorpTransRule transferRule : transferRules) {
//            CorpTransferRuleDTO transferRuleDTO = convertTransferRuleEntityToDTO(transferRule);
//            transferRuleDTOs.add(transferRuleDTO);
//        }
//
//        return transferRuleDTOs;
//    }

    private CorporateDTO convertEntityToDTO(Corporate corporate) {
        CorporateDTO corporateDTO = modelMapper.map(corporate, CorporateDTO.class);
        if (corporate.getCreatedOnDate() != null) {
            corporateDTO.setCreatedOnDate(DateFormatter.format(corporate.getCreatedOnDate()));
        }
        corporateDTO.setStatus(corporate.getStatus());
        return corporateDTO;
    }

    private Corporate convertDTOToEntity(CorporateDTO corporateDTO) {
        return modelMapper.map(corporateDTO, Corporate.class);
    }

    private List<CorporateDTO> convertEntitiesToDTOs(Iterable<Corporate> corporates) {
        List<CorporateDTO> corporateDTOList = new ArrayList<>();
        for (Corporate corporate : corporates) {
            CorporateDTO corporateDTO = convertEntityToDTO(corporate);
            Code code = codeService.getByTypeAndCode("CORPORATE_TYPE", corporate.getCorporateType());
            if (code != null) {
                corporateDTO.setCorporateType(code.getDescription());
            }
            corporateDTOList.add(corporateDTO);
        }
        return corporateDTOList;
    }

    @Override
    public Page<CorporateDTO> findCorporates(String pattern, Pageable pageDetails) {
        Page<Corporate> page = corporateRepo.findUsingPattern(pattern, pageDetails);
        List<CorporateDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();

        Page<CorporateDTO> pageImpl = new PageImpl<CorporateDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

//    @Override
//    public List<CorporateDTO> getByCustomerSegment(CodeDTO custSegment) {
//        Code code = modelMapper.map(custSegment, Code.class);
//        List<Corporate> corporates = corporateRepo.findByCustomerSegment(code);
//        return convertEntitiesToDTOs(corporates);
//    }


    private CorporateUser getCurrentUser() {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = principal.getUser();
        return (CorporateUser) user;
    }

//    @Override
//    @Verifiable(operation = "ADD_CORPORATE_ACCOUNT", description = "Adding Corporate Accounts")
//    public String addCorporateAccounts(CorporateRequestDTO requestDTO){
//
//        try {
//            addAccounts(requestDTO);
//            return messageSource.getMessage("corporate.account.add.success",null,locale);
//        }
//        catch (Exception e){
//            throw new CronJobException(messageSource.getMessage("corporate.account.add.failure",null,locale));
//        }
//    }

//    @Override
//    @Verifiable(operation = "DELETE_CORPORATE_ACCOUNT",description = "Delete Corporate Account")
//    public String deleteCorporateAccount(CorporateRequestDTO requestDTO) {
//
//        try {
//            deleteAccount(requestDTO);
//            return messageSource.getMessage("corporate.account.delete.success", null, locale);
//
//        }
//        catch (Exception e){
//            throw new CronJobException(messageSource.getMessage("corporate.account.delete.failure", null, locale));
//        }
//    }

    public void deleteAccount(CorporateRequestDTO requestDTO){

        Corporate corporate = corporateRepo.findOne(requestDTO.getId());
        List<Account> existingAccounts = corporate.getAccounts();
        for(AccountDTO accountDTO: requestDTO.getAccounts()){
            Account account = accountRepo.findOne(accountDTO.getId());
            existingAccounts.remove(account);
            accountRepo.delete(account);
        }
        corporate.setAccounts(existingAccounts);
        corporateRepo.save(corporate);
    }


}
