package cronjobservice.repositories;

import cronjobservice.models.FormNxpDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormNxpDocumentRepo extends JpaRepository<FormNxpDocument,Long> {
}
