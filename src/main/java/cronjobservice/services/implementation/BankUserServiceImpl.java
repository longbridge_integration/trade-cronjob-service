package cronjobservice.services.implementation;

import cronjobservice.dtos.BankUserDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.exception.DuplicateObjectException;
import cronjobservice.exception.VerificationInterruptedException;
import cronjobservice.models.*;
import cronjobservice.repositories.BankUserRepo;
import cronjobservice.repositories.BranchRepo;
import cronjobservice.repositories.RoleRepo;
import cronjobservice.services.BankUserService;
import cronjobservice.services.ConfigurationService;
import cronjobservice.services.MailService;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.ReflectionUtils;
import cronjobservice.utils.Verifiable;
import org.apache.commons.lang.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.*;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by chiomarose on 15/08/2017.
 */

@Service
public class BankUserServiceImpl implements BankUserService {

    private org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BankUserRepo bankUserRepo;

    @Autowired
    private BranchRepo branchRepo;

    @Autowired
    ModelMapper modelMapper;

    @Value("${host.url}")
    private String hostUrl;

    @Value("${AD.suffix}")
    private String adSuffix;

    @Autowired
    private MailService mailService;

//    @Autowired
//    private FailedLoginService failedLoginService;

    private Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    RoleRepo roleRepo;

    @Autowired
    private ConfigurationService configService;

    @Autowired
    EntityManager entityManager;

    @Autowired
    MessageSource messageSource;

//   @Autowired
//   @Qualifier("bankEncoder")
//    private BCryptPasswordEncoder passwordEncoder;
//
//    @Autowired
//    private PasswordPolicyService passwordPolicyService;

    public BankUserServiceImpl()
    {

    }


    @Override
    public BankUserDTO getBankUser(Long id)
    {
    BankUser bankUser=bankUserRepo.findOne(id);
    return convertEntitiesToDTo(bankUser);
    }

    @Override
    public boolean userExist(String userName) {
        return false;
    }

    @Override
    public BankUser getUserByName(String userName) {
        BankUser bankUser = this.bankUserRepo.findFirstByUserName(userName);

        return bankUser;

    }

    @Override
    public Iterable<BankUserDTO> getUsers() {
        return null;
    }

    @Override
    public String unlockUser(Long id) throws CronJobException {
        return null;
    }


    @Override
    public Page<BankUserDTO> getUsers(Pageable pageableDetails)
    {
        Page<BankUser> page=bankUserRepo.findAll(pageableDetails);
        List<BankUserDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t=page.getTotalElements();
        Page<BankUserDTO> pageimpl=new PageImpl<BankUserDTO>(dtOs,pageableDetails,t);
        return pageimpl;
    }

    @Override
    public Page<BankUserDTO> getUsersByBranchSortCode(String sortCode, Pageable pageDetails) {
        Branch branch = branchRepo.findBySortCode(sortCode);
        Page<BankUser> page=bankUserRepo.findByBranch(branch, pageDetails);
        List<BankUserDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t=page.getTotalElements();
        Page<BankUserDTO> pageimpl=new PageImpl<BankUserDTO>(dtOs,pageDetails,t);
        return pageimpl;
    }

    @Override
    public Page<BankUserDTO> findUsers(BankUserDTO example, Pageable pageDetails) {
        ExampleMatcher matcher = ExampleMatcher.matchingAll().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase().withIgnorePaths("version", "noOfAttempts").withIgnoreNullValues();
        BankUser entity = convertDTOToEntity(example);
        logger.info("BANK_USER {}", entity);
        ReflectionUtils.nullifyStrings(entity, 1);
        Page<BankUser> page = bankUserRepo.findAll(Example.of(entity, matcher), pageDetails);
        List<BankUserDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<BankUserDTO> pageImpl = new PageImpl<BankUserDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<BankUserDTO> findUsers(String pattern, Pageable pageDetails) {
        Page<BankUser> page = bankUserRepo.findUsingPattern(pattern, pageDetails);
        List<BankUserDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();

        Page<BankUserDTO> pageImpl = new PageImpl<BankUserDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    private BankUser convertDTOToEntity(BankUserDTO bankUserDTO) {
        BankUser bankUser =  modelMapper.map(bankUserDTO, BankUser.class);
        return bankUser;
    }

    private List<BankUserDTO> convertEntitiesToDTOs(Iterable<BankUser> bankUsers)
    {
        List<BankUserDTO> bankUserDTOList=new ArrayList<>();

        for(BankUser bankUser:bankUsers)
        {
            BankUserDTO bankUserDTO=convertEntitiesToDTo(bankUser);

            bankUserDTOList.add(bankUserDTO);

        }

        return  bankUserDTOList;
    }


    private BankUserDTO convertEntitiesToDTo(BankUser bankUser)
    {
        BankUserDTO bankUserDTO=modelMapper.map(bankUser,BankUserDTO.class);

        bankUserDTO.setRole(bankUser.getRole().getName());
        bankUserDTO.setRoleId(bankUser.getRole().getId().toString());

        if (bankUser.getBranch()!=null){
            bankUserDTO.setBranchId(bankUser.getBranch().getId().toString());
            bankUserDTO.setBranchSortCode(bankUser.getBranch().getSortCode());
            bankUserDTO.setBranchName(bankUser.getBranch().getName());
        }

        if(bankUser.getCreatedOnDate()!=null)
        {
            bankUserDTO.setCreatedOn(DateFormatter.format(bankUser.getCreatedOnDate()));
        }

        if(bankUser.getLastLoginDate()!=null)
        {
            bankUserDTO.setLastLogin(DateFormatter.format(bankUser.getLastLoginDate()));
        }

          return bankUserDTO;
    }



    @Override
    @Transactional
    @Verifiable(operation = "ADD_BRANCH_USER" ,description="adding a banks user")
    public String addBranchUsers(BankUserDTO user) throws CronJobException {

        BankUser bankUser=bankUserRepo.findFirstByUserNameIgnoreCase(user.getUserName());

        if(bankUser!=null)
        {
            throw new DuplicateObjectException(messageSource.getMessage("user.exist",null,locale));
        }

        if (configService.getSettingByName("ENABLE_BANK_ADA").isEnabled()) {
            if (!StringUtils.endsWith(user.getUserName(), adSuffix)) {
                throw new CronJobException(messageSource.getMessage("invalid.ad.username", null, locale));
            }
            if (!StringUtils.endsWith(user.getEmail(), adSuffix)) {
                throw new CronJobException(messageSource.getMessage("invalid.ad.email", null, locale));
            }
            if (!user.getEmail().equals(user.getUserName())) {
                throw new CronJobException(messageSource.getMessage("invalid.username.email", null, locale));
            }
        }

        try {
            bankUser = new BankUser();
            bankUser.setFirstName(user.getFirstName());
            bankUser.setLastName(user.getLastName());
            bankUser.setUserName(user.getUserName());
            bankUser.setEmail(user.getEmail());
            bankUser.setPhoneNumber(user.getPhoneNumber());
            bankUser.setCreatedOnDate(new Date());

            Role role = roleRepo.findOne(Long.parseLong(user.getRoleId()));
            Branch branch=branchRepo.findOne(Long.parseLong(user.getBranchId()));
            logger.info("getting role{}",role);
            bankUser.setRole(role);
            bankUser.setBranch(branch);
            bankUserRepo.save(bankUser);
            return messageSource.getMessage("user.add.success",null, LocaleContextHolder.getLocale());
        }
        catch (VerificationInterruptedException e)
        {
            return e.getMessage();
        }
        catch (CronJobException se)
        {
            throw new CronJobException(messageSource.getMessage("entrust.create.failure", null, locale), se);
        }


    }


    @Override
    public String updateUser(BankUserDTO user) throws CronJobException {
        BankUser bankUser=bankUserRepo.findOne(user.getId());

        if ("I".equals(bankUser.getStatus()))
        {
            throw new CronJobException(messageSource.getMessage("user.deactivated", null, locale));
        }

        if (configService.getSettingByName("ENABLE_BANK_ADA").isEnabled()) {
            if (!StringUtils.endsWith(user.getUserName(), adSuffix)) {
                throw new CronJobException(messageSource.getMessage("invalid.ad.username", null, locale));
            }
            if (!StringUtils.endsWith(user.getEmail(), adSuffix)) {
                throw new CronJobException(messageSource.getMessage("invalid.ad.email", null, locale));
            }
            if (!user.getEmail().equals(user.getUserName())) {
                throw new CronJobException(messageSource.getMessage("invalid.username.email", null, locale));
            }
        }

        try {
            entityManager.detach(bankUser);
            bankUser.setVersion(user.getVersion());
            bankUser.setFirstName(user.getFirstName());
            bankUser.setLastName(user.getLastName());
            bankUser.setUserName(user.getUserName());
            bankUser.setEmail(user.getEmail());
            bankUser.setPhoneNumber(user.getPhoneNumber());
            Role role = roleRepo.findOne(Long.parseLong(user.getRoleId()));
            bankUser.setRole(role);
            Branch branch=branchRepo.findOne(Long.parseLong(user.getBranchId()));
            bankUser.setBranch(branch);
            bankUserRepo.save(bankUser);

            logger.info("Bank user {} updated", bankUser.getUserName());
            return messageSource.getMessage("user.update.success", null, locale);
        } catch (VerificationInterruptedException e) {
            return e.getMessage();
        } catch (CronJobException ibe) {
            throw ibe;
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("user.update.failure", null, locale), e);
        }
    }

    @Override
    public String changeActivationStatus(Long userId) throws CronJobException {
        return null;
    }

    @Override
    public void sendActivationCredentials(BankUser user, String password) {

    }


//    @Async
//    private void sendActivateMessage(User user, String... args)
//    {
//        BankUser bankUser = getUserByName(user.getUserName());
//        logger.info("getting the activation status {}",bankUser);
//        if ("A".equals(bankUser.getStatus())) {
//            Email email = new Email.Builder()
//                    .setRecipient(user.getEmail())
//                    .setSubject(messageSource.getMessage("bnk.activation.subject", null, locale))
//                    .setBody(String.format(messageSource.getMessage("bnk.activation.message", null, locale), args))
//                    .build();
//            mailService.send(email);
//        }
//    }




    @Override
    public String deleteUser(Long userId) throws CronJobException {
        return null;
    }

    @Override
    public String resetPassword(String userName) throws CronJobException {
        return null;
    }

    @Override
    public String resetPassword(Long id) throws CronJobException {
        return null;
    }


}
