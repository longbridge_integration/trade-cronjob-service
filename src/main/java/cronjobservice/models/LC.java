package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance
@DiscriminatorColumn(name="type")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class LC extends Process{

    private String lcNumber;
    private String tempLcNumber;
    private String customerName;
    //pass the form M number to Finacle as formMnumber
    @ManyToOne
    private FormM formM;
    @ManyToOne
    @JoinColumn(name = "lc_chrg_acct")
    private Account chargeAccount;
    @ManyToOne
    @JoinColumn(name = "lc_trn_acct")
    private Account transAccount;

    @ManyToOne
    private Code lcType;
    private BigDecimal lcAmount;
    private BigDecimal partCleanline;
    private BigDecimal partConfirmed;
    private BigDecimal partUnconfirmed;
    private String lcCurrency;
    private String exchangeRate;
    private String dealId;
    private Date  dateOfAllocation;
    private String dateOfAllocations;

    //Amount utilized form m amount in form m table
    private BigDecimal utilizedFormMAmount;

    private String advBankName;
    private String advBankBranch;
    private String advBankAddress;
    private String advSwiftCode;
    private String advBankCode;

    private String advThrBankName;
    private String advThrBankBranch;
    private String advThrBankAddress;
    private String advThrSwiftCode;
    private String advThrBankCode;

    //TEXT INFO
    @Lob
    private String bankInstructions;
    @Lob
    private String docInstruction;
    @Lob
    private String conditions;
    @Lob
    private String sendRecInfo;
    @Lob
    private String periodOfPresentation;//textInfo
    @Lob
    private String chargeDescription;//textInfo

    private String countryOfExpiry;
    private String cityOfExpiry;
    private String reimbursingBankName;
    private String reimbursingBankAcct;//bank
    private String reimbursingBankBranch;
    private String reimbursingBankAddress;
    private String reimbursingSwiftCode;
    private String reimbursingBankIdent;
    private String chargesBorneBy;//dropdown A-Applicant, B-Beneficiary


    private String availWithBankName;
    private String availWithBankBranch;
    private String availWithBankAddress;
    private String availWithSwiftCode;
    private String availWithBankCode;
    private String availWithBankIdent;

    private String insurCompName;
    private String insurCompAddr;
    private String insurPolicyNo;

    private String insurPolicyDates;
    private String insurPayableAt;
    private String insurDateOfExpirys;

    private String insurPercentage;
    private BigDecimal insuredAmount;
    private BigDecimal premiumAmount;
    private String insuredBy;

    @ManyToOne
    private Code lcPaymentTenor;

    private String lcPaymentTenorCode;
    private String usancePeriod;
    private String beneficiaryName;
    private String beneficiaryAddress;
    private String beneficiaryTown;
    private String beneficiaryAccount;
    private String beneficiaryCountry;
    private String beneficiaryState;

    //benebank or applicantBank
    private String beneficiaryBankName;
    private String beneficiaryBankBranch;
    private String beneficiaryBankAddress;
    private String beneficiarySwiftCode;


    private String lcValidCountry;
    private String beneficiaryCharges;
//    private String corrNostroAcct;

    private String portOfLoading;
    private String placeOfFinalDest;
    private String portOfDischarge;
    private String shipmentPeriod;//freetext - cust
    private String shipmentTerms;
    private String shipmentMode;//Code - cust
    private String originOfGoods;

    private String tolerancePos;
    private String toleranceNeg;
    private String bidRateCode;

    private BigDecimal lcAvailableAmount;
    private BigDecimal formMAvailableAmount;
    private String lastShipment;

    private String lastShipmentDates;
    private int numberOfAmendments;
    private int numberOfReinstatement;

    private UserType userType;
    private String InitiatedBy;

    //fields to delete


    //New fields
    private String tranId;
    private String sortCode; //fetch the branchid of the approving bank
    private String applicableRules;//codes
    private String applicableSubRule;//if "others" was chosen- bank
    private String tenorDetails;//textInfo - bank

    private String corrNostroAccount;
    private String applicantName;
    private String applicantAddress;
    private String applicantCity;
    private String applicantState;
    private String applicantCountry;

    //Amount allocated from the formm allocation list
    private BigDecimal allocatedAmount;

    //Amount used from the lc table
    private BigDecimal utilizedAmount;

    private String draweeBankName;
    private String draweeBankAddr;
    private String draweeBankSwiftCode;
    private String draweeBankCtry;
    private String draweeBankState;
    private String draweeBank;
    private String draweeBankCity;
    private String draweeBankBr;
    private String draweeBankPoCode;
    private String draweeBankIdent;//code dropdown remittance

    private String instruction;
    private String instructionBy;//dropdown A-Applicant, B-Beneficiary, C-Correspondent Bank, N-Collecting Bank, O-Others, S-Self


    private String issuingBank;
    private String issuingBankBrnch;
    private String issuingBankSwift;
    private String issuingBankCity;
    private String issuingBankState;
    private String issuingBankCode;
    private String issuingBankAddr;
    private String issuingBankCountry;

    private BigDecimal outstanding;

    private String paysysId;

    private String confirmBy;//dropdown
    @OneToOne
    @JoinColumn(name = "lc_conf_inst")
    private Code confirmInstruct;
    private String availableBy;
    private String availableWith;
    private Long formmAllocationId;
    private String negeriodDay;
    private String negPeriodMonth;
    private String foreignBankRefNum;
    private String backTobackLcNo;
    private String docCreditType;
    private String negotiationPeriod;
    private Boolean guidelineApproval;
    private boolean formDetRequired;
    private Boolean utilWithoutBill;
    private Boolean utilbyBill;
    private Boolean transferable;
    private Boolean revolving;
    private Boolean revokable;
    private Boolean backToBack;
    private Boolean partShipment;
    private Boolean redLetter;
    private Boolean tranShipment;
    private Boolean deferred;
    private Boolean standBy;
    private Boolean irrevokable;
    private Boolean reimbursementMessage;
    private Boolean reimbursementAppRules;//dropdown NOTURR, URR Latest Version
    private String reimbursementAppRule;
    private String approver;
    private Date dateCreated;
    private Date instructionDate;
    private String instructionDates;
    private Date dateOfExpiry;
    private String lcDateOfExpiry;
    @ManyToOne
    private Branch docSubBranch;

    private String operativeAccount;
    private String chargeAcct;

    private String toBeLinked;
//    private String rcNumber;
//
//    public String getRcNumber() {
//        return rcNumber;
//    }
//
//    public void setRcNumber(String rcNumber) {
//        this.rcNumber = rcNumber;
//    }

    public String getToBeLinked() {
        return toBeLinked;
    }

    public void setToBeLinked(String toBeLinked) {
        this.toBeLinked = toBeLinked;
    }

    public Date getDateOfAllocation() {
        return dateOfAllocation;
    }

    public String getChargeAcct() {
        return chargeAcct;
    }

    public void setChargeAcct(String chargeAcct) {
        this.chargeAcct = chargeAcct;
    }

    public BigDecimal getPartCleanline() {
        return partCleanline;
    }

    public void setPartCleanline(BigDecimal partCleanline) {
        this.partCleanline = partCleanline;
    }

    public BigDecimal getPartConfirmed() {
        return partConfirmed;
    }

    public void setPartConfirmed(BigDecimal partConfirmed) {
        this.partConfirmed = partConfirmed;
    }

    public BigDecimal getPartUnconfirmed() {
        return partUnconfirmed;
    }

    public void setPartUnconfirmed(BigDecimal partUnconfirmed) {
        this.partUnconfirmed = partUnconfirmed;
    }

    public String getNegotiationPeriod() {
        return negotiationPeriod;
    }

    public void setNegotiationPeriod(String negotiationPeriod) {
        this.negotiationPeriod = negotiationPeriod;
    }

    public Boolean getIrrevokable() {
        return irrevokable;
    }

    public void setIrrevokable(Boolean irrevokable) {
        this.irrevokable = irrevokable;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getInstructionDates() {
        return instructionDates;
    }

    public void setInstructionDates(String instructionDates) {
        this.instructionDates = instructionDates;
    }

    public String getDocCreditType() {
        return docCreditType;
    }

    public void setDocCreditType(String docCreditType) {
        this.docCreditType = docCreditType;
    }


    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public BigDecimal getUtilizedFormMAmount() {
        return utilizedFormMAmount;
    }

    public void setUtilizedFormMAmount(BigDecimal utilizedFormMAmount) {
        this.utilizedFormMAmount = utilizedFormMAmount;
    }

    public String getBackTobackLcNo() {
        return backTobackLcNo;
    }

    public void setBackTobackLcNo(String backTobackLcNo) {
        this.backTobackLcNo = backTobackLcNo;
    }

    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public Boolean getReimbursementMessage() {
        return reimbursementMessage;
    }

    public void setReimbursementMessage(Boolean reimbursementMessage) {
        this.reimbursementMessage = reimbursementMessage;
    }

    public Boolean getStandBy() {
        return standBy;
    }

    public void setStandBy(Boolean standBy) {
        this.standBy = standBy;
    }


    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }


    public String getIssuingBankAddr() {
        return issuingBankAddr;
    }

    public void setIssuingBankAddr(String issuingBankAddr) {
        this.issuingBankAddr = issuingBankAddr;
    }

    public String getDraweeBank() {
        return draweeBank;
    }

    public String getIssuingBankCode() {
        return issuingBankCode;
    }

    public void setIssuingBankCode(String issuingBankCode) {
        this.issuingBankCode = issuingBankCode;
    }

    public void setDraweeBank(String draweeBank) {
        this.draweeBank = draweeBank;
    }

    public String getDraweeBankBr() {
        return draweeBankBr;
    }

    public void setDraweeBankBr(String draweeBankBr) {
        this.draweeBankBr = draweeBankBr;
    }

    public String getAdvThrBankCode() {
        return advThrBankCode;
    }


    public String getBeneficiaryTown() {
        return beneficiaryTown;
    }

    public void setBeneficiaryTown(String beneficiaryTown) {
        this.beneficiaryTown = beneficiaryTown;
    }

    public String getBeneficiaryAccount() {
        return beneficiaryAccount;
    }

    public void setBeneficiaryAccount(String beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
    }

    public String getBeneficiaryState() {
        return beneficiaryState;
    }

    public void setBeneficiaryState(String beneficiaryState) {
        this.beneficiaryState = beneficiaryState;
    }

    public String getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    public void setBeneficiaryCountry(String beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
    }

    public void setAdvThrBankCode(String advThrBankCode) {
        this.advThrBankCode = advThrBankCode;
    }

    public String getNegPeriodMonth() {
        return negPeriodMonth;
    }

    public void setNegPeriodMonth(String negPeriodMonth) {
        this.negPeriodMonth = negPeriodMonth;
    }

    public String getNegeriodDay() {
        return negeriodDay;
    }

    public void setNegeriodDay(String negeriodDay) {
        this.negeriodDay = negeriodDay;
    }

    public String getAdvBankCode() {
        return advBankCode;
    }

    public void setAdvBankCode(String advBankCode) {
        this.advBankCode = advBankCode;
    }

    public String getAvailWithBankCode() {
        return availWithBankCode;
    }

    public void setAvailWithBankCode(String availWithBankCode) {
        this.availWithBankCode = availWithBankCode;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public String getIssuingBank() {
        return issuingBank;
    }

    public void setIssuingBank(String issuingBank) {
        this.issuingBank = issuingBank;
    }

    public String getIssuingBankBrnch() {
        return issuingBankBrnch;
    }

    public void setIssuingBankBrnch(String issuingBankBrnch) {
        this.issuingBankBrnch = issuingBankBrnch;
    }

    public String getIssuingBankSwift() {
        return issuingBankSwift;
    }

    public void setIssuingBankSwift(String issuingBankSwift) {
        this.issuingBankSwift = issuingBankSwift;
    }

    public String getIssuingBankCity() {
        return issuingBankCity;
    }

    public void setIssuingBankCity(String issuingBankCity) {
        this.issuingBankCity = issuingBankCity;
    }

    public String getIssuingBankState() {
        return issuingBankState;
    }

    public void setIssuingBankState(String issuingBankState) {
        this.issuingBankState = issuingBankState;
    }

    public String getIssuingBankCountry() {
        return issuingBankCountry;
    }

    public void setIssuingBankCountry(String issuingBankCountry) {
        this.issuingBankCountry = issuingBankCountry;
    }

    public String getLcCurrency() {
        return lcCurrency;
    }

    public void setLcCurrency(String lcCurrency) {
        this.lcCurrency = lcCurrency;
    }

    public BigDecimal getLcAmount() {
        return lcAmount;
    }

    public void setLcAmount(BigDecimal lcAmount) {
        this.lcAmount = lcAmount;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public Boolean getTransferable() {
        return transferable;
    }

    public void setTransferable(Boolean transferable) {
        this.transferable = transferable;
    }

    public Boolean getRevolving() {
        return revolving;
    }

    public void setRevolving(Boolean revolving) {
        this.revolving = revolving;
    }

    public Boolean getRevokable() {
        return revokable;
    }

    public void setRevokable(Boolean revokable) {
        this.revokable = revokable;
    }

    public Boolean getBackToBack() {
        return backToBack;
    }

    public void setBackToBack(Boolean backToBack) {
        this.backToBack = backToBack;
    }



    public Boolean getPartShipment() {
        return partShipment;
    }

    public void setPartShipment(Boolean partShipment) {
        this.partShipment = partShipment;
    }

    public Boolean getRedLetter() {
        return redLetter;
    }

    public void setRedLetter(Boolean redLetter) {
        this.redLetter = redLetter;
    }

    public Boolean getTranShipment() {
        return tranShipment;
    }

    public void setTranShipment(Boolean tranShipment) {
        this.tranShipment = tranShipment;
    }

    public Boolean getDeferred() {
        return deferred;
    }

    public void setDeferred(Boolean deferred) {
        this.deferred = deferred;
    }

    public Date getDateOfExpiry() {
        return dateOfExpiry;
    }

    public void setDateOfExpiry(Date dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }

    public String getCountryOfExpiry() {
        return countryOfExpiry;
    }

    public void setCountryOfExpiry(String countryOfExpiry) {
        this.countryOfExpiry = countryOfExpiry;
    }

    public String getCityOfExpiry() {
        return cityOfExpiry;
    }

    public void setCityOfExpiry(String cityOfExpiry) {
        this.cityOfExpiry = cityOfExpiry;
    }

    public Code getLcPaymentTenor() {
        return lcPaymentTenor;
    }

    public void setLcPaymentTenor(Code lcPaymentTenor) {
        this.lcPaymentTenor = lcPaymentTenor;
    }

    public String getLcValidCountry() {
        return lcValidCountry;
    }

    public void setLcValidCountry(String lcValidCountry) {
        this.lcValidCountry = lcValidCountry;
    }

    public String getBeneficiaryCharges() {
        return beneficiaryCharges;
    }

    public void setBeneficiaryCharges(String beneficiaryCharges) {
        this.beneficiaryCharges = beneficiaryCharges;
    }

    public String getPortOfLoading() {
        return portOfLoading;
    }

    public void setPortOfLoading(String portOfLoading) {
        this.portOfLoading = portOfLoading;
    }



    public String getOriginOfGoods() {
        return originOfGoods;
    }

    public void setOriginOfGoods(String originOfGoods) {
        this.originOfGoods = originOfGoods;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getAvailableWith() {
        return availableWith;
    }

    public void setAvailableWith(String availableWith) {
        this.availableWith = availableWith;
    }

    public String getAvailableBy() {
        return availableBy;
    }

    public void setAvailableBy(String availableBy) {
        this.availableBy = availableBy;
    }

    public String getTolerancePos() {
        return tolerancePos;
    }

    public void setTolerancePos(String tolerancePos) {
        this.tolerancePos = tolerancePos;
    }

    public String getToleranceNeg() {
        return toleranceNeg;
    }

    public void setToleranceNeg(String toleranceNeg) {
        this.toleranceNeg = toleranceNeg;
    }

    public String getUsancePeriod() {
        return usancePeriod;
    }

    public void setUsancePeriod(String usancePeriod) {
        this.usancePeriod = usancePeriod;
    }


    public BigDecimal getLcAvailableAmount() {
        return lcAvailableAmount;
    }

    public void setLcAvailableAmount(BigDecimal lcAvailableAmount) {
        this.lcAvailableAmount = lcAvailableAmount;
    }

    public BigDecimal getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(BigDecimal outstanding) {
        this.outstanding = outstanding;
    }



    public int getNumberOfAmendments() {
        return numberOfAmendments;
    }

    public void setNumberOfAmendments(int numberOfAmendments) {
        this.numberOfAmendments = numberOfAmendments;
    }

    public int getNumberOfReinstatement() {
        return numberOfReinstatement;
    }

    public void setNumberOfReinstatement(int numberOfReinstatement) {
        this.numberOfReinstatement = numberOfReinstatement;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getInitiatedBy() {
        return InitiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        InitiatedBy = initiatedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public FormM getFormM() {
        return formM;
    }

    public void setFormM(FormM formM) {
        this.formM = formM;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getLcNumber() {
        return lcNumber;
    }

    public void setLcNumber(String lcNumber) {
        this.lcNumber = lcNumber;
    }

    public Boolean getGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(Boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public Account getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(Account chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public Account getTransAccount() {
        return transAccount;
    }

    public void setTransAccount(Account transAccount) {
        this.transAccount = transAccount;
    }

    public Code getLcType() {
        return lcType;
    }

    public void setLcType(Code lcType) {
        this.lcType = lcType;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryAddress() {
        return beneficiaryAddress;
    }

    public void setBeneficiaryAddress(String beneficiaryAddress) {
        this.beneficiaryAddress = beneficiaryAddress;
    }

    public String getBeneficiaryBankName() {
        return beneficiaryBankName;
    }

    public void setBeneficiaryBankName(String beneficiaryBankName) {
        this.beneficiaryBankName = beneficiaryBankName;
    }

    public String getBeneficiaryBankBranch() {
        return beneficiaryBankBranch;
    }

    public void setBeneficiaryBankBranch(String beneficiaryBankBranch) {
        this.beneficiaryBankBranch = beneficiaryBankBranch;
    }

    public String getBeneficiaryBankAddress() {
        return beneficiaryBankAddress;
    }

    public void setBeneficiaryBankAddress(String beneficiaryBankAddress) {
        this.beneficiaryBankAddress = beneficiaryBankAddress;
    }

    public String getBeneficiarySwiftCode() {
        return beneficiarySwiftCode;
    }

    public void setBeneficiarySwiftCode(String beneficiarySwiftCode) {
        this.beneficiarySwiftCode = beneficiarySwiftCode;
    }


    public String getShipmentTerms() {
        return shipmentTerms;
    }

    public void setShipmentTerms(String shipmentTerms) {
        this.shipmentTerms = shipmentTerms;
    }

    public String getReimbursingBankName() {
        return reimbursingBankName;
    }

    public void setReimbursingBankName(String reimbursingBankName) {
        this.reimbursingBankName = reimbursingBankName;
    }

    public String getReimbursingBankBranch() {
        return reimbursingBankBranch;
    }

    public void setReimbursingBankBranch(String reimbursingBankBranch) {
        this.reimbursingBankBranch = reimbursingBankBranch;
    }

    public String getReimbursingBankAddress() {
        return reimbursingBankAddress;
    }

    public void setReimbursingBankAddress(String reimbursingBankAddress) {
        this.reimbursingBankAddress = reimbursingBankAddress;
    }

    public String getReimbursingSwiftCode() {
        return reimbursingSwiftCode;
    }

    public void setReimbursingSwiftCode(String reimbursingSwiftCode) {
        this.reimbursingSwiftCode = reimbursingSwiftCode;
    }

    public BigDecimal getInsuredAmount() {
        return insuredAmount;
    }

    public void setInsuredAmount(BigDecimal insuredAmount) {
        this.insuredAmount = insuredAmount;
    }

    public BigDecimal getPremiumAmount() {
        return premiumAmount;
    }

    public void setPremiumAmount(BigDecimal premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    public String getInsuredBy() {
        return insuredBy;
    }

    public void setInsuredBy(String insuredBy) {
        this.insuredBy = insuredBy;
    }

    public String getApplicantAddress() {
        return applicantAddress;
    }

    public void setApplicantAddress(String applicantAddress) {
        this.applicantAddress = applicantAddress;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getApplicantCity() {
        return applicantCity;
    }

    public void setApplicantCity(String applicantCity) {
        this.applicantCity = applicantCity;
    }

    public String getApplicantState() {
        return applicantState;
    }

    public void setApplicantState(String applicantState) {
        this.applicantState = applicantState;
    }

    public String getApplicantCountry() {
        return applicantCountry;
    }

    public void setApplicantCountry(String applicantCountry) {
        this.applicantCountry = applicantCountry;
    }

    public BigDecimal getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(BigDecimal allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public BigDecimal getUtilizedAmount() {
        return utilizedAmount;
    }

    public void setUtilizedAmount(BigDecimal utilizedAmount) {
        this.utilizedAmount = utilizedAmount;
    }

    public String getPaysysId() {
        return paysysId;
    }

    public void setPaysysId(String paysysId) {
        this.paysysId = paysysId;
    }

    public String getApplicableRules() {
        return applicableRules;
    }

    public void setApplicableRules(String applicableRules) {
        this.applicableRules = applicableRules;
    }

    public String getPeriodOfPresentation() {
        return periodOfPresentation;
    }

    public void setPeriodOfPresentation(String periodOfPresentation) {
        this.periodOfPresentation = periodOfPresentation;
    }

    public String getChargeDescription() {
        return chargeDescription;
    }

    public void setChargeDescription(String chargeDescription) {
        this.chargeDescription = chargeDescription;
    }

    public String getAdvBankName() {
        return advBankName;
    }

    public void setAdvBankName(String advBankName) {
        this.advBankName = advBankName;
    }

    public String getAdvBankBranch() {
        return advBankBranch;
    }

    public void setAdvBankBranch(String advBankBranch) {
        this.advBankBranch = advBankBranch;
    }

    public String getAdvBankAddress() {
        return advBankAddress;
    }

    public void setAdvBankAddress(String advBankAddress) {
        this.advBankAddress = advBankAddress;
    }

    public String getAdvSwiftCode() {
        return advSwiftCode;
    }

    public void setAdvSwiftCode(String advSwiftCode) {
        this.advSwiftCode = advSwiftCode;
    }

    public String getAdvThrBankName() {
        return advThrBankName;
    }

    public void setAdvThrBankName(String advThrBankName) {
        this.advThrBankName = advThrBankName;
    }

    public String getAdvThrBankBranch() {
        return advThrBankBranch;
    }

    public void setAdvThrBankBranch(String advThrBankBranch) {
        this.advThrBankBranch = advThrBankBranch;
    }

    public String getAdvThrBankAddress() {
        return advThrBankAddress;
    }

    public void setAdvThrBankAddress(String advThrBankAddress) {
        this.advThrBankAddress = advThrBankAddress;
    }

    public String getAdvThrSwiftCode() {
        return advThrSwiftCode;
    }

    public void setAdvThrSwiftCode(String advThrSwiftCode) {
        this.advThrSwiftCode = advThrSwiftCode;
    }

    public String getDocInstruction() {
        return docInstruction;
    }

    public void setDocInstruction(String docInstruction) {
        this.docInstruction = docInstruction;
    }

    public String getSendRecInfo() {
        return sendRecInfo;
    }

    public void setSendRecInfo(String sendRecInfo) {
        this.sendRecInfo = sendRecInfo;
    }

    public String getForeignBankRefNum() {
        return foreignBankRefNum;
    }

    public void setForeignBankRefNum(String foreignBankRefNum) {
        this.foreignBankRefNum = foreignBankRefNum;
    }

    public String getPlaceOfFinalDest() {
        return placeOfFinalDest;
    }

    public void setPlaceOfFinalDest(String placeOfFinalDest) {
        this.placeOfFinalDest = placeOfFinalDest;
    }

    public Code getConfirmInstruct() {
        return confirmInstruct;
    }

    public void setConfirmInstruct(Code confirmInstruct) {
        this.confirmInstruct = confirmInstruct;
    }

    public String getAvailWithBankName() {
        return availWithBankName;
    }

    public void setAvailWithBankName(String availWithBankName) {
        this.availWithBankName = availWithBankName;
    }

    public String getAvailWithBankBranch() {
        return availWithBankBranch;
    }

    public void setAvailWithBankBranch(String availWithBankBranch) {
        this.availWithBankBranch = availWithBankBranch;
    }

    public String getAvailWithBankAddress() {
        return availWithBankAddress;
    }

    public void setAvailWithBankAddress(String availWithBankAddress) {
        this.availWithBankAddress = availWithBankAddress;
    }

    public String getAvailWithSwiftCode() {
        return availWithSwiftCode;
    }

    public void setAvailWithSwiftCode(String availWithSwiftCode) {
        this.availWithSwiftCode = availWithSwiftCode;
    }

    public Boolean getUtilbyBill() {
        return utilbyBill;
    }

    public void setUtilbyBill(Boolean utilbyBill) {
        this.utilbyBill = utilbyBill;
    }

    public Boolean getUtilWithoutBill() {
        return utilWithoutBill;
    }

    public void setUtilWithoutBill(Boolean utilWithoutBill) {
        this.utilWithoutBill = utilWithoutBill;
    }

    public String getInsurCompName() {
        return insurCompName;
    }

    public void setInsurCompName(String insurCompName) {
        this.insurCompName = insurCompName;
    }

    public String getInsurCompAddr() {
        return insurCompAddr;
    }

    public void setInsurCompAddr(String insurCompAddr) {
        this.insurCompAddr = insurCompAddr;
    }

    public String getInsurPolicyNo() {
        return insurPolicyNo;
    }

    public void setInsurPolicyNo(String insurPolicyNo) {
        this.insurPolicyNo = insurPolicyNo;
    }



    public String getInsurPayableAt() {
        return insurPayableAt;
    }

    public void setInsurPayableAt(String insurPayableAt) {
        this.insurPayableAt = insurPayableAt;
    }

    public String getInsurPolicyDates() {
        return insurPolicyDates;
    }

    public void setInsurPolicyDates(String insurPolicyDates) {
        this.insurPolicyDates = insurPolicyDates;
    }

    public String getLastShipmentDates() {
        return lastShipmentDates;
    }


    public void setLastShipmentDates(String lastShipmentDates) {
        this.lastShipmentDates = lastShipmentDates;
    }

    public String getInsurDateOfExpirys() {
        return insurDateOfExpirys;
    }

    public void setInsurDateOfExpirys(String insurDateOfExpirys) {
        this.insurDateOfExpirys = insurDateOfExpirys;
    }


    public String getInsurPercentage() {
        return insurPercentage;
    }

    public void setInsurPercentage(String insurPercentage) {
        this.insurPercentage = insurPercentage;
    }

    public String getCorrNostroAccount() {
        return corrNostroAccount;
    }

    public void setCorrNostroAccount(String corrNostroAccount) {
        this.corrNostroAccount = corrNostroAccount;
    }


    public boolean isFormDetRequired() {
        return formDetRequired;
    }

    public void setFormDetRequired(boolean formDetRequired) {
        this.formDetRequired = formDetRequired;
    }

    public String getDraweeBankName() {
        return draweeBankName;
    }

    public void setDraweeBankName(String draweeBankName) {
        this.draweeBankName = draweeBankName;
    }

    public String getDraweeBankAddr() {
        return draweeBankAddr;
    }

    public void setDraweeBankAddr(String draweeBankAddr) {
        this.draweeBankAddr = draweeBankAddr;
    }

    public String getDraweeBankSwiftCode() {
        return draweeBankSwiftCode;
    }

    public void setDraweeBankSwiftCode(String draweeBankSwiftCode) {
        this.draweeBankSwiftCode = draweeBankSwiftCode;
    }

    public String getDraweeBankCtry() {
        return draweeBankCtry;
    }

    public void setDraweeBankCtry(String draweeBankCtry) {
        this.draweeBankCtry = draweeBankCtry;
    }

    public String getDraweeBankState() {
        return draweeBankState;
    }

    public void setDraweeBankState(String draweeBankState) {
        this.draweeBankState = draweeBankState;
    }

    public String getDraweeBankCity() {
        return draweeBankCity;
    }

    public void setDraweeBankCity(String draweeBankCity) {
        this.draweeBankCity = draweeBankCity;
    }

    public String getDraweeBankPoCode() {
        return draweeBankPoCode;
    }

    public void setDraweeBankPoCode(String draweeBankPoCode) {
        this.draweeBankPoCode = draweeBankPoCode;
    }

    public String getDraweeBankIdent() {
        return draweeBankIdent;
    }

    public void setDraweeBankIdent(String draweeBankIdent) {
        this.draweeBankIdent = draweeBankIdent;
    }

    public String getTempLcNumber() {
        return tempLcNumber;
    }

    public void setTempLcNumber(String tempLcNumber) {
        this.tempLcNumber = tempLcNumber;
    }


    public String getBankInstructions() {
        return bankInstructions;
    }

    public void setBankInstructions(String bankInstructions) {
        this.bankInstructions = bankInstructions;
    }

    public Boolean getReimbursementAppRules() {
        return reimbursementAppRules;
    }

    public void setReimbursementAppRules(Boolean reimbursementAppRules) {
        this.reimbursementAppRules = reimbursementAppRules;
    }

    public String getReimbursingBankAcct() {
        return reimbursingBankAcct;
    }

    public void setReimbursingBankAcct(String reimbursingBankAcct) {
        this.reimbursingBankAcct = reimbursingBankAcct;
    }

    public String getReimbursingBankIdent() {
        return reimbursingBankIdent;
    }

    public void setReimbursingBankIdent(String reimbursingBankIdent) {
        this.reimbursingBankIdent = reimbursingBankIdent;
    }

    public String getChargesBorneBy() {
        return chargesBorneBy;
    }

    public void setChargesBorneBy(String chargesBorneBy) {
        this.chargesBorneBy = chargesBorneBy;
    }

    public String getAvailWithBankIdent() {
        return availWithBankIdent;
    }

    public void setAvailWithBankIdent(String availWithBankIdent) {
        this.availWithBankIdent = availWithBankIdent;
    }

    public String getShipmentPeriod() {
        return shipmentPeriod;
    }

    public void setShipmentPeriod(String shipmentPeriod) {
        this.shipmentPeriod = shipmentPeriod;
    }

    public String getShipmentMode() {
        return shipmentMode;
    }

    public void setShipmentMode(String shipmentMode) {
        this.shipmentMode = shipmentMode;
    }

    public String getConfirmBy() {
        return confirmBy;
    }

    public void setConfirmBy(String confirmBy) {
        this.confirmBy = confirmBy;
    }

    public String getBidRateCode() {
        return bidRateCode;
    }

    public void setBidRateCode(String bidRateCode) {
        this.bidRateCode = bidRateCode;
    }

    public String getLastShipment() {
        return lastShipment;
    }

    public void setLastShipment(String lastShipment) {
        this.lastShipment = lastShipment;
    }

    public String getApplicableSubRule() {
        return applicableSubRule;
    }

    public void setApplicableSubRule(String applicableSubRule) {
        this.applicableSubRule = applicableSubRule;
    }

    public String getTenorDetails() {
        return tenorDetails;
    }

    public void setTenorDetails(String tenorDetails) {
        this.tenorDetails = tenorDetails;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getInstructionBy() {
        return instructionBy;
    }

    public void setInstructionBy(String instructionBy) {
        this.instructionBy = instructionBy;
    }

    public Date getInstructionDate() {
        return instructionDate;
    }

    public void setInstructionDate(Date instructionDate) {
        this.instructionDate = instructionDate;
    }

    public Branch getDocSubBranch() {
        return docSubBranch;
    }

    public String getOperativeAccount() {
        return operativeAccount;
    }

    public void setOperativeAccount(String operativeAccount) {
        this.operativeAccount = operativeAccount;
    }

    public void setDocSubBranch(Branch docSubBranch) {
        this.docSubBranch = docSubBranch; }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setDateOfAllocation(Date dateOfAllocation) {
        this.dateOfAllocation = dateOfAllocation;
    }

    public String getDateOfAllocations() {
        return dateOfAllocations;
    }

    public void setDateOfAllocations(String dateOfAllocations) {
        this.dateOfAllocations = dateOfAllocations;
    }

    public String getLcPaymentTenorCode() {
        return lcPaymentTenorCode;
    }

    public void setLcPaymentTenorCode(String lcPaymentTenorCode) {
        this.lcPaymentTenorCode = lcPaymentTenorCode;
    }

    public BigDecimal getFormMAvailableAmount() {
        return formMAvailableAmount;
    }

    public void setFormMAvailableAmount(BigDecimal formMAvailableAmount) {
        this.formMAvailableAmount = formMAvailableAmount;
    }

    public Long getFormmAllocationId() {
        return formmAllocationId;
    }

    public void setFormmAllocationId(Long formmAllocationId) {
        this.formmAllocationId = formmAllocationId;
    }

    public String getReimbursementAppRule() {
        return reimbursementAppRule;
    }

    public void setReimbursementAppRule(String reimbursementAppRule) {
        this.reimbursementAppRule = reimbursementAppRule;
    }

    public String getLcDateOfExpiry() {
        return lcDateOfExpiry;
    }

    public void setLcDateOfExpiry(String lcDateOfExpiry) {
        this.lcDateOfExpiry = lcDateOfExpiry;
    }
}
