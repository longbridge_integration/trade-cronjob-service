package cronjobservice.utils;

import cronjobservice.config.CustomJdbcUtil;
import cronjobservice.config.GenerateFormNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

/**
 * Created by chiomarose on 28/09/2017.
 */
public class FormNumber
{

    private Logger log= LoggerFactory.getLogger(this.getClass());
    static Logger logger = LoggerFactory.getLogger(CustomJdbcUtil.class);
    public static String getFormNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("forma");
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String getRemittanceFormNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("Remittance");
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String getFormMNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("formm");
        logger.info("id form m {]",id);
         long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String getLcNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("lc");
        logger.info("id form m {]",id);
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String getFormQNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("FormQ");
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }


    public static String getFormNxpNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("form_nxp");
        logger.info("id {]",id);
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }



    public static String getExportLcNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("lc");
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String getInwGuranteeNumner()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("inward_guarantee");
        logger.info("id guarantee {]",id);
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }


    public static String getImportLcNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("lc");
        logger.info("id guarantee {]",id);
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String getOutwGuranteeNumner()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("outward_guarantee");
        logger.info("id guarantee {]",id);
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String getImportBillUnderLc()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("import_bill");
        logger.info("id import bill {]",id);
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String getImportBillNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("import_bill");
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String getPaarNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("paar");
        logger.info("id paar{}",id);
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }


    public static String generateBillNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("bills");
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

    public static String generateBillForCollectionNumber()
    {
        GenerateFormNumber formNumber=new GenerateFormNumber();
        long id=formNumber.getFormNumber("export_bill_for_collection");
        long currentFormNumber=id + 1;
        String formater=String.format("%07d",currentFormNumber);
        long year= Calendar.getInstance().get(Calendar.YEAR);
        long lastTwoDigit= year%2000;
        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
        return SerialFormNumber;
    }

//    public static String generateImportBillNumber()
//    {
//        GenerateFormNumber formNumber=new GenerateFormNumber();
//        long id=formNumber.getFormNumber("import_bill");
//        long currentFormNumber=id + 1;
//        String formater=String.format("%07d",currentFormNumber);
//        long year= Calendar.getInstance().get(Calendar.YEAR);
//        long lastTwoDigit= year%2000;
//        String  SerialFormNumber=Long.toString(lastTwoDigit)+formater;
//        return SerialFormNumber;
//    }


}
