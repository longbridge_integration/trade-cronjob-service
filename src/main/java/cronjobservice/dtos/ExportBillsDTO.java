package cronjobservice.dtos;

import cronjobservice.models.Branch;
import cronjobservice.models.UserType;
import cronjobservice.models.WorkFlow;

import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class ExportBillsDTO {

    private long id;
    private String customerName;
    private String submitAction;
    private String lCid;
    private String status;
    //    private String amount;
    private String importerAddress;
    private String importerTown;
    private String importerState;
    private String importerCountry;
    private BigDecimal billAmount;
    private String billCurrency;
    private String billCountry;
    private String invoiceNumber;
    private String invoiceAmount;

    private String coAcceptanceBank;
    private String billTenor;
    private String coMandatory;
    private String fixedTransitPeriod;
    private String tenorPeriod;

    private String discount;
    private String cifid;
    private int version;
    private long workFlowId;
    private long exportLcId;
    private BigDecimal otherBankCharges;
    private double billExchangeRate;
    private double usuancePercentage;
    private BigDecimal usuanceAmount;
    private String insuranceAmountCurrency;
    private String usuanceBy;
    private String policyNumber;

    private String usuanceCompany;
    private String payableAt;
    private BigDecimal premimumAmount;

    private String vesselName;

    private String otherBankRefNo;
    private WorkFlow workFlow;
    private String importerName;
    private String comments;
    private String portOfDischarge;

    //Date NOTE THAT
    private String onBoardOn;
    private String billOn;
    private String acceptanceOn;
    private String dueOn;
    private String policyOn;
    private String documentOn;
    private String invoiceOn;
    private  String statusDesc;
    private Date dateCreated;
    private String createdOn;


    private BigDecimal discountValue;
    private String descripantStatus;
    private String descripantComment;

    private String discrepantMsg;
    private String dicountValCurrency;
    private BigDecimal proposedDiscountVal;
    private String discountStatus;
    private String discountStatusDesc;
    private List<BillsDocumentDTO> documents;
    private String billNumber;
    private String temporalBllNumb;
    private BigDecimal advanceBillAmt;
    private BigDecimal discountAdvanceVal;

    private String advanceBillMessage;
    private String branch;
    private boolean guidelineApproval;
    private BigDecimal freightAmount;
    private String freightAmtCurrency;
    //exportlc details
    private BigDecimal lcutilized;
    private BigDecimal lcavailableAmt;
    private ExportLcDTO exportLcDTO;
    private String file;

    private BigDecimal utilizedAmount;
    private BigDecimal amtAvailableForDisAdv;
    private String operativeAcct;
    private String chargeAcct;

    private UserType userType;
    private String invoiceAmtCurrency;
    private String gracePeriod;
    private String dueDateIndicator;
    private String premiumAmtCurrency;

    private String corrBnkSwiftCode;
    private String corrBnkName;
    private String corrBankCountry;
    private String corrBankState;
    private String corrBankCity;
    private String corrBankIdentifier;
    private String corrBankAddress;
    private String nostroAcctNo;

    private String coLLBnkSwiftCode;
    private String coLLBnkName;
    private String coLLBankCountry;
    private String coLLBankState;
    private String coLLBankCity;
    private String coLLBankIdentifier;
    private String coLLBankAddress;

    private String draweeBnkAddress;
    private String draweeBnkSwift;
    private String draweeBnkName;
    private String draweeBnkCountry;
    private String draweeBnkState;
    private String draweeBnkCity;
    private String draweeBnkIdent;

    //BENEFICIARY BANK NAME
    private String beneBankName;
    private String beneBankAddr;
    private String beneBankSwiftCode;
    private String beneBankCtry;
    private String beneBankCity;
    private String beneBankState;
    private String beneBankIdent;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSubmitAction() {
        return submitAction;
    }

    public void setSubmitAction(String submitAction) {
        this.submitAction = submitAction;
    }

    public String getlCid() {
        return lCid;
    }

    public void setlCid(String lCid) {
        this.lCid = lCid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImporterAddress() {
        return importerAddress;
    }

    public void setImporterAddress(String importerAddress) {
        this.importerAddress = importerAddress;
    }

    public String getImporterTown() {
        return importerTown;
    }

    public void setImporterTown(String importerTown) {
        this.importerTown = importerTown;
    }

    public String getImporterState() {
        return importerState;
    }

    public void setImporterState(String importerState) {
        this.importerState = importerState;
    }

    public String getImporterCountry() {
        return importerCountry;
    }

    public void setImporterCountry(String importerCountry) {
        this.importerCountry = importerCountry;
    }

    public BigDecimal getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }

    public String getBillCurrency() {
        return billCurrency;
    }

    public void setBillCurrency(String billCurrency) {
        this.billCurrency = billCurrency;
    }

    public String getBillCountry() {
        return billCountry;
    }

    public void setBillCountry(String billCountry) {
        this.billCountry = billCountry;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getCoAcceptanceBank() {
        return coAcceptanceBank;
    }

    public void setCoAcceptanceBank(String coAcceptanceBank) {
        this.coAcceptanceBank = coAcceptanceBank;
    }

    public String getBillTenor() {
        return billTenor;
    }

    public void setBillTenor(String billTenor) {
        this.billTenor = billTenor;
    }

    public String getCoMandatory() {
        return coMandatory;
    }

    public void setCoMandatory(String coMandatory) {
        this.coMandatory = coMandatory;
    }

    public String getFixedTransitPeriod() {
        return fixedTransitPeriod;
    }

    public void setFixedTransitPeriod(String fixedTransitPeriod) {
        this.fixedTransitPeriod = fixedTransitPeriod;
    }

    public String getTenorPeriod() {
        return tenorPeriod;
    }

    public void setTenorPeriod(String tenorPeriod) {
        this.tenorPeriod = tenorPeriod;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public long getWorkFlowId() {
        return workFlowId;
    }

    public void setWorkFlowId(long workFlowId) {
        this.workFlowId = workFlowId;
    }

    public long getExportLcId() {
        return exportLcId;
    }

    public void setExportLcId(long exportLcId) {
        this.exportLcId = exportLcId;
    }

    public BigDecimal getOtherBankCharges() {
        return otherBankCharges;
    }

    public void setOtherBankCharges(BigDecimal otherBankCharges) {
        this.otherBankCharges = otherBankCharges;
    }

    public double getBillExchangeRate() {
        return billExchangeRate;
    }

    public void setBillExchangeRate(double billExchangeRate) {
        this.billExchangeRate = billExchangeRate;
    }

    public double getUsuancePercentage() {
        return usuancePercentage;
    }

    public void setUsuancePercentage(double usuancePercentage) {
        this.usuancePercentage = usuancePercentage;
    }

    public BigDecimal getUsuanceAmount() {
        return usuanceAmount;
    }

    public void setUsuanceAmount(BigDecimal usuanceAmount) {
        this.usuanceAmount = usuanceAmount;
    }

    public String getInsuranceAmountCurrency() {
        return insuranceAmountCurrency;
    }

    public void setInsuranceAmountCurrency(String insuranceAmountCurrency) {
        this.insuranceAmountCurrency = insuranceAmountCurrency;
    }

    public String getUsuanceBy() {
        return usuanceBy;
    }

    public void setUsuanceBy(String usuanceBy) {
        this.usuanceBy = usuanceBy;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getUsuanceCompany() {
        return usuanceCompany;
    }

    public void setUsuanceCompany(String usuanceCompany) {
        this.usuanceCompany = usuanceCompany;
    }

    public String getPayableAt() {
        return payableAt;
    }

    public void setPayableAt(String payableAt) {
        this.payableAt = payableAt;
    }

    public BigDecimal getPremimumAmount() {
        return premimumAmount;
    }

    public void setPremimumAmount(BigDecimal premimumAmount) {
        this.premimumAmount = premimumAmount;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getOtherBankRefNo() {
        return otherBankRefNo;
    }

    public void setOtherBankRefNo(String otherBankRefNo) {
        this.otherBankRefNo = otherBankRefNo;
    }

    public WorkFlow getWorkFlow() {
        return workFlow;
    }

    public void setWorkFlow(WorkFlow workFlow) {
        this.workFlow = workFlow;
    }

    public String getImporterName() {
        return importerName;
    }

    public void setImporterName(String importerName) {
        this.importerName = importerName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }

    public String getOnBoardOn() {
        return onBoardOn;
    }

    public void setOnBoardOn(String onBoardOn) {
        this.onBoardOn = onBoardOn;
    }

    public String getBillOn() {
        return billOn;
    }

    public void setBillOn(String billOn) {
        this.billOn = billOn;
    }

    public String getAcceptanceOn() {
        return acceptanceOn;
    }

    public void setAcceptanceOn(String acceptanceOn) {
        this.acceptanceOn = acceptanceOn;
    }

    public String getDueOn() {
        return dueOn;
    }

    public void setDueOn(String dueOn) {
        this.dueOn = dueOn;
    }

    public String getPolicyOn() {
        return policyOn;
    }

    public void setPolicyOn(String policyOn) {
        this.policyOn = policyOn;
    }

    public String getDocumentOn() {
        return documentOn;
    }

    public void setDocumentOn(String documentOn) {
        this.documentOn = documentOn;
    }

    public String getInvoiceOn() {
        return invoiceOn;
    }

    public void setInvoiceOn(String invoiceOn) {
        this.invoiceOn = invoiceOn;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(BigDecimal discountValue) {
        this.discountValue = discountValue;
    }

    public String getDescripantStatus() {
        return descripantStatus;
    }

    public void setDescripantStatus(String descripantStatus) {
        this.descripantStatus = descripantStatus;
    }

    public String getDescripantComment() {
        return descripantComment;
    }

    public void setDescripantComment(String descripantComment) {
        this.descripantComment = descripantComment;
    }

    public String getDiscrepantMsg() {
        return discrepantMsg;
    }

    public void setDiscrepantMsg(String discrepantMsg) {
        this.discrepantMsg = discrepantMsg;
    }

    public String getDicountValCurrency() {
        return dicountValCurrency;
    }

    public void setDicountValCurrency(String dicountValCurrency) {
        this.dicountValCurrency = dicountValCurrency;
    }

    public BigDecimal getProposedDiscountVal() {
        return proposedDiscountVal;
    }

    public void setProposedDiscountVal(BigDecimal proposedDiscountVal) {
        this.proposedDiscountVal = proposedDiscountVal;
    }

    public String getDiscountStatus() {
        return discountStatus;
    }

    public void setDiscountStatus(String discountStatus) {
        this.discountStatus = discountStatus;
    }

    public String getDiscountStatusDesc() {
        return discountStatusDesc;
    }

    public void setDiscountStatusDesc(String discountStatusDesc) {
        this.discountStatusDesc = discountStatusDesc;
    }

    public List<BillsDocumentDTO> getDocuments() {
        return documents;
    }

    public void setDocuments(List<BillsDocumentDTO> documents) {
        this.documents = documents;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getTemporalBllNumb() {
        return temporalBllNumb;
    }

    public void setTemporalBllNumb(String temporalBllNumb) {
        this.temporalBllNumb = temporalBllNumb;
    }

    public BigDecimal getAdvanceBillAmt() {
        return advanceBillAmt;
    }

    public void setAdvanceBillAmt(BigDecimal advanceBillAmt) {
        this.advanceBillAmt = advanceBillAmt;
    }

    public BigDecimal getDiscountAdvanceVal() {
        return discountAdvanceVal;
    }

    public void setDiscountAdvanceVal(BigDecimal discountAdvanceVal) {
        this.discountAdvanceVal = discountAdvanceVal;
    }

    public String getAdvanceBillMessage() {
        return advanceBillMessage;
    }

    public void setAdvanceBillMessage(String advanceBillMessage) {
        this.advanceBillMessage = advanceBillMessage;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public BigDecimal getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(BigDecimal freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getFreightAmtCurrency() {
        return freightAmtCurrency;
    }

    public void setFreightAmtCurrency(String freightAmtCurrency) {
        this.freightAmtCurrency = freightAmtCurrency;
    }

    public BigDecimal getLcutilized() {
        return lcutilized;
    }

    public void setLcutilized(BigDecimal lcutilized) {
        this.lcutilized = lcutilized;
    }

    public BigDecimal getLcavailableAmt() {
        return lcavailableAmt;
    }

    public void setLcavailableAmt(BigDecimal lcavailableAmt) {
        this.lcavailableAmt = lcavailableAmt;
    }

    public ExportLcDTO getExportLcDTO() {
        return exportLcDTO;
    }

    public void setExportLcDTO(ExportLcDTO exportLcDTO) {
        this.exportLcDTO = exportLcDTO;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public BigDecimal getUtilizedAmount() {
        return utilizedAmount;
    }

    public void setUtilizedAmount(BigDecimal utilizedAmount) {
        this.utilizedAmount = utilizedAmount;
    }

    public BigDecimal getAmtAvailableForDisAdv() {
        return amtAvailableForDisAdv;
    }

    public void setAmtAvailableForDisAdv(BigDecimal amtAvailableForDisAdv) {
        this.amtAvailableForDisAdv = amtAvailableForDisAdv;
    }

    public String getOperativeAcct() {
        return operativeAcct;
    }

    public void setOperativeAcct(String operativeAcct) {
        this.operativeAcct = operativeAcct;
    }

    public String getChargeAcct() {
        return chargeAcct;
    }

    public void setChargeAcct(String chargeAcct) {
        this.chargeAcct = chargeAcct;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getInvoiceAmtCurrency() {
        return invoiceAmtCurrency;
    }

    public void setInvoiceAmtCurrency(String invoiceAmtCurrency) {
        this.invoiceAmtCurrency = invoiceAmtCurrency;
    }

    public String getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(String gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public String getDueDateIndicator() {
        return dueDateIndicator;
    }

    public void setDueDateIndicator(String dueDateIndicator) {
        this.dueDateIndicator = dueDateIndicator;
    }

    public String getPremiumAmtCurrency() {
        return premiumAmtCurrency;
    }

    public void setPremiumAmtCurrency(String premiumAmtCurrency) {
        this.premiumAmtCurrency = premiumAmtCurrency;
    }

    public String getCorrBnkSwiftCode() {
        return corrBnkSwiftCode;
    }

    public void setCorrBnkSwiftCode(String corrBnkSwiftCode) {
        this.corrBnkSwiftCode = corrBnkSwiftCode;
    }

    public String getCorrBnkName() {
        return corrBnkName;
    }

    public void setCorrBnkName(String corrBnkName) {
        this.corrBnkName = corrBnkName;
    }

    public String getCorrBankCountry() {
        return corrBankCountry;
    }

    public void setCorrBankCountry(String corrBankCountry) {
        this.corrBankCountry = corrBankCountry;
    }

    public String getCorrBankState() {
        return corrBankState;
    }

    public void setCorrBankState(String corrBankState) {
        this.corrBankState = corrBankState;
    }

    public String getCorrBankCity() {
        return corrBankCity;
    }

    public void setCorrBankCity(String corrBankCity) {
        this.corrBankCity = corrBankCity;
    }

    public String getCorrBankIdentifier() {
        return corrBankIdentifier;
    }

    public void setCorrBankIdentifier(String corrBankIdentifier) {
        this.corrBankIdentifier = corrBankIdentifier;
    }

    public String getCorrBankAddress() {
        return corrBankAddress;
    }

    public void setCorrBankAddress(String corrBankAddress) {
        this.corrBankAddress = corrBankAddress;
    }

    public String getNostroAcctNo() {
        return nostroAcctNo;
    }

    public void setNostroAcctNo(String nostroAcctNo) {
        this.nostroAcctNo = nostroAcctNo;
    }

    public String getCoLLBnkSwiftCode() {
        return coLLBnkSwiftCode;
    }

    public void setCoLLBnkSwiftCode(String coLLBnkSwiftCode) {
        this.coLLBnkSwiftCode = coLLBnkSwiftCode;
    }

    public String getCoLLBnkName() {
        return coLLBnkName;
    }

    public void setCoLLBnkName(String coLLBnkName) {
        this.coLLBnkName = coLLBnkName;
    }

    public String getCoLLBankCountry() {
        return coLLBankCountry;
    }

    public void setCoLLBankCountry(String coLLBankCountry) {
        this.coLLBankCountry = coLLBankCountry;
    }

    public String getCoLLBankState() {
        return coLLBankState;
    }

    public void setCoLLBankState(String coLLBankState) {
        this.coLLBankState = coLLBankState;
    }

    public String getCoLLBankCity() {
        return coLLBankCity;
    }

    public void setCoLLBankCity(String coLLBankCity) {
        this.coLLBankCity = coLLBankCity;
    }

    public String getCoLLBankIdentifier() {
        return coLLBankIdentifier;
    }

    public void setCoLLBankIdentifier(String coLLBankIdentifier) {
        this.coLLBankIdentifier = coLLBankIdentifier;
    }

    public String getCoLLBankAddress() {
        return coLLBankAddress;
    }

    public void setCoLLBankAddress(String coLLBankAddress) {
        this.coLLBankAddress = coLLBankAddress;
    }

    public String getDraweeBnkAddress() {
        return draweeBnkAddress;
    }

    public void setDraweeBnkAddress(String draweeBnkAddress) {
        this.draweeBnkAddress = draweeBnkAddress;
    }

    public String getDraweeBnkSwift() {
        return draweeBnkSwift;
    }

    public void setDraweeBnkSwift(String draweeBnkSwift) {
        this.draweeBnkSwift = draweeBnkSwift;
    }

    public String getDraweeBnkName() {
        return draweeBnkName;
    }

    public void setDraweeBnkName(String draweeBnkName) {
        this.draweeBnkName = draweeBnkName;
    }

    public String getDraweeBnkCountry() {
        return draweeBnkCountry;
    }

    public void setDraweeBnkCountry(String draweeBnkCountry) {
        this.draweeBnkCountry = draweeBnkCountry;
    }

    public String getDraweeBnkState() {
        return draweeBnkState;
    }

    public void setDraweeBnkState(String draweeBnkState) {
        this.draweeBnkState = draweeBnkState;
    }

    public String getDraweeBnkCity() {
        return draweeBnkCity;
    }

    public void setDraweeBnkCity(String draweeBnkCity) {
        this.draweeBnkCity = draweeBnkCity;
    }

    public String getDraweeBnkIdent() {
        return draweeBnkIdent;
    }

    public void setDraweeBnkIdent(String draweeBnkIdent) {
        this.draweeBnkIdent = draweeBnkIdent;
    }

    public String getBeneBankName() {
        return beneBankName;
    }

    public void setBeneBankName(String beneBankName) {
        this.beneBankName = beneBankName;
    }

    public String getBeneBankAddr() {
        return beneBankAddr;
    }

    public void setBeneBankAddr(String beneBankAddr) {
        this.beneBankAddr = beneBankAddr;
    }

    public String getBeneBankSwiftCode() {
        return beneBankSwiftCode;
    }

    public void setBeneBankSwiftCode(String beneBankSwiftCode) {
        this.beneBankSwiftCode = beneBankSwiftCode;
    }

    public String getBeneBankCtry() {
        return beneBankCtry;
    }

    public void setBeneBankCtry(String beneBankCtry) {
        this.beneBankCtry = beneBankCtry;
    }

    public String getBeneBankCity() {
        return beneBankCity;
    }

    public void setBeneBankCity(String beneBankCity) {
        this.beneBankCity = beneBankCity;
    }

    public String getBeneBankState() {
        return beneBankState;
    }

    public void setBeneBankState(String beneBankState) {
        this.beneBankState = beneBankState;
    }

    public String getBeneBankIdent() {
        return beneBankIdent;
    }

    public void setBeneBankIdent(String beneBankIdent) {
        this.beneBankIdent = beneBankIdent;
    }


}
