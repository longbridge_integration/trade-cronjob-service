package cronjobservice.repositories;

import cronjobservice.models.BTA;
import cronjobservice.models.FormNxp;
import cronjobservice.models.PTA;
import cronjobservice.models.Paar;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PtaRepo extends CommonRepo<PTA,Long>{
    List<PTA> findByStatusAndSubmitFlag(String S, String SF);
    PTA findByRefNumber(String refNumber);
}
