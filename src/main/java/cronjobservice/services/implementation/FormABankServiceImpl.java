
package cronjobservice.services.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cronjobservice.api.FormAFI;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.FormNumber;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by chiomarose on 21/09/2017.
 */
@Service
public class FormABankServiceImpl implements FormABankService {


    private Logger logger= LoggerFactory.getLogger(this.getClass());

    private FormARepo formARepo;
    private MessageSource messageSource;
    private WorkFlowRepo workFlowRepo;
    private ConfigurationService configurationService;
//    private UserGroupService userGroupService;
    private RetailUserService retailUserService;
    private CorporateService corporateService;
    @Autowired
    private RetailUserRepo retailUserRepo;
    @Autowired
    private CorporateRepo corporateRepo;
    @Autowired
    private BranchRepo branchRepo;
    @Autowired
    private CodeRepo codeRepo;
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    CodeService codeService;
    private Locale locale= LocaleContextHolder.getLocale();
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    EntityManager entityManager;
//    @Autowired
//    CommentService commentService;
    @Autowired
    AccountService accountService;
    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    FormaDocumentRepo formaDocumentRepo;
    @Autowired
    BankUserService bankUserService;
    @Autowired
    FormACorporateService formACorporateService;

    @Value("${forma.upload.files}")
    private String UPLOADED_FOLDER;


    @Autowired
    public FormABankServiceImpl(FormARepo formARepo, MessageSource messageSource, WorkFlowRepo workFlowRepo, ConfigurationService configurationService, RetailUserService retailUserService, CorporateService corporateService) {
        this.formARepo = formARepo;
        this.messageSource = messageSource;
        this.workFlowRepo = workFlowRepo;
        this.configurationService = configurationService;
//        this.userGroupService = userGroupService;
        this.retailUserService = retailUserService;
        this.corporateService = corporateService;
    }

    @Override
    public FormADTO editFormA(Long id) throws CronJobException
    {
        FormA formA=formARepo.findOne(id);
        return convertEntityTODTO(formA);


    }


    @Override
    public List<AccountDTO>  getCustomerNairaAccounts(String cifid) throws CronJobException
    {

            List<AccountDTO> nairaAccountList=new ArrayList<>();
            nairaAccountList=accountService.getCustomerNairaAccounts(cifid,"NGN");
            return nairaAccountList;

    }


    public List<AccountDTO> getCustomerDomAccounts(String cifid) throws CronJobException
    {
        List<AccountDTO> domaccountList=new ArrayList<>();


            domaccountList=accountService.getCustomerDomAccounts(cifid,"NGN");
        return  domaccountList;
    }

    @Override
    public FormADTO getInitiatorDetails() throws CronJobException
    {

        FormADTO formADTO = new FormADTO();
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();

        SettingDTO wfSetting = configurationService.getSettingByName("EXCHANGE_RATE");
        SettingDTO interSwiftCode=configurationService.getSettingByName("INTERMEDIARY_SWIFT_CODE");
        SettingDTO interBeneCode=configurationService.getSettingByName("INTERMEDIARY_PORTAL_CODE");
        SettingDTO beneSwiftCode=configurationService.getSettingByName("BENEFICIARY_SWIFT_CODE");
        SettingDTO beneBeneCode=configurationService.getSettingByName("BENEFICIARY_PORTAL_CODE");
        if(users==null)
        {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        else if ((users.getUserType()).equals(UserType.BANK))
        {

            BankUser user = (BankUser) users;
            if(wfSetting!=null)
            {
                formADTO.setExchangeRate(wfSetting.getValue());
            }
            if(beneSwiftCode!=null)
            {
                formADTO.setBeneBankSwiftCode(beneSwiftCode.getValue());

            }
            if(beneBeneCode!=null)
            {
                formADTO.setBeneBankPoCode(beneBeneCode.getValue());
            }
            if(interSwiftCode!=null)
            {
                formADTO.setInterBankSwiftCode(interSwiftCode.getValue());
            }
            if(interBeneCode!=null)
            {
                formADTO.setInterBankPoCode(interBeneCode.getValue());

            }
            formADTO.setUserType("BANK");

        }

        else {
            throw new CronJobException(messageSource.getMessage("form.process.failure",null,locale));
        }

        return formADTO;

    }


    @Override
    public FormADTO getFormA(Long id) throws CronJobException
    {
        FormA formA=formARepo.findOne(id);
        FormADTO formADTO =convertEntityTODTOView(formA);
//        if(formA.getDocuments() != null) {
//            Iterator<FormaDocument> formaDocumentIterator = formA.getDocuments().iterator();
//            Iterator<FormaDocumentDTO> formaDocumentDTOIterator = formADTO.getDocuments().iterator();
//
//            while (formaDocumentIterator.hasNext() && formaDocumentDTOIterator.hasNext()) {
//                FormaDocument formaDocument = formaDocumentIterator.next();
//                FormaDocumentDTO formaDocumentDTO = formaDocumentDTOIterator.next();
//                if (formaDocument.getDocumentType() == null) {
//                    formaDocumentDTOIterator.remove();
//                    formaDocumentIterator.remove();
//                } else {
//                    formaDocumentDTO.setTypeOfDocument(formaDocument.getDocumentType().getDescription());
//                    formaDocumentDTO.setCodedoctype(formaDocument.getDocumentType().getCode());
//                }
//            }
//        }

        return  formADTO;
    }


    @Override
    public String saveFormA(FormADTO formADTO) throws CronJobException
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();

        BankUser bankUser=(BankUser) doneBy;

        if(doneBy==null)
        {
            throw new CronJobException(messageSource.getMessage("form.add.failure",null,locale));
        }

        try {
            String generatedFormNumber = FormNumber.getFormNumber();
//            if(formADTO.getDocuments() != null) {
//                for (FormaDocumentDTO d : formADTO.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, generatedFormNumber, d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//            }
            FormA formA= convertDTOTOEntity(formADTO);
            formA.setUserType(doneBy.getUserType().toString());
            formA.setInitiatedBy(doneBy.getUserName());
            formA.setDateCreated(new Date());
            formA.setFormNumber(generatedFormNumber);
            formA.setTempFormNumber(generatedFormNumber);
            formA.setUtilization(new BigDecimal(0));
            formA.setAllocatedAmount(new BigDecimal(0));
            formA.setExpiryDate(getFormExpiryDate());
            formA.setCountry("NG");
            formA.setSubmitFlag("U");
            /**
             * BillsDocument table
             */
//        if(formADTO.getDocuments() != null) {
//            if (null != formADTO.getBranch() && !formADTO.getBranch().equalsIgnoreCase("") ) {
//                System.out.println(formADTO.getBranch());
//                Branch branch = branchRepo.findOne(Long.parseLong(formADTO.getBranch()));
//                formA.setDocSubBranch(branch);
//            }
//            Iterator<FormaDocument> DocumentIterator = formA.getDocuments().iterator();
//            Iterator<FormaDocumentDTO> DocumentDTOIterator = formADTO.getDocuments().iterator();
//
//            while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                FormaDocument document = DocumentIterator.next();
//                FormaDocumentDTO documentDTO = DocumentDTOIterator.next();
//                if (documentDTO.getTypeOfDocument() == null) {
//                    DocumentIterator.remove();
//                    DocumentDTOIterator.remove();
//                } else {
//                    document.setFileUpload(documentDTO.getFileUpload());
//                    document.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                    document.setFormA(formA);
//                }
//            }
//        }
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_A_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            formA.setWorkFlow(workFlow);
            formARepo.save(formA);
            return messageSource.getMessage("forma.add.success", null, locale);
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale), e);
        }
    }

    private Date getFormExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        SettingDTO setting = configurationService.getSettingByName("FORM_A_EXPIRY");
        if (setting != null && setting.isEnabled()) {
            int days = NumberUtils.toInt(setting.getValue());
            calendar.add(Calendar.DAY_OF_YEAR, days);
            return calendar.getTime();
        }
        return null;
    }






    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS)
    {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        for(Comments comments:commentDTOS)
        {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return  commentDTOList;
    }

    private CommentDTO convertEntityToDTO(Comments comments)
    {
        WorkFlowStates workFlowStates= workFlowStatesRepo.findByTypeAndCode("FORM_A",comments.getStatus());
        if(workFlowStates!=null)
        {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments,CommentDTO.class);
    }

    @Override
    public AccountDTO getAccountByAccountNumber(String accountNumber) {

        if(accountNumber==null)
        {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }

        AccountDTO  accountDTO=accountService.getAccountByAccountNumber(accountNumber);

        return accountDTO;
    }

    public List<CommentDTO> getComments(Long id) throws CronJobException
    {
        FormA formA=formARepo.findOne(id);
        List<CommentDTO> commentDTOList=new ArrayList<>();
        if(formA!=null)
        {
            commentDTOList=convertEntitiestoDTOs(formA.getComment());

        }
        return  commentDTOList;
    }

    @Override
    public List<CommentDTO> getComments(FormA formA) throws CronJobException {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        {
            commentDTOList=convertEntitiestoDTOs(formA.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus()!="A");


        }
        return  commentDTOList;
    }

    public FormAFI getCommentsAndBankersDetails(FormA formA){
        HashMap<String, String> map = new HashMap<>();
        HashMap<String, Date> map2 = new HashMap<>();

        try {
            List<CommentDTO> commentDTO = getComments(formA);
            commentDTO.forEach(commentDtd -> {
                commentDtd.getUsername();
                commentDtd.getComment();
                commentDtd.getMadeOn();
                map.put("userName", commentDtd.getUsername());
                map.put("comment", commentDtd.getComment());
                map2.put("madeOn", commentDtd.getMadeOn());
            });
            BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
            logger.info("The Main of form AA Bank Useer{}", bankUser.getFirstName());
            FormADTO formADTO = formACorporateService.convertEntityTODTO(formA);
            FormAFI formAFI = new FormAFI();
            formAFI.setFormADTO(formADTO);
            formAFI.setComment(map.get("comment"));
            formAFI.setAuthFirstName(bankUser.getFirstName());
            formAFI.setAuthLastName(bankUser.getLastName());
            formAFI.setMadeOn(map2.get("madeOn"));
            formAFI.setSortCode(bankUser.getBranch().getSortCode());
            return formAFI;


        } catch (CronJobException e)
        {
            throw new CronJobException(e);
        }
    }


    @Override
    public String addAttachments(MultipartFile file, String formaNumber, String documentType) {
        String uploadedFileName = "";


        if (file.isEmpty()) {
            System.out.println("file is empty");
        }

        try {
            byte[] bytes = file.getBytes();
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User users = principal.getUser();

            if (users.getUserType().equals(UserType.BANK)) {

                Path  path = Paths.get(UPLOADED_FOLDER + formaNumber + documentType + file.getOriginalFilename());
                logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());


                File f = new File(path.toString());
                if(f.exists()){
                    System.out.println("file already exist");
                }else{
                    System.out.println("file does not exist");
                }

                Files.write(path, bytes);
                logger.info("file uploaded successfully to {} directory", path);
                uploadedFileName =file.getOriginalFilename();
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("failed to upload file");
        }

        logger.info("uploadFileName {}", uploadedFileName);
        if (StringUtils.isEmpty(uploadedFileName)) {
            logger.info("No file added, Please select a file to upload");

        } else {
            logger.info("You successfully uploaded {}", uploadedFileName);

        }
        return uploadedFileName;
    }


    @Override
    public Page<FormADTO> getAllFormAs(Pageable pageDetails)
    {
        Page<FormA> page = formARepo.findByStatus("P", pageDetails);
        List<FormADTO> dtOs = convertEntitiesToDTOs(page.getContent());
        List<FormADTO> sortedList = dtOs.stream().sorted(Comparator.comparing(FormADTO::getDateCreated).reversed()).collect(Collectors.toList());
        long t = page.getTotalElements();
        Page<FormADTO> pageImpl = new PageImpl<FormADTO>(sortedList, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<FormADTO> getOpenFormAs(Pageable pageable)
    {

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        RetailUser retailUser=(RetailUser)users;
        Page<FormA> pages=formARepo.findByCifidAndStatus(retailUser.getCustomerId(),"P","S","D",pageable);
        List<FormADTO> formADTOS=convertEntitiesToDTOs(pages.getContent());
        long t=pages.getTotalElements();
        Page<FormADTO> page1=new PageImpl<FormADTO>(formADTOS,pageable,t);
        return page1;
    }

    @Override
    public Page<FormADTO> getClosedFormAs(Pageable pageable)
    {
        Page<FormA> page=formARepo.findByStatusNotNull(pageable);
        List<FormADTO> formADTOS=convertEntitiesToDTOs(page.getContent());
        long t=page.getTotalElements();
        Page<FormADTO> page1=new PageImpl<FormADTO>(formADTOS,pageable,t);
        return page1;
    }



    private FormADTO convertEntityTODTO(FormA formA)
    {
        FormADTO formADTO=modelMapper.map(formA , FormADTO.class);
        if(formADTO.getStatus() != null &&!(formADTO.getStatus().equals("P"))  )
        {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formADTO.getWorkFlow().getCodeType(), formADTO.getStatus());

            formADTO.setStatusDesc(workFlowStates.getDescription());

            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
        }
        if(formA.getPurposeOfPayment() != null){
            Code codec = codeService.getByTypeAndCode("PURPOSE_OF_PAYMENT", formA.getPurposeOfPayment());
            formADTO.setPurposeOfPaymentDesc(codec.getDescription());
        }
        if(formADTO.getStatus().equals("P"))
        {
            formADTO.setStatusDesc("Saved");
        }
        if (formA.getDateCreated()!=null){
            formADTO.setDateCreated(formA.getDateCreated());
        }

        if(formA.getAllocatedAmount()==null)
        {
            formADTO.setAllocatedAmount("0");
        }
        if(formA.getUtilization()==null)
        {
            formADTO.setUtilization("0");
        }
        if(formA.getAmount()==null)
        {
            formADTO.setAmount("0");
        }
        if(formA.getAmount()!=null && formA.getCurrencyCode()!=null)
        {
            formADTO.setAmount(formA.getCurrencyCode() + " " + formA.getAmount().toString());
        }
        if (formA.getAllocatedAmount() != null) {
            formADTO.setAllocatedAmount(formA.getAllocatedAmount().toString());
        }
        if (formA.getUtilization() != null) {
            formADTO.setUtilization(formA.getUtilization().toString());
        }
        if (formA.getCifid() != null){
            RetailUser retailUser = retailUserRepo.findFirstByCustomerId(formA.getCifid());
            if (retailUser != null){
                formADTO.setCustomerName(retailUser.getFirstName() + " " + retailUser.getLastName());
            }else {
                System.out.println("this is d customer id");
                System.out.println(formA.getCifid());
                System.out.println(formA.getBeneficiaryName());
                Corporate corporate = corporateRepo.findFirstByCustomerId(formA.getCifid());
                formADTO.setCustomerName(corporate.getName());
            }
        }

        return formADTO;
    }

    @Override
    public BankCustomerDTO getBankCustomerByCustomerId(String customerId) throws CronJobException {
      BankCustomerDTO bankCustomerDTO = null;
        if (customerId != null){
        List<AccountDTO> customerAccounts = new ArrayList<>();
        List<String> accountNumbers = new ArrayList<>();

            if (retailUserRepo.existsByCustomerId(customerId)){
                System.out.println("RETAIL CUSTOMER");
                RetailUser retailUser = retailUserRepo.findFirstByCustomerId(customerId);
                System.out.println("RETAIL CUSTOMER");
                bankCustomerDTO=modelMapper.map(retailUser , BankCustomerDTO.class);
                System.out.println("RETAIL CUSTOMER");
                customerAccounts = accountService.getAccounts(customerId);
                    for(AccountDTO accountDTO: customerAccounts){
                        accountNumbers.add(accountDTO.getAccountNumber());
                    }
                bankCustomerDTO.setAccountNumbers(accountNumbers);
                bankCustomerDTO.setType("RETAIL");
                bankCustomerDTO.setLabel("Customer's name (lastname first)");
                bankCustomerDTO.setCustomerName(retailUser.getLastName() +" "+retailUser.getFirstName());
                bankCustomerDTO.setPhone(bankCustomerDTO.getPhoneNumber());
                System.out.println("RETAIL CUSTOMER");
            }else if(corporateRepo.existsByCustomerId(customerId)){
                System.out.println("CORPORATE CUSTOMER");
                Corporate corporate = corporateRepo.findFirstByCustomerId(customerId);
                System.out.println("CORPORATE CUSTOMER");
                bankCustomerDTO=modelMapper.map(corporate , BankCustomerDTO.class);
                System.out.println("CORPORATE CUSTOMER");
                customerAccounts = accountService.getAccounts(customerId);
                for(AccountDTO accountDTO: customerAccounts){
                    System.out.println("corporate customer account "+accountDTO.getAccountNumber());
                    accountNumbers.add(accountDTO.getAccountNumber());
                }
                bankCustomerDTO.setAccountNumbers(accountNumbers);
                bankCustomerDTO.setType("CORPORATE");
                bankCustomerDTO.setLabel("Corporate name");
                bankCustomerDTO.setCustomerName(corporate.getName());
                System.out.println("CORPORATE CUSTOMER");
            }else{
//                bankCustomerDTO.setType("NOT_FOUND");
                return null;
            }
        }
        return bankCustomerDTO;
    }

    private FormADTO convertEntityTODTOView(FormA formA)
    {
        FormADTO formADTO=modelMapper.map(formA , FormADTO.class);
        if(formADTO.getStatus() != null &&!(formADTO.getStatus().equals("P"))  )
        {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formADTO.getWorkFlow().getCodeType(), formADTO.getStatus());
            formADTO.setStatusDesc(workFlowStates.getDescription());
            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
            formADTO.setWorkFlow(formA.getWorkFlow());
        }
        if(formADTO.getStatus().equals("P"))
        {
            formADTO.setStatusDesc("Saved");
            formADTO.setWorkFlowId(formA.getWorkFlow().getId());
            formADTO.setWorkFlow(formA.getWorkFlow());
        }
        if (formA.getDateCreated()!=null)
        {
            formADTO.setDateCreated(formA.getDateCreated());
        }

        if(formA.getAmount()!=null ) {
            formADTO.setAmount(formA.getAmount().toString());
        }
        if (formA.getAllocatedAmount() != null) {
            formADTO.setAllocatedAmount(formA.getAllocatedAmount().toString());
        }
        if (formA.getUtilization() != null) {
            formADTO.setUtilization(formA.getUtilization().toString());
        }
        if(formA.getDocSubBranch() != null){
            formADTO.setBranch(formA.getDocSubBranch().getId().toString());
            formADTO.setBrDesc(formA.getDocSubBranch().getName());
        }
        if (formA.getCifid() != null){
            RetailUser retailUser = retailUserRepo.findFirstByCustomerId(formA.getCifid());
            if (retailUser != null){
                formADTO.setCustomerName(retailUser.getFirstName() + " " + retailUser.getLastName());
            }else {
                Corporate corporate = corporateRepo.findFirstByCustomerId(formA.getCifid());
                formADTO.setCustomerName(corporate.getName());
            }
        }
        return formADTO;
    }

    private List<FormADTO> convertEntitiesToDTOs(Iterable<FormA> formAs){
        List<FormADTO> formADTOs = new ArrayList<FormADTO>();
        for (FormA formA: formAs){
            formADTOs.add(convertEntityTODTO(formA));
        }
        return formADTOs;
    }

    @Override
    public FormHolder createFormHolder(FormADTO formADTO,String name) throws CronJobException
    {

        try {
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User doneBy = principal.getUser();

            BankUser bankUser=(BankUser) doneBy;

            FormHolder holder = new FormHolder();
            FormA formA  = new FormA();
            formA.setDateCreated(new Date());
            formA.setCountry("NG");
            formA.setSubmitFlag("U");
            formA.setUtilization(new BigDecimal(0));
            formA.setApproverName(bankUser.getUserName());
            System.out.println(formA.getDocuments().size());
            holder.setObjectData(objectMapper.writeValueAsString(formA));
            System.out.println("object mapper");
            holder.setEntityId(formA.getId());
            holder.setEntityName("FormA");
            holder.setAction(name);
            logger.info("getting the formA");
            return holder;

        }
        catch (JsonProcessingException e)
        {
            e.printStackTrace();
            throw new CronJobException(messageSource.getMessage("form.process.failure",null,locale));

        }


    }


    @Override
    public String getFormNumber() throws CronJobException
    {

        return null;
    }

    @Override
    public Iterable<FormADTO> getAllFormAs() {
        Iterable<FormA> formAs= formARepo.findAll();
        return convertEntitiesToDTOs(formAs);
    }


    private FormA convertDTOTOEntity(FormADTO formADTO)  throws CronJobException
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        WorkFlow workFlow= workFlowRepo.findOne(formADTO.getWorkFlowId());
        formADTO.setWorkFlow(workFlow);
        FormA formA=modelMapper.map(formADTO,FormA.class);
        return formA;

    }

//    private FormA addComments(FormADTO formADTO) throws CronJobException {
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User doneBy = principal.getUser();
//        logger.info("WORK_FLOW_ID {}", formADTO.getWorkFlowId());
//        WorkFlow workFlow = workFlowRepo.findOne(formADTO.getWorkFlowId());
//        formADTO.setWorkFlow(workFlow);
//        FormA singleFormA = formARepo.findOne(formADTO.getId());
//
//        System.out.println(singleFormA.getDocuments().size());
//        System.out.println(formADTO.getDocuments().size());
//
//        formADTO.setExpiryDate(singleFormA.getExpiryDate());
//        FormA formA = modelMapper.map(formADTO, FormA.class);
//        if(formADTO.getDocuments() != null){
//        System.out.println("model mapper " + formA.getDocuments());
//
//        List<FormaDocument> l = new ArrayList<>();
//        if (!"".equalsIgnoreCase(formADTO.getBranch()) && null != formADTO) {
//            System.out.println(formADTO.getBranch());
//            Branch branch = branchRepo.findOne(Long.parseLong(formADTO.getBranch()));
//            formA.setDocSubBranch(branch);
//            for (FormaDocumentDTO f : formADTO.getDocuments()) {
//                FormaDocument formaDocument = formaDocumentRepo.findOne(f.getId());
//                formaDocument.setNumberOfOriginalDoc(f.getNumberOfOriginalDoc());
//                formaDocument.setNumberOfDuplicateDoc(f.getNumberOfDuplicateDoc());
//                formaDocument.setFormA(formA);
//                l.add(formaDocument);
//            }
//            formA.setDocuments(l);
//        }
//    }
//        Comments comments=new Comments();
//        comments.setMadeOn(new Date());
//        comments.setStatus(formADTO.getSubmitAction());
//        comments.setComment(formADTO.getComments());
//        comments.setUsername(doneBy.getUserName());
//        List<Comments> oldComment=singleFormA.getComment();
//        oldComment.add(comments);
//        formA.setComment(oldComment);
//        return formA;
//    }

    @Override
    public String checkApplicationNumber(String applicationNumber) throws CronJobException {
        try {
            Boolean tmsCheck = formARepo.existsByApplicationNumber(applicationNumber);
            System.out.println(tmsCheck);
            String result = "";
            if (tmsCheck) {
                result = "true";
                return result;
            }
            Boolean finacleCheck = integrationService.checkApplicationNumber(applicationNumber);
            System.out.println(finacleCheck);
            if (finacleCheck) {
                result = "true";
                return result;
            }
        }catch (CronJobException e){
            e.printStackTrace();
        }
        return "false";
    }

    @Override
    public String checkUsertype(String cifid) throws CronJobException {
        String result = "";
        if(retailUserRepo.existsByCustomerId(cifid)){
            result= "RETAIL";
        }
        if(corporateRepo.existsByCustomerId(cifid)){
            result = "CORPORATE";
        }
        return result;
    }

    @Override
    public String updateFormA(FormADTO formADTO) throws CronJobException {
        System.out.println("got into the service");
        FormA formA = formARepo.findOne(formADTO.getId());

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();

        BankUser bankUser = (BankUser) doneBy;
        if (doneBy == null) {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
        }


        try {

            System.out.println("forma service inside the try");
//            if (formADTO.getDocuments() != null) {
//                for (FormaDocumentDTO d : formADTO.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        System.out.println("forma service saving the document");
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, formA.getTempFormNumber(), d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//            }

            entityManager.detach(formA);
            formA = convertDTOTOEntity(formADTO);
            formA.setDateCreated(new Date());
            formA.setExpiryDate(getFormExpiryDate());
            formA.setUtilization(new BigDecimal(0));
            formA.setAllocatedAmount(new BigDecimal(0));
            formA.setCountry("NG");
            System.out.println("forma service after setting all forma parameters");
            /**
             * BillsDocument table
             */
//            if (formADTO.getDocuments() != null) {
//                if (!"".equalsIgnoreCase(formADTO.getBranch()) && null != formADTO){
//                    System.out.println(formADTO.getBranch());
//                    Branch branch = branchRepo.findOne(Long.parseLong(formADTO.getBranch()));
//                    formA.setDocSubBranch(branch);
//                }
//                Iterator<FormaDocument> DocumentIterator = formA.getDocuments().iterator();
//                Iterator<FormaDocumentDTO> DocumentDTOIterator = formADTO.getDocuments().iterator();
//                Collection<FormaDocument> fields = new ArrayList<FormaDocument>();
//                while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                    FormaDocument document = DocumentIterator.next();
//                    FormaDocumentDTO documentDTO = DocumentDTOIterator.next();
//                    FormaDocument formaDocument = null;
//                    if (documentDTO.getTypeOfDocument() == null) {
//                        DocumentIterator.remove();
//                        DocumentDTOIterator.remove();
//                    } else if (documentDTO.getId() == null) {
//                        System.out.println("newly added document");
//                        formaDocument = new FormaDocument();
//                        System.out.println("before model mapper");
//                        modelMapper.map(documentDTO, formaDocument);
//                        System.out.println("after model mapper");
//                        formaDocument.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                        formaDocument.setFormA(formA);
//                        System.out.println("newly added document ending");
//                    } else {
//                        System.out.println("existing document");
//                        formaDocument = formaDocumentRepo.findOne(documentDTO.getId());
//                        System.out.println("after finding existing document");
//                        formaDocument.setDocumentValue(documentDTO.getDocumentValue());
//                        formaDocument.setDocumentRemark(documentDTO.getDocumentRemark());
//                        formaDocument.setDocumentNumber(documentDTO.getDocumentNumber());
//                        formaDocument.setDocumentDate(documentDTO.getDocumentDate());
//                        if (documentDTO.getFileUpload() != null) {
//                            formaDocument.setFileUpload(documentDTO.getFileUpload());
//                        }
//                        formaDocument.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                        formaDocument.setFormA(formA);
//                        System.out.println("ending of  existing document");
//
//                    }
//                    fields.add(formaDocument);
//                }
//                formA.setDocuments(fields);
//            }
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_A_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            formA.setWorkFlow(workFlow);
            formARepo.save(formA);
            return messageSource.getMessage("forma.update.success", null, locale);
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }


    }


}
