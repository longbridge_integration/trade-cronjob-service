package cronjobservice.repositories;

import cronjobservice.models.BillsUnderLc;
import cronjobservice.models.ImportBillUnderLc;
import org.springframework.stereotype.Repository;

@Repository
public interface ImportBillUnderLcRepo extends CommonRepo<ImportBillUnderLc,Long> {

    ImportBillUnderLc findByBillNumber(String billNumber);


}
