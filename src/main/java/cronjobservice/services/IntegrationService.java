package cronjobservice.services;

import com.fasterxml.jackson.databind.node.ObjectNode;

import cronjobservice.api.*;
import cronjobservice.utils.statement.AccountStatement;
import cronjobservice.utils.statement.TransactionHistory;
import org.springframework.scheduling.annotation.Async;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * The {@IntegrationService} interface provides the methods for accessing the various integration services
 */
public interface IntegrationService {


    AccountInfo fetchAccount(String accountNumber);

    /**
     * Returns all the accounts of a customer
     *
     * @param cifid the customer's id
     * @return a list of accounts
     */
    List<AccountInfo> fetchAccountsByCifId(String cifid);

    List<AccountInfo> fetchDollarAccountsByCifId(String cifId);

    List<AccountInfo> fetchNairaAccountsByCifId(String cifId);

    List<ExchangeRate> getExchangeRate();


//    ExchangeRate getCurrentExchangeRate(String fromCurrency, String toCurrency, String rateCode);

    /**
     //     * Fetches the account Name, Balance , Type from the account table specified by account Number
     //     *
     //     * @param acctNo account number of the account
     //     * @return Map containing the available details of the account
     //     */
    AccountDetails getAccountDetails(String acctNo);

    /**
     * Returns all the accounts of a customer
     *
     * @param cifId the customer's id
     * @return a list of accounts
     */
    CustomerDetails getCustomerDetailsByCif(String cifId);
    String getCustomerBvnByCif(String cifId);

    /**
     * Returns a list of Port of Discharge and Loading
     * @return list Ports
     */
    List<TradeCodes> getPortCodes();

    /**
     * Returns a list of Harmonised System
     * @return list HS Codes
     */
    List<TradeCodes> getHSCodes();

    /**
     * Returns a list of Sectorial Purpose Code
     * @return list Sectorial Purpose Codes
     */
    List<TradeCodes> getSectorialPurposeCodes();

    /**
     * Returns a list of Mode of Transport
     * @return list Mode of transport
     */
    List<TradeCodes> getModeOfTransport();

    /**
     * Returns a list of Payment Mode Code
     * @return list Payment Mode Codes
     */
    List<TradeCodes> getPaymentModeCode();

    /**
     * Returns a list of Purpose Codes
     * @return list Purpose Codes
     */
    List<TradeCodes> getPurposeOfPayment();

    List<NostroAccount> getNostroAccounts();

    List<TradeCodes> getCurrencyCode();

    List<TradeCodes> getCountryCode();

    List<TradeCodes> getStateCode();

    List<TradeCodes> getCityCode();

    List<FinancialInstitution> getFinancialInstitution();

    List<BranchCode> getBranch(String bankCode);

    /**
     * Returns a list of Swift Codes
     * @return list Swift Codes
     */
    List<SwiftCode> getSwiftCodes();

    Map<String, BigDecimal> getBalance(String accountId);

    /**
     * Returns a the Exchange rate
     * @return list Exchange rate
     */
    ExchangeRate getRateCode(String formCurrency, String toCurrency, String rateCode);

    /**
     * Returns a list of all Form Ms on Finacle
     * @return
     */
    List<FormMDetails> getAllFormMs();

    List<FormADetails> getAllFormAs();

    /**
     * Returns the charge amount on form M
     * @return form M Charge
     */
    String getFormMCharge();

    Boolean checkApplicationNumber(String applicationNumber);

    Boolean checkLcNumber(String lcNumber);

    /**
     * Returns a list of Form A fetched by cifid
     * @return list Form A
     */
    List<FormADetails> getFormAByCifId(String cifId);

    /**
     * Returns a list of Form M fetched by cifid
     * @return list Form M
     */
    List<FormMDetails> getFormMByCifId(String cifId);

    List<FormMDetails> getAllFormMAddedOmFinacle();

    List<FormMDetails> getFormMByFormNums(List<String> forms);

    /**
     * Returns a list of Form Nxp fetched by cifid
     * @return list Form Nxp
     */
    List<FormNxpDetails> getFormNxpByCifId(String cifId);

    /**
     * Returns a list of Remittance fetched by cifid
     * @return list Remittance
     */
    List<RemittanceDetails> getRemittanceByCifId(String cifId);

    /**
     * Returns a list of Paar fetched by cifid
     * @return list Paar
     */
    List<PaarDetails> getPaarByCifId(String cifId);

    /**
     * Returns a list of Form A Allocated from Finacle
     * @return list Form A
     */
    List<FormADetails> getFormAAllocated();

    /**
     * Returns a list of Form A added from Finacle
     * @return list Form A
     */
    List<FormADetails> getFormAAddedOnFinacle();

    List<FormQDetails> getFormQAddedOnFinacle();

    List<FormQDetails> getFormQAllocation();

    List<ImportBillDetails> getImportBillsUnderLcAddedOnFinacle();

    List<ImportBillDetails> getImportBillsUForCollectionddedOnFinacle();

    /**
     * Returns a list of Form Nxp added from Finacle
     * @return list Form Nxp
     */
    List<FormNxpDetails> getFormNxpAddedOnFinacle();

    /**
     * Returns a list of Form M added from Finacle
     * @return list Form M
     */
    List<FormMDetails> getFormMAddedOnFinacle();

    /**
     * Returns a list of Remittance added from Finacle
     * @return list Remittance
     */
    List<RemittanceDetails> getRemittanceIssuedOnFinacle();

    List<RemittanceDetails> getFundTransferIssuedOnFinacle();

    /**
     * Returns a list of Form A submit response from Finacle
     * @return list Form A
     */
    List<FormADetails> getFormASubmitResponse();

    /**
     * Returns a list of Form Nxp submit response from Finacle
     * @return list Form Nxp
     */
    List<FormNxpDetails> getFormNxpSubmitResponse();

    /**
     * Returns a list of Remittance submit response from Finacle
     * @return list Remittance
     */
    List<RemittanceDetails> getRemittanceIssueResponse();

    List<RemittanceDetails> getRemittanceFormAIssueResponse();

    /**
     * Returns the status on form M
     * @return form M Status
     */
    String getFormMStatusUpdate(String formNumber);

    /**
     * Returns a list of OutwardGuarantee submit response from Finacle
     * @return list OutwardGuarantee
     */
    List<GuaranteeDetails> getOutwardGuaranteeSubmitResponse();

    /**
     * Returns a list of Inward Guarantee added on Finacle By Cifid
     * @return list InwardGuarantee
     */
    List<GuaranteeDetails> getInwardGuaranteeAddByCifId(String cifId);

    /**
     * Returns a list of Inward Guarantee added on Finacle
     * @return list InwardGuarantee
     */
    List<GuaranteeDetails> getInwardGuaranteeAddedOnFinacle();

    List<LCDetails> getImportLcAddedOnFinacle();

    /**
     * Returns a list of Outward Guarantee added on Finacle
     * @return list OutwardGuarantee
     */
    List<GuaranteeDetails> getOutwardGuaranteeAddedOnFinacle();

    /**
     * Returns a list of Form M Allocated from Finacle
     * @return list Form M
     */
    List<FormMDetails> getFormMAllocated();

    /**
     * Returns a list of Importlc submit response from Finacle
     * @return list Importlc
     */
    List<LCDetails> getImportLcSubmitResponse();

    /**
     * Returns a list of Exportlc submit response from Finacle
     * @return list Exportlc
     */
    List<LCDetails> getExportLcSubmitResponse();

    List<LCDetails> getExportLcPendingtResponse();

    List<LCDetails> getExportLcPendingResponse();

    List<LCDetails> updateExportLcPendingVerificationToAdvised();

    List<LCDetails> getExportLcAddedOnFinacle();

    List<String> getShipmentTerms();


    List<BtaDetails> getBtaSubmitResponse();

    List<PtaDetails> getPtaSubmitResponse();

    List<ExportBillsDetails> getLodgeBillForCollectionResponse();

    List<ExportBillsDetails> getLodgeBillUnderLcResponse();

    List<FormQDetails> getFormQSubmitResponse();


//    /**
//     * Returns a list of Form M submit response from Finacle
//     * @return list Form M
//     */
//    List<FormMDetails> getFormMSubmitResponseOn();

//    List<AccountInfo> fetchAccounts(String cifid);
//
//    List<AccountInfo> fetchDummyNairaAccounts(String cifid);
//
//
//
//
//
//    Map<String, BigDecimal> getDummyBalance(String accountId);


//    List<AccountInfo> fetchDummyDollarAccounts(String cifid);

    /**
     * Fetches the {@link AccountStatement} of the account identified by
     * {@code accountId} for the period between {@code fromDate} and {@code toDate}
     *
     * @param accountNo the  account Number of the Account
     * @param fromDate  the Date from where to begin fetching the account statement
     * @param toDate    the Date to stop fetching the account statement (inclusive)
     * @return {@code AccountStatement} object
     */
    AccountStatement getAccountStatements(String accountNo, Date fromDate, Date toDate, String tranType, PaginationDetails paginationDetails);
//    AccountStatement getAccountStatements(String accountNo, Date fromDate, Date toDate, String tranType);

    List<TransactionHistory> getLastNTransactions(String accountNo, String numberOfRecords);



    //    @Override
    //    public List<AccountInfo> fetchAccounts(String cifid) {
    //        return null;
    //    }
    //
    //    @Override
    //    public List<AccountInfo> fetchDummyDollarAccounts(String cifid) {
    //        try {
    //
    //            AccountInfo accountInfo=new AccountInfo();
    //            List<AccountInfo> accountDetails=new ArrayList<>();
    //            for(int i=0; i<2; i++)
    //            {
    //                accountInfo.setAccountNumber("C3211009");
    //                accountInfo.setAccountCurrency("USD");
    //                accountInfo.setCustomerId("C0008541");
    //                accountInfo.setAccountStatus("A");
    //                accountInfo.setAccountName("chioma chukelu");
    //                accountInfo.setAvailableBalance("20000");
    //                accountDetails.add(accountInfo);
    //            }
    //
    //            return  accountDetails;
    //        } catch (Exception e) {
    //            logger.error("Exception occurred {}", e.getMessage());
    //            return new ArrayList<>();
    //        }
    //    }
    //
    //    @Override
    //    public List<AccountInfo> fetchDummyNairaAccounts(String cifid) {
    //        try {
    //
    //            AccountInfo accountInfo=new AccountInfo();
    //            List<AccountInfo> accountDetails=new ArrayList<>();
    //            for(int i=0; i<2; i++)
    //            {
    //                accountInfo.setAccountNumber("201234311");
    //                accountInfo.setAccountCurrency("NGN");
    //                accountInfo.setCustomerId("C0008541");
    //                accountInfo.setAccountStatus("A");
    //                accountInfo.setAccountName("chioma chukelu");
    //                accountInfo.setAvailableBalance("200000");
    //                accountDetails.add(accountInfo);
    //            }
    //
    //            return  accountDetails;
    //        } catch (Exception e) {
    //            logger.error("Exception occurred {}", e.getMessage());
    //            return new ArrayList<>();
    //        }
    //    }
    //
    //    @Override
    //    public ExchangeRate getCurrentExchangeRate(String fromCurrency, String toCurrency, String rateCode) {
    //       try{
    //            ExchangeRate exchangeRate= new ExchangeRate();
    //            exchangeRate.setAmountValue("306");
    //            exchangeRate.setCurrencyCode(fromCurrency);
    //           return exchangeRate;
    //       }catch (Exception e){
    //           logger.error("Exception occurred {}", e.getMessage());
    //           return null;
    //       }
    //
    //    }
    //
    //
    //
    //
    //
    ////    @Override
    ////    public AccountStatement getAccountStatements(String accountNo, Date fromDate, Date toDate, String tranType, PaginationDetails paginationDetails) {
    ////        AccountStatement statement = new AccountStatement();
    ////        try {
    ////            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
    ////       /* String dateInString = "7-Jun-2013";
    ////        Date date = formatter.parse(dateInString);*/
    ////            String uri = URI + "/statement";
    ////            Map<String, Object> params = new HashMap<>();
    ////            params.put("accountNumber", accountNo);
    ////            params.put("fromDate", formatter.format(fromDate));
    ////            params.put("solId", viewAccountDetails(accountNo).getSolId());
    ////            if (tranType != null)
    ////                params.put("tranType", tranType);
    ////            if (toDate != null) params.put("toDate", formatter.format(toDate));
    ////            if (paginationDetails != null) params.put("paginationDetails", paginationDetails);
    ////
    ////
    ////            statement = template.postForObject(uri, params, AccountStatement.class);
    ////
    ////
    ////        } catch (Exception e) {
    ////
    ////        }
    ////
    ////
    ////        return statement;
    ////    }
    AccountStatement getAccountStatements(String accountNo, Date fromDate, Date toDate, String tranType);

    ////    @Override
    ////    public List<TransactionHistory> getLastNTransactions(String accountNo, String numberOfRecords) {
    ////        List<TransactionHistory> histories = new ArrayList<>();
    ////
    ////
    ////        try {
    ////
    ////            String uri = URI + "/transactions";
    ////            Map<String, String> params = new HashMap<>();
    ////            params.put("accountNumber", accountNo);
    ////            params.put("numberOfRecords", numberOfRecords);
    ////            params.put("branchId", viewAccountDetails(accountNo).getSolId());
    ////
    ////
    ////            TransactionHistory[] t = template.postForObject(uri, params, TransactionHistory[].class);
    ////            histories.addAll(Arrays.asList(t));
    ////
    ////
    ////        } catch (Exception e) {
    ////       e.printStackTrace();
    ////        }
    ////
    ////
    ////        return histories;
    ////    }
    //
    //
    //    @Override
    //    public Map<String, BigDecimal> getDummyBalance(String accountId) {
    //        Map<String,BigDecimal> response=new HashMap<>();
    //        try {
    //
    //            response.put("availablebalance",new BigDecimal(8000));
    //          }
    //        catch (Exception e)
    //        {
    //            e.printStackTrace();
    //        }
    //        return response;
    //    }
    //
    //    @Override
    //    public Map<String, BigDecimal> getBalance(String accountId)
    //    {
    //
    //        Map<String, BigDecimal> response = new HashMap<>();
    //        try {
    //            AccountDetails details = getAccountDetails(accountId);
    //            BigDecimal availBal = new BigDecimal(details.getAvailableBalance());
    //            BigDecimal ledgBal = new BigDecimal(details.getLedgerBalance());
    //            response.put("AvailableBalance", availBal);
    //            response.put("LedgerBalance", ledgBal);
    //
    //            return response;
    //        } catch (Exception e) {
    //            response.put("AvailableBalance", new BigDecimal(0));
    //            response.put("LedgerBalance", new BigDecimal(0));
    //            e.printStackTrace();
    //            return response;
    //        }
    //    }
    //
    //    @Override
    //    public TransRequest makeTransfer(TransRequest transRequest) throws TradeTransferException {
    //
    //        TransferType type = transRequest.getTransferType();
    //
    //        //switch (type) {
    //        switch (type) {
    //            case CORONATION_BANK_TRANSFER:
    //
    //            {
    //                transRequest.setTransferType(TransferType.CORONATION_BANK_TRANSFER);
    //                TransferDetails response;
    //                String uri = URI + "/transfer/local";
    //                Map<String, String> params = new HashMap<>();
    //                params.put("debitAccountNumber", transRequest.getCustomerAccountNumber());
    //                params.put("creditAccountNumber", transRequest.getBeneficiaryAccountNumber());
    //                params.put("tranAmount", transRequest.getAmount().toString());
    //                params.put("naration", transRequest.getNarration());
    //                logger.info(params.toString());
    //
    //                try {
    //                    response = template.postForObject(uri, params, TransferDetails.class);
    //
    //
    //                    transRequest.setStatus(response.getResponseCode());
    //                    transRequest.setStatusDescription(response.getResponseDescription());
    //                    transRequest.setReferenceNumber(response.getUniqueReferenceCode());
    //                    transRequest.setNarration(response.getNarration());
    //
    //
    //                    return transRequest;
    //
    //                } catch (HttpStatusCodeException e) {
    //
    //                    e.printStackTrace();
    //                    transRequest.setStatus(e.getStatusCode().toString());
    //                    transRequest.setStatusDescription(e.getStatusCode().getReasonPhrase());
    //                    return transRequest;
    //
    //                }
    //
    //
    //            }
    //            case INTER_BANK_TRANSFER: {
    //                transRequest.setTransferType(TransferType.INTER_BANK_TRANSFER);
    //                TransferDetails response;
    //                String uri = URI + "/transfer/nip";
    //                Map<String, String> params = new HashMap<>();
    //                params.put("debitAccountNumber", transRequest.getCustomerAccountNumber());
    //                params.put("creditAccountNumber", transRequest.getBeneficiaryAccountNumber());
    //                params.put("tranAmount", transRequest.getAmount().toString());
    //                params.put("destinationInstitutionCode", transRequest.getFinancialInstitution().getInstitutionCode());
    //                params.put("tranType", "NIP");
    //                logger.info("params for transfer {}", params.toString());
    //                try {
    //                    response = template.postForObject(uri, params, TransferDetails.class);
    //
    //
    //                    logger.info("response for transfer {}", response.toString());
    //                    transRequest.setReferenceNumber(response.getUniqueReferenceCode());
    //                    transRequest.setNarration(response.getNarration());
    //                    transRequest.setStatus(response.getResponseCode());
    //                    transRequest.setStatusDescription(response.getResponseDescription());
    //
    //                    return transRequest;
    //                }
    //
    //                catch (HttpStatusCodeException e) {
    //
    //                        e.printStackTrace();
    //                        transRequest.setStatus(e.getStatusCode().toString());
    //                        transRequest.setStatusDescription(e.getStatusCode().getReasonPhrase());
    //                        return transRequest;
    //
    //                    }
    //
    //            }
    //            case INTERNATIONAL_TRANSFER: {
    //
    //            }
    //
    //            case OWN_ACCOUNT_TRANSFER: {
    //                transRequest.setTransferType(TransferType.OWN_ACCOUNT_TRANSFER);
    //                TransferDetails response;
    //                String uri = URI + "/transfer/local";
    //                Map<String, String> params = new HashMap<>();
    //                params.put("debitAccountNumber", transRequest.getCustomerAccountNumber());
    //                params.put("creditAccountNumber", transRequest.getBeneficiaryAccountNumber());
    //                params.put("tranAmount", transRequest.getAmount().toString());
    //                params.put("naration", transRequest.getNarration());
    //                logger.info("params for transfer {}", params.toString());
    //                try {
    //                    response = template.postForObject(uri, params, TransferDetails.class);
    //                    transRequest.setNarration(response.getNarration());
    //                    transRequest.setReferenceNumber(response.getUniqueReferenceCode());
    //                    transRequest.setStatus(response.getResponseCode());
    //                    transRequest.setStatusDescription(response.getResponseDescription());
    //                    return transRequest;
    //
    //
    //                }  catch (HttpStatusCodeException e) {
    //
    //                    e.printStackTrace();
    //                    transRequest.setStatus(e.getStatusCode().toString());
    //                    transRequest.setStatusDescription(e.getStatusCode().getReasonPhrase());
    //                    return transRequest;
    //
    //                }
    //
    //
    //            }
    //
    //            case RTGS: {
    //                TransRequest request =
    //                        sendTransfer(transRequest);
    //
    //
    //                return request;
    //            }
    //        }
    //        logger.trace("request did not match any type");
    //        transRequest.setStatus(ResultType.ERROR.toString());
    //        return transRequest;
    //    }
    //
    //    @Override
    //    public TransferDetails makeNapsTransfer(Naps naps) throws TradeTransferException {
    //        String uri = URI + "/transfer/naps";
    //
    //        try {
    //            TransferDetails details = template.getForObject(uri, TransferDetails.class, naps);
    //            return details;
    //        } catch (Exception e) {
    //            return new TransferDetails();
    //        }
    //
    //
    //    }
    //
    //    @Override
    //    public AccountDetails viewAccountDetails(String acctNo) {
    //
    //        String uri = URI + "/account/{acctNo}";
    //        Map<String, String> params = new HashMap<>();
    //        params.put("acctNo", acctNo);
    //        try {
    //            AccountDetails details = template.getForObject(uri, AccountDetails.class, params);
    //            return details;
    //        } catch (Exception e) {
    //            return new AccountDetails();
    //        }
    //
    //
    //    }
    //
    //    @Override
    //    public CustomerDetails isAccountValid(String accNo, String email, String dob) {
    //        CustomerDetails result = new CustomerDetails();
    //        String uri = URI + "/account/verification";
    //        Map<String, String> params = new HashMap<>();
    //        params.put("accountNumber", accNo);
    //        params.put("email", email);
    //        params.put("dateOfBirth", dob);
    //        try {
    //            result = template.postForObject(uri, params, CustomerDetails.class);
    //
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //
    //        return result;
    //    }
    //
    //    @Override
    //    public CustomerDetails viewCustomerDetails(String accNo) {
    //        CustomerDetails result = new CustomerDetails();
    //        String uri = URI + "/customer/{cifId}";
    //        String cifId = getAccountDetails(accNo).getCifId();
    //        Map<String, String> params = new HashMap<>();
    //        params.put("cifId", cifId);
    //        try {
    //            result = template.getForObject(uri, CustomerDetails.class, params);
    //            return result;
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //
    //        return result;
    //    }
    //
    //    @Override
    //    public CustomerDetails viewCustomerDetailsByCif(String cifId) {
    //        CustomerDetails result = new CustomerDetails();
    //        String uri = URI + "/customer/{cifId}";
    //        Map<String, String> params = new HashMap<>();
    //        params.put("cifId",cifId);
    //        try {
    //            result = template.getForObject(uri, CustomerDetails.class, params);
    //            return result;
    //        }
    //        catch (Exception e)
    //        {
    //            e.printStackTrace();
    //        }
    //        return result;
    //    }
    //
    //    @Override
    //    public String getAccountName(String accountNumber) {
    //        logger.info(accountNumber + "account number");
    //
    //        return getAccountDetails(accountNumber).getAccountName();
    //
    //    }
    //
    //    @Override
    //    public BigDecimal getDailyDebitTransaction(String acctNo) {
    //
    //        BigDecimal result = new BigDecimal(0);
    //        String uri = URI + "/transfer/dailyTransaction";
    //        Map<String, String> params = new HashMap<>();
    //        params.put("accountNumber", acctNo);
    //
    //        try {
    //            String response = template.postForObject(uri, params, String.class);
    //            result = new BigDecimal(response);
    //        } catch (Exception e) {
    //
    //            e.printStackTrace();
    //        }
    //
    //        return result;
    //
    //    }
    //
    //
    //    @Override
    //    public String getDailyAccountLimit(String accNo, String channel) {
    //        String result = "NAN";
    //        String uri = URI + "/transfer/limit";
    //        Map<String, String> params = new HashMap<>();
    //        params.put("accountNumber", accNo);
    //        params.put("transactionChannel", channel);
    //        try {
    //            String response = template.postForObject(uri, params, String.class);
    //            result =(response);
    //        } catch (Exception e) {
    //
    //            e.printStackTrace();
    //        }
    //
    //        return result;
    //
    //    }
    //
    //    @Override
    //    public NEnquiryDetails doNameEnquiry(String destinationInstitutionCode, String accountNumber) {
    //        NEnquiryDetails result = new NEnquiryDetails();
    //        String uri = URI + "/transfer/nameEnquiry";
    //        Map<String, String> params = new HashMap<>();
    //        params.put("destinationInstitutionCode", destinationInstitutionCode);
    //        params.put("accountNumber", accountNumber);
    //        logger.trace("params {}", params);
    //        try {
    //
    //            result = template.postForObject(uri, params, NEnquiryDetails.class);
    //
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //
    //        return result;
    //    }
    //
    //    @Override
    //    @Async
    //    public BigDecimal getAvailableBalance(String s) {
    //        try {
    //            Map<String, BigDecimal> getBalance = getBalance(s);
    //            BigDecimal balance = getBalance.get("AvailableBalance");
    //            if (balance != null) {
    //                return balance;
    //            }
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //        return new BigDecimal(0);
    //    }
    //
        @Async
        CompletableFuture<ObjectNode> sendSMS(String message, String contact, String subject);

    /**
     * Fetches the account Balance of the account specified by accountId
     *
     * @param accountId accountId of the account
     * @return Map containing the available balance and ledger balance of the account
     */
//    Map<String, BigDecimal> getBalance(String accountId);


    /**
     * Initiates a transfer request to the relevant Transfer services.
     */
//    TransRequest makeTransfer(TransRequest transRequest) throws TradeTransferException;

//    TransferDetails makeNapsTransfer(Naps naps) throws TradeTransferException;

    /**
     * Fetches the account Name, Balance , Type from the account table specified by account Number
     *
     * @param acctNo account number of the account
     * @return Map containing the available details of the account
     */
//    AccountDetails viewAccountDetails(String acctNo);

//    CustomerDetails isAccountValid(String accNo, String email, String dob);
//
//    CustomerDetails viewCustomerDetails(String accNo);
//
//    CustomerDetails viewCustomerDetailsByCif(String cifId);


    /** Validate the account, with a valid account number, email and dob
     *
     * @param accNo account number of the account
     * @param email email of the account
     * @param dob date of birth specified in the account
     * @return Map that returns false of the details doesnt correspond or exist
     */


    /**
     * This fetches the full name of the customer connected to the
     * specified account number
     *
     * @param accountNumber
     * @return The full name of the customer or null
     */
//    String getAccountName(String accountNumber);

    /**
     * This method make an API that fetches the daily transactions with accountNo
     *
     * @param acctNo account number
     * @return map containing the total transaction amount
     */

//    BigDecimal getDailyDebitTransaction(String acctNo);

    /**
     * This method make an API that fetches the Daily Account Limit with accountNo and Channel
     *
     * @param accNo   the account number
     * @param channel the transaction channel
     * @return map containing the total transaction limit
     */
//    String getDailyAccountLimit(String accNo, String channel);


//    NEnquiryDetails doNameEnquiry(String destinationInstitutionCode, String accountNumber);

//
//    BigDecimal getAvailableBalance(String s);
//    @Async
//    CompletableFuture<ObjectNode>  sendSMS(String message, String contact, String subject);
//
//    Rate getFee(String channel);
//
////    String getFormNumbers(String s);
////    String getUtilization(String s);
////    String getAllocation(String s);
//        FormADetails getFormADetails(String s);

}
