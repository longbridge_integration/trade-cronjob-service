package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import cronjobservice.utils.PrettySerializer;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by Wunmi on 27/03/2017.
 */
@Entity
@Audited(withModifiedFlag=true)
@Where(clause ="del_Flag='N'" )
public class UserGroup extends AbstractEntity implements PrettySerializer {

    private String name;
    private String description;
    private Date dateCreated;


    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    private List<AdminUser> users;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    private List<BankUser> bankUsers;

    @JsonProperty
    @OneToMany(cascade = CascadeType.ALL)
    private List<Contact> contacts;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Code> custSeg;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }



    public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}


	public List<AdminUser> getUsers() {
		return users;
	}

	public void setUsers(List<AdminUser> users) {
		this.users = users;
	}


    public List<BankUser> getBankUsers() {
        return bankUsers;
    }

    public void setBankUsers(List<BankUser> bankUsers) {
        this.bankUsers = bankUsers;
    }

    public List<Code> getCustSeg() {
        return custSeg;
    }

    public void setCustSeg(List<Code> custSeg) {
        this.custSeg = custSeg;
    }

    @Override
    public JsonSerializer<UserGroup> getSerializer()
    {
        return new JsonSerializer<UserGroup>() {

            @Override
            public void serialize(UserGroup value, JsonGenerator gen, SerializerProvider arg2)
                    throws IOException {
                gen.writeStartObject();
                gen.writeStringField("Group Name", value.name);
                gen.writeStringField("Description",value.description);



                gen.writeObjectFieldStart("Internal Admin Users");
                for(AdminUser user : value.users){
                    gen.writeObjectFieldStart(user.getId().toString());
                    //gen.writeStartObject();
                    gen.writeStringField("First Name",user.firstName);
                    gen.writeStringField("Last Name",user.lastName);
                    gen.writeStringField("Email",user.email);
                    gen.writeEndObject();
                }
                gen.writeEndObject();

                gen.writeObjectFieldStart("Internal Bank Users");
                for(BankUser bankUser : value.bankUsers){
                    gen.writeObjectFieldStart(bankUser.getId().toString());
                    //gen.writeStartObject();
                    gen.writeStringField("First Name",bankUser.firstName);
                    gen.writeStringField("Last Name",bankUser.lastName);
                    gen.writeStringField("Email",bankUser.email);
                    gen.writeEndObject();
                }
                gen.writeEndObject();

                gen.writeObjectFieldStart("External Users");
                for(Contact contact : value.contacts){
                    gen.writeObjectFieldStart(contact.getId().toString());
                    //gen.writeStartObject();
                    gen.writeStringField("First Name",contact.firstName);
                    gen.writeStringField("Last Name",contact.lastName);
                    gen.writeStringField("Email",contact.email);
                    gen.writeEndObject();
                }
                gen.writeEndObject();

                gen.writeObjectFieldStart("Customer Segment");
                for(Code code : value.custSeg){
                    gen.writeObjectFieldStart(code.getId().toString());
                    //gen.writeStartObject();
                    gen.writeStringField("Customer Segment",code.getCode());
                    gen.writeEndObject();
                }
                gen.writeEndObject();
                //gen.writeEndArray();
                gen.writeEndObject();
            }
        };
    }


}
