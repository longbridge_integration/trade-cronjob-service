package cronjobservice.jobs.formnxpjobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

/**
 * Created by Longbridge on 9/12/2017.
 */
@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class FormNxpSubmitResponseJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextFormNxp = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextFormNxp.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.FORMNXP_SUBMIT_RESPONSE.toString(),cronJobService.getCurrentExpression(JobCategory.FORMNXP_SUBMIT_RESPONSE.toString()));
            cronJobService.getFormNxpResponse();

            cronJobService.updateRunningJob(JobCategory.FORMNXP_SUBMIT_RESPONSE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
