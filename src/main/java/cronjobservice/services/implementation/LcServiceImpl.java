package cronjobservice.services.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.FormNumber;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LcServiceImpl implements LcService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ModelMapper modelMapper;

    private Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    private MessageSource messageSource;

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    FormNxpRepo formNxpRepo;

//    @Autowired
//    private WorkFlowStatesService workFlowStatesService;

    @Autowired
    private WorkFlowRepo workFlowRepo;

    @Autowired
    private FormMRepo formMRepo;

    @Autowired
    private LcAmendRepo lcAmendRepo;

    @Autowired
    private LcConfirmationRepo lcConfirmationRepo;

    @Autowired
    private ImportLcRepo lcRepo;

    @Autowired
    private ImportLcRepo importLcRepo;

    @Autowired
    private ExportLcRepo exportLcRepo;

    @Autowired
    ServiceAuditRepo serviceAuditRepo;

//    @Autowired
//    private ImportDocumentRepo importDocumentRepo;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    CorporateRepo corporateRepo;

    @Autowired
    BranchRepo branchRepo;
//    @Autowired
//    CommentService commentService;
//    @Autowired
//    UserGroupService userGroupService;
//    @Autowired
    private RetailUserService retailUserService;
    @Autowired
    private CorporateService corporateService;
    @Autowired
    private AccountRepo accountRepo;
    @Autowired
    private CodeRepo codeRepo;
    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;

    @Autowired
    EntityManager entityManager;
    @Autowired
    MailService mailService;

//    @Value("${lc.upload.files}")
    private String UPLOADED_FOLDER;



    @Override
    public String saveLC(LCDTO lcdto, FormMDTO formMDTO) throws CronJobException
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;


        if ((loggedInUser.getUserType()).equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.BANK)) {
            customerId = lcdto.getCifid();
        }

        try {
            String generatedFormNumber = FormNumber.getFormNumber();

//            logger.info("getting the form number{}",generatedFormNumber);
//            if(lcdto.getDocuments() != null) {
//                for (LcDocumentDTO d : lcdto.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, generatedFormNumber, d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//            }


            SettingDTO wfSetting = configurationService.getSettingByName("DEF_LC_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            lcdto.setWorkFlowId(workFlow.getId());
            ImportLc  importLc = convertDTOToEntity(lcdto);
            FormM formM=formMRepo.findOne(formMDTO.getId());
            importLc.setUserType(loggedInUser.getUserType());
            importLc.setInitiatedBy(loggedInUser.getUserName());
            importLc.setDateCreated(new Date());
            importLc.setCifid(customerId);
            String lcNumber = FormNumber.getLcNumber();
            importLc.setLcNumber(lcNumber);
            importLc.setTempLcNumber(lcNumber);
            importLc.setFormM(formM);
            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
            importLc.setBankCode(bankCodeSetting.getValue());
            SettingDTO getBidRateCode = configurationService.getSettingByName("LC_RATE_CODE");
            importLc.setBidRateCode(getBidRateCode.getValue());

//            if(lcdto.getDocuments() != null) {
//
//                Iterator<LcDocument> DocumentIterator = importLc.getDocuments().iterator();
//                Iterator<LcDocumentDTO> DocumentDTOIterator = lcdto.getDocuments().iterator();
//
//                while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                    LcDocument document = DocumentIterator.next();
//                    LcDocumentDTO documentDTO = DocumentDTOIterator.next();
//                    if (documentDTO.getTypeOfDocument() == null) {
//                        DocumentIterator.remove();
//                        DocumentDTOIterator.remove();
//                    } else {
//                        document.setDocumentType(codeRepo.findByTypeAndCode("IMPORT_LC_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                        document.setImportLc(importLc);
//                    }
//                }
//            }
            importLcRepo.save(importLc);
            return messageSource.getMessage("lc.add.success", null, locale);
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("lc.add.failure", null, locale), e);
        }
    }


    @Override
    public Boolean sendImportLcITSmail(ImportLc importLc) {

        try {
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "Import Lc";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(importLc.getTempLcNumber(),RequestType.IMPORT_LC);
            Context context = new Context();
            context.setVariable("name", importLc.getApplicantName());
            context.setVariable("formNumber", importLc.getTempLcNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", DateFormatter.format(new Date()));
            context.setVariable("customerId", importLc.getCifid());
            context.setVariable("username", importLc.getInitiatedBy());
            context.setVariable("error", serviceAudit.getResponseMessage());
            context.setVariable("code", "IMPORT_LC");
            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
            Email email = new Email.Builder()
                    .setRecipients(allMails)
                    .setSubject(subject)
                    .setTemplate("mail/finacleSubmission")
                    .build();

            mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("guarantee.add.success", null, locale));
        }
    }

    @Override
    public Boolean sendExportLcITSmail(ExportLc exportLc) {

        try {
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "Export Lc";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(exportLc.getTempLcNumber(),RequestType.EXPORT_LC);
            Context context = new Context();
            context.setVariable("name", exportLc.getApplicantName());
            context.setVariable("formNumber", exportLc.getTempLcNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", DateFormatter.format(new Date()));
            context.setVariable("customerId", exportLc.getCifid());
            context.setVariable("username", exportLc.getInitiatedBy());
            context.setVariable("error", serviceAudit.getResponseMessage());
            context.setVariable("code", "EXPORT_LC");
            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
            Email email = new Email.Builder()
                    .setRecipients(allMails)
                    .setSubject(subject)
                    .setTemplate("mail/finacleSubmission")
                    .build();

            mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("guarantee.add.success", null, locale));
        }
    }


    @Override
    public String saveLC(LCDTO lcdto) throws CronJobException
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;

        logger.info("saving lc in process");
        if ((loggedInUser.getUserType()).equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.BANK)) {
            customerId = lcdto.getCifid();
        }



        try {
            String generatedFormNumber = FormNumber.getFormNumber();
//           logger.info("get the form number {}",lcdto.getDocuments());
//            if(lcdto.getDocuments() != null) {
//                for (LcDocumentDTO d : lcdto.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, generatedFormNumber, d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//            }

            SettingDTO wfSetting = configurationService.getSettingByName("DEF_LC_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            lcdto.setWorkFlowId(workFlow.getId());
            ImportLc importLc = convertDTOToEntity(lcdto);
//            logger.info("getting the formm{}",lcdto.getFormMid());
//            FormM formM=formMRepo.findOne(lcdto.getFormMid());
//            logger.info("getting the form M{}",formM);
            importLc.setUserType(loggedInUser.getUserType());
            importLc.setInitiatedBy(loggedInUser.getUserName());
//            lc.setDateOfExpiry(getFormExpiryDate());
            importLc.setDateCreated(new Date());
            importLc.setCifid(customerId);
            String lcNumber = FormNumber.getLcNumber();
            importLc.setLcNumber(lcNumber);
            importLc.setTempLcNumber(lcNumber);
//            importLc.setFormM(formM);
            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
            importLc.setBankCode(bankCodeSetting.getValue());
            SettingDTO getBidRateCode = configurationService.getSettingByName("LC_RATE_CODE");
            importLc.setBidRateCode(getBidRateCode.getValue());
            importLcRepo.save(importLc);
            return messageSource.getMessage("lc.add.success", null, locale);
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("lc.add.failure", null, locale), e);
        }
    }

    @Override
    public Boolean saveLCFinacle(LCDTO lcdto) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;

        logger.info("saving lc in process");
        if ((loggedInUser.getUserType()).equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.BANK)) {
            customerId = lcdto.getCifid();
        }


        try {
            String generatedFormNumber = FormNumber.getFormNumber();
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_LC_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            lcdto.setWorkFlowId(workFlow.getId());
            ImportLc importLc = convertDTOToEntity(lcdto);
//            logger.info("getting the formm{}",lcdto.getFormMid());
//            FormM formM=formMRepo.findOne(lcdto.getFormMid());
//            logger.info("getting the form M{}",formM);
            importLc.setUserType(loggedInUser.getUserType());
            importLc.setInitiatedBy(loggedInUser.getUserName());
//            lc.setDateOfExpiry(getFormExpiryDate());
            importLc.setDateCreated(new Date());
            importLc.setCifid(customerId);
            String lcNumber = FormNumber.getLcNumber();
            importLc.setLcNumber(lcNumber);
            importLc.setTempLcNumber(lcNumber);
//            importLc.setFormM(formM);
            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
            importLc.setBankCode(bankCodeSetting.getValue());
            SettingDTO getBidRateCode = configurationService.getSettingByName("LC_RATE_CODE");
            importLc.setBidRateCode(getBidRateCode.getValue());
            importLcRepo.save(importLc);
            return true;
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("lc.add.failure", null, locale), e);
        }

    }

    @Override
    public Boolean saveImportLCFinacle(ImportLcDTO importLcDTO) throws CronJobException {
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User loggedInUser = principal.getUser();
//        String customerId = null;

        logger.info("saving lc in process");

        try {
            String generatedFormNumber = FormNumber.getFormNumber();
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_LC_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            importLcDTO.setWorkFlowId(workFlow.getId());
            ImportLc importLc = modelMapper.map(importLcDTO, ImportLc.class);
            logger.info("getting the formm{}",importLcDTO.getFormMid());
            FormM formM=formMRepo.findOne(importLcDTO.getFormMid());
            logger.info("getting the form M{}",formM);
            importLc.setUserType(UserType.BANK);
            importLc.setInitiatedBy("FINACLE");
            importLc.setDateOfExpiry(getFormExpiryDate());
            importLc.setDateCreated(new Date());
            importLc.setCifid(importLcDTO.getCifid());
            String lcNumber = FormNumber.getLcNumber();
            importLc.setLcNumber(importLcDTO.getLcNumber());
            importLc.setTempLcNumber(lcNumber);
            importLc.setFormM(formM);
            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
            importLc.setBankCode(bankCodeSetting.getValue());
            SettingDTO getBidRateCode = configurationService.getSettingByName("LC_RATE_CODE");
            importLc.setBidRateCode(getBidRateCode.getValue());
            importLcRepo.save(importLc);
            return true;
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("lc.add.failure", null, locale), e);
        }

    }

    @Override
    public Boolean saveExportLCFinacle(ExportLcDTO exportLcDTO) throws CronJobException {

        logger.info("saving lc in process");

        try {
            String generatedFormNumber = FormNumber.getFormNumber();
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_LC_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            exportLcDTO.setWorkFlowId(workFlow.getId());
            ExportLc exportLc = modelMapper.map(exportLcDTO, ExportLc.class);
            exportLc.setUserType(UserType.BANK);
            exportLc.setInitiatedBy("FINACLE");
            exportLc.setDateOfExpiry(getFormExpiryDate());
            exportLc.setDateCreated(new Date());
            exportLc.setCifid(exportLcDTO.getCifid());
            String lcNumber = FormNumber.getLcNumber();
            exportLc.setLcNumber(exportLcDTO.getLcNumber());
            exportLc.setTempLcNumber(lcNumber);
            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
            exportLc.setBankCode(bankCodeSetting.getValue());
            SettingDTO getBidRateCode = configurationService.getSettingByName("LC_RATE_CODE");
            exportLc.setBidRateCode(getBidRateCode.getValue());
            exportLcRepo.save(exportLc);
            return true;
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("lc.add.failure", null, locale), e);
        }

    }

    @Override
    public Boolean updateImportFromFinacle(ImportLc tmsimportLc, ImportLc finimportLc) throws CronJobException {
        System.out.println("Out import lc got into the service");
        logger.info("IMPORT Lc response lc number {}", finimportLc.getLcNumber());
        logger.info("Import Lc response utilized amt {} ",finimportLc.getUtilizedAmount());

        try {
            tmsimportLc.setLcNumber(finimportLc.getLcNumber());
            tmsimportLc.setTranId(finimportLc.getTranId());
            tmsimportLc.setStatus("I");
            tmsimportLc.setToBeLinked("N");
            tmsimportLc.setUtilizedAmount(new BigDecimal(0));
            importLcRepo.save(tmsimportLc);

            FormM formM = tmsimportLc.getFormM();
            if (null!=finimportLc.getUtilizedAmount()) {
                logger.info("Import Lc response utilized amt 2 {} ",finimportLc.getUtilizedAmount());
                formM.setUtilization(finimportLc.getUtilizedAmount().toString());
                formMRepo.save(formM);
            }

            BigDecimal formAmount = tmsimportLc.getLcAmount();
            BigDecimal utilized = finimportLc.getUtilizedAmount();
            int i = formAmount.compareTo(utilized);
            if(i != 0){
                formM.setStatus("PU");
            }else{
                formM.setStatus("U");
            }

            formMRepo.save(formM);



            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }


    }

    @Override
    public Boolean updateExportFromFinacle(ExportLc tmsexportLc, ExportLc finexportLc) throws CronJobException {
        System.out.println("Out export lc got into the service");

        try {
            logger.info("Export LC No froom FInacle {} ", finexportLc.getLcNumber());
            tmsexportLc.setLcNumber(finexportLc.getLcNumber());
            tmsexportLc.setTranId(finexportLc.getTranId());
            tmsexportLc.setStatus("AD");
            tmsexportLc.setSubmitFlag("S");

            exportLcRepo.save(tmsexportLc);

            FormNxp formNxp = tmsexportLc.getFormNxp();
            formNxp.setUtilization(finexportLc.getUtilizedAmount().toString());
            formNxpRepo.save(formNxp);

//            logger.info("Getting Export Lc Amt {} ",tmsexportLc.getLcAmount());
//            logger.info("Getting FormNxp Utilization in Export LC",formNxp.getUtilization());
//                if(formNxp != null) {
//                    BigDecimal lcAmount = tmsexportLc.getLcAmount();
//                    BigDecimal existingNxpUtilization = new BigDecimal(0);
//                    if(null != formNxp.getUtilization() && !"".equalsIgnoreCase(formNxp.getUtilization())){
//                         existingNxpUtilization = new BigDecimal(formNxp.getUtilization());
//                    }
//                    BigDecimal updatedUtilization = lcAmount.add(existingNxpUtilization);
//                    formNxp.setUtilization(updatedUtilization.toString());
//
//                    formNxpRepo.save(formNxp);

//            if (formA.getUtilization().compareTo(formA.getAmount()) == -1){
//                formA.setStatus("PR");
//            }else {
//                formA.setStatus("R");
//            }
//                }
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

        @Override
        public Boolean updateExportPendingFromFinacle(ExportLc tmsexportLc, ExportLc finexportLc) throws CronJobException {
            System.out.println("Out export lc got into the service");

            try {
                logger.info("Export LC pending No froom FInacle {} ",finexportLc.getLcNumber());
                tmsexportLc.setLcNumber(finexportLc.getLcNumber());
                tmsexportLc.setTranId(finexportLc.getTranId());
                tmsexportLc.setStatus("PV");
                tmsexportLc.setSubmitFlag("S");

                exportLcRepo.save(tmsexportLc);

//                FormNxp formNxp = tmsexportLc.getFormNxp();
//                formNxp.setUtilization(finexportLc.getUtilizedAmount().toString());
//                formNxpRepo.save(formNxp);

                return true;
            } catch (CronJobException e) {

                throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
            }



        }

    @Override
    public Boolean updateExportPendingVerificationFromFinacle(ExportLc tmsexportLc, ExportLc finexportLc) throws CronJobException {
        System.out.println("Out export lc got into the service");

        try {
            logger.info("Export LC pending Verification update No froom FInacle {} ",finexportLc.getLcNumber());
            tmsexportLc.setLcNumber(finexportLc.getLcNumber());
            tmsexportLc.setTranId(finexportLc.getTranId());
            tmsexportLc.setStatus("PV");
            tmsexportLc.setSubmitFlag("S");

            exportLcRepo.save(tmsexportLc);

//            FormNxp formNxp = tmsexportLc.getFormNxp();
//            formNxp.setUtilization(finexportLc.getUtilizedAmount().toString());
//            formNxpRepo.save(formNxp);

            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }



    }


    @Override
    public Boolean changeExportLcToAdvisedFromFinacle(ExportLc tmsexportLc, ExportLc finexportLc) throws CronJobException {
        System.out.println("Out export lc got into the service");

        try {
            logger.info("Export LC pending Verification update to advised No froom FInacle {} ",finexportLc.getLcNumber());
            tmsexportLc.setLcNumber(finexportLc.getLcNumber());
            tmsexportLc.setTranId(finexportLc.getTranId());
            tmsexportLc.setStatus("AD");
            tmsexportLc.setSubmitFlag("S");
            tmsexportLc.setUtilizedAmount(new BigDecimal(0));

            exportLcRepo.save(tmsexportLc);

            FormNxp formNxp = tmsexportLc.getFormNxp();
            formNxp.setUtilization(finexportLc.getUtilizedAmount().toString());

            BigDecimal formAmount = tmsexportLc.getLcAmount();
            BigDecimal utilized = finexportLc.getUtilizedAmount();
            int i = formAmount.compareTo(utilized);
            if(i != 0){
                formNxp.setStatus("PU");
            }else{
                formNxp.setStatus("U");
            }

            formNxpRepo.save(formNxp);

            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }



    }

    @Override
    public Boolean saveExportLcAddedOnFinacle(ExportLc finexportLc, FormNxp formNxp) throws CronJobException {

        System.out.println("Out export lc got into the service");

        try {

            SettingDTO wfSetting = configurationService.getSettingByName("DEF_LC_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            logger.info("export lc workflow {} ",workFlow);
            String number = FormNumber.getExportLcNumber();
            finexportLc.setTempLcNumber(number);
            finexportLc.setStatus("AD");
            finexportLc.setWorkFlow(workFlow);
            finexportLc.setInitiatedBy("FINACLE");
            finexportLc.setSubmitFlag("S");
//            finexportLc.setCifid(lcDetails.getCifid());
            finexportLc.setUserType(UserType.BANK);
            finexportLc.setDateCreated(new Date());
            finexportLc.setDateOfExpiry(getFormExpiryDate());


            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
            finexportLc.setBankCode(bankCodeSetting.getValue());
            SettingDTO getBidRateCode = configurationService.getSettingByName("LC_RATE_CODE");
            finexportLc.setBidRateCode(getBidRateCode.getValue());
            exportLcRepo.save(finexportLc);



            formNxp.setUtilization(finexportLc.getUtilizedAmount().toString());

            BigDecimal formAmount = finexportLc.getLcAmount();
            BigDecimal utilized = finexportLc.getUtilizedAmount();
            int i = formAmount.compareTo(utilized);
            if(i != 0){
                formNxp.setStatus("PU");
            }else{
                formNxp.setStatus("U");
            }

            formNxpRepo.save(formNxp);

            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }



    }

    @Override
    public Boolean saveImportLcAddedOnFinacle(ImportLc finimportLc, FormM formM) throws CronJobException {

        try {

            SettingDTO wfSetting = configurationService.getSettingByName("DEF_LC_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            String number = FormNumber.getImportLcNumber();
            finimportLc.setTempLcNumber(number);
            finimportLc.setStatus("I");
            finimportLc.setWorkFlow(workFlow);
            finimportLc.setInitiatedBy("FINACLE");
            finimportLc.setSubmitFlag("S");
//            finimportLc.setCifid(lcDetails.getCifid());
            finimportLc.setUserType(UserType.BANK);
            finimportLc.setDateCreated(new Date());
            finimportLc.setDateOfExpiry(getFormExpiryDate());


            SettingDTO bankCodeSetting = configurationService.getSettingByName("BANK_CODE");
            finimportLc.setBankCode(bankCodeSetting.getValue());
            SettingDTO getBidRateCode = configurationService.getSettingByName("LC_RATE_CODE");
            finimportLc.setBidRateCode(getBidRateCode.getValue());

            importLcRepo.save(finimportLc);

            formM.setUtilization(finimportLc.getUtilizedAmount().toString());

            BigDecimal formAmount = finimportLc.getLcAmount();
            BigDecimal utilized = finimportLc.getUtilizedAmount();
            int i = formAmount.compareTo(utilized);
            if(i != 0){
                formM.setStatus("PU");
            }else{
                formM.setStatus("U");
            }

            formMRepo.save(formM);

            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }



    }


//    @Override
//    public BigDecimal getUtilizedFormMAmount(Long id,String cifid) throws CronJobException
//    {
//        FormM formM= formMRepo.findOne(id);
//        BigDecimal lc=importLcRepo.sumAmountInLC(cifid,formM);
//        logger.info("gettting the lc {}",lc);
//      BigDecimal lcTotalAmount=new BigDecimal("0");
//      if(!lc.isEmpty())
//      {
//          for(ImportLc importLc: lc)
//          {
//            BigDecimal amount=importLc.getLcAmount();
//            if(amount!=null)
//            {
//                lcTotalAmount.add(amount);
//            }
//
//          }
//      }
//
//
//        return "";
//    }


    @Override
    public BigDecimal getUtilizedFormMAmount(Long id, String cifid) throws CronJobException {
        return null;
    }

    @Override
    public LCDTO getInitiatorDetails(FormMDTO formMDTO) throws CronJobException {
        LCDTO lcdto=new LCDTO();
          String customerid= formMDTO.getCifid();
        Corporate corporate= corporateRepo.findByCustomerId(customerid);
        if(customerid!=null && corporate!=null)
          {
             lcdto.setApplicantName(corporate.getName());
             lcdto.setApplicantAddress(corporate.getAddress());
             lcdto.setApplicantCity(corporate.getTown());
             lcdto.setApplicantState(corporate.getState());
             lcdto.setApplicantCountry(corporate.getCountry());
             BigDecimal formMAmount=formMDTO.getFormAmount();
             String formMUtilizedAmount=formMDTO.getUtilization();
//             lcdto.setFormMutilizedAmount(formMUtilizedAmount);
             logger.info("getting the form m value {}",formMAmount);

//             lcdto.setFormMAmount(formMAmount);
//             lcdto.setForex(formMDTO.isForex());
             lcdto.setLcType("CN");
             BigDecimal utilizedAmount=new BigDecimal(formMUtilizedAmount);
              if(formMAmount!=null)
             {
                 BigDecimal value=new BigDecimal("110");
                 BigDecimal value2=new BigDecimal("100");
                 BigDecimal formMIncrease= (value.divide(value2)).multiply(formMAmount);//10% of the form m amount
                 logger.info("getting the form m value {}",formMIncrease);
                 BigDecimal currentFormMAmount=formMAmount.add(formMIncrease);

                     BigDecimal availableAmount=currentFormMAmount.subtract(utilizedAmount);
                     //current form m Amount

                 logger.info("getting the form m value {}",availableAmount);
//                 lcdto.setLcAvailableAmount(availableAmount);
              }

          }
        else
            {
            throw new CronJobException(messageSource.getMessage("lc.process.failure", null, locale));
        }

        return lcdto;


    }




    @Override
    public String addAttachments(MultipartFile file, String lcNumber, String documentType) {
        String uploadedFileName = "";


        if (file.isEmpty()) {
            logger.info("getting the file");
        }

        try {
            byte[] bytes = file.getBytes();
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User users = principal.getUser();

            if (users.getUserType().equals(UserType.CORPORATE)) {
                CorporateUser corporateUser = (CorporateUser) users;
                String username = corporateUser.getUserName();
                logger.info("corporate user {} with username {}", corporateUser, username);

                Path path = Paths.get(UPLOADED_FOLDER + lcNumber + documentType + file.getOriginalFilename());
                logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());


                File f = new File(path.toString());
                if(f.exists()){
                    logger.info("already exit");

                }else{
                    logger.info("does not exist");

                }

                Files.write(path, bytes);
                logger.info("file uploaded successfully to {} directory", path);
                uploadedFileName =file.getOriginalFilename();
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("failed to upload file");
        }

        logger.info("uploadFileName {}", uploadedFileName);
        if (StringUtils.isEmpty(uploadedFileName)) {
            logger.info("No file added, Please select a file to upload");

        } else {
            logger.info("You successfully uploaded {}", uploadedFileName);

        }
        return uploadedFileName;
    }


    private ImportLc convertDTOTOEntity(LCDTO lcdto) throws CronJobException {


        WorkFlow workFlow = workFlowRepo.findOne(lcdto.getWorkFlowId());
        lcdto.setWorkFlow(workFlow);
        ImportLc lc = modelMapper.map(lcdto, ImportLc.class);
        return lc;
    }




    @Override
    public String updateLC(LCDTO lcdto) throws CronJobException {
        try {
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User doneBy = principal.getUser();
            CorporateUser corporateUser = (CorporateUser)doneBy;
            ImportLc importLc = importLcRepo.findOne(lcdto.getId());


            if (doneBy == null) {
                throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
            }

            if (lcdto.getDocuments() != null) {
                for (LcDocumentDTO d : lcdto.getDocuments()) {
                    if (!(d.getFile().isEmpty())) {
                        MultipartFile file = d.getFile();
                        String filename = addAttachments(file, importLc.getTempLcNumber(), d.getTypeOfDocument());
                        d.setFileUpload(filename);
                    }
                }
            }
            entityManager.detach(importLc);
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_LC_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            lcdto.setWorkFlowId(workFlow.getId());
            ImportLc importLc1 = convertDTOTOEntity(lcdto);
            importLc1.setUserType(doneBy.getUserType());
            importLc1.setInitiatedBy(doneBy.getUserName());
            importLc1.setDateCreated(new Date());
            importLc1.setDateOfExpiry(getFormExpiryDate());
            importLc1.setCifid(corporateUser.getCorporate().getCustomerId());
            importLc1.setAllocatedAmount(new BigDecimal(0));

//            if (importLc1.getDocuments() != null) {
//                if (!"".equalsIgnoreCase(lcdto.getBranch()) && null != lcdto) {
//                    Branch branch = branchRepo.findOne(Long.parseLong(lcdto.getBranch()));
//                    importLc.setDocSubBranch(branch);
//                }
//
//                return messageSource.getMessage("lc.update.success", null, locale);
//            }
//            Iterator<LcDocument> DocumentIterator = importLc.getDocuments().iterator();
            Iterator<LcDocumentDTO> DocumentDTOIterator = lcdto.getDocuments().iterator();
            Collection<LcDocument> fields = new ArrayList<LcDocument>();
//            while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
////                LcDocument document = DocumentIterator.next();
//                LcDocumentDTO documentDTO = DocumentDTOIterator.next();
//                LcDocument lcDocument = null;
//                if (documentDTO.getTypeOfDocument() == null) {
////                    DocumentIterator.remove();
//                    DocumentDTOIterator.remove();
//                } else if (documentDTO.getId() == null) {
//                    lcDocument = new LcDocument();
//                    modelMapper.map(documentDTO, lcDocument);
//                    System.out.println("after model mapper");
//                    lcDocument.setDocumentType(codeRepo.findByTypeAndCode("IMPORT_LC_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                    lcDocument.setImportLc(importLc);
//                    System.out.println("newly added document ending");
//                } else {
//                    System.out.println("existing document");
////                    lcDocument = importDocumentRepo.findOne(documentDTO.getId());
//                    System.out.println("after finding existing document");
//                    lcDocument.setDocumentValue(documentDTO.getDocumentValue());
//                    lcDocument.setDocumentRemark(documentDTO.getDocumentRemark());
//                    lcDocument.setDocumentNumber(documentDTO.getDocumentNumber());
//                    lcDocument.setDocumentDate(documentDTO.getDocumentDate());
//                    if (documentDTO.getFileUpload() != null) {
//                        lcDocument.setFileUpload(documentDTO.getFileUpload());
//                    }
//                    lcDocument.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                    lcDocument.setImportLc(importLc);
//                    System.out.println("ending of  existing document");
//                }
//                fields.add(lcDocument);
//            }
            return messageSource.getMessage("lc.update.success", null, locale);

        }

        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("lc.update.failure", null, locale), e);

        }
    }

    @Override
    public String amendLC(LCDTO lcdto) throws CronJobException {
        try {

            LcAmend lcAmend = lcAmendRepo.getByLcNumber(lcdto.getLcNumber());
            if (lcAmend != null && ("S".equals(lcAmend.getStatus()) || "P".equals(lcAmend.getStatus()))){
                logger.error("Pending Amendment Request already exists");
                throw new CronJobException(messageSource.getMessage("lc.pending.amendment", null, locale));
            }

            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User doneBy = principal.getUser();

            LcAmend lc = convertLCDTOToAmendEntity(lcdto);
            lc.setUserType(doneBy.getUserType());
            lc.setInitiatedBy(doneBy.getUserName());
            lc.setDateCreated(new Date());
            lcAmendRepo.save(lc);
            logger.info("Amendment Submited for LC with Id {}", lc.getLcNumber());
            return messageSource.getMessage("state.update.success", null, locale);
        } catch (CronJobException e) {
            throw e;
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("lc.update.failure", null, locale), e);
        }
    }

    @Override
    public String confirmLC(LCDTO lcdto) throws CronJobException {
        try {

            LcConfirmation lcConfirmation = lcConfirmationRepo.getByLcNumber(lcdto.getLcNumber());
            if (lcConfirmation != null && ("S".equals(lcConfirmation.getStatus()) || "P".equals(lcConfirmation.getStatus()))){
                logger.error("Pending Confirmation Request already exists");
                throw new CronJobException(messageSource.getMessage("lc.pending.confirmation", null, locale));
            }

            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User doneBy = principal.getUser();

            LcConfirmation lc = convertLCDTOToConfirmationEntity(lcdto);
            lc.setUserType(doneBy.getUserType());
            lc.setInitiatedBy(doneBy.getUserName());
            lc.setDateCreated(new Date());
            lcConfirmationRepo.save(lc);
            logger.info("Confirmation Submited for LC with Id {}", lc.getLcNumber());
            return messageSource.getMessage("state.update.success", null, locale);
        } catch (CronJobException e) {
            throw e;
        } catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("lc.update.failure", null, locale), e);
        }
    }

    @Override
    public String processConfirmLC(LCDTO lcdto) throws CronJobException {
        try {

            LcConfirmation lcConfirmation = convertLCDTOToConfirmationEntity(lcdto);

            lcConfirmationRepo.save(lcConfirmation);
            return messageSource.getMessage("state.update.success", null, locale);
        }catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("lc.update.failure", null, locale), e);
        }
    }

    @Override
    public String processAmendLC(LCDTO lcdto) throws CronJobException {
        try {

            LcAmend lcAmend = convertLCDTOToAmendEntity(lcdto);
            lcAmendRepo.save(lcAmend);
            return messageSource.getMessage("state.update.success", null, locale);
        }catch (Exception e) {
            throw new CronJobException(messageSource.getMessage("lc.update.failure", null, locale), e);
        }
    }

    @Override
    public LCDTO getLC(Long id) throws CronJobException {
        ImportLc lc = lcRepo.findOne(id);
        return convertEntityToDTO(lc);
    }


    @Override
    public FormMDTO getFormM(Long id) throws CronJobException {

        ImportLc lc=lcRepo.findOne(id);
        return  convertEntityTODto(lc);
    }

    @Override
    public Page<LCDTO> getCustomerLcS(Pageable pageDetails) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;

        if ((loggedInUser.getUserType()).equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        }

        Page<ImportLc> page = lcRepo.findByCifidAndStatusNotNull(customerId, pageDetails);
        List<LCDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<LCDTO> pageImpl = new PageImpl<LCDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<LCDTO> getAllLcS(Pageable pageDetails) {
        Page<ImportLc> page = lcRepo.findByStatusNotNull(pageDetails);
        List<LCDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<LCDTO> pageImpl = new PageImpl<LCDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public List<CommentDTO> getLcComments(FormM  formId)
    {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        CorporateUser corporateUser=(CorporateUser) doneBy;
        String cifid=corporateUser.getCorporate().getCustomerId();
        List<ImportLc> lcList=lcRepo.findByFormMAndCifid(formId,cifid);
        List<Comments> allComment=new ArrayList<>();
        for(LC lc:lcList)
        {
            for(Comments comment :lc.getComment())
            {
                WorkFlowStates workFlowStates= workFlowStatesRepo.findByTypeAndCode(lc.getWorkFlow().getCodeType(),comment.getStatus());
                if(workFlowStates!=null)
                {
                  comment.setStatus( workFlowStates.getDescription());
                }
                allComment.add(comment);
            }
        }
        return  convertEntitiesToDTOsComments(allComment);
    }

    @Override
    public List<CommentDTO> getImportLcComments(ImportLc importLc) throws CronJobException {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        {
            commentDTOList=convertEntitiesToDTOsComments(importLc.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus()!="A");


        }
        return  commentDTOList;
    }

    @Override
    public List<CommentDTO> getExportLcComments(ExportLc exportLc) throws CronJobException {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        {
            commentDTOList=convertEntitiesToDTOsComments(exportLc.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus()!="A");


        }
        return  commentDTOList;
    }



    private List<CommentDTO> convertEntitiesToDTOsComments(Iterable<Comments> allcomments) throws CronJobException
    {
        List<CommentDTO> commentDTOList=new ArrayList<>();


        for(Comments comments: allcomments)
        {
            CommentDTO commentDTO = convertEntityTODTOComments(comments);
            commentDTOList.add(commentDTO);
        }

        return commentDTOList;
    }


    private CommentDTO convertEntityTODTOComments(Comments comments) throws CronJobException
    {

        return modelMapper.map(comments,CommentDTO.class);
    }


    @Override
    public Page<LCDTO> getOpenLcS(Pageable pageable) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;

        if ((loggedInUser.getUserType()).equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        }

        Page<ImportLc> pages = lcRepo.findByCifidAndStatus(customerId, "P", "S", "D", pageable);
        List<LCDTO> lcdtos = convertEntitiesToDTOs(pages.getContent());
        long t = pages.getTotalElements();
        Page<LCDTO> page1 = new PageImpl<LCDTO>(lcdtos, pageable, t);
        return page1;
    }


    @Override
    public FormHolder formHolder(LCDTO lcdto) throws CronJobException {
        try {
            FormHolder holder = new FormHolder();
            List<Comments> allComments;
            LC lc = convertDTOToEntity(lcdto);
//            Comments comments = commentService.addComment(lcdto.getComments(), "");
////            allComments = commentService.getAllComments();
//            allComments.add(comments);
//            lc.setComment(allComments);
            holder.setObjectData(objectMapper.writeValueAsString(lc));
            holder.setEntityId(lcdto.getId());
            holder.setEntityName("LC");
            holder.setAction(lcdto.getSubmitAction());
            return holder;

        } catch (JsonProcessingException e) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }
    }

    @Override
    public FormMDTO convertEntityTODto(ImportLc lc)
    {
        FormM formM=lc.getFormM();
        FormMDTO formMDTO=modelMapper.map(formM,FormMDTO.class);
        return formMDTO;
    }



    @Override
    public LCDTO convertEntityToDTO(ImportLc lc) {
        LCDTO lcdto = modelMapper.map(lc, LCDTO.class);
        if (lc.getWorkFlow() != null) {
            lcdto.setWorkFlowId(lc.getWorkFlow().getId());
//            WorkFlowStatesDTO workFlowState = workFlowStatesService.getByTypeAndCode(lc.getWorkFlow().getCodeType(), lc.getStatus());
//            lcdto.setStatusDesc(workFlowState.getDescription());
        }
        if (lc.getDateCreated() != null) {
            lcdto.setDate(DateFormatter.format(lc.getDateCreated()));
        }

        if (lc.getTransAccount() != null){
            lcdto.setAccountForTransId(lc.getTransAccount().getId().toString());
            lcdto.setAccountForTransNumber(lc.getTransAccount().getAccountNumber());
        }
        if (lc.getChargeAccount() != null){
            lcdto.setAccountToChargeId(lc.getChargeAccount().getId().toString());
            lcdto.setAccountToChargeNumber(lc.getChargeAccount().getAccountNumber());
        }
        if (lc.getLcType() != null){
            lcdto.setLcType(lc.getLcType().getCode());
            lcdto.setLcTypeName(lc.getLcType().getDescription());
        }

        if (lc.getLcPaymentTenor() != null){
            lcdto.setLcPaymentTenorId(lc.getLcPaymentTenor().getId().toString());
            lcdto.setLcPaymentTenor(lc.getLcPaymentTenor());
        }
        return lcdto;
    }

    @Override
    public ImportLcDTO convertEntityToImportLcDTO(ImportLc importLc) {
        ImportLcDTO importLcDTO = modelMapper.map(importLc, ImportLcDTO.class);
        if (importLc.getWorkFlow() != null) {
            importLcDTO.setWorkFlowId(importLc.getWorkFlow().getId());
//            WorkFlowStatesDTO workFlowState = workFlowStatesService.getByTypeAndCode(importLc.getWorkFlow().getCodeType(), importLc.getStatus());
//            importLcDTO.setStatusDesc(workFlowState.getDescription());
        }
        if (importLc.getDateCreated() != null) {
            importLcDTO.setDate(DateFormatter.format(importLc.getDateCreated()));
        }
//
//        if (importLc.getTransAccount() != null){
//            importLcDTO.setAccountForTransId(importLc.getTransAccount().getId().toString());
//            importLcDTO.setAccountForTransNumber(importLc.getTransAccount().getAccountNumber());
//        }
//
//
//        if (importLc.getChargeAccount() != null){
//            importLcDTO.setAccountToChargeId(importLc.getChargeAccount().getId().toString());
//            importLcDTO.setAccountToChargeNumber(importLc.getChargeAccount().getAccountNumber());
//        }
        if (importLc.getLcType() != null){
            importLcDTO.setLcType(importLc.getLcType().getCode());
            importLcDTO.setLcTypeName(importLc.getLcType().getDescription());
        }

//        if (importLc.getLcPaymentTenor() != null){
//            importLcDTO.setLcPaymentTenorId(importLc.getLcPaymentTenor().getId().toString());
//            importLcDTO.setLcPaymentTenor(importLc.getLcPaymentTenor());
//        }
        return importLcDTO;
    }

    @Override
    public ExportLcDTO convertEntityToExportLcDTO(ExportLc exportLc) {
        ExportLcDTO exportLcDTO = modelMapper.map(exportLc, ExportLcDTO.class);
        if (exportLc.getWorkFlow() != null) {
            exportLcDTO.setWorkFlowId(exportLc.getWorkFlow().getId());
//            WorkFlowStatesDTO workFlowState = workFlowStatesService.getByTypeAndCode(exportLc.getWorkFlow().getCodeType(), exportLc.getStatus());
//            exportLcDTO.setStatusDesc(workFlowState.getDescription());
        }
        if (exportLc.getDateCreated() != null) {
            exportLcDTO.setDate(DateFormatter.format(exportLc.getDateCreated()));
        }

//        if (exportLc.getTransAccount() != null){
//            exportLcDTO.setAccountForTransId(exportLc.getTransAccount().getId().toString());
//            exportLcDTO.setAccountForTransNumber(exportLc.getTransAccount().getAccountNumber());
//        }
//        if (exportLc.getChargeAccount() != null){
//            exportLcDTO.setAccountToChargeId(exportLc.getChargeAccount().getId().toString());
//            exportLcDTO.setAccountToChargeNumber(exportLc.getChargeAccount().getAccountNumber());
//        }
        if (exportLc.getLcType() != null){
            exportLcDTO.setLcType(exportLc.getLcType().getCode());
            exportLcDTO.setLcTypeName(exportLc.getLcType().getDescription());
        }

//        if (exportLc.getLcPaymentTenor() != null){
//            exportLcDTO.setLcPaymentTenorId(exportLc.getLcPaymentTenor().getId().toString());
//            exportLcDTO.setLcPaymentTenor(exportLc.getLcPaymentTenor());
//        }
        return exportLcDTO;
    }

    @Override
    public ImportLc convertDTOToEntity(LCDTO lcdto) {
        ImportLc importLc = modelMapper.map(lcdto, ImportLc.class);
        importLc.setWorkFlow(workFlowRepo.findOne(lcdto.getWorkFlowId()));

        if(!(lcdto.getBranch().isEmpty()))
        {
            Branch branch = branchRepo.findOne(Long.parseLong(lcdto.getBranch()));
            importLc.setDocSubBranch(branch);
        }

        if (lcdto.getAccountForTransId() != null && !"".equals(lcdto.getAccountForTransId())) {
            Account account = accountRepo.findOne(Long.parseLong(lcdto.getAccountForTransId()));
            importLc.setTransAccount(account);
        }
        if (lcdto.getAccountToChargeId() != null && !"".equals(lcdto.getAccountToChargeId())) {
            Account accountCharge = accountRepo.findOne(Long.parseLong(lcdto.getAccountToChargeId()));
            importLc.setChargeAccount(accountCharge);
        }
        if (lcdto.getLcType() != null && !"".equals(lcdto.getLcType())) {
            Code typeOfLc = codeRepo.findByTypeAndCode("LC_TYPES", lcdto.getLcType());
            importLc.setLcType(typeOfLc);
        }
        if (lcdto.getLcPaymentTenorId() != null && !"".equals(lcdto.getLcPaymentTenorId())) {
            Code lcTenor = codeRepo.findOne(Long.parseLong(lcdto.getLcPaymentTenorId()));
            importLc.setLcPaymentTenor(lcTenor);
        }

        try {
            if (lcdto.getExpiryDate() != null && !"".equals(lcdto.getExpiryDate())) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

                importLc.setDateOfExpiry(df.parse(lcdto.getExpiryDate()));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return importLc;
    }

    @Override
    public LcAmend convertLCDTOToAmendEntity(LCDTO lcdto) {
        LcAmend lc = modelMapper.map(lcdto, LcAmend.class);
        if (lcdto.getAccountToCharge() != null && !"".equals(lcdto.getAccountToChargeId())) {
            Account accountCharge = accountRepo.findOne(Long.parseLong(lcdto.getAccountToChargeId()));
            lc.setChargeAccount(accountCharge);
        }
        if (lcdto.getLcType() != null && !"".equals(lcdto.getLcType())) {
            Code typeOfLc = codeRepo.findByTypeAndCode("LC_TYPES", lcdto.getLcType());
            lc.setLcType(typeOfLc);
        }
        if (lcdto.getLcPaymentTenorId() != null && !"".equals(lcdto.getLcPaymentTenorId())) {
            Code lcTenor = codeRepo.findOne(Long.parseLong(lcdto.getLcPaymentTenorId()));
            lc.setLcPaymentTenor(lcTenor);
        }
        try {
            if (lcdto.getExpiryDate() != null && !"".equals(lcdto.getExpiryDate())) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                lc.setDateOfExpiry(df.parse(lcdto.getExpiryDate()));
            }
            if (lcdto.getInsurExpiryDate() != null && !"".equals(lcdto.getInsurExpiryDate())) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                lc.setInsurDateOfExpiry(df.parse(lcdto.getInsurExpiryDate()));
            }
            if (lcdto.getDate() != null && !"".equals(lcdto.getDate())) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                lc.setDateCreated(df.parse(lcdto.getDate()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return lc;
    }

    @Override
    public LCDTO convertAmendEntityToLCDTO(LcAmend lcAmend) {
        LCDTO lcdto = modelMapper.map(lcAmend, LCDTO.class);
        if ("P".equals(lcAmend.getStatus())){
            lcdto.setStatusDesc("Saved");
        }else if ("S".equals(lcAmend.getStatus())) {
            lcdto.setStatusDesc("Submitted");
        }else if ("A".equals(lcAmend.getStatus())) {
            lcdto.setStatusDesc("Approved");
        }else if ("D".equals(lcAmend.getStatus())) {
            lcdto.setStatusDesc("Declined");
        }

        if (lcAmend.getDateCreated() != null) {
            lcdto.setDate(DateFormatter.format(lcAmend.getDateCreated()));
        }
        if (lcAmend.getInsurDateOfExpiry() != null) {
            lcdto.setInsurExpiryDate(DateFormatter.format(lcAmend.getInsurDateOfExpiry()));
        }
        if (lcAmend.getTransAccount() != null){
            lcdto.setAccountForTransId(lcAmend.getTransAccount().getId().toString());
            lcdto.setAccountForTransNumber(lcAmend.getTransAccount().getAccountNumber());
        }
        if (lcAmend.getChargeAccount() != null){
            lcdto.setAccountToChargeId(lcAmend.getChargeAccount().getId().toString());
            lcdto.setAccountToChargeNumber(lcAmend.getChargeAccount().getAccountNumber());
        }
        if (lcAmend.getLcType() != null){
            lcdto.setLcType(lcAmend.getLcType().getCode());
            lcdto.setLcTypeName(lcAmend.getLcType().getDescription());
        }
        if (lcAmend.getLcPaymentTenor() != null){
            lcdto.setLcPaymentTenorId(lcAmend.getLcPaymentTenor().getId().toString());
            lcdto.setLcPaymentTenor(lcAmend.getLcPaymentTenor());
        }
        return lcdto;
    }

    @Override
    public List<LCDTO> convertAmendEntitiesToLCDTOs(Iterable<LcAmend> lcs) {
        List<LCDTO> lcdtoList = new ArrayList<>();
        for (LcAmend lc : lcs) {
            LCDTO lcdto = convertAmendEntityToLCDTO(lc);
            lcdtoList.add(lcdto);
        }
        return lcdtoList;
    }


    @Override
    public List<LCDTO> convertEntitiesToDTOs(Iterable<ImportLc> lcs) {
        List<LCDTO> lcdtoList = new ArrayList<>();
        for (ImportLc lc : lcs) {
            LCDTO lcdto = convertEntityToDTO(lc);
            lcdtoList.add(lcdto);
        }
        return lcdtoList;
    }

    private Date getFormExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        SettingDTO setting = configurationService.getSettingByName("LC_EXPIRY");
        if (setting != null && setting.isEnabled()) {
            int days = NumberUtils.toInt(setting.getValue());
            calendar.add(Calendar.DAY_OF_YEAR, days);
            return calendar.getTime();
        }
        return null;
    }

    public long getPage() {
        long totalDisplayPage = 0;
        try {
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            User doneby = principal.getUser();
            CorporateUser corporateUser = (CorporateUser) doneby;

            long number = lcRepo.countByCifid(corporateUser.getCorporate().getCustomerId());
            totalDisplayPage = number / 4;
            long extraPage = number % 4;
            if (extraPage > 0) {
                totalDisplayPage = totalDisplayPage + 1;
            }

        } catch (CronJobException t) {
            new CronJobException(messageSource.getMessage("pagination.add.failure", null, locale));
        }

        return totalDisplayPage;

    }

    @Override
    public boolean checkIfAmendable(Long id) {
        LC lc = lcRepo.findOne(id);
        if ("I".equals(lc.getStatus()) || "RA".equals(lc.getStatus())){
            List<LcAmend> lcAmendList = lcAmendRepo.findByLcNumber(lc.getLcNumber());

            for(LcAmend lcAmend : lcAmendList){
                if ("S".equals(lcAmend.getStatus())){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean canConfirmationBeAdded(Long id) {
        LC lc = lcRepo.findOne(id);
        logger.info("LC_CONF {}", lc);
        if (!"CN".equals(lc.getLcType().getCode()) && "C".equals(lc.getStatus())){
            List<LcConfirmation> lcConfirmationList = lcConfirmationRepo.findByLcNumber(lc.getLcNumber());

            for(LcConfirmation lcConfirmation : lcConfirmationList){
                if ("S".equals(lcConfirmation.getStatus())){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean canReinstateLC(Long id) {
        LC lc = lcRepo.findOne(id);
        if (lc.getRevolving() && ("I".equals(lc.getStatus()) || "RA".equals(lc.getStatus()))){
            SettingDTO settingDTO = configurationService.getSettingByName("MAX_REINSTATEMENTS");
            int max = Integer.parseInt(settingDTO.getValue());

//         //   FormM formM = lc.getFormM();
//            logger.info("REINSTATE {}", formM);
//            int noOfLcs = lcRepo.countByFormM(formM);
//            logger.info("REINSTATE {}", noOfLcs);
//            return noOfLcs < max;
        }
        return false;
    }

    @Override
    public LCDTO getLcAmendment(Long id) throws CronJobException {
        LcAmend lc = lcAmendRepo.findOne(id);
        return convertAmendEntityToLCDTO(lc);
    }

    @Override
    public LCDTO getLcConfirmation(Long id) throws CronJobException {
        LcConfirmation lc = lcConfirmationRepo.findOne(id);
        return convertConfirmationEntityToLCDTO(lc);
    }

    @Override
    public Page<LCDTO> getLcAmendments(String lcNumber, Pageable pageDetails) {
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        User doneby = principal.getUser();
//        CorporateUser corporateUser = (CorporateUser) doneby;
        LC lc = lcRepo.findByLcNumber(lcNumber);
        logger.info("lcNumber is {}", lcNumber);
        Page<LcAmend> page = lcAmendRepo.findByLcNumberAndCifid(lcNumber, lc.getCifid(), pageDetails);
        List<LCDTO> dtOs = convertAmendEntitiesToLCDTOs(page);
        long t = page.getTotalElements();
        Page<LCDTO> pageImpl = new PageImpl<LCDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }



    @Override
    public LCDTO convertConfirmationEntityToLCDTO(LcConfirmation lcConfirmation) {
        LCDTO lcdto = modelMapper.map(lcConfirmation, LCDTO.class);
        if ("P".equals(lcConfirmation.getStatus())){
            lcdto.setStatusDesc("Saved");
        }else if ("S".equals(lcConfirmation.getStatus())) {
            lcdto.setStatusDesc("Submitted");
        }else if ("A".equals(lcConfirmation.getStatus())) {
            lcdto.setStatusDesc("Approved");
        }else if ("D".equals(lcConfirmation.getStatus())) {
            lcdto.setStatusDesc("Declined");
        }

        if (lcConfirmation.getDateCreated() != null) {
            lcdto.setDate(DateFormatter.format(lcConfirmation.getDateCreated()));
        }

        return lcdto;
    }

    @Override
    public List<LCDTO> convertConfirmationEntitiesToLCDTOs(Iterable<LcConfirmation> lcs) {
        List<LCDTO> lcdtoList = new ArrayList<>();
        for (LcConfirmation lc : lcs) {
            LCDTO lcdto = convertConfirmationEntityToLCDTO(lc);
            lcdtoList.add(lcdto);
        }
        return lcdtoList;
    }

    @Override
    public LcConfirmation convertLCDTOToConfirmationEntity(LCDTO lcdto) {
        LcConfirmation lc = modelMapper.map(lcdto, LcConfirmation.class);
        if (lcdto.getAccountToCharge() != null && !"".equals(lcdto.getAccountToChargeId())) {
            Account accountCharge = accountRepo.findOne(Long.parseLong(lcdto.getAccountToChargeId()));
            lc.setChargeAccount(accountCharge);
        }
        try {
            if (lcdto.getDate() != null && !"".equals(lcdto.getDate())) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                lc.setDateCreated(df.parse(lcdto.getDate()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return lc;
    }

    @Override
    public Page<LCDTO> getLcConfirmations(String lcNumber, Pageable pageDetails) {

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User doneby = principal.getUser();
        //CorporateUser corporateUser = (CorporateUser) doneby;
        LC lc = lcRepo.findByLcNumber(lcNumber);

        logger.info("lcNumber is {}", lcNumber);
        Page<LcConfirmation> page = lcConfirmationRepo.findByLcNumberAndCifid(lcNumber, lc.getCifid(), pageDetails);
        List<LCDTO> dtOs = convertConfirmationEntitiesToLCDTOs(page);
        long t = page.getTotalElements();
        Page<LCDTO> pageImpl = new PageImpl<LCDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    //Getting Information for Import Bill
    @Override
    public Page<ImportLc> getApprovedImportLcDetails(Pageable pageable) throws CronJobException {
        Page<ImportLc> formDetails = null;
        formDetails =  lcRepo.findFormByStatus("I",pageable);
        logger.info("formDetails",formDetails);
        return formDetails;
    }

    @Override
    public ImportLc getLcDetails(Long id) throws CronJobException {
        ImportLc lc=lcRepo.findOne(id);
        logger.info("id lc {}",lc);
        return lc;

    }

    //Getting Details for Import Bills
    @Override
    public ImportLc getDetailForImportLc(String lcNumber) throws CronJobException {
        ImportLc lc=lcRepo.findByLcNumber(lcNumber);
        logger.info("id lc {}",lc);
        return  lc;
    }

    @Override
    public Page<LCDTO> findAllLcs(String pattern, Pageable pageDetails) {
        Page<ImportLc> page = importLcRepo.findUsingPattern(pattern, pageDetails);
        List<LCDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        List<LCDTO> lcdtoList = new ArrayList<LCDTO>();
        for(LCDTO lcdto : dtOs){
            if(!lcdto.getStatus().equals("P") && !lcdto.getStatus().equals("PD") && !lcdto.getStatus().equals("PA")){
                lcdtoList.add(lcdto);
            }
        }
        long t = lcdtoList.size();
        Page<LCDTO> pageImpl = new PageImpl<LCDTO>(lcdtoList, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<LCDTO> getAllLcs(Pageable pageDetails) {
        Page<ImportLc> page = importLcRepo.findByStatus("P", "PD", "PA", pageDetails);
        List<LCDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        List<LCDTO> sortedList = dtOs.stream().sorted(Comparator.comparing(LCDTO::getDateCreated).reversed()).collect(Collectors.toList());
        long t = page.getTotalElements();
        Page<LCDTO> pageImpl = new PageImpl<LCDTO>(sortedList, pageDetails, t);
        return pageImpl;
    }

}
