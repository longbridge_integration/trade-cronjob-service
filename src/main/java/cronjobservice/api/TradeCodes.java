package cronjobservice.api;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TradeCodes {

    private String code;
    private String codeDescription;
    private String swiftCode;
    private String bankName;

    public TradeCodes(){

    }

    public TradeCodes(String code, String codeDescription, String swiftCode, String bankName) {
        this.code = code;
        this.codeDescription = codeDescription;
        this.swiftCode = swiftCode;
        this.bankName = bankName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDescription() {
        return codeDescription;
    }

    public void setCodeDescription(String codeDescription) {
        this.codeDescription = codeDescription;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public String toString() {
        return "TradeCodes{" +
                "code='" + code + '\'' +
                ", codeDescription='" + codeDescription + '\'' +
                ", swiftCode='" + swiftCode + '\'' +
                ", bankName='" + bankName + '\'' +
                '}';
    }
}
