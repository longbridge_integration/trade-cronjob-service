package cronjobservice.jobs.formajobs;


import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * Created by Longbridge on 9/12/2017.
 */
@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class FormASubmitJob implements Job {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextFormA = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextFormA.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.SUBMIT_FORMA.toString(),cronJobService.getCurrentExpression(JobCategory.SUBMIT_FORMA.toString()));
            logger.info("cron job monitor");
            cronJobService.submitFormAToFinacle();
            cronJobService.updateRunningJob(JobCategory.SUBMIT_FORMA.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
