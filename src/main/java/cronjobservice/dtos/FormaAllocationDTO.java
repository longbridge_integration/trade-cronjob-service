package cronjobservice.dtos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by JUDE on 1/26/2018.
 */
public class FormaAllocationDTO {
    private Long id;
    private String formaNumber;
    private String allocationAccount;
    private String allocationCurrency;
    private BigDecimal allocationAmount;
    private String exchangeRate;
    private Date dateOfAllocation;
    private String dealId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFormaNumber() {
        return formaNumber;
    }

    public void setFormaNumber(String formaNumber) {
        this.formaNumber = formaNumber;
    }

    public String getAllocationAccount() {
        return allocationAccount;
    }

    public void setAllocationAccount(String allocationAccount) {
        this.allocationAccount = allocationAccount;
    }

    public String getAllocationCurrency() {
        return allocationCurrency;
    }

    public void setAllocationCurrency(String allocationCurrency) {
        this.allocationCurrency = allocationCurrency;
    }

    public BigDecimal getAllocationAmount() {
        return allocationAmount;
    }

    public void setAllocationAmount(BigDecimal allocationAmount) {
        this.allocationAmount = allocationAmount;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Date getDateOfAllocation() {
        return dateOfAllocation;
    }

    public void setDateOfAllocation(Date dateOfAllocation) {
        this.dateOfAllocation = dateOfAllocation;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }
}
