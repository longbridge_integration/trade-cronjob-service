package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Where(clause ="del_Flag='N'" )
@Audited(withModifiedFlag=true)
public class FormQAllocation extends  Process{

    private String formNumber;
    private String allocationAccount;
    private String allocationCurrency;
    private BigDecimal allocationAmount;
    private String exchangeRate;
    private Date dateOfAllocation;
    private String dealId;

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public String getAllocationAccount() {
        return allocationAccount;
    }

    public void setAllocationAccount(String allocationAccount) {
        this.allocationAccount = allocationAccount;
    }

    public String getAllocationCurrency() {
        return allocationCurrency;
    }

    public void setAllocationCurrency(String allocationCurrency) {
        this.allocationCurrency = allocationCurrency;
    }

    public BigDecimal getAllocationAmount() {
        return allocationAmount;
    }

    public void setAllocationAmount(BigDecimal allocationAmount) {
        this.allocationAmount = allocationAmount;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Date getDateOfAllocation() {
        return dateOfAllocation;
    }

    public void setDateOfAllocation(Date dateOfAllocation) {
        this.dateOfAllocation = dateOfAllocation;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }
}
