package cronjobservice.jobs.ptajobs;


import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.services.PtaService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class PtaSubmitResponseJob  implements Job{


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextLc = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextLc.getBean (CronJobService.class);
        PtaService ptaService = contextLc.getBean (PtaService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.PTA_SUBMIT_RESPONSE.toString(),cronJobService.getCurrentExpression(JobCategory.PTA_SUBMIT_RESPONSE.toString()));

            ptaService.getPtaSubmitResponse();
            cronJobService.updateRunningJob(JobCategory.PTA_SUBMIT_RESPONSE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
