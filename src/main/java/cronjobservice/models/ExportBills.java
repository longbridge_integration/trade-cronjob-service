package cronjobservice.models;


import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Inheritance
@DiscriminatorColumn(name="type")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class ExportBills extends Process{

    private double billExchangeRate;
    //IMPORTER DETAILS
    private String importerName;
    private String importerAddress;
    private String importerTown;
    private String importerState;
    private String importerCountry;
    private String otherBankRefNo;

    private UserType userType;
    private String InitiatedBy;
    private String approvedBy;
    private String customerName;
    private Date dateCreated;
    @OneToOne
    private Branch docSubBranch;
    private boolean guidelineApproval;

    //BILL DETAILS
    private String operativeAcct;
    private String chargeAcct;
    private BigDecimal billAmount;
    private String billCurrency;
    private String billCountry;
    private String invoiceNumber;
    private String invoiceAmtCurrency;
    private BigDecimal invoiceAmount;
    private String billNumber;
    private String temporalBllNumb;

    //Date NOTE THAT
    private String onBoardOn;
    private String billOn;
    private String acceptanceOn;
    private String dueOn;
    private String policyOn;
    private String documentOn;
    private String invoiceOn;

    //TENOR DETAILS
    private String billTenor;
    private String fixedTransitPeriod;
    private String tenorPeriod;
    private String gracePeriod;
    private String dueDateIndicator;

    private BigDecimal otherBankCharges;

    //INSURANCE DETAILS
    private double usuancePercentage;
    private BigDecimal usuanceAmount;
    private String insuranceAmountCurrency;
    private String usuanceBy;
    private String policyNumber;
    private String usuanceCompany;
    private String payableAt;
    private String premiumAmtCurrency;
    private BigDecimal premimumAmount;

    //  CORRESPONDING BANK DETAILS
    private String corrBnkSwiftCode;
    private String corrBnkName;
    private String corrBankCountry;
    private String corrBankState;
    private String corrBankCity;
    private String corrBankAddress;
    private String corrBankIdentifier;
    private String nostroAcctNo;

    //  COLLECTING BANK DETAILS
    private String coLLBnkSwiftCode;
    private String coLLBnkName;
    private String coLLBankCountry;
    private String coLLBankState;
    private String coLLBankAddress;
    private String coLLBankCity;
    private String coLLBankIdentifier;

    //  DRAWEE BANK DETAILS
    private String draweeBnkSwift;
    private String draweeBnkName;
    private String draweeBnkCountry;
    private String draweeBnkState;
    private String draweeBnkAddress;
    private String draweeBnkCity;
    private String draweeBnkIdent;

    //BENEFICIARY BANK NAME
    private String beneBankName;
    private String beneBankAddr;
    private String beneBankSwiftCode;
    private String beneBankCtry;
    private String beneBankCity;
    private String beneBankState;
    private String beneBankIdent;

    //DISCREPANT
    private String descripantStatus;
    private String descripantComment;
    private String discrepantMsg;

    private String vesselName;
    private String portOfDischarge;
    private BigDecimal freightAmount;
    private String freightAmtCurrency;


    //DISCOUNT/ADVANCE DETAILS
    private BigDecimal discountValue;
    private String discount;
    private String dicountValCurrency;
    private BigDecimal proposedDiscountVal;
    private String discountStatus;
    private BigDecimal advanceBillAmt;
    private BigDecimal utilizedAmount;
    private String advanceBillMessage;



    public String getImporterName() {
        return importerName;
    }

    public void setImporterName(String importerName) {
        this.importerName = importerName;
    }

    public String getImporterAddress() {
        return importerAddress;
    }

    public void setImporterAddress(String importerAddress) {
        this.importerAddress = importerAddress;
    }

    public String getImporterTown() {
        return importerTown;
    }

    public void setImporterTown(String importerTown) {
        this.importerTown = importerTown;
    }

    public String getImporterState() {
        return importerState;
    }

    public void setImporterState(String importerState) {
        this.importerState = importerState;
    }

    public String getImporterCountry() {
        return importerCountry;
    }

    public void setImporterCountry(String importerCountry) {
        this.importerCountry = importerCountry;
    }

    public String getOtherBankRefNo() {
        return otherBankRefNo;
    }

    public void setOtherBankRefNo(String otherBankRefNo) {
        this.otherBankRefNo = otherBankRefNo;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getInitiatedBy() {
        return InitiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        InitiatedBy = initiatedBy;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Branch getDocSubBranch() {
        return docSubBranch;
    }

    public void setDocSubBranch(Branch docSubBranch) {
        this.docSubBranch = docSubBranch;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public String getOperativeAcct() {
        return operativeAcct;
    }

    public void setOperativeAcct(String operativeAcct) {
        this.operativeAcct = operativeAcct;
    }

    public String getChargeAcct() {
        return chargeAcct;
    }

    public void setChargeAcct(String chargeAcct) {
        this.chargeAcct = chargeAcct;
    }

    public BigDecimal getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }

    public String getBillCurrency() {
        return billCurrency;
    }

    public void setBillCurrency(String billCurrency) {
        this.billCurrency = billCurrency;
    }

    public String getBillCountry() {
        return billCountry;
    }

    public void setBillCountry(String billCountry) {
        this.billCountry = billCountry;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceAmtCurrency() {
        return invoiceAmtCurrency;
    }

    public void setInvoiceAmtCurrency(String invoiceAmtCurrency) {
        this.invoiceAmtCurrency = invoiceAmtCurrency;
    }

    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(BigDecimal invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getTemporalBllNumb() {
        return temporalBllNumb;
    }

    public void setTemporalBllNumb(String temporalBllNumb) {
        this.temporalBllNumb = temporalBllNumb;
    }

    public String getOnBoardOn() {
        return onBoardOn;
    }

    public void setOnBoardOn(String onBoardOn) {
        this.onBoardOn = onBoardOn;
    }

    public String getBillOn() {
        return billOn;
    }

    public void setBillOn(String billOn) {
        this.billOn = billOn;
    }

    public String getAcceptanceOn() {
        return acceptanceOn;
    }

    public void setAcceptanceOn(String acceptanceOn) {
        this.acceptanceOn = acceptanceOn;
    }

    public String getDueOn() {
        return dueOn;
    }

    public void setDueOn(String dueOn) {
        this.dueOn = dueOn;
    }

    public String getPolicyOn() {
        return policyOn;
    }

    public void setPolicyOn(String policyOn) {
        this.policyOn = policyOn;
    }

    public String getDocumentOn() {
        return documentOn;
    }

    public void setDocumentOn(String documentOn) {
        this.documentOn = documentOn;
    }

    public String getInvoiceOn() {
        return invoiceOn;
    }

    public void setInvoiceOn(String invoiceOn) {
        this.invoiceOn = invoiceOn;
    }

    public String getBillTenor() {
        return billTenor;
    }

    public void setBillTenor(String billTenor) {
        this.billTenor = billTenor;
    }

    public String getFixedTransitPeriod() {
        return fixedTransitPeriod;
    }

    public void setFixedTransitPeriod(String fixedTransitPeriod) {
        this.fixedTransitPeriod = fixedTransitPeriod;
    }

    public String getTenorPeriod() {
        return tenorPeriod;
    }

    public void setTenorPeriod(String tenorPeriod) {
        this.tenorPeriod = tenorPeriod;
    }

    public String getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(String gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public String getDueDateIndicator() {
        return dueDateIndicator;
    }

    public void setDueDateIndicator(String dueDateIndicator) {
        this.dueDateIndicator = dueDateIndicator;
    }

    public BigDecimal getOtherBankCharges() {
        return otherBankCharges;
    }

    public void setOtherBankCharges(BigDecimal otherBankCharges) {
        this.otherBankCharges = otherBankCharges;
    }

    public double getUsuancePercentage() {
        return usuancePercentage;
    }

    public void setUsuancePercentage(double usuancePercentage) {
        this.usuancePercentage = usuancePercentage;
    }

    public BigDecimal getUsuanceAmount() {
        return usuanceAmount;
    }

    public void setUsuanceAmount(BigDecimal usuanceAmount) {
        this.usuanceAmount = usuanceAmount;
    }

    public String getInsuranceAmountCurrency() {
        return insuranceAmountCurrency;
    }

    public void setInsuranceAmountCurrency(String insuranceAmountCurrency) {
        this.insuranceAmountCurrency = insuranceAmountCurrency;
    }

    public String getUsuanceBy() {
        return usuanceBy;
    }

    public void setUsuanceBy(String usuanceBy) {
        this.usuanceBy = usuanceBy;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getUsuanceCompany() {
        return usuanceCompany;
    }

    public void setUsuanceCompany(String usuanceCompany) {
        this.usuanceCompany = usuanceCompany;
    }

    public String getPayableAt() {
        return payableAt;
    }

    public void setPayableAt(String payableAt) {
        this.payableAt = payableAt;
    }

    public String getPremiumAmtCurrency() {
        return premiumAmtCurrency;
    }

    public void setPremiumAmtCurrency(String premiumAmtCurrency) {
        this.premiumAmtCurrency = premiumAmtCurrency;
    }

    public BigDecimal getPremimumAmount() {
        return premimumAmount;
    }

    public void setPremimumAmount(BigDecimal premimumAmount) {
        this.premimumAmount = premimumAmount;
    }

    public String getCorrBnkSwiftCode() {
        return corrBnkSwiftCode;
    }

    public void setCorrBnkSwiftCode(String corrBnkSwiftCode) {
        this.corrBnkSwiftCode = corrBnkSwiftCode;
    }

    public String getCorrBnkName() {
        return corrBnkName;
    }

    public void setCorrBnkName(String corrBnkName) {
        this.corrBnkName = corrBnkName;
    }

    public String getCorrBankCountry() {
        return corrBankCountry;
    }

    public void setCorrBankCountry(String corrBankCountry) {
        this.corrBankCountry = corrBankCountry;
    }

    public String getCorrBankState() {
        return corrBankState;
    }

    public void setCorrBankState(String corrBankState) {
        this.corrBankState = corrBankState;
    }

    public String getCorrBankCity() {
        return corrBankCity;
    }

    public void setCorrBankCity(String corrBankCity) {
        this.corrBankCity = corrBankCity;
    }

    public String getCorrBankAddress() {
        return corrBankAddress;
    }

    public void setCorrBankAddress(String corrBankAddress) {
        this.corrBankAddress = corrBankAddress;
    }

    public String getCorrBankIdentifier() {
        return corrBankIdentifier;
    }

    public void setCorrBankIdentifier(String corrBankIdentifier) {
        this.corrBankIdentifier = corrBankIdentifier;
    }

    public String getNostroAcctNo() {
        return nostroAcctNo;
    }

    public void setNostroAcctNo(String nostroAcctNo) {
        this.nostroAcctNo = nostroAcctNo;
    }

    public String getCoLLBnkSwiftCode() {
        return coLLBnkSwiftCode;
    }

    public void setCoLLBnkSwiftCode(String coLLBnkSwiftCode) {
        this.coLLBnkSwiftCode = coLLBnkSwiftCode;
    }

    public String getCoLLBnkName() {
        return coLLBnkName;
    }

    public void setCoLLBnkName(String coLLBnkName) {
        this.coLLBnkName = coLLBnkName;
    }

    public String getCoLLBankCountry() {
        return coLLBankCountry;
    }

    public void setCoLLBankCountry(String coLLBankCountry) {
        this.coLLBankCountry = coLLBankCountry;
    }

    public String getCoLLBankState() {
        return coLLBankState;
    }

    public void setCoLLBankState(String coLLBankState) {
        this.coLLBankState = coLLBankState;
    }

    public String getCoLLBankAddress() {
        return coLLBankAddress;
    }

    public void setCoLLBankAddress(String coLLBankAddress) {
        this.coLLBankAddress = coLLBankAddress;
    }

    public String getCoLLBankCity() {
        return coLLBankCity;
    }

    public void setCoLLBankCity(String coLLBankCity) {
        this.coLLBankCity = coLLBankCity;
    }

    public String getCoLLBankIdentifier() {
        return coLLBankIdentifier;
    }

    public void setCoLLBankIdentifier(String coLLBankIdentifier) {
        this.coLLBankIdentifier = coLLBankIdentifier;
    }

    public String getDraweeBnkSwift() {
        return draweeBnkSwift;
    }

    public void setDraweeBnkSwift(String draweeBnkSwift) {
        this.draweeBnkSwift = draweeBnkSwift;
    }

    public String getDraweeBnkName() {
        return draweeBnkName;
    }

    public void setDraweeBnkName(String draweeBnkName) {
        this.draweeBnkName = draweeBnkName;
    }

    public String getDraweeBnkCountry() {
        return draweeBnkCountry;
    }

    public void setDraweeBnkCountry(String draweeBnkCountry) {
        this.draweeBnkCountry = draweeBnkCountry;
    }

    public String getDraweeBnkState() {
        return draweeBnkState;
    }

    public void setDraweeBnkState(String draweeBnkState) {
        this.draweeBnkState = draweeBnkState;
    }

    public String getDraweeBnkAddress() {
        return draweeBnkAddress;
    }

    public void setDraweeBnkAddress(String draweeBnkAddress) {
        this.draweeBnkAddress = draweeBnkAddress;
    }

    public String getDraweeBnkCity() {
        return draweeBnkCity;
    }

    public void setDraweeBnkCity(String draweeBnkCity) {
        this.draweeBnkCity = draweeBnkCity;
    }

    public String getDraweeBnkIdent() {
        return draweeBnkIdent;
    }

    public void setDraweeBnkIdent(String draweeBnkIdent) {
        this.draweeBnkIdent = draweeBnkIdent;
    }

    public String getBeneBankName() {
        return beneBankName;
    }

    public void setBeneBankName(String beneBankName) {
        this.beneBankName = beneBankName;
    }

    public String getBeneBankAddr() {
        return beneBankAddr;
    }

    public void setBeneBankAddr(String beneBankAddr) {
        this.beneBankAddr = beneBankAddr;
    }

    public String getBeneBankSwiftCode() {
        return beneBankSwiftCode;
    }

    public void setBeneBankSwiftCode(String beneBankSwiftCode) {
        this.beneBankSwiftCode = beneBankSwiftCode;
    }

    public String getBeneBankCtry() {
        return beneBankCtry;
    }

    public void setBeneBankCtry(String beneBankCtry) {
        this.beneBankCtry = beneBankCtry;
    }

    public String getBeneBankCity() {
        return beneBankCity;
    }

    public void setBeneBankCity(String beneBankCity) {
        this.beneBankCity = beneBankCity;
    }

    public String getBeneBankState() {
        return beneBankState;
    }

    public void setBeneBankState(String beneBankState) {
        this.beneBankState = beneBankState;
    }

    public String getBeneBankIdent() {
        return beneBankIdent;
    }

    public void setBeneBankIdent(String beneBankIdent) {
        this.beneBankIdent = beneBankIdent;
    }

    public String getDescripantStatus() {
        return descripantStatus;
    }

    public void setDescripantStatus(String descripantStatus) {
        this.descripantStatus = descripantStatus;
    }

    public String getDescripantComment() {
        return descripantComment;
    }

    public void setDescripantComment(String descripantComment) {
        this.descripantComment = descripantComment;
    }

    public String getDiscrepantMsg() {
        return discrepantMsg;
    }

    public void setDiscrepantMsg(String discrepantMsg) {
        this.discrepantMsg = discrepantMsg;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }

    public BigDecimal getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(BigDecimal freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getFreightAmtCurrency() {
        return freightAmtCurrency;
    }

    public void setFreightAmtCurrency(String freightAmtCurrency) {
        this.freightAmtCurrency = freightAmtCurrency;
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(BigDecimal discountValue) {
        this.discountValue = discountValue;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDicountValCurrency() {
        return dicountValCurrency;
    }

    public void setDicountValCurrency(String dicountValCurrency) {
        this.dicountValCurrency = dicountValCurrency;
    }

    public BigDecimal getProposedDiscountVal() {
        return proposedDiscountVal;
    }

    public void setProposedDiscountVal(BigDecimal proposedDiscountVal) {
        this.proposedDiscountVal = proposedDiscountVal;
    }

    public String getDiscountStatus() {
        return discountStatus;
    }

    public void setDiscountStatus(String discountStatus) {
        this.discountStatus = discountStatus;
    }

    public BigDecimal getAdvanceBillAmt() {
        return advanceBillAmt;
    }

    public void setAdvanceBillAmt(BigDecimal advanceBillAmt) {
        this.advanceBillAmt = advanceBillAmt;
    }

    public BigDecimal getUtilizedAmount() {
        return utilizedAmount;
    }

    public void setUtilizedAmount(BigDecimal utilizedAmount) {
        this.utilizedAmount = utilizedAmount;
    }

    public String getAdvanceBillMessage() {
        return advanceBillMessage;
    }

    public void setAdvanceBillMessage(String advanceBillMessage) {
        this.advanceBillMessage = advanceBillMessage;
    }

    public double getBillExchangeRate() {
        return billExchangeRate;
    }

    public void setBillExchangeRate(double billExchangeRate) {
        this.billExchangeRate = billExchangeRate;
    }
}
