package cronjobservice.repositories;

import cronjobservice.models.FormNxp;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 9/21/2017.
 */
@Repository
public interface FormNxpRepo extends CommonRepo<FormNxp,Long> {
    FormNxp findByTempFormNumber(String tempFormNumber);

    @Query("select s from FormNxp s where s.cifid=:cifId and s.status='A'")
    Page<FormNxp> findFormByCifid(@Param("cifId") String cifId, Pageable pageable);

    @Query("select s from FormNxp s where  s.status='A'")
    Page<FormNxp> findFormByStatus(Pageable pageable);

    List<FormNxp> findByStatusAndSubmitFlag(String S, String SF);

   FormNxp findByFormNum(String formNum);

    @Query("select sum(s.totalValueOfGoods) from FormNxp s where s.cifid=:cifId and s.status=:approve")
    Integer addAmt(@Param("cifId") String cifId, @Param("approve") String approve);

    @Query("select s from FormNxp s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<FormNxp> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    @Query("select s from FormNxp s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<FormNxp> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);


    @Query("select s from FormNxp s  where  s.cifid=:cifid ")
    Page<FormNxp> findByCifid(@Param("cifid") String cifid, Pageable pageable);

    @Query("select s from FormNxp s  where  s.status=:approved or s.status=:submitted or s.status=:declined  ")
    Page<FormNxp> findNxpOpenForBank(@Param("approved") String approved, @Param("submitted") String submitted, @Param("declined") String declined, Pageable pageable);

    @Query("select s from FormNxp s where s.cifid=:cifId and s.status=:approved")
    Page<FormNxp> findApprovedFormNxp(@Param("cifId") String cifId, @Param("approved") String approved, Pageable pageable);

    Long countByCifidAndProformaInvoiceStatus(String cifid, String proformaInvoiceStatus);

    Page<FormNxp> findByCifidAndProformaInvoiceStatus(String cif, String proformaInvoiceStatus, Pageable pageable);


    @Query("select s from FormNxp s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<FormNxp> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    @Query("select s from FormNxp s where s.cifid=:cifid  and  s.status is not null")
    Page<FormNxp> findByClosedCifidAndStatus(@Param("cifid") String cifid, Pageable pageable);

    @Query("select s from FormNxp s where s.status is not null and not s.status=:saved ")
    Page<FormNxp> findByStatus(@Param("saved") String saved, Pageable pageable);

}
