package cronjobservice.services;

import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface LcService {

    Boolean sendImportLcITSmail(ImportLc importLc);

    Boolean sendExportLcITSmail(ExportLc exportLc);

    String saveLC(LCDTO lcdto) throws CronJobException;

    Boolean saveLCFinacle(LCDTO lcdto) throws CronJobException;

    Boolean saveImportLCFinacle(ImportLcDTO importLcDTO) throws CronJobException;

    Boolean saveExportLCFinacle(ExportLcDTO exportLcDTO) throws CronJobException;

    Boolean updateImportFromFinacle(ImportLc tmsimportLc, ImportLc finimportLc) throws CronJobException;

    Boolean updateExportFromFinacle(ExportLc tmsexportLc, ExportLc finexportLc) throws CronJobException;

    Boolean updateExportPendingFromFinacle(ExportLc tmsexportLc, ExportLc finexportLc) throws CronJobException;

    String saveLC(LCDTO lcdto, FormMDTO formMDTO) throws CronJobException;

    String updateLC(LCDTO lcdto) throws CronJobException;

    String amendLC(LCDTO lcdto) throws CronJobException;

    String confirmLC(LCDTO lcdto) throws CronJobException;

    String processConfirmLC(LCDTO lcdto) throws CronJobException;

    String processAmendLC(LCDTO lcdto) throws CronJobException;

    LCDTO getLC(Long id) throws CronJobException;

    FormMDTO getFormM(Long id) throws CronJobException;

    Boolean updateExportPendingVerificationFromFinacle(ExportLc tmsexportLc, ExportLc finexportLc) throws CronJobException;

    Boolean changeExportLcToAdvisedFromFinacle(ExportLc tmsexportLc, ExportLc finexportLc) throws CronJobException;

    Boolean saveExportLcAddedOnFinacle(ExportLc finexportLc, FormNxp formNxp) throws CronJobException;

    Boolean saveImportLcAddedOnFinacle(ImportLc finimportLc, FormM formM) throws CronJobException;

    BigDecimal getUtilizedFormMAmount(Long id, String cifid) throws CronJobException;

    public FormMDTO convertEntityTODto(ImportLc lc)  throws CronJobException;

    LCDTO getInitiatorDetails(FormMDTO formMDTO) throws CronJobException;

    Page<LCDTO> getCustomerLcS(Pageable pageDetails) throws CronJobException;

    Page<LCDTO> getAllLcS(Pageable pageDetails);

    Page<LCDTO> getOpenLcS(Pageable pageable) throws CronJobException;

    FormHolder formHolder(LCDTO lcdto) throws CronJobException;

    LCDTO convertEntityToDTO(ImportLc lc);

    ImportLc convertDTOToEntity(LCDTO lc);

    ImportLcDTO convertEntityToImportLcDTO(ImportLc importLc);

    ExportLcDTO convertEntityToExportLcDTO(ExportLc exportLc);

    LcAmend convertLCDTOToAmendEntity(LCDTO lcdto);

    LCDTO convertConfirmationEntityToLCDTO(LcConfirmation lcConfirmation);

    List<LCDTO> convertConfirmationEntitiesToLCDTOs(Iterable<LcConfirmation> lcs);

    LcConfirmation convertLCDTOToConfirmationEntity(LCDTO lcdto);

    LCDTO convertAmendEntityToLCDTO(LcAmend lcAmend);

    List<LCDTO> convertAmendEntitiesToLCDTOs(Iterable<LcAmend> lcs);

    List<LCDTO> convertEntitiesToDTOs(Iterable<ImportLc> lcs);

    long getPage();

    boolean checkIfAmendable(Long id);

    boolean canConfirmationBeAdded(Long id);

    boolean canReinstateLC(Long id);

    LCDTO getLcAmendment(Long id) throws CronJobException;

    LCDTO getLcConfirmation(Long id) throws CronJobException;

    Page<LCDTO> getLcAmendments(String lcNumber, Pageable pageDetails);


    Page<LCDTO> getLcConfirmations(String lcNumber, Pageable pageDetails);

    //Getting Information for Import Bill
    Page<ImportLc> getApprovedImportLcDetails(Pageable pageable) throws CronJobException;

    ImportLc getLcDetails(Long id) throws CronJobException;

    ImportLc getDetailForImportLc(String lcNumber) throws CronJobException;

    List<CommentDTO> getLcComments(FormM formM) throws CronJobException;

    List<CommentDTO> getImportLcComments(ImportLc importLc) throws CronJobException;

    List<CommentDTO> getExportLcComments(ExportLc exportLc) throws CronJobException;

    String addAttachments(MultipartFile file, String formaNumber, String documentType) throws CronJobException;

    Page<LCDTO> findAllLcs(String pattern, Pageable pageDetails);


    Page<LCDTO> getAllLcs(Pageable pageDetails);
}
