package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Collection;


@Entity
@DiscriminatorValue("Under Lc")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class BillsUnderLc extends ExportBills {

    @ManyToOne
    private ExportLc exportLc;

    @OneToMany(cascade = CascadeType.ALL)
    private Collection<BillsDocument> expBillsUnderLcDoc;

    public Collection<BillsDocument> getDocuments() {
        return expBillsUnderLcDoc;
    }

    public void setDocuments(Collection<BillsDocument> documents) {
        this.expBillsUnderLcDoc = documents;
    }

    public ExportLc getExportLc() {
        return exportLc;
    }

    public void setExportLc(ExportLc exportLc) {
        this.exportLc = exportLc;
    }
}
