package cronjobservice.services.implementation;

import cronjobservice.dtos.FormADTO;
import cronjobservice.models.RequestType;
import cronjobservice.models.ServiceAudit;
import cronjobservice.services.ServiceAuditService;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ServiceAuditServiceImpl implements ServiceAuditService {

    @Override
    public ServiceAudit getFormAServiceAudit(FormADTO formADTO, String formResponse){
        ServiceAudit serviceAudit = new ServiceAudit();
        serviceAudit.setRequestType(RequestType.FORM_A);
        serviceAudit.setRequestDate(new Date());
        serviceAudit.setTempNumber(formADTO.getTempFormNumber());
        serviceAudit.setResponseMessage(formResponse);
        if (formResponse.equalsIgnoreCase("Y")){
            serviceAudit.setResponseFlag("Y");
            serviceAudit.setResponseMessage("Successful");
        }else {
            serviceAudit.setResponseFlag("N");
            serviceAudit.setResponseMessage(formResponse);
        }
        return serviceAudit;
    }

}
