package cronjobservice.repositories;


import cronjobservice.models.ImportBillForCollection;
import cronjobservice.models.ImportBillUnderLc;
import org.springframework.stereotype.Repository;

@Repository
public interface ImportBillForCollectionRepo  extends CommonRepo<ImportBillForCollection,Long>{

    ImportBillForCollection findByBillNumber(String billNumber);
}
