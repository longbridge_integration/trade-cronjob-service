package cronjobservice.services;

import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.FormHolder;
import cronjobservice.dtos.OutwardGuaranteeDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.OutwardGuarantee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface OutwardGuaranteeService {

    Boolean updateOutwardGuaranteeFromFinacle(OutwardGuarantee tmsoutwardGuarantee, OutwardGuarantee finoutwardGuarantee) throws CronJobException;

    Boolean addOutwardGuaranteeFromFinacle(OutwardGuaranteeDTO outwardGuaranteeDTO) throws CronJobException;

//    String updateOutwGuarantee(OutwardGuaranteeDTO outwardGuaranteeDTO) throws CronJobException;

    Boolean sendOutwardGuaranteeITSmail(OutwardGuarantee outwardGuarantee);

    OutwardGuaranteeDTO editOutwGuarantee(Long id) throws CronJobException;

    OutwardGuaranteeDTO getInitiatorDetails() throws CronJobException;

//    String addOutwGuarantee(OutwardGuaranteeDTO outwardGuaranteeDTO) throws CronJobException;

    Page<OutwardGuaranteeDTO> getClosedOutwGuarantee(Pageable pageable);

    Page<OutwardGuaranteeDTO> getOutwGuaranteeForBankUser(Pageable pageable) throws CronJobException;

    FormHolder createFormHolder(OutwardGuaranteeDTO outwardGuaranteeDTO, String name) throws CronJobException;


    FormHolder executeAction(OutwardGuaranteeDTO outwardGuaranteeDTO, String name) throws CronJobException;

    Page<OutwardGuaranteeDTO> getOutwGuarantees(Pageable pageDetails);
    Page<OutwardGuaranteeDTO> getAllOutwGuarantees(Pageable pageDetails);


    Page<OutwardGuaranteeDTO> getOpenGuarantees(Pageable pageable) throws CronJobException;

//    Page<OutwardGuaranteeDTO> getBankOutwGuarantees(Pageable pageable);
//
//    Page<OutwardGuaranteeDTO> getBankOpenOutwGuarantee(Pageable pageable);
//
//    FormHolder formHolder(OutwardGuaranteeDTO outwardGuaranteeDTO) throws CronJobException;


    List<OutwardGuaranteeDTO> getCustomerClosedOutwGuarantee();

    List<OutwardGuaranteeDTO> getCustomerClosedOutwGuaranteeByCif(String cifid);

    OutwardGuarantee convertDTOTOEntity(OutwardGuaranteeDTO outwardGuaranteeDTO);

    OutwardGuaranteeDTO convertEntityToDTO(OutwardGuarantee outwardGuarantee);

//    OutwardGuaranteeDTO convertEntityToDTOView(OutwardGuarantee outwardGuarantee);

    List<CommentDTO> getComments(OutwardGuarantee outwardGuarantee) throws CronJobException;

    List<OutwardGuaranteeDTO> convertEntitiesToDTOs(Iterable<OutwardGuarantee> outwardGuarantees);


    Iterable<OutwardGuaranteeDTO> getGuarantees();

    OutwardGuaranteeDTO getOutGuaranteeNumber(String guaranteeNumber) throws CronJobException;

    Page<OutwardGuaranteeDTO> getBankOpenOutwards(Pageable pageable);

    String addAttachments(MultipartFile file, String guaranteeNumber, String documentType);

//    Page<OutwardGuaranteeDTO> getBankOpenOutwards(Pageable pageable);
}
