package cronjobservice.services.implementation;

import cronjobservice.dtos.CodeDTO;
import cronjobservice.dtos.RetailUserDTO;
import cronjobservice.dtos.SettingDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.RetailUserRepo;
import cronjobservice.repositories.RoleRepo;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.ReflectionUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.*;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ayoade_farooq@yahoo.com on 3/29/2017.
 * Modified by Fortune
 */
@Service
public class RetailUserServiceImpl implements RetailUserService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private RetailUserRepo retailUserRepo;

//    @Qualifier("bCryptPasswordEncoder")
//    private CustomerTradePassWordEncoder passwordEncoder;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MailService mailService;

//    @Autowired
//    private PasswordPolicyService passwordPolicyService;
//
//    @Autowired
//    RetSecQuestionRepo retSecQuestionRepo;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CodeService codeService;

    @Autowired
    IntegrationService integrationService;

    @Autowired
    AccountService accountService;

//    @Autowired
//    private SecurityService securityService;

    @Autowired
    private ConfigurationService configService;

    @Autowired
    private RoleRepo roleRepo;

    @Value("${host.url}")
    private String hostUrl;


//    @Autowired
//    private FailedLoginService failedLoginService;

    @Autowired
    EntityManager entityManager;

    private Locale locale = LocaleContextHolder.getLocale();

    public RetailUserServiceImpl() {

    }


    @Override
    public boolean userExists(String username) {
        RetailUser retailUsers = retailUserRepo.findFirstByUserName(username);
        return retailUsers != null;

    }


//    @Autowired
//    public RetailUserServiceImpl(RetailUserRepo retailUserRepo, CustomerTradePassWordEncoder passwordEncoder) {
//        this.retailUserRepo = retailUserRepo;
//        this.passwordEncoder = passwordEncoder;
//    }

    @Override
    public RetailUserDTO getUser(Long id) {
        RetailUser user = retailUserRepo.findOne(id);
        return convertEntityToDTO(user);
    }

    @Override
    public Page<RetailUserDTO> findUsers(RetailUserDTO example, Pageable pageDetails) {
        ExampleMatcher matcher = ExampleMatcher.matchingAll().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase().withIgnorePaths("version", "noOfAttempts").withIgnoreNullValues();
        RetailUser entity = convertDTOToEntity(example);
        ReflectionUtils.nullifyStrings(entity, 1);
        Page<RetailUser> page = retailUserRepo.findAll(Example.of(entity, matcher), pageDetails);
        List<RetailUserDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<RetailUserDTO> pageImpl = new PageImpl<RetailUserDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Iterable<RetailUserDTO> getUsers() {
        Iterable<RetailUser> retailUserss = retailUserRepo.findAll();
        return convertEntitiesToDTOs(retailUserss);
    }

    @Override
    public List<RetailUserDTO> getByCustomerSegment(CodeDTO custSegment) {
        Code code = modelMapper.map(custSegment, Code.class);
        List<RetailUser> retailUsers = retailUserRepo.findByCustomerSegment(code);
        return convertEntitiesToDTOs(retailUsers);
    }


    @Override
    public void sendActivationCredentials(RetailUser user, String password) {

        try {
            String retUrl = (hostUrl != null) ? hostUrl + "/retail" : "";

            String fullName = user.getFirstName() + " " + user.getLastName();
            Context context = new Context();
            context.setVariable("name", fullName);
            context.setVariable("username", user.getUserName());
            context.setVariable("password", password);
            context.setVariable("retUrl", retUrl);
            context.setVariable("code", "RETACT");

            Email email = new Email.Builder()
                    .setRecipient(user.getEmail())
                    .setSubject(messageSource.getMessage("retail.activation.subject", null, locale))
                    .setTemplate("mail/activation")
                    .build();

            mailService.sendMail(email, context);
        } catch (Exception e) {
            logger.error("Error occurred sending activation credentials", e);

        }
    }


    @Async
    public void sendPostActivateMessage(User user, String... args) {

        Email email = new Email.Builder()
                .setRecipient(user.getEmail())
                .setSubject(messageSource.getMessage("admin.activation.subject", null, locale))
                .setBody(String.format(messageSource.getMessage("admin.activation.message", null, locale), args))
                .build();
        mailService.send(email);
    }



    @Async
    @Override
    public void sendActivateMessage(User user, String... args)
    {
        RetailUser retailUsers = getUserByName(user.getUserName());
        if ("A".equals(retailUsers.getStatus())) {
            Email email = new Email.Builder()
                    .setRecipient(user.getEmail())
                    .setSubject(messageSource.getMessage("retail.activation.subject", null, locale))
                    .setBody(String.format(messageSource.getMessage("retail.activation.message", null, locale), args))
                    .build();
            mailService.send(email);
        }
    }

    @Override
    public RetailUser getUserByName(String name) {
        RetailUser retailUsers = this.retailUserRepo.findFirstByUserName(name);
        return retailUsers;
    }


//    public void createUserOnEntrust(RetailUser retailUsers) {
//        RetailUser user = retailUserRepo.findFirstByUserName(retailUsers.getUserName());
//        if (user != null) {
//            if ("".equals(user.getEntrustId()) || user.getEntrustId() == null) {
//                String fullName = user.getFirstName() + " " + user.getLastName();
//                SettingDTO setting = configService.getSettingByName("ENABLE_ENTRUST_CREATION");
//                String entrustId = user.getUserName();
//
//
//                if (setting != null && setting.isEnabled() ) {
//                    String group = configService.getSettingByName("DEF_ENTRUST_OPS_GRP").getValue();
//                    if ("YES".equalsIgnoreCase(setting.getValue())) {
//                       // boolean creatResult = securityService.createEntrustUser(entrustId, group, fullName, true);
////                        if (!creatResult) {
////                            throw new EntrustException(messageSource.getMessage("entrust.create.failure", null, locale));
////                        }
//
////                        boolean contactResult = securityService.addUserContacts(user.getEmail(), user.getPhoneNumber(), true, entrustId, group);
////                        if (!contactResult) {
////                            logger.error("Failed to add user contacts on Entrust");
////                            securityService.deleteEntrustUser(entrustId, group);
////                            throw new EntrustException(messageSource.getMessage("entrust.contact.failure", null, locale));
////                        }
//                    }
//                    user.setEntrustId(entrustId);
//                   // user.setEntrustGroup(group);
//                    retailUserRepo.save(user);
//                }
//            }
//        }
//    }

//    @Override
//    @Transactional
//    @Verifiable(operation = "UPDATE_RETAIL_USER", description = "Updating a retail User")
//    public String updateUser(RetailUserDTO user) throws CronJobException {
//
//        RetailUser retailUsers = retailUserRepo.findOne(user.getId());
//        if ("I".equals(retailUsers.getStatus())) {
//            throw new CronJobException(messageSource.getMessage("user.deactivated", null, locale));
//        }
//
//        try {
//            entityManager.detach(retailUsers);
//            retailUsers.setVersion(user.getVersion());
//            retailUsers.setFirstName(user.getFirstName());
//            retailUsers.setLastName(user.getLastName());
//            retailUsers.setUserName(user.getUserName());
//            retailUsers.setUserName(user.getUserName());
//            retailUsers.setEmail(user.getEmail());
//            Code customerSegment = codeService.getCodeById(user.getCustomerSegmentId());
//            retailUsers.setCustomerSegment(customerSegment);
//            if ("HARD".equalsIgnoreCase(user.getTokenType())){
//                retailUsers.setTokenType(TokenType.HARD);
//            }else if ("SOFT".equalsIgnoreCase(user.getTokenType())){
//                retailUsers.setTokenType(TokenType.SOFT);
//            }
//            Role role = roleRepo.findOne(Long.parseLong(user.getRoleId()));
//            retailUsers.setRole(role);
//            retailUserRepo.save(retailUsers);
//
//            logger.info("Admin user {} updated", retailUsers.getUserName());
//            return messageSource.getMessage("user.update.success", null, locale);
//        } catch (VerificationInterruptedException e) {
//            return e.getMessage();
//        } catch (CronJobException ibe) {
//            throw ibe;
//        } catch (Exception e) {
//            throw new CronJobException(messageSource.getMessage("user.update.failure", null, locale), e);
//        }
//    }



    private void sendPasswordResetCredentials(RetailUser user, String password) {

        try {
            String retUrl = (hostUrl != null) ? hostUrl + "/retail" : "";

            String fullName = user.getFirstName() + " " + user.getLastName();
            Context context = new Context();
            context.setVariable("name", fullName);
            context.setVariable("username", user.getUserName());
            context.setVariable("password", password);
            context.setVariable("retUrl", retUrl);
            context.setVariable("code", "RETRESPWD");

            Email email = new Email.Builder()
                    .setRecipient(user.getEmail())
                    .setSubject(messageSource.getMessage("customer.password.reset.subject", null, locale))
                    .setTemplate("mail/passreset")
                    .build();

            mailService.sendMail(email, context);
        } catch (Exception e) {
            logger.error("Error occurred sending activation credentials", e);

        }
    }



    private RetailUserDTO convertEntityToDTO(RetailUser retailUsers)
    {
        RetailUserDTO retailUsersDTO = modelMapper.map(retailUsers, RetailUserDTO.class);
        //retailUsersDTO.setRole(retailUsers.getRole().getName());
        if (retailUsers.getCustomerSegment() != null){
            retailUsersDTO.setCustomerSegmentId(retailUsers.getCustomerSegment().getId());
            retailUsersDTO.setCustomerSegment(retailUsers.getCustomerSegment().getDescription());
        }
        if (retailUsers.getRole() != null){
            retailUsersDTO.setRoleId(retailUsers.getRole().getId().toString());
            retailUsersDTO.setRole(retailUsers.getRole().getName());
        }
        if (TokenType.HARD.equals(retailUsers.getTokenType())){
            retailUsersDTO.setTokenType("HARD");
        }
        if (TokenType.SOFT.equals(retailUsers.getTokenType())){
            retailUsersDTO.setTokenType("SOFT");
        }
        if (retailUsers.getCreatedOnDate() != null) {
            retailUsersDTO.setCreatedOn(DateFormatter.format(retailUsers.getCreatedOnDate()));
        }
        if (retailUsers.getLastLoginDate() != null)
        {
            retailUsersDTO.setLastLogin(DateFormatter.format(retailUsers.getLastLoginDate()));
        }

        Code code = codeService.getByTypeAndCode("USER_STATUS", retailUsers.getStatus());
        if (code != null)
        {
            retailUsersDTO.setStatus(code.getDescription());
        }
        return retailUsersDTO;
    }

    private RetailUser convertDTOToEntity(RetailUserDTO retailUsersDTO) {
        RetailUser retailUser = modelMapper.map(retailUsersDTO, RetailUser.class);
        if ("HARD".equalsIgnoreCase(retailUsersDTO.getTokenType())){
            retailUser.setTokenType(TokenType.HARD);
        }else if ("SOFT".equalsIgnoreCase(retailUsersDTO.getTokenType())){
            retailUser.setTokenType(TokenType.SOFT);
        }
        retailUser.setCustomerSegment(codeService.getCodeById(retailUsersDTO.getCustomerSegmentId()));
        return retailUser;
    }

    private List<RetailUserDTO> convertEntitiesToDTOs(Iterable<RetailUser> retailUserss) {
        List<RetailUserDTO> retailUsersDTOList = new ArrayList<>();
        for (RetailUser retailUsersUser : retailUserss) {
            RetailUserDTO userDTO = convertEntityToDTO(retailUsersUser);
            retailUsersDTOList.add(userDTO);
        }
        return retailUsersDTOList;
    }

    @Override
    public Page<RetailUserDTO> getUsers(Pageable pageDetails) {
        Page<RetailUser> page = retailUserRepo.findAll(pageDetails);
        List<RetailUserDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<RetailUserDTO> pageImpl = new PageImpl<RetailUserDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }


    @Override
    public RetailUser getUserByEmail(String email) {
        return retailUserRepo.findFirstByEmailIgnoreCase(email);
    }


    @Override
    public Page<RetailUserDTO> findUsers(String pattern, Pageable pageDetails) {
        Page<RetailUser> page = retailUserRepo.findUsingPattern(pattern, pageDetails);
        List<RetailUserDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();

        Page<RetailUserDTO> pageImpl = new PageImpl<RetailUserDTO>(dtOs, pageDetails, t);
        return pageImpl;



    }


}
