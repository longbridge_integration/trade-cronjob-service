package cronjobservice.repositories;

import cronjobservice.models.FormA;
import cronjobservice.models.FormQ;
import cronjobservice.models.PTA;

import java.util.List;

public interface FormQRepo extends CommonRepo<FormQ, Long> {
    List<FormQ> findByStatusAndSubmitFlag(String S, String SF);
    FormQ findByTempFormNumber(String tempFormNumber);
    FormQ findByFormNumber(String formNumber);
}
