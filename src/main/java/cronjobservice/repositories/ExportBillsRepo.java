package cronjobservice.repositories;

import cronjobservice.models.ExportBills;
import cronjobservice.models.PTA;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExportBillsRepo extends CommonRepo<ExportBills,Long> {

    List<ExportBills> findByStatusAndSubmitFlag(String S, String SF);
    ExportBills findByTemporalBllNumb(String tempNumber);
}
