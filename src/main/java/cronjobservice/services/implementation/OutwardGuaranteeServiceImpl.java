package cronjobservice.services.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.FormNumber;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
public class OutwardGuaranteeServiceImpl implements OutwardGuaranteeService {

    @Value("${formnxp.upload.files}")
    private String UPLOADED_FOLDER;

    @Autowired
    MessageSource messageSource;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
//    @Autowired
//    CommentService commentService;
    @Autowired
    CorporateService corporateService;
//    @Autowired
//    WorkFlowStatesService workFlowStatesService;
    @Autowired
    RetailUserService retailUserService;

    @Autowired
    BranchRepo branchRepo;

//    @Autowired
//    OutwardDocumentRepo documentRepo;

    @Autowired
    CodeRepo codeRepo;

    @Autowired
    ServiceAuditRepo serviceAuditRepo;

    @Autowired
    MailService mailService;

    @Autowired
    EntityManager entityManager;
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private WorkFlowRepo workFlowRepo;
//    private UserGroupService userGroupService;
    @Autowired
    private ConfigurationService configurationService;
    private Locale locale = LocaleContextHolder.getLocale();
    @Autowired
    private OutwardGuaranteeRepo outwardGuaranteeRepo;
    @Value("${bank.code}")
    private String bankCode;


    @Override
    public Boolean updateOutwardGuaranteeFromFinacle(OutwardGuarantee tmsoutwardGuarantee, OutwardGuarantee finoutwardGuarantee) throws CronJobException {
        System.out.println("Out Guarantee got into the service");

        try {
            tmsoutwardGuarantee.setGuaranteeNumber(finoutwardGuarantee.getGuaranteeNumber());
            tmsoutwardGuarantee.setTranId(finoutwardGuarantee.getTranId());
            outwardGuaranteeRepo.save(tmsoutwardGuarantee);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }


    }

    @Override
    public Boolean addOutwardGuaranteeFromFinacle(OutwardGuaranteeDTO outwardGuaranteeDTO) throws CronJobException {

        try {

            String number = FormNumber.getInwGuranteeNumner();
            OutwardGuarantee outwardGuarantee = modelMapper.map(outwardGuaranteeDTO, OutwardGuarantee.class);
//            outwardGuarantee.setUserType("BANK");
            outwardGuarantee.setInitiatedBy("FINACLE");
            outwardGuarantee.setCifid(outwardGuaranteeDTO.getCifid());
            outwardGuarantee.setExpiryOn(outwardGuaranteeDTO.getExpiryOn());
            outwardGuarantee.setTempFormNumber(number);
            outwardGuarantee.setGuaranteeNumber(outwardGuaranteeDTO.getGuaranteeNumber());
            System.out.println("before getting settings");
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_OUTGUAR_WF");
            System.out.println("after getting settings");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            System.out.println("after getting workflow");
            outwardGuarantee.setWorkFlow(workFlow);
            outwardGuaranteeRepo.save(outwardGuarantee);
            return true;
        }

        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("guarantee.add.success", null, locale));
        }
    }

    @Override
    public Boolean sendOutwardGuaranteeITSmail(OutwardGuarantee outwardGuarantee) {

        try {
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "Outward Guarantee";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(outwardGuarantee.getTempFormNumber(),RequestType.OUTWARD_GUARANTEE);
            Context context = new Context();
            context.setVariable("name", outwardGuarantee.getAppName());
            context.setVariable("formNumber", outwardGuarantee.getTempFormNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", DateFormatter.format(new Date()));
            context.setVariable("customerId", outwardGuarantee.getCifid());
            context.setVariable("username", outwardGuarantee.getInitiatedBy());
            context.setVariable("error", serviceAudit.getResponseMessage());
            context.setVariable("code", "OUTWARD_GUARANTEE");
            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
            Email email = new Email.Builder()
                    .setRecipients(allMails)
                    .setSubject(subject)
                    .setTemplate("mail/finacleSubmission")
                    .build();

            mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("guarantee.add.success", null, locale));
        }
    }


    @Override
    public OutwardGuaranteeDTO editOutwGuarantee(Long id) throws CronJobException {
        OutwardGuarantee outwardGuarantee = outwardGuaranteeRepo.findOne(id);
        return convertEntityToDTO(outwardGuarantee);

    }

    @Override
    public OutwardGuaranteeDTO getInitiatorDetails() throws CronJobException {
        OutwardGuaranteeDTO doneBy = new OutwardGuaranteeDTO();
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        } else if (users.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) users;
            doneBy.setAppName(corporateUser.getCorporate().getName());
            doneBy.setAppCountry(corporateUser.getCorporate().getCountry());
            doneBy.setAppState(corporateUser.getCorporate().getState());
            doneBy.setAppCity(corporateUser.getCorporate().getTown());
            doneBy.setAppAddress1(corporateUser.getCorporate().getAddress());
            doneBy.setAppAddress2(corporateUser.getCorporate().getAddress());

        } else if (users.getUserType().equals(UserType.BANK)) {
            BankUser user = (BankUser) users;
            doneBy.setAppName(user.getFirstName());
        } else {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        return doneBy;
    }

//    @Override
//    public String addOutwGuarantee(OutwardGuaranteeDTO outwardGuaranteeDTO) throws CronJobException {
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User loggedInUser = principal.getUser();
//        String customerId = null;
//
//        if ((loggedInUser.getUserType()).equals(UserType.RETAIL)) {
//            RetailUser retailUser = (RetailUser) loggedInUser;
//            customerId = retailUser.getCustomerId();
//        } else if (loggedInUser.getUserType().equals(UserType.CORPORATE)) {
//            CorporateUser corporateUser = (CorporateUser) loggedInUser;
//            customerId = corporateUser.getCorporate().getCustomerId();
//        } else if (loggedInUser.getUserType().equals(UserType.BANK)) {
//            customerId = outwardGuaranteeDTO.getCifid();
//        }
//        try {
//
//            String generatedFormNumber = FormNumber.getOutwGuranteeNumner();
//if(outwardGuaranteeDTO.getDocuments()!=null) {
//    for (OutGuaranteeDocDTO d : outwardGuaranteeDTO.getDocuments()) {
//        if (!(d.getFile().isEmpty())) {
//            MultipartFile file = d.getFile();
//            String filename = addAttachments(file, generatedFormNumber, d.getDocumentType());
//            d.setFileUpload(filename);
//        }
//    }
//}
//
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_OUTGUAR_WF");
//            logger.info("workflow setting {}",wfSetting.toString());
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            logger.info("workflow {} and id {}",workFlow,workFlow.getId());
//            outwardGuaranteeDTO.setWorkFlowId(workFlow.getId());
//            OutwardGuarantee outwardGuarantee = convertDTOTOEntity(outwardGuaranteeDTO);
//            outwardGuarantee.setGuaranteeNumber(FormNumber.getOutwGuranteeNumner());
//            outwardGuarantee.setTempFormNumber(FormNumber.getOutwGuranteeNumner());
//            outwardGuarantee.setCreatedOn(new Date());
//            outwardGuarantee.setCifid(customerId);
//            outwardGuarantee.setSubmitFlag("U");
//            outwardGuarantee.setBankCode(bankCode);
//            outwardGuarantee.setAppCountry("NG");
//
//
//
//        if(outwardGuaranteeDTO.getDocuments()!=null) {
//            if (null != outwardGuaranteeDTO.getBranch() && !outwardGuaranteeDTO.getBranch().equalsIgnoreCase("")) {
//                logger.info(outwardGuaranteeDTO.getBranch());
//                Branch branch = branchRepo.findOne(Long.parseLong(outwardGuaranteeDTO.getBranch()));
//                outwardGuarantee.setDocSubBranch(branch);
//            }
//            Iterator<OutGuaranteeDoc> DocumentIterator = outwardGuarantee.getDocuments().iterator();
//            Iterator<OutGuaranteeDocDTO> DocumentDTOIterator = outwardGuaranteeDTO.getDocuments().iterator();
//
//            while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                OutGuaranteeDoc document = DocumentIterator.next();
//                OutGuaranteeDocDTO documentDTO = DocumentDTOIterator.next();
//                if (documentDTO.getDocumentType() == null) {
//                    DocumentIterator.remove();
//                    DocumentDTOIterator.remove();
//                } else {
//                    document.setDocumentType(codeRepo.findByTypeAndCode("GUARANTEE_DOCUMENT_TYPE", documentDTO.getDocumentType()));
//                    document.setOutwardGuarantee(outwardGuarantee);
//                }
//            }
//        }
//            outwardGuaranteeRepo.save(outwardGuarantee);
//            logger.info("Outward Guarantee created successfully");
//            return messageSource.getMessage("ouwguar.add.success", null, locale);
//        } catch (CronJobException te) {
//            throw new CronJobException(messageSource.getMessage("ouwguar.add.failure", null, locale));
//        }
//    }


    @Override
    public Page<OutwardGuaranteeDTO> getClosedOutwGuarantee(Pageable pageable) {
        return null;
    }

    @Override
    public Page<OutwardGuaranteeDTO> getOutwGuaranteeForBankUser(Pageable pageable) throws CronJobException {
        return null;
    }

    @Override
    public FormHolder createFormHolder(OutwardGuaranteeDTO outwardGuaranteeDTO, String name) throws CronJobException {
        return null;
    }

    @Override
    public FormHolder executeAction(OutwardGuaranteeDTO outwardGuaranteeDTO, String name) throws CronJobException {
        return null;
    }

    @Override
    public Page<OutwardGuaranteeDTO> getOutwGuarantees(Pageable pageDetails) {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;

        if ((loggedInUser.getUserType()).equals(UserType.RETAIL))
        {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        }
        else if (loggedInUser.getUserType().equals(UserType.CORPORATE))
        {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        }

        Page<OutwardGuarantee> page = outwardGuaranteeRepo.findByCifidAndStatusNotNull(customerId, pageDetails);
        List<OutwardGuaranteeDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<OutwardGuaranteeDTO> pageImpl = new PageImpl<OutwardGuaranteeDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<OutwardGuaranteeDTO> getAllOutwGuarantees(Pageable pageDetails) {
        Page<OutwardGuarantee> page = outwardGuaranteeRepo.findByStatusNotNull(pageDetails);
        List<OutwardGuaranteeDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<OutwardGuaranteeDTO> pageImpl = new PageImpl<OutwardGuaranteeDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<OutwardGuaranteeDTO> getOpenGuarantees(Pageable pageable) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;

        if ((loggedInUser.getUserType()).equals(UserType.RETAIL))
        {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        }
        else if (loggedInUser.getUserType().equals(UserType.CORPORATE))
        {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        }

        Page<OutwardGuarantee> pages=outwardGuaranteeRepo.findByCifidAndStatus(customerId,"P","S","D",pageable);
        List<OutwardGuaranteeDTO> outwardGuaranteeDTOS=convertEntitiesToDTOs(pages.getContent());
        long t=pages.getTotalElements();
        Page<OutwardGuaranteeDTO> page1=new PageImpl<OutwardGuaranteeDTO>(outwardGuaranteeDTOS,pageable,t);
        return page1;
    }


//    @Override
//    public FormHolder formHolder(OutwardGuaranteeDTO outwardGuaranteeDTO) throws CronJobException {
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        User doneby = principal.getUser();
//        BankUser bankUser = (BankUser) doneby;
//        try {
//            FormHolder holder = new FormHolder();
//           OutwardGuarantee outwardGuarantee=addComments(outwardGuaranteeDTO);
//            outwardGuarantee.setCreatedOn(new Date());
//          outwardGuarantee.setSubmitFlag("U");
//          outwardGuarantee.setAppCountry("NG");
//          outwardGuarantee.setBankCode(bankCode);
//          outwardGuarantee.setIssueOn(outwardGuaranteeDTO.getIssueOn());
//          //Approval name
//            holder.setObjectData(objectMapper.writeValueAsString(outwardGuarantee));
//            holder.setEntityId(outwardGuarantee.getId());
//            holder.setEntityName("OutwardGuarantee");
//            holder.setAction(outwardGuaranteeDTO.getSubmitAction());
//            return holder;
//
//        }
//        catch (JsonProcessingException e)
//        {
//            throw new CronJobException(messageSource.getMessage("form.process.failure",null,locale));
//
//        }
//    }


    @Override
    public List<OutwardGuaranteeDTO> getCustomerClosedOutwGuarantee() {
        return null;
    }

    @Override
    public List<OutwardGuaranteeDTO> getCustomerClosedOutwGuaranteeByCif(String cifid) {
        return null;
    }

    @Override
    public OutwardGuarantee convertDTOTOEntity(OutwardGuaranteeDTO outwardGuaranteeDTO) {

        WorkFlow workFlow = workFlowRepo.findOne(outwardGuaranteeDTO.getWorkFlowId());
        outwardGuaranteeDTO.setWorkFlow(workFlow);
        OutwardGuarantee outwardGuarantee = modelMapper.map(outwardGuaranteeDTO, OutwardGuarantee.class);


        return outwardGuarantee;
    }

    @Override
    public OutwardGuaranteeDTO convertEntityToDTO(OutwardGuarantee outwardGuarantee) {
        OutwardGuaranteeDTO outwardGuaranteeDTO = modelMapper.map(outwardGuarantee, OutwardGuaranteeDTO.class);


        if (outwardGuarantee.getWorkFlow() != null) {
            outwardGuaranteeDTO.setWorkFlowId(outwardGuarantee.getWorkFlow().getId());
//            WorkFlowStatesDTO workFlowState = workFlowStatesService.getByTypeAndCode(outwardGuarantee.getWorkFlow().getCodeType(), outwardGuarantee.getStatus());
//            outwardGuaranteeDTO.setStatusDesc(workFlowState.getDescription());
        }
        if(outwardGuaranteeDTO.getStatus()!=null && !(outwardGuaranteeDTO.getStatus().equals("P"))){
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(outwardGuaranteeDTO.getWorkFlow().getCodeType(), outwardGuaranteeDTO.getStatus());
            outwardGuaranteeDTO.setStatusDesc(workFlowStates.getDescription());


        }
        if (outwardGuarantee.getCreatedOn() != null) {
            outwardGuaranteeDTO.setConvert(DateFormatter.format(outwardGuarantee.getCreatedOn()));
        }
        return outwardGuaranteeDTO;
    }


    @Override
    public List<OutwardGuaranteeDTO> convertEntitiesToDTOs(Iterable<OutwardGuarantee> outwardGuarantees) {
        List<OutwardGuaranteeDTO> outwardGuaranteeDTOS = new ArrayList<>();
        for (OutwardGuarantee outwardGuarantee : outwardGuarantees) {
            OutwardGuaranteeDTO outwardGuaranteeDTO = convertEntityToDTO(outwardGuarantee);
            outwardGuaranteeDTOS.add(outwardGuaranteeDTO);
        }
        return outwardGuaranteeDTOS;
    }

    private CommentDTO convertEntityToDTO(Comments comments) {
        WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode("OUTW_GUAR", comments.getStatus());
        if (workFlowStates != null) {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments, CommentDTO.class);
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS) {
        List<CommentDTO> commentDTOList = new ArrayList<>();

        for (Comments comments : commentDTOS) {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return commentDTOList;
    }

    @Override
    public List<CommentDTO> getComments(OutwardGuarantee outwardGuarantee) throws CronJobException {
        List<CommentDTO> commentDTOList = new ArrayList<>();

        {
            commentDTOList = convertEntitiestoDTOs(outwardGuarantee.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus() != "A");


        }
        return commentDTOList;
    }

    @Override
    public Iterable<OutwardGuaranteeDTO> getGuarantees(){
        Iterable<OutwardGuarantee> outwardGuarantees=outwardGuaranteeRepo.findAll();
        logger.info("There are the guarantee &&&&",outwardGuarantees);
        return convertEntitiesToDTOs(outwardGuarantees);
    }


    @Override
    public OutwardGuaranteeDTO getOutGuaranteeNumber(String guaranteeNumber) throws CronJobException {

        OutwardGuarantee outwardGuarantee=outwardGuaranteeRepo.findByGuaranteeNumber(guaranteeNumber);
        return convertEntityToDTO(outwardGuarantee);
    }

    @Override
    public Page<OutwardGuaranteeDTO> getBankOpenOutwards(Pageable pageable) {

            Page<OutwardGuarantee> page = outwardGuaranteeRepo.findByStatusS("S",pageable);
            List<OutwardGuaranteeDTO> dtOs = convertEntitiesToDTOs(page.getContent());
            long t = page.getTotalElements();
            Page<OutwardGuaranteeDTO> pageImpl = new PageImpl<OutwardGuaranteeDTO>(dtOs, pageable, t);
            return pageImpl;

        }

    @Override
    public String addAttachments(MultipartFile file, String guaranteeNumber, String documentType) {
        String uploadedFileName = "";


        if (file.isEmpty()) {
            logger.info("file is empty");
        }

        try {
            byte[] bytes = file.getBytes();
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User users = principal.getUser();

            if (users.getUserType().equals(UserType.CORPORATE)) {
                CorporateUser corporateUser = (CorporateUser) users;
                String username = corporateUser.getUserName();
                logger.info("corporate user {} with username {}", corporateUser, username);

                Path  path = Paths.get(UPLOADED_FOLDER + guaranteeNumber + documentType + file.getOriginalFilename());
                logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());


                File f = new File(path.toString());
                if(f.exists()){
                    logger.info("file already exist");
                }else{
                    logger.info("file does not exist");
                }

                Files.write(path, bytes);
                logger.info("file uploaded successfully to {} directory", path);
                uploadedFileName =file.getOriginalFilename();
            }
            else if(users.getUserType().equals(UserType.BANK)) {
                BankUser bankUser = (BankUser) users;
                String username = bankUser.getUserName();
                logger.info("corporate user {} with username {}", bankUser, username);

                Path  path = Paths.get(UPLOADED_FOLDER + guaranteeNumber + documentType + file.getOriginalFilename());
                logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());


                File f = new File(path.toString());
                if(f.exists()){
                    logger.info("file already exist");
                }else{
                    logger.info("file does not exist");
                }

                Files.write(path, bytes);
                logger.info("file uploaded successfully to {} directory", path);
                uploadedFileName =file.getOriginalFilename();
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("failed to upload file");
        }

        logger.info("uploadFileName {}", uploadedFileName);
        if (StringUtils.isEmpty(uploadedFileName)) {
            logger.info("No file added, Please select a file to upload");

        } else {
            logger.info("You successfully uploaded {}", uploadedFileName);

        }
        return uploadedFileName;
    }

}
