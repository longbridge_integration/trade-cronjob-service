package cronjobservice.repositories;

import cronjobservice.models.FormM;
import cronjobservice.models.ImportLc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by JUDE on 1/7/2018.
 */
@Repository
public interface ImportLcRepo extends CommonRepo<ImportLc,Long>{

    Long countByCifid(String cifid);

    ImportLc findByLcNumber(String lcNumber);

    ImportLc findByTempLcNumber(String tempLcNumber);

    List<ImportLc> findByStatusAndSubmitFlag(String S, String SF);

    List<ImportLc> findByFormMAndCifid(FormM id, String customerid);

    Page<ImportLc> findByStatusIsNull(Pageable pageable);

    Page<ImportLc> findByStatusNotNull(Pageable pageable);

    Page<ImportLc> findByCifidAndStatusNotNull(String cif, Pageable pageable);

    @Query("select s from ImportLc s where  s.status=:issued")
    Page<ImportLc> findFormByStatus(@Param("issued") String submitted, Pageable pageable);

    @Query("select s from ImportLc s where s.status is not null and not (s.status=:saved or s.status=:penapp or s.status=:penddec ) ")
    Page<ImportLc> findByStatus(@Param("saved") String saved, @Param("penapp") String penapp, @Param("penddec") String penddec, Pageable pageable);

    @Query("select distinct s from ImportLc s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<ImportLc> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);

    @Query("select distinct s from ImportLc s where s.cifid=:cifid and s.status=:submitted")
    Page<ImportLc> findByCifidAndStatusS(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    @Query("select distinct s from ImportLc s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<ImportLc> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    @Query("select distinct s from ImportLc s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<ImportLc> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    //    @Query("select  sum (s.lcAmount)  from ImportLc s  where  s.cifid=:cifid and s.formmId=:formmid")
//    BigDecimal sumAmountInLC(@Param("cifid") String cifid, @Param("formmid") long formmid);


}
