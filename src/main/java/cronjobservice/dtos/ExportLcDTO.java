package cronjobservice.dtos;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class ExportLcDTO {

    private Boolean irrevokable;
    private String approver;
    private String instructionDates;

    private int version;
    private String formNxpId;
    private String formNxpNumber;
    private String lcTypeName;
    private String typeOfLc;
    //applicant Details

    private String accountToChargeId;
    private String accountToChargeNumber;
    private String accountToCharge;
    private String accountForTransNumber;//account should be of same currency as that of form + Naira; display with currency
    private String accountForTransId;
    private String accountForTrans;
    private BigDecimal partCleanline;
    private BigDecimal partConfirmed;
    private BigDecimal partUnconfirmed;

    private String advBankIdent;
    private String advThrBankIdent;
    private String expiryDate;
    private Date insurPolicyDate;
    private Date insurDateOfExpiry;
    private String insurExpiryDate;
    private String lcPaymentTenorId;
    private String negotiationPeriod;
    private String concession; //flg(Y/N)- bank
    private String docCreditType;//dropdown - bank


    private List<ExportLcDocumentDTO> documents;
    private Date lastShipmentDate;
    private String amendAction;
    private String confirmationType;

    private String date;
    private long workFlowId;
    private String statusDesc;
    private String submitAction;
    private String initiatedBy;
    private String comments;
    private String branch;
    private String brDesc;
    private String select;
    private String file;
    //private List<Bills> billsList;

    private String lcNumber;
    private String tempLcNumber;
    //pass the form M number to Finacle as formMnumber
    private String formM;
    private String chargeAccount;
    private String transAccount;

    //form M details
    private String lcType;
    private BigDecimal lcAmount;
    private String lcAccount;
    private String lcCurrency;
    private String exchangeRate;

    private String advBankName;
    private String advBankBranch;
    private String advBankAddress;
    private String advSwiftCode;
    private String advBankCode;

    private String advThrBankName;
    private String advThrBankBranch;
    private String advThrBankAddress;
    private String advThrSwiftCode;
    private String advThrBankCode;

    //TEXT INFO
    private String bankInstructions;
    private String docInstruction;
    private String conditions;
    private String sendRecInfo;
    private String periodOfPresentation;//textInfo
    private String chargeDescription;//textInfo

    private Boolean transferable;
    private Boolean revolving;
    private Boolean revokable;
    private Boolean backToBack;
    private Boolean partShipment;
    private Boolean redLetter;
    private Boolean tranShipment;
    private Boolean deferred;
    private Date dateOfExpiry;
    private String countryOfExpiry;
    private String cityOfExpiry;

    private Boolean reimbursementMessage;
    private Boolean reimbursementAppRules;//dropdown NOTURR, URR Latest Version
    private String reimbursingBankName;
    private String reimbursingBankAcct;//bank
    private String reimbursingBankBranch;
    private String reimbursingBankAddress;
    private String reimbursingSwiftCode;
    private String reimbursingBankIdent;
    private String chargesBorneBy;//dropdown A-Applicant, B-Beneficiary


    private String availWithBankName;
    private String availWithBankBranch;
    private String availWithBankAddress;
    private String availWithSwiftCode;
    private String availWithBankCode;
    private String availWithBankIdent;

    private String insurCompName;
    private String insurCompAddr;
    private String insurPolicyNo;

    private String insurPolicyDates;
    private String insurPayableAt;
    private String insurDateOfExpirys;

    private String insurPercentage;
    private BigDecimal insuredAmount;
    private BigDecimal premiumAmount;
    private String insuredBy;

    private CodeDTO lcPaymentTenor;
    private String usancePeriod;
    private String beneficiaryName;
    private String beneficiaryAddress;
    private String beneficiaryTown;
    private String beneficiaryAccount;
    private String beneficiaryBankName;
    private String beneficiaryCountry;
    private String beneficiaryState;
    private String beneficiaryBankBranch;
    private String beneficiaryBankAddress;
    private String beneficiarySwiftCode;


    private String lcValidCountry;
    private String beneficiaryCharges;

    private String portOfLoading;
    private String placeOfFinalDest;
    private String portOfDischarge;
    private String shipmentPeriod;//freetext - cust
    private String shipmentTerms;
    private String shipmentMode;//Code - cust
    private String originOfGoods;

    private String tolerancePos;
    private String toleranceNeg;
    private BigDecimal rate;
    private String bidRateCode;
    private BigDecimal lcAvailableAmount;
    private String lastShipment;

    private String lastShipmentDates;
    private int numberOfAmendments;
    private int numberOfReinstatement;
    private String userType;
    private String InitiatedBy;
    private Date dateCreated;
    private Boolean guidelineApproval;

    //New fields
    private String tranId;
    private String sortCode; //fetch the branchid of the approving bank
    private String applicableRules;//codes
    private String applicableSubRule;//if "others" was chosen- bank
    private String tenorDetails;//textInfo - bank
    private String corrNostroAccount;
    private String applicantName;
    private String applicantAddress;
    private String applicantCity;
    private String applicantState;
    private String applicantCountry;
    private BigDecimal allocatedAmount;
    private BigDecimal utilizedAmount;

    private String draweeBankName;
    private String draweeBankAddr;
    private String draweeBankSwiftCode;
    private String draweeBankCtry;
    private String draweeBankState;
    private String draweeBank;
    private String draweeBankCity;
    private String draweeBankBr;
    private String draweeBankPoCode;
    private String draweeBankIdent;//code dropdown remittance

    private String instruction;
    private String instructionBy;//dropdown A-Applicant, B-Beneficiary, C-Correspondent Bank, N-Collecting Bank, O-Others, S-Self
    private Date instructionDate;

    private String issuingBank;
    private String issuingBankBrnch;
    private String issuingBankSwift;
    private String issuingBankCity;
    private String issuingBankState;
    private String issuingBankCode;
    private String issuingBankAddr;
    private String issuingBankCountry;
    private boolean formDetRequired;
    private BigDecimal outstanding;
    private Boolean utilWithoutBill;
    private String paysysId;
    private Boolean utilbyBill;
    private String confirmBy;//dropdown
    private CodeDTO confirmInstruct;
    private String availableBy;
    private String availableWith;

    private String negeriodDay;
    private String negPeriodMonth;
    private String foreignBankRefNum;
    private String backTobackLcNo;
    private Boolean standBy;

    private String docSubBranch;

    //    private List<Comments> comment = new ArrayList<>();
    private String status;
    private String cifid;
    private Date createdOn;
    private BigDecimal dollarEquivalent;
    private String formStatus;
    private String concessionFlag;
    private Double concessionPercentage;
    private String bankCode;
    private String submitFlag;

    private String operativeAccount;
    private String chargeAcct;


    public ExportLcDTO(){}


    public String getOperativeAccount() {
        return operativeAccount;
    }

    public void setOperativeAccount(String operativeAccount) {
        this.operativeAccount = operativeAccount;
    }

    public String getChargeAcct() {
        return chargeAcct;
    }

    public void setChargeAcct(String chargeAcct) {
        this.chargeAcct = chargeAcct;
    }

    public Boolean getIrrevokable() {
        return irrevokable;
    }

    public void setIrrevokable(Boolean irrevokable) {
        this.irrevokable = irrevokable;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getInstructionDates() {
        return instructionDates;
    }

    public void setInstructionDates(String instructionDates) {
        this.instructionDates = instructionDates;
    }

    public String getSubmitFlag() {
        return submitFlag;
    }

    public void setSubmitFlag(String submitFlag) {
        this.submitFlag = submitFlag;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getFormNxpId() {
        return formNxpId;
    }

    public void setFormNxpId(String formNxpId) {
        this.formNxpId = formNxpId;
    }

    public String getFormNxpNumber() {
        return formNxpNumber;
    }

    public void setFormNxpNumber(String formNxpNumber) {
        this.formNxpNumber = formNxpNumber;
    }

    public String getLcTypeName() {
        return lcTypeName;
    }

    public void setLcTypeName(String lcTypeName) {
        this.lcTypeName = lcTypeName;
    }

    public String getTypeOfLc() {
        return typeOfLc;
    }

    public void setTypeOfLc(String typeOfLc) {
        this.typeOfLc = typeOfLc;
    }

    public String getAccountToChargeId() {
        return accountToChargeId;
    }

    public void setAccountToChargeId(String accountToChargeId) {
        this.accountToChargeId = accountToChargeId;
    }

    public String getAccountToChargeNumber() {
        return accountToChargeNumber;
    }

    public void setAccountToChargeNumber(String accountToChargeNumber) {
        this.accountToChargeNumber = accountToChargeNumber;
    }

    public String getAccountToCharge() {
        return accountToCharge;
    }

    public void setAccountToCharge(String accountToCharge) {
        this.accountToCharge = accountToCharge;
    }

    public String getAccountForTransNumber() {
        return accountForTransNumber;
    }

    public void setAccountForTransNumber(String accountForTransNumber) {
        this.accountForTransNumber = accountForTransNumber;
    }

    public String getAccountForTransId() {
        return accountForTransId;
    }

    public void setAccountForTransId(String accountForTransId) {
        this.accountForTransId = accountForTransId;
    }

    public String getAccountForTrans() {
        return accountForTrans;
    }

    public void setAccountForTrans(String accountForTrans) {
        this.accountForTrans = accountForTrans;
    }

    public BigDecimal getPartCleanline() {
        return partCleanline;
    }

    public void setPartCleanline(BigDecimal partCleanline) {
        this.partCleanline = partCleanline;
    }

    public BigDecimal getPartConfirmed() {
        return partConfirmed;
    }

    public void setPartConfirmed(BigDecimal partConfirmed) {
        this.partConfirmed = partConfirmed;
    }

    public BigDecimal getPartUnconfirmed() {
        return partUnconfirmed;
    }

    public void setPartUnconfirmed(BigDecimal partUnconfirmed) {
        this.partUnconfirmed = partUnconfirmed;
    }

    public String getAdvBankIdent() {
        return advBankIdent;
    }

    public void setAdvBankIdent(String advBankIdent) {
        this.advBankIdent = advBankIdent;
    }

    public String getAdvThrBankIdent() {
        return advThrBankIdent;
    }

    public void setAdvThrBankIdent(String advThrBankIdent) {
        this.advThrBankIdent = advThrBankIdent;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getInsurPolicyDate() {
        return insurPolicyDate;
    }

    public void setInsurPolicyDate(Date insurPolicyDate) {
        this.insurPolicyDate = insurPolicyDate;
    }

    public Date getInsurDateOfExpiry() {
        return insurDateOfExpiry;
    }

    public void setInsurDateOfExpiry(Date insurDateOfExpiry) {
        this.insurDateOfExpiry = insurDateOfExpiry;
    }

    public String getInsurExpiryDate() {
        return insurExpiryDate;
    }

    public void setInsurExpiryDate(String insurExpiryDate) {
        this.insurExpiryDate = insurExpiryDate;
    }

    public String getLcPaymentTenorId() {
        return lcPaymentTenorId;
    }

    public void setLcPaymentTenorId(String lcPaymentTenorId) {
        this.lcPaymentTenorId = lcPaymentTenorId;
    }

    public String getNegotiationPeriod() {
        return negotiationPeriod;
    }

    public void setNegotiationPeriod(String negotiationPeriod) {
        this.negotiationPeriod = negotiationPeriod;
    }

    public String getConcession() {
        return concession;
    }

    public void setConcession(String concession) {
        this.concession = concession;
    }

    public String getDocCreditType() {
        return docCreditType;
    }

    public void setDocCreditType(String docCreditType) {
        this.docCreditType = docCreditType;
    }

    public List<ExportLcDocumentDTO> getDocuments() {
        return documents;
    }

    public void setDocuments(List<ExportLcDocumentDTO> documents) {
        this.documents = documents;
    }

    public Date getLastShipmentDate() {
        return lastShipmentDate;
    }

    public void setLastShipmentDate(Date lastShipmentDate) {
        this.lastShipmentDate = lastShipmentDate;
    }

    public String getAmendAction() {
        return amendAction;
    }

    public void setAmendAction(String amendAction) {
        this.amendAction = amendAction;
    }

    public String getConfirmationType() {
        return confirmationType;
    }

    public void setConfirmationType(String confirmationType) {
        this.confirmationType = confirmationType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getWorkFlowId() {
        return workFlowId;
    }

    public void setWorkFlowId(long workFlowId) {
        this.workFlowId = workFlowId;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getSubmitAction() {
        return submitAction;
    }

    public void setSubmitAction(String submitAction) {
        this.submitAction = submitAction;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Boolean getGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(Boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public String getApplicableRules() {
        return applicableRules;
    }

    public void setApplicableRules(String applicableRules) {
        this.applicableRules = applicableRules;
    }

    public String getApplicableSubRule() {
        return applicableSubRule;
    }

    public void setApplicableSubRule(String applicableSubRule) {
        this.applicableSubRule = applicableSubRule;
    }

    public String getTenorDetails() {
        return tenorDetails;
    }

    public void setTenorDetails(String tenorDetails) {
        this.tenorDetails = tenorDetails;
    }

    public String getCorrNostroAccount() {
        return corrNostroAccount;
    }

    public void setCorrNostroAccount(String corrNostroAccount) {
        this.corrNostroAccount = corrNostroAccount;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getApplicantAddress() {
        return applicantAddress;
    }

    public void setApplicantAddress(String applicantAddress) {
        this.applicantAddress = applicantAddress;
    }

    public String getApplicantCity() {
        return applicantCity;
    }

    public void setApplicantCity(String applicantCity) {
        this.applicantCity = applicantCity;
    }

    public String getApplicantState() {
        return applicantState;
    }

    public void setApplicantState(String applicantState) {
        this.applicantState = applicantState;
    }

    public String getApplicantCountry() {
        return applicantCountry;
    }

    public void setApplicantCountry(String applicantCountry) {
        this.applicantCountry = applicantCountry;
    }

    public BigDecimal getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(BigDecimal allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public BigDecimal getUtilizedAmount() {
        return utilizedAmount;
    }

    public void setUtilizedAmount(BigDecimal utilizedAmount) {
        this.utilizedAmount = utilizedAmount;
    }

    public String getDraweeBankName() {
        return draweeBankName;
    }

    public void setDraweeBankName(String draweeBankName) {
        this.draweeBankName = draweeBankName;
    }

    public String getDraweeBankAddr() {
        return draweeBankAddr;
    }

    public void setDraweeBankAddr(String draweeBankAddr) {
        this.draweeBankAddr = draweeBankAddr;
    }

    public String getDraweeBankSwiftCode() {
        return draweeBankSwiftCode;
    }

    public void setDraweeBankSwiftCode(String draweeBankSwiftCode) {
        this.draweeBankSwiftCode = draweeBankSwiftCode;
    }

    public String getDraweeBankCtry() {
        return draweeBankCtry;
    }

    public void setDraweeBankCtry(String draweeBankCtry) {
        this.draweeBankCtry = draweeBankCtry;
    }

    public String getDraweeBankState() {
        return draweeBankState;
    }

    public void setDraweeBankState(String draweeBankState) {
        this.draweeBankState = draweeBankState;
    }

    public String getDraweeBank() {
        return draweeBank;
    }

    public void setDraweeBank(String draweeBank) {
        this.draweeBank = draweeBank;
    }

    public String getDraweeBankCity() {
        return draweeBankCity;
    }

    public void setDraweeBankCity(String draweeBankCity) {
        this.draweeBankCity = draweeBankCity;
    }

    public String getDraweeBankBr() {
        return draweeBankBr;
    }

    public void setDraweeBankBr(String draweeBankBr) {
        this.draweeBankBr = draweeBankBr;
    }

    public String getDraweeBankPoCode() {
        return draweeBankPoCode;
    }

    public void setDraweeBankPoCode(String draweeBankPoCode) {
        this.draweeBankPoCode = draweeBankPoCode;
    }

    public String getDraweeBankIdent() {
        return draweeBankIdent;
    }

    public void setDraweeBankIdent(String draweeBankIdent) {
        this.draweeBankIdent = draweeBankIdent;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getInstructionBy() {
        return instructionBy;
    }

    public void setInstructionBy(String instructionBy) {
        this.instructionBy = instructionBy;
    }

    public Date getInstructionDate() {
        return instructionDate;
    }

    public void setInstructionDate(Date instructionDate) {
        this.instructionDate = instructionDate;
    }

    public String getIssuingBank() {
        return issuingBank;
    }

    public void setIssuingBank(String issuingBank) {
        this.issuingBank = issuingBank;
    }

    public String getIssuingBankBrnch() {
        return issuingBankBrnch;
    }

    public void setIssuingBankBrnch(String issuingBankBrnch) {
        this.issuingBankBrnch = issuingBankBrnch;
    }

    public String getIssuingBankSwift() {
        return issuingBankSwift;
    }

    public void setIssuingBankSwift(String issuingBankSwift) {
        this.issuingBankSwift = issuingBankSwift;
    }

    public String getIssuingBankCity() {
        return issuingBankCity;
    }

    public void setIssuingBankCity(String issuingBankCity) {
        this.issuingBankCity = issuingBankCity;
    }

    public String getIssuingBankState() {
        return issuingBankState;
    }

    public void setIssuingBankState(String issuingBankState) {
        this.issuingBankState = issuingBankState;
    }

    public String getIssuingBankCode() {
        return issuingBankCode;
    }

    public void setIssuingBankCode(String issuingBankCode) {
        this.issuingBankCode = issuingBankCode;
    }

    public String getIssuingBankAddr() {
        return issuingBankAddr;
    }

    public void setIssuingBankAddr(String issuingBankAddr) {
        this.issuingBankAddr = issuingBankAddr;
    }

    public String getIssuingBankCountry() {
        return issuingBankCountry;
    }

    public void setIssuingBankCountry(String issuingBankCountry) {
        this.issuingBankCountry = issuingBankCountry;
    }

    public boolean isFormDetRequired() {
        return formDetRequired;
    }

    public void setFormDetRequired(boolean formDetRequired) {
        this.formDetRequired = formDetRequired;
    }

    public BigDecimal getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(BigDecimal outstanding) {
        this.outstanding = outstanding;
    }

    public Boolean getUtilWithoutBill() {
        return utilWithoutBill;
    }

    public void setUtilWithoutBill(Boolean utilWithoutBill) {
        this.utilWithoutBill = utilWithoutBill;
    }

    public String getPaysysId() {
        return paysysId;
    }

    public void setPaysysId(String paysysId) {
        this.paysysId = paysysId;
    }

    public Boolean getUtilbyBill() {
        return utilbyBill;
    }

    public void setUtilbyBill(Boolean utilbyBill) {
        this.utilbyBill = utilbyBill;
    }

    public String getConfirmBy() {
        return confirmBy;
    }

    public void setConfirmBy(String confirmBy) {
        this.confirmBy = confirmBy;
    }

    public CodeDTO getConfirmInstruct() {
        return confirmInstruct;
    }

    public void setConfirmInstruct(CodeDTO confirmInstruct) {
        this.confirmInstruct = confirmInstruct;
    }

    public String getAvailableBy() {
        return availableBy;
    }

    public void setAvailableBy(String availableBy) {
        this.availableBy = availableBy;
    }

    public String getAvailableWith() {
        return availableWith;
    }

    public void setAvailableWith(String availableWith) {
        this.availableWith = availableWith;
    }

    public String getNegeriodDay() {
        return negeriodDay;
    }

    public void setNegeriodDay(String negeriodDay) {
        this.negeriodDay = negeriodDay;
    }

    public String getNegPeriodMonth() {
        return negPeriodMonth;
    }

    public void setNegPeriodMonth(String negPeriodMonth) {
        this.negPeriodMonth = negPeriodMonth;
    }

    public String getForeignBankRefNum() {
        return foreignBankRefNum;
    }

    public void setForeignBankRefNum(String foreignBankRefNum) {
        this.foreignBankRefNum = foreignBankRefNum;
    }

    public String getBackTobackLcNo() {
        return backTobackLcNo;
    }

    public void setBackTobackLcNo(String backTobackLcNo) {
        this.backTobackLcNo = backTobackLcNo;
    }

    public Boolean getStandBy() {
        return standBy;
    }

    public void setStandBy(Boolean standBy) {
        this.standBy = standBy;
    }

    public String getDocSubBranch() {
        return docSubBranch;
    }

    public void setDocSubBranch(String docSubBranch) {
        this.docSubBranch = docSubBranch;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public BigDecimal getDollarEquivalent() {
        return dollarEquivalent;
    }

    public void setDollarEquivalent(BigDecimal dollarEquivalent) {
        this.dollarEquivalent = dollarEquivalent;
    }

    public String getFormStatus() {
        return formStatus;
    }

    public void setFormStatus(String formStatus) {
        this.formStatus = formStatus;
    }

    public String getConcessionFlag() {
        return concessionFlag;
    }

    public void setConcessionFlag(String concessionFlag) {
        this.concessionFlag = concessionFlag;
    }

    public Double getConcessionPercentage() {
        return concessionPercentage;
    }

    public void setConcessionPercentage(Double concessionPercentage) {
        this.concessionPercentage = concessionPercentage;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBrDesc() {
        return brDesc;
    }

    public void setBrDesc(String brDesc) {
        this.brDesc = brDesc;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getLcNumber() {
        return lcNumber;
    }

    public void setLcNumber(String lcNumber) {
        this.lcNumber = lcNumber;
    }

    public String getTempLcNumber() {
        return tempLcNumber;
    }

    public void setTempLcNumber(String tempLcNumber) {
        this.tempLcNumber = tempLcNumber;
    }

    public String getFormM() {
        return formM;
    }

    public void setFormM(String formM) {
        this.formM = formM;
    }

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public String getTransAccount() {
        return transAccount;
    }

    public void setTransAccount(String transAccount) {
        this.transAccount = transAccount;
    }

    public String getLcType() {
        return lcType;
    }

    public void setLcType(String lcType) {
        this.lcType = lcType;
    }

    public BigDecimal getLcAmount() {
        return lcAmount;
    }

    public void setLcAmount(BigDecimal lcAmount) {
        this.lcAmount = lcAmount;
    }

    public String getLcAccount() {
        return lcAccount;
    }

    public void setLcAccount(String lcAccount) {
        this.lcAccount = lcAccount;
    }

    public String getLcCurrency() {
        return lcCurrency;
    }

    public void setLcCurrency(String lcCurrency) {
        this.lcCurrency = lcCurrency;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getAdvBankName() {
        return advBankName;
    }

    public void setAdvBankName(String advBankName) {
        this.advBankName = advBankName;
    }

    public String getAdvBankBranch() {
        return advBankBranch;
    }

    public void setAdvBankBranch(String advBankBranch) {
        this.advBankBranch = advBankBranch;
    }

    public String getAdvBankAddress() {
        return advBankAddress;
    }

    public void setAdvBankAddress(String advBankAddress) {
        this.advBankAddress = advBankAddress;
    }

    public String getAdvSwiftCode() {
        return advSwiftCode;
    }

    public void setAdvSwiftCode(String advSwiftCode) {
        this.advSwiftCode = advSwiftCode;
    }

    public String getAdvBankCode() {
        return advBankCode;
    }

    public void setAdvBankCode(String advBankCode) {
        this.advBankCode = advBankCode;
    }

    public String getAdvThrBankName() {
        return advThrBankName;
    }

    public void setAdvThrBankName(String advThrBankName) {
        this.advThrBankName = advThrBankName;
    }

    public String getAdvThrBankBranch() {
        return advThrBankBranch;
    }

    public void setAdvThrBankBranch(String advThrBankBranch) {
        this.advThrBankBranch = advThrBankBranch;
    }

    public String getAdvThrBankAddress() {
        return advThrBankAddress;
    }

    public void setAdvThrBankAddress(String advThrBankAddress) {
        this.advThrBankAddress = advThrBankAddress;
    }

    public String getAdvThrSwiftCode() {
        return advThrSwiftCode;
    }

    public void setAdvThrSwiftCode(String advThrSwiftCode) {
        this.advThrSwiftCode = advThrSwiftCode;
    }

    public String getAdvThrBankCode() {
        return advThrBankCode;
    }

    public void setAdvThrBankCode(String advThrBankCode) {
        this.advThrBankCode = advThrBankCode;
    }

    public String getBankInstructions() {
        return bankInstructions;
    }

    public void setBankInstructions(String bankInstructions) {
        this.bankInstructions = bankInstructions;
    }

    public String getDocInstruction() {
        return docInstruction;
    }

    public void setDocInstruction(String docInstruction) {
        this.docInstruction = docInstruction;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getSendRecInfo() {
        return sendRecInfo;
    }

    public void setSendRecInfo(String sendRecInfo) {
        this.sendRecInfo = sendRecInfo;
    }

    public String getPeriodOfPresentation() {
        return periodOfPresentation;
    }

    public void setPeriodOfPresentation(String periodOfPresentation) {
        this.periodOfPresentation = periodOfPresentation;
    }

    public String getChargeDescription() {
        return chargeDescription;
    }

    public void setChargeDescription(String chargeDescription) {
        this.chargeDescription = chargeDescription;
    }

    public Boolean getTransferable() {
        return transferable;
    }

    public void setTransferable(Boolean transferable) {
        this.transferable = transferable;
    }

    public Boolean getRevolving() {
        return revolving;
    }

    public void setRevolving(Boolean revolving) {
        this.revolving = revolving;
    }

    public Boolean getRevokable() {
        return revokable;
    }

    public void setRevokable(Boolean revokable) {
        this.revokable = revokable;
    }

    public Boolean getBackToBack() {
        return backToBack;
    }

    public void setBackToBack(Boolean backToBack) {
        this.backToBack = backToBack;
    }

    public Boolean getPartShipment() {
        return partShipment;
    }

    public void setPartShipment(Boolean partShipment) {
        this.partShipment = partShipment;
    }

    public Boolean getRedLetter() {
        return redLetter;
    }

    public void setRedLetter(Boolean redLetter) {
        this.redLetter = redLetter;
    }

    public Boolean getTranShipment() {
        return tranShipment;
    }

    public void setTranShipment(Boolean tranShipment) {
        this.tranShipment = tranShipment;
    }

    public Boolean getDeferred() {
        return deferred;
    }

    public void setDeferred(Boolean deferred) {
        this.deferred = deferred;
    }

    public Date getDateOfExpiry() {
        return dateOfExpiry;
    }

    public void setDateOfExpiry(Date dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }

    public String getCountryOfExpiry() {
        return countryOfExpiry;
    }

    public void setCountryOfExpiry(String countryOfExpiry) {
        this.countryOfExpiry = countryOfExpiry;
    }

    public String getCityOfExpiry() {
        return cityOfExpiry;
    }

    public void setCityOfExpiry(String cityOfExpiry) {
        this.cityOfExpiry = cityOfExpiry;
    }

    public Boolean getReimbursementMessage() {
        return reimbursementMessage;
    }

    public void setReimbursementMessage(Boolean reimbursementMessage) {
        this.reimbursementMessage = reimbursementMessage;
    }

    public Boolean getReimbursementAppRules() {
        return reimbursementAppRules;
    }

    public void setReimbursementAppRules(Boolean reimbursementAppRules) {
        this.reimbursementAppRules = reimbursementAppRules;
    }

    public String getReimbursingBankName() {
        return reimbursingBankName;
    }

    public void setReimbursingBankName(String reimbursingBankName) {
        this.reimbursingBankName = reimbursingBankName;
    }

    public String getReimbursingBankAcct() {
        return reimbursingBankAcct;
    }

    public void setReimbursingBankAcct(String reimbursingBankAcct) {
        this.reimbursingBankAcct = reimbursingBankAcct;
    }

    public String getReimbursingBankBranch() {
        return reimbursingBankBranch;
    }

    public void setReimbursingBankBranch(String reimbursingBankBranch) {
        this.reimbursingBankBranch = reimbursingBankBranch;
    }

    public String getReimbursingBankAddress() {
        return reimbursingBankAddress;
    }

    public void setReimbursingBankAddress(String reimbursingBankAddress) {
        this.reimbursingBankAddress = reimbursingBankAddress;
    }

    public String getReimbursingSwiftCode() {
        return reimbursingSwiftCode;
    }

    public void setReimbursingSwiftCode(String reimbursingSwiftCode) {
        this.reimbursingSwiftCode = reimbursingSwiftCode;
    }

    public String getReimbursingBankIdent() {
        return reimbursingBankIdent;
    }

    public void setReimbursingBankIdent(String reimbursingBankIdent) {
        this.reimbursingBankIdent = reimbursingBankIdent;
    }

    public String getChargesBorneBy() {
        return chargesBorneBy;
    }

    public void setChargesBorneBy(String chargesBorneBy) {
        this.chargesBorneBy = chargesBorneBy;
    }

    public String getAvailWithBankName() {
        return availWithBankName;
    }

    public void setAvailWithBankName(String availWithBankName) {
        this.availWithBankName = availWithBankName;
    }

    public String getAvailWithBankBranch() {
        return availWithBankBranch;
    }

    public void setAvailWithBankBranch(String availWithBankBranch) {
        this.availWithBankBranch = availWithBankBranch;
    }

    public String getAvailWithBankAddress() {
        return availWithBankAddress;
    }

    public void setAvailWithBankAddress(String availWithBankAddress) {
        this.availWithBankAddress = availWithBankAddress;
    }

    public String getAvailWithSwiftCode() {
        return availWithSwiftCode;
    }

    public void setAvailWithSwiftCode(String availWithSwiftCode) {
        this.availWithSwiftCode = availWithSwiftCode;
    }

    public String getAvailWithBankCode() {
        return availWithBankCode;
    }

    public void setAvailWithBankCode(String availWithBankCode) {
        this.availWithBankCode = availWithBankCode;
    }

    public String getAvailWithBankIdent() {
        return availWithBankIdent;
    }

    public void setAvailWithBankIdent(String availWithBankIdent) {
        this.availWithBankIdent = availWithBankIdent;
    }

    public String getInsurCompName() {
        return insurCompName;
    }

    public void setInsurCompName(String insurCompName) {
        this.insurCompName = insurCompName;
    }

    public String getInsurCompAddr() {
        return insurCompAddr;
    }

    public void setInsurCompAddr(String insurCompAddr) {
        this.insurCompAddr = insurCompAddr;
    }

    public String getInsurPolicyNo() {
        return insurPolicyNo;
    }

    public void setInsurPolicyNo(String insurPolicyNo) {
        this.insurPolicyNo = insurPolicyNo;
    }

    public String getInsurPolicyDates() {
        return insurPolicyDates;
    }

    public void setInsurPolicyDates(String insurPolicyDates) {
        this.insurPolicyDates = insurPolicyDates;
    }

    public String getInsurPayableAt() {
        return insurPayableAt;
    }

    public void setInsurPayableAt(String insurPayableAt) {
        this.insurPayableAt = insurPayableAt;
    }

    public String getInsurDateOfExpirys() {
        return insurDateOfExpirys;
    }

    public void setInsurDateOfExpirys(String insurDateOfExpirys) {
        this.insurDateOfExpirys = insurDateOfExpirys;
    }

    public String getInsurPercentage() {
        return insurPercentage;
    }

    public void setInsurPercentage(String insurPercentage) {
        this.insurPercentage = insurPercentage;
    }

    public BigDecimal getInsuredAmount() {
        return insuredAmount;
    }

    public void setInsuredAmount(BigDecimal insuredAmount) {
        this.insuredAmount = insuredAmount;
    }

    public BigDecimal getPremiumAmount() {
        return premiumAmount;
    }

    public void setPremiumAmount(BigDecimal premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    public String getInsuredBy() {
        return insuredBy;
    }

    public void setInsuredBy(String insuredBy) {
        this.insuredBy = insuredBy;
    }

    public CodeDTO getLcPaymentTenor() {
        return lcPaymentTenor;
    }

    public void setLcPaymentTenor(CodeDTO lcPaymentTenor) {
        this.lcPaymentTenor = lcPaymentTenor;
    }

    public String getUsancePeriod() {
        return usancePeriod;
    }

    public void setUsancePeriod(String usancePeriod) {
        this.usancePeriod = usancePeriod;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryAddress() {
        return beneficiaryAddress;
    }

    public void setBeneficiaryAddress(String beneficiaryAddress) {
        this.beneficiaryAddress = beneficiaryAddress;
    }

    public String getBeneficiaryTown() {
        return beneficiaryTown;
    }

    public void setBeneficiaryTown(String beneficiaryTown) {
        this.beneficiaryTown = beneficiaryTown;
    }

    public String getBeneficiaryAccount() {
        return beneficiaryAccount;
    }

    public void setBeneficiaryAccount(String beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
    }

    public String getBeneficiaryBankName() {
        return beneficiaryBankName;
    }

    public void setBeneficiaryBankName(String beneficiaryBankName) {
        this.beneficiaryBankName = beneficiaryBankName;
    }

    public String getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    public void setBeneficiaryCountry(String beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
    }

    public String getBeneficiaryState() {
        return beneficiaryState;
    }

    public void setBeneficiaryState(String beneficiaryState) {
        this.beneficiaryState = beneficiaryState;
    }

    public String getBeneficiaryBankBranch() {
        return beneficiaryBankBranch;
    }

    public void setBeneficiaryBankBranch(String beneficiaryBankBranch) {
        this.beneficiaryBankBranch = beneficiaryBankBranch;
    }

    public String getBeneficiaryBankAddress() {
        return beneficiaryBankAddress;
    }

    public void setBeneficiaryBankAddress(String beneficiaryBankAddress) {
        this.beneficiaryBankAddress = beneficiaryBankAddress;
    }

    public String getBeneficiarySwiftCode() {
        return beneficiarySwiftCode;
    }

    public void setBeneficiarySwiftCode(String beneficiarySwiftCode) {
        this.beneficiarySwiftCode = beneficiarySwiftCode;
    }

    public String getLcValidCountry() {
        return lcValidCountry;
    }

    public void setLcValidCountry(String lcValidCountry) {
        this.lcValidCountry = lcValidCountry;
    }

    public String getBeneficiaryCharges() {
        return beneficiaryCharges;
    }

    public void setBeneficiaryCharges(String beneficiaryCharges) {
        this.beneficiaryCharges = beneficiaryCharges;
    }

    public String getPortOfLoading() {
        return portOfLoading;
    }

    public void setPortOfLoading(String portOfLoading) {
        this.portOfLoading = portOfLoading;
    }

    public String getPlaceOfFinalDest() {
        return placeOfFinalDest;
    }

    public void setPlaceOfFinalDest(String placeOfFinalDest) {
        this.placeOfFinalDest = placeOfFinalDest;
    }

    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }

    public String getShipmentPeriod() {
        return shipmentPeriod;
    }

    public void setShipmentPeriod(String shipmentPeriod) {
        this.shipmentPeriod = shipmentPeriod;
    }

    public String getShipmentTerms() {
        return shipmentTerms;
    }

    public void setShipmentTerms(String shipmentTerms) {
        this.shipmentTerms = shipmentTerms;
    }

    public String getShipmentMode() {
        return shipmentMode;
    }

    public void setShipmentMode(String shipmentMode) {
        this.shipmentMode = shipmentMode;
    }

    public String getOriginOfGoods() {
        return originOfGoods;
    }

    public void setOriginOfGoods(String originOfGoods) {
        this.originOfGoods = originOfGoods;
    }

    public String getTolerancePos() {
        return tolerancePos;
    }

    public void setTolerancePos(String tolerancePos) {
        this.tolerancePos = tolerancePos;
    }

    public String getToleranceNeg() {
        return toleranceNeg;
    }

    public void setToleranceNeg(String toleranceNeg) {
        this.toleranceNeg = toleranceNeg;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getBidRateCode() {
        return bidRateCode;
    }

    public void setBidRateCode(String bidRateCode) {
        this.bidRateCode = bidRateCode;
    }

    public BigDecimal getLcAvailableAmount() {
        return lcAvailableAmount;
    }

    public void setLcAvailableAmount(BigDecimal lcAvailableAmount) {
        this.lcAvailableAmount = lcAvailableAmount;
    }

    public String getLastShipment() {
        return lastShipment;
    }

    public void setLastShipment(String lastShipment) {
        this.lastShipment = lastShipment;
    }

    public String getLastShipmentDates() {
        return lastShipmentDates;
    }

    public void setLastShipmentDates(String lastShipmentDates) {
        this.lastShipmentDates = lastShipmentDates;
    }

    public int getNumberOfAmendments() {
        return numberOfAmendments;
    }

    public void setNumberOfAmendments(int numberOfAmendments) {
        this.numberOfAmendments = numberOfAmendments;
    }

    public int getNumberOfReinstatement() {
        return numberOfReinstatement;
    }

    public void setNumberOfReinstatement(int numberOfReinstatement) {
        this.numberOfReinstatement = numberOfReinstatement;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
