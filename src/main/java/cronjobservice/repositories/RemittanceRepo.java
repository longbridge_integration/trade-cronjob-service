package cronjobservice.repositories;

import cronjobservice.models.Remittance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chiomarose on 05/10/2017.
 */
@Repository
public interface RemittanceRepo extends CommonRepo<Remittance, Long>
{
    Remittance findByTempFormNumber(String tempFormNumber);
    Remittance findByFormNumber(String formNumber);

    List<Remittance> findByStatusAndSubmitFlag(String S, String SF);
//    @Query("select s from Remittance s where  s.status=:approved")
//    List<Remittance> findRemittanceByStatus(@Param("approved") String approved);

    @Query("select  count(s)  from Remittance s  where  s.cifid=:cifid")
    long  countByCifid(@Param("cifid") String cifid);

    @Query("select s from Remittance s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<Remittance> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);

    @Query("select  count(s)  from Remittance s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    long  countByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline);

    @Query("select s from Remittance s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<Remittance> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    Page<Remittance> findByCifid(String cifid, Pageable pageable);

    @Query("select s from Remittance s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<Remittance> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    @Query("select s from Remittance s where s.cifid=:cifid  and  s.status is not null")
    Page<Remittance> findByClosedCifidAndStatus(@Param("cifid") String cifid, Pageable pageable);

    Page<Remittance> findByStatusNotNull(Pageable pageable);

    //Dashboard Query
//    @Query("select sum(s.transferamount) from Remittance s where s.cifid=:cifId and s.status=:approve")
//    Integer addAmt(@Param("cifId") String cifId, @Param("approve") String approve);

    @Query("select s from Remittance s where s.status is not null and not s.status=:saved ")
    Page<Remittance> findByStatus(@Param("saved") String saved, Pageable pageable);


}
