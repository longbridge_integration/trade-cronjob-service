package cronjobservice.services;


import cronjobservice.dtos.CodeDTO;
import cronjobservice.dtos.RetailUserDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.RetailUser;
import cronjobservice.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

/**
 * The {@code RetailUserService} provides the methods for managing operations users
 * @see RetailUserDTO
 * @see RetailUser
 * @author Fortunatus Ekenachi
 * Created on 3/29/2017.
 */
public interface RetailUserService {

    /**
     *Returns the specified operations user
     * @param id the user's id
     * @return the Operations User
     */
    @PreAuthorize("hasAuthority('GET_ADMIN_USER')")
    RetailUserDTO getUser(Long id);

    /**Checks that the provided username is valid for use as any username already
     * existing will not be valid for another user
     * @param username the username
     * @return true if the username does not exist
     */
    boolean userExists(String username);


    @Async
    void sendActivateMessage(User user, String... args);

    RetailUser getUserByName(String name);

//    String setSecurityDetails(SecQuestionDTO secQuestionDTO);
//
//    SecQuestionDTO getSecurityQuestions(RetailUser retailUser);


    void sendActivationCredentials(RetailUser user, String password);

    void sendPostActivateMessage(User user, String... args);

    /**
     * Returns all Operations users present in th system
     * @return list of user
     */
    @PreAuthorize("hasAuthority('GET_ADMIN_USER')")
    Iterable<RetailUserDTO> getUsers();

    List<RetailUserDTO> getByCustomerSegment(CodeDTO custSegment);

//    @PreAuthorize("hasAuthority('UNLOCK_RETAIL_USER')")
//    String unlockUser(Long id) throws CronJobException;

    /**
     * Returns a paginated list of users
     * @param pageDetails the page details
     * @return list of users
     */
    @PreAuthorize("hasAuthority('GET_ADMIN_USER')")
    Page<RetailUserDTO> getUsers(Pageable pageDetails);

    @PreAuthorize("hasAuthority('GET_ADMIN_USER')")
    Page<RetailUserDTO> findUsers(RetailUserDTO example, Pageable pageDetails);

    RetailUser getUserByEmail(String email);

    @PreAuthorize("hasAuthority('GET_ADMIN_USER')")
    Page<RetailUserDTO> findUsers(String pattern, Pageable pageDetails);






    /**
     * Replaces the old password with the new password
     * Also, the password must meet the organization's password policy if any one has been defined
     * It is important that the password is hashed before storing it in the database.
     * * @param oldPassword the oldPassword
     * @param changePassword
     */
//    @PreAuthorize("hasAuthority('ADMIN_CHANGE_PASSWORD')")
//    String changePassword(RetailUser user, ChangePassword changePassword) throws CronJobException;
//
//    String changeDefaultPassword(RetailUser user, ChangeDefaultPassword changePassword) throws PasswordException;
//
//    String resetPassword(String username) throws CronJobException;
}
