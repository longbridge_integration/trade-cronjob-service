package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * Created by SYLVESTER on 10/18/2017.
 */

@Entity
@Where(clause = "del_Flag='N'")
@Audited(withModifiedFlag = true)
public class InwardGuarantee extends Process {

    private String guaranteeNumber;
    private String initiatedBy;


    //Beneficiary Details
    private String beneName;
    private String beneAddress1;
    private String beneAddress2;
    private String beneState;
    private String beneCountry;
    private String beneCity;
    private String postalCode;
    private String refNo;

    //Bank Beneficiary Details
    private String beneBankName;
    private String beneBankAddr;
    private String beneBankSwiftCode;
    private String beneBankCtry;
    private String beneBankCity;
    private String beneBankState;
    private String beneBankPoCode;
    private String beneBankIdent;//code dropdown remitance



    //Amount Details
    private BigDecimal GuaranteeAmount;
    private Date issueOn;
    private String bankRefNo;

    //Applicant Details
//    private String cifid;
    private String appName;
    private String appAddress1;
    private String appAddress2;
    private String appState;
    private String appCountry;
    private String appCity;
    private String apppostalCode;
    private String apprefNo;
    private String appBank;
    private String appBranch;

    //Guarantee Details
    private String purposeOfGuarantee;
    private String guaranteeClass;
    private String effectiveOn;
    private String subType;
    private String guaranteeType;
    private String adviceD;

    //Other details
    private String otherDetailsRefNo;
    private String otherDetailsRefDate;
    private String chargesBorneBy;

    @Lob
    private String detailsOfGuarantee;
    private String senderToReciever;
    private String remarks;


    //Expiry Details
    private String expiryOn;
    //expiry period is montha nd date
    private String expiryPeriod;
    private String expiryMonth;
    private String expiryDay;
    private String claimExpiryOn;
    private String claimPeriod;


    //Rules
    private String applicableRules;
    private String bgApplicableSubRules;

    //Counter guarantee Details
    private String linkToCounter;
    private String guaranteeNo;
    private String guaranteeExpiryD;
    private String bankCode;
    private String rateCode;
    private String guranteeStatus;

    private String branchCode;
    private String rate;


    private String operativeAcct;
    private String currency;
    //Corresponding bank Details
    private String corrBankCode;
    private String corrBranchId;
    private String corrBankName;
    private String corrBankAddr;
    private String corrBankSwiftCode;
    private String corrBankCtry;
    private String corrBankState;
    private String corrBankCity;
    private String corrBankPoCode;
    private String corrBankIdent;//code dropdown remittance

    //Collecting bank Details
    private String issueBankName;
    private String issueBankAddr;
    private String issueBankSwiftCode;
    private String issueBankCtry;
    private String issueBankState;
    private String issueBankCity;
    private String issueBankPoCode;
    private String issueBankIdent;//code dropdown remittance

    //free code details
    /**
     * fetch freeCode list from finacle
     * Bank to enter free Codes
     */
    private String freeCode1;
    private String freeCode2;
    private String freeCode3;


    /**
     * charges
     */
    private String chargeAccount; //customer nominate the account to be charged

    //to be filled by bank
    private String eventRate;
    private String eventRateCode;
    private String eventAmtCrncy;
    private String eventAmt;
    private String chrgWaiveFlg;

    private String tempFormNumber;
    private String userType;
    private String approveBy;


    public String getGuaranteeNumber() {
        return guaranteeNumber;
    }

    public void setGuaranteeNumber(String guaranteeNumber) {
        this.guaranteeNumber = guaranteeNumber;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public String getBeneName() {
        return beneName;
    }

    public void setBeneName(String beneName) {
        this.beneName = beneName;
    }

    public String getBeneAddress1() {
        return beneAddress1;
    }

    public void setBeneAddress1(String beneAddress1) {
        this.beneAddress1 = beneAddress1;
    }

    public String getBeneAddress2() {
        return beneAddress2;
    }

    public void setBeneAddress2(String beneAddress2) {
        this.beneAddress2 = beneAddress2;
    }

    public String getBeneState() {
        return beneState;
    }

    public void setBeneState(String beneState) {
        this.beneState = beneState;
    }

    public String getBeneCountry() {
        return beneCountry;
    }

    public void setBeneCountry(String beneCountry) {
        this.beneCountry = beneCountry;
    }

    public String getBeneCity() {
        return beneCity;
    }

    public void setBeneCity(String beneCity) {
        this.beneCity = beneCity;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getBeneBankName() {
        return beneBankName;
    }

    public void setBeneBankName(String beneBankName) {
        this.beneBankName = beneBankName;
    }

    public String getBeneBankAddr() {
        return beneBankAddr;
    }

    public void setBeneBankAddr(String beneBankAddr) {
        this.beneBankAddr = beneBankAddr;
    }

    public String getBeneBankSwiftCode() {
        return beneBankSwiftCode;
    }

    public void setBeneBankSwiftCode(String beneBankSwiftCode) {
        this.beneBankSwiftCode = beneBankSwiftCode;
    }

    public String getBeneBankCtry() {
        return beneBankCtry;
    }

    public void setBeneBankCtry(String beneBankCtry) {
        this.beneBankCtry = beneBankCtry;
    }

    public String getBeneBankCity() {
        return beneBankCity;
    }

    public void setBeneBankCity(String beneBankCity) {
        this.beneBankCity = beneBankCity;
    }

    public String getBeneBankState() {
        return beneBankState;
    }

    public void setBeneBankState(String beneBankState) {
        this.beneBankState = beneBankState;
    }

    public String getBeneBankPoCode() {
        return beneBankPoCode;
    }

    public void setBeneBankPoCode(String beneBankPoCode) {
        this.beneBankPoCode = beneBankPoCode;
    }

    public String getBeneBankIdent() {
        return beneBankIdent;
    }

    public void setBeneBankIdent(String beneBankIdent) {
        this.beneBankIdent = beneBankIdent;
    }

    public BigDecimal getGuaranteeAmount() {
        return GuaranteeAmount;
    }

    public void setGuaranteeAmount(BigDecimal guaranteeAmount) {
        GuaranteeAmount = guaranteeAmount;
    }

    public Date getIssueOn() {
        return issueOn;
    }

    public void setIssueOn(Date issueOn) {
        this.issueOn = issueOn;
    }

    public String getBankRefNo() {
        return bankRefNo;
    }

    public void setBankRefNo(String bankRefNo) {
        this.bankRefNo = bankRefNo;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppAddress1() {
        return appAddress1;
    }

    public void setAppAddress1(String appAddress1) {
        this.appAddress1 = appAddress1;
    }

    public String getAppAddress2() {
        return appAddress2;
    }

    public void setAppAddress2(String appAddress2) {
        this.appAddress2 = appAddress2;
    }

    public String getAppState() {
        return appState;
    }

    public void setAppState(String appState) {
        this.appState = appState;
    }

    public String getAppCountry() {
        return appCountry;
    }

    public void setAppCountry(String appCountry) {
        this.appCountry = appCountry;
    }

    public String getAppCity() {
        return appCity;
    }

    public void setAppCity(String appCity) {
        this.appCity = appCity;
    }

    public String getApppostalCode() {
        return apppostalCode;
    }

    public void setApppostalCode(String apppostalCode) {
        this.apppostalCode = apppostalCode;
    }

    public String getApprefNo() {
        return apprefNo;
    }

    public void setApprefNo(String apprefNo) {
        this.apprefNo = apprefNo;
    }

    public String getAppBank() {
        return appBank;
    }

    public void setAppBank(String appBank) {
        this.appBank = appBank;
    }

    public String getAppBranch() {
        return appBranch;
    }

    public void setAppBranch(String appBranch) {
        this.appBranch = appBranch;
    }

    public String getPurposeOfGuarantee() {
        return purposeOfGuarantee;
    }

    public void setPurposeOfGuarantee(String purposeOfGuarantee) {
        this.purposeOfGuarantee = purposeOfGuarantee;
    }

    public String getGuaranteeClass() {
        return guaranteeClass;
    }

    public void setGuaranteeClass(String guaranteeClass) {
        this.guaranteeClass = guaranteeClass;
    }

    public String getEffectiveOn() {
        return effectiveOn;
    }

    public void setEffectiveOn(String effectiveOn) {
        this.effectiveOn = effectiveOn;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getGuaranteeType() {
        return guaranteeType;
    }

    public void setGuaranteeType(String guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

    public String getAdviceD() {
        return adviceD;
    }

    public void setAdviceD(String adviceD) {
        this.adviceD = adviceD;
    }

    public String getOtherDetailsRefNo() {
        return otherDetailsRefNo;
    }

    public void setOtherDetailsRefNo(String otherDetailsRefNo) {
        this.otherDetailsRefNo = otherDetailsRefNo;
    }

    public String getOtherDetailsRefDate() {
        return otherDetailsRefDate;
    }

    public void setOtherDetailsRefDate(String otherDetailsRefDate) {
        this.otherDetailsRefDate = otherDetailsRefDate;
    }

    public String getChargesBorneBy() {
        return chargesBorneBy;
    }

    public void setChargesBorneBy(String chargesBorneBy) {
        this.chargesBorneBy = chargesBorneBy;
    }

    public String getDetailsOfGuarantee() {
        return detailsOfGuarantee;
    }

    public void setDetailsOfGuarantee(String detailsOfGuarantee) {
        this.detailsOfGuarantee = detailsOfGuarantee;
    }

    public String getSenderToReciever() {
        return senderToReciever;
    }

    public void setSenderToReciever(String senderToReciever) {
        this.senderToReciever = senderToReciever;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getExpiryOn() {
        return expiryOn;
    }

    public void setExpiryOn(String expiryOn) {
        this.expiryOn = expiryOn;
    }

    public String getExpiryPeriod() {
        return expiryPeriod;
    }

    public void setExpiryPeriod(String expiryPeriod) {
        this.expiryPeriod = expiryPeriod;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryDay() {
        return expiryDay;
    }

    public void setExpiryDay(String expiryDay) {
        this.expiryDay = expiryDay;
    }

    public String getClaimExpiryOn() {
        return claimExpiryOn;
    }

    public void setClaimExpiryOn(String claimExpiryOn) {
        this.claimExpiryOn = claimExpiryOn;
    }

    public String getClaimPeriod() {
        return claimPeriod;
    }

    public void setClaimPeriod(String claimPeriod) {
        this.claimPeriod = claimPeriod;
    }

    public String getApplicableRules() {
        return applicableRules;
    }

    public void setApplicableRules(String applicableRules) {
        this.applicableRules = applicableRules;
    }

    public String getBgApplicableSubRules() {
        return bgApplicableSubRules;
    }

    public void setBgApplicableSubRules(String bgApplicableSubRules) {
        this.bgApplicableSubRules = bgApplicableSubRules;
    }

    public String getLinkToCounter() {
        return linkToCounter;
    }

    public void setLinkToCounter(String linkToCounter) {
        this.linkToCounter = linkToCounter;
    }

    public String getGuaranteeNo() {
        return guaranteeNo;
    }

    public void setGuaranteeNo(String guaranteeNo) {
        this.guaranteeNo = guaranteeNo;
    }

    public String getGuaranteeExpiryD() {
        return guaranteeExpiryD;
    }

    public void setGuaranteeExpiryD(String guaranteeExpiryD) {
        this.guaranteeExpiryD = guaranteeExpiryD;
    }

    @Override
    public String getBankCode() {
        return bankCode;
    }

    @Override
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getRateCode() {
        return rateCode;
    }

    public void setRateCode(String rateCode) {
        this.rateCode = rateCode;
    }

    public String getGuranteeStatus() {
        return guranteeStatus;
    }

    public void setGuranteeStatus(String guranteeStatus) {
        this.guranteeStatus = guranteeStatus;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getOperativeAcct() {
        return operativeAcct;
    }

    public void setOperativeAcct(String operativeAcct) {
        this.operativeAcct = operativeAcct;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCorrBankCode() {
        return corrBankCode;
    }

    public void setCorrBankCode(String corrBankCode) {
        this.corrBankCode = corrBankCode;
    }

    public String getCorrBranchId() {
        return corrBranchId;
    }

    public void setCorrBranchId(String corrBranchId) {
        this.corrBranchId = corrBranchId;
    }

    public String getCorrBankName() {
        return corrBankName;
    }

    public void setCorrBankName(String corrBankName) {
        this.corrBankName = corrBankName;
    }

    public String getCorrBankAddr() {
        return corrBankAddr;
    }

    public void setCorrBankAddr(String corrBankAddr) {
        this.corrBankAddr = corrBankAddr;
    }

    public String getCorrBankSwiftCode() {
        return corrBankSwiftCode;
    }

    public void setCorrBankSwiftCode(String corrBankSwiftCode) {
        this.corrBankSwiftCode = corrBankSwiftCode;
    }

    public String getCorrBankCtry() {
        return corrBankCtry;
    }

    public void setCorrBankCtry(String corrBankCtry) {
        this.corrBankCtry = corrBankCtry;
    }

    public String getCorrBankState() {
        return corrBankState;
    }

    public void setCorrBankState(String corrBankState) {
        this.corrBankState = corrBankState;
    }

    public String getCorrBankCity() {
        return corrBankCity;
    }

    public void setCorrBankCity(String corrBankCity) {
        this.corrBankCity = corrBankCity;
    }

    public String getCorrBankPoCode() {
        return corrBankPoCode;
    }

    public void setCorrBankPoCode(String corrBankPoCode) {
        this.corrBankPoCode = corrBankPoCode;
    }

    public String getCorrBankIdent() {
        return corrBankIdent;
    }

    public void setCorrBankIdent(String corrBankIdent) {
        this.corrBankIdent = corrBankIdent;
    }

    public String getIssueBankName() {
        return issueBankName;
    }

    public void setIssueBankName(String issueBankName) {
        this.issueBankName = issueBankName;
    }

    public String getIssueBankAddr() {
        return issueBankAddr;
    }

    public void setIssueBankAddr(String issueBankAddr) {
        this.issueBankAddr = issueBankAddr;
    }

    public String getIssueBankSwiftCode() {
        return issueBankSwiftCode;
    }

    public void setIssueBankSwiftCode(String issueBankSwiftCode) {
        this.issueBankSwiftCode = issueBankSwiftCode;
    }

    public String getIssueBankCtry() {
        return issueBankCtry;
    }

    public void setIssueBankCtry(String issueBankCtry) {
        this.issueBankCtry = issueBankCtry;
    }

    public String getIssueBankState() {
        return issueBankState;
    }

    public void setIssueBankState(String issueBankState) {
        this.issueBankState = issueBankState;
    }

    public String getIssueBankCity() {
        return issueBankCity;
    }

    public void setIssueBankCity(String issueBankCity) {
        this.issueBankCity = issueBankCity;
    }

    public String getIssueBankPoCode() {
        return issueBankPoCode;
    }

    public void setIssueBankPoCode(String issueBankPoCode) {
        this.issueBankPoCode = issueBankPoCode;
    }

    public String getIssueBankIdent() {
        return issueBankIdent;
    }

    public void setIssueBankIdent(String issueBankIdent) {
        this.issueBankIdent = issueBankIdent;
    }

    public String getFreeCode1() {
        return freeCode1;
    }

    public void setFreeCode1(String freeCode1) {
        this.freeCode1 = freeCode1;
    }

    public String getFreeCode2() {
        return freeCode2;
    }

    public void setFreeCode2(String freeCode2) {
        this.freeCode2 = freeCode2;
    }

    public String getFreeCode3() {
        return freeCode3;
    }

    public void setFreeCode3(String freeCode3) {
        this.freeCode3 = freeCode3;
    }

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public String getEventRate() {
        return eventRate;
    }

    public void setEventRate(String eventRate) {
        this.eventRate = eventRate;
    }

    public String getEventRateCode() {
        return eventRateCode;
    }

    public void setEventRateCode(String eventRateCode) {
        this.eventRateCode = eventRateCode;
    }

    public String getEventAmtCrncy() {
        return eventAmtCrncy;
    }

    public void setEventAmtCrncy(String eventAmtCrncy) {
        this.eventAmtCrncy = eventAmtCrncy;
    }

    public String getEventAmt() {
        return eventAmt;
    }

    public void setEventAmt(String eventAmt) {
        this.eventAmt = eventAmt;
    }

    public String getChrgWaiveFlg() {
        return chrgWaiveFlg;
    }

    public void setChrgWaiveFlg(String chrgWaiveFlg) {
        this.chrgWaiveFlg = chrgWaiveFlg;
    }

    public String getTempFormNumber() {
        return tempFormNumber;
    }

    public void setTempFormNumber(String tempFormNumber) {
        this.tempFormNumber = tempFormNumber;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getApproveBy() {
        return approveBy;
    }

    public void setApproveBy(String approveBy) {
        this.approveBy = approveBy;
    }
}
