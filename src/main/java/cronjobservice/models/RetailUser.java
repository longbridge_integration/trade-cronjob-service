package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import cronjobservice.utils.PrettySerializer;
import org.hibernate.annotations.Where;
import org.hibernate.envers.AuditOverride;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.IOException;


@Entity
@Audited(withModifiedFlag=true)
@AuditOverride(forClass=User.class)
@Where(clause ="del_Flag='N'" )
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"userName","deletedOn"}))
public class RetailUser extends User implements Person,PrettySerializer {


	public RetailUser(){
		this.userType = (UserType.RETAIL);
	}

	protected String isFirstTimeLogon = "Y";

	@Enumerated(EnumType.ORDINAL)
	protected TokenType tokenType;

	@OneToOne
	private Code customerSegment;

	private String customerId;

	private String bvn;

	private String town;

	private String state;
	private String country;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getBvn() {
		return bvn;
	}

	public void setBvn(String bvn) {
		this.bvn = bvn;
	}

	public TokenType getTokenType() {
		return tokenType;
	}

	public void setTokenType(TokenType tokenType) {
		this.tokenType = tokenType;
	}

	//	@JsonIgnore
//	@ManyToMany(mappedBy = "users")
//	private List<UserGroup> groups;
//
//
//	@JsonIgnore
//	public List<UserGroup> getGroups() {
//		return groups;
//	}


//	@JsonIgnore
//	public void setGroups(List<UserGroup> groups) {
//		this.groups = groups;
//	}


	public String getIsFirstTimeLogon() {
		return isFirstTimeLogon;
	}

	public void setIsFirstTimeLogon(String isFirstTimeLogon) {
		this.isFirstTimeLogon = isFirstTimeLogon;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Code getCustomerSegment() {
		return customerSegment;
	}

	public void setCustomerSegment(Code customerSegment) {
		this.customerSegment = customerSegment;
	}

	@Override
	public String toString() {
		return "RetailUser{"+super.toString()+"}";
	}



	@Override
	@JsonIgnore
	public boolean isExternal() {
		return false;
	}


	@Override
	@JsonIgnore
	public JsonSerializer<User> getSerializer() {
		return new JsonSerializer<User>() {
			@Override
			public void serialize(User value, JsonGenerator gen, SerializerProvider serializers)
					throws IOException {

				gen.writeStartObject();
				gen.writeStringField("Username", value.userName);
				gen.writeStringField("First Name", value.firstName);
				gen.writeStringField("Last Name", value.lastName);
				gen.writeStringField("Email", value.email);
				gen.writeStringField("Phone", value.phoneNumber);
				String status =null;
				if ("A".equals(value.status))
					status = "Active";
				else if ("I".equals(value.status))
					status = "Inactive";
				else if ("L".equals(value.status))
					status = "Locked";
				gen.writeStringField("Status", status);
				//gen.writeStringField("Customer Segment", value.get);
				gen.writeStringField("Role", value.role.getName());
				gen.writeEndObject();
			}
		};
	}

}
