package cronjobservice.jobs.lcjobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class ImportLcAddedOnFinacleJob  implements Job{

    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextFormA = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextFormA.getBean (CronJobService.class);
        try {
            logger.info("Import Lc Added on Finacle Job");
//            cronJobService.saveRunningJob("in-built",cronJobService.getCurrentExpression());
            cronJobService.saveRunningJob(JobCategory.IMPORT_LC_ADDED_ON_FINACLE.toString(),cronJobService.getCurrentExpression(JobCategory.IMPORT_LC_ADDED_ON_FINACLE.toString()));

            cronJobService.getImportLcAddedOnFinacle();
            cronJobService.updateRunningJob(JobCategory.IMPORT_LC_ADDED_ON_FINACLE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
