package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class Paar extends Process{

//Name and Parties
    //Beneficiary details
    private String beneName;
    private String beneEmail;
    private String beneAddress1;
    private String beneAddress2;
    private String beneStateCode;
    private String beneFax;
    private String beneCity;
    private String beneState;
    private String benePhone;

    //Appliciant details
    private String appName;
    private String appEmail;
    private String appAddress1;
    private String appAddress2;
    private String city;
    private String appState;

    private String appPhone;
    private String appTIN;
    private String appRCNo;
    private String appFax;
    private String appNEPC;

    //Transport details
    private String customOffice;
    private String transportDocDate;
    private String transportDocNumber;
    private String voyageNumber;
    private String departureDate;
    private String countryOfSupply;
    private String carrier;
    private String modeOfTransport;
    private String netWeight;
    private String grossWeight;
    private String portOfLoading;
    private String portOfDischarge;
    private String shippingMethod;
    private String numberOfContainers;


    //Financial
    private String designatedBank;
    private String  sourceFunds;
    private String currencyCode;
    private String exchangeRate;

    //Final Invoice
    private String invNumber;
    private String invDate;
    private BigDecimal invValue;
    private String totalFOB;
    private String totalFreight;
    private String insurValue;
    private String ancillaryChD;
    private String ancillaryChNotD;

    //Goods
    private String quantityOfItems;
    private String declaredhsCode;
    private String GenDesc;
    private String assessedhsCode;
    private String stateOfGoods;
    private String sectoralPurpose;
    private String countryOfOrigin;
    private String packages;
    private String quantity;
    private String unitPrice;
    private String fobValue;
    private String freightCharge;
    private String dutiableValue;
    private String dutyPayable;
    private String attachments;
    private String formNumber;
    private String appCountry;
    private String beneCountry;



    private String paarNumber;
    @ManyToOne
    private FormM formM;
    @ManyToOne
    private Account chargeAccount;
    @ManyToOne
    private Account transAccount;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,orphanRemoval=true)
    @JoinColumn(name = "paar_id")
    private List<PaarItems> paarItems;


    private UserType userType;
    private String InitiatedBy;
    private Date dateCreated;
    private String applicantName;
    private Boolean guidelineApproval;

    public String getBeneCountry() {
        return beneCountry;
    }

    public void setBeneCountry(String beneCountry) {
        this.beneCountry = beneCountry;
    }

    public String getAppCountry() {
        return appCountry;
    }

    public void setAppCountry(String appCountry) {
        this.appCountry = appCountry;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public String getCustomOffice() {
        return customOffice;
    }

    public void setCustomOffice(String customOffice) {
        this.customOffice = customOffice;
    }

    public String getPortOfLoading() {
        return portOfLoading;
    }

    public void setPortOfLoading(String portOfLoading) {
        this.portOfLoading = portOfLoading;
    }

    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }

    public String getSourceFunds() {
        return sourceFunds;
    }

    public void setSourceFunds(String sourceFunds) {
        this.sourceFunds = sourceFunds;
    }

    public String getTotalFOB() {
        return totalFOB;
    }

    public void setTotalFOB(String totalFOB) {
        this.totalFOB = totalFOB;
    }

    public String getTotalFreight() {
        return totalFreight;
    }

    public void setTotalFreight(String totalFreight) {
        this.totalFreight = totalFreight;
    }

    public String getInsurValue() {
        return insurValue;
    }

    public void setInsurValue(String insurValue) {
        this.insurValue = insurValue;
    }

    public String getQuantityOfItems() {
        return quantityOfItems;
    }

    public void setQuantityOfItems(String quantityOfItems) {
        this.quantityOfItems = quantityOfItems;
    }

    public String getGenDesc() {
        return GenDesc;
    }

    public void setGenDesc(String genDesc) {
        GenDesc = genDesc;
    }

    public BigDecimal getInvValue() {
        return invValue;
    }

    public void setInvValue(BigDecimal invValue) {
        this.invValue = invValue;
    }

    public List<PaarItems> getPaarItems() {
        return paarItems;
    }

    public void setPaarItems(List<PaarItems> paarItems) {
        this.paarItems = paarItems;
    }

    public String getBeneName() {
        return beneName;
    }

    public void setBeneName(String beneName) {
        this.beneName = beneName;
    }

    public String getBeneEmail() {
        return beneEmail;
    }

    public void setBeneEmail(String beneEmail) {
        this.beneEmail = beneEmail;
    }

    public String getBeneAddress1() {
        return beneAddress1;
    }

    public void setBeneAddress1(String beneAddress1) {
        this.beneAddress1 = beneAddress1;
    }

    public String getBeneAddress2() {
        return beneAddress2;
    }

    public void setBeneAddress2(String beneAddress2) {
        this.beneAddress2 = beneAddress2;
    }

    public String getBeneStateCode() {
        return beneStateCode;
    }

    public void setBeneStateCode(String beneStateCode) {
        this.beneStateCode = beneStateCode;
    }

    public String getBeneFax() {
        return beneFax;
    }

    public void setBeneFax(String beneFax) {
        this.beneFax = beneFax;
    }

    public String getBeneCity() {
        return beneCity;
    }

    public void setBeneCity(String beneCity) {
        this.beneCity = beneCity;
    }

    public String getBeneState() {
        return beneState;
    }

    public void setBeneState(String beneState) {
        this.beneState = beneState;
    }

    public String getBenePhone() {
        return benePhone;
    }

    public void setBenePhone(String benePhone) {
        this.benePhone = benePhone;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppEmail() {
        return appEmail;
    }

    public void setAppEmail(String appEmail) {
        this.appEmail = appEmail;
    }

    public String getAppAddress1() {
        return appAddress1;
    }

    public void setAppAddress1(String appAddress1) {
        this.appAddress1 = appAddress1;
    }

    public String getAppAddress2() {
        return appAddress2;
    }

    public void setAppAddress2(String appAddress2) {
        this.appAddress2 = appAddress2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAppState() {
        return appState;
    }

    public void setAppState(String appState) {
        this.appState = appState;
    }

    public String getAppPhone() {
        return appPhone;
    }

    public void setAppPhone(String appPhone) {
        this.appPhone = appPhone;
    }

    public String getAppTIN() {
        return appTIN;
    }

    public void setAppTIN(String appTIN) {
        this.appTIN = appTIN;
    }

    public String getAppRCNo() {
        return appRCNo;
    }

    public void setAppRCNo(String appRCNo) {
        this.appRCNo = appRCNo;
    }

    public String getAppFax() {
        return appFax;
    }

    public void setAppFax(String appFax) {
        this.appFax = appFax;
    }

    public String getAppNEPC() {
        return appNEPC;
    }

    public void setAppNEPC(String appNEPC) {
        this.appNEPC = appNEPC;
    }

    public String getTransportDocDate() {
        return transportDocDate;
    }

    public void setTransportDocDate(String transportDocDate) {
        this.transportDocDate = transportDocDate;
    }

    public String getTransportDocNumber() {
        return transportDocNumber;
    }

    public void setTransportDocNumber(String transportDocNumber) {
        this.transportDocNumber = transportDocNumber;
    }

    public String getVoyageNumber() {
        return voyageNumber;
    }

    public void setVoyageNumber(String voyageNumber) {
        this.voyageNumber = voyageNumber;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getCountryOfSupply() {
        return countryOfSupply;
    }

    public void setCountryOfSupply(String countryOfSupply) {
        this.countryOfSupply = countryOfSupply;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getModeOfTransport() {
        return modeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        this.modeOfTransport = modeOfTransport;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }


    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getNumberOfContainers() {
        return numberOfContainers;
    }

    public void setNumberOfContainers(String numberOfContainers) {
        this.numberOfContainers = numberOfContainers;
    }

    public String getDesignatedBank() {
        return designatedBank;
    }

    public void setDesignatedBank(String designatedBank) {
        this.designatedBank = designatedBank;
    }


    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getInvNumber() {
        return invNumber;
    }

    public void setInvNumber(String invNumber) {
        this.invNumber = invNumber;
    }

    public String getInvDate() {
        return invDate;
    }

    public void setInvDate(String invDate) {
        this.invDate = invDate;
    }


    public String getAncillaryChD() {
        return ancillaryChD;
    }

    public void setAncillaryChD(String ancillaryChD) {
        this.ancillaryChD = ancillaryChD;
    }

    public String getAncillaryChNotD() {
        return ancillaryChNotD;
    }

    public void setAncillaryChNotD(String ancillaryChNotD) {
        this.ancillaryChNotD = ancillaryChNotD;
    }


    public String getDeclaredhsCode() {
        return declaredhsCode;
    }

    public void setDeclaredhsCode(String declaredhsCode) {
        this.declaredhsCode = declaredhsCode;
    }

    public String getAssessedhsCode() {
        return assessedhsCode;
    }

    public void setAssessedhsCode(String assessedhsCode) {
        this.assessedhsCode = assessedhsCode;
    }

    public String getStateOfGoods() {
        return stateOfGoods;
    }

    public void setStateOfGoods(String stateOfGoods) {
        this.stateOfGoods = stateOfGoods;
    }

    public String getSectoralPurpose() {
        return sectoralPurpose;
    }

    public void setSectoralPurpose(String sectoralPurpose) {
        this.sectoralPurpose = sectoralPurpose;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getFobValue() {
        return fobValue;
    }

    public void setFobValue(String fobValue) {
        this.fobValue = fobValue;
    }

    public String getFreightCharge() {
        return freightCharge;
    }

    public void setFreightCharge(String freightCharge) {
        this.freightCharge = freightCharge;
    }

    public String getDutiableValue() {
        return dutiableValue;
    }

    public void setDutiableValue(String dutiableValue) {
        this.dutiableValue = dutiableValue;
    }

    public String getDutyPayable() {
        return dutyPayable;
    }

    public void setDutyPayable(String dutyPayable) {
        this.dutyPayable = dutyPayable;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getPaarNumber() {
        return paarNumber;
    }

    public void setPaarNumber(String paarNumber) {
        this.paarNumber = paarNumber;
    }

    public FormM getFormM() {
        return formM;
    }

    public void setFormM(FormM formM) {
        this.formM = formM;
    }

    public Account getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(Account chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public Account getTransAccount() {
        return transAccount;
    }

    public void setTransAccount(Account transAccount) {
        this.transAccount = transAccount;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getInitiatedBy() {
        return InitiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        InitiatedBy = initiatedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public Boolean getGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(Boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    @Override
    public String toString() {
        return "Paar{" +
                "beneName='" + beneName + '\'' +
                ", beneEmail='" + beneEmail + '\'' +
                ", beneAddress1='" + beneAddress1 + '\'' +
                ", beneAddress2='" + beneAddress2 + '\'' +
                ", beneStateCode='" + beneStateCode + '\'' +
                ", beneFax='" + beneFax + '\'' +
                ", beneCity='" + beneCity + '\'' +
                ", beneState='" + beneState + '\'' +
                ", benePhone='" + benePhone + '\'' +
                ", appName='" + appName + '\'' +
                ", appEmail='" + appEmail + '\'' +
                ", appAddress1='" + appAddress1 + '\'' +
                ", appAddress2='" + appAddress2 + '\'' +
                ", city='" + city + '\'' +
                ", appState='" + appState + '\'' +
                ", appPhone='" + appPhone + '\'' +
                ", appTIN='" + appTIN + '\'' +
                ", appRCNo='" + appRCNo + '\'' +
                ", appFax='" + appFax + '\'' +
                ", appNEPC='" + appNEPC + '\'' +
                ", customOffice='" + customOffice + '\'' +
                ", transportDocDate='" + transportDocDate + '\'' +
                ", transportDocNumber='" + transportDocNumber + '\'' +
                ", voyageNumber='" + voyageNumber + '\'' +
                ", departureDate='" + departureDate + '\'' +
                ", countryOfSupply='" + countryOfSupply + '\'' +
                ", carrier='" + carrier + '\'' +
                ", modeOfTransport='" + modeOfTransport + '\'' +
                ", netWeight='" + netWeight + '\'' +
                ", grossWeight='" + grossWeight + '\'' +
                ", portOfLoading='" + portOfLoading + '\'' +
                ", portOfDischarge='" + portOfDischarge + '\'' +
                ", shippingMethod='" + shippingMethod + '\'' +
                ", numberOfContainers='" + numberOfContainers + '\'' +
                ", designatedBank='" + designatedBank + '\'' +
                ", sourceFunds='" + sourceFunds + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", exchangeRate='" + exchangeRate + '\'' +
                ", invNumber='" + invNumber + '\'' +
                ", invDate='" + invDate + '\'' +
                ", invValue=" + invValue +
                ", totalFOB='" + totalFOB + '\'' +
                ", totalFreight='" + totalFreight + '\'' +
                ", insurValue='" + insurValue + '\'' +
                ", ancillaryChD='" + ancillaryChD + '\'' +
                ", ancillaryChNotD='" + ancillaryChNotD + '\'' +
                ", quantityOfItems='" + quantityOfItems + '\'' +
                ", declaredhsCode='" + declaredhsCode + '\'' +
                ", GenDesc='" + GenDesc + '\'' +
                ", assessedhsCode='" + assessedhsCode + '\'' +
                ", stateOfGoods='" + stateOfGoods + '\'' +
                ", sectoralPurpose='" + sectoralPurpose + '\'' +
                ", countryOfOrigin='" + countryOfOrigin + '\'' +
                ", packages='" + packages + '\'' +
                ", quantity='" + quantity + '\'' +
                ", unitPrice='" + unitPrice + '\'' +
                ", fobValue='" + fobValue + '\'' +
                ", freightCharge='" + freightCharge + '\'' +
                ", dutiableValue='" + dutiableValue + '\'' +
                ", dutyPayable='" + dutyPayable + '\'' +
                ", attachments='" + attachments + '\'' +
                ", formNumber='" + formNumber + '\'' +
                ", paarNumber='" + paarNumber + '\'' +
                ", formM=" + formM +
                ", chargeAccount=" + chargeAccount +
                ", transAccount=" + transAccount +
                ", paarItems=" + paarItems +
                ", userType=" + userType +
                ", InitiatedBy='" + InitiatedBy + '\'' +
                ", dateCreated=" + dateCreated +
                ", applicantName='" + applicantName + '\'' +
                ", guidelineApproval=" + guidelineApproval +
                '}';
    }
}
