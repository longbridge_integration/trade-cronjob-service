package cronjobservice.repositories;


import cronjobservice.models.Code;
import cronjobservice.models.Corporate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Wunmi on 27/03/2017.
 */

@Repository
public interface CorporateRepo extends CommonRepo<Corporate, Long> {

    Corporate findByCustomerId(String customerId);
    Corporate findFirstByCorporateId(String corporateId);
    Corporate findFirstByCorporateIdIgnoreCase(String corporateId);
    boolean existsByCorporateIdIgnoreCase(String corporateId);
    Corporate findFirstByCustomerId(String customerId);
    boolean existsByCustomerId(String customerId);

    List<Corporate> findByCustomerSegment(Code custSegment);


}
