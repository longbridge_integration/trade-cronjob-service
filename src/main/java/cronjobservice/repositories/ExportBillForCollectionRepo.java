package cronjobservice.repositories;


import cronjobservice.models.ExportBillForCollection;
import cronjobservice.models.ExportBills;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExportBillForCollectionRepo extends CommonRepo<ExportBillForCollection,Long>{

    List<ExportBillForCollection> findByStatusAndSubmitFlag(String S, String SF);
    ExportBillForCollection findByTemporalBllNumb(String tempNumber);
}
