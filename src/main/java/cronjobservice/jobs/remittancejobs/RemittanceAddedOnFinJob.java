package cronjobservice.jobs.remittancejobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class RemittanceAddedOnFinJob implements Job{
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextRemittance = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextRemittance.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.REMITTANCE_ADDED_ON_FINACLE.toString(),cronJobService.getCurrentExpression(JobCategory.REMITTANCE_ADDED_ON_FINACLE.toString()));

            cronJobService.getRemittanceIssuedOnFinacle();

            cronJobService.updateRunningJob(JobCategory.REMITTANCE_ADDED_ON_FINACLE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
