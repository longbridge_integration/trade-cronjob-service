package cronjobservice.jobs.lcjobs;


import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class ImportLcSubmitJob implements Job{
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextLc = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextLc.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.IMPORTLC_SUBMIT.toString(),cronJobService.getCurrentExpression(JobCategory.IMPORTLC_SUBMIT.toString()));

            cronJobService.submitImportLcToFinacle();
            cronJobService.updateRunningJob(JobCategory.IMPORTLC_SUBMIT.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
