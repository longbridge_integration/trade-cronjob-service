package cronjobservice.repositories;

import cronjobservice.models.LcAmend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LcAmendRepo extends CommonRepo<LcAmend,Long>{

    Long countByCifid(String cifid);

    LcAmend getByLcNumber(String lcNumber);

    List<LcAmend> findByLcNumber(String lcNumber);

    Page<LcAmend> findByLcNumberAndCifid(String lcNumber, String cific, Pageable pageable);

    @Query("select s from LcAmend s where s.status!=:openstatus")
    Page<LcAmend> findStatus(@Param("openstatus") String status, Pageable pageable);

    @Query("select s from LcAmend s where s.status=:openstatus")
    Page<LcAmend> findOpenStatus(@Param("openstatus") String status, Pageable pageable);

    Page<LcAmend> findByStatusIsNull(Pageable pageable);

    Page<LcAmend> findByStatusNotNull(Pageable pageable);

    Page<LcAmend> findByCifidAndStatusNotNull(String cif, Pageable pageable);

    @Query("select distinct s from LcAmend s where s.cifid=:cifid and s.status=:submitted")
    Page<LcAmend> findByCifidAndStatusS(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    @Query("select distinct s from LcAmend s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<LcAmend> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);

    @Query("select distinct s from LcAmend s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<LcAmend> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    Page<LcAmend> findByCifid(String cifid, Pageable pageable);

    @Query("select distinct s from LcAmend s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<LcAmend> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    @Query("select distinct s from LcAmend s where s.cifid=:cifid  and  s.status is not null  and   s.status!=:submitted and s.status!=:saved")
    Page<LcAmend> findByClosedCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, @Param("saved") String saved, Pageable pageable);

}
