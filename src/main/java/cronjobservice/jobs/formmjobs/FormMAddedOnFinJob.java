package cronjobservice.jobs.formmjobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * Created by Longbridge on 9/12/2017.
 */
@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class FormMAddedOnFinJob implements Job {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextFormM = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextFormM.getBean (CronJobService.class);

        try {
            logger.info("Starting form M job");
            cronJobService.saveRunningJob(JobCategory.FORMM_ADDED_ON_FINACLE.toString(),cronJobService.getCurrentExpression(JobCategory.FORMM_ADDED_ON_FINACLE.toString()));

            cronJobService.getFormMAddedOnFinacle();
            cronJobService.updateRunningJob(JobCategory.FORMM_ADDED_ON_FINACLE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
