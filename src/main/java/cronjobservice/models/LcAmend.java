package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "lc_amend")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class LcAmend extends AbstractEntity{

    private String lcNumber;


    @ManyToMany(cascade={CascadeType.ALL})
    private List<Comments> comment = new ArrayList<>();

    private String status;

    private String cifid;

    private Date createdOn;

    private String amendAction;

    @ManyToOne
    @JoinColumn(name = "lc_chrg_acct")
    private Account chargeAccount;
    @ManyToOne
    @JoinColumn(name = "lc_trn_acct")
    private Account transAccount;
    @ManyToOne
    private Code lcType;
    private BigDecimal lcAmount;
    private String lcCurrency;
    private BigDecimal partCleanline;
    private BigDecimal partConfirmed;
    private BigDecimal partUnconfirmed;
    private String exchangeRate;
    private String advBankName;
    private String advBankBranch;
    private String advBankAddress;
    private String advSwiftCode;

    private String advThrBankName;
    private String advThrBankBranch;
    private String advThrBankAddress;
    private String advThrSwiftCode;

    private String instructions;
    private String docInstruction;
    private String conditions;
    private String description;
    private String sendRecInfo;
    private String periodOfPresentation;//textInfo
    private String chargeDescription;//textInfo

    private Boolean transferable;
    private Boolean revolving;
    private Boolean revokable;
    private Boolean backToBack;
    private String backTobackLcNo;
    private Boolean partShipment;
    private Boolean redLetter;
    private Boolean tranShipment;
    private Boolean deferred;
    private Date dateOfExpiry;
    private String countryOfExpiry;
    private String cityOfExpiry;
    @ManyToOne
    private Code lcPaymentTenor;
    private String usancePeriod;
    private String foreignBankRefNum;
    //private String beneficiaryBankDetails;
    //private String beneficiaryDetails;
    private String beneficiaryName;
    private String beneficiaryAddress;
    private String beneficiaryBankName;
    private String beneficiaryBankBranch;
    private String beneficiaryBankAddress;
    private String beneficiarySwiftCode;
    //private String correspondingBanks;
    private String lcValidCountry;
    private String beneficiaryCharges;
    private String negotiationPeriod;
    private String portOfLoading;
    private String placeOfFinalDest;
    private String portOfDischrge;
    private String shipmentTerms;
    private String originOfGoods;

//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "lc_docs")
//    private List<Documents> documents;

    private String availableWith;
    private String availableBy;
    private String tolerancePos;
    private String toleranceNeg;
    @OneToOne
    private Code confirmInstruct;
    private String confirmationBank;
    private String reimbursingBankName;
    private String reimbursingBankBranch;
    private String reimbursingBankAddress;
    private String reimbursingSwiftCode;
    private String availWithBankName;
    private String availWithBankBranch;
    private String availWithBankAddress;
    private String availWithSwiftCode;

    private BigDecimal rate;
    private BigDecimal lcAvailableAmount;
    private BigDecimal utilbyBill;
    private BigDecimal utilWithoutBill;
    private BigDecimal outstanding;
    private Date lastShipmentDate;
    private int numberOfAmendments;
    private int numberOfReinstatement;


    private String insurCompName;
    private String insurCompAddr;
    private String insurPolicyNo;
    private Date insurPolicyDate;
    private String insurPayableAt;
    private Date insurDateOfExpiry;
    private String insurPercentage;
    private BigDecimal insuredAmount;
    private BigDecimal premiumAmount;
    private String insuredBy;

    private UserType userType;
    private String InitiatedBy;
    private Date dateCreated;
    private String applicantName;
    private Boolean guidelineApproval;



    public String getLcCurrency() {
        return lcCurrency;
    }

    public void setLcCurrency(String lcCurrency) {
        this.lcCurrency = lcCurrency;
    }

    public BigDecimal getLcAmount() {
        return lcAmount;
    }

    public void setLcAmount(BigDecimal lcAmount) {
        this.lcAmount = lcAmount;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getTransferable() {
        return transferable;
    }

    public void setTransferable(Boolean transferable) {
        this.transferable = transferable;
    }

    public Boolean getRevolving() {
        return revolving;
    }

    public void setRevolving(Boolean revolving) {
        this.revolving = revolving;
    }

    public Boolean getRevokable() {
        return revokable;
    }

    public void setRevokable(Boolean revokable) {
        this.revokable = revokable;
    }

    public Boolean getBackToBack() {
        return backToBack;
    }

    public void setBackToBack(Boolean backToBack) {
        this.backToBack = backToBack;
    }

    public String getBackTobackLcNo() {
        return backTobackLcNo;
    }

    public void setBackTobackLcNo(String backTobackLcNo) {
        this.backTobackLcNo = backTobackLcNo;
    }

    public Boolean getPartShipment() {
        return partShipment;
    }

    public void setPartShipment(Boolean partShipment) {
        this.partShipment = partShipment;
    }

    public Boolean getRedLetter() {
        return redLetter;
    }

    public void setRedLetter(Boolean redLetter) {
        this.redLetter = redLetter;
    }

    public Boolean getTranShipment() {
        return tranShipment;
    }

    public void setTranShipment(Boolean tranShipment) {
        this.tranShipment = tranShipment;
    }

    public Boolean getDeferred() {
        return deferred;
    }

    public void setDeferred(Boolean deferred) {
        this.deferred = deferred;
    }

    public Date getDateOfExpiry() {
        return dateOfExpiry;
    }

    public void setDateOfExpiry(Date dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }

    public String getCountryOfExpiry() {
        return countryOfExpiry;
    }

    public void setCountryOfExpiry(String countryOfExpiry) {
        this.countryOfExpiry = countryOfExpiry;
    }

    public String getCityOfExpiry() {
        return cityOfExpiry;
    }

    public void setCityOfExpiry(String cityOfExpiry) {
        this.cityOfExpiry = cityOfExpiry;
    }

    public Code getLcPaymentTenor() {
        return lcPaymentTenor;
    }

    public void setLcPaymentTenor(Code lcPaymentTenor) {
        this.lcPaymentTenor = lcPaymentTenor;
    }

    public String getLcValidCountry() {
        return lcValidCountry;
    }

    public void setLcValidCountry(String lcValidCountry) {
        this.lcValidCountry = lcValidCountry;
    }

    public String getBeneficiaryCharges() {
        return beneficiaryCharges;
    }

    public void setBeneficiaryCharges(String beneficiaryCharges) {
        this.beneficiaryCharges = beneficiaryCharges;
    }

    public String getPortOfLoading() {
        return portOfLoading;
    }

    public void setPortOfLoading(String portOfLoading) {
        this.portOfLoading = portOfLoading;
    }

    public String getPortOfDischrge() {
        return portOfDischrge;
    }

    public void setPortOfDischrge(String portOfDischrge) {
        this.portOfDischrge = portOfDischrge;
    }

    public String getOriginOfGoods() {
        return originOfGoods;
    }

    public void setOriginOfGoods(String originOfGoods) {
        this.originOfGoods = originOfGoods;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

//    public List<Documents> getDocuments() {
//        return documents;
//    }
//
//    public void setDocuments(List<Documents> documents) {
//        this.documents = documents;
//    }

    public String getAvailableWith() {
        return availableWith;
    }

    public void setAvailableWith(String availableWith) {
        this.availableWith = availableWith;
    }

    public String getAvailableBy() {
        return availableBy;
    }

    public void setAvailableBy(String availableBy) {
        this.availableBy = availableBy;
    }

    public String getTolerancePos() {
        return tolerancePos;
    }

    public void setTolerancePos(String tolerancePos) {
        this.tolerancePos = tolerancePos;
    }

    public String getToleranceNeg() {
        return toleranceNeg;
    }

    public void setToleranceNeg(String toleranceNeg) {
        this.toleranceNeg = toleranceNeg;
    }

    public String getUsancePeriod() {
        return usancePeriod;
    }

    public void setUsancePeriod(String usancePeriod) {
        this.usancePeriod = usancePeriod;
    }

    public String getConfirmationBank() {
        return confirmationBank;
    }

    public void setConfirmationBank(String confirmationBank) {
        this.confirmationBank = confirmationBank;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getLcAvailableAmount() {
        return lcAvailableAmount;
    }

    public void setLcAvailableAmount(BigDecimal lcAvailableAmount) {
        this.lcAvailableAmount = lcAvailableAmount;
    }

    public BigDecimal getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(BigDecimal outstanding) {
        this.outstanding = outstanding;
    }

    public Date getLastShipmentDate() {
        return lastShipmentDate;
    }

    public void setLastShipmentDate(Date lastShipmentDate) {
        this.lastShipmentDate = lastShipmentDate;
    }

    public int getNumberOfAmendments() {
        return numberOfAmendments;
    }

    public void setNumberOfAmendments(int numberOfAmendments) {
        this.numberOfAmendments = numberOfAmendments;
    }

    public int getNumberOfReinstatement() {
        return numberOfReinstatement;
    }

    public void setNumberOfReinstatement(int numberOfReinstatement) {
        this.numberOfReinstatement = numberOfReinstatement;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getInitiatedBy() {
        return InitiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        InitiatedBy = initiatedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getLcNumber() {
        return lcNumber;
    }

    public void setLcNumber(String lcNumber) {
        this.lcNumber = lcNumber;
    }

    public Boolean getGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(Boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public Account getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(Account chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public Account getTransAccount() {
        return transAccount;
    }

    public void setTransAccount(Account transAccount) {
        this.transAccount = transAccount;
    }

    public Code getLcType() {
        return lcType;
    }

    public void setLcType(Code lcType) {
        this.lcType = lcType;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryAddress() {
        return beneficiaryAddress;
    }

    public void setBeneficiaryAddress(String beneficiaryAddress) {
        this.beneficiaryAddress = beneficiaryAddress;
    }

    public String getBeneficiaryBankName() {
        return beneficiaryBankName;
    }

    public void setBeneficiaryBankName(String beneficiaryBankName) {
        this.beneficiaryBankName = beneficiaryBankName;
    }

    public String getBeneficiaryBankBranch() {
        return beneficiaryBankBranch;
    }

    public void setBeneficiaryBankBranch(String beneficiaryBankBranch) {
        this.beneficiaryBankBranch = beneficiaryBankBranch;
    }

    public String getBeneficiaryBankAddress() {
        return beneficiaryBankAddress;
    }

    public void setBeneficiaryBankAddress(String beneficiaryBankAddress) {
        this.beneficiaryBankAddress = beneficiaryBankAddress;
    }

    public String getBeneficiarySwiftCode() {
        return beneficiarySwiftCode;
    }

    public void setBeneficiarySwiftCode(String beneficiarySwiftCode) {
        this.beneficiarySwiftCode = beneficiarySwiftCode;
    }

    public String getNegotiationPeriod() {
        return negotiationPeriod;
    }

    public void setNegotiationPeriod(String negotiationPeriod) {
        this.negotiationPeriod = negotiationPeriod;
    }

    public String getShipmentTerms() {
        return shipmentTerms;
    }

    public void setShipmentTerms(String shipmentTerms) {
        this.shipmentTerms = shipmentTerms;
    }

    public BigDecimal getPartCleanline() {
        return partCleanline;
    }

    public void setPartCleanline(BigDecimal partCleanline) {
        this.partCleanline = partCleanline;
    }

    public BigDecimal getPartConfirmed() {
        return partConfirmed;
    }

    public void setPartConfirmed(BigDecimal partConfirmed) {
        this.partConfirmed = partConfirmed;
    }

    public BigDecimal getPartUnconfirmed() {
        return partUnconfirmed;
    }

    public void setPartUnconfirmed(BigDecimal partUnconfirmed) {
        this.partUnconfirmed = partUnconfirmed;
    }

    public String getReimbursingBankName() {
        return reimbursingBankName;
    }

    public void setReimbursingBankName(String reimbursingBankName) {
        this.reimbursingBankName = reimbursingBankName;
    }

    public String getReimbursingBankBranch() {
        return reimbursingBankBranch;
    }

    public void setReimbursingBankBranch(String reimbursingBankBranch) {
        this.reimbursingBankBranch = reimbursingBankBranch;
    }

    public String getReimbursingBankAddress() {
        return reimbursingBankAddress;
    }

    public void setReimbursingBankAddress(String reimbursingBankAddress) {
        this.reimbursingBankAddress = reimbursingBankAddress;
    }

    public String getReimbursingSwiftCode() {
        return reimbursingSwiftCode;
    }

    public void setReimbursingSwiftCode(String reimbursingSwiftCode) {
        this.reimbursingSwiftCode = reimbursingSwiftCode;
    }

    public BigDecimal getInsuredAmount() {
        return insuredAmount;
    }

    public void setInsuredAmount(BigDecimal insuredAmount) {
        this.insuredAmount = insuredAmount;
    }

    public BigDecimal getPremiumAmount() {
        return premiumAmount;
    }

    public void setPremiumAmount(BigDecimal premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    public String getInsuredBy() {
        return insuredBy;
    }

    public void setInsuredBy(String insuredBy) {
        this.insuredBy = insuredBy;
    }


    public List<Comments> getComment() {
        return comment;
    }

    public void setComment(List<Comments> comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getAmendAction() {
        return amendAction;
    }

    public void setAmendAction(String amendAction) {
        this.amendAction = amendAction;
    }

    public String getAdvBankName() {
        return advBankName;
    }

    public void setAdvBankName(String advBankName) {
        this.advBankName = advBankName;
    }

    public String getAdvBankBranch() {
        return advBankBranch;
    }

    public void setAdvBankBranch(String advBankBranch) {
        this.advBankBranch = advBankBranch;
    }

    public String getAdvBankAddress() {
        return advBankAddress;
    }

    public void setAdvBankAddress(String advBankAddress) {
        this.advBankAddress = advBankAddress;
    }

    public String getAdvSwiftCode() {
        return advSwiftCode;
    }

    public void setAdvSwiftCode(String advSwiftCode) {
        this.advSwiftCode = advSwiftCode;
    }

    public String getAdvThrBankName() {
        return advThrBankName;
    }

    public void setAdvThrBankName(String advThrBankName) {
        this.advThrBankName = advThrBankName;
    }

    public String getAdvThrBankBranch() {
        return advThrBankBranch;
    }

    public void setAdvThrBankBranch(String advThrBankBranch) {
        this.advThrBankBranch = advThrBankBranch;
    }

    public String getAdvThrBankAddress() {
        return advThrBankAddress;
    }

    public void setAdvThrBankAddress(String advThrBankAddress) {
        this.advThrBankAddress = advThrBankAddress;
    }

    public String getAdvThrSwiftCode() {
        return advThrSwiftCode;
    }

    public void setAdvThrSwiftCode(String advThrSwiftCode) {
        this.advThrSwiftCode = advThrSwiftCode;
    }

    public String getDocInstruction() {
        return docInstruction;
    }

    public void setDocInstruction(String docInstruction) {
        this.docInstruction = docInstruction;
    }

    public String getSendRecInfo() {
        return sendRecInfo;
    }

    public void setSendRecInfo(String sendRecInfo) {
        this.sendRecInfo = sendRecInfo;
    }

    public String getPeriodOfPresentation() {
        return periodOfPresentation;
    }

    public void setPeriodOfPresentation(String periodOfPresentation) {
        this.periodOfPresentation = periodOfPresentation;
    }

    public String getChargeDescription() {
        return chargeDescription;
    }

    public void setChargeDescription(String chargeDescription) {
        this.chargeDescription = chargeDescription;
    }

    public String getForeignBankRefNum() {
        return foreignBankRefNum;
    }

    public void setForeignBankRefNum(String foreignBankRefNum) {
        this.foreignBankRefNum = foreignBankRefNum;
    }

    public String getPlaceOfFinalDest() {
        return placeOfFinalDest;
    }

    public void setPlaceOfFinalDest(String placeOfFinalDest) {
        this.placeOfFinalDest = placeOfFinalDest;
    }

    public Code getConfirmInstruct() {
        return confirmInstruct;
    }

    public void setConfirmInstruct(Code confirmInstruct) {
        this.confirmInstruct = confirmInstruct;
    }

    public String getAvailWithBankName() {
        return availWithBankName;
    }

    public void setAvailWithBankName(String availWithBankName) {
        this.availWithBankName = availWithBankName;
    }

    public String getAvailWithBankBranch() {
        return availWithBankBranch;
    }

    public void setAvailWithBankBranch(String availWithBankBranch) {
        this.availWithBankBranch = availWithBankBranch;
    }

    public String getAvailWithBankAddress() {
        return availWithBankAddress;
    }

    public void setAvailWithBankAddress(String availWithBankAddress) {
        this.availWithBankAddress = availWithBankAddress;
    }

    public String getAvailWithSwiftCode() {
        return availWithSwiftCode;
    }

    public void setAvailWithSwiftCode(String availWithSwiftCode) {
        this.availWithSwiftCode = availWithSwiftCode;
    }

    public BigDecimal getUtilbyBill() {
        return utilbyBill;
    }

    public void setUtilbyBill(BigDecimal utilbyBill) {
        this.utilbyBill = utilbyBill;
    }

    public BigDecimal getUtilWithoutBill() {
        return utilWithoutBill;
    }

    public void setUtilWithoutBill(BigDecimal utilWithoutBill) {
        this.utilWithoutBill = utilWithoutBill;
    }

    public String getInsurCompName() {
        return insurCompName;
    }

    public void setInsurCompName(String insurCompName) {
        this.insurCompName = insurCompName;
    }

    public String getInsurCompAddr() {
        return insurCompAddr;
    }

    public void setInsurCompAddr(String insurCompAddr) {
        this.insurCompAddr = insurCompAddr;
    }

    public String getInsurPolicyNo() {
        return insurPolicyNo;
    }

    public void setInsurPolicyNo(String insurPolicyNo) {
        this.insurPolicyNo = insurPolicyNo;
    }

    public Date getInsurPolicyDate() {
        return insurPolicyDate;
    }

    public void setInsurPolicyDate(Date insurPolicyDate) {
        this.insurPolicyDate = insurPolicyDate;
    }

    public String getInsurPayableAt() {
        return insurPayableAt;
    }

    public void setInsurPayableAt(String insurPayableAt) {
        this.insurPayableAt = insurPayableAt;
    }

    public Date getInsurDateOfExpiry() {
        return insurDateOfExpiry;
    }

    public void setInsurDateOfExpiry(Date insurDateOfExpiry) {
        this.insurDateOfExpiry = insurDateOfExpiry;
    }

    public String getInsurPercentage() {
        return insurPercentage;
    }

    public void setInsurPercentage(String insurPercentage) {
        this.insurPercentage = insurPercentage;
    }
}
