package cronjobservice.repositories;

import cronjobservice.models.WorkFlowStates;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by Showboy on 07/09/2017.
 */
@Repository
public interface WorkFlowStatesRepo extends CommonRepo<WorkFlowStates, Long> {

    WorkFlowStates findByCode(String code);

    Iterable<WorkFlowStates> findByType(String type);
    Page<WorkFlowStates> findByType(String type, Pageable pageable);
    WorkFlowStates findByTypeAndCode(String type, String code);
    @Query("select distinct c.type from WorkFlowStates c")
    Iterable<String> findAllTypes();
    @Query("select distinct c.type from WorkFlowStates c")
    Page<String> findAllTypes(Pageable pageable);


}
