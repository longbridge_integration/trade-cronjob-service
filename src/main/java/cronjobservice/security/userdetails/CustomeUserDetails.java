package cronjobservice.security.userdetails;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by ayoade_farooq@yahoo.com on 7/11/2017.
 */
public interface CustomeUserDetails extends UserDetails {
}
