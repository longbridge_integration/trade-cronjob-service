package cronjobservice.services.implementation;

import cronjobservice.dtos.SettingDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.Setting;
import cronjobservice.repositories.SettingRepo;
import cronjobservice.services.ConfigurationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Fortune on 4/13/2017.
 */


@Service
public class ConfigurationServiceImpl implements ConfigurationService {

	@Autowired
	SettingRepo settingRepo;

	@Autowired
	ModelMapper modelMapper;

	@Autowired
    MessageSource messageSource;

	Locale locale = LocaleContextHolder.getLocale();

	@Transactional
	@Override
	public String addSetting(SettingDTO dto) throws CronJobException {
		try {
			ModelMapper mapper = new ModelMapper();
			Setting setting = mapper.map(dto, Setting.class);
			settingRepo.save(setting);
			return messageSource.getMessage("setting.add.success", null, locale);
		}
		catch (Exception e){
			throw new CronJobException(messageSource.getMessage("setting.add.failure",null,locale),e);
		}
	}

	@Override
	public SettingDTO getSetting(Long id) {
		Setting setting = settingRepo.findOne(id);
		ModelMapper mapper = new ModelMapper();
		return mapper.map(setting, SettingDTO.class);
	}

	@Override
	public SettingDTO getSettingByName(String name)
	{
		return convertEntityToDTO(settingRepo.findByName(name));
	}

	@Override
	public Iterable<SettingDTO> getSettings() {
		List<Setting> all = settingRepo.findAll();
		return convertEntitiesToDTOs(all);
	}

	
	@Override
	public Page<SettingDTO> getSettings(Pageable pageDetails) {
		Page<Setting> page = settingRepo.findAll(pageDetails);
		List<SettingDTO> dtOs = convertEntitiesToDTOs(page.getContent());
		long t = page.getTotalElements();
		Page<SettingDTO> pageImpl = new PageImpl<SettingDTO>(dtOs, pageDetails, t);
		return pageImpl;
	}

	private List<SettingDTO> convertEntitiesToDTOs(List<Setting> content) {
		ModelMapper mapper = new ModelMapper();
		List<SettingDTO> allDto = new ArrayList<>();
		for (Setting s : content) {
			SettingDTO dto = mapper.map(s, SettingDTO.class);
			allDto.add(dto);
		}
		return allDto;
	}

	
	@Transactional
	@Override
	public String updateSetting(SettingDTO dto) throws CronJobException {
		try {
			Setting setting = settingRepo.findOne(dto.getId());
			ModelMapper mapper = new ModelMapper();
			mapper.map(dto, setting);
			settingRepo.save(setting);
			return messageSource.getMessage("setting.update.success", null, locale);
		} catch (Exception e) {
			throw new CronJobException(messageSource.getMessage("setting.update.failure",null,locale),e);
		}
	}

	@Override
	public String deleteSetting(Long id) throws CronJobException {
		try {
			settingRepo.delete(id);
			return messageSource.getMessage("setting.delete.success", null, locale);
		}
		catch (Exception e){
			throw new CronJobException(messageSource.getMessage("setting.delete.failure", null, locale),e);
		}
	}

	@Override
	public String toString() {
		return "ConfigurationServiceImpl{" +
				"locale=" + "COnfig is up" +
				'}';
	}

	private SettingDTO convertEntityToDTO(Setting setting) {
		if(setting!=null) {
			return modelMapper.map(setting, SettingDTO.class);
		}
		return null;
	}



}
