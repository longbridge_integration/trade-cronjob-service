package cronjobservice.services.implementation;

import cronjobservice.api.BtaDetails;
import cronjobservice.api.BtaFI;
import cronjobservice.api.FormADetails;
import cronjobservice.api.RemittanceFI;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.context.Context;

import java.util.*;

@Service
public class BtaServiceImpl implements BtaService{
    Logger logger = LoggerFactory.getLogger(this.getClass());
    private Locale locale= LocaleContextHolder.getLocale();

    @Autowired
    private BtaRepo btaRepo;
    @Autowired
    BankUserService bankUserService;
    @Autowired
    BtaService btaService;
    @Autowired
    WorkFlowRepo workFlowRepo;
    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ServiceAuditRepo serviceAuditRepo;
    @Value("${tradeservice.service.uri}")
    private String URI;
    @Autowired
    RestTemplate  template;
    @Autowired
    ConfigurationService configurationService;
    @Autowired
    MessageSource messageSource;
    @Autowired
    MailService mailService;
    @Autowired
    IntegrationService integrationService;
    @Autowired
    CorporateUserRepo corporateUserRepo;
    @Autowired
    CodeRepo codeRepo;

    @Transactional
    @Override
    public List<String> submitBtaToFinacle() throws CronJobException {
        try{
            HashMap<String, String> map = new HashMap<>();
            HashMap<String, Date> map2 = new HashMap<>();

            List<String> responses = new ArrayList<>();
            List<BTA> btas = btaRepo.findByStatusAndSubmitFlag("A", "U");

            int btasize = btas.size();
            logger.info("This is the Number of BTAs Fetched from TMS {}", btasize);

            for (BTA bta:btas) {

                List<CommentDTO> commentDTO = btaService.getComments(bta);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    commentDtd.getMadeOn();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                    map2.put("madeOn", commentDtd.getMadeOn());
                });
                logger.info("Bta Ref Num {} ",bta.getRefNumber());
                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
                BTADTO btadto = btaService.convertEntityTODTO(bta);
                String desc = "PTA_BTA PARAMETERS";
                String eventId="CHARGE EVENT ID";
                String bdcTransist = "BDC TRANSIT";
                String receviable = "BDC RECIEVABLE";
                String payable = "BDC PAYABLE";
                String chargeeventtype = "CHARGE EVENT TYPE";

                Code chargeeventId = codeRepo.findByTypeAndDescription(desc,eventId );
                Code BDCTransist = codeRepo.findByTypeAndDescription(desc,bdcTransist );
                Code BDCreceviable = codeRepo.findByTypeAndDescription(desc,receviable );
                Code BDCpayable = codeRepo.findByTypeAndDescription(desc,payable );
                Code chargetype = codeRepo.findByTypeAndDescription(desc,chargeeventtype );

                System.out.println("Bta chargeeventId {} "+chargeeventId.getCode());
                System.out.println("Bta BDCTransist {} "+BDCTransist.getCode());
                System.out.println("Bta BDCreceviable {} "+BDCreceviable.getCode());
                System.out.println("Bta BDCpayable {} "+BDCpayable.getCode());
                System.out.println("Bta chargetype {} "+chargetype.getCode());

                btadto.setEventId(chargeeventId.getCode());
                btadto.setTransitAcct(BDCTransist.getCode());
                btadto.setReceivableAcct(BDCreceviable.getCode());
                btadto.setPayableAcct(BDCpayable.getCode());
                btadto.setEventType(chargetype.getCode());




                BtaFI btaFI = new BtaFI();
                btaFI.setBtadto(btadto);
                btaFI.setComment(map.get("comment"));
                btaFI.setAuthFirstName(bankUser.getFirstName());
                btaFI.setAuthLastName(bankUser.getLastName());
                btaFI.setMadeOn(map2.get("madeOn"));
                btaFI.setSortCode(bankUser.getBranch().getSortCode());
                logger.info("The Main BTA Bank Useer{}", bankUser.getFirstName());

                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(btadto.getRefNumber(), RequestType.BTA);
                logger.info("BTA Audit size {}", serviceAudits);
                int auditSize = serviceAudits.size();

                if(auditSize >=10) {
                    bta.setSubmitFlag("F");
                    btaRepo.save(bta);
                    btaService.sendBTAITSmail(bta);

                    logger.info("UPDATED BTA SUBMIT FLAG WITH F DUE TO FAILURE");
                }

                if (auditSize < 10) {

                    String uri = URI + "/api/bta/submit";
                  try{
                      String btaSubmitResponse = template.postForObject(uri, btaFI, String.class);
                      logger.info("BTA Submit to Finacle  Message {} ", btaSubmitResponse);
                      responses.add(btaSubmitResponse);

                      ServiceAudit serviceAudit = new ServiceAudit();
                      serviceAudit.setRequestType(RequestType.BTA);
                      serviceAudit.setRequestDate(new Date());
                      serviceAudit.setTempNumber(btadto.getRefNumber());

                      if (btaSubmitResponse.equalsIgnoreCase("Y")) {
                          bta.setSubmitFlag("S");
                          serviceAudit.setResponseFlag("Y");
                          serviceAudit.setResponseMessage("Successful");
                          serviceAuditRepo.save(serviceAudit);
                          btaRepo.save(bta);

                      }
                      else if (btaSubmitResponse.equalsIgnoreCase("null")){
                          serviceAudit.setResponseFlag("N");
                          serviceAudit.setResponseMessage(btaSubmitResponse);
                          serviceAuditRepo.save(serviceAudit);

                      } else if (btaSubmitResponse.equalsIgnoreCase("S")) {
                          bta.setStatus("PV");
                          bta.setSubmitFlag("S");
                          btaRepo.save(bta);
                      } else {
                          serviceAudit.setResponseFlag("N");
                          serviceAudit.setResponseMessage(btaSubmitResponse);
                          serviceAuditRepo.save(serviceAudit);
                          logger.error("Error response for BTA {} ", btaSubmitResponse);
                      }

                  }catch (Exception e){
                      logger.info("Bta Response error {} ",e);
                  }

                }

            }
            return responses;
        }catch(Exception e){
            logger.info("Error submitting BTA to Finacle {} ",e);
            throw  new CronJobException();
        }
    }

    public void getBtaSubmitResponse (){

        List<BtaDetails> btaDetails = integrationService.getBtaSubmitResponse();

        try{

            for (BtaDetails btaDetail : btaDetails) {
                String cifId = btaDetail.getCifid();
                logger.info("The BTA Tran ID abt getting its Response {} ", btaDetail.getTranId());

                if (null==btaDetail.getTranId()) {
                    logger.info("The BTA Finacle Tran ID Does Not Exist");
                    continue;
                }
                BTA bta = modelMapper.map(btaDetail, BTA.class);
                BTADTO btadto = btaService.convertEntityTODTOFinacle(bta);
                logger.info(" BTA Parametre {} ", bta.getRefNumber());
//                CorporateUser corporateUser = corporateUserRepo.findFirstByCustomerId(cifId);
                BTA temp = btaRepo.findByRefNumber(btadto.getRefNumber());

                if(null != temp) {
                    Boolean response = btaService.updateBtaFromFinacle(temp, bta);

                    if (response) {
                        String uri = URI + "/api/bta/updateBtaFlag";
                        logger.info("This is BTA Submit Response Applicant Name {}", btadto.getAppName());
                        try {
                            template.postForObject(uri, btadto, String.class);
                            logger.info("Success after response BTA");
                        } catch (Exception ex) {
                            logger.error("Exception occurred getting response for BTA {}", ex);
                            ex.printStackTrace();
                        }
                    }
                }else {
                    logger.info("BTA Tran ID not Saved");
                }

            }
        }catch (Exception e){
            logger.info("Error getting BTA Response from FInacle {} ",e);
            throw new CronJobException();
        }
    }

    @Transactional
    @Override
    public Boolean updateBtaFromFinacle(BTA tmsbta, BTA finbta) throws CronJobException {
        try {
            logger.info("This is the PTA tran ID {} ", finbta.getTranId());
//            tmsbta.setRefNumber(finbta.getRefNumber());
            tmsbta.setTranId(finbta.getTranId());
            btaRepo.save(tmsbta);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

    @Override
    public Boolean sendBTAITSmail(BTA bta) {
        try {
            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
            String itsMail = settingDTO.getValue();
            String longbridgeMail = "fcmbtrade@longbridgetech.com";
            String[] allMails = new String[] {itsMail,longbridgeMail};
            String formType = "BTA";
            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(bta.getRefNumber(),RequestType.BTA);
            Context context = new Context();
            context.setVariable("name", bta.getAppName());
            context.setVariable("formNumber", bta.getRefNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", DateFormatter.format(new Date()));
            context.setVariable("customerId", bta.getCifid());
            context.setVariable("username", bta.getInitiatedBy());
            context.setVariable("error", serviceAudit.getResponseMessage());
            context.setVariable("code", "PTA");
            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
            Email email = new Email.Builder()
                    .setRecipients(allMails)
                    .setSubject(subject)
                    .setTemplate("mail/finacleSubmission")
                    .build();

            mailService.sendMail(email, context);
            return true;
        }
        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("guarantee.add.success", null, locale));
        }

    }

    @Override
    public BTADTO convertEntityTODTO(BTA bta) {
        BTADTO btadto = modelMapper.map(bta, BTADTO.class);

//        formNxpDTO.setProformaInvoiceDate(DateFormatter.format(formNxp.getProformaDate()));
        if (bta.getStatus() != null && !(bta.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(btadto.getWorkFlow().getCodeType(), btadto.getStatus());
            logger.info("BTA worklflow state {} ",workFlowStates);
            btadto.setStatusDesc(workFlowStates.getDescription());
            btadto.setWorkFlowId(bta.getWorkFlow().getId());
        }
        if (bta.getStatus().equals("P")) {
            btadto.setStatusDesc("Saved");
        }
//        if (bta.getDateCreated() != null) {
//            btadto.setConvert(bta.getDateCreated().toString());
//        }

//        if (bta.getFormNum() == null) {
//            btadto.setFormNum("NOT REGISTERED");
//        }
        return btadto;
    }

    @Override
    public BTADTO convertEntityTODTOFinacle(BTA bta)
    {
        BTADTO btadto=modelMapper.map(bta , BTADTO.class);

//        formNxpDTO.setProformaInvoiceDate(DateFormatter.format(formNxp.getProformaDate()));
        if(bta.getStatus() != null && !(bta.getStatus().equals("P")) ){
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(btadto.getWorkFlow().getCodeType(), btadto.getStatus());
            btadto.setStatusDesc(workFlowStates.getDescription());
            btadto.setWorkFlowId(bta.getWorkFlow().getId());
        }

//        if(bta.getDateCreated()!=null){
//            btadto.setConvert(bta.getDateCreated().toString());
//        }

        return btadto;
    }

    private BTA convertDTOTOEntity(BTADTO btadto) throws CronJobException
    {
//        try {
//            formNxpDTO.setProformaDate(DateFormatter.parse(formNxpDTO.getProformaInvoiceDate()));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        //   formNxpDTO.setWorkFlowId(formNxpDTO .getWorkFlow().getId());
        WorkFlow workFlow = workFlowRepo.findOne(btadto.getWorkFlowId());
        btadto.setWorkFlow(workFlow);
        BTA bta = modelMapper.map(btadto, BTA.class);

        return bta;
    }


    @Override
    public List<CommentDTO> getComments(BTA bta) throws CronJobException {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        {
            commentDTOList=convertEntitiestoDTOs(bta.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus()!="A");


        }
        return  commentDTOList;
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS)
    {
        List<CommentDTO> commentDTOList=new ArrayList<>();

        for(Comments comments:commentDTOS)
        {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return  commentDTOList;
    }

    private CommentDTO convertEntityToDTO(Comments comments)
    {
        WorkFlowStates workFlowStates= workFlowStatesRepo.findByTypeAndCode("BTA",comments.getStatus());
        if(workFlowStates!=null)
        {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments,CommentDTO.class);
    }



//    @Override
//    @Async
//    public String sendBTAITSmail(BTA bta) {
//
//        try {
//            // boolean result=false;
//            SettingDTO settingDTO = configurationService.getSettingByName("SUBMIT_ERROR_EMAIL");
//            String itsMail = settingDTO.getValue();
//            String longbridgeMail = "fcmbtrade@longbridgetech.com";
//            String[] allMails = new String[] {itsMail,longbridgeMail};
//            String formType = "BTA";
//            ServiceAudit serviceAudit = serviceAuditRepo.findFirstByTempNumberAndRequestType(bta.getRefNumber(),RequestType.BTA);
//            logger.info("BTA serviceAudit {}",serviceAudit);
//            Context context = new Context();
//            context.setVariable("name", bta.getAppName());
//            context.setVariable("formNumber", bta.getRefNumber());
//            context.setVariable("formType", formType);
//            context.setVariable("date", DateFormatter.format(new Date()));
//            context.setVariable("customerId", bta.getCifid());
//            context.setVariable("username", bta.getInitiatedBy());
//            context.setVariable("error", serviceAudit.getResponseMessage());
//            context.setVariable("code", "BTA");
//            String subject = String.format(messageSource.getMessage("finacle.submission.subject", null, locale), formType);
//            Email email = new Email.Builder()
//                    .setRecipients(allMails)
//                    .setSubject(subject)
//                    .setTemplate("mail/finacleSubmission")
//                    .build();
//
//            mailService.sendMail(email, context);
//            return subject;
//        }
//        catch (CronJobException e)
//        {
//            throw new CronJobException(messageSource.getMessage("form.add.success", null, locale));
//        }
//    }


}
