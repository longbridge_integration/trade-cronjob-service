package cronjobservice.models;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Showboy on 28/07/2017.
 */
public enum RequestType {

    FORM_M("FORM_M"),
    FORM_A("FORM_A"),
    REMITTANCE("REMITTANCE"),
    IMPORT_LC("IMPORT_LC"),
    EXPORT_LC("EXPORT_LC"),
    EXCHANGE_CONTROL_DOCUMENTS("ECD"),
    OUTWARD_GUARANTEE("OUT_GUAR"),
    INWARD_GUARANTEE("INW_GUAR"),
    EXPORT_BILLS("EXP_BILLS"),
    IMPORT_BILLS("IMP_BILLS"),
    FORM_NXP("FORM_NXP"),
    FORM_NCS("FORM_NCS"),
    FORM_Q("FORM_Q"),
    PTA("PTA"),
    BTA("BTA"),
    LODGE_EXPORT_BILLS_FOR_COLLECTION("LODGE_EXPORT_BILLS_FOR_COLLECTION"),
    LODGE_EXPORT_BILLS_UNDER_LC("LODGE_EXPORT_BILLS_UNDER_LC");

    private String description;

    RequestType(String description){
        this.description = description;
    }

    public static List<RequestType> getTokenTypes(){
        return Arrays.asList(RequestType.values());
    }

}