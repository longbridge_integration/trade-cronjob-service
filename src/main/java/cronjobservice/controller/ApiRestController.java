package cronjobservice.controller;

import cronjobservice.api.*;
import cronjobservice.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ApiRestController {

    //autowire the integration service

    private IntegrationService integrationService;

    @Autowired
    CronJobService cronJobService;
    @Autowired
    PtaService ptaService;
    @Autowired
    BtaService btaService;
    @Autowired
    ExportBillsService exportBillsService;
    @Autowired
    ImportBillService importBillService;
    @Autowired
    FormQService formQService;

    //setter dependency injection for integration service
    @Autowired
    public void setIntegrationService(IntegrationService integrationService) {
        this.integrationService = integrationService;
    }

    @GetMapping("/customer/{cifId}/details")
    public ResponseEntity<CustomerDetails> getCustomersDetailsWithCifId(@PathVariable String cifId) {

        try{
            CustomerDetails customerDetails = integrationService.getCustomerDetailsByCif(cifId);

            return new ResponseEntity<>(customerDetails, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/customer/{cifId}/accounts")
    public ResponseEntity<?> getCustomersAccountWithCifId(@PathVariable String cifId) {

        try{
            List<AccountInfo> listOfAccounts = integrationService.fetchAccountsByCifId(cifId);

            return new ResponseEntity<>(listOfAccounts, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/hscode")
    public ResponseEntity<?> getHSCode(){

        try
        {
            List<TradeCodes> hsCode = integrationService.getHSCodes();

            return new ResponseEntity<>( hsCode, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/sectcode")
    public ResponseEntity<?> getSectorialPurposeCode(){

        try
        {
            List<TradeCodes> sectorialPurposeCode = integrationService.getSectorialPurposeCodes();

            return new ResponseEntity<>(sectorialPurposeCode, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/transportmode")
    public ResponseEntity<?> getModeOfTransport(){

        try
        {
            List<TradeCodes> modeOfTransport = integrationService.getModeOfTransport();

            return new ResponseEntity<>(modeOfTransport, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/ports")
    public ResponseEntity<?> getPortOfDischarge(){

        try
        {
            List<TradeCodes> portOfDischarge = integrationService.getPortCodes();

            return new ResponseEntity<>(portOfDischarge, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/swiftcode")
    public ResponseEntity<?> getSwiftCode(){

        try
        {
            List<SwiftCode> swiftCodes = integrationService.getSwiftCodes();

            return new ResponseEntity<>(swiftCodes, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/v1/{accountNo}/details")
    public ResponseEntity<AccountDetails> getAccountDetailsWithAccountNumber(@PathVariable String accountNo) {

        try{
            AccountDetails accountDetails= integrationService.getAccountDetails(accountNo);

            return new ResponseEntity<>(accountDetails, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }


    @GetMapping("/forex")
    public ResponseEntity<?> getForeignExchangeRate(){

        try
        {
            List<ExchangeRate> exchangeRates = integrationService.getExchangeRate();

            return new ResponseEntity<>(exchangeRates, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/formm/charge")
    public ResponseEntity<?> getFormMCharge(){

        try
        {
            String formMCharge = integrationService.getFormMCharge();

            return new ResponseEntity<>(formMCharge, HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/formm/all")
    public ResponseEntity<?> getAllFormMs(){

        try
        {
            List<FormMDetails> formMDetails = integrationService.getAllFormMs();

            return new ResponseEntity<>(formMDetails, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/formm/{cifId}/all")
    public ResponseEntity<?> getFormMByCifid(@PathVariable String cifId){
        cifId = cifId.toUpperCase();
        try
        {
            List<FormMDetails> formMDetails = integrationService.getFormMByCifId(cifId);

            return new ResponseEntity<>(formMDetails, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/forma/all")
    public ResponseEntity<?> getAllFormAs(){

        try
        {
            List<FormADetails> formADetails = integrationService.getAllFormAs();

            return new ResponseEntity<>(formADetails, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/paymentpurpose")
    public ResponseEntity<?> getPurposeOfPayment() {

        try
        {
            List<TradeCodes> paypurpose = integrationService.getPurposeOfPayment();

            return new ResponseEntity<>(paypurpose, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/currencycode")
    public ResponseEntity<?> getCurrencyCode() {

        try
        {
            List<TradeCodes> currencycode = integrationService.getCurrencyCode();

            return new ResponseEntity<>(currencycode, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/countrycode")
    public ResponseEntity<?> getCountryCode() {

        try
        {
            List<TradeCodes> countryCode = integrationService.getCountryCode();

            return new ResponseEntity<>(countryCode, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/statecode")
    public ResponseEntity<?> getStateCode() {

        try
        {
            List<TradeCodes> stateCode = integrationService.getStateCode();

            return new ResponseEntity<>(stateCode, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/citycode")
    public ResponseEntity<?> getCityCode() {

        try
        {
            List<TradeCodes> cityCode = integrationService.getCityCode();

            return new ResponseEntity<>(cityCode, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{bankCode}/branchcode")
    public ResponseEntity<?> getBranchCode(@PathVariable String bankCode) {

        try
        {
            List<BranchCode> branchCodes = integrationService.getBranch(bankCode);

            return new ResponseEntity<>(branchCodes, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }



    @GetMapping("/fininst")
    public ResponseEntity<?> getFinancialInstitution() {

        try
        {
            List<FinancialInstitution> fininst = integrationService.getFinancialInstitution();

            return new ResponseEntity<>(fininst, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/formss")
    public void runMethods(){
//        cronJobService.testError();
//        cronJobService.addNewAccount();
//       cronJobService.submitFormAToFinacle();
//        cronJobService.getFormAAddedOnFinacle();
//        cronJobService.submitRemittanceToFinacle();
//        cronJobService.submitImportLcToFinacle();
//        cronJobService.getFormNxpAddedOnFinacle();
//        cronJobService.updateCustomerFormMsByCifId("0130262");
//        cronJobService.submitFormAToFinacleTest();
//        cronJobService.updateCorporateDetails();
//        cronJobService.updateRetailUserDetails();
//cronJobService.getRemittanceResponse();
//        cronJobService.getFormAAllocated();
//        cronJobService.submitFormNXPToFinacle();
//        cronJobService.getFormNxpResponse();
//        cronJobService.getFormAResponse();
//        cronJobService.getInwardGuaranteeAddedOnFinacle();
//        cronJobService.submitOutwardGuaranteeToFinacle();
//        cronJobService.getOutwardGuaranteeAddResponse();

//        cronJobService.getFormMAllocated();
//        cronJobService.updateCustomerFormMs();
//        cronJobService.getExportLcAddedOnFinacle();
//        cronJobService.submitExportLcToFinacle();
//        cronJobService.submitImportLcToFinacle();
//        cronJobService.getImportLcIssueResponse();
//        cronJobService.submitExportLcToFinacle();
//        cronJobService.getExportLcAdviseResponse();
//        cronJobService.updateCustomerFormMsByCifId("0299107");
//        cronJobService.getFormAAddedOnFinacle();
//        cronJobService.getFormAAllocated();
//        cronJobService.submitRemittanceToFinacle();
//        ptaService.submitPtaToFinacle();

//        ptaService.getPtaSubmitResponse();
//        btaService.submitBtaToFinacle();

//        btaService.getBtaSubmitResponse();
//        ptaService.getPtaSubmitResponse();
//        cronJobService.UpdateExportLcPendingResponse();
//        cronJobService.submitFormNXPToFinacle();
//        cronJobService.getFormNxpResponse();
//        cronJobService.submitExportLcToFinacle();
//        exportBillsService.LodgeExportBillsUnderLc();
//        exportBillsService.getLodgeResponseExportBillsUnderLc();
//        exportBillsService.LodgeExportBillsUnderCollection();
        cronJobService.getFormMAddedOnFinacle();
//        cronJobService.getOutwardGuaranteeAddedOnFinacle();
//        cronJobService.getExportLcAddedOnFinacle();
//        importBillService.getImportBillForCollectionAddedOnFinacle();
//        importBillService.getImportBillUnderLcAddedOnFinacle();
//        formQService.submitFormQToFinacle();
//        formQService.getFormQSubmitResponse();
//        formQService.getFormQAllocation();
//        cronJobService.getRemittanceIssuedOnFinacle();
//        cronJobService.getImportLcAddedOnFinacle();


    }



}
