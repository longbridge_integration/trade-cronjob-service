package cronjobservice.services;

import cronjobservice.dtos.FormHolder;
import cronjobservice.dtos.InwardGuaranteeDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.CorporateUser;
import cronjobservice.models.InwardGuarantee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface InwardGuaranteeService {

    Boolean addInwardGuaranteeFromFinacleByCifid(InwardGuaranteeDTO inwardGuaranteeDTO) throws CronJobException;

    Boolean addInwardGuaranteeFromFinacle(InwardGuaranteeDTO inwardGuaranteeDTO) throws CronJobException;

    String updateInGuarantee(InwardGuaranteeDTO inwardGuaranteeDTO) throws CronJobException;

    InwardGuaranteeDTO editInGuarantee(Long id) throws CronJobException;

    InwardGuaranteeDTO getInitiatorDetails() throws CronJobException;

    String addInGuarantee(InwardGuaranteeDTO inwardGuaranteeDTO) throws CronJobException;
    InwardGuaranteeDTO getInGuarantee(Long id) throws CronJobException;

    InwardGuaranteeDTO getInGuaranteeNumber(String guaranteeNumber) throws CronJobException;

    Page<InwardGuaranteeDTO> getClosedInGuarantee(Pageable pageable);

    Page<InwardGuaranteeDTO> getInGuaranteeForBankUser(Pageable pageable) throws CronJobException;

    FormHolder createFormHolder(InwardGuaranteeDTO inwardGuaranteeDTO, String name) throws CronJobException;


    FormHolder executeAction(InwardGuaranteeDTO inwardGuaranteeDTO, String name) throws CronJobException;

    Page<InwardGuaranteeDTO> getInGuarantees(Pageable pageDetails);
    Page<InwardGuaranteeDTO> getAllInGuarantees(Pageable pageDetails);


    Page<InwardGuaranteeDTO> getOpenGuarantees(Pageable pageable) throws CronJobException;

    List<InwardGuaranteeDTO> getCustomerClosedInGuarantee();

    List<InwardGuaranteeDTO> getCustomerClosedInGuaranteeByCif(String cifid);

    InwardGuarantee convertDTOTOEntity(InwardGuaranteeDTO inwardGuaranteeDTO);

    InwardGuaranteeDTO convertEntityToDTO(InwardGuarantee inwardGuarantee);

    List<InwardGuaranteeDTO> convertEntitiesToDTOs(Iterable<InwardGuarantee> inwardGuarantees);


    Iterable<InwardGuaranteeDTO> getGuarantees();


    void sendITSmail(CorporateUser user, InwardGuarantee inwardGuarantee);

    void sendSubmitNotification(InwardGuarantee inwardGuarantee) throws CronJobException;
}
