package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

/**
 * Created by user on 10/7/2017.
 */
@Entity
@DiscriminatorValue("Export")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class ExportLc extends LC {
    @ManyToOne
    private FormNxp formNxp;

    @OneToMany
    private List<BillsUnderLc> billsUnderLcList;

    @OneToMany(cascade = CascadeType.ALL)
    private Collection<ExportLcDocument> documents;

    private String approverName;

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public Collection<ExportLcDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(Collection<ExportLcDocument> documents) {
        this.documents = documents;
    }

    public List<BillsUnderLc> getBillsUnderLcList() {
        return billsUnderLcList;
    }

    public void setBillsUnderLcList(List<BillsUnderLc> billsUnderLcList) {
        this.billsUnderLcList = billsUnderLcList;
    }

    public FormNxp getFormNxp() {
        return formNxp;
    }

    public void setFormNxp(FormNxp formNxp) {
        this.formNxp = formNxp;
    }

}
