package cronjobservice.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

/**
 * Created by chiomarose on 25/09/2017.
 */
public class GenerateFormNumber {
    static Logger logger = LoggerFactory.getLogger(CustomJdbcUtil.class);

    private JdbcTemplate jdbcTemplate;


    @Transactional
    public long getFormNumber(String entityName)
    {
             long id = 0;
            ApplicationContext context = SpringContext.getApplicationContext();
            DataSource dataSource = context.getBean(DataSource.class);
        logger.info("entity name {}", entityName);
            String sql = "select MAX(id) FROM " + entityName;
        logger.info("max id  {}",sql);
            jdbcTemplate = new JdbcTemplate(dataSource);
            if (jdbcTemplate.queryForObject(sql, Long.class) == null)
            {
                id = 0;
            } else
                {
                id = jdbcTemplate.queryForObject(sql, Long.class);
                }
            return id;
    }

}
