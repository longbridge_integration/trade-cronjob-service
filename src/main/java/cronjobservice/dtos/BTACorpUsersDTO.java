package cronjobservice.dtos;

import org.springframework.web.multipart.MultipartFile;

public class BTACorpUsersDTO {

    private Long id;
    private int version;

    private String firstName;
    private String lastName;
    private String travelDate;
    private String flightHour;
    private String issueDate;
    private String dob;
    private String countryOfVisit;
    private String corpBvn;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public String getFlightHour() {
        return flightHour;
    }

    public void setFlightHour(String flightHour) {
        this.flightHour = flightHour;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCountryOfVisit() {
        return countryOfVisit;
    }

    public void setCountryOfVisit(String countryOfVisit) {
        this.countryOfVisit = countryOfVisit;
    }

    public String getCorpBvn() {
        return corpBvn;
    }

    public void setCorpBvn(String corpBvn) {
        this.corpBvn = corpBvn;
    }
}
