package cronjobservice.repositories;

import cronjobservice.models.FormA;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;


/**
 * Created by chiomarose on 07/09/2017.
 */
@Repository
public interface FormARepo extends CommonRepo<FormA,Long>
{

  FormA findByTempFormNumber(String tempFormNumber);
  FormA findByFormNumber(String formNumber);
//  FormA findByApplicationNumber(String applicationNumber);
  List<FormA> findByApplicationNumber(String applicationNumber);
  FormA findByFormNumberAndStatus(String formNumber, String status);
  Boolean existsByApplicationNumber(String applicationNumber);

  List<FormA> findByStatusAndSubmitFlag(String S, String SF);

//  @Query("select s from FormA s where  s.status=:approved")
//  List<FormA> findFormByStatus(@Param("approved") String approved);
  @Query("select s from FormA s where s.status!=:openstatus")
  Page<FormA> findStatus(@Param("openstatus") String status, Pageable pageable);

  @Query("select s from FormA s where s.status=:openstatus")
  Page<FormA> findOpenStatus(@Param("openstatus") String status, Pageable pageable);

  Page<FormA> findByStatusIsNull(Pageable pageable);
  Page<FormA> findByStatusNotNull(Pageable pageable);
  @Query("select s from FormA s where s.cifid=:cifid and s.status=:submitted")
  Page<FormA> findByCifidAndStatusS(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

  @Query("select s from FormA s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
  Page<FormA> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);

  @Query("select  count(s)  from FormA s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
  long  countByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline);
  @Query("select  count(s)  from FormA s  where  s.cifid=:cifid")

  long  countByCifid(@Param("cifid") String cifid);

  @Query("select s from FormA s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
  Page<FormA> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

  Page<FormA> findByCifid(String cifid, Pageable pageable);

  @Query("select s from FormA s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
  Page<FormA> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

  @Query("select s from FormA s where s.status is not null and not s.status=:saved ")
  Page<FormA> findByStatus(@Param("saved") String saved, Pageable pageable);

  @Query("select s from FormA s where s.cifid=:cifid  and  s.status is not null")
  Page<FormA> findByClosedCifidAndStatus(@Param("cifid") String cifid, Pageable pageable);

  //Dashboard Query
  @Query("select sum(s.dollarEquivalent) from FormA s where s.cifid=:cifId and s.status=:approve")
  BigDecimal addAmt(@Param("cifId") String cifId, @Param("approve") String approve);

}
