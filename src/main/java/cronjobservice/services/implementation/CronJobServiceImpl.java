package cronjobservice.services.implementation;

import cronjobservice.api.*;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.services.*;
import cronjobservice.utils.FormNumber;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Longbridge on 6/24/2017.
 */
@Service
public class CronJobServiceImpl implements CronJobService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    private AccountRepo accountRepo;
    //    @Autowired
//    private RetailUserRepo retailUserRepo;
//    @Autowired
//    private CorporateRepo corporateRepo;
    @Autowired
    private CorporateUserRepo corporateUserRepo;
    @Autowired
    private CronJobExpressionRepo cronJobExpressionRepo;
    @Autowired
    private CronJobMonitorRepo cronJobMonitorRepo;

    @Autowired
    FormARepo formARepo;

    @Autowired
    FormMRepo formMRepo;

    @Autowired
    FormNxpRepo formNxpRepo;
//
//    @Autowired
//    FormQRepo formQRepo;

    @Autowired
    RemittanceRepo remittanceRepo;

    @Autowired
    ExportLcRepo exportLcRepo;

    @Autowired
    ImportBillRepo importBillRepo;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;

    @Autowired
    ConfigurationService configurationService;

    @Autowired
    WorkFlowRepo workFlowRepo;

    @Autowired
    ServiceAuditRepo serviceAuditRepo;

    @Autowired
    CodeRepo codeRepo;

    @Autowired
    ImportLcRepo importLcRepo;

    @Autowired
    LcService lcService;

    @Autowired
    FormACorporateService formACorporateService;

    @Autowired
    CronJobService cronJobService;



    @Autowired
    FormARetailService formARetailService;

    @Autowired
    FormABankService formABankService;

    @Autowired
    BankUserService bankUserService;
    @Autowired
    AccountService accountService;

    @Autowired
    FormNxpBankService formNxpBankService;

    @Autowired
    FormMService formMService;

    @Autowired
    FormNxpService formNxpService;

    @Autowired
    RemittanceService remittanceService;

    @Autowired
    OutwardGuaranteeRepo outwardGuaranteeRepo;

    @Autowired
    OutwardGuaranteeService outwardGuaranteeService;

    @Autowired
    InwardGuaranteeService inwardGuaranteeService;

    @Autowired
    InwardGuaranteeRepo inwardGuaranteeRepo;

//    @Autowired
//    FormQService formQService;

    @Autowired
    RetailUserRepo retailUserRepo;

    @Autowired
    CorporateRepo corporateRepo;
    @Autowired
    ServiceAuditService serviceAuditService;

    @Value("${tradeservice.service.uri}")
    private String URI;
    @Value("${CMB.ALERT.URL}")
    private String cmbAlert;

    @Autowired
    private RestTemplate template;

    @Override
    public void updateAllAccountName(Account account, AccountDetails accountDetails) throws CronJobException {
        if (!account.getAccountName().equalsIgnoreCase(accountDetails.getAccountName())) {
            account.setAccountName(accountDetails.getAccountName());
//            System.out.println("the account name after setting is" + account.getAccountName());
//            accountRepo.save(account);
        }
    }

    @Override
    public void keepCronJobEprsDetials(String username, String cronExpression, String cronExprDesc, String category) throws CronJobException {
        CronJobExpression cronJobExpression = new CronJobExpression();
        cronJobExpression.setUsername(username);
        cronJobExpression.setCreatedOn(new Date());
        cronJobExpression.setFlag("Y");
        cronJobExpression.setCronExpression(cronExpression);
        cronJobExpression.setCronExpressionDesc(cronExprDesc);
        cronJobExpression.setCategory(category);
        cronJobExpressionRepo.save(cronJobExpression);
    }

    @Override
    public void deleteRunningJob(String category) throws CronJobException {
        CronJobExpression cronJobExpression = cronJobExpressionRepo.findLastByFlagAndCategory("Y", category);
        logger.info("the job to be deleted {}", cronJobExpression);
        if (cronJobExpression != null) {
//            logger.info("about deleting");
            cronJobExpression.setFlag("N");
            cronJobExpressionRepo.save(cronJobExpression);
        }
    }

//    }


    @Transactional
    @Override
    public void getPorts() {
        List<TradeCodes> tradeCodes = integrationService.getPortCodes();
        if (tradeCodes.size() > 0) {
            for (TradeCodes tradeCodes1 : tradeCodes) {
                Code code = new Code();
                code.setCode(tradeCodes1.getCode());
                code.setDescription(tradeCodes1.getCodeDescription());
                code.setType("PORT_CODES");
                codeRepo.save(code);
                logger.info("added PORT CODE SETTINGS TO CODE TABLE");
            }

        }

    }

    @Transactional
    @Override
    public void getModeOfTrans() {
        List<TradeCodes> tradeCodes = integrationService.getModeOfTransport();
        if (tradeCodes.size() > 0) {
            for (TradeCodes tradeCodes1 : tradeCodes) {
                Code code = new Code();
                code.setCode(tradeCodes1.getCode());
                code.setDescription(tradeCodes1.getCodeDescription());
                code.setType("TRANS_MODE");
                codeRepo.save(code);
                logger.info("added TRANS MODE SETTINGS TO CODE TABLE");
            }

        }

    }

    @Transactional
    @Override
    public void getPaymentModeCode() {
        List<TradeCodes> tradeCodes = integrationService.getPaymentModeCode();
        if (tradeCodes.size() > 0) {
            for (TradeCodes tradeCodes1 : tradeCodes) {
                Code code = new Code();
                code.setCode(tradeCodes1.getCode());
                code.setDescription(tradeCodes1.getCodeDescription());
                code.setType("PAYMENT_MODE");
                codeRepo.save(code);
                logger.info("added PAYMENT MODE CODE TO TABLE");
            }

        }

    }

    @Transactional
    @Override
    public void getPurposeOfPayment() {
        List<TradeCodes> tradeCodes = integrationService.getPurposeOfPayment();
        if (tradeCodes.size() > 0) {
            for (TradeCodes tradeCodes1 : tradeCodes) {
                Code code = new Code();
                code.setCode(tradeCodes1.getCode());
                code.setDescription(tradeCodes1.getCodeDescription());
                code.setType("PURPOSE_OF_PAYMENT");
                codeRepo.save(code);
                logger.info("added PURPOSE OF PAYMENT TO TABLE");
            }

        }

    }


    @Transactional
    @Override
    public void getFormItems() throws ParseException {
        List<FormMDetails> formMDetails = integrationService.getAllFormMs();
        logger.info("finacle form m {}", formMDetails.size());
        if (formMDetails.size() > 0) {
            List<FormM> formItems = new ArrayList<FormM>();
            for (FormMDetails items : formMDetails) {
//            String dateStr =  items.getDateCreated();
//            DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
//            Date startDate = (Date)formatter.parse(dateStr);
                FormM formM = new FormM();
//            formM.setDateCreated(startDate);
                SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_WF");
                logger.info("setting the workflow {}", wfSetting);
                WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
                logger.info("workflow {}", workFlow);
                logger.info("workflow ID {}", workFlow.getId());
                formM = modelMapper.map(items, FormM.class);
                formM.setStatus("C");
                formM.setWorkFlow(workFlow);
                formItems.add(formM);
            }
            formMRepo.save(formItems);
            logger.info("Successfully spooled form M record  from Finacle table");
        }
    }

    @Transactional
    @Override
    public void getFormAItems() throws ParseException {

        List<FormADetails> formADetails = integrationService.getAllFormAs();
        logger.info("finacle form A {}", formADetails.size());
        if (formADetails.size() > 0) {
            List<FormA> formItems = new ArrayList<FormA>();

            for (FormADetails items : formADetails) {
//            String dateStr =  items.getDateCreated();
//            DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
//            Date startDate = (Date)formatter.parse(dateStr);
                FormA formA = new FormA();
//            formA.setDateCreated(startDate);
                SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_A_WF");
                logger.info("setting the workflow {}", wfSetting);
                WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
                logger.info("Form A workflow {}", workFlow);
                //  formADTO.setWorkFlowId(workFlow.getId());
                logger.info("Form A workflow ID {}", workFlow.getId());


                formA = modelMapper.map(items, FormA.class);
//                formA.setCifid(items.getCifId().toString());

                formA.setStatus("C");
                formA.setWorkFlow(workFlow);
                formItems.add(formA);
            }

            formARepo.save(formItems);
            logger.info("Successfully spooled form A record from Finacle table");
        }

    }


    @Override
    public void updateAllAccountCurrency(Account account, AccountDetails accountDetails) throws CronJobException {
//        logger.info("The account size {}",allAccounts.size());
        if ((account.getCurrencyCode() == null) || (!account.getCurrencyCode().equalsIgnoreCase("")) || (!accountDetails.getCurrency().equalsIgnoreCase(account.getCurrencyCode()))) {
            account.setCurrencyCode(accountDetails.getCurrency());
//            logger.info("the new account currency {} and {}" , account.getCurrencyCode(),accountDetails.getAcctCrncyCode());
            accountRepo.save(account);
        }
    }

    @Override
    public void updateAccountStatus(Account account, AccountDetails accountDetails) throws CronJobException {
        if ((account.getStatus() == null) || (!account.getStatus().equalsIgnoreCase("")) || (!account.getStatus().equalsIgnoreCase(accountDetails.getStatus()))) {
            account.setStatus(accountDetails.getStatus());
//            System.out.println("the account status after setting is" + account.getStatus());
            accountRepo.save(account);
        }
    }


    @Override
    public void getExpiredForm() throws CronJobException {
        List<FormM> allFormMs = formMRepo.findAll();
        logger.info("getting all formMs {}", allFormMs);
        if (!(allFormMs.isEmpty())) {
            int weeks = 0;
            for (FormM formM : allFormMs) {
                Date date = formM.getExpiry();
                Date presentDate = new Date();

                long difference = date.getTime() - presentDate.getTime();
                logger.info("getting the difference {}", difference);
                //   float milliseconds=(difference/(1000*60*60*24));
                float daysBetween = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
                logger.info("getting the days between{}", daysBetween);
                weeks = (int) daysBetween / 7;
                if (weeks <= 2 && daysBetween >= 0) {
                    formM.setExpiryStatus("P");
                    formMRepo.save(formM);
                }
                if (daysBetween < 0) {
                    formM.setExpiryStatus("Y");
                    formMRepo.save(formM);
                }
            }
        }

    }



    @Override
    public void getFormMAddedOnFinacle() throws CronJobException {
        try {
            logger.info("abt fetching form m from finacle");



                List<FormMDetails> formMDetails = integrationService.getFormMAddedOnFinacle();
                for (FormMDetails formMDetail : formMDetails) {

                    if (null ==formMDetail.getFormNumber()){
                        continue;
                    }


                    logger.info("This is the form M added on finacle number {} ", formMDetail.getCifid());

                    FormM tmsFormM = formMRepo.findByFormNumber(formMDetail.getFormNumber());

                    if(tmsFormM==null) {
                        FormMDTO formMDTO = modelMapper.map(formMDetail, FormMDTO.class);
                        Boolean response = formMService.addFormMFromFinacle(formMDTO);
                    }else {
                        formMService.updateFormMFromFinacle(formMDetail,tmsFormM);
                    }

                }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
//
//    @Override
//    public void getFormMAddedOnFinacle() throws CronJobException {
//        try {
//            logger.info("abt fetching form m from finacle");
//            List<Corporate> corporateList = corporateRepo.findAll();
//            for (Corporate corporate : corporateList) {
////                List<String> formNumbers = formMRepo.findByCifid(corporate.getCustomerId());
//                logger.info("corporate id for form m  {} ",corporate);
//
////                List<FormMDetails> formMDetails = integrationService.getFormMByCifId(corporate.getCustomerId());
//                List<FormMDetails> formMDetails = integrationService.getFormMByCifId(corporate.getCustomerId());
//                for (FormMDetails formMDetail : formMDetails) {
//
//                    if (formMDetail.getFormNumber()==null){
//                        continue;
//                    }
//
//                    FormMDTO formMDTO = modelMapper.map(formMDetail, FormMDTO.class);
//                    logger.info("This is the form M aded on finacle number {} ", formMDTO.getFormNumber());
//
//                    FormM formNumberTMS = formMRepo.findByFormNumber(formMDTO.getFormNumber());
//
//
//                        Boolean response = formMService.addFormMFromFinacle(formMDTO);
//
//
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    @Override
    public void updateCustomerFormMsBkp() throws CronJobException {
        try {
            List<Corporate> corporateList = corporateRepo.findAll();
            for (Corporate corporate : corporateList) {

                List<FormMDetails> formMDetails = integrationService.getFormMByCifId(corporate.getCustomerId());
                for (FormMDetails formMDetail : formMDetails) {
                    FormMDTO formMDTO = modelMapper.map(formMDetail, FormMDTO.class);
                    logger.info("This is the form M number {} ", formMDTO.getFormNumber());
                    Boolean response = formMService.addFormMFromFinacle(formMDTO);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateCustomerFormMsByCifId(String cifId) throws CronJobException {
        try {
//            List<Corporate> corporateList = corporateRepo.findAll();
//            for (Corporate corporate : corporateList){
            List<FormMDetails> formMDetails = integrationService.getFormMByCifId(cifId);
            for (FormMDetails formMDetail : formMDetails) {

                FormMDTO formMDTO = modelMapper.map(formMDetail, FormMDTO.class);
                Boolean response = formMService.addFormMFromFinacle(formMDTO);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void updateUserDetailsByCifid(String cifid) {
        List<FormADetails> formAS = integrationService.getFormAByCifId(cifid);
        List<FormMDetails> formMDetails = integrationService.getFormMByCifId(cifid);
        List<FormNxpDetails> formNxpDetails = integrationService.getFormNxpByCifId(cifid);
        List<RemittanceDetails> remittanceDetails = integrationService.getRemittanceByCifId(cifid);
        List<PaarDetails> paarDetails = integrationService.getPaarByCifId(cifid);

    }

    @Transactional
    @Override
    public List<String> submitFormAToFinacleTest() throws CronJobException {
        try {
            List<String> responses = new ArrayList<>();
            List<FormA> formAs = formARepo.findByStatusAndSubmitFlag("A", "U");
            int formSize = formAs.size();
            logger.info("Submit Form A to Finacle size {}", formSize);


            for(FormA formA:formAs){

                logger.info("Form A submit to Finacle temp No {} ",formA.getTempFormNumber());
                FormAFI formAFI = formABankService.getCommentsAndBankersDetails(formA);
                FormADTO formADTO = formAFI.getFormADTO();
                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(formADTO.getTempFormNumber(), RequestType.FORM_A);
                int auditSize = serviceAudits.size();
                if (auditSize < 10) {
                    try {
                        String uri = URI + "/api/forma/submit";
                        String formResponse = template.postForObject(uri, formAFI, String.class);
                        logger.info("The Main of FormAAA Responsssseee  {}", formResponse);
                        responses.add(formResponse);
                        if (formResponse.equalsIgnoreCase("Y")) {
                            formA.setSubmitFlag("S");
                            formARepo.save(formA);
                        } else if (formResponse.equalsIgnoreCase("S")) {
                            formA.setStatus("PV");
                            formA.setSubmitFlag("S");
                            formARepo.save(formA);
                        } else {
                            logger.error("Error response for Form A {} ", formResponse);
                        }
                        ServiceAudit serviceAudit = serviceAuditService.getFormAServiceAudit(formADTO, formResponse);
                        serviceAuditRepo.save(serviceAudit);
                    } catch (Exception ex) {
                        logger.error("Exception occurred submitting form A{}", ex);
                        ex.printStackTrace();
                    }
                } else {
                    Boolean formAmail = formACorporateService.sendFormAITSmail(formA);
                    if (formAmail) {
                        formA.setSubmitFlag("F");
                        formARepo.save(formA);
                    }
                    logger.info("Message After Form A Sending Mail");
                }
            }
            return responses;
        } catch (CronJobException e) {
            logger.error("This is the Error", e);
            throw new CronJobException();
        }
    }


    @Transactional
    @Override
    public List<String> submitFormAToFinacle() throws CronJobException {
        try {
            HashMap<String, String> map = new HashMap<>();
            HashMap<String, Date> map2 = new HashMap<>();
            List<String> responses = new ArrayList<>();
            List<FormA> formAs = formARepo.findByStatusAndSubmitFlag("A", "U");
            int formSize = formAs.size();
            logger.info("This is the Number of Form As Fetched from TMS {}", formSize);


                for (FormA formA : formAs){
                List<CommentDTO> commentDTO = formABankService.getComments(formA);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    commentDtd.getMadeOn();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                    map2.put("madeOn", commentDtd.getMadeOn());
                });
                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
                FormADTO formADTO = formACorporateService.convertEntityTODTO(formA);
                FormAFI formAFI = new FormAFI();
                formAFI.setFormADTO(formADTO);
                formAFI.setComment(map.get("comment"));
                formAFI.setAuthFirstName(bankUser.getFirstName());
                formAFI.setAuthLastName(bankUser.getLastName());
                formAFI.setMadeOn(map2.get("madeOn"));
                formAFI.setSortCode(bankUser.getBranch().getSortCode());
                logger.info("The Main of form AA Bank Useer{}", bankUser.getFirstName());
                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(formADTO.getTempFormNumber(), RequestType.FORM_A);
                int auditSize = serviceAudits.size();
                if(auditSize >=10) {
                        formA.setSubmitFlag("F");
                        formARepo.save(formA);
                        Boolean formAmail = formACorporateService.sendFormAITSmail(formA);

                        logger.info("UPDATED FORMA SUBMIT FLAG WITH F DUE TO FAILURE");
                    }
                if (auditSize < 10) {
                    String uri = URI + "/api/forma/submit";
//                        logger.info("The Main of form FII AAA {}", formADTO);
                    logger.info("The Main of form FII AAA {}", formAFI);
                    try{

                    String formResponse = template.postForObject(uri, formAFI, String.class);
                    logger.info("The Main of FormA Response  {}", formResponse);
                    responses.add(formResponse);

                        ServiceAudit serviceAudit = new ServiceAudit();
                        serviceAudit.setRequestType(RequestType.FORM_A);
                        serviceAudit.setRequestDate(new Date());
                        serviceAudit.setTempNumber(formADTO.getTempFormNumber());

                    if (formResponse.equalsIgnoreCase("Y")) {
                        formA.setSubmitFlag("S");
                        serviceAudit.setResponseFlag("Y");
                        serviceAudit.setResponseMessage("Successful");
                        serviceAuditRepo.save(serviceAudit);
                        formARepo.save(formA);
                    } else if (formResponse.equalsIgnoreCase("S")) {
                        formA.setStatus("PV");
                        formA.setSubmitFlag("S");
                        formARepo.save(formA);
                    } else {
                        serviceAudit.setResponseFlag("N");
                        serviceAudit.setResponseMessage(formResponse);
                        logger.error("Error response for Form A {} ", formResponse);
                        serviceAuditRepo.save(serviceAudit);
                    }


                    } catch (Exception ex) {
                        logger.error("Exception occurred submitting form A{}", ex);
                        ex.printStackTrace();
                    }
                }
            }
            return responses;
        } catch (Exception e) {
            logger.error("Exception occurred submitting form A{}", e.getMessage());
            e.printStackTrace();
            throw new CronJobException();

        }
    }

    @Transactional
    @Override
    public List<String> submitFormNXPToFinacle() throws CronJobException {
        try {
//180000866  180000818 180000853
                HashMap<String, String> map = new HashMap<>();
                HashMap<String, Date> map2 = new HashMap<>();
                List<String> responses = new ArrayList<>();
                List<FormNxp> formNxps = formNxpRepo.findByStatusAndSubmitFlag("A", "U");
                int formSize = formNxps.size();

                logger.info("This is the Number of Form NXPs Fetched from TMS {}", formSize);


                for (FormNxp formNxp : formNxps) {
                    List<CommentDTO> commentDTO = formNxpBankService.getComments(formNxp);
                    commentDTO.forEach(commentDtd -> {
                        commentDtd.getUsername();
                        commentDtd.getComment();
                        commentDtd.getMadeOn();
                        map.put("userName", commentDtd.getUsername());
                        map.put("comment", commentDtd.getComment());
                        map2.put("madeOn", commentDtd.getMadeOn());
                    });

                    BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
                    logger.info("The Main of form nxp Bank Useer{}", bankUser.getFirstName());
                    FormNxpDTO formNxpDTO = formNxpService.convertEntityTODTO(formNxp);
                    FormNxpFI formNxpFI = new FormNxpFI();
                    formNxpFI.setFormNxpDTO(formNxpDTO);
                    formNxpFI.setComment(map.get("comment"));
                    formNxpFI.setAuthFirstName(bankUser.getFirstName());
                    formNxpFI.setAuthLastName(bankUser.getLastName());
                    formNxpFI.setMadeOn(map2.get("madeOn"));
                    formNxpFI.setSortCode(bankUser.getBranch().getSortCode());
                    logger.info("Form NXp Bank Useer{}", bankUser.getFirstName());
                    List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(formNxpDTO.getTempFormNumber(), RequestType.FORM_NXP);
                    int auditSize = serviceAudits.size();

                    if (auditSize >= 10) {
                        formNxp.setSubmitFlag("F");
                        formNxpRepo.save(formNxp);
                        Boolean formNxpmail = formNxpBankService.sendFormNxpITSmail(formNxp);

                        logger.info("UPDATED FORMNXP SUBMIT FLAG WITH F DUE TO FAILURE");
                    }
                    if (auditSize < 10) {
                        String uri = URI + "/api/formnxp/submit";
                        try {

                            String formResponse = template.postForObject(uri, formNxpFI, String.class);
                            logger.info("The Main of Form Nxp Response  {}", formResponse);
                            responses.add(formResponse);

                            ServiceAudit serviceAudit = new ServiceAudit();
                            serviceAudit.setRequestType(RequestType.FORM_NXP);
                            serviceAudit.setRequestDate(new Date());
                            serviceAudit.setTempNumber(formNxpDTO.getTempFormNumber());

                            if (null==formResponse){
                                serviceAudit.setResponseMessage(formResponse);
                                serviceAudit.setResponseFlag("N");
                                serviceAuditRepo.save(serviceAudit);

                            }
                            else if (formResponse.equalsIgnoreCase("Y")) {
                                formNxp.setSubmitFlag("S");
                                serviceAudit.setResponseFlag("Y");
                                serviceAudit.setResponseMessage("Successful");
                                serviceAuditRepo.save(serviceAudit);
                                formNxpRepo.save(formNxp);
                            }
                            else if (formResponse.equalsIgnoreCase("S")) {
                                formNxp.setStatus("PV");
                                formNxp.setSubmitFlag("S");
                                formNxpRepo.save(formNxp);
                            } else {
                                serviceAudit.setResponseFlag("N");
                                serviceAudit.setResponseMessage(formResponse);
                                logger.error("Error response for Form Nxp {} ", formResponse);
                                serviceAuditRepo.save(serviceAudit);
                            }


                        } catch (Exception ex) {
                            logger.error("Exception occurred submitting form NXP {}", ex);
                            ex.printStackTrace();
                        }
                    }
                }
                return responses;
            } catch (Exception e) {
                logger.error("Exception occurred submitting form Nxp {}", e.getMessage());
                e.printStackTrace();
                throw new CronJobException();

            }
    }


    //            HashMap<String, String> map = new HashMap<>();
//            HashMap<String, Date> map2 = new HashMap<>();
//            List<String> responses = new ArrayList<>();
//            List<FormNxp> formNxps = formNxpRepo.findByStatusAndSubmitFlag("A", "U");
//            int formSize = formNxps.size();
//
//            logger.info("This is the Number of Form NXPs Fetched from TMS {}", formSize);
//
//            for (FormNxp formNxp:formNxps) {
//                List<CommentDTO> commentDTO = formNxpBankService.getComments(formNxp);
//                commentDTO.forEach(commentDtd -> {
//                    commentDtd.getUsername();
//                    commentDtd.getComment();
//                    commentDtd.getMadeOn();
//                    map.put("userName", commentDtd.getUsername());
//                    map.put("comment", commentDtd.getComment());
//                    map2.put("madeOn", commentDtd.getMadeOn());
//                });
//                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
//                FormNxpDTO formNxpDTO = formNxpService.convertEntityTODTO(formNxp);
//                FormNxpFI formNxpFI = new FormNxpFI();
//                formNxpFI.setFormNxpDTO(formNxpDTO);
//                formNxpFI.setComment(map.get("comment"));
//                formNxpFI.setAuthFirstName(bankUser.getFirstName());
//                formNxpFI.setAuthLastName(bankUser.getLastName());
//                formNxpFI.setMadeOn(map2.get("madeOn"));
//                formNxpFI.setSortCode(bankUser.getBranch().getSortCode());
//                logger.info("Form NXp Bank Useer{}", bankUser.getFirstName());
//                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(formNxpDTO.getTempFormNumber(), RequestType.FORM_NXP);
//                int auditSize = serviceAudits.size();
//                if (auditSize < 10) {
//                    String uri = URI + "/api/formnxp/submit";
////                        logger.info("The Main of form FII NXP {}", formNxpDTO);
//                    logger.info("The Main of form FII NXP {}", formNxpFI);
//                    try {
//                        String formResponse = template.postForObject(uri, formNxpFI, String.class);
//                        logger.info("The Main of FormNxp Responsssseee  {}", formResponse);
//                        responses.add(formResponse);
//                        if (formResponse.equalsIgnoreCase("Y")) {
//                            formNxp.setSubmitFlag("S");
//                            formNxpRepo.save(formNxp);
//                        } else if (formResponse.equalsIgnoreCase("S")) {
//                            formNxp.setStatus("PV");
//                            formNxp.setSubmitFlag("S");
//                            formNxpRepo.save(formNxp);
//                        } else {
//                            logger.error("Error response for Form Nxp {} ", formResponse);
//                        }
//                        ServiceAudit serviceAudit = new ServiceAudit();
//                        serviceAudit.setRequestType(RequestType.FORM_NXP);
//                        serviceAudit.setRequestDate(new Date());
//                        serviceAudit.setTempNumber(formNxpDTO.getTempFormNumber());
//                        serviceAudit.setResponseMessage(formResponse);
//                        if (formResponse.equalsIgnoreCase("Y")) {
//                            serviceAudit.setResponseFlag("Y");
//                            serviceAudit.setResponseMessage("Successful");
//                        } else {
//                            serviceAudit.setResponseFlag("N");
//                            serviceAudit.setResponseMessage(formResponse);
//                        }
//                        serviceAuditRepo.save(serviceAudit);
//                    } catch (Exception ex) {
//                        logger.error("Exception occurred submitting form A{}", ex);
//                        ex.printStackTrace();
//                    }
//                }
//                else {
//                    formNxp.setSubmitFlag("F");
//                    formNxpRepo.save(formNxp);
//
//                    Boolean formNxpmail = formNxpBankService.sendFormNxpITSmail(formNxp);
//                    logger.info("Message After Form Nxp Sending Mail");
//                }
//            }
//            return responses;
//        } catch (Exception e) {
//            logger.error("Exception occurred submitting form NXP {}", e.getMessage());
//            throw new CronJobException();
//
//        }
//

    //new

//    @Transactional
//    @Override
//    public List<String> submitFormQToFinacle() throws CronJobException {
//        try {
//            HashMap<String, String> map = new HashMap<>();
//            HashMap<String, Date> map2 = new HashMap<>();
//            List<String> responses = new ArrayList<>();
//            List<FormQ> formQs = formQRepo.findByStatusAndSubmitFlag("A", "U");
//            int formSize = formQs.size();
//            logger.info("This is the Number of Form Qs Fetched from TMS {}", formSize);
//
//            for (FormQ formQ:formQs) {
////                FormQ formQ = formQs.get(b);
//                List<CommentDTO> commentDTO = formQService.getComments(formQ);
//                commentDTO.forEach(commentDtd -> {
//                    commentDtd.getUsername();
//                    commentDtd.getComment();
//                    commentDtd.getMadeOn();
//                    map.put("userName", commentDtd.getUsername());
//                    map.put("comment", commentDtd.getComment());
//                    map2.put("madeOn", commentDtd.getMadeOn());
//                });
//                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
//                FormQDTO formQDTO = formQService.convertEntityToDTO(formQ);
//                FormQFI formQFI = new FormQFI();
//                formQFI.setFormQDTO(formQDTO);
//                formQFI.setComment(map.get("comment"));
//                formQFI.setAuthFirstName(bankUser.getFirstName());
//                formQFI.setAuthLastName(bankUser.getLastName());
//                formQFI.setMadeOn(map2.get("madeOn"));
//                formQFI.setSortCode(bankUser.getBranch().getSortCode());
//                logger.info("Form QQ Bank Useer{}", bankUser.getFirstName());
//                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(formQDTO.getTempFormNumber(), RequestType.FORM_Q);
//                int auditSize = serviceAudits.size();
//                if (auditSize < 10) {
//                    String uri = URI + "/api/formq/submit";
//                    logger.info("The Main of formq FII QQQ {}", formQDTO);
//                    logger.info("The Main of formqq FII QQ {}", formQFI);
//                    String formResponse = template.postForObject(uri, formQFI, String.class);
//                    logger.info("FormQQ Responsssseee  {}", formResponse);
//                    responses.add(formResponse);
//                    if (formResponse.equalsIgnoreCase("Y")) {
//                        formQ.setSubmitFlag("S");
//                        formQRepo.save(formQ);
//                    } else if (formResponse.equalsIgnoreCase("S")) {
//                        formQ.setStatus("PV");
//                        formQ.setSubmitFlag("S");
//                        formQRepo.save(formQ);
//                    } else {
//                        logger.error("Error response for Form Q {} ", formResponse);
//                    }
//                    ServiceAudit serviceAudit = new ServiceAudit();
//                    serviceAudit.setRequestType(RequestType.FORM_Q);
//                    serviceAudit.setRequestDate(new Date());
//                    serviceAudit.setTempNumber(formQDTO.getTempFormNumber());
//                    if (formResponse.equalsIgnoreCase("Y")) {
//                        serviceAudit.setResponseFlag("Y");
//                        serviceAudit.setResponseMessage("Successful");
//                    } else {
//                        serviceAudit.setResponseFlag("N");
//                        serviceAudit.setResponseMessage(formResponse);
//                    }
//                    serviceAuditRepo.save(serviceAudit);
//                } else {
////                    Boolean formQmail =formQService.sendFormNxpITSmail(formNxp);
////                    if (formNxpmail){
////                        formNxp.setSubmitFlag("F");
////                        formNxpRepo.save(formNxp);
////                    }
////                    logger.info("Message After Form A Sending Mail");
//                }
//            }
//            return responses;
//        } catch (Exception e) {
//            logger.error("Exception occurred submitting form Q {}", e.getMessage());
//            throw new CronJobException();
//
//        }
//    }

    @Transactional
    @Override
    public List<String> submitRemittanceToFinacle() throws CronJobException {
        try {
            HashMap<String, String> map = new HashMap<>();
            HashMap<String, Date> map2 = new HashMap<>();
            List<String> responses = new ArrayList<>();
            List<Remittance> remittances = remittanceRepo.findByStatusAndSubmitFlag("A", "U");
            int size = remittances.size();
            logger.info("This is the Number of Remittances Fetched from TMS {}", size);
            for (Remittance remittance : remittances){
                List<CommentDTO> commentDTO = remittanceService.getComments(remittance);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    commentDtd.getMadeOn();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                    map2.put("madeOn", commentDtd.getMadeOn());
                });
                logger.info("remitance for user {}",map.get("userName"));
                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
                RemittanceDTO remittanceDTO = remittanceService.convertEntityToDTO(remittance);
                RemittanceFI remittanceFI = new RemittanceFI();
                remittanceFI.setRemittanceDTO(remittanceDTO);
                remittanceFI.setComment(map.get("comment"));
                remittanceFI.setAuthFirstName(bankUser.getFirstName());
                remittanceFI.setAuthLastName(bankUser.getLastName());
                remittanceFI.setMadeOn(map2.get("madeOn"));
                remittanceFI.setSortCode(bankUser.getBranch().getSortCode());
                logger.info("The Main of remittance Bank Useer{}", bankUser.getFirstName());
//
                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(remittanceDTO.getTempFormNumber(), RequestType.REMITTANCE);
                logger.info("Remittance Audit size {}", serviceAudits);
                int auditSize = serviceAudits.size();
                if (auditSize < 10) {

                        String uri = URI + "/api/remittance/submit";
                        logger.info("The Main of remittance FII REMITTANCE  {}", remittanceFI);
                        try{
                        String remitResponse = template.postForObject(uri, remittanceFI, String.class);
                        logger.info("The Main of Remittance Responsssseee  {}", remitResponse);
                        responses.add(remitResponse);
                        if (remitResponse.equalsIgnoreCase("Y")) {
                            remittance.setSubmitFlag("S");
                            remittanceRepo.save(remittance);
                        } else if (remitResponse.equalsIgnoreCase("S")) {
                            remittance.setStatus("PV");
                            remittance.setSubmitFlag("S");
                            remittanceRepo.save(remittance);
                        } else {
                            logger.error("Error response for Remittance {} ", remitResponse);
                        }
                        ServiceAudit serviceAudit = new ServiceAudit();
                        serviceAudit.setRequestType(RequestType.REMITTANCE);
                        serviceAudit.setRequestDate(new Date());
                        serviceAudit.setTempNumber(remittanceDTO.getTempFormNumber());
                        if (remitResponse.equalsIgnoreCase("Y")) {
                            serviceAudit.setResponseFlag("Y");
                            serviceAudit.setResponseMessage("Successful");
                        } else {
                            serviceAudit.setResponseFlag("N");
                            serviceAudit.setResponseMessage(remitResponse);
                        }
                        serviceAuditRepo.save(serviceAudit);
                    } catch (Exception e) {
                        logger.error("Exception occurred submitting remittance {}", e.getMessage());
                        e.printStackTrace();
                    }
                } else {
                    remittance.setSubmitFlag("F");
                    remittanceRepo.save(remittance);

                    Boolean remitMail = remittanceService.sendRemittanceITSmail(remittance);
                    logger.info("remitMail {}",remitMail);

                    logger.info("Message After Remittance Sending Mail");
                }
            }
            return responses;
        } catch (Exception e) {
            logger.error("Exception occurred submitting remittance {}", e.getMessage());
            e.printStackTrace();
            throw new CronJobException();

        }
    }

    @Override
    public void getFormAAllocated() {
        List<FormADetails> formADetails = integrationService.getFormAAllocated();
        try {
            for (FormADetails formADetail : formADetails) {
                String cifId = formADetail.getCifid();
                logger.info("The Form A allocation temp no {} ", formADetail.getTempFormNumber());
                logger.info("The Form A Number {} ", formADetail.getFormNumber());
                FormA formA = modelMapper.map(formADetail, FormA.class);
                logger.info("The Form A Allocation amount {} ", formA.getAllocatedAmount());

//                logger.info("This is the user type", corp);
                if (formA.getFormNumber() != null && !"".equals(formA.getFormNumber())) {
                    logger.info("The Form A FORM number {} ", formA.getFormNumber());
                    FormA temp = formARepo.findByFormNumber(formA.getFormNumber());
                    if (null==temp) {
                        logger.info("The Temp Form A {} Does Not Exist",formA.getFormNumber());
                        continue;
                    }
                    if(null==formA.getTempFormNumber() || "".equals(formA.getTempFormNumber())) {
                        formA.setTempFormNumber(temp.getTempFormNumber());
                    }
                    FormADTO formADTO = formACorporateService.convertEntityTODTOFinacle(formA);
                       Boolean response = formARetailService.updateFormAAllocation(temp, formA,formADTO);

                    if (response) {
                        String uri = URI + "/api/forma/updateallocationflag";
                        template.postForObject(uri, formADTO, String.class);
                        logger.info("updating allocation job was successful");
                    } else {
                        System.out.println("Allocation not saved on TMS");
                    }
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    @Override
    public String getFormAAddedOnFinacle() {
        List<FormADetails> formADetails = integrationService.getFormAAddedOnFinacle();
        try {
            for (FormADetails formADetail : formADetails) {
                try{
                List<FormA> formAs = formARepo.findByApplicationNumber(formADetail.getApplicationNumber());
                    for (FormA formA:formAs) {
                        formARepo.delete(formA);
                    }
                    FormA formA = modelMapper.map(formADetail, FormA.class);
                    SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_A_WF");
                    WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//
                    formA.setWorkFlow(workFlow);
//
                        Boolean response = formACorporateService.saveFormAFromFinacle(formA);
                        FormA formA2 = formARepo.findByFormNumber(formA.getFormNumber());
                        FormADTO formADTO2 = formACorporateService.convertEntityTODTOFinacle(formA2);
                        if (response) {
                            String uri = URI + "/api/forma/addsuccess";
                            template.postForObject(uri, formADTO2, String.class);
                            logger.info("Success after response Form A added  on finacle with utilize amt {}",formA.getUtilization());
                        } else {
                            System.out.println("Form A not Saved");
                        }

            }catch (Exception e){
                    logger.info("can't update because {}",e);
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Transactional
    @Override
    public String getFormACanceledOnFinacle() {
        List<FormADetails> formADetails = integrationService.getFormAAddedOnFinacle();
        try {
            for (FormADetails formADetail : formADetails) {

                FormA finformA = modelMapper.map(formADetail, FormA.class);
                logger.info("The Form A cancelled amount {} ", finformA.getAllocatedAmount());

                if (finformA.getFormNumber() != null && !"".equals(finformA.getFormNumber())) {
                    FormA temp = formARepo.findByFormNumber(finformA.getFormNumber());
                    if (null==temp) {
                        logger.info("The Temp Form A {} Does Not Exist",finformA.getFormNumber());
                        continue;
                    }
                    if(null==finformA.getTempFormNumber() || "".equals(finformA.getTempFormNumber())) {
                        finformA.setTempFormNumber(temp.getTempFormNumber());
                    }

                    Boolean response = formACorporateService.cancelFormAFromFinacle(temp, finformA);

                    if (response) {
                        String uri = URI + "/api/forma/cancelflag";
                        FormADTO formADTO = formACorporateService.convertEntityTODTOFinacle(finformA);
                        template.postForObject(uri, formADTO, String.class);
                        logger.info("updating form a cancel job was successful");
                    } else {
                        System.out.println("Cancelled not saved on TMS");
                    }
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getFormNxpAddedOnFinacle() {
        List<FormNxpDetails> formNxpDetails = integrationService.getFormNxpAddedOnFinacle();
        try {
            for (FormNxpDetails formNxpDetail : formNxpDetails) {
                logger.info("Form Nxp added on finacle number {}",formNxpDetail.getFormNum());
                logger.info("Form Nxp added on finacle form amt {}",formNxpDetail.getTotalValueOfGoods());
                logger.info("Form Nxp added on finacle utilization {}",formNxpDetail.getUtilization());
                FormNxp formNxpp = formNxpRepo.findByFormNum(formNxpDetail.getFormNum());
                if (formNxpp == null) {
                    FormNxp formNxp = modelMapper.map(formNxpDetail, FormNxp.class);
                    SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_NXP_WF");
                    WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//                    formNxp.setStatus("A");
//                    formNxp.setWorkFlow(workFlow);
                    FormNxpDTO formNxpDTO = formNxpService.convertEntityTODTO(formNxp);
                    formNxpDTO.setWorkFlowId(workFlow.getId());
                    formNxpDTO.setSubmitFlag("S");

                    Boolean response = formNxpService.addFormFromFinacle(formNxpDTO);

                    FormNxp formNxp2 = formNxpRepo.findByFormNum(formNxpDTO.getFormNum());
//                    FormNxpDTO formNxpDTO2 = formNxpService.convertEntityTODTO(formNxp2);
                    if (response) {
                        String uri = URI + "/api/formnxp/addsuccess";
                        logger.info("The Form NXP Added On Finacle First Name {}", formNxpDTO.getExporterName());
                        template.postForObject(uri, formNxpDTO, String.class);
                        logger.info("Success after response form NXP");
                    } else {
                        System.out.println("Form Nxp not Saved");
                    }
                } else {
                    System.out.println("This Form Nxp already exist");
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
        return null;

    }

//    @Override
//    public String getFormMAddedOnFinacle2() {
//        List<FormMDetails> formMDetails = integrationService.getFormMAddedOnFinacle();
//        try {
//            for (FormMDetails formMDetail : formMDetails) {
//
//                if (null==formMDetail.getFormNumber()) {
//                    logger.info("Form M added on finacle form number Does Not Exist");
//                    continue;
//                }
//
//                FormM formM = modelMapper.map(formMDetail, FormM.class);
////                RetailUser retailUser = retailUserRepo.findFirstByCustomerId(cifId);
//                SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_M_WF");
//                WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//                formM.setStatus("A");
//
//                FormMDTO formMDTO = formMService.convertEntityTODTO(formM);
//                formMDTO.setWorkFlowId(workFlow.getId());
//                formMDTO.setSubmitFlag("S");
//
//
//                Boolean response = formMService.addFormMFromFinacle(formMDTO);
//                if (response) {
//                    String uri = URI + "/api/formm/addsuccess";
//                    logger.info("The Form M Added On Finacle App Name {}", formMDTO.getAppName());
//                    template.postForObject(uri, formMDTO, String.class);
//                    logger.info("Success after response Form m");
//                } else {
//                    System.out.println("Form M not Saved");
//                }
//
//            }
//        } catch (CronJobException e) {
//            e.printStackTrace();
//        }
//        return null;
//
//    }


    @Transactional
    @Override
    public String getRemittanceIssuedOnFinacle() {

        List<RemittanceDetails> remittanceDetails = integrationService.getRemittanceIssuedOnFinacle();
        List<RemittanceDetails> fundTransfers = integrationService.getFundTransferIssuedOnFinacle();

        try{

            for (RemittanceDetails remittanceDetail : remittanceDetails) {
                logger.info(" Remittance added on finacle form number {} ", remittanceDetail.getFormNumber());
                logger.info(" Remittance added on finacle form a number {} ", remittanceDetail.getFormaNumber());


                if (null==remittanceDetail.getFormNumber() || null == remittanceDetail.getFormaNumber()) {
                    logger.info("Remittance number or form a number from Finacle Does Not Exist");
                    continue;
                }

                Remittance finremittance = modelMapper.map(remittanceDetail, Remittance.class);
                RemittanceDTO remittanceDTO = remittanceService.convertEntityToDTOFinacle(finremittance);
                logger.info(" Remittance added on finacle forma  utilized amt {} ", remittanceDetail.getFormaUtilizedAmt());

                FormA tmsformA = formARepo.findByFormNumber(remittanceDetail.getFormaNumber());

                Remittance tmsremittance = remittanceRepo.findByFormNumber(remittanceDTO.getFormNumber());
                if(null == tmsremittance && tmsformA !=null)  {
//                    tmsformA.setUtilization(remittanceDetail.getFormaUtilizedAmt());
//                    formARepo.save(tmsformA);
                    logger.info(" Remittance added on finacle forma  utilized amt {} ", tmsformA.getUtilization());
                    Boolean response = remittanceService.saveRemittanceFromFinacle(finremittance,tmsformA);
//                    Boolean corpResponse = formACorporateService.updateFormAUtilization(tmsformA, remittance);


                    if (response) {
                        String uri = URI + "/api/remittance/remittanceaddedonfinacle";
                        logger.info("The Remittance Added On Finacle First Name {}", remittanceDTO.getFirstName());
                        template.postForObject(uri, remittanceDTO, String.class);
                        logger.info("Success after response Remittance");
                    } else {
                        System.out.println("Remittance not Saved");
                    }
                } else {
                    System.out.println("This is Remittance already exist");
                }

            }
        }catch (Exception e){
            logger.info("Error saving remittance from FInacle {} ",e);
//            throw new CronJobException();
        }

        //Fund Transfer Added on Finacle
        try{
            for (RemittanceDetails fundTransfer : fundTransfers) {
                logger.info(" Fund Transfer added on finacle form number {} ", fundTransfer.getFormNumber());

                if (null==fundTransfer.getFormNumber()) {
                    logger.info("Fund Transfer number from Finacle Does Not Exist");
                    continue;
                }

                Remittance finFundTransfer = modelMapper.map(fundTransfer, Remittance.class);
                logger.info(" Fund Transfer added on finacle  utilized amt {} ", finFundTransfer.getTransferAmount());

                Remittance tmsFundTransfer= remittanceRepo.findByFormNumber(finFundTransfer.getFormNumber());
                if(null == tmsFundTransfer)  {
                   logger.info(" Fund Transfer added on finacle  utilized amt {} ", finFundTransfer.getTransferAmount());
                    Boolean response = remittanceService.saveFundTransferFromFinacle(finFundTransfer);
//                    Boolean corpResponse = formACorporateService.updateFormAUtilization(tmsformA, remittance);


                    if (response) {
                        String uri = URI + "/api/remittance/updatefundtransfer";
                        RemittanceDTO remittanceDTO = remittanceService.convertEntityToDTOFinacle(finFundTransfer);
                        template.postForObject(uri, remittanceDTO, String.class);
                        logger.info("Success after response Remittance");
                    } else {
                        System.out.println("Fund Transfer not Saved");
                    }
                } else {
                    System.out.println("This is Fund Transfer already exist");
                }

            }
        }catch (Exception e){
            logger.info("Error saving remittance from FInacle {} ",e);
//            throw new CronJobException();
        }

        return null;
    }

//    @Override
//    public String getRemittanceIssuedOnFinacle() {
//        List<RemittanceDetails> remittanceDetails = integrationService.getRemittanceIssuedOnFinacle();
//        try {
//            for (RemittanceDetails remittanceDetail : remittanceDetails) {
//                if (null== remittanceDetail.getFormNumber()){
//                    continue;
//                }
//                Remittance remitans = remittanceRepo.findByFormNumber(remittanceDetail.getFormNumber());
//                if (remitans == null) {
//
//                    Remittance remittance = modelMapper.map(remittanceDetail, Remittance.class);
////
//                    SettingDTO wfSetting = configurationService.getSettingByName("DEF_REMITTANCE_WF");
//                    WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//
//                    remittance.setStatus("A");
//                    remittance.setWorkFlow(workFlow);
//                    RemittanceDTO remittanceDTO = remittanceService.convertEntityToDTO(remittance);
//                    remittanceDTO.setWorkFlowId(workFlow.getId());
//                    remittanceDTO.setSubmitFlag("S");
//
//                    Boolean response = remittanceService.saveRemittanceFromFinacle(remittanceDTO);
//
//                    Remittance remittance2 = remittanceRepo.findByFormNumber(remittanceDTO.getFormNumber());
//                    RemittanceDTO remittanceDTO2 = remittanceService.convertEntityToDTO(remittance2);
//                    if (response) {
//                        String uri = URI + "/api/remittance/issuancesuccess";
//                        logger.info("The Remittance Added On Finacle First Name {}", remittanceDTO2.getFirstName());
//                        template.postForObject(uri, remittanceDTO2, String.class);
//                        logger.info("Success after response Remittance");
//                    } else {
//                        System.out.println("Remittance not Saved");
//                    }
//                } else {
//                    System.out.println("This is Remittance already exist");
//                }
//            }
//
//        } catch (CronJobException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    @Override
    public void getFormAResponse() {
        List<FormADetails> formADetails = integrationService.getFormASubmitResponse();
        try {
            for (FormADetails formADetail : formADetails) {
                String cifId = formADetail.getCifid();
                logger.info("The Form A Details Parametre {} ", formADetail.getFormNumber());
                FormA formA = modelMapper.map(formADetail, FormA.class);
                FormADTO formADTO = formACorporateService.convertEntityTODTOFinacle(formA);
                logger.info("The Form A Parametre {} ", formA.getFormNumber());
                RetailUser retailUser = retailUserRepo.findFirstByCustomerId(cifId);
                logger.info("The Form A Temp Form Parametre {} ", formA.getFormNumber());
                FormA temp = formARepo.findByTempFormNumber(formA.getTempFormNumber());
                logger.info("The Form A Temppppp {} ", temp);
                if (formA.getTempFormNumber() != null && !"".equals(formA.getTempFormNumber())) {
                    if (retailUser != null) {
                        Boolean response = formARetailService.updateFormAFromFinacle(temp, formA);
                        if (response) {
                            String uri = URI + "/api/forma/formnumresponse";
                            logger.info("This is Form A Submit Response First Name {}", formADTO.getFirstName());
                            template.postForObject(uri, formADTO, String.class);
                            logger.info("Success after response Form A");
                        } else {
                            System.out.println("Form A not Saved");
                        }
                    } else if (retailUser == null) {
                        Boolean corpResponse = formACorporateService.updateFormAFromFinacle(temp, formA);
                        if (corpResponse) {
                            String uri = URI + "/api/forma/formnumresponse";
                            logger.info("This is Form A Submit Response Corp First Name {}", formADTO.getFirstName());
                            template.postForObject(uri, formADTO, String.class);
                            logger.info("Success after response Form A");
                        } else {
                            System.out.println("Form A not Saved");
                        }
                    } else {
                        System.out.println("This user has not been profiled on the application");
                    }
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getFormNxpResponse() {
        List<FormNxpDetails> formNxpDetails = integrationService.getFormNxpSubmitResponse();
        try {
            for (FormNxpDetails formNxpDetail : formNxpDetails) {
                String cifId = formNxpDetail.getCifid();
                FormNxp formNxp = modelMapper.map(formNxpDetail, FormNxp.class);
//                RetailUser retailUser = retailUserRepo.findFirstByCustomerId(cifId);
                FormNxpDTO formNxpDTO = formNxpService.convertEntityTODTOFinacle(formNxp);
                logger.info("The tempForm Number is here {} ", formNxpDTO.getTempFormNumber());
                FormNxp temp = formNxpRepo.findByTempFormNumber(formNxpDTO.getTempFormNumber());
                if(temp==null){
                    continue;
                }
                logger.info("The Form Number is here {} ", formNxp.getFormNum());
                logger.info("This form NXP {} ", temp);
                Boolean response = formNxpService.updateFormNxpFromFinacle(temp, formNxp);
                if (response) {
                    String uri = URI + "/api/formnxp/formnumresponse";
                    logger.info("This is Form NXP Submit Response Exporter Name {}", formNxpDTO.getExporterName());
                    try{
                    template.postForObject(uri, formNxpDTO, String.class);
                    logger.info("Success after response form NXP");
                    } catch (Exception ex) {
                        logger.error("Exception occurred getting response for form NXP{}", ex);
                        ex.printStackTrace();
                    }
                } else {
                    System.out.println("Form Nxp FormNumber not Saved");
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getRemittanceResponse() {

        try {
            List<RemittanceDetails> remittanceDetails1 = integrationService.getRemittanceFormAIssueResponse();
            List<RemittanceDetails> remittanceDetails2 = integrationService.getRemittanceIssueResponse();
            List<RemittanceDetails> remittanceDetails = new ArrayList<>();
            if (remittanceDetails1 != null && remittanceDetails2 != null) {
                remittanceDetails.addAll(remittanceDetails1);
                remittanceDetails.addAll(remittanceDetails2);
            } else if (remittanceDetails1 != null && remittanceDetails2 == null) {
                remittanceDetails.addAll(remittanceDetails1);
            } else if (remittanceDetails1 == null && remittanceDetails2 != null) {
                remittanceDetails.addAll(remittanceDetails2);
            }
            for (RemittanceDetails remittanceDetail : remittanceDetails) {
//                String cifId = remittanceDetail.getCifid();
                String formaNumber = remittanceDetail.getFormaNumber();
                Remittance remittance = modelMapper.map(remittanceDetail, Remittance.class);
                RemittanceDTO remittanceDTO = remittanceService.convertEntityToDTOFinacle(remittance);
                Remittance temp = remittanceRepo.findByTempFormNumber(remittanceDTO.getTempFormNumber());

                logger.info("This is the temp number {} ", remittanceDTO.getTempFormNumber());
                logger.info("This is the corporate name {} ", remittanceDTO.getCorporateName());
                logger.info("This is the first name name {} ", remittanceDTO.getFirstName());
                logger.info("This is the last name name {} ", remittanceDTO.getLastName());
//                if(temp == null){
//                    logger.info("Remittance Temp form number does not exist");
//                    break;
//                }
//                RetailUser retailUser = retailUserRepo.findFirstByCustomerId(cifId);
                if (formaNumber != null) {
                    FormA formA = formARepo.findByFormNumber(formaNumber);
//                    FormADTO formADTO = formACorporateService.convertEntityTODTOFinacle(formA);
//                    if (retailUser != null) {
//                        Boolean response = formARetailService.updateFormAUtilization(formA, remittance);
//                    } else if (retailUser == null) {
//                    String uri = URI + "/api/forma/utilization";
//                    template.postForObject(uri, formaNumber, String.class);
                        Boolean corpResponse = formACorporateService.updateFormAUtilization(formA, remittance);
//                    }

                } else {
                    logger.info("Utilization cant be done on with out a form A {} ");
                }
                Boolean response = remittanceService.updateRemittanceFromFinacle(temp, remittance);
                logger.info("This is remittance response {} ", response);
                if (response) {
                    String uri = URI + "/api/remittance/issuancesuccess";
                    logger.info("The Main of response for Remittance {}", remittanceDTO);
                    try {
                        template.postForObject(uri, remittanceDTO, String.class);
                        logger.info("Success after response Remittance");
                    }   catch (Exception ex) {
                    ex.printStackTrace();
                    logger.error("Remittance error in LIST {}", ex);
                        logger.error("Remittance error in LIST {} specific", ex.getMessage());
                }
                } else {
                    System.out.println("Remittance not Saved");
                }

            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    @Override
    public List<String> submitOutwardGuaranteeToFinacle() throws CronJobException {
        try {
            HashMap<String, String> map = new HashMap<>();
            List<String> responses = new ArrayList<>();
            List<OutwardGuarantee> outwardGuarantees = outwardGuaranteeRepo.findByStatusAndSubmitFlag("A", "U");
            int size = outwardGuarantees.size();
            logger.info("This is the Number of Outward Guarantees Fetched from TMS {}", size);


            for(OutwardGuarantee outwardGuarantee:outwardGuarantees){
                logger.info("Outward Guarantee temp Form {}",outwardGuarantee.getTempFormNumber());
//                OutwardGuarantee outwardGuarantee = outwardGuarantees.get(b);
                List<CommentDTO> commentDTO = outwardGuaranteeService.getComments(outwardGuarantee);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                });
                logger.info("This is the banker's username {} ", map.get("userName"));
                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));
                OutwardGuaranteeDTO outwardGuaranteeDTO = outwardGuaranteeService.convertEntityToDTO(outwardGuarantee);
                outwardGuaranteeDTO.setSortCode(bankUser.getBranch().getSortCode());
                outwardGuaranteeDTO.setComments(map.get("comments"));
                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(outwardGuaranteeDTO.getTempFormNumber(), RequestType.OUTWARD_GUARANTEE);
                int auditSize = serviceAudits.size();
                logger.info("service audit size {}",auditSize);
                if (auditSize < 10) {
                    String uri = URI + "/api/guarantee/outward/submit";

                    try{
                    String guaranteeResponse = template.postForObject(uri, outwardGuaranteeDTO, String.class);
                    logger.info("The Main of Outward Guarantee Response  {}", guaranteeResponse);
                    responses.add(guaranteeResponse);
                    if (guaranteeResponse.equalsIgnoreCase("Y")) {
                        outwardGuarantee.setSubmitFlag("S");
                        outwardGuaranteeRepo.save(outwardGuarantee);
                    } else if (guaranteeResponse.equalsIgnoreCase("S")) {
                        outwardGuarantee.setStatus("PV");
                        outwardGuarantee.setSubmitFlag("S");
                        outwardGuaranteeRepo.save(outwardGuarantee);
                    } else {
                        logger.error("Error response for Outward Guarantee {} ", guaranteeResponse);
                    }
                    ServiceAudit serviceAudit = new ServiceAudit();
                    serviceAudit.setRequestType(RequestType.OUTWARD_GUARANTEE);
                    serviceAudit.setRequestDate(new Date());
                    logger.info("OutwardG temp form no{}",outwardGuaranteeDTO.getTempFormNumber());
                    serviceAudit.setTempNumber(outwardGuaranteeDTO.getTempFormNumber());
                    if (guaranteeResponse.equalsIgnoreCase("Y")) {
                        serviceAudit.setResponseFlag("Y");
                        serviceAudit.setResponseMessage("Successful");
                    } else {
                        serviceAudit.setResponseFlag("N");
                        serviceAudit.setResponseMessage(guaranteeResponse);
                    }
                    logger.info("The Main of Outward Guarantee Service Audit {}", guaranteeResponse);
                    serviceAuditRepo.save(serviceAudit);

                    } catch (Exception e) {
                        logger.error("Exception occurred submitting outward Guarantee {}", e);
                        e.printStackTrace();
                    }

                } else {
                    outwardGuarantee.setSubmitFlag("F");
                    outwardGuaranteeRepo.save(outwardGuarantee);
                    Boolean outGuarMail = outwardGuaranteeService.sendOutwardGuaranteeITSmail(outwardGuarantee);
                    logger.info("Message After Outward Guarantee Sending Mail");
                }
            }
            return responses;
        } catch (Exception e) {
            logger.error("Exception occurred submitting Outward Guarantee {}", e.getMessage());
            throw new CronJobException();
        }
    }

    @Override
    public void getOutwardGuaranteeAddResponse() {

        List<GuaranteeDetails> guaranteeDetails = integrationService.getOutwardGuaranteeSubmitResponse();
        try {
            for (GuaranteeDetails guaranteeOutwardDetail : guaranteeDetails) {
                OutwardGuarantee outwardGuarantee = modelMapper.map(guaranteeOutwardDetail, OutwardGuarantee.class);
                OutwardGuaranteeDTO outwardGuaranteeDTO = outwardGuaranteeService.convertEntityToDTO(outwardGuarantee);
                logger.info("The Outward Guaranteen tempForm Number is {} ", outwardGuarantee.getTempFormNumber());
                OutwardGuarantee tmsOutGuarantee = outwardGuaranteeRepo.findByTempFormNumber(outwardGuaranteeDTO.getTempFormNumber());
                logger.info("The Guarantee Number is {} ", outwardGuarantee.getGuaranteeNumber());
                logger.info("This Outward Guarantee {} ", tmsOutGuarantee);
                Boolean response = outwardGuaranteeService.updateOutwardGuaranteeFromFinacle(tmsOutGuarantee, outwardGuarantee);
                if (response) {
                    String uri = URI + "/api/guarantee/outward/guaranteenumresponse";
                    try {
                        template.postForObject(uri, outwardGuaranteeDTO, String.class);
                        logger.info("Success after response OUT Guarantee");
                    } catch (Exception ex) {
                        logger.error("Exception occurred getting response for OutwardG {}", ex);
                        ex.printStackTrace();
                    }
                }
                else {
                    System.out.println("OutwardGuarantee Number not Saved");
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getInwardGuaranteeAddedOnFinacleByCifid() {
        try {
            List<Corporate> corporateList = corporateRepo.findAll();
            for (Corporate corporate : corporateList) {
                List<GuaranteeDetails> guaranteeDetailsList = integrationService.getInwardGuaranteeAddByCifId(corporate.getCustomerId());
                for (GuaranteeDetails guaranteeDetails : guaranteeDetailsList) {
                    InwardGuaranteeDTO inwardGuaranteeDTO = modelMapper.map(guaranteeDetails, InwardGuaranteeDTO.class);
                    Boolean response = inwardGuaranteeService.addInwardGuaranteeFromFinacle(inwardGuaranteeDTO);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getInwardGuaranteeAddedOnFinacle() {
        List<GuaranteeDetails> guaranteeDetailsList = integrationService.getInwardGuaranteeAddedOnFinacle();
        try {
            for (GuaranteeDetails guaranteeDetail : guaranteeDetailsList) {
                logger.info("guarantee number {}",guaranteeDetail.getGuaranteeNumber());
                logger.info("Inward Guarantee Number from finacle {} ",guaranteeDetail.getGuaranteeNumber());

                InwardGuarantee inwardGuar = inwardGuaranteeRepo.findByGuaranteeNumber(guaranteeDetail.getGuaranteeNumber());
                logger.info("inwardG {}",inwardGuar);

                if (inwardGuar == null) {


                    InwardGuarantee inwardGuarantee = modelMapper.map(guaranteeDetail, InwardGuarantee.class);
                    logger.info("detail of guar {}",inwardGuarantee.getDetailsOfGuarantee());
                    SettingDTO wfSetting = configurationService.getSettingByName("DEF_INGUAR_WF");
                    WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
                    String number = FormNumber.getInwGuranteeNumner();
                    inwardGuarantee.setTempFormNumber(number);
                    inwardGuarantee.setStatus("A");
                    inwardGuarantee.setWorkFlow(workFlow);
                    inwardGuarantee.setInitiatedBy("FINACLE");
                    inwardGuarantee.setSubmitFlag("S");
                    inwardGuaranteeRepo.save(inwardGuarantee);

                    InwardGuaranteeDTO inwardGuaranteeDTO2 = inwardGuaranteeService.convertEntityToDTO(inwardGuarantee);

                        String uri = URI + "/api/guarantee/inward/addresponse";
                        logger.info("The response for Inward Guarantee Added On Finacle {}", inwardGuaranteeDTO2.getAppName());

                        try{
                        template.postForObject(uri, inwardGuaranteeDTO2, String.class);
                        logger.info("Success after response Inward Guarantee");

                    } catch (Exception ex) {
                        logger.error("Exception occurred getting inward G from finacle{}", ex);
                        ex.printStackTrace();
                    }

                } else {
                    System.out.println("Inward Guarantee already exist");
                }

            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getOutwardGuaranteeAddedOnFinacle() {
        List<GuaranteeDetails> guaranteeDetailsList = integrationService.getOutwardGuaranteeAddedOnFinacle();
        try {
            for (GuaranteeDetails guaranteeDetail : guaranteeDetailsList) {
                logger.info(" outward guarantee number {}",guaranteeDetail.getGuaranteeNumber());
                logger.info("outward Guarantee Number from finacle {} ",guaranteeDetail.getCurrency());
                if (guaranteeDetail.getGuaranteeNumber()==null){
                    continue;
                }

                OutwardGuarantee outwardGuar = outwardGuaranteeRepo.findByGuaranteeNumber(guaranteeDetail.getGuaranteeNumber());


                if (outwardGuar == null) {


                    OutwardGuarantee outwardGuarantee = modelMapper.map(guaranteeDetail, OutwardGuarantee.class);
                    logger.info("detail of outward G added on finacle {}",outwardGuarantee.getDetailsOfGuarantee());
                    SettingDTO wfSetting = configurationService.getSettingByName("DEF_OUTGUAR_WF");
                    WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
                    String number = FormNumber.getOutwGuranteeNumner();
                    outwardGuarantee.setTempFormNumber(number);
                    outwardGuarantee.setStatus("A");
                    outwardGuarantee.setWorkFlow(workFlow);
                    outwardGuarantee.setInitiatedBy("FINACLE");
                    outwardGuarantee.setSubmitFlag("S");
                    outwardGuarantee.setGuidelineApproval(true);
                    outwardGuarantee.setCreatedOn(new Date());


                    outwardGuaranteeRepo.save(outwardGuarantee);

                    OutwardGuaranteeDTO outwardGuaranteeDTO = outwardGuaranteeService.convertEntityToDTO(outwardGuarantee);

                    String uri = URI + "/api/guarantee/outward/addresponse";

                    try{
                        template.postForObject(uri, outwardGuaranteeDTO, String.class);

                    } catch (Exception ex) {
                        logger.error("Exception occurred getting outward G added on  finacle {}", ex);
                        ex.printStackTrace();
                    }

                } else {
                    System.out.println("Outward Guarantee added on finacle already exist");
                }

            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void getFormMAllocated() {
        List<FormMDetails> formMDetails = integrationService.getFormMAllocated();
        try {
            for (FormMDetails formMDetail : formMDetails) {
                logger.info("The Form M Details Allocation Parametre {} ", formMDetail.getTempFormNumber());
                FormM formM = modelMapper.map(formMDetail, FormM.class);
                logger.trace("The Form M Allocation Parametre {} ", formM.getAllocatedAmount());
                FormM existingFormM = formMRepo.findByFormNumber(formM.getFormNumber());
                logger.trace("The Form M Temp {} ", existingFormM);
                if (existingFormM != null) {
                    if(formMDetail.getAllocatedAmount() != null) {
                        formM.setAllocatedAmount(formMDetail.getAllocatedAmount());
                    }
                    formM.setStatus("L");
                    boolean successfullySave = false;
                    try {
                        formMRepo.save(formM);
                        successfullySave = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(successfullySave) {
                        FormMDTO formMDTO = formMService.convertEntityTODTO(existingFormM);
                        Boolean response = formMService.updateFormMAllocation(existingFormM, formM);
                        if (response) {
                            String uri = URI + "/api/formm/allocationresponse";
                            try {
                                template.postForObject(uri, formMDTO, String.class);
                                logger.info("Success after response Form M Allocation");
                            } catch (Exception ex) {
                                logger.error("Exception occurred submitting form A{}", ex);
                            }
                        } else {
                            System.out.println("Form M Allocation not Saved");
                        }
                    }
//
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    @Override
    public List<String> submitImportLcToFinacle() throws CronJobException {
        try {
            HashMap<String, String> map = new HashMap<>();
            List<String> responses = new ArrayList<>();
            List<ImportLc> importLcs = importLcRepo.findByStatusAndSubmitFlag("A", "U");

            logger.info("This is the Number of Import LCs Fetched from TMS {}", importLcs.size());

            //for (int b = 0; b <size; b++) {
            for (ImportLc importLc : importLcs) {

                logger.info("Import LC TempNO is: {}", importLc.getTempLcNumber());
                logger.info("Import Lc Charge Acct is: {}", importLc.getChargeAcct());
//                logger.info("Import Lc Charge Acct is: {}", importLc.getChargeAccount().getAccountNumber());

                logger.info("Import Lc Type is: {}", importLc.getLcType().getCode());
                logger.info("Transfer Acctn {} ",importLc.getOperativeAccount());
                logger.info("import lc  applicant name {} ",importLc.getApplicantName());



//                String chargeAcc = importLc.getChargeAccount().getAccountNumber();
//                String chargeAcc = importLc.getChargeAcct();
//                String transferAcc = importLc.getOperativeAccount();
                List<CommentDTO> commentDTO = lcService.getImportLcComments(importLc);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                });

                logger.info("This is the banker's username {} ", map.get("userName"));
                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));

                ImportLcDTO importLcDTO = lcService.convertEntityToImportLcDTO(importLc);
//                importLcDTO.setSortCode(bankUser.getBranch().getSortCode());
                importLcDTO.setComments(map.get("comments"));
                logger.info("form m no for lc {} ", importLc.getFormM().getFormNumber());
                logger.info("form m linking flag  for lc {} ", importLc.getToBeLinked());



                if (null!= importLc.getFormM().getFormNumber() && null!= importLc.getToBeLinked()){

                    FormM formMLinking = formMRepo.findByFormNumber(importLc.getFormM().getFormNumber());
                    if (null!=formMLinking){
                        if(importLc.getToBeLinked().equalsIgnoreCase("Y")){
                        importLcDTO.setFormmChargeAccount(formMLinking.getChargeAccount());
                        importLcDTO.setFormmPrefex(formMLinking.getPrefex());
                        importLcDTO.setFormmTransactionAccount(formMLinking.getTransactionAccount());
                        importLcDTO.setFormmPurposeOfForm(formMLinking.getPurposeOfForm());

                        importLcDTO.setRcNumber(formMLinking.getRcNumber());
                    }
                        importLcDTO.setLinkingStatus(formMLinking.getLinkingStatus());
                    }
                }

                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(importLcDTO.getTempLcNumber(), RequestType.IMPORT_LC);
                int auditSize = serviceAudits.size();
                logger.info("service audit size Import LC{}",auditSize);
                if (auditSize < 10) {
                    String uri = URI + "/api/loc/import/issue";
                    logger.info("The Applicants Name of import lc {}", importLcDTO.getApplicantName());
                    logger.info("Import LC Offshore Rate {} ", importLcDTO.getOffShoreRate());

                    try {
                        String importLcResponse = template.postForObject(uri, importLcDTO, String.class);
                        logger.info("The Main of import lc Response  {}", importLcResponse);
                        responses.add(importLcResponse);
                        if (importLcResponse.equalsIgnoreCase("Y")) {
                            importLc.setSubmitFlag("S");
                            importLcRepo.save(importLc);
                        } else if (importLcResponse.equalsIgnoreCase("S")) {
                            importLc.setStatus("PV");
                            importLc.setSubmitFlag("S");
                            importLcRepo.save(importLc);
                        } else {
                            logger.error("Error response for import lc {} ", importLcResponse);
                        }
                        ServiceAudit serviceAudit = new ServiceAudit();
                        serviceAudit.setRequestType(RequestType.IMPORT_LC);
                        serviceAudit.setRequestDate(new Date());
                        serviceAudit.setTempNumber(importLcDTO.getTempLcNumber());
                        if (importLcResponse.equalsIgnoreCase("Y")) {
                            serviceAudit.setResponseFlag("Y");
                            serviceAudit.setResponseMessage("Successful");
                        } else {
                            serviceAudit.setResponseFlag("N");
                            serviceAudit.setResponseMessage(importLcResponse);
                        }
                        serviceAuditRepo.save(serviceAudit);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        logger.error("import LC error in LIST ", ex);
                        logger.error("import LC specific error in LIST {}", ex.getMessage());
                    }
                } else {

                    importLc.setSubmitFlag("F");
                    importLcRepo.save(importLc);

                    Boolean importLcMail = lcService.sendImportLcITSmail(importLc);


                    logger.info("Message After Import LC Sending Mail");
                }


            }//end for loop
            return responses;
        } catch (Exception e) {
            logger.error("Exception occurred submitting import lc ", e);
            throw new CronJobException();
        }
    }

    @Override
    public void getImportLcIssueResponse() {

        List<LCDetails> lcDetails = integrationService.getImportLcSubmitResponse();
        logger.info("Get importLC Response {}",lcDetails.size());
//        try {
//            logger.info("Get Import LC Response");
//            for (LCDetails lcDetail : lcDetails) {
//                logger.info("utilized amt from finacle {} ", lcDetail.getUtilizedAmount());
//                logger.info("Import lc number {} ", lcDetail.getLcNumber());
//
//                if(null == lcDetail.getLcNumber() ||null==lcDetail.getUtilizedAmount() ){
//                    logger.info("Import Lc Utilized Amount not gotten from finacle");
//                    continue;
//                }
//
//                ImportLc finimportLc = modelMapper.map(lcDetail, ImportLc.class);
//                ImportLcDTO importLcDTO = lcService.convertEntityToImportLcDTO(finimportLc);
//
//                ImportLc tmsimportlc = importLcRepo.findByTempLcNumber(finimportLc.getTempLcNumber());
//                logger.info("The ImportLc Number is {} ", finimportLc.getLcNumber());
//                logger.info("This fin ImportLc utilized amt {} ", finimportLc.getUtilizedAmount());
//
//
//
//                Boolean response = lcService.updateImportFromFinacle(tmsimportlc, finimportLc);
//                if (response) {
//                    String uri = URI + "/api/loc/import/issueresponseflag";
//                    template.postForObject(uri, importLcDTO, String.class);
//                    logger.info("Success after response OUT ImportLc");
//                } else {
//                    System.out.println("ImportLc Number not Saved");
//                }
//            }
//        } catch (CronJobException e) {
//            e.printStackTrace();
//        }

        try {

            for (LCDetails lcDetail : lcDetails) {
                ImportLc importLc = modelMapper.map(lcDetail, ImportLc.class);
                ImportLcDTO importLcDTO = lcService.convertEntityToImportLcDTO(importLc);
                logger.info("The Import LC  update temp Number is {} ", importLc.getTempLcNumber());

                if (importLcDTO.getTempLcNumber() != null && !"".equals(importLcDTO.getTempLcNumber())) {
                    ImportLc tmsimportlc = importLcRepo.findByTempLcNumber(importLc.getTempLcNumber());
                    logger.info("The import lc  Number  issued  is {} ", importLc.getLcNumber());

                    if (null == tmsimportlc) {
                        logger.info("Import Lc temp no does not exist on TMS");
                        continue;
                    }


                    Boolean response = lcService.updateImportFromFinacle(tmsimportlc, importLc);
                    if (response) {
                        String uri = URI + "/api/loc/import/issueresponseflag";
                        template.postForObject(uri, importLcDTO, String.class);
                    logger.info("Success after response OUT ImportLc");
                    } else {
                        System.out.println("Import LC Number not Saved");
                    }
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String getImportLcAddedOnFinacle() {
        List<LCDetails> lcDetailsList = integrationService.getImportLcAddedOnFinacle();
        try {
            for (LCDetails lcDetails : lcDetailsList) {
                logger.info("import lc added on finacle cifid {}", lcDetails.getCifid());
                logger.info("import lc added on finacle form m no {}", lcDetails.getFormMNumber());

                if (null == lcDetails.getLcNumber() || null == lcDetails.getFormMNumber()) {
                    continue;
                }

                ImportLc finimportLc = modelMapper.map(lcDetails, ImportLc.class);
                ImportLc tmsimportlc = importLcRepo.findByLcNumber(lcDetails.getLcNumber());
                logger.info("import lc number  added on finacle  {} ", tmsimportlc.getLcNumber());
                FormM formM = formMRepo.findByFormNumber(lcDetails.getFormMNumber());

                if (tmsimportlc == null) {

                    Boolean response = lcService.saveImportLcAddedOnFinacle(finimportLc, formM);

                    ImportLcDTO importLcDTO = lcService.convertEntityToImportLcDTO(finimportLc);

                    if (response) {
                        String uri = URI + "/api/loc/import/addresponse";
                        try {
                            template.postForObject(uri, importLcDTO, String.class);
                            logger.info("Success after response Import lC added on finacle");

                        } catch (Exception ex) {
                            logger.error("Exception occurred inserting import lc dded on finacle on TMS{}", ex);
                            ex.printStackTrace();
                        }

                    } else {
                        System.out.println("Import Lc  already exist");
                    }

                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
        throw new CronJobException();
    }


    @Transactional
    @Override
    public List<String> submitExportLcToFinacle() throws CronJobException {
        try {
            HashMap<String, String> map = new HashMap<>();
            List<String> responses = new ArrayList<>();
            List<ExportLc> exportLcs = exportLcRepo.findByStatusAndSubmitFlag("A", "U");
            int size = exportLcs.size();
            logger.info("This is the Number of Export LCs Fetched from TMS {}", size);

                for (ExportLc exportLc : exportLcs){
                    logger.info("Export LC NO{}",exportLc.getLcNumber());
                    logger.info("Export LC Charge Acc{}",exportLc.getChargeAcct());
                    logger.info("Export LC transfer Acc{}",exportLc.getOperativeAccount());
//                    logger.info("Export LC transfer Acc{}",exportLc.getTransAccount().getAccountNumber());
//                String chargeAcc = exportLc.getChargeAccount().getAccountNumber();
//                String transferAcc = exportLc.getTransAccount().getAccountNumber();
//                logger.info("FormNxp formNum for Export LC{}",exportLc.getFormNxp().getFormNum());
                String formNxp = exportLc.getFormNxp().getFormNum();
                List<CommentDTO> commentDTO = lcService.getExportLcComments(exportLc);
                commentDTO.forEach(commentDtd -> {
                    commentDtd.getUsername();
                    commentDtd.getComment();
                    map.put("userName", commentDtd.getUsername());
                    map.put("comment", commentDtd.getComment());
                });
                logger.info("This is the banker's username {} ", map.get("userName"));
                BankUser bankUser = bankUserService.getUserByName(map.get("userName"));

                ExportLcDTO exportLcDTO = lcService.convertEntityToExportLcDTO(exportLc);

                exportLcDTO.setSortCode(bankUser.getBranch().getSortCode());
                exportLcDTO.setComments(map.get("comments"));
//                exportLcDTO.setChargeAccount(chargeAcc);
//                exportLcDTO.setTransAccount(transferAcc);
                exportLcDTO.setFormNxpNumber(formNxp);
                List<ServiceAudit> serviceAudits = serviceAuditRepo.findByTempNumberAndRequestType(exportLcDTO.getTempLcNumber(), RequestType.EXPORT_LC);
                int auditSize = serviceAudits.size();
                logger.info("service audit size for export lc {}",auditSize);

                    if(auditSize >=10) {
                        exportLc.setSubmitFlag("F");
                        exportLcRepo.save(exportLc);
                        Boolean exportLcMail = lcService.sendExportLcITSmail(exportLc);

                        logger.info("UPDATED EXPORT LC SUBMIT FLAG WITH F DUE TO FAILURE");
                    }

                if (auditSize < 10) {

                        String uri = URI + "/api/loc/export/advise";
                        logger.info("The Applicant's Name of export lc {}", exportLcDTO.getApplicantName());
                        try{
                        String exportLcResponse = template.postForObject(uri, exportLcDTO, String.class);
                        logger.info("The Main of export lc Response  {}", exportLcResponse);
                        responses.add(exportLcResponse);

                            ServiceAudit serviceAudit = new ServiceAudit();
                            serviceAudit.setRequestType(RequestType.EXPORT_LC);
                            serviceAudit.setRequestDate(new Date());
                            serviceAudit.setTempNumber(exportLcDTO.getTempLcNumber());

                        if (exportLcResponse.equalsIgnoreCase("Y")) {
                            exportLc.setSubmitFlag("S");
                            serviceAudit.setResponseFlag("Y");
                            serviceAudit.setResponseMessage("Successful");
                            serviceAuditRepo.save(serviceAudit);
                            exportLcRepo.save(exportLc);

                        } else if (exportLcResponse.equalsIgnoreCase("S") || exportLcResponse.equalsIgnoreCase("Pending Verification on Finacle")) {
                            logger.info("Export Lc pending verification block");
                            exportLc.setStatus("PV");
                            exportLc.setSubmitFlag("S");
                            exportLcRepo.save(exportLc);
                            serviceAudit.setResponseMessage(exportLcResponse);
                            serviceAudit.setResponseFlag("Y");
                            serviceAuditRepo.save(serviceAudit);
                            cronJobService.getExportLcPendingResponse();
                        } else {
                            serviceAudit.setResponseFlag("N");
                            serviceAudit.setResponseMessage(exportLcResponse);
                            serviceAuditRepo.save(serviceAudit);
                            logger.error("Error response for export lc {} ", exportLcResponse);
                        }


//                        if (exportLcResponse.equalsIgnoreCase("Y")) {
//                            serviceAudit.setResponseFlag("Y");
//                            serviceAudit.setResponseMessage("Successful");
//                        } else {
//
//                        }

                    } catch (Exception e) {
                        logger.error("Exception occurred submitting export lc {}", e);
                        responses.add("Failure");
                    }
                }
            }
            return responses;
        } catch (Exception e) {
            logger.error("Exception occurred submitting export lc {}", e);
            throw new CronJobException();
        }
    }



    @Override
    public void getExportLcAdviseResponse() {

        List<LCDetails> lcDetails = integrationService.getExportLcSubmitResponse();
        try {

            for (LCDetails lcDetail : lcDetails) {
                ExportLc exportLc = modelMapper.map(lcDetail, ExportLc.class);
                ExportLcDTO exportLcDTO = lcService.convertEntityToExportLcDTO(exportLc);
                logger.info("The Export LC  update temp Number is {} ", exportLc.getTempLcNumber());
                if (exportLcDTO.getTempLcNumber() != null && !"".equals(exportLcDTO.getTempLcNumber())) {
                    ExportLc tmsexportlc = exportLcRepo.findByTempLcNumber(exportLc.getTempLcNumber());
                    logger.info("The ExportLc Number  advised is {} ", exportLc.getLcNumber());
                    logger.info("This ExportLc {} ", tmsexportlc);

                    if (null == tmsexportlc) {
                        logger.info("Export Lc temp no does not exist on TMS");
                        continue;
                    }


                    Boolean response = lcService.changeExportLcToAdvisedFromFinacle(tmsexportlc, exportLc);
                    if (response) {
                        String uri = URI + "/api/loc/export/adviseresponseflag";
                        template.postForObject(uri, exportLcDTO, String.class);
                        logger.info("Export LC Pending Verification");

                        logger.info("Export LC  advise was successful");
                    } else {
                        System.out.println("ExportLc Number not Saved");
                    }
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }
    }




    @Override
    public  void getExportLcPendingResponse(){

        List<LCDetails> lcDetails = integrationService.getExportLcPendingResponse();
        try {
            for (LCDetails lcDetail : lcDetails) {
                ExportLc exportLc = modelMapper.map(lcDetail, ExportLc.class);
                ExportLcDTO exportLcDTO = lcService.convertEntityToExportLcDTO(exportLc);
                logger.info("The Export LC Pending Verification temp Number is {} ", exportLc.getTempLcNumber());
                ExportLc tmsexportlc = exportLcRepo.findByTempLcNumber(exportLc.getTempLcNumber());
                logger.info("The ExportLc Number Pending Verification is {} ", exportLc.getLcNumber());
                logger.info("This ExportLc {} ", tmsexportlc);
                Boolean response = lcService.updateExportPendingFromFinacle(tmsexportlc, exportLc);
                if (response) {
                    String uri = URI + "/api/loc/export/advisependingresponseflag";
                    template.postForObject(uri, exportLcDTO, String.class);
                    logger.info("Export LC Pending Verification");
                } else {
                    System.out.println("ExportLc Number not Saved");
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }

    }


    @Override
    public  void UpdateExportLcPendingResponse(){

        List<LCDetails> lcDetails = integrationService.updateExportLcPendingVerificationToAdvised();
        try {
            for (LCDetails lcDetail : lcDetails) {
                ExportLc exportLc = modelMapper.map(lcDetail, ExportLc.class);
                ExportLcDTO exportLcDTO = lcService.convertEntityToExportLcDTO(exportLc);
                logger.info("The Export LC Pending Verification update temp Number is {} ", exportLc.getTempLcNumber());
                if (exportLcDTO.getTempLcNumber() != null && !"".equals(exportLcDTO.getTempLcNumber())) {
                    ExportLc tmsexportlc = exportLcRepo.findByTempLcNumber(exportLc.getTempLcNumber());
                    logger.info("The ExportLc Number Pending Verification update to advised is {} ", exportLc.getLcNumber());
                    logger.info("This ExportLc {} ", tmsexportlc);

                    if (null == tmsexportlc) {
                        logger.info("Export Lc temp no does not exist on TMS");
                        continue;
                    }


                    Boolean response = lcService.changeExportLcToAdvisedFromFinacle(tmsexportlc, exportLc);
                    if (response) {
                        String uri = URI + "/api/loc/export/changePendingtoadvisedflag";
                        template.postForObject(uri, exportLcDTO, String.class);
                        logger.info("Export LC Pending Verification");

                        logger.info("Export LC Pending Verification update to advise Lc was successful");
                    } else {
                        System.out.println("ExportLc Number not Saved");
                    }
                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getExportLcAddedOnFinacle() {
        List<LCDetails> lcDetailsList = integrationService.getExportLcAddedOnFinacle();
        try {
            for (LCDetails lcDetails : lcDetailsList) {
                logger.info("Export lc added on finacle cifid {}", lcDetails.getCifid());
                if (null == lcDetails.getLcNumber() || null == lcDetails.getFormNxpNumber()) {
                    continue;
                }
                ExportLc finExportLc = modelMapper.map(lcDetails, ExportLc.class);
                ExportLc tmsexportlc = exportLcRepo.findByLcNumber(lcDetails.getLcNumber());
                FormNxp formNxp = formNxpRepo.findByFormNum(lcDetails.getFormNxpNumber());

                if (tmsexportlc == null) {

                    Boolean response = lcService.saveExportLcAddedOnFinacle(finExportLc, formNxp);

                    ExportLcDTO exportLcDTO = lcService.convertEntityToExportLcDTO(finExportLc);

                    if (response) {
                        String uri = URI + "/api/loc/export/adviseaddflag";
                        try {
                            template.postForObject(uri, exportLcDTO, String.class);
                            logger.info("Success after response export lC added on finacle");

                        } catch (Exception ex) {
                            logger.error("Exception occurred inserting export lc dded on finacle on TMS{}", ex);
                            ex.printStackTrace();
                        }

                    }

                    else {
                        System.out.println("Export Lc  already exist");
                    }

                }
            }
        } catch (CronJobException e) {
            e.printStackTrace();
            logger.info("error occurred saving export lc added on finacle in TMS {} ",e);
        }
        return null;
    }

//    @Override
//    public String getExportLcAddedOnFinacle() {
//        List<LCDetails> lcDetailsList = integrationService.getExportLcAddedOnFinacle();
//        try {
//            for (LCDetails lcDetail : lcDetailsList) {
//                ExportLc exportLlc = exportLcRepo.findByLcNumber(lcDetail.getLcNumber());
//                if (exportLlc == null) {
//                    ExportLc exportLc = modelMapper.map(lcDetail, ExportLc.class);
//                    SettingDTO wfSetting = configurationService.getSettingByName("DEF_INGUAR_WF");
//                    WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//                    exportLc.setStatus("A");
//                    exportLc.setWorkFlow(workFlow);
//                    ExportLcDTO exportLcDTO = lcService.convertEntityToExportLcDTO(exportLc);
//                    exportLcDTO.setWorkFlowId(workFlow.getId());
//                    exportLcDTO.setSubmitFlag("S");
//        Boolean response = lcService.saveExportLCFinacle(exportLcDTO);
//                    ExportLc exportLc2 = exportLcRepo.findByLcNumber(lcDetail.getLcNumber());
//                    logger.info("This is it by lc number {} ", exportLc2);
//                    ExportLcDTO exportLcDTO2 = lcService.convertEntityToExportLcDTO(exportLc2);
//                    if (response) {
//                        String uri = URI + "/api/loc/export/adviseaddflag";
//                        logger.info("This is the Name For Export LC Added On Finacle {}", exportLcDTO2.getApplicantName());
//                        template.postForObject(uri, exportLcDTO2, String.class);
//                        logger.info("Success after response lc export");
//                    } else {
//                        System.out.println("Export LC not Saved");
//                    }
//                } else {
//                    System.out.println("The Export Lc already exist");
//                }
//            }
//        } catch (CronJobException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }



    //*******************************************//

    @Override
    public boolean updateAccountDetials() throws CronJobException {
        List<Account> allAccounts = accountRepo.findAll();
        if (allAccounts.size() > 0) {
            for (Account account : allAccounts) {
                AccountDetails accountDetails = integrationService.getAccountDetails(account.getAccountNumber());
                try {
//                updateAllAccountName(account,accountDetails);
                    updateAllAccountCurrency(account, accountDetails);
                    updateAccountStatus(account, accountDetails);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }

    @Override
    public boolean updateRetailUserDetails() throws CronJobException {
        List<RetailUser> retailUsers = retailUserRepo.findAll();
        for (RetailUser retailUser : retailUsers) {
            try {
                logger.info("customer id is {}",retailUser.getCustomerId());
                CustomerDetails details = integrationService.getCustomerDetailsByCif(retailUser.getCustomerId());
                String bvn = integrationService.getCustomerBvnByCif(retailUser.getCustomerId());
                if(bvn != null) {
                    updateRetailUserBVN(retailUser, bvn);
                }
                if(details != null) {
                    updateRetailUserPhoneNo(retailUser, details);
                    updateRetailUserEmail(retailUser, details);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
//        viewCustomerDetailsByCif();
        return false;
    }

    @Override
    public void updateRetailUserBVN(RetailUser retailUser, String bvn) throws CronJobException {
        String userBvn = retailUser.getBvn();
        if (((userBvn == null) || userBvn.equalsIgnoreCase("")|| userBvn.equalsIgnoreCase("No BVN")) && (bvn !=null)) {
            retailUser.setBvn(bvn);
            logger.info("new bvn is {}",bvn);
            retailUserRepo.save(retailUser);
        }
    }

    @Override
    public void updateRetailUserPhoneNo(RetailUser retailUser, CustomerDetails details) throws CronJobException {
        String retailUserPhoneNumber = retailUser.getPhoneNumber();
//        logger.info("new phone number is {}",retailUser.getPhoneNumber());
        if ((retailUserPhoneNumber == null) || retailUserPhoneNumber.equalsIgnoreCase("") || (!retailUserPhoneNumber.equalsIgnoreCase(details.getPhoneNumber()))) {
            retailUser.setPhoneNumber(details.getPhoneNumber());
//            logger.info("new phone number 2 is {}",details.getPhone());

            retailUserRepo.save(retailUser);
        }
    }

    @Override
    public void updateRetailUserEmail(RetailUser retailUser, CustomerDetails details) throws CronJobException {
        String retailUserEmail = retailUser.getEmail();
//        logger.info("new email is {}",retailUser.getEmail());
        if ((retailUserEmail == null) || retailUserEmail.equalsIgnoreCase("") || (!retailUserEmail.equalsIgnoreCase(details.getEmail()))) {
            retailUser.setEmail(details.getEmail());
//            logger.info("new email 2 is {}",details.getEmail());
            retailUserRepo.save(retailUser);
        }
    }

    @Override
    public boolean updateCorporateUserDetails() throws CronJobException {
        List<CorporateUser> corporateUsers = corporateUserRepo.findAll();
        for (CorporateUser corporateUser : corporateUsers) {
            try {
                CustomerDetails details = integrationService.getCustomerDetailsByCif(corporateUser.getCorporate().getCustomerId());
                logger.info("CORPORATE USER {}", corporateUser.toString());
                //updateCorporateUserEmail(corporateUser,details);
                //updateCorporateUserPhoneNo(corporateUser,details);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return false;
    }
    public boolean updateCorporateDetails() throws CronJobException {
        List<Corporate> corporates = corporateRepo.findAll();
        for (Corporate corporate : corporates) {
            try {
                CustomerDetails details = integrationService.getCustomerDetailsByCif(corporate.getCustomerId());
                if(details != null) {
                    corporate.setEmail(details.getEmail());
                    corporate.setPhone(details.getPhoneNumber());
                    corporate.setAddress(details.getAddress());

                    logger.info("new corp email is {}", details.getEmail());
                }
                String bvn = integrationService.getCustomerBvnByCif(corporate.getCustomerId());
                if (((null == corporate.getBvn()) || corporate.getBvn().equalsIgnoreCase("")|| corporate.getBvn().equalsIgnoreCase("No BVN")) && (bvn !=null)) {
                    logger.info("new corp bvn is {}", bvn);
                    corporate.setBvn(bvn);
                }
                corporateRepo.save(corporate);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return false;
    }

    @Override
    public void updateCorporateUserBVN(Corporate corporate) throws CronJobException {
        CustomerDetails details = integrationService.getCustomerDetailsByCif(corporate.getCustomerId());
        String corporateBvn = corporate.getBvn();
        if ((corporateBvn == null) || corporateBvn.equalsIgnoreCase("") || (!corporateBvn.equalsIgnoreCase(details.getBvn()))) {
            corporate.setBvn(details.getBvn());

            logger.info("new corp bvn is {}", corporate.getBvn());
            try {
                corporateRepo.save(corporate);
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateCorporateUserPhoneNo(CorporateUser corporateUser, CustomerDetails details) throws CronJobException {
        String corporateUserPhoneNumber = corporateUser.getPhoneNumber();
        if ((corporateUserPhoneNumber == null) || corporateUserPhoneNumber.equalsIgnoreCase("") || (!details.getPhoneNumber().equalsIgnoreCase(corporateUserPhoneNumber))) {
            corporateUser.setPhoneNumber(details.getPhoneNumber());
            logger.info("new corp phone is {}", details.getPhoneNumber());
            try {
                corporateUserRepo.save(corporateUser);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateCorporateUserEmail(CorporateUser corporateUser, CustomerDetails details) throws CronJobException {
        String corporateUserEmail = corporateUser.getEmail();
        if ((corporateUserEmail == null) || corporateUserEmail.equalsIgnoreCase("") || (!corporateUserEmail.equalsIgnoreCase(details.getEmail()))) {
            corporateUser.setEmail(details.getEmail());
//            logger.info("new corp email is {}",details.getEmail());
            try {
                corporateUserRepo.save(corporateUser);
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    }


    @Override
    public void saveRunningJob(String jobCategory, String cronExpression) throws CronJobException {
        logger.info("job monitoring {} category",jobCategory);
        CronJobMonitor jobMonitor = cronJobMonitorRepo.findFirstByJobCategoryIgnoreCase(jobCategory);
        if(jobMonitor == null){
            CronJobMonitor jobMonitor2 =  new CronJobMonitor();
            jobMonitor2.setExpression(cronExpression);
            jobMonitor2.setJobStartTime(new Date());
            jobMonitor2.setJobCategory(jobCategory);
            jobMonitor2.setStillRunning(true);
            cronJobMonitorRepo.save(jobMonitor2);
            logger.info("job saved successfully");
        }else {
            jobMonitor.setExpression(cronExpression);
            jobMonitor.setJobStartTime(new Date());
            jobMonitor.setJobCategory(jobCategory);
            jobMonitor.setStillRunning(true);
            cronJobMonitorRepo.save(jobMonitor);

        }

    }

    @Override
    public boolean updateRunningJob(String jobCategory) throws CronJobException {
        CronJobMonitor lastIncompleteJob = cronJobMonitorRepo.findFirstByJobCategoryIgnoreCase(jobCategory);
//        logger.info("monitor is {}", lastIncompleteJob);
        if(lastIncompleteJob != null) {
//                logger.info("monitor time is {}", lastIncompleteJob.getJobStartTime());
            lastIncompleteJob.setJobEndTime(new Date());
            lastIncompleteJob.setStillRunning(false);
            cronJobMonitorRepo.save(lastIncompleteJob);
            logger.info("job updated successfully");
            return true;
        }else {
            return false;
        }
    }


    @Override
    public String getCurrentJobDesc(String category) throws CronJobException {
        CronJobExpression cronJobExpression = cronJobExpressionRepo.findLastByFlagAndCategory("Y", category);
        if (cronJobExpression != null) {
            return cronJobExpression.getCronExpressionDesc();
        } else {
            return "No Job has been configured for this category";
        }
    }


    @Override
    public boolean startCronJob() throws CronJobException {
        SettingDTO setting = configurationService.getSettingByName("ENABLE_CRON_JOB");
        if (setting != null && setting.isEnabled()) {
            logger.info("the setting read is {}", setting.isEnabled());
            return true;
        }
        return false;
    }

    @Override
    public boolean stopJob() throws CronJobException {
        return false;
    }

    @Override
    public String getCurrentExpression(String category) throws CronJobException {
        CronJobExpression cronJobExpression = cronJobExpressionRepo.findLastByFlagAndCategory("Y",category);
        if(cronJobExpression != null) {
            return cronJobExpression.getCronExpression();
        }else {
            return "0 0/20 * * * *";
        }
    }



    @Override
    public void addNewAccount() throws CronJobException {
        List<RetailUser> retailUsers = retailUserRepo.findAll();
        SettingDTO setting = configurationService.getSettingByName("TRANSACTIONAL_ACCOUNTS");
        String[] list = StringUtils.split(setting.getValue(), ",");
        logger.info("the scheme code {}", list);
        if (setting != null && setting.isEnabled()) {
            for (RetailUser retailUser : retailUsers) {
                try {
                    List<String> existingAccount = new ArrayList<>();
                logger.info("Customer id {}",retailUser.getCustomerId());
                    List<AccountInfo> accountInfos = integrationService.fetchAccountsByCifId(retailUser.getCustomerId());
                    List<Account> accounts = accountRepo.findByCustomerId(retailUser.getCustomerId());
                    for (Account account : accounts) {
                        existingAccount.add(account.getAccountNumber());
                    }

                    for (AccountInfo accountInfo : accountInfos) {
                        if (ArrayUtils.contains(list, accountInfo.getSchemeType()) && "A".equalsIgnoreCase(accountInfo.getAccountStatus())) {
                            if (!existingAccount.contains(accountInfo.getAccountNumber())) {
                                logger.info("creating new account {}", accountInfo.getAccountNumber());
                                accountService.AddFIAccount(retailUser.getCustomerId(), accountInfo);
                            }
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            List<Corporate> corporates = corporateRepo.findAll();
            for (Corporate corporate : corporates) {
                try {
                    List<String> existingAccount = new ArrayList<>();
                logger.info("Customer id is {}",corporate.getCustomerId());
                    List<AccountInfo> accountInfos = integrationService.fetchAccountsByCifId(corporate.getCustomerId());
                    List<Account> accounts = accountRepo.findByCustomerId(corporate.getCustomerId());
                    for (Account account : accounts) {
                        existingAccount.add(account.getAccountNumber());
                    }

                    for (AccountInfo accountInfo : accountInfos) {
                        if (ArrayUtils.contains(list, accountInfo.getSchemeType()) && "A".equalsIgnoreCase(accountInfo.getAccountStatus())) {
                            if (!existingAccount.contains(accountInfo.getAccountNumber())) {
                                logger.info("creating new corporate account {}", accountInfo.getAccountNumber());
                                accountService.AddFIAccount(corporate.getCustomerId(), accountInfo);
                            }
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
    @Override
public void testError(){
//        Account account = null;
//        try {
//            account.getAccountId();
//            logger.info("test error");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    private Date getFormExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        SettingDTO setting = configurationService.getSettingByName("LC_EXPIRY");
        if (setting != null && setting.isEnabled()) {
            int days = NumberUtils.toInt(setting.getValue());
            calendar.add(Calendar.DAY_OF_YEAR, days);
            return calendar.getTime();
        }
        return null;
    }
}
