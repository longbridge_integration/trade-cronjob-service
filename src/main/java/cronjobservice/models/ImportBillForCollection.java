package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Collection;


@Entity
@DiscriminatorValue("For Collection")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class ImportBillForCollection extends ImportBill {

    @ManyToOne
    FormM formM;

    @OneToMany(cascade = CascadeType.ALL)
    private Collection<ImportBillForLcDocument> billForLcDocuments;

    public Collection<ImportBillForLcDocument> getDocuments() {
        return billForLcDocuments;
    }

    public void setDocuments(Collection<ImportBillForLcDocument> billForLcDocuments) {
        this.billForLcDocuments = billForLcDocuments;
    }

    public FormM getFormM() {
        return formM;
    }

    public void setFormM(FormM formM) {
        this.formM = formM;
    }

    public Collection<ImportBillForLcDocument> getBillForLcDocuments() {
        return billForLcDocuments;
    }

    public void setBillForLcDocuments(Collection<ImportBillForLcDocument> billForLcDocuments) {
        this.billForLcDocuments = billForLcDocuments;
    }
}
