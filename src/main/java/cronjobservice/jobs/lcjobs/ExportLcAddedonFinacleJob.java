package cronjobservice.jobs.lcjobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;


@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class ExportLcAddedonFinacleJob implements Job {


    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextFormA = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextFormA.getBean (CronJobService.class);
        try {
            logger.info("Export Lc Added on Finacle Job");
            cronJobService.saveRunningJob(JobCategory.EXPORT_LC_ADDED_ON_FINACLE.toString(),cronJobService.getCurrentExpression(JobCategory.EXPORT_LC_ADDED_ON_FINACLE.toString()));

            cronJobService.getExportLcAddedOnFinacle();
            cronJobService.updateRunningJob(JobCategory.EXPORT_LC_ADDED_ON_FINACLE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
