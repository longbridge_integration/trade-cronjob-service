package cronjobservice.services;

import cronjobservice.dtos.FormADTO;
import cronjobservice.models.ServiceAudit;

public interface ServiceAuditService {

    ServiceAudit getFormAServiceAudit(FormADTO formADTO, String formResponse);
}
