package cronjobservice.repositories;

import cronjobservice.models.BillsUnderLc;
import cronjobservice.models.ExportBillForCollection;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BillsUnderLcRepo extends CommonRepo<BillsUnderLc, Long> {

    List<BillsUnderLc> findByStatusAndSubmitFlag(String S, String SF);
    BillsUnderLc findByTemporalBllNumb(String tempNumber);
}
