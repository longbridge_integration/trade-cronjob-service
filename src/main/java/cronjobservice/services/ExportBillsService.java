package cronjobservice.services;

import cronjobservice.dtos.BillsDTO;
import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.ExportBillForCollectionDTO;
import cronjobservice.dtos.ExportBillsDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.BillsUnderLc;
import cronjobservice.models.ExportBillForCollection;
import cronjobservice.models.ExportBills;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ExportBillsService {

    @Transactional
    List<String> LodgeExportBillsUnderCollection() throws CronJobException;

    @Transactional
    List<String> LodgeExportBillsUnderLc() throws CronJobException;

    void getLodgeResponseExportBillsUnderCollection();

    void getLodgeResponseExportBillsUnderLc();

    Boolean sendLodgeBillUnderCollectionITSmail(ExportBillForCollection exportBillForCollection);

    Boolean sendLodgeBillUnderLcITSmail(BillsUnderLc billsUnderLc);

    ExportBillsDTO convertEntityTODTO(ExportBills exportBills);

//    BillsDTO convertEntityTODLCTO(Bills bills);

    BillsDTO convertEntityTODTO(BillsUnderLc bills);

    ExportBillForCollectionDTO convertEntityToExportBillForCollectionDTO(ExportBillForCollection exportBillForCollection);


//    BillsDTO convertEntityToBillsUnderLcDTO(Bills billsUnderLc);

    BillsDTO convertEntityToBillsUnderLcDTO(BillsUnderLc billsUnderLc);

    List<CommentDTO> getComments(ExportBills exportBills) throws CronJobException;

    ExportBillsDTO convertEntityTODTOFinacle(ExportBills exportBills);

    @Transactional
    Boolean updateExportBillsForCollectionFromFinacle(ExportBillForCollection tmsExportBillForCollection, ExportBillForCollection finExportBillForCollection) throws CronJobException;

    @Transactional
    Boolean updateExportBillsUnderLcFromFinacle(BillsUnderLc tmsBillsUnderLc, BillsUnderLc finBillsUnderLc) throws CronJobException;

}
