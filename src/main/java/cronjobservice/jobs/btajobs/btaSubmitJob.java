package cronjobservice.jobs.btajobs;


import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.BtaService;
import cronjobservice.services.CronJobService;
import cronjobservice.services.PtaService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class btaSubmitJob implements Job {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    BtaService btaService;
    @Autowired
    CronJobService cronJobService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextLc = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextLc.getBean (CronJobService.class);
        BtaService btaService = contextLc.getBean (BtaService.class);
//        System.out.println("five minute job runing");
        try {
            logger.info("BTA Submit job is starting");
            cronJobService.saveRunningJob(JobCategory.SUBMIT_BTA.toString(),cronJobService.getCurrentExpression(JobCategory.SUBMIT_BTA.toString()));

            btaService.submitBtaToFinacle();
            cronJobService.updateRunningJob(JobCategory.SUBMIT_BTA.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
