package cronjobservice.jobs.btajobs;


import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.BtaService;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class BtaSubmitResponse implements Job {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    BtaService btaService;
    @Autowired
    CronJobService cronJobService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextLc = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextLc.getBean (CronJobService.class);
        BtaService btaService = contextLc.getBean (BtaService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.BTA_SUBMIT_RESPONSE.toString(),cronJobService.getCurrentExpression(JobCategory.BTA_SUBMIT_RESPONSE.toString()));

            btaService.getBtaSubmitResponse();
            cronJobService.updateRunningJob(JobCategory.BTA_SUBMIT_RESPONSE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
