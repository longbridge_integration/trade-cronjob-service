package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

/**
 * Created by Showboy on 25/08/2017.
 */
@Entity
@Audited(withModifiedFlag=true)
@Where(clause ="del_Flag='N'" )
public class WorkFlow extends AbstractEntity{

    private String name;
    private String description;
    private String codeType;
    private String filePath;
    private Date dateCreated;

    @JsonProperty
    @OneToMany(cascade = CascadeType.ALL)
    private List<WorkFlowRules> actions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public List<WorkFlowRules> getActions() {
        return actions;
    }

    public void setActions(List<WorkFlowRules> actions) {
        this.actions = actions;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return "WorkFlow{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", codeType='" + codeType + '\'' +
                ", filePath='" + filePath + '\'' +
                ", dateCreated=" + dateCreated +
                ", actions=" + actions +
                '}';
    }



}
