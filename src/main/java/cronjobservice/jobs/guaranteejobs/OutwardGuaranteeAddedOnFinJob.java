package cronjobservice.jobs.guaranteejobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;


@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class OutwardGuaranteeAddedOnFinJob implements Job{

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextGuarantee = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextGuarantee.getBean (CronJobService.class);
//        System.out.println("five minute job runing");
        try {
            cronJobService.saveRunningJob(JobCategory.OUTWARDGUARANATEE_ADDED_ON_FINACLE.toString(),cronJobService.getCurrentExpression(JobCategory.OUTWARDGUARANATEE_ADDED_ON_FINACLE.toString()));

            cronJobService.getOutwardGuaranteeAddedOnFinacle();
//            cronJobService.getInwardGuaranteeAddedOnFinacleByCifid();

            cronJobService.updateRunningJob(JobCategory.OUTWARDGUARANATEE_ADDED_ON_FINACLE.toString());
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
