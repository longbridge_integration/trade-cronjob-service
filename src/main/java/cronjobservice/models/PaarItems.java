package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by SYLVESTER on 9/7/2017.
 */
@Entity
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class PaarItems extends AbstractEntity{
    private String status;

    private String hsCode;

    private String goodsDescription;

    private String stateGoods;

    private String sectoralPurpose;

    private String unitMeasure;

    private long  fobvalue;

    private long  frieghtCharge;

    private String unitPrice;

    private  String quantity;

    private String modeOfPackaging;

    private String netWeight;

    private String grossWeight;

    private String count;


    @ManyToOne
    private Paar paar;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getGoodsDescription() {
        return goodsDescription;
    }

    public void setGoodsDescription(String goodsDescription) {
        this.goodsDescription = goodsDescription;
    }

    public String getStateGoods() {
        return stateGoods;
    }

    public void setStateGoods(String stateGoods) {
        this.stateGoods = stateGoods;
    }

    public String getSectoralPurpose() {
        return sectoralPurpose;
    }

    public void setSectoralPurpose(String sectoralPurpose) {
        this.sectoralPurpose = sectoralPurpose;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public long getFobvalue() {
        return fobvalue;
    }

    public void setFobvalue(long fobvalue) {
        this.fobvalue = fobvalue;
    }

    public long getFrieghtCharge() {
        return frieghtCharge;
    }

    public void setFrieghtCharge(long frieghtCharge) {
        this.frieghtCharge = frieghtCharge;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getModeOfPackaging() {
        return modeOfPackaging;
    }

    public void setModeOfPackaging(String modeOfPackaging) {
        this.modeOfPackaging = modeOfPackaging;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }

    public Paar getPaar() {
        return paar;
    }

    public void setPaar(Paar paar) {
        this.paar = paar;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
