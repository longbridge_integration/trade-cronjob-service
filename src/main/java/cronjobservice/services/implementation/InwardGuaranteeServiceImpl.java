package cronjobservice.services.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cronjobservice.dtos.FormHolder;
import cronjobservice.dtos.InwardGuaranteeDTO;
import cronjobservice.dtos.SettingDTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.*;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.FormNumber;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.*;

@Service
public class InwardGuaranteeServiceImpl implements InwardGuaranteeService {

    @Autowired
    MessageSource messageSource;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    MailService mailService;
    @Autowired
    CorporateRepo corporateRepo;

    @Autowired
    CorporateUserRepo corporateUserRepo;
//    @Autowired
//    CommentService commentService;
    @Autowired
    CorporateService corporateService;

    @Autowired
    RetailUserService retailUserService;

    @Autowired
    EntityManager entityManager;
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private WorkFlowRepo workFlowRepo;
    @Autowired
    private ConfigurationService configurationService;
    private Locale locale = LocaleContextHolder.getLocale();
    @Autowired
    private InwardGuaranteeRepo inwardGuaranteeRepo;

    @Override
    public Boolean addInwardGuaranteeFromFinacleByCifid(InwardGuaranteeDTO inwardGuaranteeDTO) throws CronJobException {

        try {

            InwardGuarantee exists = inwardGuaranteeRepo.findByGuaranteeNumber(inwardGuaranteeDTO.getGuaranteeNumber());
            if(exists == null) {
                SettingDTO wfSetting = configurationService.getSettingByName("DEF_INGUAR_WF");
                logger.info("setting the workflow {}", wfSetting);
                WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
                inwardGuaranteeDTO.setWorkFlowId(workFlow.getId());
                logger.info("workflow ID {}", workFlow.getId());

//                InwardGuarantee inwardGuarantee = convertDTOTOEntity(inwardGuaranteeDTO);
                InwardGuarantee inwardGuarantee = modelMapper.map(inwardGuaranteeDTO, InwardGuarantee.class);

                if (inwardGuarantee.getTempFormNumber() == null || inwardGuarantee.getTempFormNumber().equals(""))
                {
                    inwardGuarantee.setTempFormNumber(FormNumber.getInwGuranteeNumner());
                }
                inwardGuarantee.setInitiatedBy("FINACLE");
                inwardGuarantee.setGuaranteeNumber(inwardGuaranteeDTO.getGuaranteeNumber());
                inwardGuarantee.setInitiatedBy(UserType.CORPORATE.toString());
                inwardGuarantee.setExpiryOn(inwardGuaranteeDTO.getExpiryOn());
                inwardGuaranteeRepo.save(inwardGuarantee);

            }else{
                logger.info("Inward Guarantee already exist");
            }

            logger.info("Inward Guarantee created successfully");
            return true;
        } catch (CronJobException te) {
            te.printStackTrace();
            return false;
        }
    }

    @Transactional
    @Override
    public Boolean addInwardGuaranteeFromFinacle(InwardGuaranteeDTO inwardGuaranteeDTO) throws CronJobException {

        SettingDTO inwardGuaranteeApproval = configurationService.getSettingByName("DEF_INGUAR_WF");
        logger.info("inward G setting {} ",inwardGuaranteeApproval);
        if(inwardGuaranteeApproval!=null && inwardGuaranteeApproval.isEnabled()&& inwardGuaranteeDTO.getStatus().equals("S"))
        {
            inwardGuaranteeDTO.setStatus("A");
        }
        try {

            String number = FormNumber.getInwGuranteeNumner();

            InwardGuarantee inwardGuarantee = modelMapper.map(inwardGuaranteeDTO, InwardGuarantee.class);
//            inwardGuarantee.setUserType("BANK");
            inwardGuarantee.setInitiatedBy("FINACLE");
            logger.info("Inward Guarantee Cifid from Finacle {} ", inwardGuaranteeDTO.getCifid());
            inwardGuarantee.setCifid(inwardGuaranteeDTO.getCifid());
            inwardGuarantee.setExpiryOn(inwardGuaranteeDTO.getExpiryOn());
            inwardGuarantee.setTempFormNumber(number);
            inwardGuarantee.setGuaranteeNumber(inwardGuaranteeDTO.getGuaranteeNumber());
            System.out.println("before getting settings");
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_INGUAR_WF");
            System.out.println("after getting settings");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            System.out.println("after getting workflow");
            logger.info("Inward Guarantee Workflow {} ",workFlow);
            inwardGuarantee.setWorkFlow(workFlow);
            inwardGuaranteeRepo.save(inwardGuarantee);
            return true;
        }

        catch (CronJobException e)
        {
            throw new CronJobException(messageSource.getMessage("guarantee.add.success", null, locale));
        }
    }


    @Override
    public String updateInGuarantee(InwardGuaranteeDTO inwardGuaranteeDTO) throws CronJobException {
        try {
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            User doneBy = principal.getUser();
            InwardGuarantee inwardGuarantee = inwardGuaranteeRepo.findOne(inwardGuaranteeDTO.getId());
           logger.info("find inward with id {}",inwardGuarantee);
            entityManager.detach(inwardGuarantee);
            inwardGuarantee = convertDTOTOEntity(inwardGuaranteeDTO);
            inwardGuarantee.setInitiatedBy(doneBy.getUserName());
            inwardGuarantee.setCreatedOn(new Date());
                inwardGuaranteeRepo.save(inwardGuarantee);
            logger.info("Updated guarantee with id {}", inwardGuarantee.getId());
            return messageSource.getMessage("inwguar.update.success", null, locale);

        } catch (CronJobException e) {
            throw new CronJobException(messageSource.getMessage("inwguar.update.failure", null, locale));
        }
    }

    @Override
    public InwardGuaranteeDTO editInGuarantee(Long id) throws CronJobException {
        InwardGuarantee inwardGuarantee = inwardGuaranteeRepo.findOne(id);
        return convertEntityToDTO(inwardGuarantee);
    }

    @Override
    public InwardGuaranteeDTO getInitiatorDetails() throws CronJobException {
        return null;
    }

    @Override
    public String addInGuarantee(InwardGuaranteeDTO inwardGuaranteeDTO) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;

        if ((loggedInUser.getUserType()).equals(UserType.RETAIL)) {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        } else if (loggedInUser.getUserType().equals(UserType.BANK)) {
            customerId = inwardGuaranteeDTO.getCifid();
        }
        try {
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_INGUAR_WF");
            logger.info("workflow setting {}",wfSetting.toString());
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            logger.info("workflow {} and id {}",workFlow,workFlow.getId());
            inwardGuaranteeDTO.setWorkFlowId(workFlow.getId());
            InwardGuarantee inwardGuarantee = convertDTOTOEntity(inwardGuaranteeDTO);
            inwardGuarantee.setGuaranteeNumber(FormNumber.getInwGuranteeNumner());
            inwardGuarantee.setCreatedOn(new Date());
            inwardGuarantee.setCifid(customerId);
            inwardGuaranteeRepo.save(inwardGuarantee);
            logger.info("Inward Guarantee created successfully");
            return messageSource.getMessage("inwguar.add.success", null, locale);
        } catch (CronJobException te) {
            throw new CronJobException(messageSource.getMessage("inwguar.add.failure", null, locale));
        }
    }

    @Override
    public InwardGuaranteeDTO getInGuarantee(Long id) throws CronJobException {

        InwardGuarantee inwardGuarantee=inwardGuaranteeRepo.findOne(id);
        return convertEntityToDTO(inwardGuarantee);
    }

    @Override
    public InwardGuaranteeDTO getInGuaranteeNumber(String guaranteeNumber) throws CronJobException {

        InwardGuarantee inwardGuarantee=inwardGuaranteeRepo.findByGuaranteeNumber(guaranteeNumber);
        return convertEntityToDTO(inwardGuarantee);
    }

    @Override
    public Page<InwardGuaranteeDTO> getClosedInGuarantee(Pageable pageable) {
        return null;
    }

    @Override
    public Page<InwardGuaranteeDTO> getInGuaranteeForBankUser(Pageable pageable) throws CronJobException {
        Page<InwardGuarantee> page = inwardGuaranteeRepo.findAll(pageable);
        List<InwardGuaranteeDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<InwardGuaranteeDTO> pageImpl = new PageImpl<InwardGuaranteeDTO>(dtOs, pageable, t);
        return pageImpl;
    }

    @Override
    public FormHolder createFormHolder(InwardGuaranteeDTO inwardGuaranteeDTO, String name) throws CronJobException {
        return null;
    }


    @Override
    public FormHolder executeAction(InwardGuaranteeDTO inwardGuaranteeDTO, String name) throws CronJobException {
        return null;
    }

    @Override
    public Page<InwardGuaranteeDTO> getInGuarantees(Pageable pageDetails) {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;

        if ((loggedInUser.getUserType()).equals(UserType.RETAIL))
        {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        }
        else if (loggedInUser.getUserType().equals(UserType.CORPORATE))
        {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        }

        Page<InwardGuarantee> page = inwardGuaranteeRepo.findAll(pageDetails);
        List<InwardGuaranteeDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<InwardGuaranteeDTO> pageImpl = new PageImpl<InwardGuaranteeDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<InwardGuaranteeDTO> getAllInGuarantees(Pageable pageDetails) {
        Page<InwardGuarantee> page = inwardGuaranteeRepo.findByStatusNotNull(pageDetails);
        List<InwardGuaranteeDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<InwardGuaranteeDTO> pageImpl = new PageImpl<InwardGuaranteeDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }

    @Override
    public Page<InwardGuaranteeDTO> getOpenGuarantees(Pageable pageable) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User loggedInUser = principal.getUser();
        String customerId = null;

        if ((loggedInUser.getUserType()).equals(UserType.RETAIL))
        {
            RetailUser retailUser = (RetailUser) loggedInUser;
            customerId = retailUser.getCustomerId();
        }
        else if (loggedInUser.getUserType().equals(UserType.CORPORATE))
        {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            customerId = corporateUser.getCorporate().getCustomerId();
        }


        Page<InwardGuarantee> pages=inwardGuaranteeRepo.findByCifidAndStatus(customerId,"P","S","D",pageable);
        List<InwardGuaranteeDTO> inwardGuaranteeDTOS=convertEntitiesToDTOs(pages.getContent());
        long t=pages.getTotalElements();
        Page<InwardGuaranteeDTO> page1=new PageImpl<InwardGuaranteeDTO>(inwardGuaranteeDTOS,pageable,t);
        return page1;
    }



    @Override
    public List<InwardGuaranteeDTO> getCustomerClosedInGuarantee() {
        return null;
    }

    @Override
    public List<InwardGuaranteeDTO> getCustomerClosedInGuaranteeByCif(String cifid) {
        return null;
    }

    @Override
    public InwardGuarantee convertDTOTOEntity(InwardGuaranteeDTO inwardGuaranteeDTO) {

        WorkFlow workFlow = workFlowRepo.findOne(inwardGuaranteeDTO.getWorkFlowId());
        inwardGuaranteeDTO.setWorkFlow(workFlow);
        InwardGuarantee inwardGuarantee = modelMapper.map(inwardGuaranteeDTO, InwardGuarantee.class);


        return inwardGuarantee;
    }

    @Override
    public InwardGuaranteeDTO convertEntityToDTO(InwardGuarantee inwardGuarantee) {
        InwardGuaranteeDTO inwardGuaranteeDTO = modelMapper.map(inwardGuarantee, InwardGuaranteeDTO.class);


        if (inwardGuarantee.getWorkFlow() != null) {
            inwardGuaranteeDTO.setWorkFlowId(inwardGuarantee.getWorkFlow().getId());
//            WorkFlowStatesDTO workFlowState = workFlowStatesService.getByTypeAndCode(inwardGuarantee.getWorkFlow().getCodeType(), inwardGuarantee.getStatus());
//            inwardGuaranteeDTO.setStatusDesc(workFlowState.getDescription());
        }
//        if(inwardGuaranteeDTO.getStatus()!=null && !(inwardGuaranteeDTO.getStatus().equals("P"))){
//            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(inwardGuaranteeDTO.getWorkFlow().getCodeType(), inwardGuaranteeDTO.getStatus());
//            inwardGuaranteeDTO.setStatusDesc(workFlowStates.getDescription());
//
//
//        }
        if (inwardGuarantee.getCreatedOn() != null) {
            inwardGuaranteeDTO.setConvert(DateFormatter.format(inwardGuarantee.getCreatedOn()));
        }
        return inwardGuaranteeDTO;
    }

    @Override
    public List<InwardGuaranteeDTO> convertEntitiesToDTOs(Iterable<InwardGuarantee> inwardGuarantees) {
        List<InwardGuaranteeDTO> inwardGuaranteeDTOS = new ArrayList<>();
        for (InwardGuarantee inwardGuarantee : inwardGuarantees) {
            InwardGuaranteeDTO inwardGuaranteeDTO = convertEntityToDTO(inwardGuarantee);
            inwardGuaranteeDTOS.add(inwardGuaranteeDTO);
        }
        return inwardGuaranteeDTOS;
    }

    @Override
    public Iterable<InwardGuaranteeDTO> getGuarantees() {
        Iterable<InwardGuarantee> inwardGuarantees = inwardGuaranteeRepo.findAll();
        logger.info("this are the guarantee &&&&&&",inwardGuarantees);
        return convertEntitiesToDTOs(inwardGuarantees);
    }

    @Override
    public void sendITSmail(CorporateUser user, InwardGuarantee inwardGuarantee) {

        try {
            SettingDTO settingDTO = configurationService.getSettingByName("GROUP_EMAIL");
            String itsMail = settingDTO.getValue();
            String fullName = user.getCorporate().getName();
            String formType = "outward guarantee";
            Context context = new Context();
            context.setVariable("name", fullName);
            context.setVariable("username", user.getUserName());
            context.setVariable("formNumber", inwardGuarantee.getGuaranteeNumber());
            context.setVariable("formType", formType);
            context.setVariable("date", new Date());
            context.setVariable("customerId", user.getCorporate().getCustomerId());
            context.setVariable("code", "INWARD GUARANTEE");
            String subject = String.format(messageSource.getMessage("form.submission.subject", null, locale),formType);
            Email email = new Email.Builder()
                    .setRecipient(itsMail)
                    .setSubject(subject)
                    .setTemplate("mail/formSubmission")
                    .build();

            mailService.sendMail(email, context);
        } catch (Exception e) {
            logger.error("Error occurred sending activation credentials", e);
        }
    }


    @Override
    public void sendSubmitNotification(InwardGuarantee inwardGuarantee) throws CronJobException{
        System.out.println("send mail service");
        try {
            String customerMail = "";
            String fullName = "";
            String usertype = checkUsertype(inwardGuarantee.getCifid());

            if (usertype.equalsIgnoreCase("CORPORATE")) {
                Corporate corporate = corporateRepo.findFirstByCustomerId(inwardGuarantee.getCifid());
                customerMail = corporate.getEmail();
                List<CorporateUser> users = corporateUserRepo.findByCorporateAndCorpUserType(corporate,CorpUserType.AUTHORIZER);
                String[] recipients = new String[users.size()+1];
                recipients[0] = customerMail;
                users.forEach(user->{
                    int counter = 1;
                    recipients[counter] = user.getEmail();
                    counter++;
                });

                fullName = corporate.getName();

                String formType = "Inward Guarantee";
                Context context = new Context();
                context.setVariable("name", fullName);
                context.setVariable("username", inwardGuarantee.getInitiatedBy());
                context.setVariable("formNumber", inwardGuarantee.getGuaranteeNumber());
                context.setVariable("formType", formType);
                context.setVariable("date", new Date());
                context.setVariable("customerId", inwardGuarantee.getCifid());
                context.setVariable("code", "CUSTOMER");
                String subject = String.format(messageSource.getMessage("form.submission.subject", null, locale), formType);
                Email email = new Email.Builder()
                        .setRecipients(recipients)
                        .setSubject(subject)
                        .setTemplate("mail/bankSubmission")
                        .build();

                mailService.sendMail(email, context);
            }



        } catch (Exception e) {
            logger.error("Error occurred sending activation credentials", e);
        }
    }

    private String checkUsertype(String cifid) throws CronJobException {
        String result = "";
        if (corporateRepo.existsByCustomerId(cifid)) {
            result = "CORPORATE";
        }
        return result;
    }

}
