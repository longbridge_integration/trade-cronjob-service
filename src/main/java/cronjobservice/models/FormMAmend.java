package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "formm_amendment")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class FormMAmend extends Process{

    private boolean forex;
    private String prefex;
    private String GenDesc;
    private String netWeight;
    private String grossWeight;
    private int quantityOfItems;
    private String airTicketNum;
    private String route;
    private String airline;
    private String benePhone;
    private String beneOrderBy;
    private String appName;
    private String appEmail;
    private String appAddress1;
    private String appAddress2;
    private String city;
    private String appState;
    private String appPhone;
    private String appTIN;
    private String appRCNo;
    private String appFax;
    private String appNEPC;
    private String totalFB;
    private String totalFreight;
    private String ancillaryCharge;
    private String proformaDate;
    private String TOD;
    private String insurValue;
    private String totalCF;
    private String currencyCode;
    private String exchangeRate;
    private String sourceFunds;
    private String paymentMode;
    private String transferMode;
    private String proformaNo;
    private String paymentDate;
    private String ModeOfTransport;
    private String portOfDischarge;
    private String portOfLoading;
    private String countryOfOrigin;
    private String countryOfSupply;
    private String customOffice;
    private String beneName;
    private String beneEmail;
    private String beneAddress1;
    private String beneAddress2;
    private String beneStateCode;
    private String beneFax;
    private String appEndorsementRep;
    private String appEndorsementDeaker;
    private Date date1;
    private Date date2;
    private String attachments;
    private String beneCity;
    private String beneState;
    private String designatedBank;
    private String inspectionAgent;
    private Date expiry;
    private String allocatedAmount;
    private String initiatedBy;
    private Date dateCreated;
    private String formNumber;
    private String utilization;
    private BigDecimal formAmount;
    private boolean guidelineApproval;

    //Insurance details
    private String insuranceCompanyName;
    private String insuranceCompanyAddress;
    private String insurancePolicyNo;
    private String insurancePolicyDate;
    private String insurancePayableAt;
    private String insuranceDateOfExpiry;
    private String insurancePercentage;
    private BigDecimal insuredAmount;
    private BigDecimal premiumAmount;
    private String insuredBy;

    private UserType userType;
    private String amendAction;


    public boolean isForex() {
        return forex;
    }

    public void setForex(boolean forex) {
        this.forex = forex;
    }

    public String getPrefex() {
        return prefex;
    }

    public void setPrefex(String prefex) {
        this.prefex = prefex;
    }

    public String getGenDesc() {
        return GenDesc;
    }

    public void setGenDesc(String genDesc) {
        GenDesc = genDesc;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }

    public int getQuantityOfItems() {
        return quantityOfItems;
    }

    public void setQuantityOfItems(int quantityOfItems) {
        this.quantityOfItems = quantityOfItems;
    }

    public String getAirTicketNum() {
        return airTicketNum;
    }

    public void setAirTicketNum(String airTicketNum) {
        this.airTicketNum = airTicketNum;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getBenePhone() {
        return benePhone;
    }

    public void setBenePhone(String benePhone) {
        this.benePhone = benePhone;
    }

    public String getBeneOrderBy() {
        return beneOrderBy;
    }

    public void setBeneOrderBy(String beneOrderBy) {
        this.beneOrderBy = beneOrderBy;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppEmail() {
        return appEmail;
    }

    public void setAppEmail(String appEmail) {
        this.appEmail = appEmail;
    }

    public String getAppAddress1() {
        return appAddress1;
    }

    public void setAppAddress1(String appAddress1) {
        this.appAddress1 = appAddress1;
    }

    public String getAppAddress2() {
        return appAddress2;
    }

    public void setAppAddress2(String appAddress2) {
        this.appAddress2 = appAddress2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAppState() {
        return appState;
    }

    public void setAppState(String appState) {
        this.appState = appState;
    }

    public String getAppPhone() {
        return appPhone;
    }

    public void setAppPhone(String appPhone) {
        this.appPhone = appPhone;
    }

    public String getAppTIN() {
        return appTIN;
    }

    public void setAppTIN(String appTIN) {
        this.appTIN = appTIN;
    }

    public String getAppRCNo() {
        return appRCNo;
    }

    public void setAppRCNo(String appRCNo) {
        this.appRCNo = appRCNo;
    }

    public String getAppFax() {
        return appFax;
    }

    public void setAppFax(String appFax) {
        this.appFax = appFax;
    }

    public String getAppNEPC() {
        return appNEPC;
    }

    public void setAppNEPC(String appNEPC) {
        this.appNEPC = appNEPC;
    }

    public String getTotalFB() {
        return totalFB;
    }

    public void setTotalFB(String totalFB) {
        this.totalFB = totalFB;
    }

    public String getTotalFreight() {
        return totalFreight;
    }

    public void setTotalFreight(String totalFreight) {
        this.totalFreight = totalFreight;
    }

    public String getAncillaryCharge() {
        return ancillaryCharge;
    }

    public void setAncillaryCharge(String ancillaryCharge) {
        this.ancillaryCharge = ancillaryCharge;
    }

    public String getProformaDate() {
        return proformaDate;
    }

    public void setProformaDate(String proformaDate) {
        this.proformaDate = proformaDate;
    }

    public String getTOD() {
        return TOD;
    }

    public void setTOD(String TOD) {
        this.TOD = TOD;
    }

    public String getInsurValue() {
        return insurValue;
    }

    public void setInsurValue(String insurValue) {
        this.insurValue = insurValue;
    }

    public String getTotalCF() {
        return totalCF;
    }

    public void setTotalCF(String totalCF) {
        this.totalCF = totalCF;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getSourceFunds() {
        return sourceFunds;
    }

    public void setSourceFunds(String sourceFunds) {
        this.sourceFunds = sourceFunds;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getTransferMode() {
        return transferMode;
    }

    public void setTransferMode(String transferMode) {
        this.transferMode = transferMode;
    }

    public String getProformaNo() {
        return proformaNo;
    }

    public void setProformaNo(String proformaNo) {
        this.proformaNo = proformaNo;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getModeOfTransport() {
        return ModeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        ModeOfTransport = modeOfTransport;
    }

    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }

    public String getPortOfLoading() {
        return portOfLoading;
    }

    public void setPortOfLoading(String portOfLoading) {
        this.portOfLoading = portOfLoading;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public String getCountryOfSupply() {
        return countryOfSupply;
    }

    public void setCountryOfSupply(String countryOfSupply) {
        this.countryOfSupply = countryOfSupply;
    }

    public String getCustomOffice() {
        return customOffice;
    }

    public void setCustomOffice(String customOffice) {
        this.customOffice = customOffice;
    }

    public String getBeneName() {
        return beneName;
    }

    public void setBeneName(String beneName) {
        this.beneName = beneName;
    }

    public String getBeneEmail() {
        return beneEmail;
    }

    public void setBeneEmail(String beneEmail) {
        this.beneEmail = beneEmail;
    }

    public String getBeneAddress1() {
        return beneAddress1;
    }

    public void setBeneAddress1(String beneAddress1) {
        this.beneAddress1 = beneAddress1;
    }

    public String getBeneAddress2() {
        return beneAddress2;
    }

    public void setBeneAddress2(String beneAddress2) {
        this.beneAddress2 = beneAddress2;
    }

    public String getBeneStateCode() {
        return beneStateCode;
    }

    public void setBeneStateCode(String beneStateCode) {
        this.beneStateCode = beneStateCode;
    }

    public String getBeneFax() {
        return beneFax;
    }

    public void setBeneFax(String beneFax) {
        this.beneFax = beneFax;
    }

    public String getAppEndorsementRep() {
        return appEndorsementRep;
    }

    public void setAppEndorsementRep(String appEndorsementRep) {
        this.appEndorsementRep = appEndorsementRep;
    }

    public String getAppEndorsementDeaker() {
        return appEndorsementDeaker;
    }

    public void setAppEndorsementDeaker(String appEndorsementDeaker) {
        this.appEndorsementDeaker = appEndorsementDeaker;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getBeneCity() {
        return beneCity;
    }

    public void setBeneCity(String beneCity) {
        this.beneCity = beneCity;
    }

    public String getBeneState() {
        return beneState;
    }

    public void setBeneState(String beneState) {
        this.beneState = beneState;
    }

    public String getDesignatedBank() {
        return designatedBank;
    }

    public void setDesignatedBank(String designatedBank) {
        this.designatedBank = designatedBank;
    }

    public String getInspectionAgent() {
        return inspectionAgent;
    }

    public void setInspectionAgent(String inspectionAgent) {
        this.inspectionAgent = inspectionAgent;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public String getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(String allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public String getUtilization() {
        return utilization;
    }

    public void setUtilization(String utilization) {
        this.utilization = utilization;
    }

    public BigDecimal getFormAmount() {
        return formAmount;
    }

    public void setFormAmount(BigDecimal formAmount) {
        this.formAmount = formAmount;
    }

    public boolean isGuidelineApproval() {
        return guidelineApproval;
    }

    public void setGuidelineApproval(boolean guidelineApproval) {
        this.guidelineApproval = guidelineApproval;
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    public String getInsuranceCompanyAddress() {
        return insuranceCompanyAddress;
    }

    public void setInsuranceCompanyAddress(String insuranceCompanyAddress) {
        this.insuranceCompanyAddress = insuranceCompanyAddress;
    }

    public String getInsurancePolicyNo() {
        return insurancePolicyNo;
    }

    public void setInsurancePolicyNo(String insurancePolicyNo) {
        this.insurancePolicyNo = insurancePolicyNo;
    }

    public String getInsurancePolicyDate() {
        return insurancePolicyDate;
    }

    public void setInsurancePolicyDate(String insurancePolicyDate) {
        this.insurancePolicyDate = insurancePolicyDate;
    }

    public String getInsurancePayableAt() {
        return insurancePayableAt;
    }

    public void setInsurancePayableAt(String insurancePayableAt) {
        this.insurancePayableAt = insurancePayableAt;
    }

    public String getInsuranceDateOfExpiry() {
        return insuranceDateOfExpiry;
    }

    public void setInsuranceDateOfExpiry(String insuranceDateOfExpiry) {
        this.insuranceDateOfExpiry = insuranceDateOfExpiry;
    }

    public String getInsurancePercentage() {
        return insurancePercentage;
    }

    public void setInsurancePercentage(String insurancePercentage) {
        this.insurancePercentage = insurancePercentage;
    }

    public BigDecimal getInsuredAmount() {
        return insuredAmount;
    }

    public void setInsuredAmount(BigDecimal insuredAmount) {
        this.insuredAmount = insuredAmount;
    }

    public BigDecimal getPremiumAmount() {
        return premiumAmount;
    }

    public void setPremiumAmount(BigDecimal premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    public String getInsuredBy() {
        return insuredBy;
    }

    public void setInsuredBy(String insuredBy) {
        this.insuredBy = insuredBy;
    }


    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }


    public String getAmendAction() {
        return amendAction;
    }

    public void setAmendAction(String amendAction) {
        this.amendAction = amendAction;
    }
}
