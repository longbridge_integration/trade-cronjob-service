package cronjobservice.repositories;

import cronjobservice.models.OutwardGuarantee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by SYLVESTER on 10/18/2017.
 */
@Repository
@Transactional
public interface OutwardGuaranteeRepo extends CommonRepo<OutwardGuarantee,Long> {

    Long countByCifid(String cifid);

    List<OutwardGuarantee> findByStatusAndSubmitFlag(String S, String SF);

    OutwardGuarantee findByTempFormNumber(String tempFormNumber);
    OutwardGuarantee findByGuaranteeNumber(String guaranteeNumber);

    @Query("select s from OutwardGuarantee s where s.status!=:openstatus")
    Page<OutwardGuarantee> findStatus(@Param("openstatus") String status, Pageable pageable);

    @Query("select s from OutwardGuarantee s where s.status=:openstatus")
    Page<OutwardGuarantee> findOpenStatus(@Param("openstatus") String status, Pageable pageable);

    Page<OutwardGuarantee> findByStatusIsNull(Pageable pageable);

    Page<OutwardGuarantee> findByStatusNotNull(Pageable pageable);

    Page<OutwardGuarantee> findByCifidAndStatusNotNull(String cif, Pageable pageable);

//  @Query("select s from InwardGuarantee s where s.status is not null and s.status=:submitted")
//  Page<InwardGuarantee> findByStatusAndCifid(@Param("cifid") String cifid,@Param("submitted") String submitted,Pageable pageable);


    @Query("select distinct s from OutwardGuarantee s where s.cifid=:cifid and s.status=:submitted")
    Page<OutwardGuarantee> findByCifidAndStatusS(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

//  @Query("select s from InwardGuarantee s where s.status is null or s.status=:submitted or s.status=:declined")
//  Page<OutwardGuarantee> findByStatus(@Param("submitted") String submitted,@Param("declined") String decline,Pageable pageable);


    @Query("select distinct s from OutwardGuarantee s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<OutwardGuarantee> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);



    @Query("select distinct s from OutwardGuarantee s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<OutwardGuarantee> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    Page<OutwardGuarantee> findByCifid(String cifid, Pageable pageable);

    @Query("select distinct s from OutwardGuarantee s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<OutwardGuarantee> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    @Query("select distinct s from OutwardGuarantee s where s.cifid=:cifid  and  s.status is not null  and   s.status!=:submitted and s.status!=:saved")
    Page<OutwardGuarantee> findByClosedCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, @Param("saved") String saved, Pageable pageable);

    @Query("select s from OutwardGuarantee s where s.status=:submitted")
    Page<OutwardGuarantee> findByStatusS(@Param("submitted") String submitted, Pageable pageable);


}
