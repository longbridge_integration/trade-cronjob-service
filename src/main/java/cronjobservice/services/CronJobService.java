package cronjobservice.services;

import cronjobservice.api.AccountDetails;
import cronjobservice.api.CustomerDetails;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.Account;
import cronjobservice.models.Corporate;
import cronjobservice.models.CorporateUser;
import cronjobservice.models.RetailUser;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.util.List;

/**
 * Created by Longbridge on 6/24/2017.
 */
public interface CronJobService {
    void updateAllAccountName(Account account, AccountDetails accountDetails) throws CronJobException;
    boolean updateRetailUserDetails() throws CronJobException;
    boolean updateCorporateUserDetails() throws CronJobException;
    boolean updateCorporateDetails() throws CronJobException;
    void updateRetailUserBVN(RetailUser retailUser, String bvn) throws CronJobException;
    void updateRetailUserPhoneNo(RetailUser retailUser, CustomerDetails details) throws CronJobException;
    void updateRetailUserEmail(RetailUser retailUser, CustomerDetails details) throws CronJobException;
    void updateCorporateUserBVN(Corporate corporate) throws CronJobException;
    void updateCorporateUserPhoneNo(CorporateUser corporateUser, CustomerDetails details) throws CronJobException;
    void updateCorporateUserEmail(CorporateUser corporateUser, CustomerDetails details) throws CronJobException;
    void updateUserDetailsByCifid(String cifid);


    @Transactional
    void getPorts();
    @Transactional
    void getModeOfTrans();
    @Transactional
    void getPurposeOfPayment();
    @Transactional
    void getPaymentModeCode();

    boolean updateAccountDetials() throws CronJobException;
    void updateAllAccountCurrency(Account account, AccountDetails accountDetails) throws CronJobException;
    void updateAccountStatus(Account account, AccountDetails accountDetails) throws CronJobException;

    @Transactional
    void getFormAItems() throws ParseException;
    void getFormItems() throws ParseException;


    void getFormMAddedOnFinacle() throws CronJobException;
    void updateCustomerFormMsBkp() throws CronJobException;
    void updateCustomerFormMsByCifId(String cifId) throws CronJobException;
    void getExpiredForm() throws CronJobException;

//    CronJobService() throws CronJobException;


    @Transactional
    List<String> submitFormAToFinacleTest() throws CronJobException;

    List<String> submitFormAToFinacle() throws CronJobException;
    List<String> submitFormNXPToFinacle() throws CronJobException;
//    List<String> submitFormQToFinacle() throws CronJobException;
    List<String> submitRemittanceToFinacle() throws CronJobException;

    void getFormAAllocated();
    String getFormAAddedOnFinacle();

    @Transactional
    String getFormACanceledOnFinacle();

    String getFormNxpAddedOnFinacle();
//    String getFormMAddedOnFinacle();
    String getRemittanceIssuedOnFinacle();

    void getFormAResponse();
    void getFormNxpResponse();
    void getRemittanceResponse();

    List<String> submitOutwardGuaranteeToFinacle() throws CronJobException;
    void getOutwardGuaranteeAddResponse();
    String getInwardGuaranteeAddedOnFinacleByCifid();
    String getInwardGuaranteeAddedOnFinacle();
    String getOutwardGuaranteeAddedOnFinacle();
    void getFormMAllocated();
    List<String> submitImportLcToFinacle() throws CronJobException;
    void getImportLcIssueResponse();

    String getImportLcAddedOnFinacle();

    List<String> submitExportLcToFinacle() throws CronJobException;
    void getExportLcAdviseResponse();


    void getExportLcPendingResponse();

    void UpdateExportLcPendingResponse();

    String getExportLcAddedOnFinacle();


    void keepCronJobEprsDetials(String username, String cronExp, String cronExprDesc, String category) throws CronJobException;
    void deleteRunningJob(String category) throws CronJobException;
    void saveRunningJob(String jobCategory, String cronExpression) throws CronJobException;
    String getCurrentExpression(String category) throws CronJobException;
    String getCurrentJobDesc(String category) throws CronJobException;
    boolean updateRunningJob(String jobCategory) throws CronJobException;
    boolean startCronJob() throws CronJobException;
    boolean stopJob() throws CronJobException;
    void addNewAccount() throws CronJobException;


    void testError();
}