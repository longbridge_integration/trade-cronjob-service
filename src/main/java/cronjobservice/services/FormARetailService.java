package cronjobservice.services;


import cronjobservice.dtos.AccountDTO;
import cronjobservice.dtos.CommentDTO;
import cronjobservice.dtos.FormADTO;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.FormA;
import cronjobservice.models.Remittance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by chiomarose on 07/09/2017.
 */
public interface FormARetailService
{

//    String saveFormA(FormADTO formADTO) throws CronJobException;

//    String updateFormA(FormADTO formADTO) throws CronJobException;

    Boolean updateFormAFromFinacle(FormA tmsForm, FormA finForm) throws CronJobException;
    Boolean updateFormAAllocation(FormA tmsForm, FormA finForm, FormADTO formADTO) throws CronJobException;
    Boolean updateFormAUtilization(FormA formA, Remittance remittance) throws CronJobException;

    FormADTO getFormA(Long id) throws CronJobException;
    String deleteForm(Long id) throws CronJobException;
    List<CommentDTO> getComments(Long id) throws CronJobException;

    Page<FormADTO> getFormAs(Pageable pageDetails) throws CronJobException;

    Page<FormADTO> getAllFormAs(Pageable pageDetails);

    Page<FormADTO> getOpenFormAs(Pageable pageable) throws CronJobException;

    Page<FormADTO> getClosedFormAs(Pageable pageable);

    FormADTO editFormA(Long id) throws CronJobException;

    Iterable<FormADTO> getAllFormAs() throws CronJobException;

    FormADTO getInitiatorDetails() throws CronJobException;

     long getTotalNumberOpenFormA() throws CronJobException;

     long getTotalNumberFormA() throws CronJobException;
     Boolean checkAvailableBalance(String amount, String accountId) throws CronJobException;

     List<AccountDTO>  getCustomerNairaAccounts() throws CronJobException;

    List<AccountDTO>  getCustomerAccounts() throws CronJobException;

     List<AccountDTO> getCustomerDomAccounts() throws CronJobException;

     String deleteFormA(Long id) throws CronJobException;

    String addAttachments(MultipartFile file, String formaNumber, String documentType);

    AccountDTO getAccountByAccountNumber(String accountNumber) throws CronJobException;

}
