package cronjobservice.services.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.security.userdetails.CustomUserPrincipal;
import cronjobservice.services.AccountService;
import cronjobservice.services.ConfigurationService;
import cronjobservice.services.CorporateService;
import cronjobservice.services.FormNxpService;
import cronjobservice.utils.FormNumber;
import org.apache.commons.lang3.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by user on 9/21/2017.
 */


@Service
public class FormNxpServiceImpl implements FormNxpService {

    @Autowired
    MessageSource messageSource;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private WorkFlowRepo workFlowRepo;
    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
//    @Autowired
//    WorkFlowStatesService workFlowStatesService;
    @Autowired
    private FormNxpRepo formNxpRepo;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    EntityManager entityManager;
//    @Autowired
//    CommentService commentService;
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    AccountService accountService;

//    @Autowired
//    FormNXPAmendRepo formNXPAmendRepo;

    @Autowired
    FormNxpDocumentRepo formNxpDocumentRepo;

    @Autowired
    CorporateService corporateService;

//    @Autowired
//    UserGroupService userGroupService;

    @Autowired
    BranchRepo branchRepo;

    @Autowired
    CodeRepo codeRepo;

    @Autowired
    CorporateRepo corporateRepo;

    @Value("${bank.code}")
    private String bankCode;


    @Value("${formnxp.upload.files}")
    private String UPLOADED_FOLDER;

    private Locale locale = LocaleContextHolder.getLocale();

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public FormNxpDTO editFormM(Long id) throws CronJobException {
        return null;
    }

    @Override
    public FormNxpDTO getInitiatorDetails() throws CronJobException {

        FormNxpDTO doneBy = new FormNxpDTO();
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        } else if (users.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) users;
            logger.info("corporate user {}", corporateUser.getFirstName());
            doneBy.setExporterName(corporateUser.getCorporate().getName());
            doneBy.setExporterAddress(corporateUser.getCorporate().getAddress());
            doneBy.setExporterPhone(corporateUser.getCorporate().getPhone());
            doneBy.setExporterCountry(corporateUser.getCorporate().getCountry());
            doneBy.setExporterState(corporateUser.getCorporate().getState());
            doneBy.setExporterRcNo(corporateUser.getCorporate().getRcNumber());
            doneBy.setExporterCity(corporateUser.getCorporate().getTown());
        }
//        else if (users.getUserType().equals(UserType.BANK))
//        {
//            BankUser user = (BankUser) users;
//            logger.info("bank user {}",user.getFirstName());
//            doneBy.setExporterName(user.getFirstName());
//            doneBy.setExporterAddress(user.getAddress());
//
//           // doneBy.setExporterFirstName(user.getFirstName());
//        }
        else {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        return doneBy;

    }


//    @Override
//    public String addForm(FormNxpDTO formNxpDTO) throws CronJobException {
//        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                .getPrincipal();
//        User doneBy = principal.getUser();
//        String cif = null;
//        if ((doneBy.getUserType()).equals(UserType.RETAIL)) {
//            RetailUser retailUser = (RetailUser) doneBy;
//            cif = retailUser.getCustomerId();
//        } else if (doneBy.getUserType().equals(UserType.CORPORATE)) {
//            CorporateUser corporateUser = (CorporateUser) doneBy;
//            cif = corporateUser.getCorporate().getCustomerId();
//        } else if (doneBy.getUserType().equals(UserType.BANK)) {
//            cif = formNxpDTO.getCifid();
//        }
//        if (doneBy == null) {
//            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
//        }
//        try {
//            String generatedFormNumber = FormNumber.getFormNxpNumber();
//            if (formNxpDTO.getDocuments() != null) {
//                for (FormNxpAttachmentDTO d : formNxpDTO.getDocuments()) {
//                    if (!(d.getFile().isEmpty())) {
//                        MultipartFile file = d.getFile();
//                        String filename = addAttachments(file, generatedFormNumber, d.getTypeOfDocument());
//                        d.setFileUpload(filename);
//                    }
//                }
//
//
//            }
//
//            FormNxp formNxp = convertDTOTOEntity(formNxpDTO);
//            List<FormNxpItem> itemsList = new ArrayList<>();
//            List<FormItemsDTO> itemsDTOS = formNxpDTO.getItems();
//            FormNxpItem formNxpItem;
//            for (FormItemsDTO formItemsDTO : itemsDTOS) {
//                formNxpItem = modelMapper.map(formItemsDTO, FormNxpItem.class);
//                formNxpItem.setFormnxp(formNxp);
//
//                itemsList.add(formNxpItem);
//            }
//            formNxp.setItems(itemsList);
//            logger.info("MXP ITems {}", formNxp.getItems());
//            formNxp.setStatus(formNxpDTO.getStatus());
//            formNxp.setInitiatedBy(doneBy.getUserName());
//            formNxp.setFormNum(FormNumber.getFormNxpNumber());
//            formNxp.setTempFormNumber(generatedFormNumber);
//            formNxp.setSubmitFlag("U");
//            formNxp.setBankCode(bankCode);
//            formNxp.setExporterCountry("NG");
//            formNxp.setUserType(doneBy.getUserType().toString());
//            formNxp.setCifid(cif);
//            formNxp.setExpiryDate(getFormExpiryDate());
//            Date date = getFormExpiryDate();
//            if (date != null) {
//                formNxp.setExpiryDate(date);
//            }
//            formNxp.setDateCreated(new Date());
//            formNxp.setUtilization("0");
//            logger.info("FOB VALUE 3 {}", formNxp.getItems());
//
//
//            /**
//             * Attachment table
//             */
////            logger.info(formNxpDTO.getBranch());
//            if (formNxpDTO.getDocuments() != null) {
//                if (null != formNxpDTO.getBranch() && !formNxpDTO.getBranch().equalsIgnoreCase("") ) {
//                    System.out.println(formNxpDTO.getBranch());
//
//                    Branch branch = branchRepo.findOne(Long.parseLong(formNxpDTO.getBranch()));
//                    formNxp.setDocSubBranch(branch);
//                }
//                Iterator<FormNxpDocument> DocumentIterator = formNxp.getDocuments().iterator();
//                Iterator<FormNxpAttachmentDTO> DocumentDTOIterator = formNxpDTO.getDocuments().iterator();
//
//                while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
//                    FormNxpDocument document = DocumentIterator.next();
//                    FormNxpAttachmentDTO documentDTO = DocumentDTOIterator.next();
//                    if (documentDTO.getTypeOfDocument() == null) {
//                        DocumentIterator.remove();
//                        DocumentDTOIterator.remove();
//                    } else {
//                        document.setDocumentType(codeRepo.findByTypeAndCode("FORMNXP_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
//                        logger.info("document remarks {}",documentDTO.getDocumentRemark());
//                        document.setDocumentRemark(documentDTO.getDocumentRemark());
//                        document.setFormNxp(formNxp);
//                    }
//                }
//            }
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_NXP_WF");
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            formNxp.setWorkFlow(workFlow);
//            formNxpRepo.save(formNxp);
//
//            logger.info("Form NXP created successfully");
//
//            return messageSource.getMessage("formnxp.add.success", null, locale);
//        } catch (CronJobException te) {
//            throw new CronJobException(messageSource.getMessage("form.add.failure", null, null), te);
//
//        }
//    }

    @Override
    public Boolean addFormFromFinacle(FormNxpDTO formNxpDTO) throws CronJobException {

        String cif = formNxpDTO.getCifid();

        try {

            FormNxp formNxp = modelMapper.map(formNxpDTO, FormNxp.class);
            List<FormNxpItem> itemsList = new ArrayList<>();
            List<FormItemsDTO> itemsDTOS = formNxpDTO.getItems();
            FormNxpItem formNxpItem;
            if (itemsDTOS != null) {
                for (FormItemsDTO formItemsDTO : itemsDTOS) {
                    formNxpItem = modelMapper.map(formItemsDTO, FormNxpItem.class);
                    formNxpItem.setFormnxp(formNxp);
                    itemsList.add(formNxpItem);
                }
                formNxp.setItems(itemsList);
            }

            logger.info("MXP ITems {}", formNxp.getItems());
            formNxp.setStatus(formNxpDTO.getStatus());
            formNxp.setUserType("BANK");
            formNxp.setInitiatedBy("FINACLE");
            formNxp.setTempFormNumber(FormNumber.getFormNxpNumber());
            formNxp.setCifid(cif);
            formNxp.setDateCreated(new Date());
            formNxp.setUtilization(formNxpDTO.getUtilization());


            logger.info("Form amount for nxp added ln finacle {} ",formNxpDTO.getTotalValueOfGoods());
            logger.info("utilization for nxp added ln finacle {} ",formNxpDTO.getUtilization());

            String formAmount = formNxpDTO.getTotalValueOfGoods();
            String utilized = formNxpDTO.getUtilization();
            if(utilized != null && formAmount !=null) {
                int i = formAmount.compareTo(utilized);
                if (utilized.equals(0)) {
                    formNxp.setStatus("R");
                } else if (i > 0) {
                    formNxp.setStatus("PU");
                } else {
                    formNxp.setStatus("U");
                }
            }else
                formNxp.setStatus("R");



            formNxp.setUtilization("0");
            logger.info("FOB VALUE 3 {}", formNxp.getItems());
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_NXP_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            formNxp.setWorkFlow(workFlow);
            formNxpRepo.save(formNxp);

            logger.info("Form NXP created successfully");

            return true;
        } catch (CronJobException te) {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, null), te);

        }
    }

    @Override
    public FormNxpDTO getFormNxp(Long id) throws CronJobException {
        FormNxp formNxp = formNxpRepo.findOne(id);
        return convertEntityTODTO(formNxp);

    }

    @Override
    public FormNxp getFormNum(Long id) throws CronJobException {
        FormNxp formNxp = formNxpRepo.findOne(id);
        logger.info("id formnxp {}", formNxp);
        return formNxp;

    }

    @Override
    public FormNxp getDetForLc(String formNum) throws CronJobException {
        FormNxp formNxp = formNxpRepo.findByFormNum(formNum);
        logger.info("id formnxp {}", formNxp);
        return formNxp;

    }

    @Override
    public FormHolder executeAction(FormNxpDTO formNxpDTO, String name) throws CronJobException {
        return null;
    }

    @Override
    public Page<FormNxpDTO> getFormNxps(Pageable pageDetails) {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        CorporateUser corporateUser = (CorporateUser) users;
        Page<FormNxp> page = formNxpRepo.findByClosedCifidAndStatus(corporateUser.getCorporate().getCustomerId(), pageDetails);
        List<FormNxpDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormNxpDTO> pageImpl = new PageImpl<FormNxpDTO>(dtOs, pageDetails, t);
        return pageImpl;

    }

    @Override
    public String deleteForm(Long id) throws CronJobException {
        FormNxp formNxp = formNxpRepo.findOne(id);

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();

        if (doneBy == null) {
            logger.info("user is null");
            throw new CronJobException(messageSource.getMessage("formnxp.delete.failure", null, locale));
        }
        try {
            if(!formNxp.getStatus().equalsIgnoreCase("P")){
                throw new CronJobException(messageSource.getMessage("formnxp.delete.saved.failure", null, locale));
            }
            formNxpRepo.delete(formNxp);
            return messageSource.getMessage("formnxp.delete.success", null, locale);
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("formnxp.delete.failure", null, locale), e);
        }

    }

    @Override
    public List<FormItemsDTO> getFormItems(Long formId) {
        FormNxp formNxp = formNxpRepo.findOne(formId);
        List<FormNxpItem> fI = formNxp.getItems();
        List<FormItemsDTO> formItems = new ArrayList<FormItemsDTO>();
        for (FormNxpItem formNxpItem : fI) {
            FormItemsDTO formItemsDTO = modelMapper.map(formNxpItem, FormItemsDTO.class);
            formItems.add(formItemsDTO);
        }
        return formItems;
    }

    @Override
    public Page<FormNxp> getApprovedFormNxpDetails(Pageable pageable) throws CronJobException {

        String cif = null;
        Page<FormNxp> formDetails = null;
        formDetails = formNxpRepo.findFormByStatus(pageable);
        return formDetails;
    }

    @Override
    public Page<FormNxp> getFormNxpDetails(Pageable pageable) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        String cif = null;
        Page<FormNxp> formDetails = null;
        if (doneBy.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) doneBy;
            cif = corporateUser.getCorporate().getCustomerId();
            formDetails = formNxpRepo.findFormByCifid(cif, pageable);
        }

        return formDetails;
    }


    //convert Entities to DTOs
    private List<FormNxpDTO> convertEntitiesToDTOs(Iterable<FormNxp> formNxps) {
        List<FormNxpDTO> formNxpDTOs = new ArrayList<FormNxpDTO>();
        for (FormNxp formNxp : formNxps) {
            formNxpDTOs.add(convertEntityTODTO(formNxp));
        }
        return formNxpDTOs;
    }

    //convert Entity to DTOs
    @Override
    public FormNxpDTO convertEntityTODTO(FormNxp formNxp) {
        FormNxpDTO formNxpDTO = modelMapper.map(formNxp, FormNxpDTO.class);

//        formNxpDTO.setProformaInvoiceDate(DateFormatter.format(formNxp.getProformaDate()));
        if (formNxp.getStatus() != null && !(formNxp.getStatus().equals("P"))) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formNxpDTO.getWorkFlow().getCodeType(), formNxpDTO.getStatus());
            formNxpDTO.setStatusDesc(workFlowStates.getDescription());
            formNxpDTO.setWorkFlowId(formNxp.getWorkFlow().getId());
        }
//        if (formNxp.getStatus().equals("P")) {
//            formNxpDTO.setStatusDesc("Saved");
//        }
        if (formNxp.getDateCreated() != null) {
            formNxpDTO.setConvert(formNxp.getDateCreated().toString());
        }

        if (formNxp.getFormNum() == null) {
            formNxpDTO.setFormNum("NOT REGISTERED");
        }
        return formNxpDTO;
    }

    @Override
    public FormNxpDTO convertEntityTODTOFinacle(FormNxp formNxp)
    {
        FormNxpDTO formNxpDTO=modelMapper.map(formNxp , FormNxpDTO.class);

//        formNxpDTO.setProformaInvoiceDate(DateFormatter.format(formNxp.getProformaDate()));
        if(formNxp.getStatus() != null && !(formNxp.getStatus().equals("P")) ){
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formNxpDTO.getWorkFlow().getCodeType(), formNxpDTO.getStatus());
            formNxpDTO.setStatusDesc(workFlowStates.getDescription());
            formNxpDTO.setWorkFlowId(formNxp.getWorkFlow().getId());
        }

        if(formNxp.getDateCreated()!=null){
            formNxpDTO.setConvert(formNxp.getDateCreated().toString());
        }

        return formNxpDTO;
    }

    private FormNxp convertDTOTOEntity(FormNxpDTO formNxpDTO) throws CronJobException
    {
//        try {
//            formNxpDTO.setProformaDate(DateFormatter.parse(formNxpDTO.getProformaInvoiceDate()));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        //   formNxpDTO.setWorkFlowId(formNxpDTO .getWorkFlow().getId());
        WorkFlow workFlow = workFlowRepo.findOne(formNxpDTO.getWorkFlowId());
        formNxpDTO.setWorkFlow(workFlow);
        FormNxp formNxp = modelMapper.map(formNxpDTO, FormNxp.class);

        return formNxp;
    }


    @Override
    public String updateFormNxp(FormNxpDTO formNxpDTO) throws CronJobException {
        FormNxp formNxp = formNxpRepo.findOne(formNxpDTO.getId());

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();

        CorporateUser corporateUser = (CorporateUser) doneBy;
        if (doneBy == null) {
            throw new CronJobException(messageSource.getMessage("form.add.failure", null, locale));
        }


        try {
            logger.info("formnxp service inside the try");
            if (formNxpDTO.getDocuments() != null) {
                for (FormNxpAttachmentDTO d : formNxpDTO.getDocuments()) {
                    if (!(d.getFile().isEmpty())) {
                        logger.info("formNxp service saving the document");
                        MultipartFile file = d.getFile();
                        String filename = addAttachments(file, formNxp.getTempFormNumber(), d.getTypeOfDocument());
                        d.setFileUpload(filename);
                    }
                }

            }
            entityManager.detach(formNxp);
            formNxp = convertDTOTOEntity(formNxpDTO);
            //formNxp.setUserType(doneBy.getUserType().toString());
            formNxp.setInitiatedBy(doneBy.getUserName());
            formNxp.setDateCreated(new Date());
            formNxp.setBankCode(bankCode);
            formNxp.setExpiryDate(getFormExpiryDate());
            formNxp.setExporterCountry("NG");
            formNxp.setSubmitFlag("U");
            //  formNxp.setCifid(corporateUser.getCustomerId());
            System.out.println("formnxp service after setting all forma parameters");
            /**
             * BillsDocument table
             */
            if (formNxpDTO.getDocuments() != null) {
                if (!"".equalsIgnoreCase(formNxpDTO.getBranch()) || !formNxpDTO.getBranch().equalsIgnoreCase("")) {

                    Branch branch = branchRepo.findOne(Long.parseLong(formNxpDTO.getBranch()));
                    formNxp.setDocSubBranch(branch);
                }

                Iterator<FormNxpDocument> DocumentIterator = formNxp.getDocuments().iterator();
                Iterator<FormNxpAttachmentDTO> DocumentDTOIterator = formNxpDTO.getDocuments().iterator();
                Collection<FormNxpDocument> fields = new ArrayList<FormNxpDocument>();
                while (DocumentIterator.hasNext() && DocumentDTOIterator.hasNext()) {
                    FormNxpDocument document = DocumentIterator.next();
                    FormNxpAttachmentDTO documentDTO = DocumentDTOIterator.next();
                    FormNxpDocument formNxpDocument = null;
                    if (documentDTO.getTypeOfDocument() == null) {
                        DocumentIterator.remove();
                        DocumentDTOIterator.remove();
                    } else if (documentDTO.getId() == null) {
                        logger.info("newly added document");
                        formNxpDocument = new FormNxpDocument();
                        logger.info("before model mapper");
                        modelMapper.map(documentDTO, formNxpDocument);
                        logger.info("after model mapper");
                        formNxpDocument.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
                        formNxpDocument.setFormNxp(formNxp);
                        logger.info("newly added document ending");
                    } else {
                        System.out.println("existing document");
                        formNxpDocument = formNxpDocumentRepo.findOne(documentDTO.getId());
                        logger.info("after finding existing document");
                        formNxpDocument.setDocumentValue(documentDTO.getDocumentValue());
                        formNxpDocument.setDocumentRemark(documentDTO.getDocumentRemark());
                        formNxpDocument.setDocumentNumber(documentDTO.getDocumentNumber());
                        formNxpDocument.setDocumentDate(documentDTO.getDocumentDate());
                        if (documentDTO.getFileUpload() != null) {
                            formNxpDocument.setFileUpload(documentDTO.getFileUpload());
                        }
                        formNxpDocument.setDocumentType(codeRepo.findByTypeAndCode("FORMA_DOCUMENT_TYPE", documentDTO.getTypeOfDocument()));
                        formNxpDocument.setFormNxp(formNxp);
                        logger.info("ending of  existing document");

                    }
                    fields.add(formNxpDocument);
                }
                formNxp.setDocuments(fields);
            }
            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_NXP_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            formNxp.setWorkFlow(workFlow);
            formNxpRepo.save(formNxp);
            return messageSource.getMessage("form.update.success", null, locale);
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }


    }

    @Override
    public Boolean updateFormNxpFromFinacle(FormNxp tmsformnxp, FormNxp finformnxp) throws CronJobException {
        try {
            logger.info("This is the form Number {} ", finformnxp.getFormNum());
            tmsformnxp.setFormNum(finformnxp.getFormNum());
            tmsformnxp.setTranId(finformnxp.getTranId());
            tmsformnxp.setStatus("R");
            formNxpRepo.save(tmsformnxp);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

    //Bank Implementation

    @Override
    public Page<FormNxpDTO> getBankFormNxps(Pageable pageable) {

        Page<FormNxp> page = formNxpRepo.findAll(pageable);
        List<FormNxpDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormNxpDTO> pageImpl = new PageImpl<FormNxpDTO>(dtOs, pageable, t);
        return pageImpl;
    }

    @Override
    public FormHolder createFormHolder(FormNxpDTO formNxpDTO, String name) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User doneby = principal.getUser();
        BankUser bankUser = (BankUser) doneby;
        try {
            FormHolder holder = new FormHolder();
            FormNxp formNxp = addComments(formNxpDTO);
            formNxp.setDateCreated(new Date());
            formNxp.setSubmitFlag("U");
            formNxp.setExporterCountry("NG");
            formNxp.setBankCode(bankCode);
            formNxp.setAuthName(bankUser.getFirstName());
//            formNxp.setAuthDate();
            holder.setObjectData(objectMapper.writeValueAsString(formNxp));
            holder.setEntityId(formNxp.getId());
            holder.setEntityName("FormNxp");
            holder.setAction(name);
            logger.info("getting the formNxp");
            return holder;

        } catch (JsonProcessingException e) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }


    }

//


    @Override
    public String getTotalAmt(String cifId) throws CronJobException {

        Integer totalAmt = formNxpRepo.addAmt(cifId, "A");
        logger.info("total Amt {}", totalAmt);

        return "" + totalAmt;
    }

    @Override
    public Page<FormNxpDTO> getOpenFormNxps(Pageable pageable) {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        CorporateUser corporateUser = (CorporateUser) users;
        Page<FormNxp> pages = formNxpRepo.findByCifidAndStatus(corporateUser.getCorporate().getCustomerId(), "P", "S", "D", pageable);
        List<FormNxpDTO> formNxpDTOs = convertEntitiesToDTOs(pages.getContent());
        long t = pages.getTotalElements();
        Page<FormNxpDTO> page1 = new PageImpl<FormNxpDTO>(formNxpDTOs, pageable, t);
        return page1;
    }

    @Override
    public Page<FormNxpDTO> geBankOpenFormNxps(Pageable pageable) {

        Page<FormNxp> pages = formNxpRepo.findNxpOpenForBank("A", "S", "D", pageable);
        List<FormNxpDTO> formNxpDTOs = convertEntitiesToDTOs(pages.getContent());
        long t = pages.getTotalElements();
        Page<FormNxpDTO> page1 = new PageImpl<FormNxpDTO>(formNxpDTOs, pageable, t);
        return page1;
    }

    //Getting Details for Import Bills
//    @Override
//    public FormNxp getDetailForExportLc(String formNum) throws CronJobException {
//        FormNxp formNxp=formNxpRepo.findByFormNum(formNum);
//        logger.info("id formnxp {}",formNxp);
//        return  formNxp;
//
//    }


    @Override
    public long getTotalExpiredProformaFormNxp() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User logginUser = principal.getUser();
        long totalNumberExpiredProformaFormNxp = 0;
        if (logginUser.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) logginUser;
            totalNumberExpiredProformaFormNxp = formNxpRepo.countByCifidAndProformaInvoiceStatus(corporateUser.getCorporate().getCustomerId(), "EXPIRED");
        }

        return totalNumberExpiredProformaFormNxp;
    }

    @Override
    public Page<FormNxpDTO> getListExpiredFormNxp(Pageable pageDetails) {

        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        CorporateUser corporateUser = (CorporateUser) doneBy;
        String cifid = ((CorporateUser) doneBy).getCorporate().getCustomerId();
        Page<FormNxp> page = formNxpRepo.findByCifidAndProformaInvoiceStatus(cifid, "EXPIRED", pageDetails);
        List<FormNxpDTO> formNxpDTOS = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormNxpDTO> page1 = new PageImpl<FormNxpDTO>(formNxpDTOS, pageDetails, t);
        return page1;
    }


    @Override
    public Page<FormNxpDTO> getAllFormNxps(Pageable pageDetails)
    {
        Page<FormNxp> page = formNxpRepo.findByStatus("P", pageDetails);
        List<FormNxpDTO> dtOs = convertEntitiesToDTOs(page.getContent());
        long t = page.getTotalElements();
        Page<FormNxpDTO> pageImpl = new PageImpl<FormNxpDTO>(dtOs, pageDetails, t);
        return pageImpl;
    }


    @Override
    public String addAttachments(MultipartFile file, String formNxpNumber, String documentType) {
        String uploadedFileName = "";


        if (file.isEmpty()) {
            logger.info("file is empty");
        }

        try {
            byte[] bytes = file.getBytes();
            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            User users = principal.getUser();

            if (users.getUserType().equals(UserType.CORPORATE)) {
                CorporateUser corporateUser = (CorporateUser) users;
                String username = corporateUser.getUserName();
                logger.info("corporate user {} with username {}", corporateUser, username);

                Path path = Paths.get(UPLOADED_FOLDER + formNxpNumber + documentType + file.getOriginalFilename());
                logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());


                File f = new File(path.toString());
                if (f.exists()) {
                    System.out.println("file already exist");
                } else {
                    System.out.println("file does not exist");
                }

                Files.write(path, bytes);
                logger.info("file uploaded successfully to {} directory", path);
                uploadedFileName = file.getOriginalFilename();
            } else if (users.getUserType().equals(UserType.BANK)) {
                BankUser bankUser = (BankUser) users;
                String username = bankUser.getUserName();
                logger.info("corporate user {} with username {}", bankUser, username);

                Path path = Paths.get(UPLOADED_FOLDER + formNxpNumber + documentType + file.getOriginalFilename());
                logger.info("creating file in the directory {} with file name {}", UPLOADED_FOLDER, file.getOriginalFilename());


                File f = new File(path.toString());
                if (f.exists()) {
                    System.out.println("file already exist");
                } else {
                    System.out.println("file does not exist");
                }

                Files.write(path, bytes);
                logger.info("file uploaded successfully to {} directory", path);
                uploadedFileName = file.getOriginalFilename();
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("failed to upload file");
        }

        logger.info("uploadFileName {}", uploadedFileName);
        if (StringUtils.isEmpty(uploadedFileName)) {
            logger.info("No file added, Please select a file to upload");

        } else {
            logger.info("You successfully uploaded {}", uploadedFileName);

        }
        return uploadedFileName;
    }

//    @Override
//    public String amendFormNxp(FormNxpDTO formNxpDTO) throws CronJobException {
//        try {
//            FormNxpAmend formNxpAmend = formNXPAmendRepo.findByFormNum(formNxpDTO.getFormNum());
//            if (formNxpAmend != null && ("S".equals(formNxpAmend.getStatus()) || "P".equals(formNxpAmend.getStatus()))) {
//                logger.error("Amendment already exists");
//                throw new CronJobException(messageSource.getMessage("amendmnt exist", null, locale));
//
//            }
//            CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
//                    .getPrincipal();
//            User doneBy = principal.getUser();
//            SettingDTO wfSetting = configurationService.getSettingByName("DEF_FORM_NXP_AMEND_WF");
//            logger.info("setting the workflow {}", wfSetting);
//            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
//            logger.info("workflow {}", workFlow);
//            formNxpDTO.setWorkFlowId(workFlow.getId());
//            logger.info("workflow ID {}", workFlow.getId());
//
//            FormNxpAmend formNxpAmend1 = convertFormNxpDTOToAmendEntity(formNxpDTO);
//            formNxpAmend1.setUserType(doneBy.getUserType().toString());
//            formNxpAmend1.setInitiatedBy(doneBy.getUserName());
//            formNxpAmend1.setDateCreated(new Date());
//            formNxpAmend1.setCifid(formNxpDTO.getCifid());
//            formNxpAmend1.setFormNum(formNxpDTO.getFormNum());
//
//            formNXPAmendRepo.save(formNxpAmend1);
//            logger.info("Amendment Submited for paar with Id {}", formNxpAmend1.getFormNum());
//            return messageSource.getMessage("state.update.success", null, locale);
//        } catch (CronJobException e) {
//            throw e;
//        } catch (Exception e) {
//            throw new CronJobException(messageSource.getMessage("formnxp.update.failure", null, locale), e);
//        }
//    }

//    @Override
//    public FormNxpAmend convertFormNxpDTOToAmendEntity(FormNxpDTO formNxpDTO) {
//        WorkFlow workFlow = workFlowRepo.findOne(formNxpDTO.getWorkFlowId());
//        formNxpDTO.setWorkFlow(workFlow);
//        FormNxpAmend formNxpAmend = modelMapper.map(formNxpDTO, FormNxpAmend.class);
//        return formNxpAmend;
//    }
//
//    @Override
//    public boolean checkIfAmendable(Long id) {
//        FormNxp formNxp = formNxpRepo.findOne(id);
//        if (!"S".equals(formNxp.getStatus()) && !"P".equals(formNxp.getStatus())) {
//            List<FormNxpAmend> formNxpAmendList = formNXPAmendRepo.getByFormNum(formNxp.getFormNum());
//
//            for (FormNxpAmend formNxpAmend : formNxpAmendList) {
//                if ("S".equals(formNxpAmend.getStatus())) {
//                    return false;
//                }
//            }
//            return true;
//        }
//        return false;
//    }

   /* @Override
    public boolean canCancel(Long id) {
        FormM formM = formMRepo.findOne(id);
        if (!"S".equals(formM.getStatus()) && !"P".equals(formM.getStatus())) {

            FormMCancellation formMCancel = formMCancelRepo.findByFormNumberAndCifid(formM.getFormNumber(), formM.getCifid());
            if (formMCancel != null){
                return false;
            }else{
                List<Paar> paars = paarRepo.findByFormM(formM);

                for (Paar paar : paars) {
                    if ("I".equals(paar.getStatus())) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }*/

//    @Override
//    public Page<FormNxpDTO> getFormMAmendments(String formNumber, Pageable pageDetails) {
//        FormNxp formNxp = formNxpRepo.findByFormNum(formNumber);
//        logger.info("formNumber is {}", formNumber);
//        Page<FormNxpAmend> page = formNXPAmendRepo.findByFormNumAndCifid(formNumber, formNxp.getCifid(), pageDetails);
//        List<FormNxpDTO> dtOs = convertAmendEntitiesToFormNxpDTOs(page);
//        long t = page.getTotalElements();
//        Page<FormNxpDTO> pageImpl = new PageImpl<FormNxpDTO>(dtOs, pageDetails, t);
//        return pageImpl;
//    }

//    @Override
//    public List<FormNxpDTO> convertAmendEntitiesToFormNxpDTOs(Iterable<FormNxpAmend> formNxpAmends) {
//        List<FormNxpDTO> formNxpDTOList = new ArrayList<>();
//        for (FormNxpAmend formNxpAmend : formNxpAmends) {
//            FormNxpDTO formNxpDTO = convertAmendEntityToFormNxpDTO(formNxpAmend);
//            formNxpDTOList.add(formNxpDTO);
//        }
//        return formNxpDTOList;
//    }

//    @Override
//    public FormNxpDTO convertAmendEntityToFormNxpDTO(FormNxpAmend formNxpAmend) {
//        FormNxpDTO formNxpDTO = modelMapper.map(formNxpAmend, FormNxpDTO.class);
//        if ("P".equals(formNxpAmend.getStatus())) {
//            formNxpDTO.setStatusDesc("Saved");
//        } else if ("S".equals(formNxpAmend.getStatus())) {
//            formNxpDTO.setStatusDesc("Submitted");
//        } else if ("A".equals(formNxpAmend.getStatus())) {
//            formNxpDTO.setStatusDesc("Approved");
//        } else if ("D".equals(formNxpAmend.getStatus())) {
//            formNxpDTO.setStatusDesc("Declined");
//        }
//        if (formNxpDTO.getDateCreated() != null) {
//            formNxpDTO.setConvert(formNxpDTO.getDateCreated().toString());
//        }
//
//        return formNxpDTO;
//    }

    @Override
    public FormNxpDTO getFormNxpByFormNumber(String formNumber) throws CronJobException {
        FormNxp formNxp = formNxpRepo.findByFormNum(formNumber);
        return convertEntityTODTO(formNxp);

    }

    private FormNxp addComments(FormNxpDTO formNxpDTO) throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User doneBy = principal.getUser();
        logger.info("WORK_FLOW_ID {}", formNxpDTO.getWorkFlowId());
        WorkFlow workFlow = workFlowRepo.findOne(formNxpDTO.getWorkFlowId());
        formNxpDTO.setWorkFlow(workFlow);
        FormNxp singleFormA = formNxpRepo.findOne(formNxpDTO.getId());

        formNxpDTO.setExpiryDate(singleFormA.getExpiryDate());
        FormNxp formNxp = modelMapper.map(formNxpDTO, FormNxp.class);

        if(formNxpDTO.getDocuments() != null){
            System.out.println("model mapper " + formNxp.getDocuments());

            List<FormNxpDocument> l = new ArrayList<>();
            if (!"".equalsIgnoreCase(formNxpDTO.getBranch()) && null != formNxpDTO) {
                System.out.println(formNxpDTO.getBranch());
                Branch branch = branchRepo.findByName(formNxpDTO.getBranch());
                formNxp.setDocSubBranch(branch);
                for (FormNxpAttachmentDTO  f : formNxpDTO.getDocuments()) {
                    FormNxpDocument  formaDocument = formNxpDocumentRepo.findOne(f.getId());
                    System.out.println(f.getId());
                    formaDocument.setFormNxp(formNxp);
                    l.add(formaDocument);
                }
                formNxp.setDocuments(l);
            }
        }


        Comments comments = new Comments();
        comments.setMadeOn(new Date());
        comments.setStatus(formNxpDTO.getSubmitAction());
        comments.setComment(formNxpDTO.getComments());
        comments.setUsername(doneBy.getUserName());
        List<Comments> oldComment = singleFormA.getComment();
        oldComment.add(comments);
        formNxp.setComment(oldComment);
        return formNxp;
    }

    private List<CommentDTO> convertEntitiestoDTOs(Iterable<Comments> commentDTOS) {
        List<CommentDTO> commentDTOList = new ArrayList<>();

        for (Comments comments : commentDTOS) {
            commentDTOList.add(convertEntityToDTO(comments));

        }

        return commentDTOList;
    }

    private CommentDTO convertEntityToDTO(Comments comments) {
        WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode("FORM_NXP", comments.getStatus());
        if (workFlowStates != null) {
            comments.setStatus(workFlowStates.getDescription());
        }
        return modelMapper.map(comments, CommentDTO.class);
    }

    @Override
    public List<CommentDTO> getComments(Long id) throws CronJobException {
        FormNxp formNxp = formNxpRepo.findOne(id);
        List<CommentDTO> commentDTOList = new ArrayList<>();
        if (formNxp != null) {
            commentDTOList = convertEntitiestoDTOs(formNxp.getComment());

        }
        return commentDTOList;
    }

    @Override
    public List<CommentDTO> getComments(FormNxp formNxp) throws CronJobException {
        List<CommentDTO> commentDTOList = new ArrayList<>();

        {
            commentDTOList = convertEntitiestoDTOs(formNxp.getComment());
            commentDTOList.stream()
                    .filter(stat -> stat.getStatus() != "A");


        }
        return commentDTOList;
    }


    @Override
    public List<AccountDTO> getCustomerNairaAccounts() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> nairaAccountList = new ArrayList<>();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        if ((users.getUserType()).equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) users;
            nairaAccountList = accountService.getCustomerNairaAccounts(corporateUser.getCorporate().getCustomerId(), "NGN");

        }
        return nairaAccountList;

    }

    @Override
    public List<AccountDTO> getCustomerDomAccounts() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> domaccountList = new ArrayList<>();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }

        if (users.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) users;
            domaccountList = accountService.getCustomerDomAccounts(corporateUser.getCorporate().getCustomerId(), "NGN");
        }
        return domaccountList;
    }

    @Override
    public List<AccountDTO> getCustomerDomAccountsWithScheme() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> domaccountList = new ArrayList<>();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }

        if (users.getUserType().equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) users;
            domaccountList = accountService.getCustomerDomAccountsAndScheme(corporateUser.getCorporate().getCustomerId(), "NGN", "ODA");
        }
        return domaccountList;
    }


    @Override
    public AccountDTO getAccountByAccountNumber(String accountNumber) {

        if (accountNumber == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));

        }

        AccountDTO accountDTO = accountService.getAccountByAccountNumber(accountNumber);

        return accountDTO;
    }

    @Override
    public FormNxpDTO getFormNxpForView(Long id) throws CronJobException {
        FormNxp formNxp = formNxpRepo.findOne(id);
        FormNxpDTO formNxpDTO = convertEntityTODTOView(formNxp);
//        Iterator<FormNxpAttachment> formNxpAttachmentIterator = formNxp.getDocuments().iterator();
        Iterator<FormNxpDocument> formNxpAttachmentIterator;
        formNxpAttachmentIterator = formNxp.getDocuments().iterator();
        Iterator<FormNxpAttachmentDTO> formNxpAttachmentDTOIterator = formNxpDTO.getDocuments().iterator();

        if (formNxp.getDocSubBranch() != null) {
            formNxpDTO.setBranch(formNxp.getDocSubBranch().getName());
        }

        while (formNxpAttachmentDTOIterator.hasNext() && formNxpAttachmentDTOIterator.hasNext()) {
            FormNxpDocument formNxpAttachment = formNxpAttachmentIterator.next();
            FormNxpAttachmentDTO formNxpAttachmentDTO = formNxpAttachmentDTOIterator.next();
            if (formNxpAttachment.getDocumentType() == null) {
                formNxpAttachmentDTOIterator.remove();
                formNxpAttachmentIterator.remove();
            } else {
                formNxpAttachmentDTO.setTypeOfDocument(formNxpAttachment.getDocumentType().getDescription());
                formNxpAttachmentDTO.setCodedoctype(formNxpAttachment.getDocumentType().getCode());
            }
        }

        return formNxpDTO;
    }

    @Override
    public List<AccountDTO> getCustomerAccounts() throws CronJobException {
        CustomUserPrincipal principal = (CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User users = principal.getUser();
        List<AccountDTO> accountList = new ArrayList<>();

        if (users == null) {
            throw new CronJobException(messageSource.getMessage("form.process.failure", null, locale));
        }

        if ((users.getUserType()).equals(UserType.CORPORATE)) {
            CorporateUser corporateUser = (CorporateUser) users;

            accountList = accountService.getAccounts(corporateUser.getCorporate().getCustomerId());

        }


        return accountList;
    }

    private FormNxpDTO convertEntityTODTOView(FormNxp formNxp) {
        FormNxpDTO formNxpDTO = modelMapper.map(formNxp, FormNxpDTO.class);
        if (formNxpDTO.getStatus() != null) {
            WorkFlowStates workFlowStates = workFlowStatesRepo.findByTypeAndCode(formNxpDTO.getWorkFlow().getCodeType(), formNxpDTO.getStatus());
            formNxpDTO.setStatusDesc(workFlowStates.getDescription());
            formNxpDTO.setWorkFlowId(formNxp.getWorkFlow().getId());
        }
        if (formNxpDTO.getStatus().equals("P")) {
            formNxpDTO.setStatusDesc("Saved");
        }
        if (formNxp.getDateCreated() != null) {
            formNxpDTO.setDateCreated(formNxp.getDateCreated());
        }


        if (formNxp.getDocSubBranch() != null) {
            formNxpDTO.setBranch(formNxp.getDocSubBranch().getId().toString());
        }
        return formNxpDTO;


    }


    @Override
    public BankCustomerDTO getBankCustomerByCustomerId(String customerId) throws CronJobException {
        BankCustomerDTO bankCustomerDTO = null;
        if (customerId != null) {
            List<AccountDTO> customerAccounts = new ArrayList<>();
            List<String> accountNumbers = new ArrayList<>();

            if (corporateRepo.existsByCustomerId(customerId)) {
                System.out.println("CORPORATE CUSTOMER");
                Corporate corporate = corporateRepo.findFirstByCustomerId(customerId);
                System.out.println("CORPORATE CUSTOMER");
                bankCustomerDTO = modelMapper.map(corporate, BankCustomerDTO.class);
                System.out.println("CORPORATE CUSTOMER");
                customerAccounts = accountService.getAccounts(customerId);
                for (AccountDTO accountDTO : customerAccounts) {
                    System.out.println("corporate customer account " + accountDTO.getAccountNumber());
                    accountNumbers.add(accountDTO.getAccountNumber());
                }
                bankCustomerDTO.setAccountNumbers(accountNumbers);
                bankCustomerDTO.setType("CORPORATE");
                bankCustomerDTO.setLabel("Corporate name");
                bankCustomerDTO.setCustomerName(corporate.getName());
                bankCustomerDTO.setRcNumber(corporate.getRcNumber());
                bankCustomerDTO.setPhone(corporate.getPhone());
                bankCustomerDTO.setState(corporate.getState());
                bankCustomerDTO.setTown(corporate.getTown());
                logger.info("city me {}",(corporate.getTown()));

                System.out.println("CORPORATE CUSTOMER");
            } else {
//                bankCustomerDTO.setType("NOT_FOUND");
                return null;
            }
        }
        return bankCustomerDTO;
    }


    private Date getFormExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        SettingDTO setting = configurationService.getSettingByName("FORM_NXP_EXPIRY");
        if (setting != null && setting.isEnabled()) {
            int days = NumberUtils.toInt(setting.getValue());
            calendar.add(Calendar.DAY_OF_YEAR, days);
            return calendar.getTime();
        }
        return null;
    }


}

