package cronjobservice.jobs.remittancejobs;

import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class RemittanceSubmitReponseJob implements Job{
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ApplicationContext contextRemittance = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextRemittance.getBean (CronJobService.class);
        logger.info("Submit remitance response job about starting");
        try {
            logger.info("Submit remitance response job starts");
            cronJobService.saveRunningJob(JobCategory.REMITTANCE_SUBMIT_RESPONSE.toString(),cronJobService.getCurrentExpression(JobCategory.REMITTANCE_SUBMIT_RESPONSE.toString()));
            cronJobService.getRemittanceResponse();
            logger.info("Getting remitance response details from finacle ends");
            cronJobService.updateRunningJob(JobCategory.REMITTANCE_SUBMIT_RESPONSE.toString());
        } catch (CronJobException e) {
            logger.info("remitance response job  stops because {}",e);
            e.printStackTrace();
        }catch (Exception e){
            logger.info("remitance response job  stops because {}",e);
            e.printStackTrace();
        }
    }
}
