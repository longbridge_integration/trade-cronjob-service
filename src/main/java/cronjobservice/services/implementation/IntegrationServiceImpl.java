package cronjobservice.services.implementation;

import com.fasterxml.jackson.databind.node.ObjectNode;

import cronjobservice.api.*;
//import cronjobservice.repositories.FormARepo;
import cronjobservice.dtos.FormMDTO;
import cronjobservice.services.IntegrationService;
import cronjobservice.utils.statement.AccountStatement;
import cronjobservice.utils.statement.TransactionHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

//import longbridge.exception.TransferErrorService;

/**
 * Created by Fortune on 4/4/2017.
 * Modified by Farooq
 */

@Service
public class IntegrationServiceImpl implements IntegrationService {


    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public AccountStatement getAccountStatements(String accountNo, Date fromDate, Date toDate, String tranType, PaginationDetails paginationDetails) {
        return null;
    }

    @Override
    public List<TransactionHistory> getLastNTransactions(String accountNo, String numberOfRecords) {
        return null;
    }

    @Value("${tradeservice.service.uri}")
    private String URI;
    @Value("${CMB.ALERT.URL}")
    private String cmbAlert;

//    @Autowired
//    private FormARepo formARepo;
    @Autowired
    private RestTemplate template;

//    @Autowired
//    private MailService mailService;

//    @Autowired
//    private TemplateEngine templateEngine;
//    @Autowired
//    private ConfigurationService configService;


   // private TransferErrorService errorService;
//
//    @Autowired
//    public IntegrationServiceImpl(RestTemplate template, MailService mailService, TemplateEngine templateEngine
//            , ConfigurationService configService,
//                                  TransferErrorService errorService) {
//        this.template = template;
//        this.mailService = mailService;
//        this.templateEngine = templateEngine;
//        this.configService = configService;
//        this.errorService = errorService;
//    }


    @Override
    public AccountInfo fetchAccount(String accountNumber) {
        //TODO
        AccountInfo accountInfo = new AccountInfo();
        return accountInfo;
    }


    @Override
    public List<AccountInfo> fetchAccountsByCifId(String cifId) {
        String uri = URI + "/api/customer/" + cifId + "/accounts";
        logger.info("This is the Accounts By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            System.out.println("");
            List<AccountInfo> listAccounts = Arrays.stream(template.getForObject(uri, AccountInfo[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of All Accounts {}", listAccounts.size());
            return listAccounts;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Accounts{}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<AccountInfo> fetchDollarAccountsByCifId(String cifId) {
        String uri = URI + "/api/customer/" + cifId + "/accounts";
        logger.info("This is the Dom Accounts By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            List<AccountInfo>  dollarAccounts= new ArrayList<>();
            List<AccountInfo> listAccounts = Arrays.stream(template.getForObject(uri, AccountInfo[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Dom Accounts {}", listAccounts.size());
            for (AccountInfo accountInfo : listAccounts){
                if (accountInfo.getAccountCurrency().equalsIgnoreCase("USD")) {
                    dollarAccounts.add(accountInfo);
                }
            }
            return dollarAccounts;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Accounts{}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<AccountInfo> fetchNairaAccountsByCifId(String cifId) {
        String uri = URI + "/api/customer/" + cifId + "/accounts";
        logger.info("This is the Naira Accounts By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            List<AccountInfo>  nairaAccounts= new ArrayList<>();
            List<AccountInfo> listAccounts = Arrays.stream(template.getForObject(uri, AccountInfo[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Naira Accounts {}", listAccounts.size());
            for (AccountInfo accountInfo : listAccounts){
                if (accountInfo.getAccountCurrency().equalsIgnoreCase("NGN")) {
                    nairaAccounts.add(accountInfo);
                }
            }
            return nairaAccounts;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Accounts{}", e);
            return new ArrayList<>();
        }
    }


    @Override
    public List<ExchangeRate> getExchangeRate() {
        try {
            String uri = URI + "/api/forex";
            logger.info("This is the Exchange Rate URL {}", uri);
            return Arrays.stream(template.getForObject(uri, ExchangeRate[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching exchange rate{}", e);
            e.printStackTrace();
            return new ArrayList<>();
        }
    }


        @Override
    public AccountDetails getAccountDetails(String acctNo) {
        String uri = URI + "/api/account/" + acctNo + "/details";
            logger.info("This is the Accounts Details URL {}", uri);
            Map<String, String> params = new HashMap<>();
        params.put("acctNo", acctNo);
        try {
            AccountDetails details = template.getForObject(uri, AccountDetails.class, params);
            logger.info("This is the Account Name {}", details.getAccountName());
            return details;
        } catch (Exception e) {
            logger.error("Exception occurred fetching Account Details {}", e);
            return new AccountDetails();
        }

    }

    @Override
    public CustomerDetails getCustomerDetailsByCif(String cifId) {
        CustomerDetails result = new CustomerDetails();
        String uri = URI + "/api/customer/" + cifId + "/detail";
        logger.info("This is the Customer Details By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId",cifId);
        try {
            result = template.getForObject(uri, CustomerDetails.class, params);
            return result;
        }
        catch (Exception e)
        {
            logger.error("Exception occurred fetching Customer Details {}", e);
            e.printStackTrace();
        }
        logger.info("This is the Customer Name {}", result.getCustomerName());
        return result;
    }
    @Override
    public String getCustomerBvnByCif(String cifId) {
        String result = "";
        String uri = URI + "/api/customer/" + cifId + "/bvn";
        logger.info("This is the Customer Details By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId",cifId);
        try {
            result = template.getForObject(uri, String.class, params);
            return result;
        }
        catch (Exception e)
        {
            logger.error("Exception occurred fetching Customer bvn {}", e);
            e.printStackTrace();
        }
        logger.info("This is the Customer bvn {}", result);
        return result;
    }

    @Override
    public List<TradeCodes> getPortCodes() {

        try {
            String uri = URI + "/api/ports";
            logger.info("This is the Port Code URL {}", uri);
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching Port Codes{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<TradeCodes> getHSCodes() {

        try {
            String uri = URI + "/api/hscode";
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching Port Codes{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<TradeCodes> getSectorialPurposeCodes() {

        try {
            String uri = URI + "/api/sectcode";
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching Sectorial Purpose Code{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<TradeCodes> getModeOfTransport() {

        try {
            String uri = URI + "/api/transportmode";
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching Mode Of Transport{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<TradeCodes> getPaymentModeCode() {

        try {
            String uri = URI + "/api/paymentmode";
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching Mode Of Payment{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<TradeCodes> getPurposeOfPayment() {

        try {
            String uri = URI + "/api/paymentpurpose";
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching Purpose of payment{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<NostroAccount> getNostroAccounts() {

        try {
            String uri = URI + "/api/nostroaccount";
            return Arrays.stream(template.getForObject(uri, NostroAccount[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching nostroAccounts{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<TradeCodes> getCurrencyCode() {

        try {
            String uri = URI + "/api/currencycode";
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching currencycode{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<TradeCodes> getCountryCode() {

        try {
            String uri = URI + "/api/countrycode";
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching country code{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<TradeCodes> getStateCode() {
        try {
            String uri = URI + "/api/statecode";
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching state code{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<TradeCodes> getCityCode() {
        try {
            String uri = URI + "/api/citycode";
            return Arrays.stream(template.getForObject(uri, TradeCodes[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching city code{}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<FinancialInstitution> getFinancialInstitution() {

        try {
            String uri = URI + "/api/financialinstitution";
            List<FinancialInstitution> financialInstitutions = Arrays.stream(template.getForObject(uri, FinancialInstitution[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of All Financial Institution {}", financialInstitutions.size());
            return financialInstitutions;
                } catch (Exception e) {
            logger.error("Exception occurred fetching financial Institution {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public  List<BranchCode> getBranch(String bankCode) {
        String uri = URI + "/api/" + bankCode + "/branchcode";
        logger.info("This is the Branch Code URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("bankCode", bankCode);
        try {
            List<BranchCode> branchCodes = Arrays.stream(template.getForObject(uri, BranchCode[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of All Branch Codes {}", branchCodes.size());
            return branchCodes;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching branch details {}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<SwiftCode> getSwiftCodes() {

        try {
            String uri = URI + "/api/swiftcode";
            return Arrays.stream(template.getForObject(uri, SwiftCode[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching Swift Codes{}", e);
            return new ArrayList<>();
        }
    }

        @Override
    public Map<String, BigDecimal> getBalance(String accountId)
    {

        Map<String, BigDecimal> response = new HashMap<>();
        try {
            AccountDetails details = getAccountDetails(accountId);
            BigDecimal availBal = new BigDecimal(details.getAvailableBalance());
            BigDecimal ledgBal = new BigDecimal(details.getLedgerBalance());
            response.put("AvailableBalance", availBal);
            response.put("LedgerBalance", ledgBal);

            return response;
        } catch (Exception e) {
            response.put("AvailableBalance", new BigDecimal(0));
            response.put("LedgerBalance", new BigDecimal(0));
            e.printStackTrace();
            return response;
        }
    }

    @Override
    public ExchangeRate getRateCode(String formCurrency, String toCurrency, String rateCode) {
        String uri = URI + "/api/" +formCurrency+ "/" +toCurrency+ "/" +rateCode+ "/ratecode";
        logger.info("This is the Rate Code URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("formCurrency", formCurrency);
        params.put("toCurrency", toCurrency);
        params.put("rateCode", rateCode);
        try {
            ExchangeRate exchangeRate = template.getForObject(uri, ExchangeRate.class, params);
            logger.info("This is the Exchange Rate Amount {}", exchangeRate.getAmountValue());
            return exchangeRate;
        } catch (Exception e) {
            return new ExchangeRate();
        }

    }

    @Override
    public List<FormMDetails> getAllFormMs() {
        try {
            String uri = URI + "/api/formm/all";
            return Arrays.stream(template.getForObject(uri, FormMDetails[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching FOrmm M{}", e);
            return new ArrayList<>();
        }    }


    @Override
    public List<FormADetails> getAllFormAs() {
        try {
            String uri = URI + "/api/forma/all";
            return Arrays.stream(template.getForObject(uri, FormADetails[].class)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred fetching form A{}", e);
            return new ArrayList<>();
        }    }

    @Override
    public String getFormMCharge() {

        try {
            String uri = URI + "/api/formm/charge";
            String formMCharges = template.getForObject(uri,String.class);
            return formMCharges;
        } catch (Exception e) {
            logger.error("Exception occurred fetching Port Codes{}", e);
            return new String();
        }
    }

    @Override
    public Boolean checkApplicationNumber(String applicationNumber) {
        Boolean response;
        try {
            String uri = URI + "/api/" +applicationNumber+ "/applnumber";
            logger.info("This is the Application Number URL {}", uri);
            Map<String, String> params = new HashMap<>();
            params.put("applicationNumber", applicationNumber);
            int applNum = template.getForObject(uri,Integer.class);
                if (applNum>0){
                    response = true;
                }else {
                    response = false;
                }
            return response;
        } catch (Exception e) {
            logger.error("Exception occurred fetching Application Num status {} ", e);
            return false;
        }
    }

    @Override
    public Boolean checkLcNumber(String lcNumber) {
        Boolean response;
        try {
            String uri = URI + "/api/loc/" +lcNumber+ "/lcnumber";
            logger.info("This is the LC Number URL {}", uri);
            Map<String, String> params = new HashMap<>();
            params.put("lcNumber", lcNumber);
            int lcNum = template.getForObject(uri,Integer.class);
            if (lcNum>0){
                response = true;
            }else {
                response = false;
            }
            return response;
        } catch (Exception e) {
            logger.error("Exception occurred fetching lc Num status {} ", e);
            return false;
        }
    }

    @Override
    public List<FormADetails> getFormAByCifId(String cifId) {
        String uri = URI + "/api/forma/" +cifId + "/all";
        logger.info("This is the Form As By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            List<FormADetails> formADetails = Arrays.stream(template.getForObject(uri, FormADetails[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Form As Cifid {}", formADetails.size());
            return formADetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormADetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormMDetails> getFormMByCifId(String cifId) {
        logger.info("This is the Form M By Cifid {} ", cifId);
        String uri = URI + "/api/formm/" +cifId + "/all";
        logger.info("This is the Form M By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            List<FormMDetails> formMDetails = Arrays.stream(template.getForObject(uri, FormMDetails[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Form Ms Cifid {}", formMDetails.size());
            return formMDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormMDetails {}", e);
            return new ArrayList<>();
        }
    }


    @Override
    public List<FormMDetails> getAllFormMAddedOmFinacle() {
        String uri = URI + "/api/formm/allformm";
        logger.info("This is the Form M fetch all URL {}", uri);
        try {
            List<FormMDetails> formMDetails = Arrays.stream(template.getForObject(uri, FormMDetails[].class)).collect(Collectors.toList());
//            List<FormMDetails> formMDetails = Arrays.stream(template.getForObject(uri, FormMDetails[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Form Ms fetched {}", formMDetails.size());
            return formMDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormMDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormMDetails> getFormMByFormNums(List<String> cifId) {
        String uri = URI + "/api/formm/" +cifId + "/all";
        logger.info("This is the Form M By Cifid URL {}", uri);
        Map<String, List<String>> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            List<FormMDetails> formMDetails = Arrays.stream(template.getForObject(uri, FormMDetails[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Form Ms Cifid {}", formMDetails.size());
            return formMDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormMDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormNxpDetails> getFormNxpByCifId(String cifId) {
        String uri = URI + "/api/formnxp/" +cifId + "/all";
        logger.info("This is the Form NXP By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            List<FormNxpDetails> formNxpDetails = Arrays.stream(template.getForObject(uri, FormNxpDetails[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Form NXPs Cifid {}", formNxpDetails.size());
            return formNxpDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormNxpDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<RemittanceDetails> getRemittanceByCifId(String cifId) {
        String uri = URI + "/api/remittance/" +cifId+ "/all";
        logger.info("This is the Remittance By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            List<RemittanceDetails> remittanceDetails = Arrays.stream(template.getForObject(uri, RemittanceDetails[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Remittances Cifid {}", remittanceDetails.size());
            return remittanceDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching RemittanceDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<PaarDetails> getPaarByCifId(String cifId) {
        String uri = URI + "/api/paar/" +cifId+ "/all";
        logger.info("This is the Paar By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            List<PaarDetails> paarDetails = Arrays.stream(template.getForObject(uri, PaarDetails[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Paars Cifid {}", paarDetails.size());
            return paarDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching RemittanceDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormADetails> getFormAAllocated() {
        String uri = URI + "/api/forma/allocation";
        try {
            List<FormADetails> formADetails = Arrays.stream(template.getForObject(uri, FormADetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form A Allocation fetched {}", formADetails.size());
            return formADetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormADetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormADetails> getFormAAddedOnFinacle() {
        String uri = URI + "/api/forma/add";
        try {
            List<FormADetails> formADetails = Arrays.stream(template.getForObject(uri, FormADetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form As Added On Finacle {}", formADetails.size());
            return formADetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormADetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormQDetails> getFormQAddedOnFinacle() {
        String uri = URI + "/api/formq/add";
        try {
            List<FormQDetails> formQDetails = Arrays.stream(template.getForObject(uri, FormQDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form Qs Added On Finacle {}", formQDetails.size());
            return formQDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormQ Details from Finacle {}", e);
            return new ArrayList<>();
        }
    }


    @Override
    public List<FormQDetails> getFormQAllocation() {
        String uri = URI + "/api/formq/allocation";
        try {
            List<FormQDetails> formQDetails = Arrays.stream(template.getForObject(uri, FormQDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form Q Allocation fetched {}", formQDetails.size());
            return formQDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Form QDetails  {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<ImportBillDetails> getImportBillsUnderLcAddedOnFinacle() {
        String uri = URI + "/api/bills/import/underlc";
        try {
            List<ImportBillDetails> importBillDetails = Arrays.stream(template.getForObject(uri, ImportBillDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Import Bill Under Lc Added On Finacle {}", importBillDetails.size());
            return importBillDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Import Bill Under Lc  {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<ImportBillDetails> getImportBillsUForCollectionddedOnFinacle() {
        String uri = URI + "/api/bills/import/forcollection";
        try {
            List<ImportBillDetails> importBillDetails = Arrays.stream(template.getForObject(uri, ImportBillDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Import Bill for collection Added On Finacle {}", importBillDetails.size());
            return importBillDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Import Bill for collection   {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormNxpDetails> getFormNxpAddedOnFinacle() {
        String uri = URI + "/api/formnxp/add";
        try {
            List<FormNxpDetails> formNxpDetails = Arrays.stream(template.getForObject(uri, FormNxpDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form NXPs Added On Finacle {}", formNxpDetails.size());
            return formNxpDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormNxpDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormMDetails> getFormMAddedOnFinacle() {
        String uri = URI + "/api/formm/allformm";
        try {
            List<FormMDetails> formMDetails = Arrays.stream(template.getForObject(uri, FormMDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form Ms Added On Finacle {}", formMDetails.size());
            return formMDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormADetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<RemittanceDetails> getRemittanceIssuedOnFinacle() {
        String uri = URI + "/api/remittance/add";
        try {
            List<RemittanceDetails> remittanceDetails = Arrays.stream(template.getForObject(uri, RemittanceDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Remittances Added On Finacle {}", remittanceDetails.size());
            return remittanceDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Remittance {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<RemittanceDetails> getFundTransferIssuedOnFinacle() {
        String uri = URI + "/api/remittance/fundtransfer";
        try {
            List<RemittanceDetails> remittanceDetails = Arrays.stream(template.getForObject(uri, RemittanceDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Fund Transfer Added On Finacle {}", remittanceDetails.size());
            return remittanceDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Fund Transfer Added on Finacle {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormADetails> getFormASubmitResponse() {
        String uri = URI + "/api/forma/submit/response";
        try {
            List<FormADetails> formADetails = Arrays.stream(template.getForObject(uri, FormADetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form A Submit Response {}", formADetails.size());
            return formADetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormADetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormNxpDetails> getFormNxpSubmitResponse() {
        String uri = URI + "/api/formnxp/submit/response";
        try {
            List<FormNxpDetails> formNxpDetails = Arrays.stream(template.getForObject(uri, FormNxpDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form NXP Submit Response {}", formNxpDetails.size());
            return formNxpDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormNxpDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<RemittanceDetails> getRemittanceIssueResponse() {
        String uri = URI + "/api/remittance/submit/response";
        try {
            List<RemittanceDetails> remittanceDetails = Arrays.stream(template.getForObject(uri, RemittanceDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Remittance Submit Response {}", remittanceDetails.size());
            return remittanceDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Remittance {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<RemittanceDetails> getRemittanceFormAIssueResponse() {
        String uri = URI + "/api/remittance/submit/formaresponse";
        try {
            List<RemittanceDetails> remittanceDetails = Arrays.stream(template.getForObject(uri, RemittanceDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Remittance Submit Forma Response {}", remittanceDetails.size());
            return remittanceDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Remittance {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public String getFormMStatusUpdate(String formNumber) {
        try {
            String uri = URI + "/api/formm/" + formNumber + "/status";
            logger.info("This is Form M Status Update URL {} ", uri);
            String formmStatus = template.getForObject(uri,String.class);
            logger.info("This is Form M Status {}", formmStatus);
            return formmStatus;
        } catch (Exception e) {
            logger.error("Exception occurred getting status {}", e);
            return "";
        }
    }

    @Override
    public List<GuaranteeDetails> getOutwardGuaranteeSubmitResponse() {
        String uri = URI + "/api/guarantee/outward/submitresponse";
        try {
            List<GuaranteeDetails> guaranteeDetails = Arrays.stream(template.getForObject(uri, GuaranteeDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Outward Guarantee Submit Response {}", guaranteeDetails.size());
            return guaranteeDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching OutwardGuarantee {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<GuaranteeDetails> getInwardGuaranteeAddByCifId(String cifId) {
        String uri = URI + "/api/guarantee/inward/" +cifId+ "/all";
        logger.info("This is the Inward Guarantee By Cifid URL {}", uri);
        Map<String, String> params = new HashMap<>();
        params.put("cifId", cifId);
        try {
            List<GuaranteeDetails> guaranteeDetails = Arrays.stream(template.getForObject(uri, GuaranteeDetails[].class, params)).collect(Collectors.toList());
            logger.info("This is the Number Of Inward Guarantees By Cifid {}", guaranteeDetails.size());
            return guaranteeDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching GuaranteeDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<GuaranteeDetails> getInwardGuaranteeAddedOnFinacle() {
        String uri = URI + "/api/guarantee/inward/add";
        try {
            List<GuaranteeDetails> guaranteeDetails = Arrays.stream(template.getForObject(uri, GuaranteeDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Inward Guarantees Added On Finacle {}", guaranteeDetails.size());
            return guaranteeDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Inward GuaranteeDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<LCDetails> getImportLcAddedOnFinacle() {
        String uri = URI + "/api/loc/import/addedonfinacle";
        try {
            List<LCDetails> lcDetails = Arrays.stream(template.getForObject(uri, LCDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Import Lc Added On Finacle {}", lcDetails.size());
            return lcDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Import Lc Details {}", e);
            return new ArrayList<>();
        }
    }


    @Override
    public List<GuaranteeDetails> getOutwardGuaranteeAddedOnFinacle() {
        String uri = URI + "/api/guarantee/outward/addedonfinacle";
        try {
            List<GuaranteeDetails> guaranteeDetails = Arrays.stream(template.getForObject(uri, GuaranteeDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Outward Guarantees Added On Finacle {}", guaranteeDetails.size());
            return guaranteeDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching outward GuaranteeDetails added on finacle {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<FormMDetails> getFormMAllocated() {
        String uri = URI + "/api/formm/allocation";
        try {
            List<FormMDetails> formMDetails = Arrays.stream(template.getForObject(uri, FormMDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form M Allocated {}", formMDetails.size());
            return formMDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormMDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<LCDetails> getImportLcSubmitResponse() {
        String uri = URI + "/api/loc/import/issueresponse";
        try {
            List<LCDetails> lcDetails = Arrays.stream(template.getForObject(uri, LCDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Import LC Submit Response {}", lcDetails.size());
            return lcDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching import LCDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<LCDetails> getExportLcSubmitResponse() {
        String uri = URI + "/api/loc/export/adviseresponse";
        try {
            List<LCDetails> lcDetails = Arrays.stream(template.getForObject(uri, LCDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Export LC Submit Response {}", lcDetails.size());
            return lcDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching export LCDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<LCDetails> getExportLcPendingtResponse() {
        String uri = URI + "/api/loc/export/pendingresponse";
        try {
            List<LCDetails> lcDetails = Arrays.stream(template.getForObject(uri, LCDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Export LC pending Response {}", lcDetails.size());
            return lcDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching export pending responseLCDetails {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<LCDetails> getExportLcPendingResponse(){

        String uri = URI + "/api/loc/export/advisependingresponse";
        try {
            List<LCDetails> lcDetails = Arrays.stream(template.getForObject(uri, LCDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Export LC Pending Response {}", lcDetails.size());
            return lcDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching export LCDetails {}", e);
            return new ArrayList<>();
        }

    }


    @Override
    public List<LCDetails> updateExportLcPendingVerificationToAdvised(){

        String uri = URI + "/api/loc/export/updatependingverificationtoadvised";
        try {
            List<LCDetails> lcDetails = Arrays.stream(template.getForObject(uri, LCDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Export LC Pending  verification  to advised Response {}", lcDetails.size());
            return lcDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching export LCDetails form verification to advised {}", e);
            return new ArrayList<>();
        }

    }


    @Override
    public List<LCDetails> getExportLcAddedOnFinacle() {
        String uri = URI + "/api/loc/export/adviseadd";
        try {
            List<LCDetails> lcDetails = Arrays.stream(template.getForObject(uri, LCDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Export LCs Added On Finacle {}", lcDetails.size());
            return lcDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Export Lc details added on finacle {}", e);
            return new ArrayList<>();
        }
    }

    public List<String> getShipmentTerms(){
        String uri = URI + "/api/shipterms";
        try {
            List<String> shipment = Arrays.stream(template.getForObject(uri, String[].class)).collect(Collectors.toList());
            logger.info("This is the List of String ShipTerms {}", shipment);
            return shipment;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching List of String ShipTerms {}", e.getMessage());
            return new ArrayList<>();
        }
    }


    @Override
    public List<BtaDetails> getBtaSubmitResponse() {
        String uri = URI + "/api/bta/submit/response";
        try {
            List<BtaDetails> btaDetails = Arrays.stream(template.getForObject(uri, BtaDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Bta Submit Response {}", btaDetails.size());
            return btaDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching BTA Details {}", e);
            return new ArrayList<>();
        }
    }


    @Override
    public List<PtaDetails> getPtaSubmitResponse() {
        String uri = URI + "/api/pta/submit/response";
        try {
            List<PtaDetails> ptaDetails = Arrays.stream(template.getForObject(uri, PtaDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Pta Submit Response {}", ptaDetails.size());
            return ptaDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching PTA Details {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<ExportBillsDetails> getLodgeBillForCollectionResponse() {
        String uri = URI + "/api/bills/export/forcollection//lodge/response";
        try {
            List<ExportBillsDetails> exportBillsDetails = Arrays.stream(template.getForObject(uri, ExportBillsDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Lodge Bill For collection Response {}", exportBillsDetails.size());
            return exportBillsDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Export Bills Details For collection {}", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<ExportBillsDetails> getLodgeBillUnderLcResponse() {
        String uri = URI + "/api/bills/export/underlc/lodge/response";
        try {
            List<ExportBillsDetails> exportBillsDetails = Arrays.stream(template.getForObject(uri, ExportBillsDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Lodge Bill under lc  Response {}", exportBillsDetails.size());
            return exportBillsDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching Export Bills Details under lc  {}", e);
            return new ArrayList<>();
        }
    }


    @Override
    public List<FormQDetails> getFormQSubmitResponse() {
        String uri = URI + "/api/formq/submit/response";
        try {
            List<FormQDetails> formQDetails = Arrays.stream(template.getForObject(uri, FormQDetails[].class)).collect(Collectors.toList());
            logger.info("This is the Number Of Form Q Submit Response {}", formQDetails.size());
            return formQDetails;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred fetching FormQ Details from Finacle {}", e);
            return new ArrayList<>();
        }
    }


//    @Override
//    public List<FormMDetails> getFormMSubmitResponseOn() {
//        String uri = URI + "/api/formm/submi";
//        try {
//            List<FormMDetails> formMDetails = Arrays.stream(template.getForObject(uri, FormMDetails[].class)).collect(Collectors.toList());
//            logger.info("this is the Striiiinggggg {}", formMDetails);
//            return formMDetails;
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.error("Exception occurred fetching FormADetails {}", e.getMessage());
//            return new ArrayList<>();
//        }
//    }


    //************************  working ones *****************//


//    @Override
//    public List<AccountInfo> fetchAccounts(String cifid) {
//        return null;
//    }
//
//    @Override
//    public List<AccountInfo> fetchDummyDollarAccounts(String cifid) {
//        try {
//
//            AccountInfo accountInfo=new AccountInfo();
//            List<AccountInfo> accountDetails=new ArrayList<>();
//            for(int i=0; i<2; i++)
//            {
//                accountInfo.setAccountNumber("C3211009");
//                accountInfo.setAccountCurrency("USD");
//                accountInfo.setCustomerId("C0008541");
//                accountInfo.setAccountStatus("A");
//                accountInfo.setAccountName("chioma chukelu");
//                accountInfo.setAvailableBalance("20000");
//                accountDetails.add(accountInfo);
//            }
//
//            return  accountDetails;
//        } catch (Exception e) {
//            logger.error("Exception occurred {}", e.getMessage());
//            return new ArrayList<>();
//        }
//    }
//
//    @Override
//    public List<AccountInfo> fetchDummyNairaAccounts(String cifid) {
//        try {
//
//            AccountInfo accountInfo=new AccountInfo();
//            List<AccountInfo> accountDetails=new ArrayList<>();
//            for(int i=0; i<2; i++)
//            {
//                accountInfo.setAccountNumber("201234311");
//                accountInfo.setAccountCurrency("NGN");
//                accountInfo.setCustomerId("C0008541");
//                accountInfo.setAccountStatus("A");
//                accountInfo.setAccountName("chioma chukelu");
//                accountInfo.setAvailableBalance("200000");
//                accountDetails.add(accountInfo);
//            }
//
//            return  accountDetails;
//        } catch (Exception e) {
//            logger.error("Exception occurred {}", e.getMessage());
//            return new ArrayList<>();
//        }
//    }
//
//    @Override
//    public ExchangeRate getCurrentExchangeRate(String fromCurrency, String toCurrency, String rateCode) {
//       try{
//            ExchangeRate exchangeRate= new ExchangeRate();
//            exchangeRate.setAmountValue("306");
//            exchangeRate.setCurrencyCode(fromCurrency);
//           return exchangeRate;
//       }catch (Exception e){
//           logger.error("Exception occurred {}", e.getMessage());
//           return null;
//       }
//
//    }
//
//
//
//
//
////    @Override
////    public AccountStatement getAccountStatements(String accountNo, Date fromDate, Date toDate, String tranType, PaginationDetails paginationDetails) {
////        AccountStatement statement = new AccountStatement();
////        try {
////            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
////       /* String dateInString = "7-Jun-2013";
////        Date date = formatter.parse(dateInString);*/
////            String uri = URI + "/statement";
////            Map<String, Object> params = new HashMap<>();
////            params.put("accountNumber", accountNo);
////            params.put("fromDate", formatter.format(fromDate));
////            params.put("solId", viewAccountDetails(accountNo).getSolId());
////            if (tranType != null)
////                params.put("tranType", tranType);
////            if (toDate != null) params.put("toDate", formatter.format(toDate));
////            if (paginationDetails != null) params.put("paginationDetails", paginationDetails);
////
////
////            statement = template.postForObject(uri, params, AccountStatement.class);
////
////
////        } catch (Exception e) {
////
////        }
////
////
////        return statement;
////    }
    @Override
    public AccountStatement getAccountStatements(String accountNo, Date fromDate, Date toDate, String tranType) {
        AccountStatement statement = new AccountStatement();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
       /* String dateInString = "7-Jun-2013";
        Date date = formatter.parse(dateInString);*/
            String uri = URI + "/statement";
            Map<String, Object> params = new HashMap<>();
            params.put("accountNumber", accountNo);
            params.put("fromDate", formatter.format(fromDate));
            params.put("solId", getAccountDetails(accountNo).getSolId());
            if (tranType != null)
                params.put("tranType", tranType);
            if (toDate != null) params.put("toDate", formatter.format(toDate));



            statement = template.postForObject(uri, params, AccountStatement.class);


        } catch (Exception e) {

        }


        return statement;
    }
////    @Override
////    public List<TransactionHistory> getLastNTransactions(String accountNo, String numberOfRecords) {
////        List<TransactionHistory> histories = new ArrayList<>();
////
////
////        try {
////
////            String uri = URI + "/transactions";
////            Map<String, String> params = new HashMap<>();
////            params.put("accountNumber", accountNo);
////            params.put("numberOfRecords", numberOfRecords);
////            params.put("branchId", viewAccountDetails(accountNo).getSolId());
////
////
////            TransactionHistory[] t = template.postForObject(uri, params, TransactionHistory[].class);
////            histories.addAll(Arrays.asList(t));
////
////
////        } catch (Exception e) {
////       e.printStackTrace();
////        }
////
////
////        return histories;
////    }
//
//
//    @Override
//    public Map<String, BigDecimal> getDummyBalance(String accountId) {
//        Map<String,BigDecimal> response=new HashMap<>();
//        try {
//
//            response.put("availablebalance",new BigDecimal(8000));
//          }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        return response;
//    }
//
//    @Override
//    public Map<String, BigDecimal> getBalance(String accountId)
//    {
//
//        Map<String, BigDecimal> response = new HashMap<>();
//        try {
//            AccountDetails details = getAccountDetails(accountId);
//            BigDecimal availBal = new BigDecimal(details.getAvailableBalance());
//            BigDecimal ledgBal = new BigDecimal(details.getLedgerBalance());
//            response.put("AvailableBalance", availBal);
//            response.put("LedgerBalance", ledgBal);
//
//            return response;
//        } catch (Exception e) {
//            response.put("AvailableBalance", new BigDecimal(0));
//            response.put("LedgerBalance", new BigDecimal(0));
//            e.printStackTrace();
//            return response;
//        }
//    }
//
//    @Override
//    public TransRequest makeTransfer(TransRequest transRequest) throws TradeTransferException {
//
//        TransferType type = transRequest.getTransferType();
//
//        //switch (type) {
//        switch (type) {
//            case CORONATION_BANK_TRANSFER:
//
//            {
//                transRequest.setTransferType(TransferType.CORONATION_BANK_TRANSFER);
//                TransferDetails response;
//                String uri = URI + "/transfer/local";
//                Map<String, String> params = new HashMap<>();
//                params.put("debitAccountNumber", transRequest.getCustomerAccountNumber());
//                params.put("creditAccountNumber", transRequest.getBeneficiaryAccountNumber());
//                params.put("tranAmount", transRequest.getAmount().toString());
//                params.put("naration", transRequest.getNarration());
//                logger.info(params.toString());
//
//                try {
//                    response = template.postForObject(uri, params, TransferDetails.class);
//
//
//                    transRequest.setStatus(response.getResponseCode());
//                    transRequest.setStatusDescription(response.getResponseDescription());
//                    transRequest.setReferenceNumber(response.getUniqueReferenceCode());
//                    transRequest.setNarration(response.getNarration());
//
//
//                    return transRequest;
//
//                } catch (HttpStatusCodeException e) {
//
//                    e.printStackTrace();
//                    transRequest.setStatus(e.getStatusCode().toString());
//                    transRequest.setStatusDescription(e.getStatusCode().getReasonPhrase());
//                    return transRequest;
//
//                }
//
//
//            }
//            case INTER_BANK_TRANSFER: {
//                transRequest.setTransferType(TransferType.INTER_BANK_TRANSFER);
//                TransferDetails response;
//                String uri = URI + "/transfer/nip";
//                Map<String, String> params = new HashMap<>();
//                params.put("debitAccountNumber", transRequest.getCustomerAccountNumber());
//                params.put("creditAccountNumber", transRequest.getBeneficiaryAccountNumber());
//                params.put("tranAmount", transRequest.getAmount().toString());
//                params.put("destinationInstitutionCode", transRequest.getFinancialInstitution().getInstitutionCode());
//                params.put("tranType", "NIP");
//                logger.info("params for transfer {}", params.toString());
//                try {
//                    response = template.postForObject(uri, params, TransferDetails.class);
//
//
//                    logger.info("response for transfer {}", response.toString());
//                    transRequest.setReferenceNumber(response.getUniqueReferenceCode());
//                    transRequest.setNarration(response.getNarration());
//                    transRequest.setStatus(response.getResponseCode());
//                    transRequest.setStatusDescription(response.getResponseDescription());
//
//                    return transRequest;
//                }
//
//                catch (HttpStatusCodeException e) {
//
//                        e.printStackTrace();
//                        transRequest.setStatus(e.getStatusCode().toString());
//                        transRequest.setStatusDescription(e.getStatusCode().getReasonPhrase());
//                        return transRequest;
//
//                    }
//
//            }
//            case INTERNATIONAL_TRANSFER: {
//
//            }
//
//            case OWN_ACCOUNT_TRANSFER: {
//                transRequest.setTransferType(TransferType.OWN_ACCOUNT_TRANSFER);
//                TransferDetails response;
//                String uri = URI + "/transfer/local";
//                Map<String, String> params = new HashMap<>();
//                params.put("debitAccountNumber", transRequest.getCustomerAccountNumber());
//                params.put("creditAccountNumber", transRequest.getBeneficiaryAccountNumber());
//                params.put("tranAmount", transRequest.getAmount().toString());
//                params.put("naration", transRequest.getNarration());
//                logger.info("params for transfer {}", params.toString());
//                try {
//                    response = template.postForObject(uri, params, TransferDetails.class);
//                    transRequest.setNarration(response.getNarration());
//                    transRequest.setReferenceNumber(response.getUniqueReferenceCode());
//                    transRequest.setStatus(response.getResponseCode());
//                    transRequest.setStatusDescription(response.getResponseDescription());
//                    return transRequest;
//
//
//                }  catch (HttpStatusCodeException e) {
//
//                    e.printStackTrace();
//                    transRequest.setStatus(e.getStatusCode().toString());
//                    transRequest.setStatusDescription(e.getStatusCode().getReasonPhrase());
//                    return transRequest;
//
//                }
//
//
//            }
//
//            case RTGS: {
//                TransRequest request =
//                        sendTransfer(transRequest);
//
//
//                return request;
//            }
//        }
//        logger.trace("request did not match any type");
//        transRequest.setStatus(ResultType.ERROR.toString());
//        return transRequest;
//    }
//
//    @Override
//    public TransferDetails makeNapsTransfer(Naps naps) throws TradeTransferException {
//        String uri = URI + "/transfer/naps";
//
//        try {
//            TransferDetails details = template.getForObject(uri, TransferDetails.class, naps);
//            return details;
//        } catch (Exception e) {
//            return new TransferDetails();
//        }
//
//
//    }
//
//    @Override
//    public AccountDetails viewAccountDetails(String acctNo) {
//
//        String uri = URI + "/account/{acctNo}";
//        Map<String, String> params = new HashMap<>();
//        params.put("acctNo", acctNo);
//        try {
//            AccountDetails details = template.getForObject(uri, AccountDetails.class, params);
//            return details;
//        } catch (Exception e) {
//            return new AccountDetails();
//        }
//
//
//    }
//
//    @Override
//    public CustomerDetails isAccountValid(String accNo, String email, String dob) {
//        CustomerDetails result = new CustomerDetails();
//        String uri = URI + "/account/verification";
//        Map<String, String> params = new HashMap<>();
//        params.put("accountNumber", accNo);
//        params.put("email", email);
//        params.put("dateOfBirth", dob);
//        try {
//            result = template.postForObject(uri, params, CustomerDetails.class);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//    @Override
//    public CustomerDetails viewCustomerDetails(String accNo) {
//        CustomerDetails result = new CustomerDetails();
//        String uri = URI + "/customer/{cifId}";
//        String cifId = getAccountDetails(accNo).getCifId();
//        Map<String, String> params = new HashMap<>();
//        params.put("cifId", cifId);
//        try {
//            result = template.getForObject(uri, CustomerDetails.class, params);
//            return result;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//    @Override
//    public CustomerDetails viewCustomerDetailsByCif(String cifId) {
//        CustomerDetails result = new CustomerDetails();
//        String uri = URI + "/customer/{cifId}";
//        Map<String, String> params = new HashMap<>();
//        params.put("cifId",cifId);
//        try {
//            result = template.getForObject(uri, CustomerDetails.class, params);
//            return result;
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        return result;
//    }
//
//    @Override
//    public String getAccountName(String accountNumber) {
//        logger.info(accountNumber + "account number");
//
//        return getAccountDetails(accountNumber).getAccountName();
//
//    }
//
//    @Override
//    public BigDecimal getDailyDebitTransaction(String acctNo) {
//
//        BigDecimal result = new BigDecimal(0);
//        String uri = URI + "/transfer/dailyTransaction";
//        Map<String, String> params = new HashMap<>();
//        params.put("accountNumber", acctNo);
//
//        try {
//            String response = template.postForObject(uri, params, String.class);
//            result = new BigDecimal(response);
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        }
//
//        return result;
//
//    }
//
//
//    @Override
//    public String getDailyAccountLimit(String accNo, String channel) {
//        String result = "NAN";
//        String uri = URI + "/transfer/limit";
//        Map<String, String> params = new HashMap<>();
//        params.put("accountNumber", accNo);
//        params.put("transactionChannel", channel);
//        try {
//            String response = template.postForObject(uri, params, String.class);
//            result =(response);
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        }
//
//        return result;
//
//    }
//
//    @Override
//    public NEnquiryDetails doNameEnquiry(String destinationInstitutionCode, String accountNumber) {
//        NEnquiryDetails result = new NEnquiryDetails();
//        String uri = URI + "/transfer/nameEnquiry";
//        Map<String, String> params = new HashMap<>();
//        params.put("destinationInstitutionCode", destinationInstitutionCode);
//        params.put("accountNumber", accountNumber);
//        logger.trace("params {}", params);
//        try {
//
//            result = template.postForObject(uri, params, NEnquiryDetails.class);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//    @Override
//    @Async
//    public BigDecimal getAvailableBalance(String s) {
//        try {
//            Map<String, BigDecimal> getBalance = getBalance(s);
//            BigDecimal balance = getBalance.get("AvailableBalance");
//            if (balance != null) {
//                return balance;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return new BigDecimal(0);
//    }
//
    @Override
    @Async
    public CompletableFuture<ObjectNode> sendSMS(String message, String contact, String subject) {
        List<String> contacts = new ArrayList<>();
        contacts.add(contact);
        ObjectNode result = null;
        String uri = cmbAlert;
        Map<String, Object> params = new HashMap<>();
        params.put("alertType", "SMS");
        params.put("message", message);
        params.put("subject", subject);
        params.put("contactList", contacts);
        logger.trace("params {}", params);

        try {

            result = template.postForObject(uri, params, ObjectNode.class);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(uri,params,e);

        }

        return  CompletableFuture.completedFuture(result);
    }

//    @Override
//    //@Async
//    public Rate  getFee(String channel) {
//
//        String uri = URI + "/transfer/fee";
//        Map<String, String> params = new HashMap<>();
//        params.put("transactionChannel", channel);
//        try {
//            Rate details = template.postForObject(uri, params, Rate.class);
//            return details;
//        } catch (Exception e) {
//
//            return  new Rate("", "0", "");
//        }
//
//
//    }
//
////    @Override
////    public FormADetails getFormADetails(String s) {
////        Map<String, String> response = new HashMap<>();
////        String utilization="10";
////        String allocatedAmount="10";
////        String formNumber="A67892045";
////        response.put("utilization",utilization);
////        response.put("allocatedAmount",allocatedAmount);
////        response.put("formNumber",formNumber);
////        return (FormADetails) response;
////    }
//
////    @Override
////    public String getFormNumbers(String s) {
////        return null;
////    }
////
////    @Override
////    public String getUtilization(String s) {
////
////        return null;
////    }
////
////    @Override
////    public String getAllocation(String s) {
////
////        return null;
////    }
//
//    public TransRequest sendTransfer(TransRequest transRequest) {
//
//        try {
//
//            Context scontext = new Context();
//            scontext.setVariable("transRequest", transRequest);
//            String recipient = "";
//
//            String mail = templateEngine.process("/cust/transfer/mailtemplate", scontext);
//            SettingDTO setting = configService.getSettingByName("BACK_OFFICE_EMAIL");
//            if ((setting.isEnabled())) {
//                recipient = setting.getValue();
//            }
//
//            mailService.send(recipient, transRequest.getTransferType().toString(), mail);
//            transRequest.setStatus("000");
//            transRequest.setStatus("Approved or completed successfully");
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.error("Exception occurred {}", e);
//            transRequest.setStatus("96");
//            transRequest.setStatusDescription("TRANSACTION FAILED");
//        }
//
//        return transRequest;
//    }


}

