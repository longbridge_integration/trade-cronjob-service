
package cronjobservice.jobs;

import cronjobservice.config.SpringContext;
import cronjobservice.jobs.billunderlcjobs.LodgeBillUnderLcResponseJob;
import cronjobservice.jobs.billunderlcjobs.LodgeBillUnderLcSubmitJob;
import cronjobservice.jobs.btajobs.BtaSubmitResponse;
import cronjobservice.jobs.exportbillundercollectionjobs.LodgeBillResponseJob;
import cronjobservice.jobs.exportbillundercollectionjobs.LodgeExportBillsSubmitJob;
import cronjobservice.jobs.formajobs.FormAAddedOnFinJob;
import cronjobservice.jobs.formajobs.FormAAllocationJob;
import cronjobservice.jobs.formajobs.FormASubmitJob;
import cronjobservice.jobs.formajobs.FormASubmitResponseJob;
import cronjobservice.jobs.formmjobs.FormMAddedOnFinJob;
import cronjobservice.jobs.formnxpjobs.FormNxpAddedOnFinJob;
import cronjobservice.jobs.formnxpjobs.FormNxpSubmitJob;
import cronjobservice.jobs.formnxpjobs.FormNxpSubmitResponseJob;
import cronjobservice.jobs.formqjobs.FormQAllocationJob;
import cronjobservice.jobs.formqjobs.FormQSubmitJob;
import cronjobservice.jobs.formqjobs.FormQSubmitResponseJob;
import cronjobservice.jobs.guaranteejobs.InwardGuaranteeAddedOnFinJob;
import cronjobservice.jobs.guaranteejobs.OutGuaranteeSubmitResponseJob;
import cronjobservice.jobs.guaranteejobs.OutwardGuaranteeAddedOnFinJob;
import cronjobservice.jobs.guaranteejobs.OutwardGuaranteeSubmitJob;
import cronjobservice.jobs.importbilljob.ImportBillForCollectionAddedOnFinacle;
import cronjobservice.jobs.importbilljob.ImportBillUnderLcAddedOnFinacle;
import cronjobservice.jobs.lcjobs.*;
import cronjobservice.jobs.ptajobs.PtaSubmitJob;
import cronjobservice.jobs.ptajobs.PtaSubmitResponseJob;
import cronjobservice.jobs.remittancejobs.RemittanceAddedOnFinJob;
import cronjobservice.jobs.remittancejobs.RemittanceSubmitJob;
import cronjobservice.jobs.remittancejobs.RemittanceSubmitReponseJob;
import cronjobservice.services.CronJobService;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Created by Longbridge on 6/25/2017.
 */
@Service
public class CronJobScheduler {
    public static void startJobs() {
//        JobKey OneTimeKey = new JobKey("OneTime", "ibtest");
//        JobDetail OneTimeJobs = JobBuilder.newJob(RunningJob.class)
//                .withIdentity(OneTimeKey).build();

//        JobKey FormAKey = new JobKey("formA", "formA");
//        JobDetail FormAJobs = JobBuilder.newJob(FormAJob.class)
//                                .withIdentity(FormAKey).build();

        //****** Form A ******//
        JobKey FormASubmitKey = new JobKey("formASubmit", "formASubmit");
        JobDetail FormASubmitJob = JobBuilder.newJob(FormASubmitJob.class)
                .withIdentity(FormASubmitKey).build();

        JobKey FormASubmitResponseKey = new JobKey("formASubmitResponse", "formASubmitResponse");
        JobDetail FormASubmitResponseJob = JobBuilder.newJob(FormASubmitResponseJob.class)
                .withIdentity(FormASubmitResponseKey).build();

        JobKey FormAAllocationKey = new JobKey("formAAllocation", "formAAllocation");
        JobDetail FormAAllocationJob = JobBuilder.newJob(FormAAllocationJob.class)
                .withIdentity(FormAAllocationKey).build();

        JobKey FormAAddedOnFinacleKey = new JobKey("formAAddedOnFinacle", "formAAddedOnFinacle");
        JobDetail FormAAddedOnFinJob = JobBuilder.newJob(FormAAddedOnFinJob.class)
                .withIdentity(FormAAddedOnFinacleKey).build();

        //****** Form Nxp ******//
        JobKey FormNxpSubmitKey = new JobKey("formNxpSubmit", "formNxpSubmit");
        JobDetail FormNxpSubmitJob = JobBuilder.newJob(FormNxpSubmitJob.class)
                .withIdentity(FormNxpSubmitKey).build();

        JobKey FormNxpSubmitResponseKey = new JobKey("formNxpSubmitResponse", "formNxpSubmitResponse");
        JobDetail FormNxpSubmitResponseJob = JobBuilder.newJob(FormNxpSubmitResponseJob.class)
                .withIdentity(FormNxpSubmitResponseKey).build();

        JobKey FormNxpAddedOnFinKey = new JobKey("formNxpAddedOnFin", "formNxpAddedOnFin");
        JobDetail FormNxpAddedOnFinJob = JobBuilder.newJob(FormNxpAddedOnFinJob.class)
                .withIdentity(FormNxpAddedOnFinKey).build();

        //****** Remittance ******//
        JobKey RemittanceSubmitKey = new JobKey("remittanceSubmit", "remittanceSubmit");
        JobDetail RemittanceSubmitJob = JobBuilder.newJob(RemittanceSubmitJob.class)
                .withIdentity(RemittanceSubmitKey).build();

        JobKey RemittanceSubmitResponseKey = new JobKey("remittanceSubmitResponse", "remittanceSubmitResponse");
        JobDetail RemittanceSubmitResponseJob = JobBuilder.newJob(RemittanceSubmitReponseJob.class)
                .withIdentity(RemittanceSubmitResponseKey).build();

//        JobKey RemittanceAddedOnFinKey = new JobKey("remittanceAddedOnFin", "remittanceAddedOnFin");
//        JobDetail RemittanceAddedOnFinJob = JobBuilder.newJob(RemittanceAddedOnFinJob.class)
//                .withIdentity(RemittanceAddedOnFinKey).build();

        //******* Form M *******//
        JobKey FormMAddedOnFinKey = new JobKey("formMAddedOnFin", "formMAddedOnFin");
        JobDetail FormMAddedOnFinJob = JobBuilder.newJob(FormMAddedOnFinJob.class)
                .withIdentity(FormMAddedOnFinKey).build();
        JobKey FormMAllocationKey = new JobKey("formMAllocation", "formMAllocation");
        JobDetail FormMAllocationJob = JobBuilder.newJob(FormAAllocationJob.class)
                .withIdentity(FormMAllocationKey).build();

        //******* Guarantee *******//  OutwardGuaranteeAddedOnFinJob
        JobKey OutwardGuaranteeSubmitKey = new JobKey("outwardGuaranteeSubmit", "outwardGuaranteeSubmit");
        JobDetail OutwardGuaranteeSubmitJob = JobBuilder.newJob(OutwardGuaranteeSubmitJob.class)
                .withIdentity(OutwardGuaranteeSubmitKey).build();
        JobKey OutwardGuaranteeSubmitResponseKey = new JobKey("outGuaranteeSubmitResponse", "outGuaranteeSubmitResponse");
        JobDetail OutGuaranteeSubmitResponseJob = JobBuilder.newJob(OutGuaranteeSubmitResponseJob.class)
                .withIdentity(OutwardGuaranteeSubmitResponseKey).build();
        JobKey InwardGuaranteeAddedOnFinKey = new JobKey("inwardGuaranteeAddedOnFin", "inwardGuaranteeAddedOnFin");
        JobDetail InwardGuaranteeAddedOnFinJob = JobBuilder.newJob(InwardGuaranteeAddedOnFinJob.class)
                .withIdentity(InwardGuaranteeAddedOnFinKey).build();
        JobKey OutwardGuaranteeAddedOnFinKey = new JobKey("outwardGuaranteeAddedOnFin", "outwardGuaranteeAddedOnFin");
        JobDetail OutwardGuaranteeAddedOnFinJob = JobBuilder.newJob(cronjobservice.jobs.guaranteejobs.OutwardGuaranteeAddedOnFinJob.class)
                .withIdentity(OutwardGuaranteeAddedOnFinKey).build();


        //******* LOC *******//  ExportLcAddedonFinacleJob
        JobKey ImportLcSubmitKey = new JobKey("importLcSubmit", "importLcSubmit");
        JobDetail ImportLcSubmitJob = JobBuilder.newJob(ImportLcSubmitJob.class)
                .withIdentity(ImportLcSubmitKey).build();
        JobKey ImportLcSubmitResponseKey = new JobKey("importLcSubmitResponse", "importLcSubmitResponse");
        JobDetail ImportLcSubmitResponseJob = JobBuilder.newJob(ImportLcSubmitResponseJob.class)
                .withIdentity(ImportLcSubmitResponseKey).build();
        JobKey ExportLcSubmitKey = new JobKey("exportLcSubmit", "exportLcSubmit");
        JobDetail ExportLcSubmitJob = JobBuilder.newJob(ExportLcSubmitJob.class)
                .withIdentity(ExportLcSubmitKey).build();
        JobKey ExportLcSubmitResponseKey = new JobKey("exportLcSubmitResponse", "exportLcSubmitResponse");
        JobDetail ExportLcSubmitResponseJob = JobBuilder.newJob(ExportLcSubmitResponseJob.class)
                .withIdentity(ExportLcSubmitResponseKey).build();

        JobKey ExportLcPendingToVerifiedKey = new JobKey("updateExportLcPendingVerification", "updateExportLcPendingVerification");
        JobDetail UpdateExportLcPendingVerificationJob = JobBuilder.newJob(UpdateExportLcPendingVerificationJob.class)
                .withIdentity(ExportLcPendingToVerifiedKey).build();

        JobKey ExportLcAddedOnFinacleKey = new JobKey("exportLcAddedOnFinacle", "exportLcAddedOnFinacle");
        JobDetail ExportLcAddedonFinacleJob= JobBuilder.newJob(ExportLcAddedonFinacleJob.class)
                .withIdentity(ExportLcAddedOnFinacleKey).build();
        //End LOC



        JobKey Account = new JobKey("account", "account");
        JobDetail AccountJobs = JobBuilder.newJob(AccountJob.class)
                .withIdentity(Account).build();

        JobKey CustomerInformation = new JobKey("customerInformation", "customerInformation");
        JobDetail CustomerInformationJobs = JobBuilder.newJob(CustomerJob.class)
                .withIdentity(CustomerInformation).build();

//        JobKey FormM = new JobKey("formM", "formM");
//        JobDetail FormMJobs = JobBuilder.newJob(FormMJob.class)
//                .withIdentity(FormM).build();

        JobKey Bills = new JobKey("bills", "bills");
        JobDetail BillsJobs = JobBuilder.newJob(BillsJob.class)
                .withIdentity(Bills).build();

//        JobKey FormNxp = new JobKey("formNxp", "formNxp");
//        JobDetail FormNxpJobs = JobBuilder.newJob(FormNxpJob.class)
//                .withIdentity(FormNxp).build();

        JobKey FormQ = new JobKey("formQ", "formQ");
        JobDetail FormQJobs = JobBuilder.newJob(FormQJob.class)
                .withIdentity(FormQ).build();

        JobKey Guarantee = new JobKey("guarantee", "guarantee");
        JobDetail GuaranteeJobs = JobBuilder.newJob(GuaranteeJob.class)
                .withIdentity(Guarantee).build();

        JobKey Lc = new JobKey("lc", "lc");
        JobDetail LcJobs = JobBuilder.newJob(LcJob.class)
                .withIdentity(Lc).build();

        //******PTA ******//
        JobKey PtaSubmitKey = new JobKey("PtaSubmit", "PtaSubmit");
        JobDetail PtaSubmitJob = JobBuilder.newJob(cronjobservice.jobs.ptajobs.PtaSubmitJob.class)
                .withIdentity(PtaSubmitKey).build();

        JobKey PtaSubmitResponseKey = new JobKey("PtaSubmitResponse", "PtaSubmitResponse");
        JobDetail PtaSubmitResponseJob = JobBuilder.newJob(PtaSubmitResponseJob.class)
                .withIdentity(PtaSubmitResponseKey).build();

        //******BTA ******//
        JobKey BtaSubmitKey = new JobKey("BtaSubmit", "BtaSubmit");
        JobDetail BtaSubmitJob = JobBuilder.newJob(cronjobservice.jobs.btajobs.btaSubmitJob.class)
                .withIdentity(BtaSubmitKey).build();

        JobKey BtaSubmitResponseKey = new JobKey("BtaSubmitResponse", "BtaSubmitResponse");
        JobDetail BtaSubmitResponseJob = JobBuilder.newJob(BtaSubmitResponse.class)
                .withIdentity(BtaSubmitResponseKey).build();

        //******BILLS ******//
        JobKey LodgeExportBillsUnderLcKey = new JobKey("LodgeExportBillsUnderLc", "LodgeExportBillsUnderLc");
        JobDetail LodgeExportBillsUnderLcJob = JobBuilder.newJob(LodgeBillUnderLcSubmitJob.class)
                .withIdentity(LodgeExportBillsUnderLcKey).build();

        JobKey LodgeExportBillsUnderLcResponseKey = new JobKey("LodgeExportBillsUnderLcResponse", "LodgeExportBillsUnderLcResponse");
        JobDetail LodgeExportBillsUnderLcResponseJob = JobBuilder.newJob(LodgeBillUnderLcResponseJob.class)
                .withIdentity(LodgeExportBillsUnderLcResponseKey).build();

        JobKey LodgeExportBillsForCollectionKey = new JobKey("LodgeExportBillsForCollection", "LodgeExportBillsForCollection");
        JobDetail LodgeExportBillsSubmitJob = JobBuilder.newJob(cronjobservice.jobs.exportbillundercollectionjobs.LodgeExportBillsSubmitJob.class)
                .withIdentity(LodgeExportBillsForCollectionKey).build();

        JobKey LodgeExportBillsForCollectionResponseKey = new JobKey("LodgeExportBillsForCollectionResponse", "LodgeExportBillsForCollectionResponse");
        JobDetail LodgeExportBillsForCollectionResponseJob = JobBuilder.newJob(LodgeBillResponseJob.class)
                .withIdentity(LodgeExportBillsForCollectionResponseKey).build();

        JobKey ImportBillsForCollectionKey = new JobKey("ImportBillForCollection", "ImportBillForCollection");
        JobDetail ImportBillsForCollectionJob = JobBuilder.newJob(ImportBillForCollectionAddedOnFinacle.class)
                .withIdentity(ImportBillsForCollectionKey).build();

        JobKey ImportBillsUnderLcKey = new JobKey("ImportBillUnderLc", "ImportBillUnderLc");
        JobDetail ImportBillsUnderLcJob = JobBuilder.newJob(ImportBillUnderLcAddedOnFinacle.class)
                .withIdentity(ImportBillsUnderLcKey).build();

        //End

        //FormQ
        JobKey FormQSubmitKey = new JobKey("formqSubmit", "formqSubmit");
        JobDetail FormQSubmitJob = JobBuilder.newJob(cronjobservice.jobs.formqjobs.FormQSubmitJob.class)
                .withIdentity(FormQSubmitKey).build();

        JobKey FormQSubmitResponseKey = new JobKey("formqSubmitResponse", "formqSubmitResponse");
        JobDetail FormQSubmitResponseJob = JobBuilder.newJob(FormQSubmitResponseJob.class)
                .withIdentity(FormQSubmitResponseKey).build();

        JobKey FormQSubmitAllocationKey = new JobKey("formqAllocation", "formqAllocation");
        JobDetail FormQAllocationJob = JobBuilder.newJob(FormQAllocationJob.class)
                .withIdentity(FormQSubmitAllocationKey).build();
        //End Formq




        /**
         * JOB Triggers
         * @Shedules
         */
        ApplicationContext context = SpringContext.getApplicationContext();
        CronJobService cronJobService = context.getBean (CronJobService.class);


        Trigger formASubmit = TriggerBuilder
                .newTrigger()
                .withIdentity("formASubmit", "formASubmit")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formASubmit")))
                .build();
        Trigger formASubmitResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("formASubmitResponse", "formASubmitResponse")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formASubmitResponse")))
                .build();
        Trigger formAAllocation = TriggerBuilder
                .newTrigger()
                .withIdentity("formAAllocation", "formAAllocation")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formAAllocation")))
                .build();
        Trigger formAAddedOnFinacle = TriggerBuilder
                .newTrigger()
                .withIdentity("formAAddedOnFinacle", "formAAddedOnFinacle")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formAAddedOnFinacle")))
                .build();

        Trigger formNxpSubmit = TriggerBuilder
                .newTrigger()
                .withIdentity("formNxpSubmit", "formNxpSubmit")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formNxpSubmit")))
                .build();
        Trigger formNxpSubmitResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("formNxpSubmitResponse", "formNxpSubmitResponse")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formNxpSubmitResponse")))
                .build();
        Trigger formNxpAddedOnFin = TriggerBuilder
                .newTrigger()
                .withIdentity("formNxpAddedOnFin", "formNxpAddedOnFin")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formNxpAddedOnFin")))
                .build();

        Trigger remittanceSubmit = TriggerBuilder
                .newTrigger()
                .withIdentity("remittanceSubmit", "remittanceSubmit")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("remittanceSubmit")))
                .build();
        Trigger remittanceSubmitResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("remittanceSubmitResponse", "remittanceSubmitResponse")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("remittanceSubmitResponse")))
                .build();
        Trigger remittanceAddedOnFin = TriggerBuilder
                .newTrigger()
                .withIdentity("remittanceAddedOnFin", "remittanceAddedOnFin")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("remittanceAddedOnFin")))
                .build();

        Trigger formMAddedOnFin = TriggerBuilder
                .newTrigger()
                .withIdentity("formMAddedOnFin", "formMAddedOnFin")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formMAddedOnFin")))
                .build();
        Trigger formMAllocation = TriggerBuilder
                .newTrigger()
                .withIdentity("formMAllocation", "formMAllocation")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formMAllocation")))
                .build();

        Trigger outwardGuaranteeSubmit = TriggerBuilder
                .newTrigger()
                .withIdentity("outwardGuaranteeSubmit", "outwardGuaranteeSubmit")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("outwardGuaranteeSubmit")))
                .build();
        Trigger outGuaranteeSubmitResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("outGuaranteeSubmitResponse", "outGuaranteeSubmitResponse")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("outGuaranteeSubmitResponse")))
                .build();
        Trigger inwardGuaranteeAddedOnFin = TriggerBuilder
                .newTrigger()
                .withIdentity("inwardGuaranteeAddedOnFin", "inwardGuaranteeAddedOnFin")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("inwardGuaranteeAddedOnFin")))
                .build();

        Trigger outwardGuaranteeAddedOnFin = TriggerBuilder
                .newTrigger()
                .withIdentity("outwardGuaranteeAddedOnFin", "outwardGuaranteeAddedOnFin")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("outwardGuaranteeAddedOnFin")))
                .build();

        Trigger importLcSubmit = TriggerBuilder
                .newTrigger()
                .withIdentity("importLcSubmit", "importLcSubmit")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("importLcSubmit")))
                .build();
        Trigger importLcSubmitResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("importLcSubmitResponse", "importLcSubmitResponse")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("importLcSubmitResponse")))
                .build();
        Trigger exportLcSubmit = TriggerBuilder
                .newTrigger()
                .withIdentity("exportLcSubmit", "exportLcSubmit")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("exportLcSubmit")))
                .build();
        Trigger exportLcSubmitResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("exportLcSubmitResponse", "exportLcSubmitResponse")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("exportLcSubmitResponse")))
                .build();
        Trigger updateExportLcPendingVerification = TriggerBuilder
                .newTrigger()
                .withIdentity("updateExportLcPendingVerification", "updateExportLcPendingVerification")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("updateExportLcPendingVerification")))
                .build();

        //exportLcAddedOnFinacle
        Trigger exportLcAddedOnFinacle = TriggerBuilder
                .newTrigger()
                .withIdentity("exportLcAddedOnFinacle", "exportLcAddedOnFinacle")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("exportLcAddedOnFinacle")))
                .build();


        Trigger account = TriggerBuilder
                .newTrigger()
                .withIdentity("account", "account")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("account")))
                .build();

        Trigger customerInformation = TriggerBuilder
                .newTrigger()
                .withIdentity("customerInformation", "customerInformation")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("customerInformation")))
                .build();

        Trigger PtaSubmit = TriggerBuilder
                .newTrigger()
                .withIdentity("PtaSubmit", "PtaSubmit")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("PtaSubmit")))
                .build();
        Trigger PtaSubmitResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("PtaSubmitResponse", "PtaSubmitResponse")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("PtaSubmitResponse")))
                .build();

        Trigger BtaSubmit = TriggerBuilder
                .newTrigger()
                .withIdentity("BtaSubmit", "BtaSubmit")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("BtaSubmit")))
                .build();
        Trigger BtaSubmitResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("BtaSubmitResponse", "BtaSubmitResponse")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("BtaSubmitResponse")))
                .build();

        Trigger LodgeExportBillsUnderLc = TriggerBuilder
                .newTrigger()
                .withIdentity("LodgeExportBillsUnderLc", "LodgeExportBillsUnderLc")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("LodgeExportBillsUnderLc")))
                .build();
        Trigger LodgeExportBillsUnderLcResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("LodgeExportBillsUnderLcResponse", "LodgeExportBillsUnderLcResponse")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("LodgeExportBillsUnderLcResponse")))
                .build();

        Trigger LodgeExportBillsForCollection = TriggerBuilder
                .newTrigger()
                .withIdentity("LodgeExportBillsForCollection", "LodgeExportBillsForCollection")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("LodgeExportBillsForCollection")))
                .build();
        Trigger LodgeExportBillsForCollectionResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("LodgeExportBillsForCollectionResponse", "LodgeExportBillsForCollectionResponse")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("LodgeExportBillsForCollectionResponse")))
                .build();

        Trigger ImportBillForCollection = TriggerBuilder
                .newTrigger()
                .withIdentity("ImportBillForCollection", "ImportBillForCollection")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("ImportBillForCollection")))
                .build();
        Trigger ImportBillUnderLc = TriggerBuilder
                .newTrigger()
                .withIdentity("ImportBillUnderLc", "ImportBillUnderLc")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("ImportBillUnderLc")))
                .build();

//        Trigger formM = TriggerBuilder
//                .newTrigger()
//                .withIdentity("formM", "formM")
//                .withSchedule(
//                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formM")))
//                .build();

        Trigger bills = TriggerBuilder
                .newTrigger()
                .withIdentity("bills", "bills")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("bills")))
                .build();

        Trigger formQ = TriggerBuilder
                .newTrigger()
                .withIdentity("formQ", "formQ")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formQ")))
                .build();


        Trigger formqSubmit = TriggerBuilder
                .newTrigger()
                .withIdentity("formqSubmit", "formqSubmit")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formqSubmit")))
                .build();

        Trigger formqSubmitResponse = TriggerBuilder
                .newTrigger()
                .withIdentity("formqSubmitResponse", "formqSubmitResponse")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formqSubmitResponse")))
                .build();


        Trigger formqAllocation = TriggerBuilder
                .newTrigger()
                .withIdentity("formqAllocation", "formqAllocation")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronJobService.getCurrentExpression("formqAllocation")))
                .build();




        /**
         * STart Schedules.............
         */
        try {
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
//            scheduler.scheduleJob(FormAJobs, formA);
            scheduler.scheduleJob(FormASubmitJob,formASubmit);
            scheduler.scheduleJob(FormASubmitResponseJob,formASubmitResponse);
            scheduler.scheduleJob(FormAAllocationJob,formAAllocation);
            scheduler.scheduleJob(FormAAddedOnFinJob,formAAddedOnFinacle);
//            scheduler.scheduleJob(FormNxpJobs, formNxp);
            scheduler.scheduleJob(FormNxpSubmitJob, formNxpSubmit);
            scheduler.scheduleJob(FormNxpSubmitResponseJob, formNxpSubmitResponse);
            scheduler.scheduleJob(FormNxpAddedOnFinJob, formNxpAddedOnFin);
//            scheduler.scheduleJob(RemittanceJobs, remittance);
            scheduler.scheduleJob(RemittanceSubmitJob, remittanceSubmit);
            scheduler.scheduleJob(RemittanceSubmitResponseJob, remittanceSubmitResponse);
//            scheduler.scheduleJob(RemittanceAddedOnFinJob, remittanceAddedOnFin);
//            scheduler.scheduleJob(FormMJobs, formM);
            scheduler.scheduleJob(FormMAddedOnFinJob, formMAddedOnFin);
            scheduler.scheduleJob(FormMAllocationJob, formMAllocation);
//            scheduler.scheduleJob(GuaranteeJobs, guarantee);
            scheduler.scheduleJob(OutwardGuaranteeSubmitJob, outwardGuaranteeSubmit);
            scheduler.scheduleJob(OutGuaranteeSubmitResponseJob, outGuaranteeSubmitResponse);
            scheduler.scheduleJob(InwardGuaranteeAddedOnFinJob, inwardGuaranteeAddedOnFin);
            scheduler.scheduleJob(OutwardGuaranteeAddedOnFinJob, outwardGuaranteeAddedOnFin);
//            scheduler.scheduleJob(LcJobs, lc);
            scheduler.scheduleJob(ImportLcSubmitJob, importLcSubmit);
            scheduler.scheduleJob(ImportLcSubmitResponseJob, importLcSubmitResponse);
            scheduler.scheduleJob(ExportLcSubmitJob, exportLcSubmit);
            scheduler.scheduleJob(ExportLcSubmitResponseJob, exportLcSubmitResponse);
            scheduler.scheduleJob(UpdateExportLcPendingVerificationJob, updateExportLcPendingVerification);
            scheduler.scheduleJob(ExportLcAddedonFinacleJob, exportLcAddedOnFinacle);
            //exportLcAddedOnFinacle

            scheduler.scheduleJob(AccountJobs, account);
            scheduler.scheduleJob(CustomerInformationJobs, customerInformation);
            scheduler.scheduleJob(BillsJobs, bills);
            scheduler.scheduleJob(FormQJobs, formQ);

            scheduler.scheduleJob(PtaSubmitJob,PtaSubmit);
            scheduler.scheduleJob(PtaSubmitResponseJob,PtaSubmitResponse);  //LodgeExportBillsUnderLcResponse  LodgeExportBillsForCollectionResponse

            scheduler.scheduleJob(BtaSubmitJob,BtaSubmit);
            scheduler.scheduleJob(BtaSubmitResponseJob,BtaSubmitResponse);

            scheduler.scheduleJob(LodgeExportBillsUnderLcJob,LodgeExportBillsUnderLc);
            scheduler.scheduleJob(LodgeExportBillsUnderLcResponseJob,LodgeExportBillsUnderLcResponse);

            scheduler.scheduleJob(LodgeExportBillsSubmitJob,LodgeExportBillsForCollection);
            scheduler.scheduleJob(LodgeExportBillsForCollectionResponseJob,LodgeExportBillsForCollectionResponse);

            scheduler.scheduleJob(ImportBillsForCollectionJob,ImportBillForCollection);
            scheduler.scheduleJob(ImportBillsUnderLcJob,ImportBillUnderLc);

            scheduler.scheduleJob(FormQSubmitJob,formqSubmit);
            scheduler.scheduleJob(FormQSubmitResponseJob,formqSubmitResponse);
            scheduler.scheduleJob(FormQAllocationJob,formqAllocation);

//            scheduler.scheduleJob(OneTimeJobs, oneTime);
            scheduler.start();
        }catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}