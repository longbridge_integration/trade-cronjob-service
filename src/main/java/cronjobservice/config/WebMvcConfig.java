package cronjobservice.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.mvc.WebContentInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.Locale;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {


//    @Bean
//    public BankUserLoginInterceptor bankUserLoginInterceptor()
//    {
//        return new BankUserLoginInterceptor();
//    }
//
//    @Bean
//    public AdminUserLoginInterceptor adminUserLoginInterceptor() {
//        return new AdminUserLoginInterceptor();
//    }
//
//    @Bean
//    public RetailUserLoginInterceptor retailUserLoginInterceptor() {
//        return new RetailUserLoginInterceptor();
//    }
//
//    @Bean
//    public CorporateUserLoginInterceptor corporateUserLoginInterceptor() {
//        return new CorporateUserLoginInterceptor();
//    }


//    @Bean("bCryptPasswordEncoder")
//    public BCryptPasswordEncoder passwordEncoder() {
//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
//        return bCryptPasswordEncoder;
//    }
//
//    @Bean
//    public AdmTradePassWordEncoder passWordEncode(){
//        return  new AdmTradePassWordEncoder() ;
//    }
//
//    @Bean("bnkTradePassWordEncoder")
//    public BnkTradePassWordEncoder passWordEncoder(){
//        return  new BnkTradePassWordEncoder() ;
//    }

    @Bean
    public DocumentBuilder builder (){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        factory.setNamespaceAware(true);
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder ;
        }catch (Exception e){
            System.out.println("could not create builder !");
        }
        return null ;
    }
    @Bean
    public CommonAnnotationBeanPostProcessor initCommonAnnotationBeanPostProcessor() {
        return new CommonAnnotationBeanPostProcessor();
    }

    @Bean()
    public ModelMapper modelMapper() {
        ModelMapper mm = new ModelMapper();
        mm.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return mm;
    }


    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        builder.setConnectTimeout(1*60);
        builder.setReadTimeout(1*60);
       // builder.basicAuthorization()
        return builder.build();
    }



//    @Override
//    public void addInterceptors(final InterceptorRegistry registry) {
//        final LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
//        localeChangeInterceptor.setParamName("lang");
//        registry.addInterceptor(localeChangeInterceptor);
//        registry.addInterceptor(adminUserLoginInterceptor()).addPathPatterns("/admin/**");
//        registry.addInterceptor(bankUserLoginInterceptor()).addPathPatterns("/bank/**");
//        registry.addInterceptor(retailUserLoginInterceptor()).addPathPatterns("/retail/**");
//        registry.addInterceptor(corporateUserLoginInterceptor()).addPathPatterns("/corporate/**");
//        registry.addInterceptor(webContentInterceptor()).addPathPatterns("/retail/**");
//        registry.addInterceptor(webContentInterceptor()).addPathPatterns("/corporate/**");
//       // registry.addInterceptor(webContentInterceptor()).addPathPatterns("/retail/**");
//    }

//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new SessionTimerInterceptor());
//    }

    @Bean
    public LocaleResolver localeResolver() {
        final CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setDefaultLocale(Locale.ENGLISH);
        return cookieLocaleResolver;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Bean
    @ConditionalOnMissingBean(RequestContextListener.class)
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        String[] baseNames = new String[]{"i18n/messages", "i18n/menu" ,"i18n/integration", "i18n/validator"};
        source.setBasenames(baseNames);  // name of the resource bundle
        source.setCacheSeconds(1000);
        source.setUseCodeAsDefaultMessage(true);
        return source;
    }

    //
//    @Bean
//    public AdminAuthenticationSuccessHandler successHandler() {
//        AdminAuthenticationSuccessHandler handler = new AdminAuthenticationSuccessHandler();
//        handler.setUseReferer(true);
//        return handler;
//    }

//    @Bean
//    public RetailTransferAuthInterceptor retailTransferAuthInterceptor() {
//        return new RetailTransferAuthInterceptor();
//    }
//    @Bean
//    public CorporateTransferAuthInterceptor corpTransferAuthInterceptor() {
//        return new CorporateTransferAuthInterceptor();
//    }
//    @Bean
//    public OpUserLoginInterceptor OpUserLoginInterceptor()
//    {
//        return new OpUserLoginInterceptor();
//    }
//
////    @Bean
//    public AdminUserLoginInterceptor adminUserLoginInterceptor() {
//        return new AdminUserLoginInterceptor();
//    }
//
//    @Bean
//    public RetailUserLoginInterceptor retailUserLoginInterceptor() {
//        return new RetailUserLoginInterceptor();
//    }
//
//    @Bean
//    public CorporateUserLoginInterceptor corporateUserLoginInterceptor() {
//
//        return new CorporateUserLoginInterceptor();
//    }
    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
    @Bean
    public WebContentInterceptor webContentInterceptor() {
        WebContentInterceptor interceptor = new WebContentInterceptor();
        interceptor.setCacheSeconds(-1);
        interceptor.setUseExpiresHeader(true);
        interceptor.setAlwaysMustRevalidate(true);
        interceptor.setUseCacheControlHeader(true);
        interceptor.setUseCacheControlNoStore(true);
        return interceptor;
    }


//
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/resources/static/**")
//                .addResourceLocations("/resources/static/");
//    }


    // Bean name must be "multipartResolver", by default Spring uses method name as bean name.
    @Bean
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }

    	// if the method name is different, you must define the bean name manually like this :
	@Bean(name = "multipartResolver")
    public MultipartResolver createMultipartResolver() {
        return new StandardServletMultipartResolver();
    }

    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/corporate/corporate/html/");
        viewResolver.setPrefix("/bank/bank/html/");
        viewResolver.setSuffix(".html");
        return viewResolver;
    }
}