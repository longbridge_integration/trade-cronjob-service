package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Collection;

@Entity
@DiscriminatorValue("For Collection")
@Where(clause="del_Flag='N'")
@Audited(withModifiedFlag=true)
public class ExportBillForCollection extends ExportBills{

    @ManyToOne
    private FormNxp formNxp;

    @OneToMany(cascade = CascadeType.ALL)
    private Collection<ExportBillForCollectionDocument> expCollectionDoc;


    public Collection<ExportBillForCollectionDocument> getDocuments() {
        return expCollectionDoc;
    }

    public void setDocuments(Collection<ExportBillForCollectionDocument> documents) {
        this.expCollectionDoc = documents;
    }

    public FormNxp getFormNxp() {
        return formNxp;
    }

    public void setFormNxp(FormNxp formNxp) {
        this.formNxp = formNxp;
    }

}
