package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import cronjobservice.utils.PrettySerializer;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * The {@code FinancialInstitution} class models contains details of a banks or any
 * other financial institution that participates in electronic transfers
 * @author Fortunatus Ekenachi
 * Created on 3/30/2017.
 */
@Entity
@Audited(withModifiedFlag=true)
@Where(clause ="del_Flag='N'")
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"swiftCode"}))
public class FinancialInstitution extends AbstractEntity implements PrettySerializer {

    private String institutionCode;

    private FinancialInstitutionType institutionType;

    private String institutionName;

    private String swiftCode;

    private String bankAddress;

    private String bankCity;

    private String bankState;

    private String bankCountry;

    private String bankPostalCode;
    private String branchId;

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getInstitutionCode() {
        return institutionCode;
    }

    public void setInstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode;
    }

    public FinancialInstitutionType getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(FinancialInstitutionType institutionType) {
        this.institutionType = institutionType;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getBankCity() {
        return bankCity;
    }

    public void setBankCity(String bankCity) {
        this.bankCity = bankCity;
    }

    public String getBankState() {
        return bankState;
    }

    public void setBankState(String bankState) {
        this.bankState = bankState;
    }

    public String getBankCountry() {
        return bankCountry;
    }

    public void setBankCountry(String bankCountry) {
        this.bankCountry = bankCountry;
    }

    public String getBankPostalCode() {
        return bankPostalCode;
    }

    public void setBankPostalCode(String bankPostalCode) {
        this.bankPostalCode = bankPostalCode;
    }

    @Override
    public String toString() {
        return "FinancialInstitution{" +
                "institutionCode='" + institutionCode + '\'' +
                ", institutionType=" + institutionType +
                ", institutionName='" + institutionName + '\'' +
                ", swiftCode='" + swiftCode + '\'' +
                ", bankAddress='" + bankAddress + '\'' +
                ", bankCity='" + bankCity + '\'' +
                ", bankState='" + bankState + '\'' +
                ", bankCountry='" + bankCountry + '\'' +
                ", bankPostalCode='" + bankPostalCode + '\'' +
                ", id=" + id +
                ", version=" + version +
                ", delFlag='" + delFlag + '\'' +
                ", deletedOn=" + deletedOn +
                '}';
    }

    @Override
   	public List<String> getDefaultSearchFields() {
   		return Arrays.asList("institutionCode", "institutionName");
   	}


    @Override @JsonIgnore
    public JsonSerializer<FinancialInstitution> getSerializer() {
        return new JsonSerializer<FinancialInstitution>() {
            @Override
            public void serialize(FinancialInstitution value, JsonGenerator gen, SerializerProvider serializers)
                    throws IOException {
                gen.writeStartObject();
                gen.writeStringField("Institution Code",value.institutionCode);
                gen.writeStringField("Institution Name",value.institutionName);
                gen.writeStringField("Institution Type",value.institutionType.name());
                gen.writeStringField("Institution Swift Code", value.swiftCode);
                gen.writeEndObject();
            }
        };
    }

}

