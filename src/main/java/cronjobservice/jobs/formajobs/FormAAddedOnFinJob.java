package cronjobservice.jobs.formajobs;


import cronjobservice.config.SpringContext;
import cronjobservice.exception.CronJobException;
import cronjobservice.services.CronJobService;
import cronjobservice.utils.JobCategory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * Created by Longbridge on 9/12/2017.
 */
@org.springframework.stereotype.Component
@DisallowConcurrentExecution
public class FormAAddedOnFinJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Logger logger = LoggerFactory.getLogger(this.getClass());
        ApplicationContext contextFormA = SpringContext.getApplicationContext();
        CronJobService cronJobService = contextFormA.getBean (CronJobService.class);

        try {
            logger.info("adding formA form Finacle job starts runing");
//            cronJobService.saveRunningJob("in-built",cronJobService.getCurrentExpression());
            cronJobService.saveRunningJob(JobCategory.FORMA_ADDED_ON_FINACLE.toString(),cronJobService.getCurrentExpression(JobCategory.FORMA_ADDED_ON_FINACLE.toString()));

            cronJobService.getFormAAddedOnFinacle();
            cronJobService.updateRunningJob(JobCategory.FORMA_ADDED_ON_FINACLE.toString());
            logger.info("adding formA form Finacle job ends");
        } catch (CronJobException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
