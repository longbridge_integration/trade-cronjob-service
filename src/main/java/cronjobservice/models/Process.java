package cronjobservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonSerializer;
import cronjobservice.utils.PrettySerializer;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Showboy on 27/08/2017.
 */

@MappedSuperclass
public class Process extends AbstractEntity implements PrettySerializer
{
    @OneToOne
    private WorkFlow workFlow;

    @ManyToMany(cascade ={ CascadeType.MERGE})
    private List<Comments> comment = new ArrayList<>();

    private String status;

    private String cifid;

    private Date createdOn;

    private BigDecimal dollarEquivalent;

    private String formStatus;
    private String concessionFlag;
    private Double concessionPercentage;

    @Value("${bank.code}")
    private String bankCode;
    private String tranId;

    private String submitFlag = "U";

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getConcessionFlag() {
        return concessionFlag;
    }

    public void setConcessionFlag(String concessionFlag) {
        this.concessionFlag = concessionFlag;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getFormStatus() {
        return formStatus;
    }

    public void setFormStatus(String formStatus) {
        this.formStatus = formStatus;
    }

    public Double getConcessionPercentage() {
        return concessionPercentage;
    }

    public void setConcessionPercentage(Double concessionPercentage) {
        this.concessionPercentage = concessionPercentage;
    }

    public BigDecimal getDollarEquivalent() {
        return dollarEquivalent;
    }

    public void setDollarEquivalent(BigDecimal dollarEquivalent) {
        this.dollarEquivalent = dollarEquivalent;
    }

    public List<Comments> getComment() {
        return comment;
    }

    public void setComment(List<Comments> comment) {
        this.comment = comment;
    }

    public WorkFlow getWorkFlow() {
        return workFlow;
    }

    public void setWorkFlow(WorkFlow workFlow)
    {
        this.workFlow = workFlow;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCifid() {
        return cifid;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getSubmitFlag() {
        return submitFlag;
    }

    public void setSubmitFlag(String submitFlag) {
        this.submitFlag = submitFlag;
    }

    @Override
    @JsonIgnore
    public <T> JsonSerializer<T> getSerializer() {
        return null;
    }





}
