package cronjobservice.models;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Showboy on 07/09/2017.
 */
@Entity
@Audited(withModifiedFlag=true)
@Where(clause ="del_Flag='N'" )
public class WorkFlowRules extends AbstractEntity{
    @OneToOne
    private WorkFlowStates currentState;

    @ManyToMany
    @JoinTable(name = "wfr_endState", joinColumns =
    @JoinColumn(name = "wfr_id", referencedColumnName = "id"), inverseJoinColumns =
    @JoinColumn(name = "endstate_id", referencedColumnName = "id") )
    private List<WorkFlowStates> endstates;

    private String renderPage;

    public WorkFlowStates getCurrentState()
    {

        return currentState;
    }

    public void setCurrentState(WorkFlowStates currentState)
    {
        this.currentState = currentState;
    }

    public List<WorkFlowStates> getEndstates() {
        return endstates;
    }

    public void setEndstates(List<WorkFlowStates> endstates) {
        this.endstates = endstates;
    }

    public String getRenderPage() {
        return renderPage;
    }

    public void setRenderPage(String renderPage) {
        this.renderPage = renderPage;
    }

    @Override
    public String toString() {
        return "WorkFlowRules{" +
                "currentState=" + currentState +
                ", endstates=" + endstates +
                ", renderPage='" + renderPage + '\'' +
                '}';
    }
}
