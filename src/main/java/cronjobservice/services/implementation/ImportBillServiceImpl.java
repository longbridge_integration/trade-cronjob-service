package cronjobservice.services.implementation;

import cronjobservice.api.ExportBillsDetails;
import cronjobservice.api.FormADetails;
import cronjobservice.api.GuaranteeDetails;
import cronjobservice.api.ImportBillDetails;
import cronjobservice.dtos.*;
import cronjobservice.exception.CronJobException;
import cronjobservice.models.*;
import cronjobservice.repositories.*;
import cronjobservice.services.ConfigurationService;
import cronjobservice.services.FormMService;
import cronjobservice.services.ImportBillService;
import cronjobservice.services.IntegrationService;
import cronjobservice.utils.DateFormatter;
import cronjobservice.utils.FormNumber;
import org.modelmapper.ModelMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

@Service
public class ImportBillServiceImpl implements ImportBillService {

    private Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    IntegrationService integrationService;
    @Autowired
    ConfigurationService configurationService;
    @Autowired
    WorkFlowRepo workFlowRepo;
    @Value("${tradeservice.service.uri}")
    private String URI;
    @Autowired
    private RestTemplate template;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ImportBillUnderLcRepo importBillUnderLcRepo;
    @Autowired
    ImportBillService importBillService;
    @Autowired
    FormMService formMService;
    @Autowired
    FormMRepo formMRepo;
    @Autowired
    ImportBillForCollectionRepo importBillForCollectionRepo;

    @Autowired
    WorkFlowStatesRepo workFlowStatesRepo;
    @Autowired
    MessageSource messageSource;



    org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Transactional
    @Override
    public String getImportBillUnderLcAddedOnFinacle() {

        List<ImportBillDetails> importBillDetails = integrationService.getImportBillsUnderLcAddedOnFinacle();

        try{

            for (ImportBillDetails importBillDetail : importBillDetails) {


                if (null==importBillDetail.getBillNumber()) {
                    logger.info("The Import  Bill number under lc from Finacle Does Not Exist");
                    continue;
                }

                ImportBillUnderLc finbillsUnderLc = modelMapper.map(importBillDetail, ImportBillUnderLc.class);
                ImportBillUnderLcDTO importBillUnderLcDTO = importBillService.convertEntityToBillsUnderLcDTO(finbillsUnderLc);
                logger.info(" Import Bill Parametre {} ", importBillUnderLcDTO.getBillNumber());

                FormM formm = formMRepo.findByFormNumber(importBillDetail.getFormNumber());

                if (null != formm && null == formm.getCifid()) {

                        formm.setCifid(importBillDetail.getFormMCif());
                        formMRepo.save(formm);
                }

                ImportBillUnderLc tmsbillsunderlc = importBillUnderLcRepo.findByBillNumber(importBillUnderLcDTO.getBillNumber());
                if(null == tmsbillsunderlc) {
//                    ImportBillUnderLc importBillUnderLc = new ImportBillUnderLc();
                    Boolean response = importBillService.saveImportBillsUnderLcFromFinacle(finbillsUnderLc);

                    if (response) {
                        String uri = URI + "/api/bills/updateimportbillunderlc";
                        try {
                            template.postForObject(uri, importBillUnderLcDTO, String.class);
                            logger.info("Success after response Import Bills under lc");
                        } catch (Exception ex) {
                            logger.error("Exception occurred getting response for Import Bills under lc {}", ex);
                            ex.printStackTrace();
                        }
                    }
                }else {
                    logger.info("lodge  Import bill under lc already exist");
                }

            }
        }catch (Exception e){
            logger.info("Error saving Import Bill under lc from FInacle {} ",e);
//            throw new CronJobException();
        }
        return null;
    }


    @Transactional
    @Override
    public String getImportBillForCollectionAddedOnFinacle() {

        List<ImportBillDetails> importBillDetails = integrationService.getImportBillsUForCollectionddedOnFinacle();

        try{

            for (ImportBillDetails importBillDetail : importBillDetails) {


                if (null==importBillDetail.getBillNumber()) {
                    logger.info("The Import  Bill number for collection from Finacle Does Not Exist");
                    continue;
                }

                logger.info("Import bill for collection bill name {} ", importBillDetail.getExporterName());
                logger.info("Import bill for collection bill form m number {} ", importBillDetail.getFormNumber());

                ImportBillForCollection finbillsforcollection = modelMapper.map(importBillDetail, ImportBillForCollection.class);
                logger.info("Import bill for collection bill number {} ", finbillsforcollection.getBillNumber());
                ImportBillForCollectionDTO importBillForCollectionDTO = importBillService.convertEntityToBillsForCollectioncDTO(finbillsforcollection);
                logger.info(" Import Bill Parametre {} ", importBillForCollectionDTO.getBillNumber());

                FormM formm = formMRepo.findByFormNumber(importBillDetail.getFormNumber());
//
                if (null != formm && null == formm.getCifid()) {
//
                    formm.setCifid(importBillDetail.getFormMCif());
                    formMRepo.save(formm);
                }

                ImportBillForCollection tmsbillsforcollection= importBillForCollectionRepo.findByBillNumber(importBillForCollectionDTO.getBillNumber());
                if(null == tmsbillsforcollection) {
//                    ImportBillForCollection importBillForCollection = new ImportBillForCollection();
                    Boolean response = importBillService.saveImportBillsForCollectionFromFinacle(finbillsforcollection);

                    if (response) {
                        String uri = URI + "/api/bills/updateimportbillforcollection";
                        try {
                            template.postForObject(uri, importBillForCollectionDTO, String.class);
                            logger.info("Success after response Import Bills for collection ");
                        } catch (Exception ex) {
                            logger.error("Exception occurred getting response for Import Bills for collectionn {}", ex);
                            ex.printStackTrace();
                        }
                    }
//                }else {
//                    logger.info("lodge  Import bill for collection already exist");
//                }
                }else {
                    logger.info("lodge  Import bill for collection already exist");
                }
            }
        }catch (Exception e){
            logger.info("Error saving Import Bill for collection from FInacle {} ",e);
//            throw new CronJobException();
        }
        return null;
    }


    @Override
    public ImportBillUnderLcDTO convertEntityToDTO(ImportBillUnderLc importBillUnderLc) {
        ImportBillUnderLcDTO importBillUnderLcDTO = modelMapper.map(importBillUnderLc, ImportBillUnderLcDTO.class);


        if (importBillUnderLc.getWorkFlow() != null) {
//            importBillUnderLcDTO.setWorkFlowId(importBillUnderLc.getWorkFlow().getId());
//            WorkFlowStatesDTO workFlowState = workFlowStatesService.getByTypeAndCode(outwardGuarantee.getWorkFlow().getCodeType(), outwardGuarantee.getStatus());
//            outwardGuaranteeDTO.setStatusDesc(workFlowState.getDescription());
        }

        return importBillUnderLcDTO;
    }

    @Override
    public ImportBillUnderLcDTO convertEntityToBillsUnderLcDTO(ImportBillUnderLc billsUnderLc) {
        ImportBillUnderLcDTO billsUnderLcDTO = modelMapper.map(billsUnderLc, ImportBillUnderLcDTO.class);
//        billsUnderLcDTO.setLcNumber(billsUnderLc.getImportLc().getLcNumber());
//        billsUnderLcDTO.setLcCurrency(billsUnderLc.getExportLc().getLcCurrency());
//        billsUnderLcDTO.setExporterName(billsUnderLc.getExportLc().getFormNxp().getExporterName());
//        billsUnderLcDTO.setExporterAddress(billsUnderLc.getExportLc().getFormNxp().getExporterAddress());
//        billsUnderLcDTO.setExporterCountry(billsUnderLc.getExportLc().getFormNxp().getExporterCountry());
//        billsUnderLcDTO.setExporterState(billsUnderLc.getExportLc().getFormNxp().getExporterState());
//        billsUnderLcDTO.setFormNxpNumber(billsUnderLc.getExportLc().getFormNxp().getFormNum());

        if (billsUnderLc.getWorkFlow() != null) {
            billsUnderLcDTO.setWorkFlowId(billsUnderLc.getWorkFlow().getId());

        }

        return billsUnderLcDTO;
    }

    @Override
    public ImportBillForCollectionDTO convertEntityToBillsForCollectioncDTO(ImportBillForCollection billsForCollection) {
        ImportBillForCollectionDTO billsForCollectionDTO = modelMapper.map(billsForCollection, ImportBillForCollectionDTO.class);

        if (billsForCollection.getWorkFlow() != null) {
            billsForCollectionDTO.setWorkFlowId(billsForCollection.getWorkFlow().getId());

        }

        return billsForCollectionDTO;
    }



    @Transactional
    @Override
    public Boolean saveImportBillsUnderLcFromFinacle(ImportBillUnderLc billsUnderLc) throws CronJobException {
        try {

//            billsUnderLc.setStatus("A");
            billsUnderLc.setInitiatedBy("FINACLE");
            billsUnderLc.setCreatedOn(new Date());
            billsUnderLc.setSubmitFlag("S");

            SettingDTO wfSetting = configurationService.getSettingByName("DEF_IMPORT_BILLS_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            billsUnderLc.setWorkFlow(workFlow);

            String generatedFormNumber = FormNumber.getImportBillUnderLc();

//            tmsBillsUnderLc.setUserType("BAN")
            billsUnderLc.setTemporalBllNumb(generatedFormNumber);

            importBillUnderLcRepo.save(billsUnderLc);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

    @Transactional
    @Override
    public Boolean saveImportBillsForCollectionFromFinacle(ImportBillForCollection billsForCollection) throws CronJobException {
        try {
//            billsForCollection.setStatus("A");
            billsForCollection.setInitiatedBy("FINACLE");
            billsForCollection.setCreatedOn(new Date());
            billsForCollection.setSubmitFlag("S");

            SettingDTO wfSetting = configurationService.getSettingByName("DEF_IMPORT_BILLS_WF");
            WorkFlow workFlow = workFlowRepo.findByCodeType(wfSetting.getValue());
            billsForCollection.setWorkFlow(workFlow);

            String generatedFormNumber = FormNumber.getImportBillUnderLc();

//            tmsBillsUnderLc.setUserType("BAN")
            billsForCollection.setTemporalBllNumb(generatedFormNumber);

            importBillForCollectionRepo.save(billsForCollection);
            return true;
        } catch (CronJobException e) {

            throw new CronJobException(messageSource.getMessage("form.update.failure", null, locale), e);
        }
    }

}
