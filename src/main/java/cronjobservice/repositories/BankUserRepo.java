package cronjobservice.repositories;

import cronjobservice.models.BankUser;
import cronjobservice.models.Branch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by chiomarose on 15/08/2017.
 */

@Repository
@Transactional
public interface BankUserRepo extends CommonRepo<BankUser,Long > {

    BankUser findFirstByUserNameIgnoreCase(String s);

    BankUser findFirstByUserName(String s);

    Page<BankUser> findByBranch(Branch branch, Pageable pageable);

    @Modifying
    @Query("update BankUser  u set u.lastLoginDate = current_timestamp() , u.lockedUntilDate = NULL, u.noOfLoginAttempts = 0 where u.userName = :name")
    void updateUserAfterLogin(@Param("name") String userName);


}
