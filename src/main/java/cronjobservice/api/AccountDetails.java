package cronjobservice.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by ayoade_farooq@yahoo.com on 3/20/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDetails {

    private String cifId;
    private String accountNumber;
    private String accountName;
    private String schemeType;
    private String schemeCode;
    private String currency;
    private String status;
    private String  solId;
    private String ledgerBalance;
    private String availableBalance;
    private String freezeCode;
    private String sanctionLimit;
    private  String drawingPower;
    private String accountId;
    private boolean isRetail;

    public AccountDetails(){

    }

    public String getCifId() {
        return cifId;
    }

    public void setCifId(String cifId) {
        this.cifId = cifId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getSchemeType() {
        return schemeType;
    }

    public void setSchemeType(String schemeType) {
        this.schemeType = schemeType;
    }

    public String getSchemeCode() {
        return schemeCode;
    }

    public void setSchemeCode(String schemeCode) {
        this.schemeCode = schemeCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSolId() {
        return solId;
    }

    public void setSolId(String solId) {
        this.solId = solId;
    }

    public String getLedgerBalance() {
        return ledgerBalance;
    }

    public void setLedgerBalance(String ledgerBalance) {
        this.ledgerBalance = ledgerBalance;
    }

    public String getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        this.availableBalance = availableBalance;
    }

    public String getFreezeCode() {
        return freezeCode;
    }

    public void setFreezeCode(String freezeCode) {
        this.freezeCode = freezeCode;
    }

    public String getSanctionLimit() {
        return sanctionLimit;
    }

    public void setSanctionLimit(String sanctionLimit) {
        this.sanctionLimit = sanctionLimit;
    }

    public String getDrawingPower() {
        return drawingPower;
    }

    public void setDrawingPower(String drawingPower) {
        this.drawingPower = drawingPower;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public boolean isRetail() {
        return isRetail;
    }

    public void setRetail(boolean retail) {
        isRetail = retail;
    }

    @Override
    public String toString() {
        return "AccountDetails{" +
                "cifId='" + cifId + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", accountName='" + accountName + '\'' +
                ", schemeType='" + schemeType + '\'' +
                ", schemeCode='" + schemeCode + '\'' +
                ", currency='" + currency + '\'' +
                ", status='" + status + '\'' +
                ", solId='" + solId + '\'' +
                ", ledgerBalance='" + ledgerBalance + '\'' +
                ", availableBalance='" + availableBalance + '\'' +
                ", freezeCode='" + freezeCode + '\'' +
                ", sanctionLimit='" + sanctionLimit + '\'' +
                ", drawingPower='" + drawingPower + '\'' +
                ", accountId='" + accountId + '\'' +
                ", isRetail=" + isRetail +
                '}';
    }
}
