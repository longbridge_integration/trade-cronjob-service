package cronjobservice.exception;

/**
 * Created by Fortune on 5/9/2017.
 */
public class VerificationException extends CronJobException {

    public VerificationException(){super();}

    public VerificationException(String message){super(message);}
}
