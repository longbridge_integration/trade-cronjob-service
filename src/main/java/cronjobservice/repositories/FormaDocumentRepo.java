package cronjobservice.repositories;

import cronjobservice.models.FormaDocument;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by JUDE on 1/8/2018.
 */
public interface FormaDocumentRepo extends JpaRepository<FormaDocument, Long> {
}
