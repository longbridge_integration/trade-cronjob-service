package cronjobservice.repositories;


import cronjobservice.models.FormM;
import cronjobservice.models.LC;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LcRepo extends CommonRepo<LC,Long>{

    int countByFormM(FormM formM);

//    List<LC> findByFormmNotNull(FormM formM);

    Long countByCifid(String cifid);

    @Query("select s from LC s where s.status!=:openstatus")
    Page<LC> findStatus(@Param("openstatus") String status, Pageable pageable);

    @Query("select s from LC s where s.status=:openstatus")
    Page<LC> findOpenStatus(@Param("openstatus") String status, Pageable pageable);

    Page<LC> findByStatusIsNull(Pageable pageable);

    Page<LC> findByStatusNotNull(Pageable pageable);

    Page<LC> findByCifidAndStatusNotNull(String cif, Pageable pageable);

    @Query("select s from LC s where s.cifid=:cifid  and  s.status=:submitted ")
    Page<LC> findClosedLC(@Param("cifid") String cifid, @Param("submitted") String status, Pageable pageable);

//  @Query("select s from LC s where s.status is not null and s.status=:submitted")
//  Page<LC> findByStatusAndCifid(@Param("cifid") String cifid,@Param("submitted") String submitted,Pageable pageable);


    @Query("select distinct s from LC s where s.cifid=:cifid and s.status=:submitted")
    Page<LC> findByCifidAndStatusS(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

//  @Query("select s from LC s where s.status is null or s.status=:submitted or s.status=:declined")
//  Page<LC> findByStatus(@Param("submitted") String submitted,@Param("declined") String decline,Pageable pageable);


    @Query("select distinct s from LC s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<LC> findByCifidAndStatus(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);



    @Query("select distinct s from LC s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<LC> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    Page<LC> findByCifid(String cifid, Pageable pageable);

    @Query("select distinct s from LC s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<LC> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);


    @Query("select distinct s from LC s where s.cifid=:cifid  and  s.status is not null  and   s.status!=:submitted and s.status!=:saved")
    Page<LC> findByClosedCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, @Param("saved") String saved, Pageable pageable);

    //Getting Lc Details for Import Bill
    LC findByLcNumber(String lcNumber);

    @Query("select s from LC s where  s.status=:issued")
    Page<LC> findFormByStatus(@Param("issued") String submitted, Pageable pageable);

     List<LC> findByFormMAndCifid(FormM id, String customerid);

//    @Query("select  s  from LC s  where  s.cifid=:cifid and s.formM=:formM")
//    List<LC> findByformMId(@Param("cifid") String cifid, @Param("formM")FormM formM);

}
