package cronjobservice.repositories;

import cronjobservice.models.FormaAllocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by JUDE on 1/26/2018.
 */
public interface FormaAllocationRepo extends JpaRepository<FormaAllocation,Long> {
         List<FormaAllocation> findByFormaNumber(String formaNumber);
//        FormaAllocation findByFormaNumberAndDealId(String formaNumber, String dealId);
    List<FormaAllocation> findByFormaNumberAndDealId(String formaNumber, String dealId);

    @Query("select  sum (s.allocationAmount)  from FormaAllocation s  where  s.formaNumber=:formaNumber")
    BigDecimal sumAllocationAmountInFormaAllocation(@Param("formaNumber") String formaNumber);

}
