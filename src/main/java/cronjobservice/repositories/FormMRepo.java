package cronjobservice.repositories;//package longbridge.repositories;

import cronjobservice.models.FormM;
import cronjobservice.models.PTA;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by SYLVESTER on 9/7/2017.
 */
@Repository
@Transactional
public interface FormMRepo extends CommonRepo<FormM,Long> {

    FormM findByFormNumber(String formNumber);
    List<String> findByCifid(String cifId);


    FormM findByTempFormNumber(String tempFormNumber);


//    FormM findByFormNumber(String formNumber);



    @Query("select s from FormM s where s.status=:openstatus")
    Page<FormM> findOpenStatus(@Param("openstatus") String status, Pageable pageable);

    Page<FormM> findByStatusIsNull(Pageable pageable);

    Page<FormM> findByStatusNotNull(Pageable pageable);

    Page<FormM> findByCifidAndExpiryStatus(String cif, String status, Pageable pageable);

    Page<FormM> findByCifidAndStatusNotNull(String cif, Pageable pageable);

    List<FormM> findByCifidAndStatus(String cifid, String status);

    @Query("select s from FormM s where s.cifid=:cifid  and  s.status is not null and not s.status=:submitted ")
    Page<FormM> findByCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    @Query("select s from FormM s where s.status=:submitted or s.status=:approved")
    Page<FormM> findByCifidAndStatusSA(@Param("submitted") String submitted, @Param("approved") String approved, Pageable pageable);


    @Query("select s from FormM s where s.cifid=:cifid  and  s.status is not null")
    Page<FormM> findByClosedCifidAndStatus(@Param("cifid") String cifid, Pageable pageable);



    @Query("select s from FormM s where s.cifid=:cifid and s.status=:submitted")
    Page<FormM> findByCifidAndStatusS(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    @Query("select s from FormM s where s.status is null or s.status=:submitted or s.status=:declined")
    Page<FormM> findByStatus(@Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);

    @Query("select s from FormM s  where  s.cifid=:cifid and (s.status=:saved or s.status=:submitted or s.status=:declined ) ")
    Page<FormM> findByCifidAndStatusP(@Param("cifid") String cifid, @Param("saved") String saved, @Param("submitted") String submitted, @Param("declined") String decline, Pageable pageable);

    @Query("select s from FormM s where s.status=:completed")
    Page<FormM> findByStatusClosed(@Param("completed") String completed, Pageable pageable);

    @Query("select s from FormM s where s.status=:submitted")
    Page<FormM> findByStatusS(@Param("submitted") String submitted, Pageable pageable);
    //Getting Details for Import Bill For Collection
    @Query("select s from FormM s where  s.status=:approved")
    Page<FormM> findFormByStatus(@Param("approved") String approved, Pageable pageable);


    Long countByCifidAndExpiryStatus(String cifid, String expiredStatus);

    @Query("select s from FormM s where s.cifid=:cifid  and  s.status is not null and  s.status=:submitted ")
    Page<FormM> findByOpenCifidAndStatus(@Param("cifid") String cifid, @Param("submitted") String submitted, Pageable pageable);

    @Query("select s from FormM s where s.status is not null and not s.status=:saved ")
    Page<FormM> findByStatus(@Param("saved") String saved, Pageable pageable);

}
